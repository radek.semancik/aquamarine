<?php

$out_of_stock = 2; //Chování, pokud není skladem
$id_shop = 1;
$active = 1; // Default 0
$limitImport = 0;
$featureFlavor = 8;
$catHome = 2;

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/../../classes/shop/Shop.php');
require_once(dirname(__FILE__).'/../../classes/Tools.php');
require_once(dirname(__FILE__).'/../../controllers/admin/AdminImportController.php');
require_once(dirname(__FILE__).'/../../override/controllers/admin/AdminImportController.php');
require_once(dirname(__FILE__).'/function.php');


$gift = Configuration::get('HOME_GIFTED_CAT');
$top = Configuration::get('HOME_TOP_CAT');
$recommend = Configuration::get('HOME_FEATURED_CAT');

$xmlDir = array();
//$xmlDir[] = "xml/product.xml";
$xmlDir[] = "https://aquamarinespa.boost.space/api/product/presta";
//$xmlDir[] = "https://ipktest.boost.space/api/product/presta";

foreach ($xmlDir as $xml_url) {
    $xml = simplexml_load_file($xml_url);
    if (isset($xml->SHOPITEM)) {
        $importProduct = loadXmlCategory($xml, $limitImport, $catHome, $gift, $top, $recommend);
        if ($importProduct) {
            echo $importProduct;
        } else {
            echo "Žádná nová kategorie k přiřazení";
        }
    }
}

function loadXmlCategory($xml, $limitImport, $catHome, $gift, $top, $recommend)
{
    $result = false;
    $repeatImport = 1;
    if (isset($xml->SHOPITEM)) {
        foreach ($xml->SHOPITEM as $item) {
            if ($repeatImport <= $limitImport || $limitImport == 0) {
               
                $sql = 'SELECT id_product FROM '._DB_PREFIX_.'product WHERE product_no ="'.$item->ITEM_ID.'"';
                $productNo = $item->ITEM_ID;
                if (!empty($item->ITEMGROUP_ID)) {
                    $productNo = $item->ITEMGROUP_ID;
                }

                $sql = 'SELECT id_product FROM '._DB_PREFIX_.'product WHERE product_no ="'.$productNo.'"';
                $productData = Db::getInstance()->getRow($sql);

                $insertProductId = $productData["id_product"];
                //echo $insertProductId;die();
                if ($insertProductId) {
                    // Get positions for product
                    $positions = [];
                    $res = Db::getInstance()->executeS('SELECT id_category, position FROM '._DB_PREFIX_.'category_product WHERE id_product="'.$insertProductId.'"');
                    if ($res) {
                        foreach ($res as $data) {
                            $positions[$data['id_category']] = $data['position'];
                        }
                    }

                    //Delete Category
                    Db::getInstance()->delete('category_product', "id_product = ".$insertProductId);

                    //Dárek
                    if ($item->GIFT == 1) {
                        //Insert Gift  
                        $position = 0;  
                        if (array_key_exists($gift, $positions)) {
                            $position = $positions[$gift];
                        } 
                        if (!$position) {
                            $position = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'category_product WHERE position > 0 AND id_category="' . $gift. '"') + 1;
                        }

                        Db::getInstance()->insert('category_product', array("id_category" => $gift, "id_product" => $insertProductId, "position" => $position));
                    } else {
                        //Produkty
                        //Insert Default category
                        $position = 0; 
                        if (array_key_exists($catHome, $positions)) {
                            $position = $positions[$catHome];
                        } 
                        if (!$position) {
                            $position = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'category_product WHERE position > 0 AND id_category="' . $catHome. '"') + 1;
                        }
                        $insertProductCat = Db::getInstance()->insert('category_product', array("id_category" => $catHome, "id_product" => $insertProductId, "position" => $position));

                        if ($item->RECOMMENDED_PRODUCT == 1) {
                            //Insert Recommended
                            $position = 0;    
                            if (array_key_exists($recommend, $positions)) {
                                $position = $positions[$recommend];
                            } 
                            if (!$position) {
                                $position = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'category_product WHERE position > 0 AND id_category="' . $recommend. '"') + 1;
                            }
                            Db::getInstance()->insert('category_product', array("id_category" => $recommend, "id_product" => $insertProductId, "position" => $position));
                        }

                        if ($item->TOP_PRODUCT == 1) {
                            //Insert Recommended
                            $position = 0;    
                            if (array_key_exists($top, $positions)) {
                                $position = $positions[$top];
                            } 
                            if (!$position) {
                                $position = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'category_product WHERE position > 0 AND id_category="' . $top. '"') + 1;
                            }
                            Db::getInstance()->insert('category_product', array("id_category" => $top, "id_product" => $insertProductId, "position" => $position));
                        }

                        //Inset Category Product                        
                        foreach ($item->CATEGORIES->CATEGORY as $category) {    
                            $categoryId = getIdCategory($category);                     
                            $position = 0;   
                            if (array_key_exists($categoryId, $positions)) {
                                $position = $positions[$categoryId];
                            } 
                            if (!$position) {
                                $position = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'category_product WHERE position > 0 AND id_category="' . $categoryId. '"') + 1;
                            }

                            $insertProductCat = Db::getInstance()->insert('category_product', array("id_category" => $categoryId, "id_product" => $insertProductId, "position" => $position));
                            if ($insertProductCat) {
                                $result .= 'Produkt '.$item->PRODUCT_NAME.' (id: '.$insertProductId.') přidán do kategorie '.$category.'<br />';
                            } else {
                                $result .= "ERR6 - Nepodařilo se přiradit kategorii k produktu - ".$item->PRODUCT_NAME." id_category: ".$category.'<br />';
                                Db::getInstance()->insert('ps_import_product_log', array("message" => "ERR6 - Nepodařilo se přiradit kategorii k produktu - ".$item->PRODUCT_NAME." id_category: ".$category, "ean" => $item->EAN, "state" => "warning", "product_id" => $insertProductId));
                            }
                        }                       
                    }
                }
            }

            $repeatImport++;
        }
    }

    return $result;
}
