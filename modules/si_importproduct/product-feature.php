<?php

error_reporting('E_ALL');
ini_set('display_errors', '1');

$idShop = 1;
$idLang = 2;
$rootCategory = 2;

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/function.php');

$xml_url = "https://aquamarinespa.boost.space/api/product/presta/features";
$xml = simplexml_load_file($xml_url);


$result = false;

if (isset($xml->FEATURE)) {
    foreach ($xml->FEATURE as $item) {

        $sql = 'SELECT count(*) as count_feature FROM '._DB_PREFIX_.'feature WHERE real_feature_id ="'.(string) $item->ITEM_ID.'"';
        $countProduct = Db::getInstance()->getRow($sql);

        //Import xml Category
        $featureArr = array();
        $featureArr['real_feature_id'] = (string) $item->ITEM_ID;
        $featureArr['date_upd'] = (string) $item->DATE_UPD;
        $featureArr['position'] = (string) $item->POSITION;


        if ($countProduct["count_feature"] == 0) {
            $insertCategory = Db::getInstance()->insert('feature', $featureArr);

            if ($insertCategory) {
                $result .= 'Import proveden - '.$item->NAME.'<br />';

                $featureId = (int) Db::getInstance()->Insert_ID();
                addFeature($featureId,$item, $idShop, $idLang);
                addFeatureLang($item, $idLang, $featureId, true);
                
            }
        } else {
            $insertFeature = Db::getInstance()->update('feature', $featureArr, 'real_feature_id = '.(string) $item->ITEM_ID);

            if ($insertFeature) {

                if(Db::getInstance()->Affected_Rows()){
                    $result .= 'Update proveden - '.$item->NAME.'<br />';
                }

                $featureId = getIdFeature($item->ITEM_ID);
                addFeature($featureId,$item, $idShop, $idLang);
                addFeatureLang($item, $idLang, $featureId, false);

               
            }
        }
    }
}



if (empty($result)) {
    echo "Žádná nová vlastnost";
} else {
    echo $result;
}