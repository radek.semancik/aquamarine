<?php
error_reporting('E_ALL');
ini_set('display_errors', '1');

$out_of_stock  = 2; //Chování, pokud není skladem
$id_shop       = 1;
$active        = 1; // Default 0
$limitImport   = 0;
$featureFlavor = 8;
$catHome       = 2;
$log           = true;
$level         = 0;
$idLang = 2;



include 'category.php';

include 'product-feature.php';

include 'product-attributes.php';


ini_set('memory_limit', '-1');
ini_set('max_execution_time', '9999');

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/../../classes/shop/Shop.php');
require_once(dirname(__FILE__).'/../../classes/Tools.php');
require_once(dirname(__FILE__).'/../../controllers/admin/AdminImportController.php');
require_once(dirname(__FILE__).'/../../override/controllers/admin/AdminImportController.php');
require_once(dirname(__FILE__).'/function.php');




$gift = false; //Configuration::get('HOME_GIFTED_CAT');
//$xml_url = "https://www.dropshipping.cz/xml/client-eshop/product-list/6224/";

$xmlDir   = array();
//$xmlDir[] = "xml/product.xml";
$xmlDir[] = "https://aquamarinespa.boost.space/api/product/presta";
//$xmlDir[] = "https://ipktest.boost.space/api/product/presta";



foreach ($xmlDir as $xml_url) {

    $xml = simplexml_load_file($xml_url);
    if (isset($xml->SHOPITEM)) {
        $importProduct = loadXml($xml, $limitImport, $gift, $idLang, $id_shop, $out_of_stock);        
        if ($importProduct) {
            echo $importProduct;
        } else {
            echo $xml_url." - Žádná nová data <br />";
        }
        Tools::clearSmartyCache();
    }
}

function loadXml($xml, $limitImport, $gift, $idLang, $id_shop, $out_of_stock)
{
    $result       = false;
    $repeatImport = 1;


//    foreach ($xml->SHOPITEM as $item) {
//        if (!empty($item->ITEMGROUP_ID)) {
//            $productCombination = getProductAllWithNo($item->ITEMGROUP_ID);
//
//            if ($item->DATE_UPD > $productCombination['date_upd']) {
//                deleteCombination($productCombination['id_product'], $item);
//            }
//        }
//    }


    $products = [];
    $productsActive = [];
    $productsDepracated = [];

    $sql   = 'SELECT * FROM '._DB_PREFIX_.'lang';
    $langs = Db::getInstance()->ExecuteS($sql);




    foreach ($xml->SHOPITEM as $item) {

        foreach ($langs as $lang) {

            if (in_array(Configuration::get('BOOSTSPACE_PRODUCT_LIST_'.strtoupper($lang['iso_code'])), explode(",",$item->WRAPPERS))) {
                if (($repeatImport <= $limitImport || $limitImport == 0)) {

                    $sql             = 'SELECT id_product FROM '._DB_PREFIX_.'product WHERE product_no ="'.$item->ITEM_ID.'"';
                    $insertProductId = Db::getInstance()->getValue($sql);

                    $insert = true;

                    if ($insertProductId) {
                        $insert  = false;
                        $product = getProduct($insertProductId);
                    }

                    $products[] = "" . $item->ITEM_ID;

                    if ($item->STATUS == 'deprecated') {
                        $productsDepracated[] = "" . $item->ITEM_ID;
                    } else {
                        $productsActive[] = "" . $item->ITEM_ID;
                    }

                    // Call only change product date update
                    if ((isset($product) && $item->DATE_UPD > $product['date_upd']) || $insert) {
                        //Insert New product
                        //Todo Parameter delete
                        $insertProductId = addProduct($item, $insert, $gift);

                        if (!$insertProductId && $insert) {
                            $result .= 'Error - '.$item->PRODUCT_NAME.'<br />';
                            continue;
                        }
                        //Inset Product Lang
                        addProductLang($insertProductId, $lang['id_lang'], $id_shop, $item, $insert);
                        //Insert Product Shop
                        addProductShop($insertProductId, $id_shop, $item, $insert, $gift);
                        //Insert Product Variant
                        //TODO delete variant product - active 0
                        addProductVariant($insertProductId, $id_shop, $item);

                        //Insert Feature Product (vlastnosti)
                        addProductFeature($insertProductId, $item, $product);

                        //Insert Specific price
                        addSpecificPrice($insertProductId, $id_shop, $item, $insert);
                        //InsetImg
                        addImg($insertProductId, $item);
                        //Insert Stock
                        addStock($insertProductId, $id_shop, $out_of_stock, $item);

                        //Accessory product
                        addRelatedProduct($insertProductId, $item);

                        $result .= 'Import proveden - '.$item->PRODUCT_NAME.'<br />';
                    }
                }
            }
        }





        $repeatImport++;
    }

    // set inactive deleted product from aquamarinespa.boost.space
    if (is_array($products) && !empty($products)) {
        $sql = 'UPDATE '._DB_PREFIX_.'product SET active=0 WHERE product_no NOT IN (' . implode(",", $products) . ')';
        Db::getInstance()->execute($sql);
        $sql = 'UPDATE '._DB_PREFIX_.'product_shop SET active=0 WHERE id_product IN (SELECT id_product FROM '._DB_PREFIX_.'product WHERE product_no NOT IN (' . implode(",", $products) . '))';
        Db::getInstance()->execute($sql);
    }


    if (is_array($productsActive) && !empty($productsActive)) {
        $sql = 'UPDATE '._DB_PREFIX_.'product SET active=1 WHERE product_no IN (' . implode(",", $productsActive) . ')';
        Db::getInstance()->execute($sql);
        $sql = 'UPDATE '._DB_PREFIX_.'product_shop SET active=1 WHERE id_product IN (SELECT id_product FROM '._DB_PREFIX_.'product WHERE product_no IN (' . implode(",", $productsActive) . '))';
        Db::getInstance()->execute($sql);
    }

    if (is_array($productsDepracated) && !empty($productsDepracated)) {
        $sql = 'UPDATE '._DB_PREFIX_.'product SET active=0 WHERE product_no IN (' . implode(",", $productsDepracated) . ')';
        Db::getInstance()->execute($sql);
        $sql = 'UPDATE '._DB_PREFIX_.'product_shop SET active=0 WHERE id_product IN (SELECT id_product FROM '._DB_PREFIX_.'product WHERE product_no IN (' . implode(",", $productsDepracated) . '))';
        Db::getInstance()->execute($sql);
    }



    return $result;
}
include 'product-category.php';
echo "End of import";
Tools::clearSmartyCache();