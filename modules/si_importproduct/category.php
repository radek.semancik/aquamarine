<?php

error_reporting('E_ALL');
ini_set('display_errors', '1');

$idShop = 1;
$idLang = 2;
$rootCategory = 2;

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/function.php');

$xml_url = "https://aquamarinespa.boost.space/api/product-category/presta";
//$xml_url = "https://ipktest.boost.space/api/product-category/presta";
$xml = simplexml_load_file($xml_url);
$result = false;

if (isset($xml->CATEGORY)) {
    foreach ($xml->CATEGORY as $item) {

        $sql = 'SELECT count(*) as count_category FROM '._DB_PREFIX_.'category WHERE real_cat_id ="'.(string) $item->ITEM_ID.'"';
        $countProduct = Db::getInstance()->getRow($sql);

        //Import xml Category
        $categoryArr = array();
        $categoryArr['real_cat_id'] = (string) $item->ITEM_ID;
        $categoryArr['real_par_id'] = (string) $item->PARENT_ID;
        $categoryArr['date_upd'] = (string) $item->DATE_UPD;
        $categoryArr['active'] = (string) $item->ACTIVE;
        $categoryArr['level_depth'] = (string) $item->LEVEL;

        if ($item->PARENT_ID == 0) {
            $categoryArr['id_parent'] = $rootCategory;
        }

        if ($countProduct["count_category"] == 0) {
            $categoryArr['date_add'] = date("Y-m-d H:i:s");
            $insertCategory = Db::getInstance()->insert('category', $categoryArr);

            if ($insertCategory) {
                $result .= 'Import proveden - '.$item->NAME.'<br />';

                $lastId = (int) Db::getInstance()->Insert_ID();
                addCategoryGroup($lastId);
                addCategoryLang($item, $idShop, $idLang, $lastId, true);
                addCategoryShop($item, $idShop, $lastId, true);
            }
        } else {
            $insertCategory = Db::getInstance()->update('category', $categoryArr, 'real_cat_id = '.(string) $item->ITEM_ID);

            if ($insertCategory) {
                
                if(Db::getInstance()->Affected_Rows()){
                    $result .= 'Update proveden - '.$item->NAME.'<br />';
                }

                $categoryId = getIdCategory($item->ITEM_ID);
                addCategoryLang($item, $idShop, $idLang, $categoryId, false);
                addCategoryShop($item, $idShop, $categoryId, false);
            }
        }
    }

    //Sets Id by prestashop
    foreach ($xml->CATEGORY as $item) {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'category WHERE real_cat_id ="'.(string) $item->ITEM_ID.'"';
        $categoryData = Db::getInstance()->getRow($sql);

        $insertCategory = Db::getInstance()->update('category', array("id_parent" => $categoryData["id_category"]), 'real_par_id = '.$item->ITEM_ID);
    }
}



if (empty($result)) {
    echo "Žádná nová kategorie";
} else {
    echo $result;
}

Category::regenerateEntireNtree();
Tools::clearSmartyCache();