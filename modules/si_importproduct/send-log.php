<?php

error_reporting('E_ALL');
ini_set('display_errors', '1');

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/../../classes/shop/Shop.php');
require_once(dirname(__FILE__).'/../../classes/Tools.php');
require_once(dirname(__FILE__).'/../../controllers/admin/AdminImportController.php');
require_once(dirname(__FILE__).'/../../override/controllers/admin/AdminImportController.php');
require_once(dirname(__FILE__).'/../../classes/Mail.php');

$yesterday = date('Y-m-d',strtotime("-1 days"));
$today = date('Y-m-d');

$sql = 'SELECT * FROM '._DB_PREFIX_.'si_topcigars_log WHERE `date` > "'.$yesterday.'" && `date` < "'.$today.'" and state = "price"';
$log = Db::getInstance()->executeS($sql);

$html = '';
$html .= '<h2>Změna ceny</h2>';
$html .= '<table border="1" cellspacing="0" cellpadding="10" width="600">';
$html .= '<tr>';
$html .= '<td width="100">ID produktu</td>';
$html .= '<td width="200">Jméno produktu</td>';
$html .= '<td width="300">Zpráva</td>';
$html .= '</tr>';
foreach ($log as $value) {
  $html .= '<tr>';
  $html .= '<td valign="top" width="100">'.$value['product_id'].'</td>';
  $html .= '<td valign="top" width="200">'.$value['product_name'].'</td>';
  $html .= '<td valign="top" width="300">'.$value['message'].'</td>';
  $html .= '</tr>';
}
$html .= '</table>';



$sql = 'SELECT * FROM '._DB_PREFIX_.'si_topcigars_log WHERE `date` > "'.$yesterday.'" && `date` < "'.$today.'" and state = "product"';
$log = Db::getInstance()->executeS($sql);

$html .= "<h2>Přidání nové produktu</h2>";
$html .= '<table border="1" cellspacing="0" cellpadding="10" width="600">';
$html .= '<tr>';
$html .= '<td width="100">ID produktu</td>';
$html .= '<td width="200">Jméno produktu</td>';
$html .= '</tr>';
foreach ($log as $value) {
  $html .= '<tr>';
  $html .= '<td valign="top" width="100">'.$value['product_id'].'</td>';
  $html .= '<td valign="top" width="200">'.$value['product_name'].'</td>';
  $html .= '</tr>';
}
$html .= '</table>';



$sql = 'SELECT * FROM '._DB_PREFIX_.'si_topcigars_log WHERE `date` > "'.$yesterday.'" && `date` < "'.$today.'" and state = "stock"';
$log = Db::getInstance()->executeS($sql);

$html .= "<h2>Změna množství</h2>";
$html .= '<table border="1" cellspacing="0" cellpadding="10" width="600">';
$html .= '<tr>';
$html .= '<td width="100">ID produktu</td>';
$html .= '<td width="200">Jméno produktu</td>';
$html .= '<td width="300">Zpráva</td>';
$html .= '</tr>';
foreach ($log as $value) {
  $html .= '<tr>';
  $html .= '<td valign="top" width="100">'.$value['product_id'].'</td>';
  $html .= '<td valign="top" width="200">'.$value['product_name'].'</td>';
  $html .= '<td valign="top" width="300">'.$value['message'].'</td>';
  $html .= '</tr>';
}
$html .= '</table>';




$to      = 'radek@svetit.cz'; // 'johny@example.com, sally@example.com'
$subject = 'log import product top cigars';


$urlLogUpload = _PS_UPLOAD_DIR_."log-".date("ymd").".html";
$urlLog = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST']._PS_UPLOAD_FOLDER_."log-".date("ymd").".html";
$message = 'Export ze dne: '.date('d. m. Y.',strtotime("-1 days")).' naleznete <a href="'.$urlLog.'">zde</a>.';


$myfile = fopen($urlLogUpload, "w") or die("Unable to open file!");
fwrite($myfile, iconv("UTF-8", "Windows-1250", $html));


// To send HTML mail, the Content-type header must be set
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=utf-8';

// Additional headers
$headers[] = 'From: info@svetit.cz';
//$headers[] = 'Cc: birthdayarchive@example.com';
//$headers[] = 'Bcc: birthdaycheck@example.com';

// Mail it
$send = mail($to, $subject, $message, implode("\r\n", $headers));

if($send){
   echo "Email byl úspěšně odeslán";
}