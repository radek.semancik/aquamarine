<?php

ini_set('memory_limit', '-1');
ini_set('max_execution_time', '9999');

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/../../classes/shop/Shop.php');
require_once(dirname(__FILE__).'/../../classes/Tools.php');
require_once(dirname(__FILE__).'/../../controllers/admin/AdminImportController.php');
require_once(dirname(__FILE__).'/../../override/controllers/admin/AdminImportController.php');
require_once(dirname(__FILE__).'/function.php');


Tools::clearSmartyCache();


