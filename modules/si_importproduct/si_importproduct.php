<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Foundation\Database\EntityManager;

class Si_Importproduct extends Module
{

    public function __construct(EntityManager $entity_manager)
    {
        $this->name                   = 'si_importproduct';
        $this->tab                    = 'front_office_features';
        $this->version                = '1.0.0';
        $this->author                 = 'BoostSpace';
        $this->need_instance          = 0;
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap              = true;

        parent::__construct();

        $this->entity_manager = $entity_manager;
        $this->displayName    = $this->l('Import Product');
        $this->description    = $this->l('Import XML Data');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        copy(__DIR__.'/override/AdminImportController.php', _PS_ROOT_DIR_.'/override/controllers/admin/AdminImportController.php');

        if (!parent::install() || !$this->alterTable('add')) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        $html = false;
        $html .= "<p>Kategorie: /modules/si_importproduct/category.php</p>";
        $html .= "<p>Produkty: /modules/si_importproduct/product-full.php</p>";

        $langs_repository = $this->entity_manager->getRepository('Language');
        $langs            = $langs_repository->findAll();

        if (Tools::isSubmit('submitImportProduct')) {

            foreach ($langs as $lang) {
                if (!empty(Tools::getValue('BOOSTSPACE_PRODUCT_LIST_'.strtoupper($lang->iso_code)))) {
                    Configuration::updateValue('BOOSTSPACE_PRODUCT_LIST_'.strtoupper($lang->iso_code), Tools::getValue('BOOSTSPACE_PRODUCT_LIST_'.strtoupper($lang->iso_code)));
                }else{
                    Configuration::updateValue('BOOSTSPACE_PRODUCT_LIST_'.strtoupper($lang->iso_code), null);
                }
            }


            $html .= $this->displayConfirmation($this->trans('Settings updated.', array(), 'Admin.Notifications.Success'));

            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=6&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
        }

        $helper                = new HelperForm();
        $helper->submit_action = 'submitImportProduct';
        

        $field = array();
        foreach ($langs as $lang) {          
            $field[] = array(
                'type' => 'text',
                'label' => $this->trans('Products list '.$lang->iso_code, array(), 'Modules.Importproduct.Admin'),
                'name' => 'BOOSTSPACE_PRODUCT_LIST_'.strtoupper($lang->iso_code),
                'value' => Configuration::get('BOOSTSPACE_PRODUCT_LIST_'.strtoupper($lang->iso_code)),
                'class' => 'fixed-width-xl',
            );
            $helper->fields_value['BOOSTSPACE_PRODUCT_LIST_'.strtoupper($lang->iso_code)] = Configuration::get('BOOSTSPACE_PRODUCT_LIST_'.strtoupper($lang->iso_code));
        }

        $field_result = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Settings', array(), 'Admin.Global'),
                    'icon' => 'icon-cogs'
                ),
                'input' => $field,
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions')
                ),
            ),
        );

        $html .= $helper->generateForm(array($field_result));

        return $html;
    }

    public function alterTable($method)
    {
        switch ($method) {
            case 'add':
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'product ADD `product_no` varchar(255) NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'product ADD `image_upd` datetime NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'product ADD `gift` TINYINT(1) NOT NULL DEFAULT 0');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'product ADD `unit` varchar(255) NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'product ADD `unit_packaging` varchar(255) NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'category ADD `real_cat_id` BIGINT(1) NOT NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'category ADD `real_par_id` BIGINT(1) NOT NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'feature ADD `real_feature_id` BIGINT(1) NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'feature ADD COLUMN `date_upd` DATETIME NULL DEFAULT NULL AFTER `real_feature_id`;');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'feature_value ADD COLUMN `real_feature_value_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `custom`;');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'product_attribute ADD `product_no` varchar(255) NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'product_attribute ADD `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
                Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'import_product_log (
  `id` int(11) unsigned NOT NULL,
  `message` text,
  `state` varchar(50) DEFAULT NULL,
  `ean` varchar(50) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'import_product_log ADD PRIMARY KEY (`id`);');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'import_product_log MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'address ADD `real_id` int(11) NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'customer ADD `real_user_id` int(11) NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'customer ADD `payment_terms` text NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'attribute_group ADD `real_group_id` int(11) NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'attribute ADD `real_attribute_id` int(11) NULL');
                Db::getInstance()->Execute('ALTER TABLE '._DB_PREFIX_.'attribute ADD `del` int(11) NULL');




                break;

            case 'remove':
                break;
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() OR ! $this->alterTable('remove')) return false;
        return true;
    }
}