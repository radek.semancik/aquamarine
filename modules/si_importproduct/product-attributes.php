<?php

error_reporting('E_ALL');
ini_set('display_errors', '1');

$idShop = 1;
$idLang = 2;
$rootCategory = 2;

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/function.php');

$xml_url = "https://aquamarinespa.boost.space/api/product/presta/attributes";
$xml = simplexml_load_file($xml_url);
$result = false;

if (isset($xml->ATTRIBUTE)) {
    foreach ($xml->ATTRIBUTE as $item) {

        $sql = 'SELECT count(*) as count_group FROM '._DB_PREFIX_.'attribute_group WHERE real_group_id ="'.(string) $item->ITEM_ID.'"';
        $countProduct = Db::getInstance()->getRow($sql);

        //Import xml Category
        $attributeArr = array();
        $attributeArr['real_group_id'] = (string) $item->ITEM_ID;        
        $attributeArr['group_type'] = "select";
 
        if ($countProduct["count_group"] == 0) {
            $insertGroup = Db::getInstance()->insert('attribute_group', $attributeArr);

            if ($insertGroup) {
                $result .= 'Import proveden - '.$item->NAME.'<br />';

                $groupId = (int) Db::getInstance()->Insert_ID();
                addGroupLang($groupId, $item, $idLang, $idShop, true);
                addAttribute($groupId, $item, $idLang, $idShop, true);
            }
        } else {
            $insertFeature = Db::getInstance()->update('attribute_group', $attributeArr, 'real_group_id = '.(string) $item->ITEM_ID);

            if ($insertFeature) {

                if(Db::getInstance()->Affected_Rows()){
                    $result .= 'Update proveden - '.$item->NAME.'<br />';
                }

                $groupId = getIdGroup($item->ITEM_ID);
                addGroupLang($groupId, $item, $idLang, $idShop, false);
                if(addAttribute($groupId, $item, $idLang, $idShop, false)){
                     $result .= 'Update proveden - '.$item->NAME.'<br />';
                }

               
            }
        }
    }
}



if (empty($result)) {
    echo "Žádný nový atribut";
} else {
    echo $result;
}