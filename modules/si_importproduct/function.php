<?php

function addCategoryGroup($categoryId)
{
    //Add to GroupID
    $groupId = array(1, 2, 3);
    foreach ($groupId as $group) {
        Db::getInstance()->insert('category_group', array("id_category" => $categoryId, "id_group" => $group));
    }
}

function addCategoryLang($category, $idShop, $idLang, $lastId, $insert = false)
{
    //Category lang
    $categoryArr                 = array();
    $categoryArr['id_category']  = $lastId;
    $categoryArr['id_shop']      = $idShop;
    $categoryArr['id_lang']      = $idLang;
    $categoryArr['name']         = (string) $category->NAME;
    $categoryArr['link_rewrite'] = Tools::str2url((string) $category->NAME);

    if ($insert) {
        Db::getInstance()->insert('category_lang', $categoryArr);
    } else {
        Db::getInstance()->update('category_lang', $categoryArr, 'id_category = '.(string) $lastId);
    }
}

function addCategoryShop($category, $idShop, $lastId, $insert = false)
{
    $categoryArr                = array();
    $categoryArr['id_category'] = $lastId;
    $categoryArr['id_shop']     = $idShop;
    $categoryArr['position']    = $category->POSITION;

    if ($insert) {
        Db::getInstance()->insert('category_shop', $categoryArr);
    } else {
        Db::getInstance()->update('category_shop', $categoryArr, 'id_category = '.$lastId);
    }
}

function getIdCategory($realCatId)
{
    $sql        = 'SELECT id_category FROM '._DB_PREFIX_.'category WHERE real_cat_id ="'.$realCatId.'"';
    $categoryId = Db::getInstance()->getValue($sql);
    return $categoryId;
}

function getProductId($productNo)
{
    $sql       = 'SELECT id_product FROM '._DB_PREFIX_.'product WHERE product_no ="'.$productNo.'"';
    $productId = Db::getInstance()->getValue($sql);
    return $productId;
}

function getProductAllWithNo($productNo)
{
    $sql = 'SELECT * FROM '._DB_PREFIX_.'product WHERE product_no ="'.$productNo.'"';
    return Db::getInstance()->getRow($sql);
}

function getProductAttributeId($productNo)
{
    $sql       = 'SELECT id_product_attribute FROM '._DB_PREFIX_.'product_attribute WHERE product_no ="'.$productNo.'"';
    $productId = Db::getInstance()->getValue($sql);
    return $productId;
}

function getProduct($id)
{
    $sql = 'SELECT * FROM '._DB_PREFIX_.'product WHERE id_product ="'.$id.'"';
    return Db::getInstance()->getRow($sql);
}

function getProductUpdate($id)
{
    $sql = 'SELECT date_upd FROM '._DB_PREFIX_.'product WHERE id_product ="'.$id.'"';
    return Db::getInstance()->getValue($sql);
}

function getProductImageUpdate($id)
{
    $sql = 'SELECT image_upd FROM '._DB_PREFIX_.'product WHERE id_product ="'.$id.'"';
    return Db::getInstance()->getValue($sql);
}

function getDefaultAttribute($id)
{
    $sql = 'SELECT id_product_attribute FROM '._DB_PREFIX_.'product_attribute WHERE id_product ='.$id.' and default_on = 1';
    return Db::getInstance()->getValue($sql);
}

function setProductUpdate($date, $id)
{
    Db::getInstance()->update('product', array('date_upd' => $date), 'id_product = '.$id);
}

function setProductImageUpdate($date, $id)
{
    Db::getInstance()->update('product', array('image_upd' => $date), 'id_product = '.$id);
}

function setProductVariant($id, $idAttr)
{
    Db::getInstance()->update('product', array('cache_default_attribute' => $idAttr), 'id_product = '.$id);
}

function addProduct($item, $insert = false, $gift = false)
{
    $productArr                       = array();
    $productArr['date_add']           = date("Y-m-d H:i:s");
    $productArr['date_upd']           = date("Y-m-d H:i:s");
    $productArr['active']             = 1;
    $productArr['id_tax_rules_group'] = 1;
    $productArr['redirect_type']      = '404';
    if (count($item->PARAMETERS->PARAMETER) == 0) {
        $productArr['ean13'] = (string) $item->EAN;
    }
    $productArr['product_no']      = (string) $item->ITEM_ID;
    $productArr['reference']       = (string) $item->CODE;
    $productArr['wholesale_price'] = (string) $item->PRICE_WHOLESALE;
    $productArr['price']           = (string) $item->PRICE;
    $productArr['gift']            = (string) $item->GIFT;
    $productArr['unit']            = (string) $item->UNIT;
    $productArr['unit_packaging']  = (string) $item->PACKAGING_UNIT;

    if (!empty($item->GIFT)) {
        $productArr['id_category_default'] = $gift;
    } else {
        $productArr['id_category_default'] = getIdCategory($item->ID_CATEGORY_DEFAULT);
    }


    if ($insert) {
        Db::getInstance()->insert('product', $productArr);
        $insertProductId = Db::getInstance()->Insert_ID();
        Db::getInstance()->insert('import_product_log',
            array("message" => "Přidání nového produktu", "ean" => $item->EAN, "state" => "product", "product_id" => $insertProductId, "product_name" => $item->PRODUCT_NAME));
    } else {
        $itemId          = (string) $item->ITEM_ID;
        $insertProductId = getProductId($itemId);
        Db::getInstance()->update('product', $productArr, 'id_product = '.$insertProductId);
    }

    return $insertProductId;
}

function addProductLang($insertProductId, $idLang, $id_shop, $item, $insert = false)
{

    $productLangArr                      = array();
    $productLangArr['id_product']        = $insertProductId;
    $productLangArr['id_shop']           = $id_shop;
    $productLangArr['id_lang']           = $idLang;
    $html                                = preg_replace('#<!\[CDATA\[(.+?)\]\]>#s', '$1', $item->DESCRIPTION_HTML);
    $productLangArr['description']       = $html;
    $productLangArr['description_short'] = (string) strip_tags($item->DESCRIPTION, "<p><a><br><ul><li><strong>");
    $productLangArr['name']              = (string) mres($item->PRODUCT_NAME);
    $productLangArr['available_now']     = "skladem";
    $productLangArr['available_later']   = "na objednávku";
    $productLangArr['link_rewrite']      = Tools::str2url((string) mres($item->PRODUCT_NAME));
    
    if ($insert) {
        $insertProductLang = Db::getInstance()->insert('product_lang', $productLangArr);

        if (!$insertProductLang) {
            Db::getInstance()->insert('import_product_log',
                array("message" => "ERR3 - Nepodařilo se vložit product lang  - ".$item->PRODUCT_NAME, "ean" => $item->EAN, "state" => "warning", "product_id" => $insertProductId));
        }
    } else {
        Db::getInstance()->update('product_lang', $productLangArr, 'id_product = '.$insertProductId.' && id_lang = '.$idLang);
    }
}

function addProductShop($insertProductId, $id_shop, $item, $insert = false, $gift = false)
{
    $productShopArr                       = array();
    $productShopArr['id_product']         = $insertProductId;
    $productShopArr['id_shop']            = $id_shop;
    $productShopArr['date_add']           = date("Y-m-d H:i:s");
    $productShopArr['date_upd']           = date("Y-m-d H:i:s");
    $productShopArr['active']             = 1;
    $productShopArr['id_tax_rules_group'] = 1;
    $productShopArr['redirect_type']      = '404';
    $productShopArr['wholesale_price']    = (string) $item->PRICE;
    $productShopArr['price']              = (string) $item->PRICE;
    $productShopArr['indexed']            = 1;
    if (!empty($item->GIFT)) {
        $productShopArr['id_category_default'] = $gift;
    } else {
        $productShopArr['id_category_default'] = getIdCategory($item->ID_CATEGORY_DEFAULT);
    }


    if ($insert) {
        $insertProductShop = Db::getInstance()->insert('product_shop', $productShopArr);

        if (!$insertProductShop) {
            Db::getInstance()->insert('import_product_log',
                array("message" => "ERR4 - Nepodařilo se vložit product shop  - ".$item->PRODUCT_NAME, "ean" => $item->EAN, "state" => "warning", "product_id" => $insertProductId));
        }
    } else {
        Db::getInstance()->update('product_shop', $productShopArr, 'id_product = '.$insertProductId);
    }
}

function addProductVariant($insertProductId, $id_shop, $item)
{
    deleteCombination($insertProductId, $item);

    foreach ($item->PARAMETERS->PARAMETER as $parametr) {
        $idAttribute = addProductAttribute($insertProductId, $parametr);
        addProductAttributeShop($insertProductId, $idAttribute, $parametr, $id_shop);
        addStockAvaliable($insertProductId, $idAttribute, $id_shop, $item);
        addProductCombination($idAttribute, $parametr);
        setProductVariant($insertProductId, getDefaultAttribute($insertProductId));
        //addProductLayred($insertProductId, $parametr, $id_shop);
    }
}

function addProductAttribute($insertProductId, $parametr)
{
    $productVariantArr               = array();
    $productVariantArr['id_product'] = $insertProductId;
    if (!empty($parametr['price'])) {
        $productVariantArr['price'] = $parametr['price'];
    }

    if (!empty($parametr['ean'])) {
        $productVariantArr['ean13'] = $parametr['ean'];
    }

    $productVariantArr['reference']  = $parametr['code'];
    //$productVariantArr['quantity']   = $parametr['stock'];
    if($parametr['default_on'] == 1){
        $productVariantArr['default_on'] = $parametr['default_on'];
    }
    
    Db::getInstance()->insert('product_attribute', $productVariantArr, true);
    $insertAttributeId = Db::getInstance()->Insert_ID();

    return $insertAttributeId;
}

function addProductAttributeShop($insertProductId, $idAttribute, $parametr, $id_shop)
{
    $productVariantArr                         = array();
    $productVariantArr['id_product']           = $insertProductId;
    $productVariantArr['id_product_attribute'] = $idAttribute;
    //$productVariantArr['wholesale_price']      = (empty($item['wholesale_price'])) ? 0 : (string) $item['wholesale_price'];
    if (!empty($parametr['price'])) {
        $productVariantArr['price'] = $parametr['price'];
    }
    $productVariantArr['id_shop']    = $id_shop;
    if($parametr['default_on'] == 1){
        $productVariantArr['default_on'] = $parametr['default_on'];
    }

    Db::getInstance()->insert('product_attribute_shop', $productVariantArr, true);
}

function addStockAvaliable($insertProductId, $idAttribute, $id_shop, $item)
{
    $productVariantArr                         = array();
    $productVariantArr['id_product']           = $insertProductId;
    $productVariantArr['id_product_attribute'] = $idAttribute;
    $productVariantArr['id_shop']              = $id_shop;
    $productVariantArr['quantity']             = $item->STOCK;
    $productVariantArr['out_of_stock']             = 1;

    Db::getInstance()->insert('stock_available', $productVariantArr);
}

function addProductCombination($idProductAttribute, $parametr)
{
    $param = explode("|", $parametr["id"]);

    foreach ($param as $realId) {
        $sql         = 'SELECT id_attribute FROM '._DB_PREFIX_.'attribute WHERE real_attribute_id ='.$realId;
        $idAttribute = Db::getInstance()->getValue($sql);

        Db::getInstance()->insert('product_attribute_combination', array("id_attribute" => $idAttribute, "id_product_attribute" => $idProductAttribute));
    }
}

//function addProductLayred($insertProductId, $parametr, $id_shop)
//{
//    $layred                       = array();
//    $layred['id_product']         = $insertProductId;
//    $layred['id_attribute']       = $parametr['id'];
//    $layred['id_shop']            = $id_shop;
//    $layred['id_attribute_group'] = 1;
//
//    Db::getInstance()->insert('layered_product_attribute', $layred, true);
//
//    $extra                       = array();
//    $extra['product_extra']         = $insertProductId;
//
//    Db::getInstance()->insert('product_extra', $extra, true);
//
//}

function deleteCombination($insertProductId, $item)
{

    Db::getInstance()->delete('product_attribute_shop', "id_product = ".$insertProductId);
    Db::getInstance()->delete('product_attribute', "id_product = ".$insertProductId);
    Db::getInstance()->delete('product_attribute_combination', "id_product_attribute = ".getProductAttributeId($item->ITEM_ID));
    Db::getInstance()->delete('stock_available', "id_product = ".$insertProductId);
}

function addProductFeature($insertProductId, $item)
{
    if ($item->FEATURES) {
        Db::getInstance()->delete('feature_product', "id_product = ".$insertProductId);

        foreach ($item->FEATURES->FEATURE as $feature) {

            $sql       = 'SELECT id_feature FROM '._DB_PREFIX_.'feature WHERE real_feature_id ='.$feature['id_feature'];
            $idFeature = Db::getInstance()->getValue($sql);

            $features = explode("|", $feature);
            foreach ($features as $value) {
                $sql            = 'SELECT id_feature_value FROM '._DB_PREFIX_.'feature_value WHERE real_feature_value_id ='.$value;
                $idFeatureValue = Db::getInstance()->getValue($sql);

                Db::getInstance()->insert('feature_product',
                    array("id_feature" => $idFeature, "id_product" => $insertProductId, "id_feature_value" => $idFeatureValue));
            }
        }
    }
}

function addFeature($featureId, $item, $id_shop, $idLang)
{
    if ($item->PARAMETERS) {
        foreach ($item->PARAMETERS->PARAMETER as $param) {

            $sql            = 'SELECT id_feature_value FROM '._DB_PREFIX_.'feature_value WHERE real_feature_value_id ='.$param['id'];
            $idFeatureValue = Db::getInstance()->getValue($sql);

            if (!empty($idFeatureValue)) {
                Db::getInstance()->update('feature_value_lang', array("id_feature_value" => $idFeatureValue, "id_lang" => $idLang, "value" => $param),
                    'id_feature_value = '.$idFeatureValue);
            } else {
                Db::getInstance()->insert('feature_value', array("id_feature" => $featureId, "custom" => 0, "real_feature_value_id" => $param['id']));
                $idFeatureValue = Db::getInstance()->Insert_ID();
                Db::getInstance()->insert('feature_value_lang', array("id_feature_value" => $idFeatureValue, "id_lang" => $idLang, "value" => $param));

                Db::getInstance()->insert('feature_shop', array("id_feature" => $featureId, "id_shop" => $id_shop));
                Db::getInstance()->insert('layered_indexable_feature', array("id_feature" => $featureId, "indexable" => 1));
            }
        }
    }
}

function addFeatureLang($category, $idLang, $featureId, $insert = false)
{
    $featureArr               = array();
    $featureArr['id_feature'] = $featureId;
    $featureArr['id_lang']    = $idLang;
    $featureArr['name']       = (string) $category->NAME;

    if ($insert) {
        Db::getInstance()->insert('feature_lang', $featureArr);
    } else {
        Db::getInstance()->update('feature_lang', $featureArr, 'id_feature = '.(string) $featureId);
    }
}

function getIdFeature($realFeatureId)
{
    $sql       = 'SELECT id_feature FROM '._DB_PREFIX_.'feature WHERE real_feature_id ="'.$realFeatureId.'"';
    $featureId = Db::getInstance()->getValue($sql);
    return $featureId;
}

function addGroupLang($groupId, $item, $idLang, $idShop, $insert)
{
    $groupArr                       = array();
    $groupArr['id_attribute_group'] = $groupId;
    $groupArr['id_lang']            = $idLang;
    $groupArr['name']               = (string) $item->NAME;
    $groupArr['public_name']        = (string) $item->NAME;
    $result                         = false;
    if ($insert) {
        $result = Db::getInstance()->insert('attribute_group_lang', $groupArr);
        Db::getInstance()->insert('attribute_group_shop', array("id_attribute_group" => $groupId, "id_shop" => $idShop));
    } else {
        $result = Db::getInstance()->update('attribute_group_lang', $groupArr, 'id_attribute_group = '.(string) $groupId);
    }

    return $result;
}

function getIdGroup($realGroupId)
{
    $sql     = 'SELECT id_attribute_group FROM '._DB_PREFIX_.'attribute_group WHERE real_group_id ="'.$realGroupId.'"';
    $groupId = Db::getInstance()->getValue($sql);
    return $groupId;
}

function addAttribute($groupId, $item, $idLang, $idShop)
{
    if ($item->PARAMETERS) {
        Db::getInstance()->update('attribute', array("del" => 1), 'id_attribute_group = '.$groupId);
        $position = 0;
        foreach ($item->PARAMETERS->PARAMETER as $param) {

            $sql         = 'SELECT id_attribute FROM '._DB_PREFIX_.'attribute WHERE real_attribute_id ='.$param['id'];
            $idAttribute = Db::getInstance()->getValue($sql);

            if (!empty($idAttribute)) {
                Db::getInstance()->update('attribute_lang', array("id_attribute" => $idAttribute, "id_lang" => $idLang, "value" => $param),
                    'id_attribute = '.$idAttribute);
                Db::getInstance()->update('attribute', array("del" => 0), 'id_attribute = '.$idAttribute);
            } else {
                $result = Db::getInstance()->insert('attribute',
                    array("id_attribute_group" => $groupId, "real_attribute_id" => $param["id"], "del" => 0, "position" => $position));
                $attrId = (int) Db::getInstance()->Insert_ID();
                Db::getInstance()->insert('attribute_lang', array("id_attribute" => $attrId, "id_lang" => $idLang, "name" => $param));
                Db::getInstance()->insert('attribute_shop', array("id_attribute" => $attrId, "id_shop" => $idShop));
            }

            $position++;
        }

        deleteAttribute();
    }

    return $result;
}

function deleteAttribute()
{
    $sql          = 'SELECT id_attribute FROM '._DB_PREFIX_.'attribute WHERE del = 1';
    $idAttributes = Db::getInstance()->executeS($sql);


    foreach ($idAttributes as $idAttribute) {
        Db::getInstance()->delete('attribute', "id_attribute = ".$idAttribute['id_attribute']);
        Db::getInstance()->delete('attribute_lang', "id_attribute = ".$idAttribute['id_attribute']);
        Db::getInstance()->delete('attribute_shop', "id_attribute = ".$idAttribute['id_attribute']);
    }
}

function addSpecificPrice($insertProductId, $id_shop, $item)
{
    Db::getInstance()->delete('specific_price', "id_product = ".$insertProductId);

    foreach ($item->SPECIFIC_PRICE->PRICE as $price) {
        $specificPriceArr                   = array();
        $specificPriceArr['id_product']     = $insertProductId;
        $specificPriceArr['id_shop']        = $id_shop;
        $specificPriceArr['reduction']      = $price;
        $specificPriceArr['reduction_type'] = (string) $price['type'];
        $specificPriceArr['price']          = -1.000000;

        Db::getInstance()->insert('specific_price', $specificPriceArr);
    }
}

function addStock($insertProductId, $id_shop, $out_of_stock, $item)
{

    Db::getInstance()->delete('stock_available', "id_product = ".$insertProductId);

//Inset Stock
    $productStockArr                         = array();
    $productStockArr['id_product']           = $insertProductId;
    $productStockArr['id_shop']              = $id_shop;
    $productStockArr['id_product_attribute'] = 0;
    $productStockArr['quantity']             = $item->STOCK;
//    if ($item->DELIVERY_DATE == 0) {
//        $productStockArr['quantity'] = 10000;
//    }
    $productStockArr['out_of_stock']         = $out_of_stock;
    Db::getInstance()->insert('stock_available', $productStockArr);
}

function addImg($id_product, $item)
{
    //Delete image
    Db::getInstance()->delete('image', "id_product = ".$id_product);
    Db::getInstance()->delete('image_lang', "id_product = ".$id_product);
    Db::getInstance()->delete('image_shop', "id_product = ".$id_product);
    Db::getInstance()->delete('image_type', "id_product = ".$id_product);

    //Insert IMG
    $url               = $item->IMGURL;
    $shops             = Shop::getShops(true, null, true);
    $image             = new Image();
    $image->id_product = $id_product;
    $image->position   = Image::getHighestPosition($id_product) + 1;
    $image->cover      = true; // or false;
    if (($image->validateFields(false, true)) === true &&
        ($image->validateFieldsLang(false, true)) === true && $image->add()) {
        $image->associateTo($shops);
        if (!AdminImportController::copyImg($id_product, $image->id, $url)) {
            $image->delete();
        }
    }


    foreach ($item->IMAGES as $value) {

        //Add  main product image
        $url               = $value->IMGURL;
        $shops             = Shop::getShops(true, null, true);
        $image             = new Image();
        $image->cover      = true;
        $image->id_product = $id_product;
        $image->position   = Image::getHighestPosition($id_product) + 1;
        $image->cover      = false; // or false;
        if (($image->validateFields(false, true)) === true &&
            ($image->validateFieldsLang(false, true)) === true && $image->add()) {
            $image->associateTo($shops);
            if (!AdminImportController::copyImg($id_product, $image->id, $url)) {
                $image->delete();
            }
        }
    }

    setProductImageUpdate($item->DATE_IMAGE_UPD, $id_product);
}

function addRelatedProduct($idProduct, $item)
{
    Db::getInstance()->delete('accessory', "id_product_1 = ".$idProduct);

    if ($item->ACCESSORY) {
        foreach ($item->ACCESSORY->PRODUCT as $idReal) {

            $sql        = 'SELECT id_product FROM '._DB_PREFIX_.'product WHERE product_no ='.$idReal;
            $IdProduct2 = Db::getInstance()->getValue($sql);

            if ($IdProduct2) {
                Db::getInstance()->insert('accessory', array("id_product_1" => $idProduct, "id_product_2" => $IdProduct2));
            }
        }
    }
}

function randomPassword()
{
    $alphabet    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass        = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n      = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function mres($value)
{
    $search  = array("\\", "\x00", "\n", "\r", "'", '"', "\x1a");
    $replace = array("\\\\", "\\0", "\\n", "\\r", "\'", '\"', "\\Z");

    return str_replace($search, $replace, $value);
}
