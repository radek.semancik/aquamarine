<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Foundation\Database\EntityManager;

class Si_Exporttoboostspace extends Module
{

    public function __construct(EntityManager $entity_manager)
    {
        $this->name                   = 'si_exporttoboostpace';
        $this->tab                    = 'front_office_features';
        $this->version                = '1.0.0';
        $this->author                 = 'BoostSpace';
        $this->need_instance          = 0;
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap              = true;

        parent::__construct();

        $this->entity_manager = $entity_manager;
        $this->displayName    = $this->l('Export data to Boostspace');
        $this->description    = $this->l('Export data to Boostspace');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        

        if (!parent::install()) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
       return true;
    }



    public function uninstall()
    {
        if (!parent::uninstall()) return false;
        return true;
    }
}