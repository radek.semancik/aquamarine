<?php

class AeucEmailEntity extends ObjectModel
{
    /** @var integer id_mail */
    public $id_mail;

    /** @var string filename */
    public $filename;

    /** @var string display_name */
    public $display_name;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'aeuc_email',
        'primary' => 'id',
        'fields' => array(
            'id_mail' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'filename' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 64),
            'display_name' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 64),
        ),
    );

    const URL = 'http://api2.ecomailapp.cz/';

    private function sendRequest($url, $request = 'POST', $data = '')
    {

        $http_headers   = array();
        $http_headers[] = "key: ".$this->parameters['key'];
        $http_headers[] = "Content-Type: application/json";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $http_headers);

        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            if ($request == 'POST') {
                curl_setopt($ch, CURLOPT_POST, TRUE);
            } else {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request);
            }
        }

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

    public function addSubscriber($list_const, $data = array(), $trigger_autoresponders = FALSE, $update_existing = TRUE, $resubscribe = FALSE)
    {
        if (!array_key_exists($list_const, $this->parameters)) {
            throw new \Exception('You must specify Ecomail contact list.');
        }

        $url = self::URL.'lists/'.$this->parameters[$list_const].'/subscribe';

        $dataRequest = array();

        foreach ($data as $key => $value) {
            $dataRequest[$key] = $value;
        }

        $post = json_encode(array(
            'subscriber_data' => $dataRequest,
            'trigger_autoresponders' => $trigger_autoresponders,
            'update_existing' => $update_existing,
            'resubscribe' => $resubscribe
        ));

        return $this->sendRequest($url, 'POST', $post);
    }

    /**
     * Return the complete email collection from DB
     * @return array|false
     * @throws PrestaShopDatabaseException
     */
    public static function getAll()
    {
        $sql = '
		SELECT *
		FROM `'._DB_PREFIX_.AeucEmailEntity::$definition['table'].'`';

        return Db::getInstance()->executeS
                ($sql);
    }

    public static function getMailIdFromTplFilename($tpl_name)
    {
        $sql = '
		SELECT `id_mail`
		FROM `'._DB_PREFIX_.AeucEmailEntity::$definition['table'].'`
		WHERE `filename` = "'.pSQL($tpl_name).'"';

        return Db::getInstance()->getRow($sql);
    }
}