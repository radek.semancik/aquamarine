<?php

if (!defined('_PS_VERSION_'))

	exit;

/**

 * Includes 

 */   

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;

use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;

use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;

use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;

use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;

use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;

use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;

use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;

class Tea_producttabhome extends Module

{    

    public $_html;

    public $templateFile;

    public function __construct()

	{

		$this->name = 'tea_producttabhome';

		$this->tab = 'front_office_features';

		$this->version = '1.0.1';

		$this->author = 'prestagold.com';

		$this->need_instance = 0;

		$this->secure_key = Tools::encrypt($this->name);

		$this->bootstrap = true;



		parent::__construct();



		$this->displayName = $this->l('Prestagold product tab home page');

		$this->description = $this->l('Product tab home page');

		$this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => _PS_VERSION_);     

        $this->templateFile = 'module:tea_producttabhome/views/templates/hook/list_products.tpl';   

    }

     /**

	 * @see Module::install()

	 */

    public function install()

	{

	    $res = parent::install();        

        $this->registerHook('displayHome');

        $this->registerHook('displayHeader');

        $this->registerHook('home'); 

        Configuration::updateValue('PRODUCT_TAB_NEW_PRODUCT', 1);

		Configuration::updateValue('PRODUCT_TAB_FEATURED_PRODUCT', 1);

		Configuration::updateValue('PRODUCT_TAB_BEST_SELLERS_PRODUCT', 1);

        Configuration::updateValue('PRODUCT_TAB_SPECIALS_PRODUCT', 1);

        Configuration::updateValue('PRODUCT_TAB_COUNT_PRODUCT', 8);  
        ProductSale::fillProductSales();
        return  $res;

    }

    /**

	 * @see Module::uninstall()

	 */

	public function uninstall()

	{

        Configuration::deleteByName('PRODUCT_TAB_NEW_PRODUCT');

        Configuration::deleteByName('PRODUCT_TAB_FEATURED_PRODUCT');

        Configuration::deleteByName('PRODUCT_TAB_BEST_SELLERS_PRODUCT');

        Configuration::deleteByName('PRODUCT_TAB_SPECIALS_PRODUCT');

        Configuration::deleteByName('PRODUCT_TAB_COUNT_PRODUCT');

        return parent::uninstall();

    }

    public function hookDisplayHeader()

    { 

        $this->context->controller->addCSS($this->_path.'css/customproductimage.css', 'all');        

    }

    public function getContent()

    {

        if (Tools::isSubmit('btnSubmit'))

		{

		      $this->_postProcess();

		}

		return $this->_html.$this->renderForm();

    }

    protected function _postProcess()

	{

		if (Tools::isSubmit('btnSubmit'))

		{

			Configuration::updateValue('PRODUCT_TAB_NEW_PRODUCT', Tools::getValue('PRODUCT_TAB_NEW_PRODUCT'));

			Configuration::updateValue('PRODUCT_TAB_FEATURED_PRODUCT', Tools::getValue('PRODUCT_TAB_FEATURED_PRODUCT'));

			Configuration::updateValue('PRODUCT_TAB_BEST_SELLERS_PRODUCT', Tools::getValue('PRODUCT_TAB_BEST_SELLERS_PRODUCT'));

            Configuration::updateValue('PRODUCT_TAB_SPECIALS_PRODUCT', Tools::getValue('PRODUCT_TAB_SPECIALS_PRODUCT'));

            Configuration::updateValue('PRODUCT_TAB_COUNT_PRODUCT', Tools::getValue('PRODUCT_TAB_COUNT_PRODUCT'));

		}

		$this->_html .= $this->displayConfirmation($this->l('Settings updated'));

	}

    public function renderForm()

	{

		$fields_form = array(

			'form' => array(

				'legend' => array(

					'title' => $this->l('Configure tab home page'),

					'icon' => 'icon-envelope'

				),

				'input' => array(

					array(

						'type' => 'switch',

						'label' => $this->l('New product tab'),

						'name' => 'PRODUCT_TAB_NEW_PRODUCT',

						'values' => array(

							array(

								'id' => 'active_on',

								'value' => 1,

								'label' => $this->l('Yes')

							),

							array(

								'id' => 'active_off',

								'value' => 0,

								'label' => $this->l('No')

							)

						),

					),

					array(

						'type' => 'switch',

						'label' => $this->l('Featured product tab'),

						'name' => 'PRODUCT_TAB_FEATURED_PRODUCT',

						'values' => array(

							array(

								'id' => 'active_on',

								'value' => 1,

								'label' => $this->l('Yes')

							),

							array(

								'id' => 'active_off',

								'value' => 0,

								'label' => $this->l('No')

							)

						),

					),

					array(

						'type' => 'switch',

						'label' => $this->l('Best sellers proudct tab'),

						'name' => 'PRODUCT_TAB_BEST_SELLERS_PRODUCT',

						'values' => array(

							array(

								'id' => 'active_on',

								'value' => 1,

								'label' => $this->l('Yes')

							),

							array(

								'id' => 'active_off',

								'value' => 0,

								'label' => $this->l('No')

							)

						),

					),

                    array(

						'type' => 'switch',

						'label' => $this->l('Specials proudct tab'),

						'name' => 'PRODUCT_TAB_SPECIALS_PRODUCT',

						'values' => array(

							array(

								'id' => 'active_on',

								'value' => 1,

								'label' => $this->l('Yes')

							),

							array(

								'id' => 'active_off',

								'value' => 0,

								'label' => $this->l('No')

							)

						),

					),

                    array(

						'type' => 'text',

						'label' => $this->l('Products to display'),

						'name' => 'PRODUCT_TAB_COUNT_PRODUCT',

                        'desc' =>$this->l('Define the number of products to be displayed in this block.'),

					),

				),

				'submit' => array(

					'title' => $this->l('Save'),

				)

			),

		);



		$helper = new HelperForm();

		$helper->show_toolbar = false;

		$helper->table = $this->table;

		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

		$helper->default_form_language = $lang->id;

		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

		$this->fields_form = array();

		$helper->id = (int)Tools::getValue('id_carrier');

		$helper->identifier = $this->identifier;

		$helper->submit_action = 'btnSubmit';

		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;

		$helper->token = Tools::getAdminTokenLite('AdminModules');

		$helper->tpl_vars = array(

			'fields_value' => $this->getConfigFieldsValues(),

			'languages' => $this->context->controller->getLanguages(),

			'id_language' => $this->context->language->id

		);



		return $helper->generateForm(array($fields_form));

	}



	public function getConfigFieldsValues()

	{

		return array(

			'PRODUCT_TAB_NEW_PRODUCT' => Tools::getValue('PRODUCT_TAB_NEW_PRODUCT', Configuration::get('PRODUCT_TAB_NEW_PRODUCT')),

			'PRODUCT_TAB_FEATURED_PRODUCT' => Tools::getValue('PRODUCT_TAB_FEATURED_PRODUCT', Configuration::get('PRODUCT_TAB_FEATURED_PRODUCT')),

			'PRODUCT_TAB_BEST_SELLERS_PRODUCT' => Tools::getValue('PRODUCT_TAB_BEST_SELLERS_PRODUCT', Configuration::get('PRODUCT_TAB_BEST_SELLERS_PRODUCT')),

            'PRODUCT_TAB_SPECIALS_PRODUCT' => Tools::getValue('PRODUCT_TAB_SPECIALS_PRODUCT', Configuration::get('PRODUCT_TAB_SPECIALS_PRODUCT')),

            'PRODUCT_TAB_COUNT_PRODUCT' => Tools::getValue('PRODUCT_TAB_COUNT_PRODUCT', Configuration::get('PRODUCT_TAB_COUNT_PRODUCT')),

		);

	}

    public function hookDisplayHome()

    {

        $assign = array();

        if((int)Configuration::get('PRODUCT_TAB_COUNT_PRODUCT'))

            $nb_product= Configuration::get('PRODUCT_TAB_COUNT_PRODUCT');

        else

            $nb_product =8;

        if((int)Configuration::get('PRODUCT_TAB_NEW_PRODUCT'))

        {

            $assign['newproducts']=$this->getNewProducts($nb_product);

        }

        if((int)Configuration::get('PRODUCT_TAB_FEATURED_PRODUCT'))

        {

            $id_category= (int)Configuration::get('HOME_FEATURED_CAT')?(int)Configuration::get('HOME_FEATURED_CAT'):2;

            $assign['featuredproducts']= $this->getProductsByIdCategory($id_category,$nb_product);

        }

        if((int)Configuration::get('PRODUCT_TAB_BEST_SELLERS_PRODUCT'))

        {

            $assign['bestsellerproducts'] = $this->getProductsBestSales($nb_product);

        }

        if((int)Configuration::get('PRODUCT_TAB_SPECIALS_PRODUCT'))

            $assign['specialproducts']= $this->getProductsSpecial($nb_product);

        $this->context->smarty->assign($assign);

        return $this->display(__FILE__, 'hometab.tpl');

    }

    public function hookHome(){

        return $this->hookDisplayHome();

    }

    public function getProductsByIdCategory($id_category,$count_product)

    {

        $category = new Category($id_category,$this->context->language->id);

        $products = $category->getProducts($this->context->language->id, 1, $count_product);

        $assembler = new ProductAssembler($this->context);

        $presenterFactory = new ProductPresenterFactory($this->context);

        $presentationSettings = $presenterFactory->getPresentationSettings();

        $presenter = new ProductListingPresenter(

            new ImageRetriever(

                $this->context->link

            ),

            $this->context->link,

            new PriceFormatter(),

            new ProductColorsRetriever(),

            $this->context->getTranslator()

        );



        $products_for_template = [];

        if($products)

            foreach ($products as $rawProduct) {

                $products_for_template[] = $presenter->present(

                    $presentationSettings,

                    $assembler->assembleProduct($rawProduct),

                    $this->context->language

                );

            }

        $this->context->smarty->assign('products',$products_for_template);

        return $this->display(__FILE__, 'list_products.tpl');

    }

    public function getProductsBestSales($count_product)

    {

        $products = ProductSale::getBestSales($this->context->language->id,0 ,$count_product,null, null);

        $assembler = new ProductAssembler($this->context);

        $presenterFactory = new ProductPresenterFactory($this->context);

        $presentationSettings = $presenterFactory->getPresentationSettings();

        $presenter = new ProductListingPresenter(

            new ImageRetriever(

                $this->context->link

            ),

            $this->context->link,

            new PriceFormatter(),

            new ProductColorsRetriever(),

            $this->context->getTranslator()

        );



        $products_for_template = [];

        if($products)

            foreach ($products as $rawProduct) {

                $products_for_template[] = $presenter->present(

                    $presentationSettings,

                    $assembler->assembleProduct($rawProduct),

                    $this->context->language

                );

            }

        $this->context->smarty->assign('products',$products_for_template);

        return $this->display(__FILE__, 'list_products.tpl');

    }

    public function getProductsSpecial($count_product)

    {

        $products = Product::getPricesDrop($this->context->language->id,0, (int)$count_product, false, null, null);

        $assembler = new ProductAssembler($this->context);

        $presenterFactory = new ProductPresenterFactory($this->context);

        $presentationSettings = $presenterFactory->getPresentationSettings();

        $presenter = new ProductListingPresenter(

            new ImageRetriever(

                $this->context->link

            ),

            $this->context->link,

            new PriceFormatter(),

            new ProductColorsRetriever(),

            $this->context->getTranslator()

        );



        $products_for_template = [];

        if($products)

            foreach ($products as $rawProduct) {

                $products_for_template[] = $presenter->present(

                    $presentationSettings,

                    $assembler->assembleProduct($rawProduct),

                    $this->context->language

                );

            }

        $this->context->smarty->assign('products',$products_for_template);

        return $this->display(__FILE__, 'list_products.tpl');

    }

    public function getNewProducts($count_product)

    {

        $products = Product::getNewProducts($this->context->language->id, 0, (int)$count_product, false, null, null);

        $assembler = new ProductAssembler($this->context);

        $presenterFactory = new ProductPresenterFactory($this->context);

        $presentationSettings = $presenterFactory->getPresentationSettings();

        $presenter = new ProductListingPresenter(

            new ImageRetriever(

                $this->context->link

            ),

            $this->context->link,

            new PriceFormatter(),

            new ProductColorsRetriever(),

            $this->context->getTranslator()

        );



        $products_for_template = [];

        if($products)

            foreach ($products as $rawProduct) {

                $products_for_template[] = $presenter->present(

                    $presentationSettings,

                    $assembler->assembleProduct($rawProduct),

                    $this->context->language

                );

            }

        $this->context->smarty->assign('products',$products_for_template);

        return $this->display(__FILE__, 'list_products.tpl');

    }  

}