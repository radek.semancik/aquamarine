{if isset($featuredproducts) || isset($newproducts)||isset($bestsellerproducts)||isset($specialproducts)}
    {assign var='active' value=true}
    <ul id="product-home-page-tabs" class="clearfix panel-filters">
        {if isset($featuredproducts)}
            <li class="homefeatured {if $active}active{/if}">
                <a class="homefeatured" href="#tabhomefeatured" data-toggle="tab">{l s='Popular' mod='tea_producttabhome'}</a>
            </li>
            {assign var='active' value=false}
        {/if}
        {if isset($bestsellerproducts)}
            <li class="blockbestsellers {if $active}active{/if}">
                <a class="blockbestsellers" href="#tabblockbestsellers" data-toggle="tab">{l s='Best Sellers' mod='tea_producttabhome'}</a>
            </li>
            {assign var='active' value=false}
        {/if}
        {if isset($specialproducts)}
            <li class="blockspecials {if $active}active{/if}">
                <a class="blockspecials" href="#tabblockspecials" data-toggle="tab">{l s='Specials' mod='tea_producttabhome'}</a>
            </li>
            {assign var='active' value=false}
        {/if}
        {if isset($newproducts)}
            <li class="blocknewproduct {if $active}active{/if}">
                <a class="blocknewproduct" href="#tabblocknewproduct" data-toggle="tab">{l s='New product' mod='tea_producttabhome'}</a>
            </li>
            {assign var='active' value=false}
        {/if}
    </ul>
{/if}
<div id="tab-content-products" class="tab-content">
    {assign var='active' value=true}
    {if isset($featuredproducts)}
        <div id="tabhomefeatured" class="tab-pane {if $active}active{/if}">{$featuredproducts nofilter}</div>
        {assign var='active' value=false}
    {/if}
    {if isset($newproducts)}
        <div id="tabblocknewproduct" class="tab-pane {if $active}active{/if}">{$newproducts nofilter}</div>
        {assign var='active' value=false}
    {/if}
    {if isset($bestsellerproducts)}
        <div id="tabblockbestsellers" class="tab-pane {if $active}active{/if}">{$bestsellerproducts nofilter}</div>
        {assign var='active' value=false}
    {/if}
    {if isset($specialproducts)}
        <div id="tabblockspecials" class="tab-pane {if $active}active{/if}">{$specialproducts nofilter}</div>
        {assign var='active' value=false} 
    {/if}
</div>
<script type="text/javascript">
$(document).on('click','#product-home-page-tabs li a',function(e){
    alert('xxx');
   $('#product-home-page-tabs li').removeClass('active');
   $(this).parent().addClass('active'); 
   $('#tab-content-products.tab-content .tab-pane').removeClass('active');
   $('#tab-content-products.tab-content '+$(this).attr('href')).addClass('active');
   return false;
});
</script>