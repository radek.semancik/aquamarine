{**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{$number_line = 3}
{foreach from=$group_cat_result item=group_cat name=group_cat_result}
	{assign var='id' value=$group_cat.id_labgroupcategory}
	{assign var='show_sub' value=$group_cat.show_sub}
	{assign var='use_slider' value=$group_cat.use_slider}
	{assign var='type_display' value=$group_cat.type_display}
{/foreach}
{$id_lang = Context::getContext()->language->id}
	<div class="labproductcategory_column labcolumn clearfix">
	<div class="row">
		{foreach from=$group_cat_info item=cat_info name=g_cat_info}
		<div class="column  lab-prod-cat-{$cat_info.id_cat|intval} col-xs-12 col-sm-4">
			<div class="cat-bar">
			  <div class="out-lab-prod">
				{if $cat_info.cat_icon!='' }
					<span class="icon_cat" style="background-color:{$cat_info.cat_color|escape:'html':'UTF-8'}">
					   <img src="{$icon_path|escape:'html':'UTF-8'}{$cat_info.cat_icon|escape:'html':'UTF-8'}" alt=""/>
					</span>
				{/if}
			    <p class="title-name"><a href="{$link->getCategoryLink($cat_info.id_cat, $cat_info.link_rewrite)|escape:'html':'UTF-8'}" title="{$cat_info.cat_name|escape:'html':'UTF-8'}">{$cat_info.cat_name|escape:'html':'UTF-8'}</a></p>
			  </div>
			</div>
			{if $cat_info.show_img == 1 && isset($cat_info.id_image) && $cat_info.id_image > 0}
			<div class="cat-img">
				<a href="{$link->getCategoryLink($cat_info.id_cat, $cat_info.link_rewrite)|escape:'html':'UTF-8'}" title="{$cat_info.cat_name|escape:'html':'UTF-8'}">
					<img src="{$link->getCatImageLink($cat_info.link_rewrite, $cat_info.id_image, 'medium_default')|escape:'html':'UTF-8'}"/>
				</a>
			</div>
			{/if}
			{if $show_sub}
			<div class="sub-cat">
				<ul class="sub-cat-ul">
					{foreach from = $cat_info.sub_cat item=sub_cat name=sub_cat_info}
						<li><a href="{$link->getCategoryLink($sub_cat.id_category, $sub_cat.link_rewrite)|escape:'html':'UTF-8'}" title="{$sub_cat.name|escape:'html':'UTF-8'}">{$sub_cat.name|escape:'html':'UTF-8'}</a></li>
					{/foreach}
					
				</ul>
			</div>
			{/if}
			{if $cat_info.cat_banner!='' }
			<div class="cat-banner">
				<a href="{$link->getCategoryLink($cat_info.id_cat, $cat_info.link_rewrite)|escape:'html':'UTF-8'}" title="{$cat_info.cat_name|escape:'html':'UTF-8'}">
					<img src="{$banner_path|escape:'html':'UTF-8'}{$cat_info.cat_banner|escape:'html':'UTF-8'}" alt=""/>
				</a>
			</div>
			{/if}
			<div class="product_list">
			<div class="row">
				<div class="owlProductCate-{$type_display|escape:'html':'UTF-8'}-{$cat_info.id_cat|intval}{$id}">
					{if isset($cat_info.product_list) && count($cat_info.product_list) > 0}
					{foreach from=$cat_info.product_list item=product name=product_list}
						{if $smarty.foreach.product_list.iteration % $number_line == 1 || $number_line == 1}
						<div class="item-inner ajax_block_product">
						{/if}



						<div class="item">
									<div class="product-container media-body">

										<div class="left-block pull-left">
											
												<a class="product_img_link" href="{$product.link|escape:'html':'UTF-8'}"
												   title="{$product.legend|escape:html:'UTF-8'}"><img
															src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}"
															alt="{$product.legend|escape:html:'UTF-8'}"/></a>
											
										</div>

										<div class="right-block media-body">
											<h5 class="product-name"><a href="{$product.link|escape:'html':'UTF-8'}" title="{$product.legend|escape:html:'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a></h5>

											
												{if $product.show_price}
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
														{if $product.has_discount}
															{hook h='displayProductPriceBlock' product=$product type="old_price"}

															<span class="regular-price">{$product.regular_price}</span>
															{if $product.discount_type === 'percentage'}
																<span class="discount-percentage">{$product.discount_percentage}</span>
															{/if}
														{/if}

														{hook h='displayProductPriceBlock' product=$product type="before_price"}

														<span itemprop="price" class="price">{$product.price}</span>

														{hook h='displayProductPriceBlock' product=$product type='unit_price'}

														{hook h='displayProductPriceBlock' product=$product type='weight'}
													</div>
												{/if}
											<!-- <form action="{$urls.pages.cart}" method="post">
												<input type="hidden" value="{$product.id_product}" name="id_product">
												<input  class="input-group form-control" name="qty" value="1" min="1"> 
												<button data-button-action="add-to-cart" class="btn btn-primary">
													{l s='Add to cart' d='Modules.labproductcategory.Admin'}
												</button>
											</form>
											-->
										</div>

									</div>
								</div>



						{if $smarty.foreach.product_list.iteration % $number_line == 0 || $smarty.foreach.product_list.last || $number_line == 1}
						</div>
						{/if}
					{/foreach}
				{else}
					<div class="item product-box ajax_block_product">
						<p class="alert alert-warning">{l s='No product at this time' d='Modules.labproductcategory.Admin'}</p>
					</div>
				{/if}
				</div>
				{if count($cat_info.manufacture)>0}
				<div class="manu-list">
					<ul>
						{foreach from=$cat_info.manufacture item=manu_item name=manufacture}
							<li><a href="#">{$manu_item->name|escape:'html':'UTF-8'}</a></li>
						{/foreach}
					</ul>
				</div>
				{/if}
			</div>
			</div>
			{if $use_slider == 1}
				<script type="text/javascript">
				$(document).ready(function() {
					var owl = $(".owlProductCate-{$type_display|escape:'html':'UTF-8'}-{$cat_info.id_cat|intval}{$id}");
					owl.owlCarousel({
						items : 1,
						itemsDesktop : [1199,1],
						itemsDesktopSmall : [991,1],
						itemsTablet: [767,2],
						itemsMobile : [480,1],
						autoHeight : true,
						navigation : true,
						navigationText : ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
						rewindNav : false,
						autoPlay :  false,
						stopOnHover: false,
						pagination : false,
					});
				});
				</script>
			{/if}
		</div>
		{/foreach}
	</div>
	</div>