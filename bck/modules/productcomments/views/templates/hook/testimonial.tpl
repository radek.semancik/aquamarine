<div class="block-testimonial">
    <h3 class="panel-heading">{l s='What they says about us' mod='productcomments'}</h3>
    <div class="panel-testimonial owl-carousel" data-carousel='{literal}{"items": 1, "loop": true, "center": false, "margin": 0, "autoWidth": false, "rtl": false, "autoHeight": false, "autoplay": true, "autoplayTimeout": 5000, "nav": false, "dots": true}{/literal}'>
          {if $product_comments}
            {foreach from=$product_comments item='product_comment'}
                <article class="panel-testimonial-item">
                    <a href="{$product_comment.link_product}"><img class="panel-testimonial-avatar" src="{$product_comment.link_image_product}" width="80px" height="80px" alt="" style="width:80px" /> </a>
                    <div class="panel-testimonial-rating" data-rating='{literal}{"score" : {/literal}{$product_comment.grade}{literal}, "readOnly": true}{/literal}'></div>
                    <p class="panel-testimonial-summary">{$product_comment.content nofilter} </p>
                    <h4 class="panel-testimonial-name">{$product_comment.customer_name}</h4>
                </article>
            {/foreach}
          {/if}
    </div>
</div>