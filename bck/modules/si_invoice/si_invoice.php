<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

class Si_Invoice extends Module
{

    public function __construct()
    {
        $this->name                   = 'si_invoice';
        $this->tab                    = 'front_office_features';
        $this->version                = '1.0.0';
        $this->author                 = 'BoostSpace';
        $this->need_instance          = 0;
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap              = true;

        parent::__construct();

        $this->displayName = $this->l('Invoice BoostSpace');
        $this->description = $this->l('Add Invoice BoostSpace parametr');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('displayInvoiceLegalFreeText')
        ) return false;

        return true;
    }

    public function hookDisplayInvoiceLegalFreeText($param)
    {

        $order = $param['order'];

        if ($order->id_customer) {
            $sql = 'SELECT payment_terms FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$order->id_customer;            
            return  Db::getInstance()->getValue($sql);
        }
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        unregisterHook('displayInvoice');
        return true;
    }
}