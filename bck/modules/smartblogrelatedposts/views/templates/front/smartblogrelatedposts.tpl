{if isset($posts) AND !empty($posts)}
<div id="articleRelated">
     <p class="panel-article-heading">{l s='Related articles ' mod='smartblogrelatedposts'}</p>
     <div class="row">
        {foreach from=$posts item="post"}
            {assign var="options" value=null}
            {$options.id_post= $post.id_smart_blog_post}
            {$options.slug= $post.link_rewrite}
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="blog_related_item">
                    <a class="panel-news-media" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">
                        <img itemprop="image" alt="{$post.meta_title}" src="{$modules_dir}/smartblog/images/{$post.id_smart_blog_post}-single-default.jpg" class="imageFeatured" />
                    </a>
                    <a class="panel-news-category" href="{$post.link_category}" title="{$post.category_name}">{$post.category_name}</a>
                    <h3 class="panel-news-title">
                       <a class="title panel-news-link" title="{$post.meta_title}" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">{$post.meta_title}</a>
                    </h3>
                    
                    <p class="panel-news-summary">
                        {$post.short_description}
                    </p> 
                    <ul class="panel-info-post">
                        <li>{$post.created|date_format:"%b %d, %Y"}</li>
                    </ul>
                </div>
            </div>
        {/foreach}
    </div>
</div>
{/if}