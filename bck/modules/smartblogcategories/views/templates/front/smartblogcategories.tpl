{if isset($categories) AND !empty($categories)}
<div id="category_blog_block_left"  class="panel-sidebar panel-sidebar-categories">
  <div class="panel-sidebar-heading">
      <h3 class='panel-sidebar-title'>
        <a href="{smartblog::GetSmartBlogLink('smartblog_list')}">
            {l s='Blog Categories' mod='smartblogcategories'}
            <div class="navbar-toggler collapse-icons" data-toggle="collapse" data-target="#category_blog_block_left_content">
              <i class="material-icons add">&#xE145;</i>
              <i class="material-icons remove">&#xE15B;</i>
            </div>
        </a>
      </h3>
      <a href="#" class="panel-sidebar-toggle"></a>
  </div>
   <div id="category_blog_block_left_content" class="block_content list-block panel-sidebar-inner">
         <ul class="nav-sidebar">
        	{foreach from=$categories item="category"}
                {assign var="options" value=null}
                {$options.id_category = $category.id_smart_blog_category}
                {$options.slug = $category.link_rewrite}
                <li>
                    <a href="{smartblog::GetSmartBlogLink('smartblog_category',$options)}">{$category.meta_title} <span>({$category.count})</span></a>
                </li>
        	{/foreach}
        </ul>
   </div>
</div>
{/if}