<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2016 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class SampleDataHtml
{
	public function initData($base_url)
	{
		$content_block1 = '
			<div class="laberStatic laberthemes  ">
			<div class="row ">
			<div class="col-md-4">
			<div class="img"><a title="" href="#"> <img src="{static_block_url}img/cms/img_1_1.png" alt="images" /> </a></div>
			</div>
			<div class="col-md-4">
			<div class="img"><a title="" href="#"> <img src="{static_block_url}img/cms/img_1_2.png" alt="images" /> </a></div>
			</div>
			<div class="col-md-4">
			<div class="img"><a title="" href="#"> <img src="{static_block_url}img/cms/img_1_3.png" alt="images" /> </a></div>
			</div>
			</div>
			</div>
		';
								
		$content_block2 = '
			<div class="laberStatic">
			<div class="row ">
			<div class="col-md-6">
			<div class="img"><a title="" href="#"> <img src="{static_block_url}img/cms/img_4_4.png" alt="images" /> </a></div>
			</div>
			<div class="col-md-6">
			<div class="img"><a title="" href="#"> <img src="{static_block_url}img/cms/img_4_5.png" alt="images" /> </a></div>
			</div>
			</div>
			</div>
		';
		
		$content_block3 = '
			<div class="laber-contact col-md-6 ">
			<div class="logo-footer"><img src="{static_block_url}img/cms/logo-footer.png" />
			<p>Outstock is a premium Templates theme with advanced admin module. It’s extremely customizable, easy to use and fully responsive and retina ready.</p>
			</div>
			<div class="contactus">
			<p><i class="icon pe-7s-map-marker"></i> Add: 1234 Heaven Stress, Beverly Hill, Melbourne, USA.</p>
			<p><i class="icon pe-7s-mail-open-file"></i> Email: Contact@erentheme.com</p>
			<p><i class="icon pe-7s-call"></i>Phone Number: (800) 123 456 789</p>
			</div>
			</div>
		';
		$content_block4 = '
			<div class="copyright col-md-6">© 2017 <a href="http://laberthemes.com/">Laberthemes. </a>All Rights Reserved</div>
		';
		$content_block5 = '
			<div class="payment col-md-6"><a href="#"><i class="fa fa-cc-mastercard"></i></a> <a href="#"><i class="fa fa-cc-visa"></i></a> <a href="#"><i class="fa fa-cc-paypal"></i></a> <a href="#"><i class="fa fa-cc-discover"></i></a> <a href="#"><i class="fa fa-credit-card"></i></a> <a href="#"><i class="fa fa-cc-amex"></i></a></div>
		';
		
		$displayNav = Hook::getIdByName('displayNav');
		$displayNav1 = Hook::getIdByName('displayNav1');
		$displayNav2 = Hook::getIdByName('displayNav2');
		$displayTop = Hook::getIdByName('displayTop');
		$displayMegamenu = Hook::getIdByName('displayMegamenu');
		$displayImageSlider = Hook::getIdByName('displayImageSlider');
		$displayLeftColumn = Hook::getIdByName('displayLeftColumn');
		$displayRightColumn = Hook::getIdByName('displayRightColumn');
		$displayHome = Hook::getIdByName('displayHome');
		$displayPosition1 = Hook::getIdByName('displayPosition1');
		$displayPosition2 = Hook::getIdByName('displayPosition2');
		$displayPosition3 = Hook::getIdByName('displayPosition3');
		$displayPosition4 = Hook::getIdByName('displayPosition4');
		$displayPosition4 = Hook::getIdByName('displayPosition5');
		$displayPosition4 = Hook::getIdByName('displayPosition6');
		$displayFooterBefore = Hook::getIdByName('displayFooterBefore');
		$displayFooter = Hook::getIdByName('displayFooter');
		$displayFooterAfter = Hook::getIdByName('displayFooterAfter');
		
		
		$id_shop = Configuration::get('PS_SHOP_DEFAULT');
		
		/*install static Block*/
		$result = true;
		$result &= Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'labcustomhtml` (`id_labcustomhtml`, `hook`, `active`) 
			VALUES
			(1, "displayPosition1", 1),
			(2, "displayPosition2", 1),
			(3, "displayFooter", 1),
			(4, "displayFooterAfter", 1),
			(5, "displayFooterAfter", 1)
			
			;'); 
		
		$result &= Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'labcustomhtml_shop` (`id_labcustomhtml`, `id_shop`,`active`) 
			VALUES 
			(1,'.$id_shop.', 1),
			(2,'.$id_shop.', 1),
			(3,'.$id_shop.', 1),
			(4,'.$id_shop.', 1),
			(5,'.$id_shop.', 1)
			
			
			;');
		
		foreach (Language::getLanguages(false) as $lang)
		{
			$result &= Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'labcustomhtml_lang` (`id_labcustomhtml`, `id_shop`, `id_lang`, `title`, `content`) 
			VALUES 
			("1", "'.$id_shop.'","'.$lang['id_lang'].'","banner displayposition1", \''.$content_block1.'\'),
			("2", "'.$id_shop.'","'.$lang['id_lang'].'","banner displayposition2", \''.$content_block2.'\'),
			("3", "'.$id_shop.'","'.$lang['id_lang'].'","contac footer", \''.$content_block3.'\'),
			("4", "'.$id_shop.'","'.$lang['id_lang'].'","copyright", \''.$content_block4.'\'),
			("5", "'.$id_shop.'","'.$lang['id_lang'].'","paypal", \''.$content_block5.'\')
			
			;');
		}
		return $result;
	}
}