<?php
error_reporting('E_ALL');
ini_set('display_errors', '1');

$idShop       = 1;
$idLang       = 1;
$rootCategory = 2;

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/function.php');

//TODO boostspace
//Primarni kontakt a primarni hodnoty telefon, email apod.
//Datum poslední změny

$xml_url = "https://app.ipkarting.com/api/subject/presta";
$xml     = simplexml_load_file($xml_url);
$result  = false;
if (isset($xml->CUSTOMER)) {
    foreach ($xml->CUSTOMER as $item) {

        if (empty($item->ID_CUSTOMER)) {
            echo "Company: ".$item->COMPANY." - User is empty.<br />";
            continue;
        }

        if (!filter_var($item->EMAIL, FILTER_VALIDATE_EMAIL)) {
            echo "Company: ".$item->COMPANY." - Email address '$item->EMAIL' is considered invalid.<br />";
            continue;
        }

        if (empty($item->NAME) || empty($item->SURNAME)) {
            echo "Company: ".$item->COMPANY." - User name is empty.<br />";
            continue;
        }

        $sql          = 'SELECT count(*) as count_customer FROM '._DB_PREFIX_.'customer WHERE real_user_id ="'.(string) $item->ID_CUSTOMER.'"';
        $countCutomer = Db::getInstance()->getRow($sql);

        //Import xml Category
        $categoryArr                     = array();
        $categoryArr['real_user_id']     = (string) $item->ID_CUSTOMER;
        $categoryArr['firstname']        = (string) mres($item->NAME);
        $categoryArr['lastname']         = (string) mres($item->SURNAME);
        $categoryArr['email']            = (string) $item->EMAIL;
        $categoryArr['company']          = (string) mres($item->COMPANY);
        $categoryArr['active']           = 1;
        $categoryArr['last_passwd_gen']  = date('Y-m-d H:i:s', strtotime(' -5 day'));
        $categoryArr['id_default_group'] = $item->ID_GROUP;
        $categoryArr['payment_terms']    = $item->PAYMENT_CONDITION;
        $categoryArr['view_feature']     = $item->CUSTOMER_VIEW_FEATURE;



        if ($countCutomer["count_customer"] == 0) {
            $categoryArr['date_add']   = date("Y-m-d H:i:s");
            $categoryArr['passwd']     = (string) randomPassword();
            $categoryArr['secure_key'] = md5(randomPassword().date("Y-m-d H:i:s"));
            $insertCustomer            = Db::getInstance()->insert('customer', $categoryArr);
            $customerId                = DB::getInstance()->Insert_ID();
            if ($insertCustomer) {
                $result .= 'Import proveden - '.$item->NAME.'<br />';

                $customerGroup                   = array();
                $customerGroup[0]['id_group']    = 3;
                $customerGroup[0]['id_customer'] = (string) $customerId;

                if ($item->ID_GROUP) {
                    $customerGroup[1]['id_group']    = (string) $item->ID_GROUP;
                    $customerGroup[1]['id_customer'] = (string) $customerId;
                }

                foreach ($customerGroup as $value) {
                    $insertCustomerGroup = Db::getInstance()->insert('customer_group', $value);
                }
            }
        } else {
            $categoryArr['date_upd'] = date("Y-m-d H:i:s");
            $insertCustomer          = Db::getInstance()->update('customer', $categoryArr, 'real_user_id = '.(string) $item->ID_CUSTOMER);

            $sql        = 'SELECT id_customer FROM '._DB_PREFIX_.'customer WHERE real_user_id ="'.(string) $item->ID_CUSTOMER.'"';
            $customer   = Db::getInstance()->getRow($sql);
            $customerId = $customer['id_customer'];

            if ($insertCustomer) {

                if (Db::getInstance()->Affected_Rows()) {
                    $result .= 'Update proveden - '.$item->NAME.'<br />';
                }
            }

            Db::getInstance()->delete('customer_group', 'id_customer = '.$customerId);
            $customerGroup                   = array();
            $customerGroup[0]['id_group']    = 3;
            $customerGroup[0]['id_customer'] = (string) $customerId;

            if ($item->ID_GROUP) {
                $customerGroup[1]['id_group']    = (string) $item->ID_GROUP;
                $customerGroup[1]['id_customer'] = (string) $customerId;
            }

            foreach ($customerGroup as $value) {
                $insertCustomerGroup = Db::getInstance()->insert('customer_group', $value);
            }



            if ($customergroupUpdate) {

                if (Db::getInstance()->Affected_Rows()) {
                    $customergroupUpdate .= 'Změna skupiny provedena - '.$item->NAME.'<br />';
                }
            }
        }





//         foreach ($item->ADDRESS as $address) {
// 
//             if (empty($address->CITY)) {
//                 echo "Company: ".$item->COMPANY." - Address city is empty.<br />";
//                 continue;
//             }
// 
//             if (empty($address->COMPANY)) {
//                 echo "Company: ".$item->COMPANY." - Address company is empty.<br />";
//                 continue;
//             }
// 
//             if (empty($address->ADDRESS)) {
//                 echo "Company: ".$item->COMPANY." - Address is empty.<br />";
//                 continue;
//             }
// 
//             if (empty($address->POST_CODE)) {
//                 echo "Company: ".$item->COMPANY." - Post code is empty.<br />";
//                 continue;
//             }
// 
//             if (empty($address->COUNTRY)) {
//                 echo "Company: ".$item->COMPANY." - Country is empty.<br />";
//                 continue;
//             }
// 
//             $sql          = 'SELECT count(*) as count_address FROM '._DB_PREFIX_.'address WHERE real_id ="'.(string) $address->ID.'" and deleted = 0';
//             $countAddress = Db::getInstance()->getRow($sql);
// 
// 
//             $sql       = 'SELECT id_country FROM '._DB_PREFIX_.'country WHERE iso_code = "'.(string) $address->COUNTRY.'" ';
//             $country   = Db::getInstance()->getRow($sql);
//             $countryId = $country['id_country'];
// 
// 
// 
//             $sql     = 'SELECT id_state FROM '._DB_PREFIX_.'state WHERE iso_code = "'.(string) $address->STATE.'" and id_country = '.$countryId;
//             $state   = Db::getInstance()->getRow($sql);
//             $stateId = $state['id_state'];
// 
// 
//             $addressArr                = array();
//             $addressArr['alias']       = (string) $address->ALIAS;
//             $addressArr['company']     = (string) $address->COMPANY;
//             $addressArr['address1']    = (string) $address->ADDRESS;
//             $addressArr['postcode']    = (string) $address->POST_CODE;
//             $addressArr['city']        = (string) $address->CITY;
//             $addressArr['firstname']   = (string) $item->NAME;
//             $addressArr['lastname']    = (string) $item->SURNAME;
//             $addressArr['id_customer'] = $customerId;
//             $addressArr['real_id']     = $address->ID;
//             $addressArr['id_country']  = $countryId;
// 
//             if (!empty($stateId)) {
//                 $addressArr['id_state'] = $stateId;
//             }
// 
//             if ($countAddress["count_address"] == 0) {
//                 $addressArr['date_add'] = date("Y-m-d H:i:s");
//                 Db::getInstance()->insert('address', $addressArr);
//             } else {
//                 //                Db::getInstance()->update('address', $addressArr, 'real_id = '.(string) $address->ID);
//                 //                if (Db::getInstance()->Affected_Rows()) {
//                 //                    $categoryArr['date_upd'] = date("Y-m-d H:i:s");
//                 //                    Db::getInstance()->update('customer', $addressArr, 'real_id = '.(string) $address->ID);
//                 //                }
//             }
//         }
    }
}



if (empty($result)) {
    echo "Žádný nový zákazník";
} else {
    echo $result;
}

Tools::clearSmartyCache();
