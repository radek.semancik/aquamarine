<?php

error_reporting('E_ALL');
ini_set('display_errors', '1');
ini_set('memory_limit', '-1');

$out_of_stock = 2; //Chování, pokud není skladem
$id_shop = 1;
$active = 1; // Default 0
$limitImport = 0;
$featureFlavor = 8;
$catHome = 2;

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/../../classes/shop/Shop.php');
require_once(dirname(__FILE__).'/../../classes/Tools.php');
require_once(dirname(__FILE__).'/../../controllers/admin/AdminImportController.php');
require_once(dirname(__FILE__).'/../../override/controllers/admin/AdminImportController.php');
require_once(dirname(__FILE__).'/function.php');


$gift = Configuration::get('HOME_GIFTED_CAT');
$top = Configuration::get('HOME_TOP_CAT');
$recommend = Configuration::get('HOME_FEATURED_CAT');

$xmlDir = array();
//$xmlDir[] = "xml/product.xml";
$xmlDir[] = "https://app.ipkarting.com/api/product/presta";
//$xmlDir[] = "https://ipktest.boost.space/api/product/presta";

foreach ($xmlDir as $xml_url) {
    $xml = simplexml_load_file($xml_url);
    if (isset($xml->SHOPITEM)) {
        $importProduct = loadXmlCategory($xml, $limitImport, $catHome, $gift, $top, $recommend);
        if ($importProduct) {
            echo $importProduct;
        } else {
            echo "Žádná nová kategorie k přiřazení";
        }
    }
}

function loadXmlCategory($xml, $limitImport, $catHome, $gift, $top, $recommend)
{
    $result = false;
    $repeatImport = 1;
    if (isset($xml->SHOPITEM)) {
        foreach ($xml->SHOPITEM as $item) {
            if ($repeatImport <= $limitImport || $limitImport == 0) {
               
                $sql = 'SELECT id_product FROM '._DB_PREFIX_.'product WHERE product_no ="'.$item->ITEM_ID.'"';
                $productNo = $item->ITEM_ID;
                if (!empty($item->ITEMGROUP_ID)) {
                    $productNo = $item->ITEMGROUP_ID;
                }

                $sql = 'SELECT id_product FROM '._DB_PREFIX_.'product WHERE product_no ="'.$productNo.'"';
                $productData = Db::getInstance()->getRow($sql);

                $insertProductId = $productData["id_product"];
                //echo $insertProductId;die();
                if ($insertProductId) {

                    //Delete Category
                    Db::getInstance()->delete('category_product', "id_product = ".$insertProductId);

                    //Dárek
                    if ($item->GIFT == 1) {
                        //Insert Gift                        
                        Db::getInstance()->insert('category_product', array("id_category" => $gift, "id_product" => $insertProductId));
                    } else {
                        //Produkty
                        //Insert Default category
                        $insertProductCat = Db::getInstance()->insert('category_product', array("id_category" => $catHome, "id_product" => $insertProductId));

                        if ($item->RECOMMENDED_PRODUCT == 1) {
                            //Insert Recommended
                            Db::getInstance()->insert('category_product', array("id_category" => $recommend, "id_product" => $insertProductId));
                        }

                        if ($item->TOP_PRODUCT == 1) {
                            //Insert Recommended
                            Db::getInstance()->insert('category_product', array("id_category" => $top, "id_product" => $insertProductId));
                        }

                        //Inset Category Product                        
                        foreach ($item->CATEGORIES->CATEGORY as $category) {                         
                            $insertProductCat = Db::getInstance()->insert('category_product', array("id_category" => getIdCategory($category), "id_product" => $insertProductId));
                            if ($insertProductCat) {
                                $result .= 'Produkt '.$item->PRODUCT_NAME.' (id: '.$insertProductId.') přidán do kategorie '.$category.'<br />';
                            } else {
                                Db::getInstance()->insert('ps_import_product_log', array("message" => "ERR6 - Nepodařilo se přiradit kategorii k produktu - ".$item->PRODUCT_NAME." id_category: ".$category, "ean" => $item->EAN, "state" => "warning", "product_id" => $insertProductId));
                            }
                        }                       
                    }
                }
            }

            $repeatImport++;
        }
    }

    return $result;
}
