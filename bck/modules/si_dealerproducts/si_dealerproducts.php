<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Core\Product\ProductExtraContentFinder;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Addon\Module\ModuleManagerBuilder;
use PrestaShop\PrestaShop\Adapter\ObjectPresenter;

class Si_Dealerproducts extends Module
{

    public function __construct()
    {
        $this->name                   = 'si_dealerproducts';
        $this->tab                    = 'front_office_features';
        $this->version                = '1.0.0';
        $this->author                 = 'BoostSpace';
        $this->need_instance          = 0;
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap              = true;

        parent::__construct();

        $this->displayName = $this->l('Dealer product BoostSpace');
        $this->description = $this->l('Filtr dealers product');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('displayProductExtraContent')
        ) return false;

        return true;
    }

    public function hookDisplayProductExtraContent($param)
    {
        ini_set("display_errors", 1);
        error_reporting(E_ERROR | E_WARNING);
        //tea_themeconfig

        $productSettings       = $this->getProductPresentationSettings();        
        $this->objectPresenter = new ObjectPresenter();

        $obj_product = $param['product'];

        $allowProduct = array(3, 4);

        if (in_array($obj_product->id,$allowProduct)) {
            
            $product                         = $this->objectPresenter->present($obj_product);
            $product['id_product']           = (int) $obj_product->id;
            $product['out_of_stock']         = (int) $obj_product->out_of_stock;
            $product['new']                  = (int) $obj_product->new;

             $presenter = $this->getProductPresenter();

          return $presenter->present(
                    $productSettings, $product, $this->context->language
            );
                                        
        }

        return false;
        
       
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        unregisterHook('displayProductExtraContent');
        return true;
    }

    private function getFactory()
    {
        return new ProductPresenterFactory($this->context, new TaxConfiguration());
    }

    protected function getProductPresentationSettings()
    {
        return $this->getFactory()->getPresentationSettings();
    }

    protected function getProductPresenter()
    {
        return $this->getFactory()->getPresenter();
    }

       private function getIdProductAttribute($obj_product)
    {
        $requestedIdProductAttribute = (int)Tools::getValue('id_product_attribute');

        if (!Configuration::get('PS_DISP_UNAVAILABLE_ATTR')) {
            $productAttributes = array_filter(
                $obj_product->getAttributeCombinations(),
                function ($elem) {
                    return $elem['quantity'] > 0;
                });
            $productAttribute = array_filter(
                $productAttributes,
                function ($elem) use ($requestedIdProductAttribute) {
                    return $elem['id_product_attribute'] == $requestedIdProductAttribute;
                });
            if (empty($productAttribute) && !empty($productAttributes)) {
                return (int)array_shift($productAttributes)['id_product_attribute'];
            }
        }
        return $requestedIdProductAttribute;
    }

     protected function getProductMinimalQuantity($product,$obj_product)
    {
        $minimal_quantity = 1;

        if ($product['id_product_attribute']) {
            $combination = $this->findProductCombinationById($product['id_product_attribute'],$obj_product);
            if ($combination['minimal_quantity']) {
                $minimal_quantity = $combination['minimal_quantity'];
            }
        } else {
            $minimal_quantity = $obj_product->minimal_quantity;
        }

        return $minimal_quantity;
    }

       protected function getRequiredQuantity($product,$obj_product)
    {
        $requiredQuantity = (int) Tools::getValue('quantity_wanted', $this->getProductMinimalQuantity($product,$obj_product));
        if ($requiredQuantity < $product['minimal_quantity']) {
            $requiredQuantity = $product['minimal_quantity'];
        }

        return $requiredQuantity;
    }
}