var _0xaae8="";
! function(e) {
    "use strict";
    var t = e("body"),
       
        s = e("#owl-large"),
        l = e("#owl-thumbnail"),
        f = function() {
            var t = e("[data-countdown]"),
                n = '<div class="countdown-item"><div class="countdown-inner"><div class="countdown-cover"><div class="countdown-table"><div class="countdown-cell"><div class="countdown-time">%-D</div><div class="countdown-text">Day%!D</div></div></div></div></div></div><div class="countdown-item"><div class="countdown-inner"><div class="countdown-cover"><div class="countdown-table"><div class="countdown-cell"><span class="countdown-time">%H</span><div class="countdown-text">HR%!H</div></div></div></div></div></div><div class="countdown-item"><div class="countdown-inner"><div class="countdown-cover"><div class="countdown-table"><div class="countdown-cell"><span class="countdown-time">%M</span><div class="countdown-text">Min%!M</div></div></div></div></div></div><div class="countdown-item"><div class="countdown-inner"><div class="countdown-cover"><div class="countdown-table"><div class="countdown-cell"><span class="countdown-time">%S</span><div class="countdown-text">Sec%!S</div></div></div></div></div></div>';
            t.length > 0 && t.each(function() {
                var t = e(this).data("countdown");
                e(this).countdown(t).on("update.countdown", function(t) {
                    e(this).html(t.strftime(n))
                })
            })
        },
        B = function() {
            var t = !1,
                n = 300;
            s.length > 0 && s.owlCarousel({
                items: 1,
                loop: !1,
                center: !1,
                margin: 0,
                autoWidth: !1,
                rtl: !1,
                responsive: {},
                responsiveBaseElement: window,
                lazyLoad: !1,
                autoHeight: !1,
                autoplay: !1,
                autoplayTimeout: 5e3,
                autoplayHoverPause: !1,
                nav: !0,
                navText: "",
                navElement: "button",
                navClass: ["owl-prev fa fa-angle-left", "owl-next fa fa-angle-right"],
                dots: !1
            }).on("changed.owl.carousel", function(e) {
                t || (t = !0, l.trigger("to.owl.carousel", [e.item.index, n, !0]), t = !1)
            }), l.length > 0 && l.owlCarousel({
                items: 4,
                loop: !1,
                center: !1,
                margin: 20,
                autoWidth: !1,
                rtl: !1,
                responsive: {},
                responsiveBaseElement: window,
                lazyLoad: !1,
                autoHeight: !1,
                autoplay: !1,
                autoplayTimeout: 5e3,
                autoplayHoverPause: !1,
                nav: !0,
                navText: "",
                navElement: "button",
                navClass: ["owl-prev fa fa-angle-left", "owl-next fa fa-angle-right"],
                dots: !1
            }).on("click", ".owl-item", function() {
                s.trigger("to.owl.carousel", [e(this).index(), n, !0])
            }).on("changed.owl.carousel", function(e) {
                t || (t = !0, s.trigger("to.owl.carousel", [e.item.index, n, !0]), t = !1)
            })
        },
        g = function() {
            var t = e("[data-rating]"),
                n = {
                    half: !0,
                    number: 5,
                    space: !1,
                    starType: "i"
                };
            t.length > 0 && t.each(function() {
                var t = e(this).data("rating"),
                    a = e.extend({}, n, t);
                e(this).raty(a)
            })
        },
        P = function() {
            var t = e("[data-carousel]"),
                n = {
                    items: 3,
                    loop: !1,
                    center: !1,
                    margin: 20,
                    autoWidth: !1,
                    rtl: !1,
                    responsive: {},
                    responsiveBaseElement: window,
                    lazyLoad: !1,
                    autoHeight: !1,
                    autoplay: !1,
                    autoplayTimeout: 5e3,
                    autoplayHoverPause: !1,
                    nav: !1,
                    navText: "",
                    navElement: "button",
                    navClass: ["owl-prev fa fa-angle-left", "owl-next fa fa-angle-right"],
                    dots: !0
                };
            t.length > 0 && t.each(function() {
                var t = e(this).data("carousel"),
                    a = e.extend({}, n, t),
                    i = e("." + e(this).data("scope")),
                    o = i.find("." + e(this).data("prev")),
                    s = i.find("." + e(this).data("next")),
                    l = e(this).owlCarousel(a);
                s.click(function() {
                    l.trigger("next.owl.carousel")
                }), o.click(function() {
                    l.trigger("prev.owl.carousel")
                })
            })
        };
    e(document).ready(function() {
        B(), P(),f(),g()
    })
}(jQuery);