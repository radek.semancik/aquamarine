{if $products}
    <div class="panel-special-heading">
      <p class="panel-special-summary">{l s='SEASON DISCOUNT' mod='tea_blockspecials'}</p>
      <h2 class="panel-heading">{l s='Special Deals' mod='tea_blockspecials'}</h2>
    </div>
    <div class="row flex">
      <div class="col-md-3 col-xs-12">
        <div class="panel-offer owl-carousel" data-mh="special" data-carousel='{literal}{"items": 1, "loop": true, "center": false, "margin": 30, "autoWidth": false, "rtl": false, "autoHeight": false, "autoplay": true, "autoplayTimeout": 5000, "nav": false, "dots": true, "responsive": {"0" : {"items": 1, "margin": 0, "loop": true}, "479" : {"items": 2, "margin": 15, "loop": true}, "768" : {"items": 2, "loop": true}, "992" : {"items": 1, "margin": 0, "loop": true}}}{/literal}'>
          {foreach from=$products item='product'}
              <div class="product-item">
                    <div class="product-item-thumbnail">
                          <span class="product-item-label product-item-green">{l s='offter'}</span>
                          <article class="product-miniature js-product-miniature" itemtype="http://schema.org/Product" itemscope="" data-id-product-attribute="{$product.id_product_attribute}" data-id-product="{$product.id_product}">
                              <a class="thumbnail product-thumbnail" href="{$product.link}">
                                <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" width="270" height="340" alt="{$product.name}"/>
                              </a>
                              <a href="#" class="quick-view product-item-view" data-link-action="quickview">
                                {l s='Quick view' d='Shop.Theme.Actions'}
                              </a>
                          </article>
                    </div>
                    <h4 class="product-item-category">
                        <a href="{$link->getCategoryLink($product.id_category_default)}">
                            {$product.category_name}
                        </a>
                    </h4>
                    <h3 class="product-item-name"><a href="{$product.link}">{$product.name}</a></h3>
                    <div class="product-item-review" data-rating='{literal}{"score" : 3.2, "readOnly": true}{/literal}'></div>
                    <p class="product-item-price">
                        {hook h="displayProductPriceBlock" product=$product type="old_price"}
                        {$product.price}
                        <span class="product-price-old">{$product.regular_price}</span></p>
                    <div class="product-item-control">
                      <div class="product-item-action">
                        <form action="{$urls.pages.cart}" method="post">
                            <input type="hidden" name="token" value="{$static_token}" />
                            <input type="hidden" value="{$product.id_product}" name="id_product" />
                            <button data-button-action="add-to-cart" class="product-item-cart">
                                {l s='Add to cart' d='Shop.Theme.Actions'}
                            </button>
                        </form>
                      </div>
                    </div>
              </div>
          {/foreach}
        </div><!--/.panel-offer-->
      </div>
      <div class="col-md-9 col-xs-12">
        <div class="panel-discount sang owl-carousel" data-mh="special" data-carousel='{literal}{"items": 1, "loop": true, "center": false, "margin": 0, "autoWidth": false, "rtl": false, "autoHeight": false, "autoplay": true, "autoplayTimeout": 5000, "nav": true, "dots": false}{/literal}'>
          {foreach from=$products2 item='product'}
              <article class="panel-discount-item">
                <div class="row">
                  <div class=" col-xs-12 col-ms-6 col-sm-6">
                    <div class="panel-discount-thumbnail">
                      <span class="sale-off">{if $product.discount_type!='amount'}{$product.discount_percentage}{else}-{$product.discount_amount}{/if}</span>
                      <a href="{$product.link}" title="{$product.name}">   
                        <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" width="370" height="466" alt="{$product.name}"/>
                      </a>
                    </div>
                  </div>
                  
                  <div class=" col-xs-12 col-ms-6 col-sm-6">
                    <div class="panel-discount-content">
                      <h4 class="panel-discount-category">
                        <a href="{$link->getCategoryLink($product.id_category_default)}" title="{$product.category_name}">{$product.category_name}</a>
                      </h4>
                      <h3 class="panel-discount-name">
                        <a href="{$product.link}" title="{$product.name}">{$product.name}</a>
                      </h3>
                      <div class="panel-discount-review" data-rating='{literal}{"score" : 1.2, "readOnly": true}{/literal}'></div>
                      <div class="panel-discount-summary">
                        {$product.description_short nofilter}</div>
                      <p class="panel-discount-price">
                          {hook h="displayProductPriceBlock" product=$product type="old_price"}
                          {$product.price}
                          <span>{$product.regular_price}</span>
                      </p>
                      {if $product.specific_prices.to!='0000-00-00 00:00:00'}
                      <div class="panel-discount-countdown" data-countdown="{$product.specific_prices.to}"></div>
                      {/if}
                    </div>
                  </div>
                </div>
              </article>
          {/foreach}
        </div>
      </div>
    </div>
{/if}