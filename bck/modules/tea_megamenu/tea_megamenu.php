<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*	Developer tunghv
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/	

/**
 * @since   1.5.0
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'tea_megamenu/Megamenu.php');
include_once(_PS_MODULE_DIR_.'tea_megamenu/MegamenuGroup.php');
include_once(_PS_MODULE_DIR_.'tea_megamenu/install/export.php');
include_once(_PS_MODULE_DIR_.'tea_megamenu/install/import.php');
class tea_megamenu extends Module
{
	protected $_html = '';
    protected $_errors='';
	protected $teamegamenu_style = 'mega';
    private $default_hook = array();	
	public function __construct()
	{
		$this->name = 'tea_megamenu';
		$this->tab = 'front_office_features';
		$this->version = '1.1.0';
		$this->author = 'prestagold.com';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		$this->bootstrap = true;

		parent::__construct();
        $this->default_hook = array(
            'displayMenu'=>$this->l('Menu left'),
            'displayMenu2'=>$this->l('Menu right'),
            'displayTop'=>$this->l('Menu top'),
            'displayMenu3' => $this->l('Menu basic')
        );
        $this->displayName = $this->l('Prestagold megamenu');
        $this->description = $this->l('Adds a new horizontal mega menu to the top of your e-commerce website.');
		$this->ps_versions_compliancy = array('min' => '1.6.0.4', 'max' => _PS_VERSION_);
	}

	/**
	 * @see Module::install()
	 */
	public function install()
	{
		/* Adds Module */
        if (parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('actionObjectCategoryUpdateAfter') &&
            $this->registerHook('actionObjectCategoryDeleteAfter') &&
            $this->registerHook('actionObjectCategoryAddAfter') &&
            $this->registerHook('actionObjectCmsUpdateAfter') &&
            $this->registerHook('actionObjectCmsDeleteAfter') &&
            $this->registerHook('actionObjectCmsAddAfter') &&
            $this->registerHook('actionObjectSupplierUpdateAfter') &&
            $this->registerHook('actionObjectSupplierDeleteAfter') &&
            $this->registerHook('actionObjectSupplierAddAfter') &&
            $this->registerHook('actionObjectManufacturerUpdateAfter') &&
            $this->registerHook('actionObjectManufacturerDeleteAfter') &&
            $this->registerHook('actionObjectManufacturerAddAfter') &&
            $this->registerHook('actionObjectProductUpdateAfter') &&
            $this->registerHook('actionObjectProductDeleteAfter') &&
            $this->registerHook('actionObjectProductAddAfter') &&
            $this->registerHook('categoryUpdate') &&
            $this->registerHook('actionShopDataDuplication'))
		{
			foreach ($this->default_hook as $key=> $hook)
			{
				if (!$this->registerHook ($key))
					return false;
			}
			$shops = Shop::getContextListShopID();
			$shop_groups_list = array();

			/* Setup each shop */
			foreach ($shops as $shop_id)
			{
				$shop_group_id = (int)Shop::getGroupFromShop($shop_id, true);

				if (!in_array($shop_group_id, $shop_groups_list))
					$shop_groups_list[] = $shop_group_id;

				/* Sets up configuration */
				$res = Configuration::updateValue('teamegamenu_style', $this->teamegamenu_style, false, $shop_group_id, $shop_id);
			}

			/* Sets up Shop Group configuration */
			if (count($shop_groups_list))
			{
				foreach ($shop_groups_list as $shop_group_id)
				{
					$res = Configuration::updateValue('teamegamenu_style', $this->teamegamenu_style, false, $shop_group_id);
				}
			}

			/* Sets up Global configuration */
			$res = Configuration::updateValue('teamegamenu_style', $this->teamegamenu_style);	
			/* Creates tables */
			$res &= $this->createTables();
			/* Adds samples */
            if ($res)
			{
                $import = new TeaImport();
                $import->importMenu();
			}	
			return (bool)$res;
		}

		return false;
	}
	/**
	 * @see Module::uninstall()
	 */
	public function uninstall()
	{
		/* Deletes Module */
		if (parent::uninstall())
		{
			/* Deletes tables */
			$res = $this->deleteTables();
			$res &= Configuration::deleteByName('teamegamenu_style');
			return (bool)$res;
		}
		return false;
	}

	/**
	 * Creates tables
	 */
	protected function createTables()
	{
		/* teamegamenu */
		$res = Db::getInstance ()->Execute ('DROP TABLE IF EXISTS `'._DB_PREFIX_.'teamegamenu_group`')
			&& Db::getInstance ()->Execute ('CREATE TABLE `'._DB_PREFIX_.'teamegamenu_group` (
			`id_teamegamenu_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`hook` varchar(20) NOT NULL, 
			`params` text NOT NULL DEFAULT \'\' ,
			`status` tinyint(1) NOT NULL DEFAULT \'1\',
			`position` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id_teamegamenu_group`)) ENGINE=InnoDB default CHARSET=utf8');
		$res &= Db::getInstance ()->Execute ('DROP TABLE IF EXISTS `'._DB_PREFIX_.'teamegamenu_group_shop`')
			&& Db::getInstance ()->Execute ('CREATE TABLE `'._DB_PREFIX_.'teamegamenu_group_shop` (
			`id_teamegamenu_group` int(10) unsigned NOT NULL,
			`id_shop` int(10) unsigned NOT NULL, 
			PRIMARY KEY (`id_teamegamenu_group`,`id_shop`)) ENGINE=InnoDB default CHARSET=utf8');
		$res &= Db::getInstance ()->Execute ('DROP TABLE IF EXISTS `'._DB_PREFIX_.'teamegamenu_group_lang`')
			&& Db::getInstance ()->Execute ('CREATE TABLE '._DB_PREFIX_.'teamegamenu_group_lang (
			`id_teamegamenu_group` int(10) unsigned NOT NULL,
			`id_lang` int(10) unsigned NOT NULL,
			`title` varchar(255) NOT NULL DEFAULT \'\',
			PRIMARY KEY (`id_teamegamenu_group`,`id_lang`)) ENGINE=InnoDB default CHARSET=utf8');
			
		$res &= (bool)Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'teamegamenu` (
				`id_teamegamenu` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_teamegamenu_group` int(10) unsigned NOT NULL,
				`id_parent` int(11) NOT NULL,
				`value` varchar(255) NOT NULL,
				`type` varchar(20) NOT NULL,
				`width` varchar(25) NOT NULL,
				`menu_class` varchar(255) NOT NULL,
				`show_title` tinyint(1) NOT NULL,			  
				`sub_menu` varchar(25) NOT NULL,
				`sub_width` varchar(25) NOT NULL,
                `sub_position` varchar(25) NOT NULL,
				`lesp` int(11) NOT NULL,
				`cat_subcategories` int(11) NOT NULL,
				`sp_lesp` int(11) NOT NULL,
				`position` int(11) NOT NULL,
				`active` tinyint(1) NOT NULL,
			  PRIMARY KEY (`id_teamegamenu`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8 AUTO_INCREMENT=2;
		');

		/* teamegamenu lang */
		$res &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'teamegamenu_lang` (
			  `id_teamegamenu` int(11) NOT NULL,
			  `id_lang` int(11) NOT NULL,
			  `title` varchar(255) DEFAULT NULL,
              `sub_title` varchar(255) DEFAULT NULL,
			  `html` text NOT NULL,
			  `url` text NOT NULL,
			  PRIMARY KEY (`id_teamegamenu`,`id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
		');

		/* menus shop */
		$res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'teamegamenu_shop` (
			  `id_teamegamenu` int(11) NOT NULL,
			  `id_shop` int(11) NOT NULL,
			  PRIMARY KEY (`id_teamegamenu`,`id_shop`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;
		');

		return $res;
	}

	/**
	 * deletes tables
	 */
	protected function deleteTables()
	{
		$menus = $this->getMenus();
		foreach ($menus as $menu)
		{
			$to_del = new Megamenu($menu['id_teamegamenu']);
			$to_del->delete();
		}

		return Db::getInstance()->execute('
			DROP TABLE IF EXISTS 
				`'._DB_PREFIX_.'teamegamenu`, 
				`'._DB_PREFIX_.'teamegamenu_lang`, 
				`'._DB_PREFIX_.'teamegamenu_shop`,
				`'._DB_PREFIX_.'teamegamenu_group`,
				`'._DB_PREFIX_.'teamegamenu_group_shop`,
				`'._DB_PREFIX_.'teamegamenu_group_lang`;
		');
	}

	public function getContent()
	{
		$this->_html .= $this->headerHTML();
		/* Validate & process */
        if(Tools::isSubmit('export_data'))
        {
            $export = new TeaExport();
            $export->exportMenus();
        }
		if (Tools::isSubmit('submitMenu') || Tools::isSubmit('delete_id_teamegamenu') || Tools::isSubmit('duplicate_id_teamegamenu') ||
			Tools::isSubmit('changeStatus') || Tools::getValue('savePosition') || Tools::isSubmit('submitConfigMenu') || Tools::isSubmit('saveAndStayGroup')
			|| Tools::isSubmit('saveGroupMenu') || Tools::isSubmit('changeStatusMenuGroup') ||  Tools::isSubmit('duplicateMenuGroup') || Tools::isSubmit('deleteMenuGroup')
		)
		{
			if ($this->_postValidation())
			{
				$this->_postProcess();
			}
			else
            {
               die( Tools::jsonEncode(
                  array(
                    'error'=>true,
                    'errors'=>$this->_errors,
                  )  
               ));
            }
		}
        elseif(Tools::isSubmit('viewMenuGroup')&&(int)Tools::getValue('id_teamegamenu_group'))
        {
            $this->_html .= $this->renderList();
        }
        elseif((Tools::isSubmit('addMenuItem')||Tools::isSubmit('editMenuItem')) &&(int)Tools::getValue('id_teamegamenu_group'))
        {
            if(Tools::isSubmit('ajax'))
                die(Tools::jsonEncode( array(
                    'block_form'=>$this->renderAddForm(),
                )));
            else
                $this->_html .= $this->renderAddForm();
        }
		elseif(Tools::isSubmit('addMenuGroup') || Tools::isSubmit('editMenugroup'))
		{
            if(Tools::isSubmit('ajax'))
                die(Tools::jsonEncode( array(
                    'block_form'=>$this->initForm(),
                )));
            else
			$this->_html .= $this->initForm();
		}
		else // Default viewport
		{
			$this->_html .= $this->displayForm();
		}

		return $this->_html.$this->display(__FILE__,'admin.tpl');
	}

	protected function _postValidation()
	{
		$errors = array();

		if (Tools::isSubmit('changeStatus'))
		{
			if (!Validate::isInt(Tools::getValue('id_teamegamenu')))
				$errors[] = $this->l('Invalid menu');
		}
		/* Validation for menu */
		elseif (Tools::isSubmit('submitMenu'))
		{
			/* Checks state (active) */
			if (!Validate::isInt(Tools::getValue('active_menu')) || (Tools::getValue('active_menu') != 0 && Tools::getValue('active_menu') != 1))
				$errors[] = $this->l('Invalid menu state.');
			/* Checks position */
			if (!Validate::isInt(Tools::getValue('position')) || (Tools::getValue('position') < 0))
				$errors[] = $this->l('Invalid menu position.');
			/* If edit : checks id_teamegamenu */
			if (Tools::isSubmit('id_teamegamenu'))
			{

				//d(var_dump(Tools::getValue('id_teamegamenu')));
				if (!Validate::isInt(Tools::getValue('id_teamegamenu')) && !$this->menuExists(Tools::getValue('id_teamegamenu')))
					$errors[] = $this->l('Invalid menu ID');
			}
			if (Tools::getValue('type') == 'product')
				if (!Validate::isInt(Tools::getValue('type_product[product]')))
					$errors[] = $this->l('Invalid Product ID');
			/* Checks title/url/legend/description/image */
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
			{
				if (Tools::strlen(Tools::getValue('title_'.$language['id_lang'])) > 255)
					$errors[] = $this->l('The title is too long.');
				if (Tools::strlen(Tools::getValue('url_'.$language['id_lang'])) > 255)
					$errors[] = $this->l('The URL is too long.');						
				if (Tools::strlen(Tools::getValue('url_'.$language['id_lang'])) > 0 && !Validate::isUrl(Tools::getValue('url_'.$language['id_lang'])))
					$errors[] = $this->l('The URL format is not correct.');
			}

			/* Checks title/url/legend/description for default lang */
			$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
			if (Tools::strlen(Tools::getValue('title_'.$id_lang_default)) == 0)
				$errors[] = $this->l('The title is not set.');
			if (Tools::getValue('type') == 'url')
				if (Tools::strlen(Tools::getValue('url_'.$id_lang_default)) == 0)
				$errors[] = $this->l('The url is not set.');
		} /* Validation for deletion */
		elseif (Tools::isSubmit('delete_id_teamegamenu') && (!Validate::isInt(Tools::getValue('delete_id_teamegamenu')) || !$this->menuExists((int)Tools::getValue('delete_id_teamegamenu'))))
			$errors[] = $this->l('Invalid menu ID');
        elseif(Tools::isSubmit('saveGroupMenu'))
        {
            $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
			if (Tools::strlen(Tools::getValue('title_'.$id_lang_default)) == 0)
				$errors[] = $this->l('The title is not set.');
        }

		/* Display errors if needed */
		if (count($errors))
		{
			$this->_errors .= $this->displayError(implode('<br />', $errors));

			return false;
		}

		/* Returns if validation is ok */

		return true;
	}

	protected function _postProcess()
	{
		$errors = array();
		$shop_context = Shop::getContext();
		if (Tools::isSubmit('submitConfigMenu'))
		{
			$shop_groups_list = array();
			$shops = Shop::getContextListShopID();

			foreach ($shops as $shop_id)
			{
				$shop_group_id = (int)Shop::getGroupFromShop($shop_id, true);

				if (!in_array($shop_group_id, $shop_groups_list))
					$shop_groups_list[] = $shop_group_id;

				$res = Configuration::updateValue('teamegamenu_style', Tools::getValue('teamegamenu_style'), false, $shop_group_id, $shop_id);
			}

			/* Update global shop context if needed*/
			switch ($shop_context)
			{
				case Shop::CONTEXT_ALL:
					$res = Configuration::updateValue('teamegamenu_style', Tools::getValue('teamegamenu_style'));
					if (count($shop_groups_list))
					{
						foreach ($shop_groups_list as $shop_group_id)
						{
							$res = Configuration::updateValue('teamegamenu_style', Tools::getValue('teamegamenu_style'), false, $shop_group_id);
						}
					}
					break;
				case Shop::CONTEXT_GROUP:
					if (count($shop_groups_list))
					{
						foreach ($shop_groups_list as $shop_group_id)
						{
							$res = Configuration::updateValue('teamegamenu_style', Tools::getValue('teamegamenu_style'), false, $shop_group_id);
						}
					}
					break;
			}

			$this->clearCache();

			if (!$res)
				$errors[] = $this->displayError($this->l('The configuration could not be updated.'));
			else
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=6&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
		} 	
		elseif (Tools::isSubmit('changeStatusMenuGroup') && Tools::getValue('id_teamegamenu_group'))
		{
			$menugroup = new MegamenuGroup((int)Tools::getValue('id_teamegamenu_group'));
			if ($menugroup->status == 0)
				$menugroup->status = 1;
			else
				$menugroup->status = 0;
				
			$res = $menugroup->update();
			$this->clearCache();
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=6&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
		}	
		elseif (Tools::isSubmit('deleteMenuGroup'))
		{
			$menugroup = new MegamenuGroup((int)Tools::getValue('id_teamegamenu_group'));
			if($menugroup->delete()){
				$menus = $this->getIdMenuByGroup((int)Tools::getValue('id_teamegamenu_group'));
				if($menus){
					foreach($menus as $menu){
						$new_menu = new Megamenu((int)$menu['id_teamegamenu']);
						$new_menu->delete();
					}
				}
                if(Tools::getValue('ajax'))
                  die(Tools::jsonEncode(
                      array(
                        'error'=>false,
                        'vals'=>array(
                            'id'=>$menugroup->id,
                        ),
                      )));
                else  
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=6&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
			}else{
                if(Tools::getValue('ajax'))
                    die(Tools::jsonEncode(
                        array(
                            'error'=>true,
                            'errors'=>$this->displayError('Could not delete.'),
                        )
                    ));
                else
				    $this->_html .= $this->displayError('Could not delete.');
                    
			}
			$this->clearCache();
		}		
		elseif (Tools::isSubmit('saveGroupMenu'))
		{
			if (Tools::getValue('id_teamegamenu_group'))
			{
				$menugroup = new MegamenuGroup((int)Tools::getValue ('id_teamegamenu_group'));
				if (!Validate::isLoadedObject($menugroup))
				{
				    die(Tools::jsonEncode(
                        array(
                            'error'=>true,
                            'errors'=>$this->displayError($this->l('Invalid ID')),
                        )
                    ));
				}
			}
			else
				$menugroup = new MegamenuGroup();
			$next_ps = $this->getNextPosition();
			$menugroup->position = (!empty($menugroup->position)) ? (int)$menugroup->position : $next_ps;
			$menugroup->status = (Tools::getValue('status')) ? (int)Tools::getValue('status') : 0;
			$menugroup->hook	= Tools::getValue('hook');
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
			{
				$menugroup->title[$language['id_lang']] = Tools::getValue('title_'.$language['id_lang']);
			}
			
			Tools::getValue ('id_teamegamenu_group') && $this->moduleExists((int)Tools::getValue ('id_teamegamenu_group'))  ? $menugroup->update() : $menugroup->add ();
			$this->clearCache();
			die(Tools::jsonEncode(
                array(
                    'error'=>false,
                    'vals'=>$this->getGroupFiles($menugroup->id),
                )
            ));
		}		
		elseif (Tools::isSubmit('changeStatus') && Tools::isSubmit('id_teamegamenu'))
		{
			$menu = new Megamenu((int)Tools::getValue('id_teamegamenu'));
			if ($menu->active == 0)
				$menu->active = 1;
			else
				$menu->active = 0;
			$res = $menu->update();
			$this->clearCache();
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&viewMenuGroup&id_teamegamenu_group='.(int)Tools::getValue('id_teamegamenu_group'));
		}
		elseif(Tools::getValue('savePosition') && Tools::getValue('serialized')){
			$mgmenu   = new Megamenu();
			$serialized = Tools::getValue('serialized');
			$lists =  Tools::jsonDecode($serialized, true); 
			$lesp = 1;
			$id_parent = 1;
			$mgmenu->updatePositions($lists,$lesp,$id_parent);
			$this->clearCache();
			die($mgmenu);
        }
		/* Processes menu */
		elseif (Tools::isSubmit('submitMenu'))
		{
			/* Sets ID if needed */
			if (Tools::getValue('id_teamegamenu'))
			{
				$mgmenu = new Megamenu((int)Tools::getValue('id_teamegamenu'));
				if (!Validate::isLoadedObject($mgmenu))
				{
				    die(Tools::jsonEncode(
                        array(
                            'error'=>true,
                            'errors'=>$this->displayError($this->l('Invalid menu ID')),
                        )
                    ));
				}
			}
			else
				$mgmenu = new Megamenu();
				
			/* Sets id_parent */
			$mgmenu->id_parent = (int)Tools::getValue('id_parent');	
			/* Sets type */
			$mgmenu->type = Tools::getValue('type');				
			/* Sets position */
			if (!Tools::getValue('id_teamegamenu'))
			{
				$mgmenu->position = (int)$mgmenu->getmaxPositonMenu();	
			}
			/* Sets active */
			$mgmenu->active = (int)Tools::getValue('active');
			/* Sets show_title */
			$mgmenu->show_title = (int)Tools::getValue('show_title');	
			/* Sets menu_class */
			$mgmenu->menu_class = Tools::getValue('menu_class');	
			/* Sets width */
			$mgmenu->width = Tools::getValue('width');
			/* Sets sub_menu */
			$mgmenu->sub_menu = Tools::getValue('sub_menu');	
			/* Sets sub_width */
			$mgmenu->sub_width = Tools::getValue('sub_width');
            $mgmenu->sub_position = Tools::getValue('sub_position');								
			/* Sets cat_subcategories */
			$mgmenu->cat_subcategories  = (int)Tools::getValue('cat_subcategories');	
			/* Sets limit_subcategories */
			$mgmenu->id_teamegamenu_group  = (int)Tools::getValue('id_teamegamenu_group');			
			/* Sets type */
            if( $mgmenu->type && $mgmenu->type !="html" && Tools::getValue("type_".$mgmenu->type) ){
                $mgmenu->value = serialize(Tools::getValue("type_".$mgmenu->type));
            }

			/* Sets each langue fields */
			$languages = Language::getLanguages(false);

			foreach ($languages as $language)
			{
				$mgmenu->title[$language['id_lang']] = Tools::getValue('title_'.$language['id_lang']);
                $mgmenu->sub_title[$language['id_lang']] = Tools::getValue('sub_title_'.$language['id_lang']);
				$mgmenu->url[$language['id_lang']] = Tools::getValue('url_'.$language['id_lang']);
				$mgmenu->html[$language['id_lang']] = Tools::getValue('html_'.$language['id_lang']);
			}
			/* Processes if no errors  */
			if (!$errors)
			{
				/* Adds */
				if (!Tools::getValue('id_teamegamenu'))
				{
					if (!$mgmenu->add())
						$errors[] = $this->displayError($this->l('The menu could not be added.'));
				}
				/* Update */
				elseif ($mgmenu->update())
					//$errors[] = $this->displayError($this->l('The menu could not be updated.'));
				$this->clearCache();
                die(Tools::jsonEncode(
                    array(
                        'error'=>false,
                        'vals'=>$this->getMenuFiles($mgmenu->id),
                    )
                ));
			}
		} /* Deletes */
		elseif (Tools::isSubmit('delete_id_teamegamenu'))
		{
			Megamenu::deleteMenu((int)Tools::getValue('delete_id_teamegamenu'));
			$this->clearCache();
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=1&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&viewMenuGroup&id_teamegamenu_group='.(int)Tools::getValue('id_teamegamenu_group'));
		}		
		/* Display errors if needed */
		if (count($errors))
			$this->_html .= $this->displayError(implode('<br />', $errors));
		elseif (Tools::isSubmit('submitMenu') && Tools::getValue('id_teamegamenu'))
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_teamegamenu='.Tools::getValue('id_teamegamenu').'&viewMenuGroup&id_teamegamenu_group='.(int)Tools::getValue('id_teamegamenu_group'));
		elseif (Tools::isSubmit('submitMenu'))
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=3&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&viewMenuGroup&id_teamegamenu_group='.(int)Tools::getValue('id_teamegamenu_group'));
	}

	protected function _prepareHook($hook)
	{
		if (!$this->isCached('teamegamenu.tpl', $this->getCacheId()))
		{
			global $link;
			if(!$hook)
				return;
			$id_shop_group = Shop::getContextShopGroupID();
			$id_shop = Shop::getContextShopID();	
			$teamegamenu_style = Tools::getValue('teamegamenu_style', Configuration::get('teamegamenu_style', null, $id_shop_group, $id_shop));
			$current_link = $link->getPageLink('', false, $this->context->language->id);
			$object = new Megamenu();
			$groups 	= $this->getIdGroupByHook($hook);
			
			if(isset($groups['id_teamegamenu_group']) && $groups['id_teamegamenu_group'])
				$megamenu = $object->getMegamenu(1, 1,$groups['id_teamegamenu_group']);
			else
				$megamenu = '';
			$this->smarty->assign( 'teamegamenu', $megamenu );
			$this->smarty->assign( 'current_link', $current_link );
			$this->smarty->assign( 'teamegamenu_style', $teamegamenu_style );
			return $this->display(__FILE__,'teamegamenu.tpl');
		}

		return true;
	}
    public function hookHeader()
    {
		$this->context->controller->addCss( __PS_BASE_URI__.'modules/tea_megamenu/css/teamegamenu.css' );
        $this->context->controller->addJS(__PS_BASE_URI__.'modules/tea_megamenu/js/tea_megamenu.js'); 
    }
	public function hookdisplayMenu($params)
	{
        
		if (!$this->_prepareHook('displayMenu'))
			return false;

		return $this->display(__FILE__, 'teamegamenu.tpl', $this->getCacheId());
	}
	public function hookdisplayMenu2($params)
	{
		if (!$this->_prepareHook('displayMenu2'))
			return false;
		return $this->display(__FILE__, 'teamegamenu2.tpl', $this->getCacheId());
	}
    public function hookdisplayMenu3($params)
	{
		if (!$this->_prepareHook('displayMenu3'))
			return false;
		return $this->display(__FILE__, 'teamegamenu3.tpl', $this->getCacheId());
	}
    public function hookDisplayTop($params)
	{
	   //return $this->_prepareHook('displayTop');
       if (!$this->_prepareHook('displayTop'))
			return false;
		return $this->display(__FILE__, 'teamegamenu-top.tpl', $this->getCacheId());
	}
	
	public function clearCache()
	{
		$this->_clearCache('teamegamenu.tpl');
	}
	
	private function getIdGroupByHook($hook){
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
				SELECT mg.`id_teamegamenu_group`                                                                          
				FROM `'._DB_PREFIX_.'teamegamenu_group` mg,'._DB_PREFIX_.'teamegamenu_group_shop mgs
				WHERE mg.status=1 AND mg.hook = \''.pSQL($hook).'\' AND mg.id_teamegamenu_group = mgs.id_teamegamenu_group AND mgs.id_shop='.(int)$this->context->shop->id);
        return $result;
	}	

	public function hookActionShopDataDuplication($params)
	{
		Db::getInstance()->execute('
			INSERT IGNORE INTO '._DB_PREFIX_.'teamegamenu_shop (id_teamegamenu, id_shop)
			SELECT id_teamegamenu, '.(int)$params['new_id_shop'].'
			FROM '._DB_PREFIX_.'teamegamenu
			WHERE id_shop = '.(int)$params['old_id_shop']
		);
		$this->clearCache();
	}

	public function headerHTML()
	{
        $this->context->controller->addJqueryPlugin('jgrowl');
		if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name)
			return;
		$action = AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules');
		$html = '<script type="text/javascript">var action="'.$action.'";</script>';
		return $html;
	}
	
	public function SortGroupHTML()
	{
		if (Tools::getValue ('controller') != 'AdminModules' && Tools::getValue ('configure') != $this->name)
			return;
		$this->context->controller->addJqueryUI ('ui.sortable');
		$html = '<script type="text/javascript">
			$(function() {
				var $gird_items = $("#gird_items");
				$gird_items.sortable({
					opacity: 0.6,
					cursor: "move",
					handle: ".dragGroup",
					update: function() {
						var order = $(this).sortable("serialize") + "&action=updateGroupPosition";
							$.ajax({
								type: "POST",
								dataType: "json",
								data:order,
								url:"'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/ajax_spsortgroup.php?secure_key='.$this->secure_key.'",
								success: function (msg){
									if (msg.error)
									{
										showErrorMessage(msg.error);
										return;
									}
									$(".positions", $gird_items).each(function(i){
										$(this).text(i);
									});
									showSuccessMessage(msg.success);
								}
							});
						
						}
					});
					$(".dragGroup",$gird_items).hover(function() {
						$(this).css("cursor","move");
					},
					function() {
						$(this).css("cursor","auto");
				    });
			});
		</script>
		';
		$html .= '<style type="text/css">#gird_items .ui-sortable-helper{display:table!important;}</style>';
		return $html;
	}	

	public function getMenus($active = null)
	{
		$this->context = Context::getContext();
		$id_shop = $this->context->shop->id;
		$id_lang = $this->context->language->id;

		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_teamegamenu` as id_teamegamenu, hs.`position`, hs.`active`,hs.`type`, hssl.`title`
			FROM '._DB_PREFIX_.'teamegamenu hs
			LEFT JOIN '._DB_PREFIX_.'teamegamenu_shop hss ON (hs.id_teamegamenu = hss.id_teamegamenu)
			LEFT JOIN '._DB_PREFIX_.'teamegamenu_lang hssl ON (hss.id_teamegamenu = hssl.id_teamegamenu)
			WHERE id_shop = '.(int)$id_shop.'
			AND hssl.id_lang = '.(int)$id_lang.
			($active ? ' AND hs.`active` = 1' : ' ').'
			ORDER BY hs.position'
		);
	}
	public function menuExists($id_teamegamenu)
	{
		$req = 'SELECT hs.`id_teamegamenu` as id_teamegamenu
				FROM `'._DB_PREFIX_.'teamegamenu` hs
				WHERE hs.`id_teamegamenu` = '.(int)$id_teamegamenu;
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);

		return ($row);
	}
	
	public function initForm()
	{
		$default_lang = (int)Configuration::get ('PS_LANG_DEFAULT');
		$hooks = $this->getHookList ();
		$this->fields_form[0]['form'] = array(
			'tinymce' => true,
			'legend'  => array(
				'title' => $this->l('General Options'),
				'icon'  => 'icon-cogs'
			),
			'input'   => array(
				array(
					'type'     => 'text',
					'label'    => $this->l('Title'),
					'lang'     => true,
					'name'     => 'title',
					'required'	=> true,
					'class'    => 'fixed-width-xl',
					'hint'     => $this->l('Title Of Module')
				),

				array(
					'type'   => 'switch',
					'label'  => $this->l('Status'),
					'name'   => 'status',
					'hint'   => $this->l('Status Of Module'),
					'values' => array(
						array(
							'id'    => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id'    => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						)
					)
				),
				array(
					'type'    => 'select',
					'label'   => $this->l('Hook into'),
					'name'    => 'hook',
					'hint'    => $this->l('Select Hook for Module'),
					'options' => array(
						'query' => $hooks,
						'id'    => 'key',
						'name'  => 'name'
					)
				),
                array(
					'type'     => 'text',
					'label'    => $this->l('Class'),
					'name'     => 'params',
					'class'    => 'fixed-width-xl',
				),
			),
			'submit'  => array(
				'title' => $this->l('Save')
			),
		);
		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = 'teamegamenu_group';
		$helper->identifier = $this->identifier;
		$helper->token = Tools::getAdminTokenLite ('AdminModules');
		$helper->show_cancel_button = true;
		$helper->back_url = AdminController::$currentIndex.'&configure='.$this->name.'&token='
			.Tools::getAdminTokenLite ('AdminModules');
		foreach (Language::getLanguages (false) as $lang)
			$helper->languages[] = array(
				'id_lang'    => $lang['id_lang'],
				'iso_code'   => $lang['iso_code'],
				'name'       => $lang['name'],
				'is_default' => ( $default_lang == $lang['id_lang']?1:0 )
			);
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		$helper->toolbar_scroll = true;
		$helper->title = $this->displayName;
		$helper->submit_action = 'saveGroupMenu';
		$helper->toolbar_btn = array(
			'save' => array(
				'desc' => $this->l('Save'),
				'href' => AdminController::$currentIndex.'&configure='.$this->name
					.'&save'.$this->name.'&token='.Tools::getAdminTokenLite ('AdminModules')
			),
			'back' => array(
				'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='
					.Tools::getAdminTokenLite ('AdminModules'),
				'desc' => $this->l('Back to list') )
		);
		$id_teamegamenu_group = (int)Tools::getValue ('id_teamegamenu_group');

		if (Tools::isSubmit ('id_teamegamenu_group') && $id_teamegamenu_group)
		{
			$teamenugroup = new MegamenuGroup((int)$id_teamegamenu_group);
			$params = unserialize($teamenugroup->params);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'hidden',
				'name' => 'id_teamegamenu_group' );
		$helper->fields_value['id_teamegamenu_group'] = Tools::getValue ('id_teamegamenu_group', $teamenugroup->id);
		}
		else
		{
			$teamenugroup = new MegamenuGroup();
			$params = array();
		}
		foreach (Language::getLanguages (false) as $lang)
		{
			$helper->fields_value['title'][(int)$lang['id_lang']] = Tools::getValue ('title_'
				.(int)$lang['id_lang'],
				$teamenugroup->title[(int)$lang['id_lang']]);
		}

		$helper->fields_value['hook'] = Tools::getValue ('hook', $teamenugroup->hook);
		$helper->fields_value['status'] = (int)Tools::getValue('status', $teamenugroup->status);
		$helper->fields_value['params'] = Tools::getValue ('params',$teamenugroup->params);
		return $helper->generateForm ($this->fields_form);
	}	
	
	private function displayForm()
	{
		$currentIndex = AdminController::$currentIndex;
		$modules = array();
		$this->_html .= $this->headerHTML ();
		$this->_html .= $this->SortGroupHTML();
		if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL)
			$this->_html .= $this->getWarningMultishopHtml();
		else if (Shop::getContext() != Shop::CONTEXT_GROUP && Shop::getContext() != Shop::CONTEXT_ALL)
		{
			$modules = $this->getGridItems ();

			if (!empty($modules))
			{
				foreach ($modules as $key => $mod)
				{
					$associated_shop_ids = MegamenuGroup::getAssociatedIdsShop((int)$mod['id_teamegamenu_group']);
					if ($associated_shop_ids && count($associated_shop_ids) > 1)
						$modules[$key]['is_shared'] = true;
					else
						$modules[$key]['is_shared'] = false;
				}
			}
		}
		$this->_html .= '
	 	<div class="panel">
			<div class="panel-heading">
			'.$this->l('Module Manager').'
			<span class="panel-heading-action">
					<a id="add_new_module" class="list-toolbar-btn" href="'.$currentIndex.'&configure='.$this->name
			.'&token='.Tools::getAdminTokenLite ('AdminModules').'&addMenuGroup">
			<span data-toggle="tooltip" class="label-tooltip" data-original-title="'
			.$this->l('Add new module').'" data-html="true"><i class="process-icon-new "></i></span></a>
			</span>
			</div>
			<table width="100%" class="table" cellspacing="0" cellpadding="0" id="list-menus">
    			<thead>
        			<tr class="nodrag nodrop">
        				<th>'.$this->l('ID').'</th>
        				<th>'.$this->l('Ordering').'</th>
        				<th class=" left">'.$this->l('Title').'</th>
        				<th class=" left">'.$this->l('Hook into').'</th>
        				<th class=" left">'.$this->l('Status').'</th>
        				<th class=" right"><span class="title_box text-right">'.$this->l('Actions').'</span></th>
        			</tr>
    			</thead>
			<tbody id="gird_items">';
		if (!empty($modules))
		{
			static $irow;
			foreach ($modules as $teamenugroup)
			{
				$this->_html .= '
				<tr id="item_'.$teamenugroup['id_teamegamenu_group'].'" class=" '.( $irow ++ % 2?' ':'' ).'">
					<td class=" 	" onclick="document.location = \''.$currentIndex.'&configure='.$this->name.'&token='
					.Tools::getAdminTokenLite ('AdminModules').'&viewMenuGroup&id_teamegamenu_group='
					.$teamenugroup['id_teamegamenu_group'].'\'">'
					.$teamenugroup['id_teamegamenu_group'].'</td>
					<td class=" dragHandle"><div class="dragGroup"><div class="positions">'.$teamenugroup['position']
					.'</div></div></td>
					<td class="title" onclick="document.location = \''.$currentIndex.'&configure='.$this->name.'&token='
					.Tools::getAdminTokenLite ('AdminModules')
					.'&viewMenuGroup&id_teamegamenu_group='.$teamenugroup['id_teamegamenu_group'].'\'">'.$teamenugroup['title']
					.' '.($teamenugroup['is_shared'] ? '<span class="label color_field"
		style="background-color:#108510;color:white;margin-top:5px;">'.$this->l('Shared').'</span>' : '').'</td>
					<td class="display-hook" onclick="document.location = \''.$currentIndex.'&configure='.$this->name
					.'&token='.Tools::getAdminTokenLite ('AdminModules').'&viewMenuGroup&id_teamegamenu_group='
					.$teamenugroup['id_teamegamenu_group'].'\'">'
					.$this->default_hook[$teamenugroup['hook']].'</td>
					<td class="status"> <a href="'.$currentIndex.'&configure='.$this->name.'&token='
					.Tools::getAdminTokenLite ('AdminModules')
					.'&changeStatusMenuGroup&id_teamegamenu_group='.$teamenugroup['id_teamegamenu_group'].'&status='
					.$teamenugroup['status'].'&hook='.$teamenugroup['hook'].'">'.( ($teamenugroup['status'] && $teamenugroup['status'] == 1)?'
					<i class="icon-check"></i>':'<i class="icon-remove"></i>' ).'</a> </td>
					<td class="text-right">
						<div class="btn-group-action">
							<div class="btn-group pull-right">
								<a class="btn btn-default edit_group_menu" href="'.$currentIndex.'&configure='.$this->name.'&token='
		.Tools::getAdminTokenLite ('AdminModules').'&editMenugroup&id_teamegamenu_group='.$teamenugroup['id_teamegamenu_group'].'">
									<i class="icon-pencil"></i> Edit
								</a> 
								<button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
									<span class="caret"></span>&nbsp;
								</button>
								<ul class="dropdown-menu">
									<li>
							<a title="'.$this->l('Add item').'" href="'.$currentIndex.'&configure='
					.$this->name.'&token='
					.Tools::getAdminTokenLite ('AdminModules').'&viewMenuGroup&id_teamegamenu_group='
					.$teamenugroup['id_teamegamenu_group'].'">
											<i class="icon-copy"></i> '.$this->l('Add item').'
										</a>								
									</li>
									<li class="divider"></li>
									<li>
										<a class="delete_group_menu" title ="'.$this->l('Delete').'" href="'.$currentIndex
					.'&configure='.$this->name.'&token='
					.Tools::getAdminTokenLite ('AdminModules').'&deleteMenuGroup&id_teamegamenu_group='
					.$teamenugroup['id_teamegamenu_group'].'">
											<i class="icon-trash"></i> '.$this->l('Delete').'
										</a>
									</li>
								</ul>
							</div>
						</div>
					</td>
				</tr>';
			}
		}
		else
		{
			$this->_html .= '<td colspan="5" class="list-empty">
								<div class="list-empty-msg">
									<i class="icon-warning-sign list-empty-icon"></i>
									'.$this->l('No records found').'
								</div>
							</td>';
		}
		$this->_html .= '
			</tbody>
			</table>
		</div>';
	}	
	
	public function renderList()
	{
		$this->context->controller->addJS( __PS_BASE_URI__.'modules/tea_megamenu/js/jquery.nestable.js' ); 
        $this->context->controller->addJS( __PS_BASE_URI__.'modules/tea_megamenu/js/form.js' );
        $this->context->controller->addJS(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');
        $this->context->controller->addJS(_PS_JS_DIR_.'admin/tinymce.inc.js');
		$this->context->controller->addCss( __PS_BASE_URI__.'modules/tea_megamenu/css/form.css' ); 
		$object   		= new Megamenu();
		$output = '
			<div class="form_content col-md-12 col-lg-12">
				<h3>
					<i class="icon-list-ul"></i>
						menus list
					<span class="form-heading-action">
					   <menu id="teamegamenu-menu">
							<button type="button" class="btn btn-info" data-action="expand-all">Expand All</button>
							<button type="button" class="btn btn-info" data-action="collapse-all">Collapse All</button>
						</menu>
						<p><input type="button" value="'.$this->l('Update Position').'" id="savePosition" data-loading-text="'.$this->l('Processing ...').'" class="btn btn-info" name="savePosition"></p>
						<a id="add-new-menu" class="list-toolbar-btn" href="'.Context::getContext()->link->getAdminLink('AdminModules').'&configure=tea_megamenu&addMenuItem=1&id_teamegamenu_group='.(int)Tools::getValue('id_teamegamenu_group').'">
							<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="'.$this->l('Add new').'" data-html="true">
								<i class="process-icon-new "></i>
							</span>
						</a>
                        <a id="black-list-menu" class="list-toolbar-btn" href="'.Context::getContext()->link->getAdminLink('AdminModules').'&configure='.$this->name.'">
							<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="'.$this->l('Back').'" data-html="true">
								<i class="process-icon-back"></i>
							</span>
						</a>
					</span>
				</h3>';
		$output .= $object->getTree(1,1);
		$output .= '</div>';
		return $output;
	}
	
	public function renderForm()
	{
		$style = array(
            array(
                'value' => 'css',
                'label' => $this->l('Css Menu')
            ),
            array(
                'value' => 'mega',
                'label' => $this->l('Mega Menu')
            )
        );
		
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'select',
						'label' => $this->l('Style'),
						'name' => 'teamegamenu_style',
						'options' => array(  'query' => $style,
						'id' => 'value',
						'name' => 'label' ),
						'default' => 'mega',
					),				
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitConfigMenu';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}	

	public function renderAddForm()
	{ 
        $this->context->controller->addJS( __PS_BASE_URI__.'modules/tea_megamenu/js/jquery.nestable.js' ); 
        $this->context->controller->addJS( __PS_BASE_URI__.'modules/tea_megamenu/js/form.js' );
		$this->context->controller->addCss( __PS_BASE_URI__.'modules/tea_megamenu/css/form.css' ); 
		$id_lang    	= $this->context->language->id;
		$id_shop    	= $this->context->shop->id;
		$id_teamegamenu 	= Tools::getValue('id_teamegamenu') ? (int)Tools::getValue('id_teamegamenu') : 0;
		$object   		= new Megamenu($id_teamegamenu);
		$selected_categories = array();
		if($object->value){
			$object->value = unserialize($object->value);
			if($object->cat_subcategories && $object->type == 'subcategories')
				$selected_categories[0] = $object->cat_subcategories;			
		}	
		$categories 	= Category::getCategories( $id_lang, true, false  ) ;
        $manufacturers 	= Manufacturer::getManufacturers(false, $id_lang, true);
        $suppliers     	= Supplier::getSuppliers(false, $id_lang, true);
        $cms          	= CMS::listCms($this->context->language->id, false, true);
        $listMenu  = $object->getChildren(null, $id_lang,$id_shop,true,Tools::getValue('id_teamegamenu_group'));
		$menu =array();
        $menu[]= array(
            'id_teamegamenu'=>1,
            'title' => $this->l('Root'),
        );
        $sub_positions =array(
            array(
                'name'=>'Center',
                'value'=>'position_sub_center',
            ),
            array(
                'name'=>'Left',
                'value'=>'position_sub_left',
            ),
            array(
                'name'=>'Right',
                'value'=>'position_sub_right',
            ),
            array(
                'name'=>'Full',
                'value' =>'position_sub_full',
            )
        );
        if($listMenu)
            foreach($listMenu as $val)
            {
                if($val['id_teamegamenu']!=$id_teamegamenu)
                    $menu[]=array(
                        'id_teamegamenu'=>$val['id_teamegamenu'],
                        'title' => $val['title'],
                    );
            }         	
		$active = array(
            array(
                'id' => 'active_on',
                'value' => 1,
                'label' => $this->l('Enabled')
            ),
            array(
                'id' => 'active_off',
                'value' => 0,
                'label' => $this->l('Disabled')
            )
        );
		
		$sub_menu = array(
            array(
                'value' => 'yes',
                'label' => $this->l('Yes')
            ),
            array(
                'value' => 'no',
                'label' => $this->l('No')
            )
        );
		
		$list_product = array(
			array(
				'id' => 'new',
				'label' => $this->l('New')
			),
			array(
				'id' => 'bestseller',
				'label' => $this->l('Bestseller')
			),
			array(
				'id' => 'special',
				'label' => $this->l('Special')
			),
			array(
				'id' => 'featured',
				'label' => $this->l('Featured')
			)
		);	
		
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Menu information'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
                    array(
                        'type'  => 'text',
                        'label' => $this->l('Title'),
                        'name'  => 'title',
                        'value' => true,
                        'lang'  => true,
    					'required'=> true,
                        'default'=> '',
    					'col' => '6',
                    ),
                    array(
                        'type'  => 'text',
                        'label' => $this->l('Sub title'),
                        'name'  => 'sub_title',
                        'lang'  => true,
                        'default'=> '',
    					'col' => '6',
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Show Title'),
                        'name' => 'show_title',
                        'values' => $active,
                        'default' => 1,
        
                     ),				
                    array(
                        'type' => 'select',
                        'label' => $this->l('Parent ID'),
                        'name' => 'id_parent',
                        'options' => array(  'query' =>   $menu,
                        'id' => 'id_teamegamenu',
                        'name' => 'title' ),
                        'default' => 1,
             
                     ), 			 
                    array(
                        'type' => 'select',
                        'label' => $this->l('Menu Type'),
                        'name' => 'type',
                        'id'    => 'select_menu',
                        'options' => array(  'query' => array(
                            array('id' => 'url', 'name' => $this->l('Url')),
    						array('id' => 'product', 'name' => $this->l('Product')),
    						array('id' => 'productlist', 'name' => $this->l('Product List')),
                            array('id' => 'manufacture', 'name' => $this->l('Manufacture')),
    						array('id' => 'all_manufacture', 'name' => $this->l('All Manufacture')),
                            array('id' => 'supplier', 'name' => $this->l('Supplier')),
    						array('id' => 'all_supplier', 'name' => $this->l('All Supplier')),
                            array('id' => 'cms', 'name' => $this->l('Cms')),
                            array('id' => 'html', 'name' => $this->l('Html')),
                            array('id' => 'category', 'name' => $this->l('Category')),
    						array('id' => 'subcategories', 'name' => $this->l('Sub Categories'))
                        ),
                         'id' => 'id',
                        'name' => 'name' ),
                        'default' => "url",
             
                     ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Product ID'),
                        'name' => 'type_product[product]',
                        'id' => 'type_product',
                        'class'=> 'type_group',
                        'default' => "",
    					'desc'    => $this->l('Ex: 3')
                    ),
    				array(
    					'type' 	  => 'select',
    					'label'   => $this->l('Products List'),
    					'name' 	  => 'type_productlist[type]',
    					'options' => array(  'query' => $list_product ,
    					'id' 	  => 'id',
    					'name' 	  => 'label' ),
    					'default' => "new",
    					'class'=> 'type_group type_product_type',
    					'desc'    => $this->l('Select Product Type')
    	            ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Limit Product'),
                        'name' => 'type_productlist[limit]',
                        'id' => 'type_limit_product',
                        'class'=> 'type_group',
                        'default' => 4,
                    ),				
                    array(
                        'type' => 'select',
                        'label' => $this->l('CMS Type'),
                        'name' => 'type_cms[cms]',
                        'id'   => 'type_cms',
                        'options' => array(  'query' => $cms,
                        'id' => 'id_cms',
                        'name' => 'meta_title' ),
                        'default' => "",
                        'class'=> 'type_group', 
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('URL'),
                        'name' => 'url',
                        'id' => 'type_url',
    					'required'=> true,
    					'lang'  => true,
                        'class'=> 'type_group_lang',
                        'default' => "#",
    					'col' => '6',
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Category'),
                        'name' => 'type_category[category]',
                        'id'   => 'type_category',
                        'options' => array(  'query' => $categories,
                        'id' => 'id_category',
                        'name' => 'name' ),
                        'default' => "",
                        'class'=> 'type_group', 
                    ),
    				array(
    					'type'  => 'categories',
    					'label' => $this->l('Sub categories'),
    					'name'  => "cat_subcategories",
    					'tree'  => array(
    						'id'                  => 'type_subcategories',
    						'selected_categories' => $selected_categories,
                            'root_category'       => $this->context->shop->getCategory()
    					)
    				),
    				array(
                        'type' => 'text',
                        'label' => $this->l('Limit Subcategories Lever 1'),
                        'name' => "type_subcategories[limit1]",
                        'class'=> 'type_group type_limit_subcategories',
                        'default' => 4,
                    ),	
    				array(
                        'type' => 'text',
                        'label' => $this->l('Limit Subcategories Lever 2'),
                        'name' => "type_subcategories[limit2]",
                        'class'=> 'type_group type_limit_subcategories',
                        'default' => 4,
                    ),	
    				array(
                        'type' => 'text',
                        'label' => $this->l('Limit Subcategories Lever 3'),
                        'name' => "type_subcategories[limit3]",
                        'class'=> 'type_group type_limit_subcategories',
                        'default' => 4,
                    ),									
    				array(
                        'type' => 'select',
                        'label' => $this->l('Show Image Sub Categories'),
                        'name' => "type_subcategories[showimg]",
    					'class'	=> 'showimg_subcategories',
    					'options' => array('query' => $sub_menu,
                        'id' => 'value',
                        'name' => 'label' ),
                        'default' => 'no',
                    ),	
    				array(
                        'type' => 'select',
                        'label' => $this->l('Show Image Children Categories'),
                        'name' => "type_subcategories[showimgchild]",
    					'class'	=> 'showimgchild_subcategories',
    					'options' => array(  'query' => $sub_menu,
                        'id' => 'value',
                        'name' => 'label' ),
                        'default' => 'no',
                    ),					 
                    array(
                        'type' => 'select',
                        'label' => $this->l('Manufacture Type'),
                        'name' => 'type_manufacture[manufacture]',
                        'id' => 'type_manufacture',
                        'options' => array(  'query' => $manufacturers,
                        'id' => 'id_manufacturer',
                        'name' => 'name' ),
                        'default' => "",
                        'class'=> 'type_group', 
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Supplier Type'),
                        'name' => 'type_supplier[supplier]',
                        'id' => 'type_supplier',
                        'options' => array(  'query' => $suppliers,
                        'id' => 'id_supplier',
                        'name' => 'name' ),
                        'default' => "",
                        'class'=> 'type_group', 
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Html'),
                        'name' => 'html',
                        'lang' => true,
                        'default' => '',
                        'autoload_rte' => true,
                        'class'=> 'html_lang', 
    					'col' => '6',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Class'),
                        'name' => 'menu_class',
                        'display_image' => true,
                        'default' => '',
    					'col' => '6',
                    ),
    				array(
                        'type' => 'text',
                        'label' => $this->l('Width'),
                        'name' => 'width',
                        'id' => 'width',
                        'class'=> 'width',
                        'default' => "",
    					'desc' => "Ex : 960px , 100%",
    					'col' => '6',
                    ),
    				array(
                        'type' => 'select',
                        'label' => $this->l('Have Sub Menu'),
                        'name' => 'sub_menu',
    					'options' => array(  'query' => $sub_menu,
                        'id' => 'value',
                        'name' => 'label' ),
                        'default' => 'no',
                    ),
    				array(
                        'type' => 'text',
                        'label' => $this->l('Submenu Width'),
                        'name' => 'sub_width',
                        'id' => 'sub_width',
                        'class'=> 'sub_width',
                        'default' => "",
    					'desc' => "Ex : 960px , 100%",
    					'col' => '6',
                    ),
                    array(
                        'type'=>'select',
                        'name'=>'sub_position',
                        'label' => $this->l('Submenu position'),
                        'id' => 'sub_position',
                        'class'=> 'sub_position',
                        'options' => array(  'query' => $sub_positions, 'id'=>'value','name'=>'name'),
                    ),
				    array(
                        'type' => 'switch',
                        'label' => $this->l('Active'),
                        'name' => 'active',
                        'values' => $active,
                        'default' => 1,
                    ),
				
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		if (Tools::isSubmit('id_teamegamenu') && $this->menuExists((int)Tools::getValue('id_teamegamenu')))
		{
			$menu = new Megamenu((int)Tools::getValue('id_teamegamenu'));
			$fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_teamegamenu');
		}
        $fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_teamegamenu_group');
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->module = $this;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitMenu';
        $helper->show_cancel_button = true;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->tpl_vars = array(
			'base_url' => $this->context->shop->getBaseURL(),
			'language' => array(
				'id_lang' => $language->id,
				'iso_code' => $language->iso_code
			),
			'fields_value' => $this->getAddFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
			'image_baseurl' => $this->_path.'images/'
		);

		$helper->override_folder = '/';

		$languages = Language::getLanguages(false);

		if (count($languages) > 1)
			return $this->getMultiLanguageInfoMsg().$helper->generateForm(array($fields_form));
		else
			return $helper->generateForm(array($fields_form));
	}

	public function getAddFieldsValues()
	{
		$fields = array();

		if (Tools::isSubmit('id_teamegamenu') && $this->menuExists((int)Tools::getValue('id_teamegamenu')))
		{
			$mgmenu = new Megamenu((int)Tools::getValue('id_teamegamenu'));
			$fields['id_teamegamenu'] = (int)Tools::getValue('id_teamegamenu', $mgmenu->id);
		}
		else
			$mgmenu = new Megamenu();

		$fields['active'] = Tools::getValue('active', $mgmenu->active);
		$fields['id_parent'] = Tools::getValue('id_parent', $mgmenu->id_parent);
		$fields['type'] = Tools::getValue('type', $mgmenu->type);
		$fields['position'] = Tools::getValue('position', $mgmenu->position);
		$fields['show_title'] = Tools::getValue('show_title', $mgmenu->show_title);
		$fields['menu_class'] = Tools::getValue('menu_class', $mgmenu->menu_class);
		$fields['width'] = Tools::getValue('width', $mgmenu->width);
		$fields['sub_menu'] = Tools::getValue('sub_menu', $mgmenu->sub_menu);
		$fields['sub_width'] = Tools::getValue('sub_width', $mgmenu->sub_width);
        $fields['sub_position']=Tools::getValue('sub_position',$mgmenu->sub_position);
		$fields['sub_width'] = Tools::getValue('sub_width', $mgmenu->sub_width);
		$fields['cat_subcategories'] = Tools::getValue('cat_subcategories', $mgmenu->cat_subcategories);
		$fields['id_teamegamenu_group'] =(int)Tools::getValue('id_teamegamenu_group');
		$array_type = array('product' =>array('product'),
							'productlist' =>array('type','limit'),
							'cms' =>array('cms'),
							'category' =>array('category'),
							'manufacture' =>array('manufacture'),
							'supplier' =>array('supplier'),
							'subcategories' => array('limit1','limit2','limit3','showimg','showimgchild'),
							);
		
		foreach($array_type as $type=>$configs){
			if(isset($mgmenu->type) && $mgmenu->type && $mgmenu->type == $type){
				$values	= unserialize(Tools::getValue('value',$mgmenu->value));
				if($values){
					foreach($values as $key=>$value)
						$fields['type_'.$mgmenu->type.'['.$key.']'] = $value;
				}
			}	
			else{
				$fields["type_".$type] = '';
				if($configs){
					foreach($configs as $config)
						$fields['type_'.$type.'['.$config.']'] = '';
				}
			}	
		}
		$languages = Language::getLanguages(false);

		foreach ($languages as $lang)
		{
			$fields['title'][$lang['id_lang']] = Tools::getValue('title_'.(int)$lang['id_lang'], $mgmenu->title[$lang['id_lang']]);
			$fields['sub_title'][$lang['id_lang']] = Tools::getValue('sub_title_'.$lang['id_lang'],$mgmenu->sub_title[$lang['id_lang']]);
			$fields['url'][$lang['id_lang']] = Tools::getValue('url_'.(int)$lang['id_lang'], $mgmenu->url[$lang['id_lang']]);
			$fields['html'][$lang['id_lang']] = Tools::getValue('html_'.(int)$lang['id_lang'], $mgmenu->html[$lang['id_lang']]);
		}
		return $fields;
	}
	
	private function getGridItems()
	{
		$this->context = Context::getContext ();
		$id_lang = $this->context->language->id;
		$id_shop = $this->context->shop->id;
		$sql = 'SELECT b.`id_teamegamenu_group`,  b.`hook`, b.`position`, b.`status`, bl.`title`
			FROM `'._DB_PREFIX_.'teamegamenu_group` b
			LEFT JOIN `'._DB_PREFIX_.'teamegamenu_group_shop` bs ON (b.`id_teamegamenu_group` = bs.`id_teamegamenu_group` )
			LEFT JOIN `'._DB_PREFIX_.'teamegamenu_group_lang` bl ON (b.`id_teamegamenu_group` = bl.`id_teamegamenu_group`)
			WHERE bs.`id_shop` = '.(int)$id_shop.' 
			AND bl.`id_lang` = '.(int)$id_lang.'
			ORDER BY b.`position`';
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	}
	

	protected function getMultiLanguageInfoMsg()
	{
		return '<p class="alert alert-warning">'.
					$this->l('Since multiple languages are activated on your shop, please mind to upload your image for each one of them').
				'</p>';
	}
	
	public function getConfigFieldsValues()
	{
		$id_shop_group = Shop::getContextShopGroupID();
		$id_shop = Shop::getContextShopID();

		return array(
			'teamegamenu_style' => Tools::getValue('teamegamenu_style', Configuration::get('teamegamenu_style', null, $id_shop_group, $id_shop)),
		);
	}
	
	public function renderProductList($products){
		$tpl = 'views/templates/hook/product.tpl';
		$this->smarty->assign( 'products', $products );
		return $this->display(__FILE__, $tpl);
	}
	
	public function getHookList()
	{
		$hooks = array();
		foreach ($this->default_hook as $key=>$hook)
		{
			$hooks[$key]['key'] = $key;
			$hooks[$key]['name'] = $hook;
		}
		return $hooks;
	}	
	
	private function getHookTitle($id_hook, $name = false)
	{
		if (!$result = Db::getInstance ()->getRow ('
			SELECT `name`,`title`
			FROM `'._DB_PREFIX_.'hook`
			WHERE `id_hook` = '.( $id_hook )))
			return false;
		return ( ( $result['title'] != '' && $name )?$result['title']:$result['name'] );
	}
		
	public function getNextPosition()
	{
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT MAX(cs.`position`) AS `next_position`
			FROM `'._DB_PREFIX_.'teamegamenu_group` cs, `'._DB_PREFIX_.'teamegamenu_group_shop` css
			WHERE css.`id_teamegamenu_group` = cs.`id_teamegamenu_group` AND css.`id_shop` = '.(int)$this->context->shop->id
		);

		return (++$row['next_position']);
	}	
	
	public function moduleExists($id_teamegamenu_group)
	{
		$req = 'SELECT cs.`id_teamegamenu_group` 
				FROM `'._DB_PREFIX_.'teamegamenu_group` cs
				WHERE cs.`id_teamegamenu_group` = '.(int)$id_teamegamenu_group;
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);

		return ($row);
	}
	
	private function getIdMenuByGroup($id_teamegamenu_group,$id_parent=false){
		$sql = '	SELECT ss.`id_teamegamenu`
					FROM `'._DB_PREFIX_.'teamegamenu` ss
					WHERE ss.`id_teamegamenu_group` = '.(int)$id_teamegamenu_group ;
		if($id_parent)	
		$sql .= ' AND id_parent = 1';
				
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		return $result;
	}
	
	public function duplicateParentMenu($id_parent,$id_teamegamenu,$id_teamegamenu_group){
		$id_lang       = Context::getContext()->language->id;
		$id_shop       = Context::getContext()->shop->id;
		$teamegamenu = new Megamenu();
		$menus= $teamegamenu->getChildren($id_parent, $id_lang,$id_shop);
		if($menus){
			foreach($menus as $menu){
				$id_children  = $menu['id_teamegamenu'];
				$parent_menu = new Megamenu((int)$id_children);
				$parent_menu->id = null;
				$parent_menu->id_teamegamenu = null;
				$parent_menu->id_teamegamenu_group = $id_teamegamenu_group;
				$parent_menu->active = 1;
				$parent_menu->id_parent = $id_teamegamenu;
				$parent_menu->position = $parent_menu->getmaxPositonMenu();
				$parent_menu->add();
					$this->duplicateParentMenu($id_children,$parent_menu->id,$id_teamegamenu_group);
			}
		}
	}
    protected function getWarningMultishopHtml()
	{
		if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL)
			return '<p class="alert alert-warning">'.
						$this->l('You cannot manage look book items from a "All Shops" or a "Group Shop" context, select directly the shop you want to edit').
					'</p>';
		else
			return '';
	}
    public function getGroupFiles($id_teamegamenu_group=0)
    {
        $id_lang= Context::getContext()->language->id;
        if($id_teamegamenu_group)
            $menugroup = new MegamenuGroup($id_teamegamenu_group);
        else
            $menugroup = new MegamenuGroup();
        $fields=array();
        
        $fields['title'] = Tools::getValue('title_'.$id_lang, $menugroup->title[$id_lang]);//title_1
        $fields['params']=Tools::getValue('params',$menugroup->params);
        $fields['status'] =Tools::getValue('status',$menugroup->status);
        $fields['id']=$menugroup->id;
        $fields['hook']=$this->default_hook[$menugroup->hook];
        $fields['token']=Tools::getValue('token');
        return $fields;
    }
    public function getMenuFiles($id_menu=0)
    {
        $id_lang=Context::getContext()->language->id;
        if($id_menu)
            $megamenu = new Megamenu($id_menu);
        else
            $megamenu = new Megamenu();
        $fileds =array();
        $fileds['id']=$megamenu->id;
        $fileds['id_parent']=$megamenu->id_parent;
        $fileds['id_group']=$megamenu->id_teamegamenu_group;
        $fileds['position']=$megamenu->position;
        $fileds['active'] =$megamenu->active;
        $fileds['title'] =$megamenu->title[$id_lang];
        $fileds['token']=Tools::getValue('token');
        return $fileds;
    }
}
