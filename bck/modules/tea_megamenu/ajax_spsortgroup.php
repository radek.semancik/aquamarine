<?php
/**
 * @version 1.0.1
 * @author    TeaThemes http://www.teathems.net
 * @copyright (c) 2016 Tea Themes. All Rights Reserved.
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

include_once ( '../../config/config.inc.php' );
include_once ( '../../init.php' );
include_once ( 'tea_megamenu.php' );
$context = Context::getContext ();
$teamegamenu = new tea_megamenu();
$items = array();
if (!Tools::isSubmit ('secure_key') || Tools::getValue ('secure_key') != $teamegamenu->secure_key || !Tools::getValue ('action'))
	die( 1 );
if (Tools::getValue ('action') == 'updateGroupPosition' && Tools::getValue ('item'))
{
	$items = Tools::getValue ('item');
	$pos = array();
	$success = true;
	foreach ($items as $position => $item)
	{
		$success &= Db::getInstance ()->execute ('
			UPDATE `'._DB_PREFIX_.'teamegamenu_group` SET `position` = '.(int)$position.'
			WHERE `id_teamegamenu_group` = '.(int)$item);
	}

	if (!$success)
		die( Tools::jsonEncode (array(
			'error' => 'Update Fail'
		)) );
	die( Tools::jsonEncode (array(
		'success' => 'Update Success !',
		'error'   => false
	)) );
}
