<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Megamenu extends ObjectModel
{
	public $id;
    public $id_teamegamenu;
    public $id_parent = 1;
	public $value;
    public $width;
	public $sub_menu;
	public $sub_width;
    public $sub_position;
    public $type;
    public $show_title = 1;
    public $sp_lesp;
    public $active = 1;
	public $id_teamegamenu_group;
    public $position;
    public $url;
    public $menu_class;
	public $limit_product = 4;
	public $cat_subcategories;
    // Lang
    public 	$title;
    public 	$sub_title;
    public 	$html;
    public 	$id_shop = '';
	private $user_groups;
	private $page_name = '';
	/**
	 * @see ObjectModel::$definition
	 */
 public static $definition = array(
        'table' => 'teamegamenu',
        'primary' => 'id_teamegamenu',
        'multilang' => true,
        'fields' => array(
            'id_parent' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            'width' => array('type' => self::TYPE_STRING, 'validate' => 'isCatalogName', 'size' => 25),
			'sub_menu' => array('type' => self::TYPE_STRING, 'validate' => 'isCatalogName', 'size' => 25),
			'sub_width' => array('type' => self::TYPE_STRING, 'validate' => 'isCatalogName', 'size' => 25),
            'sub_position' => array('type'=>self::TYPE_STRING,'validate'=>'isCatalogName','size'=>25),
            'value' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 255),
            'type' => array('type' => self::TYPE_STRING, 'validate' => 'isCatalogName', 'size' => 255),
            'show_title' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'sp_lesp' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'position' => array('type' => self::TYPE_INT),
			'cat_subcategories' => array('type' => self::TYPE_INT),
			'id_teamegamenu_group' => array('type' => self::TYPE_INT),
            'menu_class' => array('type' => self::TYPE_STRING, 'validate' => 'isCatalogName', 'size' => 255),
            //Lang fields
            'title' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 255),
            'sub_title' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
			'url' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml'),
            'html' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
        ),
    );

	public function add($autodate = true,$null_values = false)
	{
		$id_shop = Context::getContext()->shop->id;
        if(!$this->id_parent)
        {
            if(!Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."teamegamenu_shop WHERE id_teamegamenu=1 AND id_shop=".(int)$id_shop))
                Db::getInstance()->execute("INSERT INTO `"._DB_PREFIX_."teamegamenu_shop` (`id_teamegamenu`, `id_shop`) VALUES(1, ".(int)$id_shop.")");
            $this->id_parent=1;
        }
		$parent = new Megamenu($this->id_parent);
		$this->sp_lesp = $parent->sp_lesp + 1;
		$res = parent::add($autodate,$null_values);
		$res &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'teamegamenu_shop` (`id_shop`, `id_teamegamenu`)
			VALUES('.(int)$id_shop.', '.(int)$this->id.')'
		);
		return $res;
	}

	public function update($null_values = false)
	{
		$id_shop = Context::getContext()->shop->id;
		$parent = new Megamenu($this->id_parent);
		$this->sp_lesp = $parent->sp_lesp + 1;
		parent::update($null_values);
	}

	public function delete()
	{
		$res = true;
		$res &= Db::getInstance()->execute('
			DELETE FROM `'._DB_PREFIX_.'teamegamenu_shop`
			WHERE `id_teamegamenu` = '.(int)$this->id
		);

		$res &= parent::delete();
		return $res;
	}

	public function getChildren($id_teamegamenu = null, $id_lang = null, $id_shop = null, $active = false,$id_teamegamenu_group = false) {
        if (!$id_lang)
            $id_lang = Context::getContext()->language->id; 
        if (!$id_shop)
            $id_shop = Context::getContext()->shop->id;
        $sql = ' SELECT m.*, ml.title,ml.sub_title, ml.html, ml.url
	FROM ' . _DB_PREFIX_ . 'teamegamenu m
	LEFT JOIN ' . _DB_PREFIX_ . 'teamegamenu_lang ml ON m.id_teamegamenu = ml.id_teamegamenu AND ml.id_lang = ' . (int) $id_lang
	. ' JOIN ' . _DB_PREFIX_ . 'teamegamenu_shop ms ON m.id_teamegamenu = ms.id_teamegamenu AND ms.id_shop = ' . (int) ($id_shop);
        if ($id_teamegamenu != null) {
            $sql .= ' WHERE id_parent=' . (int) $id_teamegamenu;
        }
		if ($active)
            $sql .= ' AND m.`active`=1 ';
		if($id_teamegamenu_group)
			$sql .= ' AND m.`id_teamegamenu_group`='.(int)$id_teamegamenu_group;

        $sql .= ' ORDER BY `position` ';
        //die($sql);
        return Db::getInstance()->executeS($sql);
    }


	public function getTree($id_parent, $sp_lesp) {
            $id_lang       = Context::getContext()->language->id;
			$id_shop       = Context::getContext()->shop->id;
			$id_teamegamenu_group =  Tools::getValue('id_teamegamenu_group');
			$menus         	= $this->getChildren($id_parent, $id_lang,$id_shop,false,$id_teamegamenu_group);
			$id_teamegamenu_group = (int)Tools::getValue('id_teamegamenu_group');
			$current 		= AdminController::$currentIndex.'&configure=tea_megamenu&token='.Tools::getAdminTokenLite('AdminModules').'';
			$output = '';
            if($sp_lesp == 1)
			$output .= '<div class="spmenu" id="teamegamenu">';

			$output .= '<ol class="sp_lesp'.$sp_lesp.' spmenu-list">';
            foreach ($menus as $menu) {
                $slc = Tools::getValue('id_teamegamenu') == $menu['id_teamegamenu']?"selected":"";
				$disable = ($menu['active'] && $menu['active'] == 1) ? '' : 'disable';
                $output .='<li id="list_' . $menu['id_teamegamenu'] . '" class="'.$slc.' spmenu-item " data-id="' . $menu['id_teamegamenu'] . '">
				<div class="spmenu-handle"></div>
				<div class="spmenu-content">
						<div class="col-md-6">
							<h4 class="pull-left title">
								#'.$menu['id_teamegamenu'].' - '.$menu['title'].'
							</h4>
						</div>
						<div class="col-md-6">
							<div class="btn-group-action pull-right">
								'.$this->displayStatus($menu['id_teamegamenu'], $menu['active']).'
								<a class="btn btn-default edit_menu"
									href="'.Context::getContext()->link->getAdminLink('AdminModules').'&configure=tea_megamenu&id_teamegamenu='.$menu['id_teamegamenu'].'&editMenuItem&id_teamegamenu_group='.(int)Tools::getValue('id_teamegamenu_group').'">
									<i class="icon-edit"></i>
									Edit
								</a>
								<a class="btn btn-default btn-danger remove-menu"
									href="'.Context::getContext()->link->getAdminLink('AdminModules').'&configure=tea_megamenu&delete_id_teamegamenu='.$menu['id_teamegamenu'].'&deleteMenuItem&id_teamegamenu_group='.(int)Tools::getValue('id_teamegamenu_group').'">
									<i class="icon-trash"></i>
									Delete
								</a>
							</div>
						</div>
				</div>';
				$chil = $this->getCategoryChild($menu['id_teamegamenu']);
                if ($menu['id_teamegamenu'] != $id_parent && count($chil) > 0)
                    $output .= $this->getTree($menu['id_teamegamenu'], $sp_lesp + 1);
                $output .= '</li>';
            }

            $output .= '</ol>';

			if($sp_lesp == 1)
				$output .= '</div>';

            return $output;
    }

	public function displayStatus($id_teamegamenu, $active)
	{
		$title = ((int)$active == 0 ? 'Disabled' : 'Enabled');
		$icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
		$class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
		$id_teamegamenu_group = (int)Tools::getValue('id_teamegamenu_group');
		$html = '<a class="tea_active btn '.$class.'" href="'.AdminController::$currentIndex.
			'&configure=tea_megamenu
				&token='.Tools::getAdminTokenLite('AdminModules').'
				&changeStatus&id_teamegamenu='.(int)$id_teamegamenu.'&viewMenuGroup&id_teamegamenu_group='.(int)$id_teamegamenu_group.'" title="'.$title.'"><i class="'.$icon.'"></i> '.$title.'</a>';

		return $html;
	}

	public function updatePositions($lists,$sp_lesp,$id_parent){
		if($lists){
			foreach ($lists as $position => $list)
			{
				$res = Db::getInstance()->execute('
					UPDATE `'._DB_PREFIX_.'teamegamenu` SET `position` = '.(int)$position.',`sp_lesp` = '.(int)$sp_lesp.',`id_parent` = '.(int)$id_parent.'
				WHERE `id_teamegamenu` = '.(int)$list['id']
				);
				if(isset($list['children']) && $list['children']){
					$sp_lesp++;
					$this->updatePositions($list['children'],$sp_lesp,$list['id']);
				}
			}
		}
	}



	public function renderSubMenu($id_teamegamenu,$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group=0){
		$output = '';
			if ($id_teamegamenu != $id_parent){
				$parent = new Megamenu($id_teamegamenu);
				$output .= '<div class="sub-menu dropdown-menu" '.$style_sub.'>';
					$output .= $this->getMegamenu($id_teamegamenu, $sp_lesp + 1,$id_teamegamenu_group);
				$output .= '</div>';
			 }
		return 	$output;
	}

	public static function deleteMenu($idteamegamenu){
		$object = new Megamenu((int)$idteamegamenu);
		if($object->delete()){
			self::deleteChildren($idteamegamenu);
		}
	}

	public static function deleteChildren($idteamegamenu){
		$childrens =  self::getCategoryChild($idteamegamenu);
		if($childrens){
			foreach($childrens as $children)
				self::deleteMenu($children['id_teamegamenu']);
		}
	}


	public static function getCategoryChild($id_teamegamenu){
		$id_lang    = Context::getContext()->language->id;
		$id_shop    = Context::getContext()->shop->id;
		$sql = ' SELECT m.*, ml.title , ml.html, ml.url
					FROM ' . _DB_PREFIX_ . 'teamegamenu m
						LEFT JOIN ' . _DB_PREFIX_ . 'teamegamenu_lang ml ON m.id_teamegamenu = ml.id_teamegamenu AND ml.id_lang = ' . (int) $id_lang
						. ' JOIN ' . _DB_PREFIX_ . 'teamegamenu_shop ms ON m.id_teamegamenu = ms.id_teamegamenu AND ms.id_shop = ' . (int) ($id_shop).'
							WHERE m.id_parent = ' . (int) ($id_teamegamenu);
		return Db::getInstance()->executeS($sql);
	}



	public function getmaxPositonMenu(){
		$sql = ' SELECT MAX(position) as max
					FROM ' . _DB_PREFIX_ . 'teamegamenu';
		$max =  Db::getInstance()->getRow($sql);
		return $max['max'] + 1;
	}

	public static function getAssociatedIdsShop($id_teamegamenu)
	{
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_shop`
			FROM `'._DB_PREFIX_.'teamegamenu_shop` hs
			WHERE hs.`id_teamegamenu` = '.(int)$id_teamegamenu
		);

		if (!is_array($result))
			return false;

		$return = array();

		foreach ($result as $id_shop)
			$return[] = (int)$id_shop['id_shop'];

		return $return;
	}

	public function getMegamenu($id_parent, $sp_lesp,$id_teamegamenu_group=null){
		global $link, $cookie;
		$this->page_name = Dispatcher::getInstance()->getController();
		$output = '';
		$module    	= new tea_megamenu();
		$id_lang    = Context::getContext()->language->id;
		$id_shop    = Context::getContext()->shop->id;
		$menus      = $this->getChildren($id_parent, $id_lang,$id_shop,true,$id_teamegamenu_group);
		$current	=	'';
		$grower = '';
		if($menus ){
		if($sp_lesp ==1 )
			$output = '<ul class="nav-main clearfix sp_lesp level-'.$sp_lesp.'">';
		else
			$output ='<ul class="menu-sub level-'.$sp_lesp.'">';
			foreach ($menus as $menu) {
			$class = 'item-'.$sp_lesp.' ';
			if($menu['menu_class'])
				$class .= $menu['menu_class'];

			$chil = $this->getCategoryChild($menu['id_teamegamenu']);

			if(count($chil) > 0){
				$class .= " parent has-child has-sub";
				$grower = '<span class="growers close"> </span>';
			}
			else
				$grower = '';
			$style = '';
			$style_sub = '';
			if($menu['width'])
				$style = 'style="width:'.$menu['width'].'"';
			if($menu['sub_menu'] == 'yes' && $menu['sub_width'])
				$style_sub = 'style="width:'.$menu['sub_width'].'"';
			$config = @unserialize($menu['value']);
			switch ($menu['type'])
			{
				case 'subcategories':
					$output .= $this->generateCategoriesMenu(Category::getNestedCategories($menu['cat_subcategories'], $id_lang, false, $this->user_groups),0,$class,$style,$menu);
					break;
				case 'category':
						if($this->page_name == 'category' && (Tools::getValue('id_category') == $config['category']))
							$class = $class.' active';
						if (Validate::isLoadedObject($category = new Category($config['category'], $id_lang))) {
							$output .= '<li class="'.$class.' '.($menu['sub_position']? $menu['sub_position']:'position_sub_center').'" '.$style.'><a href="'.Tools::HtmlEntitiesUTF8($link->getCategoryLink((int) $category->id, $category->link_rewrite, $id_lang)).'" title="'.($menu['show_title']?$menu['title']:$category->name).'">'.($menu['show_title']?$menu['title']:$category->name).'<span class="sub_menu">'.$menu['sub_title'].'</span></a>';	
							if(count($chil) > 0 && $menu['sub_menu'] == 'yes')
							$output .= $this->renderSubMenu($menu['id_teamegamenu'],$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group);
							$output .= $grower;
							$output .= '</li>'.PHP_EOL;
						}
					break;
				case 'product':
					$product = new Product((int)$config['product'], true, (int)$id_lang);
					if (!is_null($product->id))
                    {
                        $output .= '<li class="'.$class.' '.($menu['sub_position']? $menu['sub_position']:'position_sub_center').'" '.$style.'><a href="'.Tools::HtmlEntitiesUTF8($product->getLink()).'" title="'.($menu['show_title']?$menu['title']:$product->name).'">'.($menu['show_title']?$menu['title']:$product->name).'<span class="sub_menu">'.$menu['sub_title'].'</span></a>';
						if(count($chil) > 0 && $menu['sub_menu'] == 'yes')
							$output .= $this->renderSubMenu($menu['id_teamegamenu'],$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group);
						$output .= $grower;
						$output .= '</li>'.PHP_EOL; 
                    }	
					break;

				case 'productlist':
						$output .= '<li class="'.$class.' '.($menu['sub_position']? $menu['sub_position']:'position_sub_center').'" '.$style.'>';
						$limit = (isset($config['limit']) && $config['limit']) ? (int)$config['limit'] : 4;
						$output .= $menu['show_title']?'<h2 class="title">'.($menu['title']).'<span class="sub_menu">'.$menu['sub_title'].'</span></h2>':'';
						if( $config['type']){
							$products = array();
							switch ( $config['type'] ) {
								case 'new':
									 $products = Product::getNewProducts((int) Context::getContext()->language->id, 0, $limit);
									break;
								case 'featured':
									$category = new Category(Context::getContext()->shop->getCategory(), (int)(Context::getContext()->language->id) );
									$products = $category->getProducts((int)(Context::getContext()->language->id), 1, $limit);
									break;
								case 'bestseller':
									$products = ProductSale::getBestSales((int)(Context::getContext()->language->id), 0, $limit,'date_add');
									break;
								case 'special':
									 $products = $special = Product::getPricesDrop((int)(Context::getContext()->language->id), 0, $limit,false);
									break;
							}
							$module = new tea_megamenu();
							$output .= 	$module->renderProductList($products);
						}
						if(count($chil) > 0 && $menu['sub_menu'] == 'yes')
							$output .= $this->renderSubMenu($menu['id_teamegamenu'],$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group);
						$output .= $grower;
                        $output .= '</li>';

					break;

				case 'cms':
					if($this->page_name == 'cms' && (Tools::getValue('id_cms') == $config['cms']))
							$class = $class.' active';
					$cms = CMS::getLinks((int)$id_lang, array($config['cms']));
					if (count($cms))
                    {
                        $output .= '<li class="'.$class.' '.($menu['sub_position']? $menu['sub_position']:'position_sub_center').'" '.$style.'><a href="'.Tools::HtmlEntitiesUTF8($cms[0]['link']).'" title="'.($menu['show_title']? Tools::safeOutput($menu['title']):$cms[0]['meta_title']).'">'.($menu['show_title']? Tools::safeOutput($menu['title']):$cms[0]['meta_title']).'</a>';
							
						if(count($chil) > 0 && $menu['sub_menu'] == 'yes')
							$output .= $this->renderSubMenu($menu['id_teamegamenu'],$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group);
						$output .= $grower;
						$output .= '</li>'.PHP_EOL;
                    }	
					break;

				// Case to handle the option to show all Manufacturers
				case 'all_manufacture':
					$link = new Link;
					$output .= '<li class="'.$class.' '.($menu['sub_position']? $menu['sub_position']:'position_sub_center').'" '.$style.'>';
                    $output .=$menu['show_title']?'<a href="'.$link->getPageLink('manufacturer').'" title="'.$menu['title'].'">'.$menu['title'].'<span class="sub_menu">'.$menu['sub_title'].'</span></a>':'';
                    $manufacturers = Manufacturer::getManufacturers();
                    if($manufacturers)
                    {
                        $output .= '<ul>'.PHP_EOL;
    					foreach ($manufacturers as $key => $manufacturer)
    						$output .= '<li><a href="'.$link->getManufacturerLink((int)$manufacturer['id_manufacturer'], $manufacturer['link_rewrite']).'" title="'.Tools::safeOutput($manufacturer['name']).'">'.Tools::safeOutput($manufacturer['name']).'</a></li>'.PHP_EOL;
    					$output .= '</ul>';
                    }
                    if(count($chil) > 0 && $menu['sub_menu'] == 'yes')
							$output .= $this->renderSubMenu($menu['id_teamegamenu'],$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group);
						$output .= $grower;
                    $output .='</li>';
					break;
				case 'manufacture':
					if($this->page_name == 'manufacturer' && (Tools::getValue('id_manufacturer') == $config['manufacture']))
							$class = $class.' active';
					$manufacturer = new Manufacturer((int)$config['manufacture'], (int)$id_lang);
					if (!is_null($manufacturer->id))
					{
						if (intval(Configuration::get('PS_REWRITING_SETTINGS')))
							$manufacturer->link_rewrite = Tools::link_rewrite($manufacturer->name);
						else
							$manufacturer->link_rewrite = 0;
						$link = new Link;
						$output .= '<li class="'.$class.' '.($menu['sub_position']? $menu['sub_position']:'position_sub_center').'" '.$style.'><a href="'.Tools::HtmlEntitiesUTF8($link->getManufacturerLink((int)$config['manufacture'], $manufacturer->link_rewrite)).'" title="'.($menu['show_title']? Tools::safeOutput($menu['title']):$manufacturer->name).'">'.($menu['show_title']? Tools::safeOutput($menu['title']):$manufacturer->name).'</a>';
						if(count($chil) > 0 && $menu['sub_menu'] == 'yes')
							$output .= $this->renderSubMenu($menu['id_teamegamenu'],$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group);
						$output .= $grower;
						$output .= '</li>'.PHP_EOL;
					}
					break;

				// Case to handle the option to show all Suppliers
				case 'all_supplier':
					$link = new Link;
					$output .= '<li class="'.$class.' '.($menu['sub_position']? $menu['sub_position']:'position_sub_center').'" '.$style.'>';
                    $output .=$menu['show_title']?'<a href="'.$link->getPageLink('supplier').'" title="'.$menu['title'].'">'.$menu['title'].'<span class="sub_menu">'.$menu['sub_title'].'</span></a>':'';
                    $suppliers = Supplier::getSuppliers();
                    if($suppliers)
                    {
                        $output .= '<ul>'.PHP_EOL;
					
    					foreach ($suppliers as $key => $supplier)
    						$output .= '<li><a href="'.$link->getSupplierLink((int)$supplier['id_supplier'], $supplier['link_rewrite']).'" title="'.Tools::safeOutput($supplier['name']).'">'.Tools::safeOutput($supplier['name']).'</a></li>'.PHP_EOL;
    					$output .= '</ul>';
                    }
                    if(count($chil) > 0 && $menu['sub_menu'] == 'yes')
							$output .= $this->renderSubMenu($menu['id_teamegamenu'],$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group);
						$output .= $grower;
					$output .='</li>';
					break;

				case 'supplier':
					if($this->page_name == 'supplier' && (Tools::getValue('id_supplier') == $config['supplier']))
						$class = $class.' active';
					$supplier = new Supplier((int)$config['supplier'], (int)$id_lang);
					if (!is_null($supplier->id))
					{
						$link = new Link;
						$output .= '<li class="'.$class.' '.($menu['sub_position']? $menu['sub_position']:'position_sub_center').'" '.$style.'><a href="'.Tools::HtmlEntitiesUTF8($link->getSupplierLink((int)$config['supplier'], $supplier->link_rewrite)).'" title="'.($menu['show_title']?$menu['title']:$supplier->name).'">'.($menu['show_title']?$menu['title']:$supplier->name).'</a>';
						if(count($chil) > 0 && $menu['sub_menu'] == 'yes')
							$output .= $this->renderSubMenu($menu['id_teamegamenu'],$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group);
						$output .= $grower;
						$output .= '</li>'.PHP_EOL;
					}
					break;
				case 'url':
						$output .= '<li class="'.$class.' '.($menu['sub_position']? $menu['sub_position']:'position_sub_center').'" '.$style.'>';
						$output .= $menu['show_title']?'<a href="'.Tools::HtmlEntitiesUTF8($menu['url']).'" title="'.Tools::safeOutput($menu['title']).'">'.Tools::safeOutput($menu['title']).'<span class="sub_menu">'.$menu['sub_title'].'</span></a>':'';	
							if(count($chil) > 0 && $menu['sub_menu'] == 'yes')
								$output .= $this->renderSubMenu($menu['id_teamegamenu'],$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group);
						$output .= $grower;
						$output .= '</li>'.PHP_EOL;
					break;
				case 'html':
						$output .= '<li class="'.$class.' '.($menu['sub_position']? $menu['sub_position']:'position_sub_center').'" '.$style.'>';
						$output .= $menu['show_title']?'<h2 class="title">'.$menu['title'].'<span class="sub_menu">'.$menu['sub_title'].'</span></h2>':'';
						if( $menu['html']){
							$output .= '<div class="menu-content">'.$menu['html'].'</div>';
						}
                        if(count($chil) > 0 && $menu['sub_menu'] == 'yes')
							$output .= $this->renderSubMenu($menu['id_teamegamenu'],$id_parent,$sp_lesp,$style_sub,$id_teamegamenu_group);
						$output .= $grower;
						$output .= '</li>';
					break;
			}
		}
		$output .= '</ul>';
		}

		return $output;
	}

	private function getCMSMenuItems($parent, $depth = 1, $id_lang = false)
	{
		$id_lang = $id_lang ? (int)$id_lang : (int)Context::getContext()->language->id;

		if ($depth > 3)
			return;

		$categories = $this->getCMSCategories(false, (int)$parent, (int)$id_lang);
		$pages = $this->getCMSPages((int)$parent);

		if (count($categories) || count($pages))
		{
			$output .= '<ul>';

			foreach ($categories as $category)
			{
				$cat = new CMSCategory((int)$category['id_cms_category'], (int)$id_lang);

				$output .= '<li>';
				$output .= '<a href="'.Tools::HtmlEntitiesUTF8($cat->getLink()).'">'.$category['name'].'</a>';
				$this->getCMSMenuItems($category['id_cms_category'], (int)$depth + 1);
				$output .= '</li>';
			}

			foreach ($pages as $page)
			{
				$cms = new CMS($page['id_cms'], (int)$id_lang);
				$links = $cms->getLinks((int)$id_lang, array((int)$cms->id));

				$selected = ($this->page_name == 'cms' && ((int)Tools::getValue('id_cms') == $page['id_cms'])) ? ' class="sfHoverForce"' : '';
				$output .= '<li '.$selected.'>';
				$output .= '<a href="'.$links[0]['link'].'">'.$cms->meta_title.'</a>';
				$output .= '</li>';
			}

			$output .= '</ul>';
		}
	}

	protected function generateCategoriesMenu($categories, $is_children = 0,$class=null,$style=null,$menu=array(),$rep = 0)
    {

        $html = '';
		$rep++;
		$context = Context::getContext();
		$id_lang    = Context::getContext()->language->id;
		$config = unserialize($menu['value']);
        foreach ($categories as $key => $category) {
            if ($category['level_depth'] > 1) {
                $cat = new Category($category['id_category']);
                $link = Tools::HtmlEntitiesUTF8($cat->getLink());
            } else {
                $link=$context->link->getPageLink('index');
            }
            /* Whenever a category is not active we shouldnt display it to customer */
            if ((bool)$category['active'] === false) {
                continue;
            }
			$str_class = '';
			$grower = '';
            
			if (isset($category['children']) && !empty($category['children']) && ( strpos( $class, 'parent' ) === false )) {
				$class .= ' parent';
				$grower .= '<span class="growers close"> </span>';
			}
			if($this->page_name == 'category' && (Tools::getValue('category') == $menu['type']))
				$class = $class.' sfHoverForce';

			$str_class = 'class="'.$class.' sub_type_category"';
            $html .= '<li '.$str_class.' '.$style.' >';
            $html .= '<a href="'.$link.'" title="'.$category['name'].'">'.$category['name'].'</a>';
			if (isset($config) && $config['showimgchild'] == 'yes' && $rep >1) {
			$subcate = new Category($category['id_category'], $id_lang);
				$html .= '<div><img src="'. $context->link->getCatImageLink($subcate->link_rewrite, $subcate->id_image, 'category_default')
					.'" alt="'.Tools::SafeOutput($category['name']).'" title="'
					.Tools::SafeOutput($category['name']).'" class="imgm" /></div>';
			}
            if (isset($category['children']) && !empty($category['children'])) {

				$check = 1;
				$childrens = array();
				if(isset($config['limit'.$rep]) && $config['limit'.$rep]){
					foreach($category['children'] as $key=>$children){
						if($check <= $config['limit'.$rep])
							$childrens[$key] = $children;
						$check ++ ;
					}
				}
				else
					$childrens = $category['children'];

                $html .= '<div class="dropdown-menu"><ul>';

                $html .= $this->generateCategoriesMenu($childrens, 1,null,null,$menu,$rep);
                if (isset($config) && $config['showimg'] == 'yes' && $rep <=1) {
					$cate = new Category($category['id_category'], $id_lang);
                        $html .= '<li class="category-thumbnail">';
                                $html .= '<div><img src="'. $context->link->getCatImageLink($cate->link_rewrite, $cate->id_image, 'category_default')
                                .'" alt="'.Tools::SafeOutput($category['name']).'" title="'
                                .Tools::SafeOutput($category['name']).'" class="imgm" /></div>';
                        $html .= '</li>';
                }
                $html .= '</ul></div>';
            }
			$html .= $grower;
            $html .= '</li>';
        }
        
        return $html;
    }
}
