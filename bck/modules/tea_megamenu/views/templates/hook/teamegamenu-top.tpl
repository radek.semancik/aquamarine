{if $teamegamenu != ''}
	<div id="tea_megamenu_top" class="teamegamenu">

		<nav class="navbar-default">

            <button class="navbar-brand">

				<img width="25" height="22" src="{$img_dir}icon/bar.svg" alt="" />

			</button>

            <div class="overlay_menu"></div>

			<div id="tea-megamenu-top" class="{$teamegamenu_style} tea-megamenu clearfix">

                <h3 class="title_menu">{l s='Menu' mod='tea_megamenu'}<span id="remove-megamenu"><i class="close-menu pe-7s-close"></i></span></h3>

				<div class="menu_show_mobile hidden-lg hidden-md hidden-sm">

                    <div class="search_top">

                        <a data-toggle="modal" data-target="#search_block_top" title="Search" href="javascript:void(0);">

                            <i class="pe-7s-search"></i>

                        </a>

                    </div>

                    <div class="header_user_info">

                    	{if $is_logged}

                    		<a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">

                    			<i class="pe-7s-user"></i>

                    		</a>

                            <div class="my_account_link">

                                <ul>

                                    <li>

                                        <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">{l s='My account' mod='blockuserinfo'}</a>

                                    </li>

                        			<li>

                                        <a href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="{l s='My orders' mod='blockuserinfo'}" rel="nofollow">{l s='My orders' mod='blockuserinfo'}</a></li>

                        			{if $returnAllowed}<li>

                                        <a href="{$link->getPageLink('order-follow', true)|escape:'html':'UTF-8'}" title="{l s='My merchandise returns' mod='blockuserinfo'}" rel="nofollow">{l s='My merchandise returns' mod='blockuserinfo'}</a></li>{/if}

                        			<li>

                                        <a href="{$link->getPageLink('order-slip', true)|escape:'html':'UTF-8'}" title="{l s='My credit slips' mod='blockuserinfo'}" rel="nofollow">{l s='My credit slips' mod='blockuserinfo'}</a></li>

                        			<li>

                                        <a href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='My addresses' mod='blockuserinfo'}" rel="nofollow">{l s='My addresses' mod='blockuserinfo'}</a></li>

                        			<li>

                                        <a href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" title="{l s='Manage my personal information' mod='blockuserinfo'}" rel="nofollow">{l s='My personal info' mod='blockuserinfo'}</a></li>

                        			{if $voucherAllowed}<li>

                                        <a href="{$link->getPageLink('discount', true)|escape:'html':'UTF-8'}" title="{l s='My vouchers' mod='blockuserinfo'}" rel="nofollow">{l s='My vouchers' mod='blockuserinfo'}</a></li>{/if}

                        			{$HOOK_BLOCK_MY_ACCOUNT}

                                    <li>

                                        <a href="{$link->getPageLink('index')}?mylogout" title="{l s='Sign out' mod='blockuserinfo'}" rel="nofollow">{l s='Sign out' mod='blockuserinfo'}</a>

                                    </li>

                    \

                                </ul>

                            </div>

                    	{else}

                    		<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">

                    			<i class="pe-7s-user"></i>

                    		</a>

                            <div class="my_account_link">

                                <ul>

                                    <li>

                                        <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">{l s='Login / Register' mod='blockuserinfo'}</a>

                                    </li>

                                </ul>

                            </div>

                    	{/if}

                    </div>

                    

                    <a class="lnk_wishlist"	href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|escape:'html':'UTF-8'}" title="{l s='My wishlists' mod='tea_megamenu'}">

                		<i class="pe-7s-like"></i>

                	</a>

                    

                    

                    

                </div>

                {$teamegamenu}

                

			</div>

		</nav>

	</div>

{/if}