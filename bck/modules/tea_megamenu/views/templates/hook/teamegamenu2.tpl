{if $teamegamenu != ''}
	<div id="tea_megamenu_right" class="teamegamenu">
		<nav class="navbar-default" role="navigation">
			<div id="tea-megamenu" class="{$teamegamenu_style} tea-megamenu clearfix">
				<span id="remove-megamenu" class="icon-remove"></span>
				<a href="#" class="navbar-brand">
					<i class="icon-home"></i>
				</a>
				{$teamegamenu nofilter}
			</div>
		</nav>
	</div>
{/if}