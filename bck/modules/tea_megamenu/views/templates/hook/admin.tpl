<div id="block-form" style="">

</div>
<script>
var edit_text ='{l s='Edit' mod='tea_megamenu'}';
var add_item_text ='{l s='Add item' mod='tea_megamenu'}';
var delete_text ='{l s='Delete' mod='tea_megamenu'}';
var delete_question ='{l s='Are you sure delete item?' mod='tea_megamenu'}';
var enabled_text ='{l s='Enabled' mod='tea_megamenu'}';
var disable_text ='{l s='Disable' mod='tea_megamenu'}';
var successful_deletion ='{l s='Successful deletion' mod='tea_megamenu'}';
var successful_creation  ='{l s='Successful creation' mod='tea_megamenu'}';
var successful_update  = '{l s='Successful update' mod='tea_megamenu'}'; 
$(document).ready(function(){
   $(document).on('click','#add_new_module,.edit_group_menu',function(e){
        e.preventDefault();
        $.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: $(this).attr('href'),
			async: true,
			cache: false,
			dataType : "json",
			data: 'ajax=1',
			success: function(jsonData)
			{
			     $('#block-form').show();
			     $('#block-form').html('<span class="tea_close_popup"></span>'+jsonData.block_form);
                 $('.panel-footer a.btn-default').removeAttr('onclick');
			}
		});
   });
   $(document).on('click','#add-new-menu,.edit_menu',function(e){
        e.preventDefault();
        $.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: $(this).attr('href'),
			async: true,
			cache: false,
			dataType : "json",
			data: 'ajax=1',
			success: function(jsonData)
			{
			     $('#block-form').show();
			     $('#block-form').html('<span class="tea_close_popup"></span>'+jsonData.block_form);
                 eventSelectMenu();
                 eventSelectWidth();
                 $('.panel-footer a.btn-default').removeAttr('onclick');
			}
		});
   });
   $(document).on('click','button[name="saveGroupMenu"]',function(e){
            e.preventDefault();
            var formData = new FormData($(this).parents('form').get(0));
            $.ajax({
                url: $(this).parents('form').eq(0).attr('action'),
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(json){
                    if(json.error)
                    {
                        alert(json.errors);
                    }                
                    else
                    {
                        $('#block-form').hide();
                        if($('#list-menus > #gird_items #item_'+json.vals.id).length <=0 )
                        {
                            $.growl.notice({ message: successful_creation });
                            var html='<tr id="item_'+json.vals.id+'" class=" ">';
                                html +='<td class=" " onclick="document.location = \'index.php?controller=AdminModules&configure=tea_megamenu&token='+json.vals.token+'&viewMenuGroup&id_teamegamenu_group='+json.vals.id+'\'">'+json.vals.id+'</td>';
                                html +='<td class="dragHandle"><div class="dragGroup" style="cursor: auto;"><div class="positions">'+json.vals.id+'</div></div></td>';
                                html +='<td class="title" onclick="document.location = \'index.php?controller=AdminModules&configure=tea_megamenu&token='+json.vals.token+'&viewMenuGroup&id_teamegamenu_group='+json.vals.id+'\'">'+json.vals.title+'</td>';
                                html +='<td class="display-hook" onclick="document.location = \'index.php?controller=AdminModules&configure=tea_megamenu&token='+json.vals.token+'&viewMenuGroup&id_teamegamenu_group='+json.vals.id+'\'">'+json.vals.hook+'</td>';
                                html +='<td class="status"><a href="index.php?controller=AdminModules&configure=tea_megamenu&token='+json.vals.token+'&changeStatusMenuGroup&id_teamegamenu_group='+json.vals.id+'&status='+json.vals.status+'">'+(json.vals.status==0?'<i class="icon-remove"></i>':'<i class="icon-check"></i>')+'</a></td>';
                                html +='<td class="text-right">';
                                    html +='<div class="btn-group-action">';
                                      html +='<div class="btn-group pull-right">';
                                        html +='<a class="btn btn-default edit_group_menu" href="index.php?controller=AdminModules&configure=tea_megamenu&token='+json.vals.token+'&editMenugroup&id_teamegamenu_group='+json.vals.id+'"><i class="icon-pencil"></i>'+edit_text+'</a>';
                                        html +='<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>';
                                        html +='<ul class="dropdown-menu">';
                                            html +='<li><a href="index.php?controller=AdminModules&configure=tea_megamenu&token='+json.vals.token+'&viewMenuGroup&id_teamegamenu_group='+json.vals.id+'" title="'+add_item_text+'"><i class="icon-copy"></i> '+add_item_text+'</a></li>';
                                            html +='<li class="divider"></li>';
    									    html +='<li>';
    										html +='<a class="delete_group_menu" href="index.php?controller=AdminModules&configure=tea_megamenu&token='+json.vals.token+'&deleteMenuGroup&id_teamegamenu_group='+json.vals.id+'" title="'+delete_text+'"><i class="icon-trash"></i>'+delete_text+'</a>';
    									    html +='</li>' ;  
								        html +='</ul>';
                                      html +='</div>';  
                                    html +='</div>';
                                html +='</td>'; 
                            html +='</tr>';
                            $('#list-menus > #gird_items').append(html);
                        }
                        else
                        {
                            $.growl.notice({ message: successful_update });
                            $('#list-menus > #gird_items #item_'+json.vals.id+' .title').html(json.vals.title);
                            $('#list-menus > #gird_items #item_'+json.vals.id+' .display-hook').html(json.vals.hook);
                            $('#list-menus > #gird_items #item_'+json.vals.id+' .status').html('<a href="index.php?controller=AdminModules&configure=tea_megamenu&token='+json.vals.token+'&changeStatusMenuGroup&id_teamegamenu_group='+json.vals.id+'&status='+json.vals.status+'">'+(json.vals.status==0?'<i class="icon-remove"></i>':'<i class="icon-check"></i>')+'</a>');
                        }   
                    }
                },
                error: function(xhr, status, error)
                {
                    var err = eval("(" + xhr.responseText + ")");     
                    $.growl.error({ message: err.Message });               
                }
            });         
   });
   $(document).on('click','button[name="submitMenu"]',function(e){ 
            e.preventDefault();
            tinyMCE.triggerSave();
            var formData = new FormData($(this).parents('form').get(0));
            $.ajax({
                url: $(this).parents('form').eq(0).attr('action'),
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(json){
                    if(json.error)
                    {
                        $.growl.error({ message:json.errors });
                    }                
                    else
                    {
                        $('#block-form').hide();
                        if($('#teamegamenu #list_'+json.vals.id).length <=0 )
                        {
                            $.growl.notice({ message: successful_creation });
                            var html ='<li id="list_'+json.vals.id+'" class="spmenu-item" data-id="'+json.vals.id+'">';
                                    html +='<div class="spmenu-handle"></div>';
                                    html +='<div class="spmenu-content">';
                                        html +='<div class="col-md-6">';
                                            html +='<h4 class="pull-left title"> #'+json.vals.id+' - '+json.vals.title+' </h4>';
                                        html +='</div>';
                                        html +='<div class="col-md-6">';
                                            html +='<div class="btn-group-action pull-right">';
                                                html +='<a class="btn '+(!json.vals.active?'btn-danger':'btn-success')+'" title="'+(!json.vals.active?disable_text:enabled_text)+'" href="index.php?controller=AdminModules&configure=tea_megamenu&token='+json.vals.token+'&changeStatus&id_teamegamenu='+json.vals.id+'&viewMenuGroup&id_teamegamenu_group='+json.vals.id_group+'"><i class="'+(!json.vals.active?'icon-remove':'icon-check')+'"></i>'+enabled_text+'</a>';
                                                html +='<a class="btn btn-default edit_menu" href="index.php?controller=AdminModules&token='+json.vals.token+'&configure=tea_megamenu&id_teamegamenu='+json.vals.id+'&editMenuItem&id_teamegamenu_group='+json.vals.id_group+'"><i class="icon-edit"></i>'+edit_text+'</a>';
                                                html +='<a class="btn btn-default btn-danger remove-menu" href="index.php?controller=AdminModules&token='+json.vals.token+'&configure=tea_megamenu&delete_id_teamegamenu='+json.vals.id+'&deleteMenuItem&id_teamegamenu_group='+json.vals.id_group+'"><i class="icon-trash"></i>'+delete_text+'</a>';
                                            html +='</div>';
                                        html +='</div>';
                                    html +='</div>';
                            html +='</li>';
                            if(json.vals.id_parent==1)
                            {
                               $('#teamegamenu ol.sp_lesp1.spmenu-list').append(html); 
                            }
                            else
                            {
                                if($('#list_'+json.vals.id_parent+' > ol.spmenu-list').length<=0)
                                    $('#list_'+json.vals.id_parent).append('<ol class="spmenu-list"></ol>');
                                $('#list_'+json.vals.id_parent+' ol.spmenu-list').append(html); 
                            }
                        }
                        else
                        {
                            $.growl.notice({ message: successful_update });
                            if(json.vals.id_parent==1)
                            {
                                $('#teamegamenu ol.sp_lesp1.spmenu-list #list_'+json.vals.id+' h4.title').html('#'+json.vals.id+' - '+json.vals.title);
                                    if(json.vals.active==1)
                                    {
                                       $('#teamegamenu ol.sp_lesp1.spmenu-list #list_'+json.vals.id+' .tea_active').removeClass('btn-danger').addClass('btn-success')
                                       $('#teamegamenu ol.sp_lesp1.spmenu-list #list_'+json.vals.id+' .tea_active').attr('title',enabled_text); 
                                       $('#teamegamenu ol.sp_lesp1.spmenu-list #list_'+json.vals.id+' .tea_active').html('<i class="icon-check")></i>'+enabled_text);
                                    }
                                    else
                                    {
                                        $('#teamegamenu ol.sp_lesp1.spmenu-list #list_'+json.vals.id+' .tea_active').removeClass('btn-success').addClass('btn-danger');
                                        $('#teamegamenu ol.sp_lesp1.spmenu-list #list_'+json.vals.id+' .tea_active').attr('title',disable_text);
                                        $('#teamegamenu ol.sp_lesp1.spmenu-list #list_'+json.vals.id+' .tea_active').html('<i class="icon-remove")></i>'+disable_text); 
                                    }
                                if($('#teamegamenu ol.sp_lesp1.spmenu-list > #list_'+json.vals.id).length<=0)
                                {
                                    var html = $('#teamegamenu #list_'+json.vals.id).clone();
                                    $('#teamegamenu #list_'+json.vals.id).remove();
                                    $('#teamegamenu ol.sp_lesp1.spmenu-list').append(html);
                                }
                            }
                            else
                            {
                                if($('#list_'+json.vals.id_parent+' > ol.spmenu-list').length<=0)

                                    $('#list_'+json.vals.id_parent).append('<ol class="spmenu-list"></ol>');

                                $('#list_'+json.vals.id_parent+' ol.spmenu-list #list_'+json.vals.id+' h4.title').html('#'+json.vals.id+' - '+json.vals.title);
                                if(json.vals.active==1)
                                {
                                   $('#list_'+json.vals.id_parent+' ol.spmenu-list #list_'+json.vals.id+' .tea_active').removeClass('btn-danger').addClass('btn-success');

                                   $('#list_'+json.vals.id_parent+' ol.spmenu-list #list_'+json.vals.id+' .tea_active').attr('title',enabled_text);

                                   $('#list_'+json.vals.id_parent+' ol.spmenu-list #list_'+json.vals.id+' .tea_active').html('<i class="icon-check"></i>'+enabled_text); 

                                }
                                else
                                {
                                    $('#list_'+json.vals.id_parent+' ol.spmenu-list #list_'+json.vals.id+' .tea_active').removeClass('btn-success').addClass('btn-danger');

                                    $('#list_'+json.vals.id_parent+' ol.spmenu-list #list_'+json.vals.id+' .tea_active').attr('title',disable_text);

                                    $('#list_'+json.vals.id_parent+' ol.spmenu-list #list_'+json.vals.id+' .tea_active').html('<i class="icon-remove"></i>'+disable_text);
                                }
                                if($('#list_'+json.vals.id_parent+' ol.spmenu-list > #list_'+json.vals.id).length<=0)
                                {
                                    var html=$('#teamegamenu #list_'+json.vals.id).clone();

                                    $('#teamegamenu #list_'+json.vals.id).remove();

                                    $('#list_'+json.vals.id_parent+' ol.spmenu-list').append(html);

                                    

                                }

                                

                            }

                        }   

                    }

                },

                error: function(xhr, status, error)

                {

                    var err = eval("(" + xhr.responseText + ")");

                    $.growl.error({ message: err.Message });                   

                }

            });         

   });

   $(document).on('click','.delete_group_menu',function(e){

        e.preventDefault();

        $delete_group_menu = $(this);

        var confirm_delete= confirm(delete_question);

        if(confirm_delete)

        {

            $.ajax({

			type: 'POST',

			headers: { "cache-control": "no-cache" },

			url: $(this).attr('href'),

			async: true,

			cache: false,

			dataType : "json",

			data: 'ajax=1',

			success: function(json)

			{

			    if(json.error)

                  $.growl.error({ message: json.errors }); 

                else

                {

                    $.growl.notice({ message: successful_deletion });

                    $('#list-menus #item_'+json.vals.id).remove();

                }   

			}

		});

        }

   });  

});

</script>
<style type="text/css">

 #block-form {

  background-color: rgba(0, 0, 0, 0.7);

  bottom: 0;

  left: 0;

  overflow: scroll;

  position: fixed;

  right: 0;

  top: 0;

  z-index: 9999;

  display: none;

}

#block-form > form {

  float: left;

  left: 50%;

  margin: 10% 0;

  max-width: 100%;

  position: relative;

  transform: translateX(-50%);

  -webkit-transform: translateX(-50%);

  width: 800px;

}

.tea_close_popup {

  bottom: 0;

  cursor: pointer;

  left: 0;

  opacity: 0;

  position: fixed;

  right: 0;

  top: 0;

}



</style>