{if $teamegamenu != ''}
	<div id="tea_megamenu_left" class="teamegamenu">
		<nav class="navbar-default">
			<div id="tea-megamenu" class="{$teamegamenu_style} tea-megamenu clearfix">
				<span id="remove-megamenu" class="icon-remove"></span>
				<button class="navbar-brand">
					<span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
				</button>
				{$teamegamenu nofilter}
			</div>
		</nav>
	</div>
{/if}

