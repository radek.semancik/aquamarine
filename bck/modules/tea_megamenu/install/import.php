<?php
include_once(_PS_MODULE_DIR_.'tea_megamenu/Megamenu.php');
include_once(_PS_MODULE_DIR_.'tea_megamenu/MegamenuGroup.php');
class TeaImport extends Module
{
	public	function __construct()
	{
		$this->name = 'tea_megamenu';
		parent::__construct();
	}
    public function importMenu()
    {
        $context = Context::getContext();
		$shop_id = $context->shop->id;
        $languages = Language::getLanguages(false);
        $filename="import.xml";
        if (file_exists(_PS_MODULE_DIR_.'tea_megamenu/xml/'.$filename))	
		{	
			$xml = simplexml_load_file(_PS_MODULE_DIR_.'tea_megamenu/xml/'.$filename);
            foreach($xml->group as $group)
            {
                $megamenuGroup = new MegamenuGroup();
                $megamenuGroup->status= (int)$group['status'];
                $megamenuGroup->position= (int)$group['position'];
                $megamenuGroup->id_shop= $shop_id;
                $megamenuGroup->hook = (string)$group['hook'];
                $megamenuGroup->params = (string)$group['params'];
    			foreach ($languages as $language)
    			{
    				$megamenuGroup->title[$language['id_lang']] = (string)$group['title'];
    			}
                $megamenuGroup->add();
                $id_teamegamenu_group= $megamenuGroup->id;
                foreach($group->menuitem as $menuitem)
                {
                   $mgmenu = new Megamenu();
                   $mgmenu->id_parent=(int)$menuitem['id_parent'];
                   $mgmenu->value= (string)$menuitem['value'];
                   $mgmenu->width = (string)$menuitem['width'];
                   $mgmenu->sub_menu= (string)$menuitem['sub_menu'];
                   $mgmenu->sub_width = (string)$menuitem['sub_width']; 
                   $mgmenu->sub_position=(string)$menuitem['sub_position'];
                   $mgmenu->type= (string)$menuitem['type'];
                   $mgmenu->show_title= (int)$menuitem['show_title'];
                   $mgmenu->sp_lesp= (int)$menuitem['sp_lesp'];
                   $mgmenu->active=(int)$menuitem['active'];
                   $mgmenu->id_teamegamenu_group=$id_teamegamenu_group;
                   $mgmenu->position= (int)$menuitem['position'];
                   $mgmenu->menu_class= (string)$menuitem['menu_class'];
                   $mgmenu->cat_subcategories= (string)$menuitem['cat_subcategories'];
                   foreach ($languages as $language)
                   {
                    	$mgmenu->title[$language['id_lang']] = (string)$menuitem['title'];
                        $mgmenu->url[$language['id_lang']] = (string)$menuitem['url'];
                        $mgmenu->html[$language['id_lang']] = (string)$menuitem->htmlData[0];
                   }
                   $mgmenu->id_shop= $shop_id;
                   $mgmenu->add();
                }
            }
        }
    }
    
}
?>