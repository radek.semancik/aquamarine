<?php

include_once(_PS_MODULE_DIR_.'tea_megamenu/Megamenu.php');

include_once(_PS_MODULE_DIR_.'tea_megamenu/MegamenuGroup.php');

class TeaExport extends Module

{

	public	function __construct()

	{

		$this->name = 'tea_megamenu';

		parent::__construct();

	}

    public function getMenuItems ($id_group,$id_shop=null)

    {

        if(!$id_shop)
            $id_shop=(int)$this->context->shop->id;
        $sql ='SELECT * FROM `'._DB_PREFIX_.'teamegamenu` t,'._DB_PREFIX_.'teamegamenu_shop ts,'._DB_PREFIX_.'teamegamenu_lang tl WHERE  t.id_teamegamenu = ts.id_teamegamenu and t.id_teamegamenu=tl.id_teamegamenu and ts.id_shop='.(int)$id_shop.' and tl.id_lang=1 AND t.id_teamegamenu_group='.(int)$id_group.' AND t.id_teamegamenu!=1 ORDER BY t.id_teamegamenu ASC'; 
        return Db::getInstance()->executeS($sql);

    }

    public function exportMenus()

    {

        $sql ='SELECT * FROM `'._DB_PREFIX_.'teamegamenu_group` g, '._DB_PREFIX_.'teamegamenu_group_shop gs, '._DB_PREFIX_.'teamegamenu_group_lang gl WHERE g.id_teamegamenu_group= gs.id_teamegamenu_group AND g.id_teamegamenu_group=gl.id_teamegamenu_group AND gs.id_shop='.(int)$this->context->shop->id.' AND gl.id_lang=1';

        $groups = Db::getInstance()->executeS($sql);
        
        if($groups)
        {
            header('Content-type: text/xml');
    		header('Content-Disposition: attachment; filename="import.xml"');	
    		$xml_output = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
            $xml_output .= '<entity_profile>'."\n";	
            foreach($groups as $group)
            {
                $xml_output .="<group hook='".$group['hook']."' params='".$group['params']."' status='".$group['status']."' position='".$group['position']."' title='".$group['title']."'>"."\n";
                $items= $this->getMenuItems($group['id_teamegamenu_group']);
                if($items)
                {
                    foreach($items as $item)
                    {
                        $xml_output .="<menuitem id_parent='".$item['id_parent']."' value= '".addslashes($item['value'])."' type='".$item['type']."' width='".$item['width']."' menu_class='".$item['menu_class']."' show_title='".$item['show_title']."'  sub_menu='".$item['sub_menu']."' sub_width='".$item['sub_width']."' lesp ='".$item['lesp']."' cat_subcategories='".$item['cat_subcategories']."'  sp_lesp='".$item['sp_lesp']."' position='".$item['position']."' active='".$item['active']."' id_teamegamenu='".$item['id_teamegamenu']."' title='".$item['title']."' sub_position='".$item['sub_position']."' url='".$item['url']."' >"."\n";
                        $xml_output .="<htmlData><![CDATA[".$item['html']."]]></htmlData>"."\n";
                        $xml_output .="</menuitem>"."\n";
                    } 
                }
                $xml_output .='</group>'."\n";
            }
            $xml_output .='</entity_profile>'."\n";
            echo $xml_output; 
            exit; 
        }
    }
}

?>