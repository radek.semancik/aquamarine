{if isset($comparator_max_item) && $comparator_max_item}
	<div class="compare">
		<a class="add_to_compare {if $checked}checked{/if}" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}">
            <i class="show_not_check pe-7s-graph3"></i>
            <i class="show_when_check pe-7s-close"></i>
        </a>
	</div>
{/if}