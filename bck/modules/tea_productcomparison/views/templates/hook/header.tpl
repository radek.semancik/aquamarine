<script type="text/javascript">
    var min_item ='{l s='Please select at least one product' mod='tea_productcomparison'}';
    var text_add_compare ='{l s='Add compare successful' mod='tea_productcomparison'}';
    var text_delete_compare ='{l s='Delete compare successful' mod='tea_productcomparison'}';
    var max_item= '{l s='You cannot add more than' mod='tea_productcomparison'} '+{$comparator_max_item|intval}+' {l s='product(s) to the product comparison' mod='tea_productcomparison'}';
    var comparator_max_item ={$comparator_max_item|intval};
    var comparedProductsIds =[{$comparedProductsIds}];
    var ulr_compare = '{$link->getModuleLink('tea_productcomparison')|escape:'html':'UTF-8'}';  
</script>