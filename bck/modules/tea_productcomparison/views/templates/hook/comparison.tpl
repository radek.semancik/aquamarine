{if $comparator_max_item}
	<form method="post" action="{$link->getModuleLink('tea_productcomparison')|escape:'html':'UTF-8'}" class="compare-form">
		<button type="submit" class="btn btn-default button button-medium bt_compare bt_compare{if isset($paginationId)}_{$paginationId}{/if}" disabled="disabled">
			<i class="pe-7s-graph3"></i><span class="total-compare-val">{count($compared_products)}</span>
		</button>
		<input type="hidden" name="compare_product_count" class="compare_product_count" value="{count($compared_products)}" />
		<input type="hidden" name="compare_product_list" class="compare_product_list" value="" />
	</form>
{/if}