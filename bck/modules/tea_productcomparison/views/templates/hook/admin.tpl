<div class="row">
	<div class="col-lg-12">
        <form novalidate="" enctype="multipart/form-data" method="post" action="" class="defaultForm form-horizontal AdminStatuses" id="order_state_form">
			 <div id="fieldset_0" class="panel">
                <div class="panel-heading">
				    <i class="icon-time"></i> {l s='Order status' mod='informationtax'}
				</div>
                <div class="form-wrapper">
                    <p>{l s='Select order status which will be used to calculate information for tax' mod='informationtax'}</p>
                    {foreach from=$order_status item='order_statu'}  
                    <div class="form-group">
				        <div class="col-lg-9 col-lg-offset-3">
                            <div class="checkbox">
    				            <label style="padding-left:0px" for="order_status_{$order_statu.id_order_state}"><input type="checkbox" value="{$order_statu.id_order_state}" class="" id="order_status_{$order_statu.id_order_state}" name="order_status[]" {if in_array($order_statu.id_order_state,$information_in_status)}checked="checked"{/if} />{$order_statu.name}</label>
    				        </div>				
				        </div>
	               </div>
                   {/foreach}
                </div>
                <div class="panel-footer">
    				<button class="btn btn-default pull-right" name="submitAddorder_state_information" id="order_state_form_submit_btn" value="1" type="submit">
    				    <i class="process-icon-save"></i> {l s='Save'}
    				</button>
				</div>
             </div>	
        </form> 
    </div>
</div>