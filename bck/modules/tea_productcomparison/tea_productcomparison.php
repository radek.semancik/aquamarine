<?php
if (!defined('_PS_VERSION_'))
	exit;
class Tea_productcomparison extends Module
{
	private $_hooks = array(       
		'header',
		'displayProductListFunctionalButtons',
        'displayTopListProductExtra'
    );
    public $_html;
    public function __construct()
	{
	    $this->name = 'tea_productcomparison';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'prestagold.com';
		$this->need_instance = 0;
	 	parent::__construct();
		$this->displayName = $this->l('Product comparison');
		$this->description = $this->l('Product comparison in prestashop 1.7');
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall module?');
        $this->bootstrap = true;
        $compared_products = array();
        if (isset($this->context->cookie->id_compare)) {
            $compared_products = self::getCompareProducts($this->context->cookie->id_compare);
        }
        $comparedProductsIds='';
        if($compared_products)
            foreach($compared_products as $id_product)
            {
                $comparedProductsIds .=$id_product.',';
            }
        $this->context->smarty->assign(array(
            'compared_products'=>$compared_products,
            'comparator_max_item'  => (int)Configuration::get('PS_COMPARATOR_MAX_ITEM'),
            'comparedProductsIds' =>trim($comparedProductsIds,',')
        ));
    }
    public function install()
	{
		if (!parent::install())
			return false;
		foreach ($this->_hooks as $hook) {
            if(!$this->registerHook($hook)) return false;
        }
        
		return $this->installDb();
	}     
    public function uninstall()
	{
		if (!parent::uninstall()) return false;
		  return $this->uninstallDb();
	}
    public function installDb()
    {
        $res = (bool)Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'compare` (
				`id_compare` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_customer` int(10) unsigned NOT NULL,
				PRIMARY KEY (`id_compare`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');
		$res &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'compare_product` (
			  `id_compare` int(10) unsigned NOT NULL,
			  `id_product` int(10) unsigned NOT NULL DEFAULT \'0\',
			  `date_add` datetime NOT NULL,
              `date_upd` datetime NOT NULL,
			  PRIMARY KEY (`id_compare`,`id_product`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');
        Configuration::updateValue('PS_COMPARATOR_MAX_ITEM',4);
        return $res;
    }
    public function uninstallDb()
    {
        Configuration::deleteByName('PS_COMPARATOR_MAX_ITEM');
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'compare`, `'._DB_PREFIX_.'compare_product`');
    }
    public function getContent()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postProcess();
		}
		else
			$this->_html .= '<br />';
		$this->_html .= $this->renderForm();
		return $this->_html;
	}
    protected function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			Configuration::updateValue('PS_COMPARATOR_MAX_ITEM', (int)Tools::getValue('PS_COMPARATOR_MAX_ITEM'));
		}
		$this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}
    public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Configuration'),
					'icon' => 'icon-envelope'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Comparatior max item'),
						'name' => 'PS_COMPARATOR_MAX_ITEM',
						'required' => true
					)
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->id = (int)Tools::getValue('id_carrier');
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'btnSubmit';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		return array(
			'PS_COMPARATOR_MAX_ITEM' => (int)Tools::getValue('PS_COMPARATOR_MAX_ITEM', Configuration::get('PS_COMPARATOR_MAX_ITEM')),
		);
	}
	public function hookHeader($params)
	{
		$this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/comparator.css', 'all');
        $this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/products-comparison.js');
        return $this->display(__FILE__,'header.tpl');
	}
   	public function hookDisplayProductListFunctionalButtons($params)
	{
		
        $compared_products = array();
        if (isset($this->context->cookie->id_compare)) {
            $compared_products = self::getCompareProducts($this->context->cookie->id_compare);
        }
        $checked=false;
        if($compared_products)
        {
            foreach($compared_products as $id_product)
                if($params['product']['id_product']==$id_product)
                {
                    $checked=true;
                    break;
                }
        }
        $this->context->smarty->assign(
            array(
                'product'=>$params['product'],
                'checked'=>$checked,
            )
        );
		return $this->display(__FILE__, 'comparison_button.tpl');
	}
    public function hookDisplayTopListProductExtra($params)
    {
        $this->context->smarty->assign(
            array(
                'link'=>$this->context->link,
            )        
        );
        return $this->display(__FILE__,'comparison.tpl');
    }
    public static function getCompareProducts($id_compare)
    {
        $results = Db::getInstance()->executeS('
		SELECT DISTINCT `id_product`
		FROM `'._DB_PREFIX_.'compare` c
		LEFT JOIN `'._DB_PREFIX_.'compare_product` cp ON (cp.`id_compare` = c.`id_compare`)
		WHERE cp.`id_compare` = '.(int)($id_compare));

        $compareProducts = null;

        if ($results) {
            foreach ($results as $result) {
                $compareProducts[] = (int)$result['id_product'];
            }
        }
        return $compareProducts;
    }
    /**
     * Add a compare product for the customer
     * @param int $id_customer, int $id_product
     * @return bool
     */
    public static function addCompareProduct($id_compare, $id_product)
    {
        // Check if compare row exists
        $id_compare = Db::getInstance()->getValue('
			SELECT `id_compare`
			FROM `'._DB_PREFIX_.'compare`
			WHERE `id_compare` = '.(int)$id_compare);

        if (!$id_compare) {
            $id_customer = false;
            if (Context::getContext()->customer) {
                $id_customer = Context::getContext()->customer->id;
            }
            $sql = Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'compare` (`id_compare`, `id_customer`) VALUES (NULL, "'.($id_customer ? $id_customer: '0').'")');
            if ($sql) {
                $id_compare = Db::getInstance()->getValue('SELECT MAX(`id_compare`) FROM `'._DB_PREFIX_.'compare`');
                Context::getContext()->cookie->id_compare = $id_compare;
            }
        }

        return Db::getInstance()->execute('
			INSERT IGNORE INTO `'._DB_PREFIX_.'compare_product` (`id_compare`, `id_product`, `date_add`, `date_upd`)
			VALUES ('.(int)($id_compare).', '.(int)($id_product).', NOW(), NOW())');
    }

    /**
     * Remove a compare product for the customer
     * @param int $id_compare
     * @param int $id_product
     * @return bool
     */
    public static function removeCompareProduct($id_compare, $id_product)
    {
        return Db::getInstance()->execute('
		DELETE cp FROM `'._DB_PREFIX_.'compare_product` cp, `'._DB_PREFIX_.'compare` c
		WHERE cp.`id_compare`=c.`id_compare`
		AND cp.`id_product` = '.(int)$id_product.'
		AND c.`id_compare` = '.(int)$id_compare);
    }

    /**
     * Get the number of compare products of the customer
     * @param int $id_compare
     * @return int
     */
    public static function getNumberProducts($id_compare)
    {
        return (int)(Db::getInstance()->getValue('
			SELECT count(`id_compare`)
			FROM `'._DB_PREFIX_.'compare_product`
			WHERE `id_compare` = '.(int)($id_compare)));
    }


    /**
     * Clean entries which are older than the period
     * @param string $period
     * @return void
     */
    public static function cleanCompareProducts($period = null)
    {
        if ($period !== null) {
            Tools::displayParameterAsDeprecated('period');
        }

        Db::getInstance()->execute('
        DELETE cp, c FROM `'._DB_PREFIX_.'compare_product` cp, `'._DB_PREFIX_.'compare` c
        WHERE cp.date_upd < DATE_SUB(NOW(), INTERVAL 1 WEEK) AND c.`id_compare`=cp.`id_compare`');
    }

    /**
     * Get the id_compare by id_customer
     * @param int $id_customer
     * @return int $id_compare
     */
    public static function getIdCompareByIdCustomer($id_customer)
    {
        return (int)Db::getInstance()->getValue('
		SELECT `id_compare`
		FROM `'._DB_PREFIX_.'compare`
		WHERE `id_customer`= '.(int)$id_customer);
    }
     public static function getFeaturesForComparison($list_ids_product, $id_lang)
    {
        if (!Feature::isFeatureActive()) {
            return false;
        }

        $ids = '';
        foreach ($list_ids_product as $id) {
            $ids .= (int)$id.',';
        }

        $ids = rtrim($ids, ',');

        if (empty($ids)) {
            return false;
        }

        return Db::getInstance()->executeS('
			SELECT f.*, fl.*
			FROM `'._DB_PREFIX_.'feature` f
			LEFT JOIN `'._DB_PREFIX_.'feature_product` fp
				ON f.`id_feature` = fp.`id_feature`
			LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl
				ON f.`id_feature` = fl.`id_feature`
			WHERE fp.`id_product` IN ('.$ids.')
			AND `id_lang` = '.(int)$id_lang.'
			GROUP BY f.`id_feature`
			ORDER BY f.`position` ASC
		');
    }
}