<?php
class Tea_customattributeAjaxModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    /**
    * @see FrontController::initContent()
    */
    public function initContent()
    {
        $id_product=(int)Tools::getValue('id_product');
        $id_product_attribute = (int)Product::getIdProductAttributesByIdAttributes($id_product, Tools::getValue('group'));
        $price = Product::getPriceStatic($id_product,true,$id_product_attribute,6,null,false,false);
        $price_discount = Product::getPriceStatic($id_product,true,$id_product_attribute,6,null,false,true);
        ob_end_clean();
        header('Content-Type: application/json');
        die(json_encode([
            'success' => true,
            'modal'   => $modal
        ]));
    }
}