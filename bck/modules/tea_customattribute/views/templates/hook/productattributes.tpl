<div class="product-list-variants">
    <div class="product-attribute">
      {foreach from=$groups key=id_attribute_group item=group}
        <div class="clearfix product-variants-item">
          <h4 class="control-label panel-product-label">{$group.name}</h4>
          <div class="attribute-values">
          {if $group.group_type == 'select'}
            <select class="data_product_attribute"
              {*id="group_{$id_attribute_group}"*}
              data-product-attribute="{$id_attribute_group}"
              name="group[{$id_attribute_group}]">
              {foreach from=$group.attributes key=id_attribute item=group_attribute}
                <option value="{$id_attribute}" title="{$group_attribute.name}"{if $group_attribute.selected} selected="selected"{/if}>{$group_attribute.name}</option>
              {/foreach}
            </select>
          {elseif $group.group_type == 'color'}
            <ul {*id="group_{$id_attribute_group}"*} class="panel-product-line panel-product-colors">
              {foreach from=$group.attributes key=id_attribute item=group_attribute}
                <li title="{$group_attribute.name}" class="pull-xs-left input-container">
                  <input class="input-color data_product_attribute" type="radio" data-product-attribute="{$id_attribute_group}" name="group[{$id_attribute_group}]" value="{$id_attribute}"{if $group_attribute.selected} checked="checked"{/if} />
                  <span title="{$group_attribute.name}"
                    {if $group_attribute.html_color_code}class="color" style="background-color: {$group_attribute.html_color_code}" {/if}
                    {if $group_attribute.texture}class="color texture" style="background-image: url({$group_attribute.texture})" {/if}
                  ><span class="sr-only">{$group_attribute.name}</span></span>
                </li>
              {/foreach}
            </ul>
          {elseif $group.group_type == 'radio'}
            <ul {*id="group_{$id_attribute_group}"*}>
              {foreach from=$group.attributes key=id_attribute item=group_attribute}
                <li title="{$group_attribute.name}" class="input-container pull-xs-left">
                  <input class="input-radio data_product_attribute" type="radio" data-product-attribute="{$id_attribute_group}" name="group[{$id_attribute_group}]" value="{$id_attribute}"{if $group_attribute.selected} checked="checked"{/if}>
                  <span class="radio-label" title="{$group_attribute.name}">{$group_attribute.name}</span>
                </li>
              {/foreach}
            </ul>
          {/if}
          </div>
        </div>
      {/foreach}
    </div>
</div>
<div class="block_price_ajax" style="display:none"></div>
<input type="hidden" value="1" name="ajax" />
<input type="hidden" value="productrefresh" name="action" />


