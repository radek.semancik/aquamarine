$(document).ready(function(){
  $(document).on('change','.product-list-variants select,.product-list-variants input',function(){
        var formData = new FormData($(this).parents('form').get(0));
        var $form = $(this).closest('form');
        var $product_miniature = $(this).closest('.product-miniature');
        $.ajax({
            url: $form.attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(json){
                 $.ajax({
                    url: json.productUrl,
                    headers: { "cache-control": "no-cache" },
                    data: 'ajax=1&action=refresh',
                    async: true,
				    cache: false,
                    type: 'post',
                    dataType: 'json',
                    success: function(jsonData){
                        $form.find('.product-list-variants').html(jsonData.product_variants); 
                        $form.find('.product-list-variants >.product-variants').removeClass('product-variants');
                        $form.find('.block_price_ajax').html(jsonData.product_prices);
                        $product_miniature.find('.product-item-price').html($form.find('.block_price_ajax .current-price').html());
                        $form.find('.block_price_ajax').html('');                      
                    },
                    error: function(xhr, status, error)
                    {
                        var err = eval("(" + xhr.responseText + ")");
                        alert(err.Message);                    
                    }            
                });                            
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);                    
            }            
        });
  }); 
});