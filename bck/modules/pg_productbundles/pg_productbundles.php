<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @version  Release: $Revision$
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;
use PrestaShop\PrestaShop\Adapter\Cart\CartPresenter;
class Pg_productbundles extends Module
{
    private $_html = '';
    public $is17 = false;
    public $animations;
    public $effects;
    public $googlefonts = array();
    public function __construct()
	{
        $this->name = 'pg_productbundles';
		$this->tab = 'front_office_features';
		$this->version = '1.0.2';
		$this->author = 'prestagold.com';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		$this->bootstrap = true;
        if(version_compare(_PS_VERSION_, '1.7', '>='))
            $this->is17 = true; 
        $this->module_key = 'da314fdf1af6d043f9b2f15dce2bef1e';
		parent::__construct();
		$this->displayName = $this->l('Product bundles');
		$this->description = $this->l('Product bundles');
		$this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => _PS_VERSION_);
    }
    public function install()
	{
	    return parent::install()&& $this->registerHook('displayHeader') && $this->registerHook('displayBackOfficeHeader')&& $this->registerHook('displayFooter') && $this->registerHook('displayAdminProductsExtra') && $this->registerHook('displayProductAdditionalInfo')&& $this->registerHook('displayLeftColumnProduct')&& $this->registerHook('actionProductUpdate')&& $this->registerHook('top') && $this->registerHook('displayFooterProduct')&& $this->registerHook('displayCustomization')&& $this->_installDb();
    }    
    /**
	 * @see Module::uninstall()
	 */
	public function uninstall()
	{
        return parent::uninstall() && $this->_uninstallDb();
    }
    private function _installDb()
    {
        return Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pg_productbundles` (
          `id_pg_productbundles` int(11) NOT NULL AUTO_INCREMENT,
          `id_product` int(11) NOT NULL,
          `id_product_bundles` int(11) NOT NULL,
          `id_product_attribute_bundles` int(11) NOT NULL,
          `proudct_bundles_discount` float(10,2) NOT NULL,
          PRIMARY KEY (`id_pg_productbundles`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1'
        ) && Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pg_productbundles_combination` (
          `id_pg_productbundles_combination` int(11) NOT NULL AUTO_INCREMENT,
          `id_customization` int(11) NOT NULL,
          `id_product` int(11) NOT NULL,
          `id_product_attribute` int(11) NOT NULL,
          `name` varchar(222) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
          `old_price` float(10,2) NOT NULL,
          `price` float(10,2) NOT NULL,
          PRIMARY KEY (`id_pg_productbundles_combination`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1');
    }
    private function _uninstallDb()
    {      
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'pg_productbundles`, `'._DB_PREFIX_.'pg_productbundles_combination`');
    }
    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'views/css/productbundles_frontend.css');
        $this->context->controller->addJS($this->_path.'views/js/productbundles.js');
        if(Tools::getValue('convertPriceSmarty'))
        {
            die(Tools::jsonEncode(
                array(
                    'price'=> Tools::displayPrice(Tools::getValue('price')),
                )
            ));
        }
    }
    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addJquery();
        $id_product= Tools::getValue('id_product');
        $this->context->controller->addCSS($this->_path.'views/css/admin.css');
        $this->context->controller->addJS($this->_path.'views/js/admin.js');
    }
    public function getContent()
	{
	   if(Tools::isSubmit('add_proudct_bundles')&& Tools::getValue('id_product_bundles') && Tools::getValue('id_product'))
       {
            $id_product= Tools::getValue('id_product');
            $id_product_bundles = Tools::getValue('id_product_bundles');
            $product= new Product($id_product_bundles,true,$this->context->language->id);
            $id_product_attribute_bundles= Tools::getValue('id_product_attribute_bundles');
            $proudct_bundles_discount = Tools::getValue('proudct_bundles_discount');
            $id_pg_productbundles=0;
            if($id_pg_productbundles=(int)Db::getInstance()->getValue('SELECT id_pg_productbundles FROM '._DB_PREFIX_.'pg_productbundles WHERE id_product="'.(int)$id_product.'" AND id_product_bundles="'.(int)$id_product_bundles.'" AND id_product_attribute_bundles="'.(int)$id_product_attribute_bundles.'"'))
            {
               Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'pg_productbundles set proudct_bundles_discount="'.(float)$proudct_bundles_discount.'" WHERE id_pg_productbundles='.(int)$id_pg_productbundles);  
            }
            else
            {
                Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'pg_productbundles(id_product,id_product_bundles,id_product_attribute_bundles,proudct_bundles_discount) VALUES("'.(int)$id_product.'","'.(int)$id_product_bundles.'","'.(int)$id_product_attribute_bundles.'","'.(float)$proudct_bundles_discount.'")');
                $id_pg_productbundles = Db::getInstance()->Insert_ID();
            }
            die(Tools::jsonEncode(
                array(
                    'id_pg_productbundles'=>$id_pg_productbundles,
                    'product_name' => $product->name,
                    'product_attribute'=> $this->getAllAttributes($id_product_attribute_bundles)['attributes'],
                    'proudct_bundles_discount' => (float)$proudct_bundles_discount,
                )
            ));
       }
       if(Tools::isSubmit('delete_proudct_bundles') && $id_pg_productbundles=(int)Tools::getValue('id_pg_productbundles'))
       {
            Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'pg_productbundles where id_pg_productbundles='.(int)$id_pg_productbundles);
            die('1');
       }
       $productBundles = Db::getInstance()->executeS('
        SELECT * FROM '._DB_PREFIX_.'pg_productbundles pb,'._DB_PREFIX_.'product p
        LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (p.id_product=pl.id_product AND pl.id_lang="'.(int)$this->context->language->id.'")
        WHERE p.active=1 AND pb.id_product =p.id_product GROUP BY pb.id_product');
       if($productBundles)
       {
            foreach($productBundles as &$productBundle)
            {
                $id_image = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int)$productBundle['id_product'].' AND cover=1');
                if(version_compare(_PS_VERSION_, '1.7', '>='))
                    $type_image= ImageType::getFormattedName('small');
                else
                    $type_image= ImageType::getFormatedName('small');
                $url_image = Context::getContext()->link->getImageLink($productBundle['link_rewrite'],$id_image,$type_image);
                $productBundle['url_image']=$url_image;
                $productBundle['url_product'] = $this->context->link->getAdminLink('AdminProducts',true,array('id_product'=>$productBundle['id_product']));
            }
       }
       $this->context->smarty->assign(
            array(
                'productBundles' =>$productBundles,
                'link_listproduct' =>$this->context->link->getAdminLink('AdminProducts',true),
            )
       );
       return $this->display(__FILE__,'admin.tpl');
    }
    public function hookDisplayAdminProductsExtra($param)
    {
        $id_product = ($this->is17? (int)$param['id_product'] : (int)Tools::getValue('id_product'));
        $this->context->smarty->assign(
            array(
                'bundles_products' => $this->getproductbundles($id_product),
                'url_search_product_ajax'=>Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/pg_productbundles/ajax_product_list.php',
                'PG_PB_MODULE_URL_AJAX'=> $this->context->link->getAdminLink('AdminModules',true).'&configure='.$this->name,
            )
        );        
        return $this->display(__FILE__,'product_extra.tpl');
    }
    public function getproductbundles($id_product)
    {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'pg_productbundles pb
        INNER JOIN '._DB_PREFIX_.'product p ON (pb.id_product_bundles=p.id_product)
        LEFT JOIN '._DB_PREFIX_.'product_lang pl ON ( pb.id_product_bundles=pl.id_product AND pl.id_lang="'.(int)$this->context->language->id.'") WHERE pb.id_product ="'.(int)$id_product.'" AND p.active=1 GROUP BY id_product_bundles,id_product_attribute_bundles';
        $products = Db::getInstance()->executeS($sql);
        $id_customer = ($this->context->customer->id) ? (int)($this->context->customer->id) : 0;
        $id_group = null;
        if ($id_customer) {
            $id_group = Customer::getDefaultGroupId((int)$id_customer);
        }
        if (!$id_group) {
            $id_group = (int)Group::getCurrent()->id;
        }
        $group= new Group($id_group);
        if($group->price_display_method)
            $tax=false;
        else
            $tax=true;
        if($products)
        {
            $total_price=0;
            $total_price_old=0;
            foreach($products as &$product)
            {
                $attributes= $this->getAllAttributes($product['id_product_attribute_bundles']);
                $product['attributes'] =$attributes['attributes'];
                $product_class = new Product($product['id_product_bundles'], true, $this->context->language->id, $this->context->shop->id);
                $price = $product_class->getPrice($tax,null);
                $total_price_old = $total_price_old + $price;
                if($product['proudct_bundles_discount'])
                {
                    $product['price_old'] = Tools::displayPrice($price);
                    $product['price_old_rl'] = $price;
                    $price= $price-$price*$product['proudct_bundles_discount']/100;
                    if($price<0)
                        $price=0;
                    $product['price']=Tools::displayPrice($price);
                    $product['price_rl']=$price;
                }
                else
                {
                    $product['price']=Tools::displayPrice($price);
                    $product['price_old_rl']=$product['price_rl']=$price;
                }
                     
                $total_price = $total_price+$price;
                if($product['id_product_attribute_bundles'])
                {
                     $image=Product::getCombinationImageById($product['id_product_attribute_bundles'],Context::getContext()->language->id);
                     $id_image=$image['id_image'];
                }
                if(!isset($id_image)|| !$id_image)
                    $id_image = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int)$product['id_product_bundles'].' AND cover=1');
                if(version_compare(_PS_VERSION_, '1.7', '>='))
                    $type_image= ImageType::getFormattedName('small');
                else
                    $type_image= ImageType::getFormatedName('small');
                $url_image = Context::getContext()->link->getImageLink($product['link_rewrite'],$id_image,$type_image);
                $product['url_image']=$url_image;
                
            }
            $this->context->smarty->assign(
                array(
                    'total_price' => Tools::displayPrice($total_price),
                    'total_price_old'=> Tools::displayPrice($total_price_old),
                    'total_price_old_rl'=>$total_price_old,
                    'currencyFormat'=> $this->context->currency->format,
                    'currencySign'=> $this->context->currency->sign,
                    'currencyBlank'=>$this->context->currency->blank,
                    'decimals'=>$this->context->currency->decimals,
                    'total_discount' => $total_price_old>$total_price? Tools::displayPrice($total_price_old-$total_price):'',
                )
            );
        }
        return $products;
    }
    public function getAllAttributes($id_product_attribute)
    {
        $attributes='';
        $attributes_small='';
        $id_lang = $this->context->language->id;
        $result = Db::getInstance()->executeS('
			SELECT pac.`id_product_attribute`, agl.`public_name` AS public_group_name, al.`name` AS attribute_name
			FROM `'._DB_PREFIX_.'product_attribute_combination` pac
			LEFT JOIN `'._DB_PREFIX_.'attribute` a ON a.`id_attribute` = pac.`id_attribute`
			LEFT JOIN `'._DB_PREFIX_.'attribute_group` ag ON ag.`id_attribute_group` = a.`id_attribute_group`
			LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al ON (
				a.`id_attribute` = al.`id_attribute`
				AND al.`id_lang` = '.(int)$id_lang.'
			)
			LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl ON (
				ag.`id_attribute_group` = agl.`id_attribute_group`
				AND agl.`id_lang` = '.(int)$id_lang.'
			)
			WHERE pac.`id_product_attribute` = '.(int)$id_product_attribute.'
			ORDER BY ag.`position` ASC, a.`position` ASC'
        );
        foreach ($result as $row) {
            $attributes .= $row['public_group_name'].' : '.$row['attribute_name'].', ';
            $attributes_small .= $row['attribute_name'].', ';
        }
        return array(
            'attributes'=>trim($attributes,', '),
            'attributes_small'=>trim($attributes_small,', '),
        );
    }
    public function hookDisplayProductAdditionalInfo($param)
    {
        $id_product= Tools::getValue('id_product');
        $this->context->smarty->assign(
            array(
                'bundles_products' => $this->getproductbundles($id_product),
                'static_token'=>Tools::getToken(false),
            )
        );
        return $this->display(__FILE__,'pruduct_bundles.tpl');
    }
    public function displayAjaxUpdate($id_product,$id_product_attribute)
    {      
        $cart_url = $this->getCartSummaryURL();
        $assign = array(
            'cart' => (new PrestaShop\PrestaShop\Adapter\Cart\CartPresenter)->present($this->context->cart),
            'refresh_url' => $this->context->link->getModuleLink('ps_shoppingcart', 'ajax', array(), null, null, null, true),
            'cart_url' => $cart_url
        );
        $this->context->smarty->assign($assign);
        $renderPreview = $this->fetch('module:pg_productbundles/views/templates/hook/_shoppingcart.tpl');
        $modal = $this->renderModal($this->context->cart, $id_product,$id_product_attribute);
        die(Tools::jsonEncode(array(
            'preview' => $renderPreview,
            'modal'   => $modal
        )));
    }
    public function renderModal(Cart $cart, $id_product, $id_product_attribute)
    {
        $data = (new CartPresenter)->present($cart);
        $product = null;
        foreach ($data['products'] as $p) {
            if ($p['id_product'] == $id_product && $p['id_product_attribute'] == $id_product_attribute) {
                $product = $p;
                break;
            }
        }

        $this->smarty->assign(array(
            'product' => $product,
            'cart' => $data,
            'cart_url' => $this->getCartSummaryURL(),
        ));

        return $this->fetch('module:pg_productbundles/views/templates/hook/modal.tpl');
    }
    private function getCartSummaryURL()
    {
        return $this->context->link->getPageLink(
            'cart',
            null,
            $this->context->language->id,
            array(
                'action' => 'show'
            ),
            false,
            null,
            true
        );
    }
    public function hookDisplayCustomization($param)
    {
        if($param['customization']['id_module']==$this->id)
        {
            $id_customization = (int)$param['customization']['id_customization'];
            $products = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'pg_productbundles_combination WHERE id_customization='.(int)$id_customization);
            if($products)
            {
                foreach($products as &$product)
                {
                    $product['old_price'] = Tools::displayPrice($product['old_price']);
                    $product['price'] = Tools::displayPrice($product['price']);
                    if($product['id_product_attribute'])
                    {
                        $image=Product::getCombinationImageById($product['id_product_attribute'],Context::getContext()->language->id);
                        $id_image=$image['id_image'];
                        if(!isset($id_image)|| !$id_image)
                            $id_image = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int)$product['id_product'].' AND cover=1');
                        if(version_compare(_PS_VERSION_, '1.7', '>='))
                            $type_image= ImageType::getFormattedName('small');
                        else
                            $type_image= ImageType::getFormatedName('small');
                        $product_class= new Product($product['id_product'],true,$this->context->language->id);
                        $url_image = Context::getContext()->link->getImageLink($product_class->link_rewrite,$id_image,$type_image);
                        $product['url_image'] = $url_image;
                        $product['link'] = $this->context->link->getProductLink($product['id_product']);
                    }
                }
            }
            $this->context->smarty->assign(
                array(
                    'products'=>$products
                )
            );
            return $this->display(__FILE__,'productbundles_cart.tpl');
        }
    }
}