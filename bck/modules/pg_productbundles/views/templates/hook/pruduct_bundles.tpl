<script type="text/javascript">
var pg_static_token ='{$static_token}';
var total_price_old_rl ={if isset($total_price_old_rl)}{$total_price_old_rl|floatval}{else}0{/if};
</script>
{if $bundles_products}
    <div class="pg_productbundles">
        <h4 class="wpa-title">{l s='Buy this bundle' mod='pg_productbundles'}</h4>
        <div class="list-image flxwr">    
            {foreach from =$bundles_products item='product'}
                <div class="item flx alc">
                    <div class="image">
                        <img class="attachment-70x70 size-70x70 wp-post-image" width="70" height="70" alt="" src="{$product.url_image}" />
                    </div>
                    <span class="plus">+</span>
                </div>
            {/foreach}
        </div>
        <div class="list-select px-product-bundles">
            {foreach from =$bundles_products item='product'}
                <div class="item">
                    <input checked="checked" class="pg_product_bundle" data-price-old="{$product['price_old_rl']}" value="{$product['price_old_rl']-$product['price_rl']}" name="pg_product_bundle[]" type="checkbox" data-id-product="{$product.id_product_bundles|intval}" data-id-product-attribute="{$product.id_product_attribute_bundles}"/>
                    <div class="product-name-bundles">{$product.name}</div>
                    {if $product.proudct_bundles_discount}
                        <div class="product-price-old">{$product.price_old}</div><span class="product-price-old-extra">/</span>
                    {/if}
                    <div class="product-price-bundles">{$product.price}</div>
                    {if $product.proudct_bundles_discount} 
                        <div class="product-discount">{$product.proudct_bundles_discount}%</div>
                    {/if}
                </div>
            {/foreach}
        </div>
        {if $total_discount}
        <div class="total price"><strong>{l s='Price for all' mod='pg_productbundles'}</strong>:<span class="current-price">{$total_price}</span>/<span class="current-price-old">{$total_price_old}</span>(save<span class="save-price">{$total_discount}</span>)</div>
        {/if}
        <div class="button-container">
            <a class="pg_ajax_add_to_cart_button btn btn-primary" title="{l s='Add bundle to cart' mod='pg_productbundles'}" rel="nofollow" href="{$link->getModuleLink('pg_productbundles','cart')}">
                <span>{l s='Add bundle to cart' mod='pg_productbundles'}</span>
            </a>
        </div>
    </div>
{/if}