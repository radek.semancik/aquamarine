<div class="product-bundles">
    <h3>{l s='List product bundles' mod='pg_productbundles'}</h3>
    <p class="bundels-link">{l s='Go to' mod='pg_productbundles'}&nbsp;<a href="{$link_listproduct}">{l s='Product page'}</a>&nbsp;{l s='add product bundles' mod='pg_productbundles'}</p>
    {if $productBundles}
        <table class="bundles-table">
            <tr class="table-header">
                <td>{l s='ID product' mod='pg_productbundles'}</td>
                <td>{l s='Product image' mod='pg_productbundles'}</td>
                <td>{l s='Product name' mod='pg_productbundles'}</td>
            </tr>
            {foreach from=$productBundles item='productBundle'}
                <tr class="table-body" title="{l s='View product' mod='pg_productbundles'}" onclick="document.location = '{$productBundle.url_product}'">
                    <td>{$productBundle.id_product|intval}</td>
                    <td><img src="{$productBundle.url_image}" alt="{$productBundle.name}" /></td>
                    <td>{$productBundle.name}</td>
                </tr>
            {/foreach}
        </table>
    {else}
    <p>{l s='No have product bundles' mod='pg_productbundles'}</p>
    {/if}
</div>