{if $products}
<ul class="product-bundle">
    {foreach from=$products item='product'}
        <li class="flex middle-xs">
            <a title="{$product.name}" href="{$product.link}">
                <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" width="80" height="102" sizes="(max-width: 80px) 100vw, 80px" src="{$product.url_image}" />
            </a>
            <div class="product-info">
                <div class="product-name">{$product.name}</div>
                <div class="price-old">{$product.old_price}</div>
                <div class="price">{$product.price}</div>
            </div>
        </li>
    {/foreach}
</ul>
{/if}