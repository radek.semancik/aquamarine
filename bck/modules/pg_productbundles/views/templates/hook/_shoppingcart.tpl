<div id="desktop_cart" class="claue-action">
    {if isset($order_mini_cart) && $order_mini_cart == 'minicart'}<div class="background-overlay"></div>{/if}
  <div class="blockcart cart-preview {if $cart.products_count > 0}active{else}inactive{/if}" data-refresh-url="{$refresh_url}">
    <div class="header">
        <a class="cart-contents" rel="nofollow" href="{$cart_url}">
            <i class="pe-7s-shopbag"></i>
            <span class="cart-products-count pa count">{$cart.products_count}</span>
        </a>
        <div class="body cart-wishlist-content">
              {if isset($order_mini_cart) && $order_mini_cart == 'minicart'}
                    <h3>{l s='Mini Cart' d='Shop.Theme.Catalog'}</h3>
                    <a id="close-cart" href="javascript:void(0);"></a>
              {/if}
              <ul>
                {foreach from=$cart.products item=product}
                  <li class="cart-wishlist-item">{include 'module:ps_shoppingcart/ps_shoppingcart-product-line.tpl' product=$product}</li>
                {/foreach}
              </ul>
              <div class="cart-subtotals">
                {foreach from=$cart.subtotals item="subtotal"}
                  <div class="{$subtotal.type}">
                    <span class="label">{$subtotal.label}</span>
                    <span class="value">{$subtotal.value}</span>
                  </div>
                {/foreach}
              </div>
              <div class="cart-total">
                <span class="label">{$cart.totals.total.label}</span>
                <span class="value">{$cart.totals.total.value}</span>
              </div>
              <div class="cart-wishlist-action">
                  <a class="cart-wishlist-viewcart" href="{$cart_url}">view cart</a>
                  <a class="cart-wishlist-checkout" href="{$urls.pages.order}">check out</a>
                </div>
        </div>
    </div>
  </div>
</div>
{if isset($order_mini_cart) && $order_mini_cart == 'minicart'}
    <script type="text/javascript">
        var order_mini_cart = '{$order_mini_cart}';
    </script>
{/if}