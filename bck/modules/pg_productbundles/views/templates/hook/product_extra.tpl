<script type="text/javascript">
var url_search_product_ajax ='{$url_search_product_ajax}';
var PG_PB_MODULE_URL_AJAX= '{$PG_PB_MODULE_URL_AJAX}';
</script>
<div class="bundles-list">
    <div class="proudct_bundles">
        <ul class="products">
             {if $bundles_products}
                {foreach from=$bundles_products item='product'}
                    <li data-id-productbundles="{$product.id_pg_productbundles|intval}"><img src="{$product.url_image}" /> {$product.name}-{$product.attributes} ({$product.proudct_bundles_discount}%) <span class="delete">{l s='Delete' mod='pg_productbundles'}</span></li>
                {/foreach}
             {/if}
        </ul>
    </div>
    <div class="form-group">
        <input type ="hidden" name="id_product_bundles" id="id_product_bundles" value="0"/>
        <input type ="hidden" name="id_product_attribute_bundles" id="id_product_attribute_bundles" value="0"/>
        <input type="text" placeholder="{l s='Search product' mod='pg_productbundles'}" class="search_product_bundles"/>
        <input type="text" placeholder="{l s='discount (%)' mod='pg_productbundles'}" name="proudct_bundles_discount"/>
        <button type="button" class="add_proudct_bundles">{l s='Add' mod='pg_productbundles'}</button>
    </div>
    <div class="product-resuts">
        <ul class="products"> 
        </ul>
    </div>
</div>
