$(document).ready(function(){
   var id_product=parseInt($('#form_id_product').val()); 
   var ok_ajax = 0;
   $(document).on('keyup focus','.search_product_bundles',function(){
        var search = $(this).val();
        if(search.length >= 3)
        {
            if(ok_ajax!=0)
                ok_ajax.abort();
            $('.product-resuts .products').html('');
            $('.search_product_bundles').removeClass('added');
            $('#id_product_bundles').val('');
            $('#id_product_attribute_bundles').val('');
            ok_ajax = $.ajax({
                url:url_search_product_ajax,
                data:{
                    q: search,
                    limit: 20,
                    timestamp: $.now(),
                    id_product:id_product,
                },
                dataType: 'json',
                type: 'post',
                success: function(json){
                    if(json['items'])
                    {
						$('.product-resuts').show();
                        $.each(json['items'],function(index,item){
                            $('.product-resuts .products').append('<li data-id-product="'+item.id_product+'" data-id-product-attribute="'+(item.id_product_attribute?item.id_product_attribute:0)+'" data-name-product="'+item.name+(item.reference ? ' ('+item.reference+') ' : '')+'-'+item.attributes+'">'+(item.url_image?'<img style="width:50px;" src="'+item.url_image+'"/>':'')+'&nbsp;'+item.id_product+' - '+item.name+(item.reference ? ' ('+item.reference+') ' : '')+'-'+item.attributes+'</li>');
                        });
                    }
					else
						$('.product-resuts').hide();
                }
            });
        }
        else
            $('.product-resuts').hide();
    });
    $(document).on('click','.product-resuts .products li',function(e){
        $('#id_product_bundles').val($(this).attr('data-id-product'));
        $('#id_product_attribute_bundles').val($(this).attr('data-id-product-attribute'));
        $('.search_product_bundles').val($(this).attr('data-name-product'));
        $('.search_product_bundles').addClass('added');
        $('.product-resuts').hide();
    });
    $(document).on('click','.add_proudct_bundles',function(){
        if(!parseInt($('#id_product_bundles').val()))
        {
            alert('No products selected');
        }
        else
        {
            $.ajax({
                url:PG_PB_MODULE_URL_AJAX,
                data:{
                    id_product:id_product,
                    id_product_bundles: $('#id_product_bundles').val(),
                    id_product_attribute_bundles:$('#id_product_attribute_bundles').val(),
                    proudct_bundles_discount:$('input[name="proudct_bundles_discount"]').val(),
                    add_proudct_bundles:1,
                },
                dataType: 'json',
                type: 'post',
                success: function(json){
                    $('.search_product_bundles').val('');
                    $('.search_product_bundles').removeClass('added');
                    $('#id_product_bundles').val('');
                    $('#id_product_attribute_bundles').val('');
                    if($('.proudct_bundles li[data-id-productbundles="'+json.id_pg_productbundles+'"]').length)
                    {
                        $('.proudct_bundles li[data-id-productbundles="'+json.id_pg_productbundles+'"]').html(json.product_name+' - '+json.product_attribute +' ('+json.proudct_bundles_discount+'%) <span clsss="delete">delete</span>');
                    }
                    else
                    {
                        $('.proudct_bundles .products').append('<li data-id-productbundles ="'+json.id_pg_productbundles+'">'+json.product_name+' - '+json.product_attribute +' ('+json.proudct_bundles_discount+'%) <span class="delete">delete</span></li>');
                    }
                }
            });
        }
    });
    $(document).on('click','.proudct_bundles .products .delete',function(e){
        $parent = $(this).parent();
        var id_pg_productbundles= $(this).parent().attr('data-id-productbundles');
        $.ajax({
            url:PG_PB_MODULE_URL_AJAX,
            data:{
                id_pg_productbundles:id_pg_productbundles,
                delete_proudct_bundles:1,
            },
            dataType: 'json',
            type: 'post',
            success: function(json){
                $parent.remove();
            }
        });
    });
});