$(document).on('click','.pg_ajax_add_to_cart_button', function(e){
    e.preventDefault();
    var url_ajax= $(this).attr('href');
    var idProducts = [];
    $('input[name="pg_product_bundle[]"]').each(function(){
        if(!$(this).hasClass('disabled') && $(this).is(':checked')){ 
            idProducts.push({'id_product':$(this).attr('data-id-product'),'id_product_attribute':$(this).attr('data-id-product-attribute'),'qty':1});
        }
    });
    $.ajax({
    		type: 'POST',
    		headers: { "cache-control": "no-cache" },
    		url: url_ajax,
    		async: true,
    		cache: false,
    		dataType : "json",
            data: $('#add-to-cart-or-refresh').serialize()+'&add=1&ajax=true&idProducts='+JSON.stringify(idProducts),
            success: function(jsonData)
            {
                prestashop.blockcart = prestashop.blockcart || {};
                var showModal = prestashop.blockcart.showModal || function (modal) {
                    var $body = $('body');
                    $body.append(modal);
                    $body.one('click', '#blockcart-modal', function (event) {
                        if (event.target.id === 'blockcart-modal') {
                            $(event.target).remove();
                        }
                    });
                };
                $('.blockcart').replaceWith($(jsonData.preview).find('.blockcart'));
                if (jsonData.modal) {
                    if (order_mini_cart != 'minicart'){
                        showModal(jsonData.modal);
                    } else {
                        $('.cart-wishlist-content').addClass('minicart');
                        $('.background-overlay').addClass('visible');
                        $('body').addClass('modal-open'); 
                    }
                    //showModal(jsonData.modal);
                }
            },
            error : function(XMLHttpRequest, textStatus, errorThrown){
                alert("Impossible to add the product to the cart.\n\ntextStatus: '" + textStatus + "'\nerrorThrown: '" + errorThrown + "'\nresponseText:\n" + XMLHttpRequest.responseText);
            }
        });
});
$(document).ready(function(){
   $('.pg_product_bundle').click(function(){
        changeProductBundles();
   }); 
});
function changeProductBundles(){
    if($('.pg_product_bundle').length)
    {
        var price_save= 0;
        var price_old=0;
        $('.pg_product_bundle:checked').each(function(){
                price_save +=parseFloat($(this).val());
                price_old += parseFloat($(this).attr('data-price-old'));
        });
        displayPriceSmarty(price_save,$('.pg_productbundles .save-price'));
        displayPriceSmarty(price_old-price_save,$('.pg_productbundles .current-price'));
        displayPriceSmarty(price_old,$('.pg_productbundles .current-price-old'));
    }
};
function displayPriceSmarty(price,obj)
{
    $.ajax({
    		type: 'POST',
    		headers: { "cache-control": "no-cache" },
    		url: '',
    		async: true,
    		cache: false,
    		dataType : "json",
            data: 'convertPriceSmarty=1&price='+price,
            success: function(jsonData)
            {
                obj.html(jsonData.price);
            },
            error : function(XMLHttpRequest, textStatus, errorThrown){
                alert("Impossible to add the product to the cart.\n\ntextStatus: '" + textStatus + "'\nerrorThrown: '" + errorThrown + "'\nresponseText:\n" + XMLHttpRequest.responseText);
            }
        });
}