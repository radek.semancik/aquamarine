<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  @version  Release: $Revision$
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(__FILE__).'../../../config/config.inc.php');
require_once(dirname(__FILE__).'../../../init.php');
/* Getting cookie or logout */
$query = Tools::getValue('q', false);
if (!$query OR $query == '' OR Tools::strlen($query) < 1)
	die();
/*
 * In the SQL request the "q" param is used entirely to match result in database.
 * In this way if string:"(ref : #ref_pattern#)" is displayed on the return list, 
 * they are no return values just because string:"(ref : #ref_pattern#)" 
 * is not write in the name field of the product.
 * So the ref pattern will be cut for the search request.
 */
if($pos = strpos($query, ' (ref:'))
	$query = Tools::substr($query, 0, $pos);

$excludeIds = Tools::getValue('id_product');

// Excluding downloadable products from packs because download from pack is not supported
$excludeVirtuals = (bool)Tools::getValue('excludeVirtuals', false);
$exclude_packs = (bool)Tools::getValue('exclude_packs', false);
$pg_productbundles = Module::getInstanceByName('pg_productbundles');
$sql = 'SELECT p.`id_product`, p.`reference`, pl.name,pl.link_rewrite,pa.id_product_attribute
		FROM `'._DB_PREFIX_.'product` p
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)Context::getContext()->language->id.Shop::addSqlRestrictionOnLang('pl').')
        LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (pa.id_product= p.id_product)
		WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')'.
		(!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
		($excludeVirtuals ? 'AND p.id_product NOT IN (SELECT pd.id_product FROM `'._DB_PREFIX_.'product_download` pd WHERE (pd.id_product = p.id_product))' : '').
		($exclude_packs ? 'AND (p.cache_is_pack IS NULL OR p.cache_is_pack = 0)' : '');

$items = Db::getInstance()->executeS($sql);

if ($items)
foreach ($items as &$item)
{
    if($item['id_product_attribute'])
    {
         $image=Product::getCombinationImageById($item['id_product_attribute'],Context::getContext()->language->id);
         $id_image=$image['id_image'];
    }
    if(!isset($id_image)|| !$id_image)
        $id_image = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int)$item['id_product'].' AND cover=1');
    if(version_compare(_PS_VERSION_, '1.7', '>='))
        $type_image= ImageType::getFormattedName('small');
    else
        $type_image= ImageType::getFormatedName('small');
    $url_image = Context::getContext()->link->getImageLink($item['link_rewrite'],$id_image,$type_image);
    $item['url_image']=$url_image;
    if($item['id_product_attribute'])
        $attributes=$pg_productbundles->getAllAttributes($item['id_product_attribute'])['attributes'];
    else
        $attributes='Null'; 
    $item['attributes']=$attributes;
}
die(Tools::jsonEncode(array(
    'items'=>$items,
)));
