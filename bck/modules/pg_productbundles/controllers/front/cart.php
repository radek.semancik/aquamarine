<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  @version  Release: $Revision$
*  International Registered Trademark & Property of PrestaShop SA
*/
if (!defined('_PS_VERSION_'))
	exit;
class Pg_productbundlesCartModuleFrontController extends ModuleFrontController
{
    protected $id_product;
    protected $id_product_attribute;
    protected $id_address_delivery;
    protected $customization_id;
    protected $qty;
    public $ssl = true;
    public $products;
    public function init()
    {
        parent::init();
        header('X-Robots-Tag: noindex, nofollow', true);

        // Get page main parameters
        $this->id_product = (int)Tools::getValue('id_product', null);
        $this->id_product_attribute = (int)Tools::getValue('id_product_attribute', Tools::getValue('ipa'));
        $this->customization_id = (int)Tools::getValue('id_customization');
        $this->qty = abs(Tools::getValue('qty', 1));
        $this->id_address_delivery = (int)Tools::getValue('id_address_delivery');
        $idProducts = Tools::getValue('idProducts')?Tools::getValue('idProducts'): '';
        $this->products =  (array)Tools::jsonDecode($idProducts);
        if($this->products)
        {
            $this->_newCustomization = true;
            if($this->id_product_attribute)
            {
                $this->addCusomization($this->module->l('Product bundles','cart'), $this->id_product, $this->id_product_attribute, $this->qty);
            }
            else
            {
                $this->addCusomization($this->module->l('Product bundles','cart'), $this->id_product, 0, $this->qty);
            }
        }
    }
    public function initContent()
    {
        parent::initContent();
        
        if(Tools::getIsset('item') && $this->isTokenValid() && version_compare(_PS_VERSION_, '1.7.0', '<=')){
            $idCombination = Tools::getValue('ida',0);
            $id_product = Tools::getValue('idp',0);
            if(!$id_product && !$idCombination){
                $this->ajaxDie(Tools::jsonEncode(array(
                    'hasError' => true,
                    'errors' => array($this->module->l('Product is null','etscart')),
                )));
            }else{
                $this->ajaxDie(Tools::jsonEncode(array(
                    'hasError' => false,
                    'product' => ptProduct::getProductAttribute($id_product, $idCombination, $this->context->shop->id),
                )));
            }
        }
        if ($this->context->cookie->exists() && !$this->errors && !($this->context->customer->isLogged() && !$this->isTokenValid()) ) {
            if (Tools::getIsset('add')) {
                $this->processChangeProductInCart();
            }
        } elseif (!$this->isTokenValid()) {
            if (Tools::getValue('ajax')) {
                $this->ajaxDie(Tools::jsonEncode(array(
                    'hasError' => true,
                    'errors' => array($this->module->l('Impossible to add the product to the cart. Please refresh page.','etscart')),
                )));
            } else {
                Tools::redirect('index.php');
            }
        }
    }
    
    /**
     * Quá trình này thêm nhiều sản phẩm vào giỏ hàng
     */
    protected function processChangeProductInCart()
    {
        $mode = (Tools::getIsset('update') && $this->id_product) ? 'update' : 'add';

        if (Tools::getIsset('group')) {
            $this->id_product_attribute = (int)Product::getIdProductAttributesByIdAttributes($this->id_product, Tools::getValue('group'));
        }
        if ($this->qty == 0) {
            $this->errors[] = $this->trans('Null quantity.', array(), 'Shop.Notifications.Error');
        } elseif (!$this->id_product) {
            $this->errors[] = $this->trans('Product not found', array(), 'Shop.Notifications.Error');
        }

        $product = new Product($this->id_product, true, $this->context->language->id);
        if (!$product->id || !$product->active || !$product->checkAccess($this->context->cart->id_customer)) {
            $this->errors[] = $this->trans('This product is no longer available.', array(), 'Shop.Notifications.Error');
            return;
        }

        $qty_to_check = $this->qty;
        $cart_products = $this->context->cart->getProducts();

        if (is_array($cart_products)) {
            foreach ($cart_products as $cart_product) {
                if ($this->productInCartMatchesCriteria($cart_product)) {
                    $qty_to_check = $cart_product['cart_quantity'];

                    if (Tools::getValue('op', 'up') == 'down') {
                        $qty_to_check -= $this->qty;
                    } else {
                        $qty_to_check += $this->qty;
                    }

                    break;
                }
            }
        }

        // Check product quantity availability
        if ($this->id_product_attribute) {
            if (!Product::isAvailableWhenOutOfStock($product->out_of_stock) && !Attribute::checkAttributeQty($this->id_product_attribute, $qty_to_check)) {
                $this->errors[] = $this->trans('There are not enough products in stock', array(), 'Shop.Notifications.Error');
            }
        } elseif ($product->hasAttributes()) {
            $minimumQuantity = ($product->out_of_stock == 2) ? !Configuration::get('PS_ORDER_OUT_OF_STOCK') : !$product->out_of_stock;
            $this->id_product_attribute = Product::getDefaultAttribute($product->id, $minimumQuantity);
            // @todo do something better than a redirect admin !!
            if (!$this->id_product_attribute) {
                Tools::redirectAdmin($this->context->link->getProductLink($product));
            } elseif (!Product::isAvailableWhenOutOfStock($product->out_of_stock) && !Attribute::checkAttributeQty($this->id_product_attribute, $qty_to_check)) {
                $this->errors[] = $this->trans('There are not enough products in stock', array(), 'Shop.Notifications.Error');
            }
        } elseif (!$product->checkQty($qty_to_check)) {
            $this->errors[] = $this->trans('There are not enough products in stock', array(), 'Shop.Notifications.Error');
        }

        // If no errors, process product addition
        if (!$this->errors) {
            // Add cart if no cart found
            if (!$this->context->cart->id) {
                if (Context::getContext()->cookie->id_guest) {
                    $guest = new Guest(Context::getContext()->cookie->id_guest);
                    $this->context->cart->mobile_theme = $guest->mobile_theme;
                }
                $this->context->cart->add();
                if ($this->context->cart->id) {
                    $this->context->cookie->id_cart = (int)$this->context->cart->id;
                }
            }

            // Check customizable fields
            if (!$product->hasAllRequiredCustomizableFields() && !$this->customization_id) {
                $this->errors[] = $this->trans('Please fill in all of the required fields, and then save your customizations.', array(), 'Shop.Notifications.Error');
            }

            if (!$this->errors) {
                $cart_rules = $this->context->cart->getCartRules();
                $available_cart_rules = CartRule::getCustomerCartRules($this->context->language->id, (isset($this->context->customer->id) ? $this->context->customer->id : 0), true, true, true, $this->context->cart, false, true);
                $update_quantity = $this->context->cart->updateQty($this->qty, $this->id_product, $this->id_product_attribute, $this->customization_id, Tools::getValue('op', 'up'), $this->id_address_delivery);
                if ($update_quantity < 0) {
                    // If product has attribute, minimal quantity is set with minimal quantity of attribute
                    $minimal_quantity = ($this->id_product_attribute) ? Attribute::getAttributeMinimalQty($this->id_product_attribute) : $product->minimal_quantity;
                    $this->errors[] = $this->trans('You must add %d minimum quantity', array($minimal_quantity), 'Shop.Notifications.Error');
                } elseif (!$update_quantity) {
                    $this->errors[] = $this->trans('You already have the maximum quantity available for this product.', array(), 'Shop.Notifications.Error');
                }
            }
        }
        $removed = CartRule::autoRemoveFromCart();
        CartRule::autoAddToCart();
        if($this->products)
        {
             global $cart;
             if($cart->id)
             {
                if($this->id_product_attribute)
                {
                    $sql = "UPDATE "._DB_PREFIX_."customization 
                    SET in_cart = 1
                    WHERE id_product_attribute=".$this->id_product_attribute." AND id_cart=".$this->context->cart->id." AND id_product=".$this->id_product;
                }
                else
                {
                    $sql = "UPDATE "._DB_PREFIX_."customization 
                    SET in_cart = 1
                    WHERE id_product_attribute=0 AND id_cart=".$this->context->cart->id." AND id_product=".$this->id_product;
                }
                Db::getInstance()->execute($sql);
                Configuration::updateValue('PS_CUSTOMIZATION_FEATURE_ACTIVE',1);
             }
        }
        $pg_productbundles= new Pg_productbundles();
        $pg_productbundles->displayAjaxUpdate($this->id_product,$this->id_product_attribute);
    }
    public function productInCartMatchesCriteria($productInCart)
    {
        return (
            !isset($this->id_product_attribute) ||
            (
                $productInCart['id_product_attribute'] == $this->id_product_attribute &&
                $productInCart['id_customization'] == $this->customization_id
            )
        ) && isset($this->id_product) && $productInCart['id_product'] == $this->id_product;
    }
    private function addCusomization($fieldName, $id_product, $id_product_attribute, $quantity)
    {
        // If cart has not been saved, we need to do it so that customization fields can have an id_cart
		// We check that the cookie exists first to avoid ghost carts
		if (!$this->context->cart->id && isset($_COOKIE[$this->context->cookie->getName()]))
		{
			$this->context->cart->add();
			$this->context->cookie->id_cart = (int)$this->context->cart->id;
		}
        
        $languages = Language::getLanguages();
        $id_cart = $this->context->cart->id;
        $sql = "SELECT cf.* FROM "._DB_PREFIX_."customization_field cf, "._DB_PREFIX_."customization_field_lang cfl 
        where cf.id_customization_field = cfl.id_customization_field and cfl.name='$fieldName' and cf.id_product=$id_product";
        $rs = Db::getInstance()->executeS($sql);
        if($rs)
        {
            $id_customization_field = $rs[0]['id_customization_field'];
        }
        else
        {
            $sql = "UPDATE "._DB_PREFIX_."product set customizable=1,text_fields=text_fields+1
                WHERE id_product = $id_product";           
            Db::getInstance()->execute($sql);
            
            $sql = "UPDATE "._DB_PREFIX_."product_shop set customizable=1,text_fields=text_fields+1
                    WHERE id_product = $id_product";
            Db::getInstance()->execute($sql);
            
            $sql = "INSERT INTO "._DB_PREFIX_."customization_field(`id_product`, `type`, `required`,is_module) VALUES($id_product,1,0,1)";
            Db::getInstance()->execute($sql);
            $id_customization_field = Db::getInstance()->Insert_ID();
            
            foreach($languages as $l)
            {
                $id_lang = $l['id_lang'];
                $sql = "INSERT INTO "._DB_PREFIX_."customization_field_lang(`id_customization_field`, `id_lang`, `name`) values($id_customization_field,$id_lang,'$fieldName')";
                Db::getInstance()->execute($sql);                
            }
        }
        
        //Add customization
        
        if($this->_newCustomization)
        {
        	if ($this->context->customer->isLogged()){
            	$id_adv = $this->context->cart->id_address_delivery;
        	}else{
            	$id_adv = 0;
        	}
        	
            $sql = "INSERT INTO "._DB_PREFIX_."customization(`id_product_attribute`,`id_address_delivery`, `id_cart`, `id_product`, `quantity`, `in_cart`) 
            VALUES($id_product_attribute,$id_adv,$id_cart,$id_product,$quantity,0)";
            Db::getInstance()->execute($sql);
            $this->_newCustomization = false;
        }
        
        $sql = "SELECT max(id_customization) as maxid from "._DB_PREFIX_."customization";
        $rs = Db::getInstance()->executeS($sql);
        if($rs)
        {
            $id_customization = $rs[0]['maxid'];
        }
        else
            $id_customization = 1;   
        $totalPrice=0;  
        $id_customer = ($this->context->customer->id) ? (int)($this->context->customer->id) : 0;
        $id_group = null;
        if ($id_customer) {
            $id_group = Customer::getDefaultGroupId((int)$id_customer);
        }
        if (!$id_group) {
            $id_group = (int)Group::getCurrent()->id;
        }
        $group= new Group($id_group);
        if($group->price_display_method)
            $tax=false;
        else
            $tax=true;
        $id_country = $id_customer ? (int) Customer::getCurrentCountry($id_customer) : (int) Tools::getCountry();
        $specific_price = SpecificPrice::getSpecificPrice(
            (int)$id_product,
            $this->context->shop->id,
            $this->context->currency->id,
            $id_country,
            $id_group,
            $quantity,
            $id_product_attribute,
            $id_customer,
            $id_cart,
            0
        );
        if($this->products)
        {
            foreach($this->products as $product)
            {
                $id_product_bundles= $product->id_product;
                $id_product_attribute_bundles = $product->id_product_attribute;
                
                $product_class= new Product($id_product_bundles,true,$this->context->language->id);
                $proudct_bundles_discount = Db::getInstance()->getValue('SELECT proudct_bundles_discount FROM '._DB_PREFIX_.'pg_productbundles WHERE id_product="'.(int)$this->id_product.'" AND id_product_bundles="'.(int)$id_product_bundles.'" AND id_product_attribute_bundles="'.(int)$id_product_attribute_bundles.'"');
                $price_old = $product_class->getPrice($tax,null);
                $price= $price_old-$price_old*$proudct_bundles_discount/100;
                $totalPrice +=$price;
                if($id_product_attribute_bundles)
                {
                    $product_name = $product_class->name.'-'.$this->module->getAllAttributes($id_product_attribute_bundles)['attributes_small'];
                }
                else
                {
                    $product_name = $product_class->name;
                }
                $sql ='INSERT INTO '._DB_PREFIX_.'pg_productbundles_combination(id_customization,id_product,id_product_attribute,name,old_price,price) VALUES("'.(int)$id_customization.'","'.(int)$id_product_bundles.'","'.(int)$id_product_attribute_bundles.'","'.pSQL($product_name).'","'.(float)$price_old.'","'.(float)$price.'")';
                Db::getInstance()->execute($sql);
            }
        }
        if($specific_price && isset($specific_price['reduction_type']) && $specific_price['reduction_type']=='percentage' && $specific_price['reduction'])
        {
            $totalPrice = $totalPrice/(1-$specific_price['reduction']);
        }
        $this->customization_id=$id_customization;
        $sql = "INSERT INTO "._DB_PREFIX_."customized_data(`id_customization`, `type`, `index`, `id_module`,`price`,`weight`)
                VALUES($id_customization,1,$id_customization_field,'".(int)$this->module->id."','".(float)$totalPrice."','0')";
        Db::getInstance()->execute($sql);
        
    }
}