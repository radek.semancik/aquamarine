<div class="row">
    <div class="sdsblog-box-content">
        {if isset($view_data) AND !empty($view_data)}
            {assign var='i' value=1}
            {foreach from=$view_data item=post}
               
                    {assign var="options" value=null}
                    {$options.id_post = $post.id}
                    {$options.slug = $post.link_rewrite}
                    <div id="sds_blog_post" class="col-md-4 col-sm-4 col-xs-12">
                        <div class="panel-news-item">
                            <a class="panel-news-media" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}"><img alt="{$post.title}" class="feat_img_small" src="{$modules_dir}smartblog/images/{$post.post_img}-home-default.jpg" /></a>
                            
                            
                            <h3 class="sds_post_title panel-news-title">
                                <a class="panel-news-link" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">
                                    {$post.title}
                                </a>
                            </h3>
                            <p class="panel-news-summary">
                                {$post.short_description|escape:'htmlall':'UTF-8'}
                            </p>
                            {*
                            <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}"  class="r_more">
                                {l s='Read More' mod='smartbloghomelatestnews'}
                            </a>
                            *}
                            {assign var='date_format' value='F j, Y'}
                            <ul class="panel-info-post">
                                <li>
                                        {date($date_format,strtotime($post.date_added))|escape:'html':'UTF-8'}
                                </li>
                                <li>
                                     <i class="fa fa-eye"></i>{$post.viewed|escape:'html':'UTF-8'}
                                </li>
                            </ul>
                        </div>
                    </div>
                
                {$i=$i+1}
            {/foreach}
        {/if}
     </div>
</div>