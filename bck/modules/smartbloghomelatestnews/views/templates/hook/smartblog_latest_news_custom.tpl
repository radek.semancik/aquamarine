<div class="row">
    <div class="sdsblog-box-content">
        {if isset($view_data) AND !empty($view_data)}
            {assign var='i' value=1}
            {foreach from=$view_data item=post}
                    {assign var="options" value=null}
                    {$options.id_post = $post.id}
                    {$options.slug = $post.link_rewrite}
                    {assign var='date_format' value='F j, Y'}
                    
                    
           
                    <pre>
                        {print_r($post)}
                    </pre>
                    
                    
                    <div id="sds_blog_post" class="col-md-4 col-sm-4 col-xs-12">
                        <div class="post_thumbnail">
                            <a class="panel-news-media" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}"><img alt="{$post.title}" class="feat_img_small" src="{$modules_dir}smartblog/images/{$post.post_img}-home-default.jpg" /></a>
                            
                            
                            <div class="inside-thumb posi_ab">
                                <div class="post-meta">
                
                                    {l s='By' d='Shop.Theme.Catalog'}<span> {*$post.lastname*}</span> {l s='in' d='Shop.Theme.Catalog'}
                                    <a href="{$post.link_category}">
                                         <span class="panel-news-category">
                                            {$post.category_name}
                                         </span>
                                     </a>
                                     {*
                                     <a title="{$post.totalcomment} Comments" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}#articleComments">
                                        <span class="comment"> 
                                        {$post.totalcomment} {if $post.totalcomment > 1}{l s='Comments' d='Shop.Theme.Catalog'}{else}{l s='Comment' d='Shop.Theme.Catalog'}{/if}
                                        </span>
                                     </a>*}
                                    <span>
                                        {$post.viewed|escape:'html':'UTF-8'} {if $post.viewed > 1}{l s='Views' d='Shop.Theme.Catalog'}{else}{l s='View' d='Shop.Theme.Catalog'}{/if}
                                    </span>
                                     
                                </div>
                                <h2 class="post-title">
                                    <a class="panel-news-link" title="{$post.title}" href='{smartblog::GetSmartBlogLink('smartblog_post',$options)}'>
                                        {$post.title}
                                    </a>
                                </h2>
                                <span class="posted-on">
                                    {date($date_format,strtotime($post.date_added))|escape:'html':'UTF-8'}
                                </span>
                             </div>
                            
                            


                        </div>
                    </div>
                {$i=$i+1}
            {/foreach}
        {/if}
     </div>
</div>