<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @version  Release: $Revision$
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class SlideExport extends Module
{
	public	function __construct()
	{
		parent::__construct();
        $this->context = Context::getContext();
	}
    public function exportSlider()
    {
        $groupSlides = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'pg_group_slide gs,'._DB_PREFIX_.'pg_group_slide_lang gsl WHERE gs.id_pg_group_slide=gsl.id_pg_group_slide AND gsl.id_lang='.$this->context->language->id);
        if($groupSlides)
        {
            $filename = 'slider.xml';
    		header('Content-type: text/xml');
    		header('Content-Disposition: attachment; filename="'.$filename.'"');	
    		$xml_output = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
    		$xml_output .= '<entity_profile>'."\n";	
            $definition_group = Pg_group_slide::$definition;
            $fields_group = $definition_group['fields'];
            foreach($groupSlides as $groupSlide)
            {
                $xml_output .='<groupslide ';
                if($fields_group)
                {
                    foreach($fields_group as $key=> $field)
                       if(!isset($field['lang'])|| !$field['lang'])
                            $xml_output.=$key.'="'.$groupSlide[$key].'" ';   
                }
                $xml_output .='>'."\n";
                foreach($fields_group as $key=> $field)
                    if(isset($field['lang'])&&$field['lang'])
                       $xml_output.='<'.$key.'><![CDATA['.$groupSlide[$key].']]></'.$key.'>'."\n";
                $sliders = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'pg_slide s,'._DB_PREFIX_.'pg_slide_lang sl WHERE s.id_pg_slide=sl.id_pg_slide AND sl.id_lang="'.(int)$this->context->language->id.'" AND s.id_pg_group_slide='.(int)$groupSlide['id_pg_group_slide']);
                if($sliders)
                {
                    $definition_slide = Pg_slide::$definition;
                    $fields_slide = $definition_slide['fields'];
                    foreach($sliders as $slider)
                    {
                        $xml_output .='<slider ';
                        if($fields_slide)
                        {
                            foreach($fields_slide as $key=> $field)
                               if(!isset($field['lang'])||!$field['lang'])
                                    $xml_output.=$key.'="'.$slider[$key].'" ';   
                        }
                        $xml_output .='>'."\n";
                        foreach($fields_slide as $key=> $field)
                        if(isset($field['lang'])&& $field['lang'])
                           $xml_output.='<'.$key.'><![CDATA['.$slider[$key].']]></'.$key.'>'."\n";
                        $items = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'pg_slide_item i, '._DB_PREFIX_.'pg_slide_item_lang il WHERE i.id_pg_slide_item=il.id_pg_slide_item AND i.id_pg_slide='.$slider['id_pg_slide'].' AND il.id_lang='.(int)$this->context->language->id);
                        if($items)
                        {
                            $definition_item = Pg_slide_item::$definition;
                            $fields_item = $definition_item['fields'];
                            foreach($items as $item)
                            {
                                $xml_output .='<item ';
                                if($fields_item)
                                {
                                foreach($fields_item as $key=> $field)
                                if(!isset($field['lang'])||!$field['lang'])
                                    $xml_output.=$key.'="'.$item[$key].'" ';   
                                }
                                $xml_output .='>'."\n";
                                foreach($fields_item as $key=> $field)
                                if(isset($field['lang']) && $field['lang'])
                                   $xml_output.='<'.$key.'><![CDATA['.$item[$key].']]></'.$key.'>'."\n";
                                $xml_output .='</item>'."\n";
                            }
                        }
                        $xml_output .='</slider>'."\n";
                    }
                }
                $xml_output .='</groupslide>'."\n";
            }
            $xml_output .= '</entity_profile>'."\n";
    		echo $xml_output; 
    		exit;
        }
    }
    public function importSlide()
    {
        $context = Context::getContext();
		$shop_id = $context->shop->id;
        $languages = Language::getLanguages(false);
        $definition_group = Pg_group_slide::$definition;
        $fields_group = $definition_group['fields'];
        $definition_slide = Pg_slide::$definition;
        $fields_slide = $definition_slide['fields'];
        $definition_item = Pg_slide_item::$definition;
        $fields_item = $definition_item['fields'];
        $filename="slider.xml";
        if (file_exists(_PS_MODULE_DIR_.'pg_slider/xml/'.$filename))	
		{	
			$xml = simplexml_load_file(_PS_MODULE_DIR_.'pg_slider/xml/'.$filename);
            if($xml->groupslide)
            {
                foreach($xml->groupslide as $groupslide)
                {
                    $pg_group_slide = new Pg_group_slide();
                    foreach($fields_group as $key=> $field)
                    {
                        if(!isset($field['lang'])|| !$field['lang'])
                            $pg_group_slide->$key = (string)$groupslide[$key];
                    }
                    foreach($languages as $language)
                    {
                        $pg_group_slide->title[$language['id_lang']] = (string)$groupslide->title;
                    }
                    $pg_group_slide->add();
                    if($groupslide->slider && $pg_group_slide->id)
                    {
                        foreach($groupslide->slider as $slider)
                        {
                            $pg_slide=new Pg_slide();
                            foreach($fields_slide as $key=> $field)
                            {
                            if(!isset($field['lang'])|| !$field['lang'])
                                $pg_slide->$key = (string)$slider[$key];
                            }
                            foreach($languages as $language)
                            {
                                $pg_slide->title[$language['id_lang']] = (string)$slider->title;
                                $pg_slide->link[$language['id_lang']] = (string)$slider->link;
                            }
                            $pg_slide->id_pg_group_slide = $pg_group_slide->id;
                            $pg_slide->add();
                            if($slider->item && $pg_slide->id)
                            {
                                foreach($slider->item as $item)
                                {
                                    $pg_slide_item = new Pg_slide_item();
                                    foreach($fields_item as $key=> $field)
                                    {
                                    if(!isset($field['lang'])|| !$field['lang'])
                                        $pg_slide_item->$key = (string)$item[$key];
                                    }
                                    foreach($languages as $language)
                                    {
                                        $pg_slide_item->text_html[$language['id_lang']] = (string)$item->text_html;
                                        $pg_slide_item->link[$language['id_lang']] = (string)$item->link;
                                        $pg_slide_item->label_button[$language['id_lang']] = (string)$item->label_button;
                                    }
                                    $pg_slide_item->id_pg_slide = $pg_slide->id;
                                    $pg_slide_item->add();
                                }
                            }
                        }
                    }
                    
                }
            }
        }
    }
}