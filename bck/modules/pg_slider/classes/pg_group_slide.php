<?php
class Pg_group_slide extends ObjectModel
{
    public $id_shop;
	public $title;
	public $position_hook;
    public $slide_layout;
	public $desktop_width;
    public $desktop_height;
    public $notebook_size;
    public $notebook_width;
    public $notebook_height;
    public $tablet_size;
    public $tablet_width;
    public $tablet_height;
    public $mobile_size;
    public $mobile_width;
    public $mobile_height;
    public $background_color;
    public $background_image;
    public $slide_width;
    public $slide_height;
    public $pause_time;
    public $auto_play;
    public $pase_when_hover;
    public $slide_lop;
    public $slide_nav;
    public $position_nav;
    public $slide_dots;
    public $position_dots;
    public $custom_class;
    public $active;
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'pg_group_slide',
		'primary' => 'id_pg_group_slide',
		'multilang' => true,
		'fields' => array(
			'title' =>			    array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml'),
			'position_hook' =>	    array('type' => self::TYPE_STRING),
            'slide_layout' =>       array('type'=>self::TYPE_STRING),
            'desktop_width'=>       array('type'=>self::TYPE_FLOAT),
            'desktop_height'=>      array('type'=>self::TYPE_FLOAT),
            'notebook_size' =>      array('type'=>self::TYPE_INT),
            'notebook_width' =>     array('type'=>self::TYPE_FLOAT),
            'notebook_height' =>    array('type'=>self::TYPE_FLOAT),
            'tablet_size' =>        array('type'=>self::TYPE_INT),
            'tablet_width' =>       array('type'=>self::TYPE_FLOAT),
            'tablet_height' =>      array('type'=>self::TYPE_FLOAT),
            'mobile_size' =>        array('type'=>self::TYPE_INT),
            'mobile_width' =>       array('type'=>self::TYPE_FLOAT),
            'mobile_height' =>      array('type'=>self::TYPE_FLOAT),
            'background_color'=>    array('type' => self::TYPE_STRING),
            'background_image' =>   array('type'=>self::TYPE_STRING),
            'pause_time' =>         array('type'=>self::TYPE_INT),
            'auto_play'=>           array('type'=>self::TYPE_INT),
            'pase_when_hover'=>     array('type'=>self::TYPE_INT),
            'slide_lop'=>           array('type'=>self::TYPE_INT),
            'slide_nav'=>           array('type'=>self::TYPE_INT),
            'position_nav'=>        array('type'=>self::TYPE_STRING),
            'slide_dots'=>          array('type'=>self::TYPE_INT),
            'position_dots'=>       array('type'=>self::TYPE_STRING),
            'custom_class'=>        array('type'=>self::TYPE_STRING),
            'active'=>              array('type'=>self::TYPE_INT),
		)
	);
	public	function __construct($id_group_slider = null, $id_lang = null, $id_shop = null, Context $context = null)
	{
		parent::__construct($id_group_slider, $id_lang, $id_shop);
	}
	public function add($autodate = true, $null_values = false)
	{
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$res = parent::add($autodate, $null_values);
		$res &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'pg_group_slide_shop` (`id_shop`, `id_pg_group_slide`)
			VALUES('.(int)$id_shop.', '.(int)$this->id.')'
		);
		return $res;
	}
	public function delete()
	{
		$res = true;
        if ($this->background_image && file_exists(dirname(__FILE__).'/../views/img/slide/'.$this->background_image))
			$res &= @unlink(dirname(__FILE__).'/../views/img/slide/'.$this->background_image);
		$res &= Db::getInstance()->execute('
			DELETE FROM `'._DB_PREFIX_.'pg_group_slide_shop`
			WHERE `id_pg_group_slide` = '.(int)$this->id
		);
		$res &= parent::delete();
		return $res;
	}
}
?>