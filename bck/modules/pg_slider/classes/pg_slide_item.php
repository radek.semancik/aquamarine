<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Pg_slide_item extends ObjectModel
{
	public $active;
    public $id_pg_slide;
    public $type;
    public $text_html;
    public $font_family;
    public $font_size;
    public $background_color;
    public $color;
    public $padding_top;
    public $padding_right;
    public $padding_bottom;
    public $padding_left;
    public $font_weight;
    public $text_transform;
    public $text_decoration;
    public $custom_class;
    public $image;
    public $width;
    public $height;
    public $link;
    public $label_button;
    public $position_top;
    public $position_left;
    public $effect_in;
    public $delay_in;
    public $time_effect_in;
    public $effect_out;
    public $delay_out;
    public $time_effect_out;
    public $font_style;
    public $border_radius;
    public $border_width_top;
    public $border_width_left;
    public $border_width_bottom;
    public $border_width_right;
    public $border_color;
    public $border_color_hover;
    public $border_style;
    public $hover_color;
    public $hover_background_color;
    public $position;
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'pg_slide_item',
		'primary' => 'id_pg_slide_item',
		'multilang' => true,
		'fields' => array(
            'active' =>			      array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'id_pg_slide' =>          array('type'=>self::TYPE_INT),
            'type' =>                 array('type'=>self::TYPE_STRING),
			'font_family' =>          array('type' => self::TYPE_STRING),
            'font_size' =>            array('type'=>self::TYPE_FLOAT),
            'background_color'=>      array('type'=>self::TYPE_STRING),
            'color' =>                array('type'=>self::TYPE_STRING),
            'padding_top'=>           array('type'=>self::TYPE_FLOAT),
            'padding_right'=>         array('type'=>self::TYPE_FLOAT),
            'padding_bottom' =>       array('type'=>self::TYPE_FLOAT),
            'padding_left' =>         array('type'=>self::TYPE_FLOAT),
            'font_weight' =>          array('type'=>self::TYPE_STRING),
            'text_transform' =>       array('type'=>self::TYPE_STRING),
            'text_decoration' =>      array('type'=>self::TYPE_STRING),
            'custom_class' =>         array('type'=>self::TYPE_STRING),
            'image' =>                array('type'=>self::TYPE_STRING),
            'width' =>                array('type'=>self::TYPE_FLOAT),
            'height' =>               array('type'=>self::TYPE_FLOAT),
            'position_top' =>         array('type'=>self::TYPE_FLOAT),
            'position_left' =>        array('type'=>self::TYPE_FLOAT),
            'effect_in' =>            array('type'=>self::TYPE_STRING),
            'delay_in' =>             array('type'=>self::TYPE_INT),
            'time_effect_in' =>       array('type'=>self::TYPE_INT),
            'effect_out' =>           array('type'=>self::TYPE_STRING),
            'delay_out' =>            array('type'=>self::TYPE_INT),
            'time_effect_out' =>      array('type'=>self::TYPE_INT),
            'font_style' =>           array('type'=>self::TYPE_STRING),
            'border_radius' =>    array('type'=>self::TYPE_STRING),
            'border_width_top' =>     array('type'=>self::TYPE_STRING),
            'border_width_left' =>    array('type'=>self::TYPE_STRING),
            'border_width_bottom' =>  array('type'=>self::TYPE_STRING),
            'border_width_right' =>   array('type'=>self::TYPE_STRING),
            'border_style'=>          array('type'=>self::TYPE_STRING),
            'border_color' =>         array('type'=>self::TYPE_STRING),
            'border_color_hover' =>   array('type'=>self::TYPE_STRING),
            'hover_color' =>         array('type'=>self::TYPE_STRING),
            'hover_background_color' =>         array('type'=>self::TYPE_STRING),
            'link' =>                 array('type'=>self::TYPE_STRING,'lang'=>true),
            'text_html'  =>           array('type'=>self::TYPE_STRING,'lang'=>true),
            'label_button' =>         array('type'=>self::TYPE_STRING,'lang'=>true),
            'position' =>             array('type'=>self::TYPE_INT),
		)
	);

	public	function __construct($id_slide = null, $id_lang = null,$id_shop = null)
	{
		parent::__construct($id_slide, $id_lang,$id_shop);
	}
	public function delete()
	{
		$res = true;
		if ($this->image && file_exists(dirname(__FILE__).'/../views/img/item/'.$this->image))
			$res &= @unlink(dirname(__FILE__).'/../views/img/item/'.$this->image);
		$res &= parent::delete();
		return $res;
	}
}