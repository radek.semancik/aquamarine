<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Pg_slide extends ObjectModel
{
	public $title;
    public $backgroud_image;
	public $backgroud_color;
	public $repeat;
	public $background_size;
    public $background_position;
    public $effect_in;
    public $time_in;
    public $effect_out;
    public $time_out;
    public $custom_class;
    public $position;
    public $link;
    public $active;
    public $id_shop;
    public $id_pg_group_slide;
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'pg_slide',
		'primary' => 'id_pg_slide',
		'multilang' => true,
		'fields' => array(
            'id_pg_group_slide' =>  array('type'=>self::TYPE_INT),
            'active' =>			    array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'backgroud_image' =>    array('type'=>self::TYPE_STRING),
			'backgroud_color' =>    array('type' => self::TYPE_STRING),
            'repeat' =>             array('type'=>self::TYPE_STRING),
            'background_size'=>     array('type'=>self::TYPE_STRING),
            'background_position'=> array('type'=>self::TYPE_STRING),
            'effect_in' =>          array('type'=>self::TYPE_STRING),
            'time_in' =>            array('type'=>self::TYPE_INT),
            'effect_out'=>          array('type'=>self::TYPE_STRING),
            'time_out' =>             array('type'=>self::TYPE_INT),
            'custom_class'=>        array('type'=>self::TYPE_STRING),
            'position' =>           array('type'=>self::TYPE_INT),
            'link' =>               array('type'=>self::TYPE_STRING,'lang'=>true),
            'title'  =>             array('type'=>self::TYPE_STRING,'lang'=>true),
		)
	);

	public	function __construct($id_slide = null, $id_lang = null,$id_shop = null)
	{
		parent::__construct($id_slide, $id_lang,$id_shop);
	}
	public function add($autodate = true, $null_values = false)
	{
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$res = parent::add($autodate, $null_values);
		$res &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'pg_slide_shop` (`id_shop`, `id_pg_slide`)
			VALUES('.(int)$id_shop.', '.(int)$this->id.')'
		);
		return $res;
	}
	public function delete()
	{
		$res = true;
		if ($this->backgroud_image && file_exists(dirname(__FILE__).'/../views/img/slide/'.$this->backgroud_image))
			$res &= @unlink(dirname(__FILE__).'/../views/img/slide/'.$this->backgroud_image);
		$res &= $this->reOrderPositions();
		$res &= Db::getInstance()->execute('
			DELETE FROM `'._DB_PREFIX_.'pg_slide_shop`
			WHERE `id_pg_slide` = '.(int)$this->id
		);
		$res &= parent::delete();
		return $res;
	}
	public function reOrderPositions()
	{
		$id_pg_slide = $this->id;
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$max = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT MAX(s.`position`) as position
			FROM `'._DB_PREFIX_.'pg_slide` s, `'._DB_PREFIX_.'pg_slide_shop` ss
			WHERE s.`id_pg_slide` = ss.`id_pg_slide` AND ss.`id_shop` = '.(int)$id_shop
		);
		if ((int)$max == (int)$id_pg_slide)
			return true;
		$rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT s.`position` as position, s.`id_pg_slide` as id_slide
			FROM `'._DB_PREFIX_.'pg_slide` s
			LEFT JOIN `'._DB_PREFIX_.'pg_slide_shop` ss ON (s.`id_pg_slide` = ss.`id_pg_slide`)
			WHERE ss.`id_shop` = '.(int)$id_shop.' AND s.`position` > '.(int)$this->position
		);
		foreach ($rows as $row)
		{
			$current_slide = new Pg_slide($row['id_slide']);
			--$current_slide->position;
			$current_slide->update();
			unset($current_slide);
		}
		return true;
	}
}