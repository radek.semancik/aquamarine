{if $fields}
    {assign var='tab_active' value=true}
    {foreach from=$fields item='field'}
        {if isset($field.begin_tab) && $field.begin_tab}
        <div id="pg_form_tab_{$field.begin_tab}" class="pg_form_tab_content {if $tab_active}active{/if}">
        {assign var='tab_active' value=false}
        {/if}
        <div class="pg_form_control {if $field.type=='file'}pg_upload_img{/if} {if $field.type=='list_input'}pg_custom_button_ena{/if} {if isset($fields_values[$field.name]) && !$fields_values[$field.name] && isset($field.null_disable)&& $field.null_disable}disable{/if}">
            {if $field.type=='hidden'}
                <input type="hidden" name="{$field.name}" value="{$field.value}" />
            {else}
                <label for="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}">{$field.label}</label>
                {if isset($field.lang) && $field.lang}
                    {if $field.type=='text'}
                        {if $languages|count > 1}
        				    <div class="form-group">
        				{/if}
                        {foreach $languages as $language}
        					{assign var='value_text' value=$fields_values[$field.name][$language.id_lang]}
        					{if $languages|count > 1}
        					<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $defaultFormLanguage}style="display:none"{/if}>
        						<div class="col-lg-9">
        					{/if}
      							   <input type="text" name="{$field.name}_{$language.id_lang}" id="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}_{$language.id_lang}" value="{$value_text}" />
        					{if $languages|count > 1}
        						</div>
        						<div class="col-lg-2">
        							<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
        								{$language.iso_code}
        								<i class="icon-caret-down"></i>
        							</button>
        							<ul class="dropdown-menu">
        								{foreach from=$languages item=language}
        								<li><a href="javascript:hideOtherLanguage({$language.id_lang});" tabindex="-1">{$language.name}</a></li>
        								{/foreach}
        							</ul>
        						</div>
        					</div>
        					{/if}
        				{/foreach}
                        {if $languages|count > 1}
        				    </div>
        				{/if}
                    {/if}
                    {if $field.type=='textarea'}
                        {foreach $languages as $language}
    						{if $languages|count > 1}
    						<div class="form-group translatable-field lang-{$language.id_lang}"{if $language.id_lang != $defaultFormLanguage} style="display:none;"{/if}>
    							<div class="col-lg-9">
    						{/if}
    						<textarea name="{$field.name}_{$language.id_lang}" id="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}_{$language.id_lang}" class="{if isset($input.autoload_rte) && $input.autoload_rte}rte autoload_rte{else}textarea-autosize{/if}{if isset($input.class)} {$input.class}{/if}"{if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}{if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}>{$fields_values[$field.name][$language.id_lang]|escape:'html':'UTF-8'}</textarea>
    						{if $languages|count > 1}
    							</div>
    							<div class="col-lg-2">
    								<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
    									{$language.iso_code}
    									<span class="caret"></span>
    								</button>
    								<ul class="dropdown-menu">
    									{foreach from=$languages item=language}
    									<li>
    										<a href="javascript:hideOtherLanguage({$language.id_lang});" tabindex="-1">{$language.name}</a>
    									</li>
    									{/foreach}
    								</ul>
    							</div>
    						</div>
    						{/if}
    					{/foreach}
                    {/if}
                {else}
                    {if isset($field.suffix)}
                    <div class="suffix">
                    {/if}
                    {if $field.type=='select'}
                        <select name="{$field.name}" id="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}" {if isset($field.multiple) && $field.multiple} multiple="multiple"{/if}>
                            {if $field.options}
                                {foreach from =$field.options item='option'}
                                    <option value="{$option.id}" 
                                    {if isset($field.multiple)}
        								{foreach $fields_values[$field.name] as $field_value}
        									{if $field_value == $option.id}
        										selected="selected"
        									{/if}
        								{/foreach}
        							{else}
        								{if $fields_values[$field.name] == $option.id}
        									selected="selected"
        								{/if}
        							{/if}>{$option.name}</option>
                                {/foreach}
                            {/if}
                        </select>
                    {/if}
                    {if $field.type=='color'}
                        <input data-hex="true" type="color" name="{$field.name}" id="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}" class="color mColorPickerInput" value="{$fields_values[$field.name]|escape:'html':'UTF-8'}" style="background-color:{$fields_values[$field.name]|escape:'html':'UTF-8'}" />
                    {/if}
                    {if $field.type=='text'}
                        <input type="text" name="{$field.name}" id="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}" value="{$fields_values[$field.name]}" />
                    {/if}
                    {if $field.type=='switch'}
                        <span class="switch prestashop-switch fixed-width-lg">
        					<input type="radio" name="{$field.name}"  id="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}_on" value="1" {if isset($fields_values[$field.name]) && $fields_values[$field.name]==1}checked="checked"{/if}/>
        					{strip}
        					<label for="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}_on">
        							{l s='Yes' mod='pg_slider'}
        					</label>
        					{/strip}
                            <input type="radio" name="{$field.name}"  id="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}_off" value="0" {if isset($fields_values[$field.name]) && $fields_values[$field.name]==0}checked="checked"{/if}/>
        					{strip}
        					<label for="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}_off">
        							{l s='No' mod='pg_slider'}
        					</label>
        					{/strip}
        					<a class="slide-button btn"></a>
        				</span>
                    {/if}
                    {if $field.type=='file'}
                        {if isset($fields_values[$field.name]) && $fields_values[$field.name]}
                            <img src="{$img_path}{$field.type_img}/{$fields_values[$field.name]}" />
                            {if isset($field.link_delete)&&$field.link_delete}
                                <a class="delete_image" href="{$field.link_delete}" title="{l s='Delete image' mod='pg_slider'}"><i class="fa fa-trash-o"></i>{l s='Delete image' mod='pg_slider'}</a>
                            {/if}
                        {/if}
                        
                        <input type="file" name="{$field.name}" id="{if isset($field.id)}{$field.id}{else}{$field.name}{/if}" />
                    {/if}
                    {if $field.type=='list_input'}
                        {foreach from =$field.inputs item='input'}
                            <span class="pg_pd {$input.class}">
                                <label for="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}">{if isset($input.label)}{$input.label}{/if}</label>
                                    {if $input.type=='text'}
                                        <input type="text" name="{$input.name}" id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}" value="{$fields_values[$input.name]}" />
                                    {/if}
                                    {if $input.type=='switch'}
                                        <span class="switch prestashop-switch fixed-width-lg">
                        					<input type="radio" name="{$input.name}"  id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}_on" value="1" {if isset($fields_values[$input.name]) && $fields_values[$input.name]==1}checked="checked"{/if}/>
                        					{strip}
                        					<label for="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}_on">
                        							{l s='Yes' mod='pg_slider'}
                        					</label>
                        					{/strip}
                                            <input type="radio" name="{$input.name}"  id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}_off" value="0" {if isset($fields_values[$input.name]) && $fields_values[$input.name]==0}checked="checked"{/if}/>
                        					{strip}
                        					<label for="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}_off">
                        							{l s='No' mod='pg_slider'}
                        					</label>
                        					{/strip}
                        					<a class="slide-button btn"></a>
                        				</span>
                                    {/if}
                                    {if $input.type=='color'}
                                        <input data-hex="true" type="color" name="{$input.name}" id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}" value="{$fields_values[$input.name]}" class="color mColorPickerInput" />
                                    {/if}
                            </span>
                        {/foreach}
                    {/if}
                    {if $field.type=='radio'}
                        {foreach $field.options item='option'}
                            <span class="col-sm-4"> <input type="radio" value="{$option.id}" id="{$field.name}_{$option.id}" {if $fields_values[$field.name] == $option.id} checked="checked" {/if} name="{$field.name}" /><label for="{$field.name}_{$option.id}"></label> {$option.name} </span>
                        {/foreach}
                    {/if}
                    {if isset($field.suffix)}
                        <span class="input-group-addon">{$field.suffix}</span>
                    </div>
                    {/if}
                {/if}
            {/if}
            {if isset($field.desc)&& $field.desc}
                <p class="help-block">{$field.desc} </p>
            {/if}
        </div>
        {if isset($field.end_tab)}
        </div>
        {/if}
    {/foreach}
{/if}