<div class="container" id="form-pg-slide">
    <div class="pg_config_slider">
        <span title="{l s='Main Setting' mod='pg_slider'}" class="pg_config_button">
            <span class="pg_button_spinner2 pg_button_spinner_top"><i class="fa fa-cog"></i></span>
            <span class="pg_button_spinner pg_button_spinner_large"><i class="fa fa-cog"></i></span>
            <span class="pg_button_spinner2 pg_button_spinner_bottom"><i class="fa fa-cog"></i></span>
        </span>
        <div class="pg_form_config_slider">
            {$form_config}
        </div>
    </div>
    <ul class="pg_slider_tab_header">
        {if $slides}
            {foreach from =$slides key='key' item='slide'}
                <li id="slides-{$slide.id_pg_slide}" class="pg_tab_slider_item"><a href="#" {if $key==0}class="active"{/if} data-id-slide="{$slide.id_pg_slide}">{$slide.title}</a></li>
            {/foreach}
        {/if}
        <li>
            <a href="#pg_form_slide" class="add_new_slide pg_button_layer"><i class="fa fa-plus"></i></a>
        </li>
        <li>
            <a href="{$link_module}" title="{l s='Back' mod='pg_slider'}">{l s='Back' mod='pg_slider'}</a>
        </li>
    </ul>
    <div id="list-slide-content">
        <div class="ui-sortable pg_slider_content_tab active">
            {if $slides}
                {foreach from= $slides item='slide'}
                    <div class="pg_slider_group_action">
                        <a href="#pg_listitems" class="pg_button_layer pg_button_viewlist" data-id-slide="{$slide.id_pg_slide}"><i class="fa fa-bars"></i>{l s='View Items' mod='pg_slider'}</a>
                        <a href="#pg_form_add_item" class="pg_button_layer pg_button_addtext" data-id-slide="{$slide.id_pg_slide}"><i class="fa fa-text-height"></i>{l s='Add Text' mod='pg_slider'}</a>
                        <a href="#pg_form_add_item" class="pg_button_layer pg_button_addimg" data-id-slide="{$slide.id_pg_slide}"><i class="fa fa-file-image-o"></i>{l s='Add Image' mod='pg_slider'}</a>
                        <a href="#pg_form_add_item" class="pg_button_layer pg_button_addbutton" data-id-slide="{$slide.id_pg_slide}"><i class="fa fa-hand-o-up"></i>{l s='Add Button' mod='pg_slider'}</a>
                        <a href="#pg_form_slide" class="pg_button_layer pg_button_edit_slide" data-id-slide="{$slide.id_pg_slide}"><i class="fa fa-pencil-square-o"></i>{l s='Edit' mod='pg_slider'}</a>
                        <a class="pg_button_layer pg_button_coppy" data-id-slide="{$slide.id_pg_slide}"><i class="fa fa-files-o"></i>{l s='Dupplicate' mod='pg_slider'}</a>
                        <a class="pg_button_layer pg_button_dellayer" data-id-slide="{$slide.id_pg_slide}"><i class="fa fa-trash-o"></i>{l s='Delete' mod='pg_slide'}</a>
                        <a class="pg_button_layer pg_button_active" data-id-slide="{$slide.id_pg_slide}">{if $slide.active}<i class="fa fa-check-circle"></i>{else}<i class="fa fa-times"></i>{/if}{if $slide.active}{l s='Enable' mod='pg_slide'}{else}{l s='Disable' mod='pg_slide'}{/if}</a>
                    </div>
                    <div class="pg_slider_background">
                        <div id="slider-{$slide.id_pg_slide}" class="pg_slider_background_img pg_sl_animated" style="width:{$width_slide}px; height:{$height_slide}px;background-image: url('{$img_path}slide/{$slide.backgroud_image}');position: relative;"  >
                            {if $itemsSlide}
                                {foreach from=$itemsSlide item='itemSlide'}
                                    {if $itemSlide.type=='text'}
                                        <div class="pg_listitem pg_sl_animated ui-widget-content draggable itemslide-draggable" id="itemslide-{$itemSlide.id_pg_slide_item|intval}" data-id-item="{$itemSlide.id_pg_slide_item|intval}" style="
                                        {if $itemSlide.font_style}font-style:{$itemSlide.font_style};{/if} 
                                        {if $itemSlide.padding_right!=0}padding-right:{$itemSlide.padding_right}px;{/if}
                                        {if $itemSlide.padding_bottom!=0}padding-bottom:{$itemSlide.padding_bottom}px;{/if}
                                        {if $itemSlide.padding_left!=0}padding-left:{$itemSlide.padding_left}px;{/if} 
                                        {if $itemSlide.padding_top}padding-top:{$itemSlide.padding_top}px;{/if}
                                        position: absolute;left:{$itemSlide.position_left}px;top:{$itemSlide.position_top}px;
                                        {if $itemSlide.color}color:{$itemSlide.color};{/if}
                                        {if $itemSlide.background_color}background:{$itemSlide.background_color};{/if}
                                        {if $itemSlide.font_size!=0}font-size:{$itemSlide.font_size}px;{/if}
                                        {if $itemSlide.font_family}font-family:{$itemSlide.font_family};{/if}
                                        {if $itemSlide.text_decoration}text-decoration:{$itemSlide.text_decoration};{/if}
                                        {if $itemSlide.text_transform}text-transform:{$itemSlide.text_transform};{/if}
                                        {if $itemSlide.font_weight}font-weight:{$itemSlide.font_weight};{/if}
                                        {if $itemSlide.font_style}font-style:{$itemSlide.font_style};{/if}">
                                            {$itemSlide.text_html}
                                        </div>
                                    {/if}
                                    {if $itemSlide.type=='image'}
                                        <div class="pg_listitem pg_sl_animated ui-widget-content itemslide-draggable" id="itemslide-{$itemSlide.id_pg_slide_item|intval}" data-id-item="{$itemSlide.id_pg_slide_item|intval}" style="
                                        {if $itemSlide.padding_right!=0}padding-right:{$itemSlide.padding_right}px;{/if}
                                        {if $itemSlide.padding_bottom!=0}padding-bottom:{$itemSlide.padding_bottom}px;{/if}
                                        {if $itemSlide.padding_left!=0}padding-left:{$itemSlide.padding_left}px;{/if} 
                                        {if $itemSlide.padding_top}padding-top:{$itemSlide.padding_top}px;{/if}
                                        position: absolute;left:{$itemSlide.position_left}px;top:{$itemSlide.position_top}px;
                                        {if $itemSlide.color}:{$itemSlide.color};{/if}
                                        {if $itemSlide.background_color}background:{$itemSlide.background_color};{/if}">
                                             <img style="{if $itemSlide.width!=0}width:{$itemSlide.width}px{/if};{if $itemSlide.height!=0}height:{$itemSlide.height}px{/if}" src="{$img_path}item/{$itemSlide.image}" />
                                        </div>
                                    {/if}
                                    {if $itemSlide.type=='button'}
                                        <div class="pg_listitem pg_sl_animated ui-widget-content itemslide-draggable" id="itemslide-{$itemSlide.id_pg_slide_item|intval}" data-id-item="{$itemSlide.id_pg_slide_item|intval}" 
                                        style="
                                        {if $itemSlide.font_style}
                                        font-style:{$itemSlide.font_style};
                                        {/if}
                                        position: absolute;left:{$itemSlide.position_left}px;top:{$itemSlide.position_top}px;"
                                        >
                                            <a style="
                                            {if $itemSlide.padding_right!=0}padding-right:{$itemSlide.padding_right}px;{/if}
                                            {if $itemSlide.padding_bottom!=0}padding-bottom:{$itemSlide.padding_bottom}px;{/if}
                                            {if $itemSlide.padding_left!=0}padding-left:{$itemSlide.padding_left}px;{/if}
                                            {if $itemSlide.padding_top}padding-top:{$itemSlide.padding_top}px;{/if}
                                            
                                            {if $itemSlide.color}color:{$itemSlide.color};{/if}
                                            {if $itemSlide.background_color}background:{$itemSlide.background_color};{/if}
                                            {if $itemSlide.font_size!=0}font-size:{$itemSlide.font_size}px;{/if}
                                            {if $itemSlide.font_family}font-family:{$itemSlide.font_family};{/if}
                                            {if $itemSlide.text_decoration}text-decoration:{$itemSlide.text_decoration};{/if}
                                            {if $itemSlide.text_transform}text-transform:{$itemSlide.text_transform};{/if}
                                            {if $itemSlide.font_weight}font-weight:{$itemSlide.font_weight};{/if}
                                            {if $itemSlide.border_width_bottom!=0}border-bottom:{$itemSlide.border_width_bottom}px;{/if}
                                            {if $itemSlide.border_width_right!=0}border-right:{$itemSlide.border_width_right}px;{/if}
                                            {if $itemSlide.border_width_left!=0}border-left:{$itemSlide.border_width_left}px;{/if} 
                                            {if $itemSlide.border_width_top!=0}border-top:{$itemSlide.border_width_top}px;{/if} 
                                            {if $itemSlide.border_radius}border-radius:{$itemSlide.border_radius}px;{/if} 
                                            {if $itemSlide.font_style}font-style:{$itemSlide.font_style};{/if}
                                            {if $itemSlide.border_style}border-style:{$itemSlide.border_style};{/if}{if $itemSlide.border_color}border-color:{$itemSlide.border_color};{/if}" 
                                            
                                            title="{$itemSlide.label_button}" href="{if $itemSlide.link}{$itemSlide.link}{else}javascript:void(0){/if}">{$itemSlide.label_button}</a>
                                            <style>
                                                #itemslide-{$itemSlide.id_pg_slide_item|intval} a:hover
                                                {literal}
                                                    {
                                                {/literal}
                                                        {if $itemSlide.hover_color}color:{$itemSlide.hover_color}!important;{/if}
                                                        {if $itemSlide.hover_background_color}background:{$itemSlide.hover_background_color}!important;{/if}
                                                        {if $itemSlide.border_color_hover}border-color:{$itemSlide.border_color_hover}!important;{/if}
                                                {literal}
                                                    }
                                                {/literal}
                                            </style>
                                        </div>
                                    {/if}
                                {/foreach}
                            {/if}
                        </div>
                    </div>
                    {break}
                {/foreach}
            {else}
                {l s='No Slide' mod='pg_slider'}
            {/if}
        </div>
    </div>
    <div class="pg_listitems pg_form_fixed" id="pg_listitems">
        
    </div>
    <div class="pg_form_fixed" id="pg_form_slide">  
    </div>
    <div id="pg_form_add_item" class="pg_form_fixed">
        
    </div>
</div>