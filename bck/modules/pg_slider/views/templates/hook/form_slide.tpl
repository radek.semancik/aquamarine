<script src="{$js_dir_path|escape:'quotes':'UTF-8'}jquery.colorpicker.js"></script>
<form action="{$action}" method="post" enctype="multipart/form-data">
    <div class="pg_form_header">
        <h3 class="pg_form_header_title">
            {$form_title}
        </h3>
        <span class="pg_close_form_parent"></span>
    </div>
    <div class="pg_form_add_content">
        <div class="pg_form_tab_header">
            <a class="active" href="#pg_form_tab_general">{l s='General' mod='pg_slide'}</a>
            <a class="" href="#pg_form_tab_animation">{l s='Animation' mod='pg_slide'}</a>
        </div>
        {$list_fields}
    </div>
    <input type="hidden" name="saveSlide" value="1" />
    <button type="submit" name="saveSlide"><i class="process-icon-save"></i>{l s='Save'}</button>
</form>