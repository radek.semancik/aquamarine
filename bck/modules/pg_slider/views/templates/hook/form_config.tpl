<script src="{$js_dir_path|escape:'quotes':'UTF-8'}jquery.colorpicker.js"></script>
<form action="{$action}" method="post" enctype="multipart/form-data">
    <div class="pg_form_config_slider_header">
        <h3 class="pg_form_config_slider_title">
            {$title_form}
        </h3>
        <span class="pg_close_form_config"></span>
    </div>
    <div class="pg_form_config_slider_content">
        <div class="pg_form_tab_header">
            <a class="active" href="#pg_form_tab_general">{l s='General' mod='pg_slide'}</a>
            <a class="" href="#pg_form_tab_animation">{l s='Animation' mod='pg_slide'}</a>
        </div>
        {$list_fields}
    </div>
    <input type="hidden" name="saveConfig" value="1" />
    <button type="submit" name="saveConfig"><i class="process-icon-save"></i>{l s='Save'}</button>
</form>