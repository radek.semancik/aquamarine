{if $groupslides}
    {foreach from =$groupslides item='groupslide'}
        <div class="pg_wrapper_slide">
            <div id="pg_groupslide_{$groupslide.id_group}" class="pg_sl_group {$groupslide.custom_class}" data-group-desktop-height="{$groupslide.desktop_height}" data-group-desktop-width="{$groupslide.desktop_width}" data-group-layout="{$groupslide.slide_layout}" data-group-loop="{$groupslide.slide_lop}" data-group-pause-hover="{$groupslide.pase_when_hover}" data-group-auto-play="{$groupslide.auto_play}" data-group-pause-time="{$groupslide.pause_time}" {if $groupslide.mobile_size}date-group-mobile="1" data-group-mobile-width="{$groupslide.mobile_width}" data-group-mobile-height="{$groupslide.mobile_height}"{else}date-group-mobile="0"{/if} {if $groupslide.tablet_size}date-group-tablet="1" data-group-tablet-width="{$groupslide.tablet_width}" data-group-tablet-height="{$groupslide.tablet_height}"{else}date-group-tablet="0"{/if} {if $groupslide.notebook_size}date-group-notebook="1" data-group-notebook-width="{$groupslide.notebook_width}" data-group-notebook-height="{$groupslide.notebook_height}"{else}date-group-notebook="0"{/if} style="height:{$groupslide.desktop_height}px;width:{$groupslide.desktop_width}px; background:{$groupslide.background_color};{if $groupslide.background_image}background-image: url('{$image_baseurl}/group/{$groupslide.background_image}{/if}');" >
                {if $groupslide.slides}
                    {foreach from=$groupslide.slides item='slide'}{*[repeat_x],[repeat_y],[effect_in],[effect_out],[custom_class],[position],[id_lang],[title],[link]*}
                        <div id="slide_{$slide.id_pg_slide}" data-slider-time-in="{$slide.time_in}" data-slider-time-out="{$slide.time_out}" data-slider-in="{$slide.effect_in}" data-slider-out="{$slide.effect_out}" class=" pg_sl_slide_listitem {$slide.custom_class}" 
                            style="background-color:{$slide.backgroud_color};">
                            <a class="pg_sl_item_link"{if isset($slide.link) && $slide.link} href="{$slide.link}"{/if}>
                                <img src="{$image_baseurl}slide/{$slide.backgroud_image}" alt="{$slide.title}" title="{$slide.title}" />  
                            </a>
                            {if $slide.items}
                                {foreach from=$slide.items item='item'}
                                    {if $item.type=='text'}
                                        <div data-item-effect-in="{$item.effect_in}" data-item-effect-out="{$item.effect_out}" {if $item.font_size!=0}data-item-font-size="{$item.font_size}"{else}data-item-font-size="14"{/if} data-item-delay-in="{$item.delay_in}" data-item-delay-out="{$item.delay_out}" data-item-position-top="{$item.position_top}" data-item-position-left="{$item.position_left}" class="pg_listitem   draggable itemslide-draggable {$item.custom_class}" id="itemslide-{$item.id_pg_slide_item|intval}" data-id-item="{$item.id_pg_slide_item|intval}" style="
                                        {if $item.font_style}font-style:{$item.font_style};{/if} 
                                        {if $item.padding_right!=0}padding-right:{$item.padding_right}px;{/if}
                                        {if $item.padding_bottom!=0}padding-bottom:{$item.padding_bottom}px;{/if}
                                        {if $item.padding_left!=0}padding-left:{$item.padding_left}px;{/if} 
                                        {if $item.padding_top}padding-top:{$item.padding_top}px;{/if}
                                        position: absolute;left:{$item.position_left}px;top:{$item.position_top}px;
                                        {if $item.color}color:{$item.color};{/if}
                                        {if $item.background_color}background:{$item.background_color};{/if}
                                        {if $item.font_size!=0}font-size:{$item.font_size}px;{else}font-size:14px;{/if}
                                        {if $item.font_family}font-family:{$item.font_family};{/if}
                                        {if $item.text_decoration}text-decoration:{$item.text_decoration};{/if}
                                        {if $item.text_transform}text-transform:{$item.text_transform};{/if}
                                        {if $item.font_weight}font-weight:{$item.font_weight};{/if}
                                        {if $item.font_style}font-style:{$item.font_style};{/if}">
                                            {$item.text_html}
                                        </div>
                                    {/if}
                                    {if $item.type=='image'}
                                        <div data-item-effect-in="{$item.effect_in}" data-item-effect-out="{$item.effect_out}" data-item-delay-in="{$item.delay_in}" data-item-delay-out="{$item.delay_out}" data-item-position-top="{$item.position_top}" data-item-position-left="{$item.position_left}" class="pg_listitem   itemslide-draggable {$item.custom_class}" id="itemslide-{$item.id_pg_slide_item|intval}" data-id-item="{$item.id_pg_slide_item|intval}" style="
                                        {if $item.padding_right!=0}padding-top:{$item.padding_right}px;{/if}
                                        {if $item.padding_bottom!=0}padding-bottom:{$item.padding_bottom}px;{/if}
                                        {if $item.padding_left!=0}padding-left:{$item.padding_left}px;{/if} 
                                        {if $item.padding_top}padding-top:{$item.padding_top}px;{/if}
                                        position: absolute;left:{$item.position_left}px;top:{$item.position_top}px;
                                        {if $item.color}:{$item.color};{/if}
                                        {if $item.background_color}background:{$item.background_color};{/if}">
                                             <img style="{if $item.width!=0}width:{$item.width}px{/if};{if $item.height!=0}height:{$item.height}px{/if}" src="{$image_baseurl}item/{$item.image}" />
                                        </div>
                                    {/if}
                                    {if $item.type=='button'}
                                        <div data-item-padding-bottom="{$item.padding_bottom}" data-item-padding-left={$item.padding_left} data-item-padding-right="{$item.padding_right}" data-item-padding-top="{$item.padding_top}"  data-item-effect-in="{$item.effect_in}" data-item-effect-out="{$item.effect_out}" {if $item.font_size!=0}data-item-font-size="{$item.font_size}"{else}data-item-font-size="14"{/if} data-item-delay-in="{$item.delay_in}" data-item-delay-out="{$item.delay_out}" data-item-position-top="{$item.position_top}" data-item-position-left="{$item.position_left}" class="pg_listitem  itemslide-draggable {$item.custom_class}" id="itemslide-{$item.id_pg_slide_item|intval}" data-id-item="{$item.id_pg_slide_item|intval}" 
                                        style="
                                        {if $item.font_style}
                                        font-style:{$item.font_style};
                                        {/if}
                                        position: absolute;left:{$item.position_left}px;top:{$item.position_top}px;{if $item.font_size!=0}font-size:{$item.font_size}px;{else}font-size:14px;{/if}"
                                        >
                                            <a style="
                                            {if $item.padding_right!=0}padding-right:{$item.padding_right}px;{/if}
                                            {if $item.padding_bottom!=0}padding-bottom:{$item.padding_bottom}px;{/if}
                                            {if $item.padding_left!=0}padding-left:{$item.padding_left}px;{/if}
                                            {if $item.padding_top}padding-top:{$item.padding_top}px;{/if}
                                            {if $item.border_color}border-color:{$item.border_color};{/if}
                                            {if $item.color}color:{$item.color};{/if}
                                            {if $item.background_color}background:{$item.background_color};{/if}
                                            {if $item.font_family}font-family:{$item.font_family};{/if}
                                            {if $item.text_decoration}text-decoration:{$item.text_decoration};{/if}
                                            {if $item.text_transform}text-transform:{$item.text_transform};{/if}
                                            {if $item.font_weight}font-weight:{$item.font_weight};{/if}
                                            {if $item.border_width_bottom!=0}border-bottom:{$item.border_width_bottom}px;{/if}
                                            {if $item.border_width_right!=0}border-right:{$item.border_width_right}px;{/if}
                                            {if $item.border_width_left!=0}border-left:{$item.border_width_left}px;{/if} 
                                            {if $item.border_width_top!=0}border-top:{$item.border_width_top}px;{/if} 
                                            {if $item.border_radius}border-radius:{$item.border_radius}px;{/if} 
                                            {if $item.font_style}font-style:{$item.font_style};{/if}
                                            {if $item.border_style}border-style:{$item.border_style};{/if}" 
                                            title="{$item.label_button}" href="{if $item.link}{$item.link}{else}javascript:void(0){/if}">{$item.label_button}</a>
                                            <style>
                                                #itemslide-{$item.id_pg_slide_item|intval} a:hover
                                                {literal}
                                                    {
                                                {/literal}
                                                        {if $item.hover_color}color:{$item.hover_color}!important;{/if}
                                                        {if $item.hover_background_color}background:{$item.hover_background_color}!important;{/if}
                                                        {if $item.border_color_hover}border-color:{$item.border_color_hover}!important;{/if}
                                                {literal}
                                                    }
                                                {/literal}
                                            </style>
                                        </div>
                                    {/if}
                                {/foreach}
                            {/if}
                        </div>
                    {/foreach}
                    {if $groupslide.slide_nav}
                        <div class="pg_sl_nav {$groupslide.position_nav}">
                            <div class="pg_sl_nav_back"><i class="fa fa-angle-left"></i></div>
                            <div class="pg_sl_nav_next"><i class="fa fa-angle-right"></i></div>
                        </div>
                    {/if}
                    {if $groupslide.slide_dots}
                        <div class="pg_sl_dots {$groupslide.position_dots}">
                            {foreach from=$groupslide.slides item='slide'}
                                <span class="pg_sl_dot pg_sl_dot_slide_{$slide.id_pg_slide}" data-id="{$slide.id_pg_slide}"></span>
                            {/foreach}
                        </div>
                    {/if}
                {/if}
            </div>
            <div class="clearfix"></div>
        </div>
    {/foreach}
{/if}