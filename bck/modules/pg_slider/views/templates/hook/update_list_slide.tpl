{if $slides}
    {foreach from =$slides key='key' item='slide'}
        <li id="slides-{$slide.id_pg_slide}" class="pg_tab_slider_item"><a href="#" {if $slide.id_pg_slide==$id_slide}class="active"{/if} data-id-slide="{$slide.id_pg_slide}">{$slide.title}</a></li>
    {/foreach}
{/if}
<li>
    <a href="#pg_form_slide" class="add_new_slide pg_button_layer"><i class="fa fa-plus"></i></a>
</li>
<li>
    <a href="{$link_module}" title="{l s='Back' mod='pg_slider'}">{l s='Back' mod='pg_slider'}</a>
</li>