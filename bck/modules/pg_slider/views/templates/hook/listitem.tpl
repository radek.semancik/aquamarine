<div class="pg_listitems_header">
    <h3 class="pg_listitems_title">
        {l s='List items' mod='pg_slider'}
    </h3>
    <span class="pg_close_listitems pg_close_list_parent"></span>
</div>
<div class="pg_listitems_content">
    {if $items}
        {foreach from=$items item='item'}
            {if $item.type=='text'}
                <div class="pg_listitem type_text" id="item_{$item.id_pg_slide_item}">
                    <span class="pg_listitem_move">
                        <i class="fa fa-arrows-alt"></i>
                    </span>
                    <div class="pg_listitem_content">
                        {$item.text_html}
                    </div>
                    <div class="pg_listitem_action">
                        <a title="{l s='Edit' mod='pg_slider'}" href="#pg_form_add_item" class="pg_listitem_button pg_listitem_edit" data-type="text" data-id-item="{$item.id_pg_slide_item}" data-id-slide="{$item.id_pg_slide}"><i class="fa fa-pencil-square-o"></i></a>
                        <a title="Dupplicate" class="pg_listitem_button pg_listitem_coppy" data-type="text" data-id-item="{$item.id_pg_slide_item}"><i class="fa fa-files-o"></i></a>
                        <a title="Delete" class="pg_listitem_button pg_listitem_dellayer" data-type="text" data-id-item="{$item.id_pg_slide_item}"><i class="fa fa-trash-o"></i></a>
                    </div>
                </div>
            {/if}
            {if $item.type=='image'}
                <div class="pg_listitem type_img" id="item_{$item.id_pg_slide_item}">
                    <span class="pg_listitem_move">
                            <i class="fa fa-arrows-alt"></i>
                        </span>
                    <div class="pg_listitem_content">
                        <img title="" alt="" src="{$img_path}item/{$item.image}"/>
                    </div>
                    <div class="pg_listitem_action">
                        <a title="Edit" href="#pg_form_add_item" class="pg_listitem_button pg_listitem_edit" data-type="img" data-id-item="{$item.id_pg_slide_item}" data-id-slide="{$item.id_pg_slide}"><i class="fa fa-pencil-square-o"></i></a>
                        <a title="Dupplicate" class="pg_listitem_button pg_listitem_coppy" data-type="img" data-id-item="{$item.id_pg_slide_item}"><i class="fa fa-files-o"></i></a>
                        <a title="Delete" class="pg_listitem_button pg_listitem_dellayer" data-type="img" data-id-item="{$item.id_pg_slide_item}"><i class="fa fa-trash-o"></i></a>
                    </div>
                </div>
            {/if}
            {if $item.type=='button'}
                <div class="pg_listitem type_button" id="item_{$item.id_pg_slide_item}">
                    <span class="pg_listitem_move">
                        <i class="fa fa-arrows-alt"></i>
                    </span>
                    <div class="pg_listitem_content">
                        <span>{l s='Button' mod='pg_slider'}:</span> {$item.label_button}
                    </div>
                    <div class="pg_listitem_action">
                        <a title="Edit" href="#pg_form_add_item" class="pg_listitem_button pg_listitem_edit" data-type="button" data-id-item="{$item.id_pg_slide_item}" data-id-slide="{$item.id_pg_slide}"><i class="fa fa-pencil-square-o"></i></a>
                        <a title="Dupplicate" class="pg_listitem_button pg_listitem_coppy" data-type="button" data-id-item="{$item.id_pg_slide_item}"><i class="fa fa-files-o"></i></a>
                        <a title="Delete" class="pg_listitem_button pg_listitem_dellayer" data-type="button" data-id-item="{$item.id_pg_slide_item}"><i class="fa fa-trash-o"></i></a>
                    </div>
                </div>
            {/if}
        {/foreach}
    {/if}
</div>