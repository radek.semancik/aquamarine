<div class="panel">
    <h3><i class="icon-list-ul"></i> {l s='Group slides list' mod='pg_slider'}
    	<span class="panel-heading-action">
    		<a id="desc-product-new" class="list-toolbar-btn" href="{$link->getAdminLink('AdminModules')}&configure=pg_slider&addGroupSlide=1">
    			<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Add new group' mod='pg_slider'}" data-html="true">
    				<i class="process-icon-new "></i>
    			</span>
    		</a>
    	</span>
	</h3>
	<div id="listgroupsilders">
		<div id="groupsilders">
            {if $groupsliders}
    			{foreach from=$groupsliders item=groupslider}
    				<div id="groupsilders_{$groupslider.id_pg_group_slide}" class="panel">
                        <div class="row">
    						<div class="col-md-6">
    							<h4 class="pull-left">
    								#{$groupslider.id_pg_group_slide} - {$groupslider.title}
    							</h4>
    						</div>
                            <div class="col-md-3">
                                {$groupslider.position_hook}
                            </div>
                            <div class="col-md-3">
                                <div class="btn-group-action pull-right">
    								<a class="btn btn-default"
    									href="{$link->getAdminLink('AdminModules')}&configure=pg_slider&editGroup=1&id_pg_group_slide={$groupslider.id_pg_group_slide}">
    									<i class="icon-edit"></i>
    									{l s='Edit' mod='pg_slider'}
    								</a>
    								<a class="btn btn-default" href="{$link->getAdminLink('AdminModules')}&configure=pg_slider&delete_id_pg_group_slide={$groupslider.id_pg_group_slide}">
    									<i class="icon-trash"></i>
    									{l s='Delete' mod='pg_slider'}
    								</a>
                                    <a class="btn btn-default" href="{$link->getAdminLink('AdminModules')}&configure=pg_slider&viewGroup&id_pg_group_slide={$groupslider.id_pg_group_slide}">
    									<i class="icon-search-plus"></i>
    									{l s='View' mod='pg_slider'}
    								</a>
    							</div>
                            </div>
					   </div>
    				</div>
    			{/foreach}
            {/if}
		</div>
	</div>
</div>