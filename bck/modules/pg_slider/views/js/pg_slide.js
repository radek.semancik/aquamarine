var intervals =[];
$(document).ready(function(){
    if($('.pg_sl_group').length)
    {
        $('.pg_sl_group').each(function(){
           setGroupwidth($(this));
           if(parseInt($(this).attr('data-group-auto-play'))==1)
           {
                var pause_time= parseInt($(this).attr('data-group-pause-time'));
                if(pause_time <=0)
                    pause_time =5000;
                var group_id= $(this).attr('id');
                intervals[group_id]= setInterval(autoLoadSlide, pause_time,group_id);
           }
        });
    }
    $(".pg_sl_group").hover(function(){
            if(parseInt($(this).attr('data-group-auto-play'))==1&& parseInt($(this).attr('data-group-pause-hover'))==1)
            {
                var group_id= $(this).attr('id');
                clearInterval(intervals[group_id]);
            }    
        }, function(){
            if(parseInt($(this).attr('data-group-auto-play'))==1 && parseInt($(this).attr('data-group-pause-hover'))==1)
            {
                var pause_time= parseInt($(this).attr('data-group-pause-time'));
                if(pause_time <=0)
                    pause_time =5000;
                var group_id= $(this).attr('id');
                intervals[group_id]= setInterval(autoLoadSlide, pause_time,group_id);
            }
    });
    $(window).resize(function(){
        $('.pg_listitem.pg_sl_animated').removeClass('pg_sl_animated');
        if($('.pg_sl_group').length)
        {
           $('.pg_sl_group').each(function(){
               setGroupwidth($(this)); 
            }); 
        }
    });
    $('.pg_sl_group').each(function(){
        runSlide($(this).find('.pg_sl_slide_listitem').eq(0));
    });
    $(document).on('click','.pg_sl_nav_next:not(.notclick)',function(){
        var $parent = $(this).closest('.pg_sl_group');
        var id_group = $parent.attr('id');
        if($('#'+id_group+' .pg_sl_slide_listitem').eq(-1).hasClass('active'))
        {
            runSlide($('#'+id_group+' .pg_sl_slide_listitem').eq(0));
        }
        else
            runSlide($('#'+id_group+' .pg_sl_slide_listitem.active').next());
    });
    $(document).on('click','.pg_sl_nav_back:not(.notclick)',function(){
        var $parent = $(this).closest('.pg_sl_group');
        var id_group = $parent.attr('id');
        if($('#'+id_group+' .pg_sl_slide_listitem').eq(0).hasClass('active'))
        {
            runSlide($('#'+id_group+' .pg_sl_slide_listitem').eq(-1));
        }
        else
            runSlide($('#'+id_group+' .pg_sl_slide_listitem.active').prev());
    });
    $(document).on('click','.pg_sl_dot:not(.notclick)',function(e){
        var $parent = $(this).closest('.pg_sl_group');
        var id_group = $parent.attr('id');
        runSlide($('#'+id_group+' #slide_'+$(this).attr('data-id')));
    });
});
function runSlide($slide)
{
    var $parent = $slide.closest('.pg_sl_group');
    var id_group = $parent.attr('id');
    var width_desktop = $parent.attr('data-group-desktop-width');
    var height_desktop = $parent.attr('data-group-desktop-height');
    var width_slide =$parent.width();
    var height_slide = $parent.height();
    var data_in = $slide.attr('data-slider-in');
    var data_out = $slide.attr('data-slider-out');
    var data_time_in = $slide.attr('data-slider-time-in');
    $('#'+id_group).find('.pg_sl_dot').removeClass('active');
    $('.pg_sl_dot_'+$slide.attr('id')).addClass('active');
    $('#'+id_group).find('.pg_sl_nav_back').addClass('notclick');
    $('#'+id_group).find('.pg_sl_nav_next').addClass('notclick');
    $('#'+id_group).find('.pg_sl_dot').addClass('notclick');
    if($('#'+id_group+' .pg_sl_slide_listitem.active').length)
    {
        $('#'+id_group+' .pg_sl_slide_listitem.active').addClass('slide_out');
        var data_out_active = $('#'+id_group+' .pg_sl_slide_listitem.active.slide_out').attr('data-slider-out');
        var data_in_active = $('#'+id_group+' .pg_sl_slide_listitem.active.slide_out').attr('data-slider-in');
        var data_time_out_active=$('#'+id_group+' .pg_sl_slide_listitem.active.slide_out').attr('data-slider-time-out');
        var max_delay_out =0;
        if($('#'+id_group+' .pg_sl_slide_listitem.active.slide_out .pg_listitem').length)
        {
            $('#'+id_group+' .pg_sl_slide_listitem.active.slide_out .pg_listitem').each(function(){
                if(max_delay_out < parseInt($(this).attr('data-item-delay-out')))
                    max_delay_out = parseInt($(this).attr('data-item-delay-out'));
            });
        }
        if($('#'+id_group+' .pg_sl_slide_listitem.active.slide_out .pg_listitem').length)
        {
            $('#'+id_group+' .pg_sl_slide_listitem.active.slide_out .pg_listitem').each(function(){
                outItem($(this));
            });
        }
        setTimeout(function(){
            $slide.addClass('active');
            $slide.css("animation-duration",data_time_in+"ms");
            $slide.removeClass('pg_sl_'+data_out);
            $slide.addClass('pg_sl_'+data_in);
            $slide.addClass('pg_sl_animated');
            if($('#'+id_group+' .pg_sl_slide_listitem.active:not(.slide_out) .pg_listitem'))
            {
                $('#'+id_group+' .pg_sl_slide_listitem.active:not(.slide_out) .pg_listitem').each(function(){
                    inItem($(this),width_slide,height_slide,width_desktop,height_desktop);
                });
            }
        },max_delay_out+500);
        var time =parseInt(data_time_out_active)+parseInt(max_delay_out);
        setTimeout(function(){
            $('#'+id_group+' .pg_sl_slide_listitem.active.slide_out').removeClass('pg_sl_'+data_in_active);
            $('#'+id_group+' .pg_sl_slide_listitem.active.slide_out').addClass('pg_sl_'+data_out_active);
            $('#'+id_group+' .pg_sl_slide_listitem.active.slide_out').css("animation-duration",data_time_out_active+"ms");
            setTimeout(function(){
                $('#'+id_group+' .pg_sl_slide_listitem.active.slide_out').removeClass('active');
                checkLopSlider(id_group);
                $('#'+id_group+' .pg_sl_slide_listitem.active.slide_out').removeClass('pg_sl_animated');
                $('#'+id_group+' .pg_sl_slide_listitem.slide_out').removeClass('slide_out');
                $('#'+id_group).find('.pg_sl_nav_back').removeClass('notclick');
                $('#'+id_group).find('.pg_sl_nav_next').removeClass('notclick');
                $('#'+id_group).find('.pg_sl_dot').removeClass('notclick');
                
            },data_time_out_active);
           
        },max_delay_out+500);
    }
    else
    {
        $slide.css("animation-duration",data_time_in+"ms");
        //$slide.css("transition-duration",data_time_in+"ms");
        $slide.removeClass('pg_sl_'+data_out);
        $slide.addClass('pg_sl_'+data_in);
        $slide.addClass('pg_sl_animated');
        $slide.addClass('pg_sl_animated');
        $slide.addClass('active');
        var max_delay_in =0;
        if($('#'+id_group+' .pg_sl_slide_listitem.active:not(.slide_out) .pg_listitem').length)
        {
            $('#'+id_group+' .pg_sl_slide_listitem.active:not(.slide_out) .pg_listitem').each(function(){
                if(max_delay_in < parseInt($(this).attr('data-item-delay-in')))
                    max_delay_in = parseInt($(this).attr('data-item-delay-in'));
            });
        }
        checkLopSlider(id_group);
        if($('#'+id_group+' .pg_sl_slide_listitem.active:not(.slide_out) .pg_listitem').length)
        {
            $('#'+id_group+' .pg_sl_slide_listitem.active:not(.slide_out) .pg_listitem').each(function(){
                inItem($(this),width_slide,height_slide,width_desktop,height_desktop);
            });
        }
        setTimeout(function(){
            $('#'+id_group).find('.pg_sl_nav_back').removeClass('notclick');
            $('#'+id_group).find('.pg_sl_nav_next').removeClass('notclick');
            $('#'+id_group).find('.pg_sl_dot').removeClass('notclick');
        },max_delay_in);
    }
    
}
function setGroupwidth($group)
{
    var layout = $group.attr('data-group-layout');
    var width_screen = parseFloat($(window).width());
    var height_screen = parseFloat($(window).height());
    var width_desktop = parseFloat($group.attr('data-group-desktop-width'));
    var height_desktop = parseFloat($group.attr('data-group-desktop-height'));
    var width_wrapper= parseFloat($group.closest('.pg_wrapper_slide').width());
    var left =$group.closest('.pg_wrapper_slide').position().left;
    if(layout=='auto')
    {
        if(width_screen>=1025)
        {
            if(width_desktop> width_wrapper)
            {
                var height = width_wrapper*height_desktop/width_desktop;
                $group.css('height',height+'px');
            }
            else
                $group.css('height',height_desktop+'px');
        }
        if(width_screen<1024 && width_screen>=768)
        {
            if($group.attr('date-group-notebook')=='1')
            {
                var width_notebook = parseFloat($group.attr('data-group-notebook-width'));
                var height_notebook = parseFloat($group.attr('data-group-notebook-height'));
                if(width_notebook > width_wrapper)
                {
                    var height = width_wrapper*height_notebook/width_notebook;
                    $group.css('height',height+'px');
                }
                else
                {
                    $group.css('height',height_notebook+'px'); 
                }
            }
            else
            {
                if(width_desktop> width_wrapper)
                {
                    var height = width_wrapper*height_desktop/width_desktop;
                    $group.css('height',height+'px');
                }
                else
                    $group.css('height',height_desktop+'px');
            }
        }
        if(width_screen>481 && width_screen<767)
        {
            if($group.attr('date-group-tablet')=='1')
            {
                var width_tablet = parseFloat($group.attr('data-group-tablet-width'));
                var height_tablet = parseFloat($group.attr('data-group-tablet-height'));
                if(width_tablet> width_wrapper)
                {
                    var height = width_wrapper*height_tablet/width_tablet;
                    $group.css('height',height+'px');
                }
                else
                {
                    $group.css('height',height_tablet+'px'); 
                }
            }
            else
            {
                if(width_desktop> width_wrapper)
                {
                    var height = width_wrapper*height_desktop/width_desktop;
                    $group.css('height',height+'px');
                }
                else
                    $group.css('height',height_desktop+'px');
            }
        }
        if(width_screen<=481)
        {
            if($group.attr('date-group-mobile')=='1')
            {
                var width_mobile = parseFloat($group.attr('data-group-mobile-width'));
                var height_mobile = parseFloat($group.attr('data-group-mobile-height'));
                if(width_mobile> width_wrapper)
                {
                    var height = width_wrapper*height_mobile/width_mobile;
                    $group.css('height',height+'px');
                }
                else
                {
                    $group.css('height',height_mobile+'px'); 
                }
            }
            else
            {
                if(width_desktop> width_wrapper)
                {
                    var height = width_wrapper*height_desktop/width_desktop;
                    $group.css('height',height+'px');
                }
                else
                    $group.css('height',height_desktop+'px');
            }
        }
    }
    if(layout=='full_width'){
        if(width_screen>=1025)
        {
            if(width_desktop> width_screen)
            {
                var height = width_screen*height_desktop/width_desktop;
                $group.css('height',height+'px');
                $group.css('width',width_screen+'px');
            }
            else
            {
                $group.css('width',width_screen+'px');
                $group.css('height',height_desktop+'px');
            }
        }
        if(width_screen<1024 && width_screen>=768)
        {
            if($group.attr('date-group-notebook')=='1')
            {
                var width_notebook = parseFloat($group.attr('data-group-notebook-width'));
                var height_notebook = parseFloat($group.attr('data-group-notebook-height'));
                if(width_notebook> width_screen)
                {
                    var height = width_screen*height_notebook/width_notebook;
                    $group.css('height',height+'px');
                    $group.css('width',width_screen+'px');
                }
                else
                {
                    $group.css('height',height_notebook+'px'); 
                    $group.css('width',width_screen+'px');
                }
            }
            else
            {
                if(width_desktop> width_screen)
                {
                    var height = width_screen*height_desktop/width_desktop;
                    $group.css('height',height+'px');
                    $group.css('width',width_screen+'px');
                }
                else
                    $group.css('height',height_desktop+'px');
            }
        }
        if(width_screen>481 && width_screen<767)
        {
            if($group.attr('date-group-tablet')=='1')
            {
                var width_tablet = parseFloat($group.attr('data-group-tablet-width'));
                var height_tablet = parseFloat($group.attr('data-group-tablet-height'));
                if(width_tablet> width_screen)
                {
                    var height = width_screen*height_tablet/width_tablet;
                    $group.css('height',height+'px');
                    $group.css('width',width_screen+'px');
                }
                else
                {
                    $group.css('height',height_tablet+'px'); 
                }
            }
            else
            {
                if(width_desktop> width_screen)
                {
                    var height = width_screen*height_desktop/width_desktop;
                    $group.css('height',height+'px');
                    $group.css('width',width_screen+'px');
                }
                else
                    $group.css('height',height_desktop+'px');
            }
        }
        if(width_screen<=481)
        {
            if($group.attr('date-group-mobile')=='1')
            {
                var width_mobile = parseFloat($group.attr('data-group-mobile-width'));
                var height_mobile = parseFloat($group.attr('data-group-mobile-height'));
                if(width_mobile> width_screen)
                {
                    var height = width_screen*height_mobile/width_mobile;
                    $group.css('height',height+'px');
                    $group.css('width',width_screen+'px');
                }
                else
                {
                    $group.css('height',height_mobile+'px'); 
                    $group.css('width',width_screen+'px');
                }
            }
            else
            {
                if(width_desktop> width_screen)
                {
                    var height = width_screen*height_desktop/width_desktop;
                    $group.css('height',height+'px');
                    $group.css('width',width_screen+'px');
                }
                else
                    $group.css('height',height_desktop+'px');
            }
        }
        if(left>0)
            $group.css('margin-left','-'+left+'px');
    }
    if(layout=='full_screen')
    {
        $group.css('width',width_screen+'px');
        $group.css('height',height_screen+'px');
        if(left>0)
            $group.css('margin-left','-'+left+'px');
    }
    setTimeout(function(){
        if($('#'+$group.attr('id')+' .pg_sl_slide_listitem.active:not(.slide_out) .pg_listitem').length)
        {
            $('#'+$group.attr('id')+' .pg_sl_slide_listitem.active:not(.slide_out) .pg_listitem').each(function(){
                inItem($(this),$group.width(),$group.height(),width_desktop,height_desktop);
            });
        }
    },1000);
}
function inItem($item,width_slide,height_slide,width_desktop,height_desktop)
{
    var time_in =$item.attr('data-item-time-in');
    var effect_in= $item.attr('data-item-effect-in');
    var effect_out= $item.attr('data-item-effect-out');
    var position_top =parseFloat($item.attr('data-item-position-top'));
    var position_left =parseFloat($item.attr('data-item-position-left'));
    var delay_in = $item.attr('data-item-delay-in');
    var scaleSlider = width_slide/width_desktop; //SANG
    //alert(scaleSlider);
    if(width_slide<width_desktop)
    {
        position_left = parseFloat(width_slide)*position_left/parseFloat(width_desktop);
        var font_size= parseFloat($item.attr('data-item-font-size'));
        var padding_bottom= parseFloat($item.attr('data-item-padding-bottom'));
        var padding_right= parseFloat($item.attr('data-item-padding-right'));
        var padding_left= parseFloat($item.attr('data-item-padding-left'));
        var padding_top= parseFloat($item.attr('data-item-padding-top'));
        padding_left = padding_left*width_slide/width_desktop;
        padding_bottom = padding_bottom*width_slide/width_desktop;
        padding_right = padding_right*width_slide/width_desktop;
        padding_top = padding_top*width_slide/width_desktop;
        font_size = font_size*width_slide/width_desktop;
        $item.css('font-size',font_size+'px'); 
        $item.find('a').css('padding-top',padding_top+'px');
        $item.find('a').css('padding-bottom',padding_bottom+'px');
        $item.find('a').css('padding-right',padding_right+'px');
        $item.find('a').css('padding-left',padding_left+'px');
        position_top = scaleSlider*position_top; // SANG
    }  
    else
    {
        position_left =parseFloat(position_left)+ parseFloat((width_slide-width_desktop)/2);
        var font_size= parseFloat($item.attr('data-item-font-size'));
        var padding_bottom= parseFloat($item.attr('data-item-padding-bottom'));
        var padding_right= parseFloat($item.attr('data-item-padding-right'));
        var padding_left= parseFloat($item.attr('data-item-padding-left'));
        var padding_top= parseFloat($item.attr('data-item-padding-top'));
        $item.css('font-size',font_size+'px'); 
        $item.find('a').css('padding-top',padding_top+'px');
        $item.find('a').css('padding-bottom',padding_bottom+'px');
        $item.find('a').css('padding-right',padding_right+'px');
        $item.find('a').css('padding-left',padding_left+'px');
       
    }
    //SANG
    /*if(height_slide <height_desktop)
        //position_top = parseFloat(height_slide)*position_top/parseFloat(height_desktop);
        position_top = scaleSlider*position_top;
    else
        //position_top = position_top+(height_slide-height_desktop)/2;*/
    /*END SANG*/
    setTimeout(function(){
        $item.css('top',position_top+'px');
        $item.css('left',position_left+'px');
        $item.removeClass('pg_sl_'+effect_out);
        $item.addClass('pg_sl_'+effect_in);
        $item.addClass('pg_sl_animated');
        $item.css("animation-duration",time_in+"ms");
        //$item.css("transition-duration",time_in+"ms");
    },parseInt(delay_in));
}
function outItem($item)
{
    var time_out= $item.attr('data-item-time-out');
    var effect_out= $item.attr('data-item-effect-out');
    var effect_in= $item.attr('data-item-effect-in');
    var delay_out = $item.attr('data-item-delay-out');
    setTimeout(function(){
        $item.removeClass('pg_sl_'+effect_in);
        $item.addClass('pg_sl_'+effect_out);
        setTimeout(function(){
            $item.removeClass('pg_sl_animated');
        },time_out);
        $item.css("animation-duration",time_out+"ms");
        $item.css("transition-duration",time_out+"ms");
    },parseInt(delay_out));
}
function checkLopSlider(id_group)
{
    if($('#'+id_group+' .pg_sl_slide_listitem').eq(-1).hasClass('active'))
    {
        if(parseInt($('#'+id_group).attr('data-group-loop'))==1)
        {
            $('#'+id_group).find('.pg_sl_nav_next').show();
        }
        else
            $('#'+id_group).find('.pg_sl_nav_next').hide();
    }
    else
        $('#'+id_group).find('.pg_sl_nav_next').show();
    if($('#'+id_group+' .pg_sl_slide_listitem').eq(0).hasClass('active'))
    {
        if(parseInt($('#'+id_group).attr('data-group-loop'))==1)
    {
        $('#'+id_group).find('.pg_sl_nav_back').show();
    }
    else
        $('#'+id_group).find('.pg_sl_nav_back').hide();
    }
    else
        $('#'+id_group).find('.pg_sl_nav_back').show();
}
function autoLoadSlide(id_group)
{
    if(!$('#'+id_group+' .pg_sl_nav_next').hasClass('notclick'))
    {
        if($('#'+id_group+' .pg_sl_slide_listitem').eq(-1).hasClass('active'))
        {
            if(parseInt($('#'+id_group).attr('data-group-loop'))==1)
            {
                runSlide($('#'+id_group+' .pg_sl_slide_listitem').eq(0));
            }
        }
        else
            runSlide($('#'+id_group+' .pg_sl_slide_listitem.active').next());
    }
}
