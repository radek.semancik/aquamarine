var draggable;
var ratio =1;
$(document).ready(function(){
    ScalePgSlider();
    $(window).load(function(){
        ScalePgSlider();
    });
    $(window).resize(function(){
        ScalePgSlider();
    });
    $(document).on('change','#position_hook',function(){
        if($(this).val()=='custom_hook')
    {
        $(this).parent().find('.help-block').show();
    }
    else
        $(this).parent().find('.help-block').hide();
    });
    if($('#position_hook').val()=='custom_hook')
    {
        $('#position_hook').parent().find('.help-block').show();
    }
    else
        $('#position_hook').parent().find('.help-block').hide();
    makeDrag('.itemslide-draggable');
    updatePositionSlide($(".pg_slider_tab_header"));
    $(document).on('click','button[name="saveConfig"]',function(e){
        $('#form-pg-slide').addClass('reload');
        e.preventDefault(); 
        var formData = new FormData($(this).parents('form').get(0));
        $.ajax({
            url: $(this).parents('form').eq(0).attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(json){
                $('#form-pg-slide').removeClass('reload');
                if(json.error)
                {
                    $.growl.error({ message: json.errors });
                }                
                else
                {
                    $.growl.notice({ message: json.success });
                }
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });
    });
    $(document).on('click','.add_new_slide',function(e){
        e.preventDefault();
        $('#form-pg-slide').addClass('reload');
        $.ajax({
            url: window.location.href ,
            data: 'action=getForm&form=slide',
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $('#pg_form_add_item').removeClass('active');
                $('#pg_form_add_item').html('');
                $('#form-pg-slide').removeClass('reload');
                $('#pg_form_slide').html(json.form);
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });
    });
    $(document).on('click','.pg_button_edit_slide',function(e){
       e.preventDefault();
       $('#form-pg-slide').addClass('reload');
       var id_slide = $(this).attr('data-id-slide');
       $.ajax({
            url: window.location.href ,
            data: 'action=getForm&form=slide&id_slide='+id_slide,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $('#form-pg-slide').removeClass('reload');
                $('#pg_form_add_item').removeClass('active');
                $('#pg_form_add_item').html('');
                $('#pg_form_slide').html(json.form);
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        }); 
    });
    $(document).on('click','.delete_image',function(e){
        e.preventDefault();
        var $img = $(this).parent().find('img');
        $this =$(this);
        $.ajax({
            url: $(this).attr('href'),
            data: 'ajax=1',
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $img.remove();
                $this.remove();
                $.growl.notice({ message: json.success });
            },
            error: function(xhr, status, error)
            {    
                $.growl.error({ message: err.Message });               
            }
        }); 
    });
    $(document).on('click','.pg_tab_slider_item a',function(e){
       e.preventDefault();
       $('#pg_form_add_item').removeClass('active').html('');
       $('#pg_form_slide').removeClass('active').html('');
       $('#pg_listitems').removeClass('active').html('');
       $('#form-pg-slide').addClass('reload');
       $('.pg_tab_slider_item a').removeClass('active');
       $(this).addClass('active');
       var id_slide = $(this).attr('data-id-slide');
       $.ajax({
            url: window.location.href ,
            data: 'action=getForm&form=slideitem&id_slide='+id_slide,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $('#form-pg-slide').removeClass('reload');
                $('#list-slide-content').html(json.form);
                makeDrag('.itemslide-draggable');
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        }); 
    });
    $(document).on('click','button[name="backListItem"]',function(e){
        e.preventDefault();
        $('.pg_button_viewlist').click();
    });
    $(document).on('click','.pg_button_viewlist',function(e){
       e.preventDefault();
       $('#form-pg-slide').addClass('reload');
       var id_slide = $(this).attr('data-id-slide');
       $.ajax({
            url: window.location.href ,
            data: 'action=getForm&form=getslideitem&id_slide='+id_slide,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $('#form-pg-slide').removeClass('reload');
                $('#pg_listitems').html(json.form);
                updatePositionSlideItem($('.pg_listitems_content'));
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        }); 
    });
    $(document).on('click','.pg_button_addtext',function(e){
       e.preventDefault();
       $('#form-pg-slide').addClass('reload');
       var id_slide = $(this).attr('data-id-slide');
       $.ajax({
            url: window.location.href ,
            data: 'action=getForm&form=addtextitem&id_slide='+id_slide,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $('#pg_form_slide').removeClass('active');
                $('#pg_form_slide').html('');
                $('#form-pg-slide').removeClass('reload');
                $('#pg_form_add_item').html(json.form);
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        }); 
    });
    $(document).on('click','.pg_button_active',function(e){
        e.preventDefault();
        $('#form-pg-slide').addClass('reload');
        var id_slide = $(this).attr('data-id-slide');
        $.ajax({
            url: window.location.href ,
            data: 'action=updateactiveslide&id_slide='+id_slide,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $.growl.notice({ message: json.success });
                $('#form-pg-slide').removeClass('reload');
                $('#list-slide-content').html(json.list_item);
                makeDrag('.itemslide-draggable');
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });
    });
    $(document).on('click','.pg_button_dellayer',function(e){
        e.preventDefault();
        $('#form-pg-slide').addClass('reload');
        var id_slide = $(this).attr('data-id-slide');
        $.ajax({
            url: window.location.href ,
            data: 'action=deleteslide&id_slide='+id_slide,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $.growl.notice({ message: json.success });
                $('#form-pg-slide').removeClass('reload');
                $('.pg_slider_tab_header').html(json.list_slide);
                $('#list-slide-content').html(json.list_item);
                makeDrag('.itemslide-draggable');
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });
    });
    $(document).on('click','.pg_listitem_dellayer',function(e){
        e.preventDefault();
        $('#form-pg-slide').addClass('reload');
        var id_item = $(this).attr('data-id-item');
        $.ajax({
            url: window.location.href ,
            data: 'action=deleteSlideItem&id_item='+id_item,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $.growl.notice({ message: json.success });
                $('#form-pg-slide').removeClass('reload');
                $('#list-slide-content').html(json.list_item);
                makeDrag('.itemslide-draggable');
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });
    });
    $(document).on('click','button[name="saveItem"]',function(e){
        e.preventDefault();
        $('#form-pg-slide').addClass('reload');
        var formData = new FormData($(this).parents('form').get(0));
        $.ajax({
            url: $(this).parents('form').eq(0).attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(json){
                $('#form-pg-slide').removeClass('reload');
                if(json.error)
                {
                    $.growl.error({ message: json.errors });
                }                
                else
                {
                    $.growl.notice({ message: json.success });
                    $('#list-slide-content').html(json.list_item);
                    $('input[name="id_item"]').val(json.id_slide_item);
                    makeDrag('.itemslide-draggable');
                }
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });              
            }
        });
    });
    $(document).on('click','.pg_button_addimg',function(e){
       e.preventDefault();
       $('#form-pg-slide').addClass('reload');
       var id_slide = $(this).attr('data-id-slide');
       $.ajax({
            url: window.location.href ,
            data: 'action=getForm&form=addimgitem&id_slide='+id_slide,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $('#pg_form_add_item').html(json.form);
                $('#form-pg-slide').removeClass('reload');
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        }); 
    });
    $(document).on('click','.pg_button_addbutton',function(e){
       e.preventDefault();
       $('#form-pg-slide').addClass('reload');
       var id_slide = $(this).attr('data-id-slide');
       $.ajax({
            url: window.location.href ,
            data: 'action=getForm&form=addbuttonitem&id_slide='+id_slide,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $('#pg_form_add_item').html(json.form);
                $('#form-pg-slide').removeClass('reload');
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        }); 
    });
    $(document).on('click','.pg_listitem_edit',function(e){
       e.preventDefault();
       $('#form-pg-slide').addClass('reload');
       var id_slide = $(this).attr('data-id-slide');
       var id_item = $(this).attr('data-id-item');
       var type=$(this).attr('data-type');
       $.ajax({
            url: window.location.href ,
            data: 'action=getForm&form=add'+type+'item&id_slide='+id_slide+'&id_item='+id_item,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                $('#pg_form_add_item').html(json.form);
                $('#form-pg-slide').removeClass('reload');
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        }); 
    });
    $(document).on('click','.pg_button_coppy',function(e){
        e.preventDefault();
        $('#form-pg-slide').addClass('reload');
        var id_slide = $(this).attr('data-id-slide');
        $.ajax({
            url: window.location.href ,
            data: 'action=duplicateSlide&id_slide='+id_slide,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                if(json.error)
                {
                    $.growl.error({ message: json.errors });
                }                
                else
                {
                    $.growl.notice({ message: json.success });
                    $('.pg_slider_tab_header').html(json.list_slide);
                    $('#list-slide-content').html(json.list_item);
                    makeDrag('.itemslide-draggable');
                }
                $('#form-pg-slide').removeClass('reload');
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });
    });
    $(document).on('click','.pg_listitem_coppy',function(e){
        e.preventDefault();
        $('#form-pg-slide').addClass('reload');
        var id_item = $(this).attr('data-id-item');
        $.ajax({
            url: window.location.href ,
            data: 'action=duplicateSlideItem&id_item='+id_item,
            type: 'post',
            async: true,
			cache: false,
			dataType : "json",
            success: function(json){
                if(json.error)
                {
                    $.growl.error({ message: json.errors });
                }                
                else
                {
                    $.growl.notice({ message: json.success });
                    $('#list-slide-content').html(json.list_item);
                    makeDrag('.itemslide-draggable');
                }
                $('#form-pg-slide').removeClass('reload');
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });
    });
    $(document).on('click','button[name="saveSlide"]',function(e){
        e.preventDefault();
        $('#form-pg-slide').addClass('reload');
        var formData = new FormData($(this).parents('form').get(0));
        $.ajax({
            url: $(this).parents('form').eq(0).attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(json){
                $('#form-pg-slide').removeClass('reload');
                if(json.error)
                {
                    $.growl.error({ message: json.errors });
                }                
                else
                {
                    $.growl.notice({ message: json.success });
                    $('.pg_slider_tab_header').html(json.list_slide);
                    $('#list-slide-content').html(json.list_item);
                    makeDrag('.itemslide-draggable');
                    updatePositionSlide($(".pg_slider_tab_header"));
                }
            },
            error: function(xhr, status, error)
            {
                $('#form-pg-slide').removeClass('reload');
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });
    });
    $(document).on('click','.pg_close_form_parent',function(){
       $(this).parent().parent().parent().toggleClass('active');
    });
    $(document).on('click','.pg_close_list_parent',function(){
       $(this).parent().parent().toggleClass('active');
    });
    $(document).on('click','.pg_config_button, .pg_close_form_config',function(){
       $('.pg_config_slider').toggleClass('active');
    });
    $(document).on('click','.pg_button_layer',function(){
        $('.pg_form_fixed').removeClass('active');
        var data_show = $(this).attr('href');
        $('body').find(data_show).toggleClass('active');
    });
    $(document).on('click','.pg_form_tab_header a',function(){
        var data_show1 = $(this).attr('href');
        $('.pg_form_tab_header a').removeClass('active');
        $(this).addClass('active');
        $(this).parent().parent().find('.pg_form_tab_content').removeClass('active');
        $(this).parent().parent().find(data_show1).toggleClass('active');
    });
    $(document).on('click','.pg_listitem_button',function(){
        $('.pg_form_fixed').removeClass('active');
        var data_show2 = $(this).attr('href');
        $('.pg_listitems, .pg_form_fixed').removeClass('active');
        $('body').find(data_show2).toggleClass('active');
    });
    $(document).on('change','#pg_form_add_item #effect_in',function(){
       
        var id_item= $('input[name="id_item"]').val();
        var effect_in = 'pg_sl_'+$(this).val();
        $('#itemslide-'+id_item).addClass(effect_in);
        $('#itemslide-'+id_item).css("animation-duration","1500ms");
        setTimeout(function(){
            $('#itemslide-'+id_item).removeClass(effect_in);
        },1500);
    });
    $(document).on('change','#pg_form_add_item #effect_out',function(){
       
        var id_item= $('input[name="id_item"]').val();
        var effect_out = 'pg_sl_'+$(this).val();
        $('#itemslide-'+id_item).addClass(effect_out);
        $('#itemslide-'+id_item).css("animation-duration","1500ms");
        setTimeout(function(){
            $('#itemslide-'+id_item).removeClass(effect_out);
        },1500);
    });
    $(document).on('change','#pg_form_slide #effect_in',function(){
       
        var id_slide= $('input[name="id_slide"]').val();
        var effect_in = 'pg_sl_'+$(this).val();
        $('#slider-'+id_slide).addClass(effect_in);
        $('#slider-'+id_slide).css("animation-duration","1500ms");
        setTimeout(function(){
            $('#slider-'+id_slide).removeClass(effect_in);
        },1500);
    });
    $(document).on('change','#pg_form_slide #effect_out',function(){
        var id_slide= $('input[name="id_slide"]').val();
        var effect_out = 'pg_sl_'+$(this).val();
        $('#slider-'+id_slide).addClass(effect_out);
        $('#slider-'+id_slide).css("animation-duration","1500ms");
        setTimeout(function(){
            $('#slider-'+id_slide).removeClass(effect_out);
        },1500);
    });
});

function ScalePgSlider(){
    $(".row_lookbook_addnote").each(function() {
        ratio= $(this).width()/$(this).find('.lookbook_addnoteform').width();
        var height = ratio*$(this).find('.lookbook_addnoteform').height();
        if ( ratio <= 1 ){
            var buttonscale = 2 - ratio;
        } else {
            var buttonscale = ratio - 1;
        }
        if($(this).width() < $(this).find('.lookbook_addnoteform').width())
        {            
            $(this).find('.lookbook_addnote_content').css('height',height+'px');
            $(this).find('.lookbook_addnoteform').css('transform', 'scale('+ratio+')');
            $('.lookbook_addnoteform .draggable').css('transform', 'scale('+buttonscale+')');
        }
        else
        {
            $(this).find('.lookbook_addnoteform').css('transform', '');
            $(this).find('.lookbook_addnote_content').css('height','auto');
        }
    });
}
function makeDrag(selector)
{
    
    var element = $(selector);
    var click = {
            x: 0,
            y: 0
        };
    element.draggable({
        cursor: "move",
        create: function( event, ui ) {
        },
        start: function( event, ui ) {
            click.x = event.clientX;
            click.y = event.clientY;
            ui.helper.css('right','auto');
            element.attr('data-left',ui.position.left).attr('data-top',ui.position.top);
        },
        stop: function( event, ui ) {
            id_item= ui.helper.attr('data-id-item');
            $.ajax({
                url: '',
                data:'action=updatePositionItemSlide&left='+ui.position.left+'&top='+ui.position.top+'&id_item='+id_item,
                type: 'post',
                dataType: 'json',
                success: function(json){
                    if(json.error)
                    {
                        $.growl.error({ message: json.errors });
                    }                
                    else
                    {
                        $.growl.notice({ message: json.success });  
                    }
                },
                error: function(xhr, status, error)
                {
                    var err = eval("(" + xhr.responseText + ")");     
                    $.growl.error({ message: err.Message });               
                }
            });
            element.attr('data-left',ui.position.left).attr('data-top',ui.position.top);
        },
        drag: function(event, ui) {
//            var original = ui.originalPosition;
//            ui.position = {
//                left: (event.clientX - click.x + original.left) / ratio,
//                top:  (event.clientY - click.y + original.top ) / ratio
//            };   
        }
    });
}
function updatePositionSlide($mySlide)
{
	$mySlide.sortable({
		opacity: 0.6,
		cursor: "move",
		update: function() {
			var order = $(this).sortable("serialize") + "&action=updateSlideOrdering";						
			$.post("", order);
		},
    	stop: function( event, ui ) {
  			$.growl.notice({ message: 'Update success'});
   		}
	});
	$mySlide.hover(function() {
		$(this).css("cursor","move");
		},
		function() {
		$(this).css("cursor","auto");
	});
}
function updatePositionSlideItem($myItem)
{
	$myItem.sortable({
		opacity: 0.6,
		cursor: "move",
		update: function() {
			var order = $(this).sortable("serialize") + "&action=updateSlideItemOrdering";						
            $.ajax({
                url: '',
                data:order,
                type: 'post',
                dataType: 'json',
                success: function(json){
                    $.growl.notice({ message: json.success });
                    $('#list-slide-content').html(json.list_item);
                    makeDrag('.itemslide-draggable');
                },
                error: function(xhr, status, error)
                {
                    var err = eval("(" + xhr.responseText + ")");     
                    $.growl.error({ message: err.Message });               
                }
            });
		},
    	stop: function( event, ui ) {
  			
   		}
	});
	$myItem.hover(function() {
		$(this).css("cursor","move");
		},
		function() {
		$(this).css("cursor","auto");
	});
}