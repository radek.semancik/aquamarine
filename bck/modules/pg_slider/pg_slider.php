<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @version  Release: $Revision$
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;
include_once(_PS_MODULE_DIR_.'pg_slider/classes/pg_group_slide.php');
include_once(_PS_MODULE_DIR_.'pg_slider/classes/pg_slide.php');
include_once(_PS_MODULE_DIR_.'pg_slider/classes/pg_slide_item.php');
include_once(_PS_MODULE_DIR_.'pg_slider/classes/SlideExport.php');
class Pg_slider extends Module
{
    private $_html = '';
    public $is17 = false;
    public $animations;
    public $effects;
    public $googlefonts = array();
    public function __construct()
	{
        $this->name = 'pg_slider';
		$this->tab = 'front_office_features';
		$this->version = '1.0.2';
		$this->author = 'prestagold.com';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		$this->bootstrap = true;
        if(version_compare(_PS_VERSION_, '1.7', '>='))
            $this->is17 = true; 
        $this->module_key = 'da314fdf1af6d043f9b2f15dce2bef1e';
		parent::__construct();
        $this->googlefonts = Tools::jsonDecode(Tools::file_get_contents(dirname(__FILE__).'/fonts/google_font_list.txt'),true);
        if(!$this->googlefonts)
        {
            $this->googlefonts = array(
                array(
                    'id' => '',
                    'name' => $this->l('Default font'),
                ),
                array(
                    'id' => 'Arial',
                    'name' => 'Arial',
                ),
                array(
                    'id' => 'Times new roman',
                    'name' => 'Times new roman',
                ),
            );
        }
        $this->animations = array(
           array(
                'id'=>'fadeInUp',
                'name'=>'fadeInUp',
           ),
           array(
                'id'=>'fadeIn',
                'name'=>'fadeIn',
           ),
           array(
                'id'=>'fadeInDown',
                'name'=>'fadeInDown',
           ),
           array(
                'id'=>'fadeInLeft',
                'name'=>'fadeInLeft',
           ),
           array(
                'id'=>'fadeInRight',
                'name'=>'fadeInRight',
           ),
           array(
                'id'=>'zoomIn',
                'name'=>'zoomIn',
           ),
           array(
                'id'=>'slideInUp',
                'name'=>'slideInUp',
           ),
           array(
                'id'=>'slideInDown',
                'name'=>'slideInDown',
           ),
           array(
                'id'=>'slideInLeft',
                'name'=>'slideInLeft',
           ),
           array(
                'id'=>'slideInRight',
                'name'=>'slideInRight',
           ),
           array(
                'id'=>'flipInX',
                'name'=>'flipInX',
           ),
           array(
                'id'=>'flipInY',
                'name'=>'flipInY',
           ),
        );
        $this->effects = array(
           array(
                'id'=>'fadeOutUp',
                'name'=>'fadeOutUp',
           ),
           array(
                'id'=>'fadeOut',
                'name'=>'fadeOut',
           ),
           array(
                'id'=>'fadeOutDown',
                'name'=>'fadeOutDown',
           ),
           array(
                'id'=>'fadeOutLeft',
                'name'=>'fadeOutLeft',
           ),
           array(
                'id'=>'fadeOutRight',
                'name'=>'fadeOutRight',
           ),
           array(
                'id'=>'zoomOut',
                'name'=>'zoomOut',
           ),
           array(
                'id'=>'slideOutUp',
                'name'=>'slideOutUp',
           ),
           array(
                'id'=>'slideOutDown',
                'name'=>'slideOutDown',
           ),
           array(
                'id'=>'slideOutLeft',
                'name'=>'slideOutLeft',
           ),
           array(
                'id'=>'slideOutRight',
                'name'=>'slideOutRight',
           ),
           array(
                'id'=>'flipOutX',
                'name'=>'flipOutX',
           ),
           array(
                'id'=>'flipOutY',
                'name'=>'flipOutY',
           ),
        );
		$this->displayName = $this->l('PrestaGold slider');
		$this->description = $this->l('PrestaGold slider');
		$this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => _PS_VERSION_);
    }
    public function install()
	{
        if(parent::install()&& $this->registerHook('displayHeader') && $this->registerHook('displayBackOfficeHeader')&& $this->registerHook('displayTop') && $this->registerHook('top') && $this->registerHook('home')&& $this->registerHook('displayTopColumn')&& $this->registerHook('pgCustomSlide')&& $this->_installDb())
        {
            $export= new SlideExport();
            $export->importSlide(); 
            return true;
        }
        return false;
    }    
    /**
	 * @see Module::uninstall()
	 */
	public function uninstall()
	{
        return parent::uninstall() && $this->_uninstallDb();
    }
    private function _installDb()
    {
        Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pg_slide_shop` (
        `id_pg_slide` INT(11) NOT NULL , 
        `id_shop` INT(11) NOT NULL ) ENGINE = InnoDB;
        ') && Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pg_slide` ( 
        `id_pg_slide` INT(11) NOT NULL AUTO_INCREMENT , 
        `id_pg_group_slide` INT(11) NOT NULL,
        `active` INT(11) NOT NULL , 
        `backgroud_image` VARCHAR(222) NOT NULL , 
        `backgroud_color` VARCHAR(22) NOT NULL , 
        `repeat` VARCHAR(222) NOT NULL , 
        `background_size` VARCHAR(222) NOT NULL , 
        `background_position` VARCHAR(222) NOT NULL , 
        `effect_in` VARCHAR(22) NOT NULL , 
        `time_in` INT(11) NOT NULL,
        `effect_out` VARCHAR(22) NOT NULL , 
        `time_out` INT(11) NOT NULL,
        `custom_class` VARCHAR(222) NOT NULL , 
        `position` INT(11) NOT NULL , 
        PRIMARY KEY (`id_pg_slide`)) ENGINE = InnoDB;    
        ') && Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pg_slide_lang` ( 
        `id_pg_slide` INT(11) NOT NULL , 
        `id_lang` INT(11) NOT NULL , 
        `title` VARCHAR(222) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
        `link` VARCHAR(222) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
        PRIMARY KEY (`id_pg_slide`, `id_lang`)) ENGINE = InnoDB;
        ')&& Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pg_slide_item` ( 
        `id_pg_slide_item` INT(11) NOT NULL AUTO_INCREMENT ,
        `active` INT(1) NOT NULL , 
        `id_pg_slide` INT(11) NOT NULL , 
        `type` VARCHAR(222) NOT NULL , 
        `font_family` VARCHAR(222) NOT NULL , 
        `font_size` FLOAT(10,2) NOT NULL , 
        `font_style` VARCHAR(22) NOT NULL ,
        `border_radius` VARCHAR(22) NOT NULL ,
        `border_width_top` VARCHAR(22) NOT NULL ,
        `border_width_left` VARCHAR(22) NOT NULL ,
        `border_width_bottom` VARCHAR(22) NOT NULL ,
        `border_width_right` VARCHAR(22) NOT NULL ,
        `border_style` VARCHAR(22) NOT NULL ,
        `border_color` VARCHAR(22) NOT NULL ,
        `border_color_hover` VARCHAR(22) NOT NULL,
        `background_color` VARCHAR(22) NOT NULL , 
        `color` VARCHAR(22) NOT NULL , 
        `hover_color` VARCHAR(22) NOT NULL ,
        `hover_background_color` VARCHAR(22) NOT NULL ,
        `padding_top` FLOAT(10,2) NOT NULL , 
        `padding_right` FLOAT(10,2) NOT NULL , 
        `padding_bottom` FLOAT(10,2) NOT NULL , 
        `padding_left` FLOAT(10,2) NOT NULL , 
        `font_weight` VARCHAR(222) NOT NULL , 
        `text_transform` VARCHAR(222) NOT NULL , 
        `text_decoration` VARCHAR(222) NOT NULL , 
        `custom_class` VARCHAR(222) NOT NULL , 
        `image` VARCHAR(222) NOT NULL , 
        `width` FLOAT(10,2) NOT NULL ,
        `height` FLOAT(10,2) NOT NULL , 
        `position_top` FLOAT(10,2) NOT NULL , 
        `position_left` FLOAT(10,2) NOT NULL , 
        `effect_in` VARCHAR(222) NOT NULL , 
        `delay_in` INT(11) NOT NULL , 
        `time_effect_in` INT(11) NOT NULL , 
        `effect_out` VARCHAR(222) NOT NULL , 
        `delay_out` INT(11) NOT NULL , 
        `time_effect_out` INT(11) NOT NULL , 
        `position` INT(11) NOT NULL,
        PRIMARY KEY (`id_pg_slide_item`)) ENGINE = InnoDB;
        ')&&Db::getInstance()->execute('
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pg_slide_item_lang` ( 
        `id_pg_slide_item` INT(11) NOT NULL , 
        `id_lang` INT(11) NOT NULL , 
        `link` VARCHAR(222) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
        `text_html` VARCHAR(222) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
        `label_button` VARCHAR(222) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
        PRIMARY KEY (`id_pg_slide_item`,`id_lang`)) ENGINE = InnoDB;
        ')&&Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pg_group_slide` (
			  `id_pg_group_slide` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `position_hook` VARCHAR(222),
              `slide_layout` VARCHAR(222),
              `desktop_width` FLOAT(10,2),
              `desktop_height` FLOAT(10,2),
              `notebook_size` INT(1),
              `notebook_width` FLOAT(10,2),
              `notebook_height` FLOAT(10,2),
              `tablet_size` INT(1),
              `tablet_width` FLOAT(10,2),
              `tablet_height` FLOAT(10,2),
              `mobile_size` INT(1),
              `mobile_width` FLOAT(10,2),
              `mobile_height` FLOAT(10,2),
              `background_color` VARCHAR(222),
              `background_image` VARCHAR(222),
              `slide_width` float(10,2),
              `slide_height` float(10,2),
			  `pause_time` int(11) NOT NULL,
              `auto_play` int(1) NOT NULL,
              `pase_when_hover` int(1) NOT NULL,
              `slide_lop` int(1) NOT NULL,
              `slide_nav` int(1) NOT NULL,
              `position_nav` VARCHAR(222),
              `slide_dots` int(1) NOT NULL,
              `position_dots` VARCHAR(222),
              `custom_class` VARCHAR(222),
              `active` int(1) NOT NULL,
			  PRIMARY KEY (`id_pg_group_slide`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		') && Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pg_group_slide_lang` (
			  `id_pg_group_slide` int(10) unsigned NOT NULL,
			  `id_lang` int(10) unsigned NOT NULL,
			  `title` varchar(255) NOT NULL,
			  PRIMARY KEY (`id_pg_group_slide`,`id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		')&& Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pg_group_slide_shop` (
				`id_pg_group_slide` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_shop` int(10) unsigned NOT NULL,
				PRIMARY KEY (`id_pg_group_slide`, `id_shop`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');
        return true;
    }
    private function _uninstallDb()
    {      
        return Db::getInstance()->execute('DROP TABLE IF  EXISTS '._DB_PREFIX_.'pg_slide_shop')&& Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'pg_slide') && Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'pg_slide_lang') && Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'pg_slide_item') && Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'pg_slide_item_lang')&& Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'pg_group_slide')&& Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'pg_group_slide_lang')&& Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'pg_group_slide_shop');
    }
    public function hookDisplayBackOfficeHeader()
    {
        if(Tools::getValue('configure')=='pg_slider')
        {
            $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/font-awesome.min.css','all');
            $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/admin.css','all');
            $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/animate.css','all');
        }
    }
    public function getContent()
	{
        $this->context->smarty->assign(
            array(
                'img_path' => $this->_path.'/views/img/',
                'link_module'=>$this->context->link->getAdminLink('AdminModules').'&configure=pg_slider',
            )
        );
        if(Tools::getValue('export_data')==1)
        {
            $export= new  SlideExport();
            $export->exportSlider();
        }
        if(Tools::getValue('action')=='updateSlideOrdering' && Tools::getValue('slides'))
        {
            foreach(Tools::getValue('slides') as $key=>$value)
            {
                Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'pg_slide SET position="'.(int)$key.'" WHERE id_pg_slide='.(int)$value);
            }
            die('1');
        }
        if(Tools::getValue('action')=='updateSlideItemOrdering' && Tools::getValue('item'))
        {
            $id_pg_slide=0;
            foreach(Tools::getValue('item') as $key=>$value)
            {
                Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'pg_slide_item SET position="'.(int)$key.'" WHERE id_pg_slide_item='.(int)$value);
                if(!$id_pg_slide)
                    $id_pg_slide = Db::getInstance()->getValue('SELECT id_pg_slide FROM '._DB_PREFIX_.'pg_slide_item WHERE id_pg_slide_item='.(int)$value);
            }
            die(Tools::jsonEncode(
                array(
                    'success' => $this->l('successfully updated'),
                    'list_item'=> $this->getFormItemSlide($id_pg_slide),
                )
            ));
        }
        
        if(Tools::getValue('delete_image_item')&&$id_item=(int)Tools::getValue('id_item'))
        {
            $image= Db::getInstance()->getValue('SELECT image FROM '._DB_PREFIX_.'pg_slide_item where id_pg_slide_item='.(int)$id_item);
            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'pg_slide_item SET image="" WHERE id_pg_slide_item='.(int)$id_item);
            if (file_exists(dirname(__FILE__).'/../views/img/item/'.$image))
			     @unlink(dirname(__FILE__).'/../views/img/item/'.$image);
            if(Tools::isSubmit('ajax')&&Tools::getValue('ajax')==1)
                die(Tools::jsonEncode(
                    array(
                        'success' => $this->l('successfully image deleted'),
                    )
                ));
        }
        if(Tools::isSubmit('delete_image_group') && $id_group=(int)Tools::getValue('id_pg_group_slide'))
        {
            $background_image = Db::getInstance()->getValue('SELECT background_image FROM '._DB_PREFIX_.'pg_group_slide WHERE id_pg_group_slide='.(int)$id_group);
            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'pg_group_slide set background_image="" WHERE id_pg_group_slide='.(int)$id_group);
            if (file_exists(dirname(__FILE__).'/../views/img/item/'.$background_image))
			     @unlink(dirname(__FILE__).'/../views/img/item/'.$background_image);
            if(Tools::isSubmit('ajax')&&Tools::getValue('ajax')==1)
            die(Tools::jsonEncode(
                array(
                    'success' => $this->l('successfully image deleted'),
                )
            ));
        }
        if(Tools::isSubmit('id_pg_group_slide')&& $id_group= (int)Tools::getValue('id_pg_group_slide'))
        {
            $groupSlide= new Pg_group_slide($id_group);
            $this->context->smarty->assign(
                array(
                    'width_slide' =>$groupSlide->desktop_width,
                    'height_slide' =>$groupSlide->desktop_height,
                )
            );
        }
        if(Tools::getValue('action')=='getForm' && $form=Tools::getValue('form'))
            $this->getFormAjax($form);
        if(Tools::getValue('action')=='updatePositionItemSlide')
        {
            $top= Tools::getValue('top');
            $left= Tools::getValue('left');
            $id_slide_item= Tools::getValue('id_item');
            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'pg_slide_item set position_top="'.$top.'", position_left="'.$left.'" WHERE id_pg_slide_item='.(int)$id_slide_item);
            die(Tools::jsonEncode(
                array(
                    'success' => $this->l('successfully updated'),
                    'id_slide_item'=> $id_slide_item,
                )
            ));            
        }
        if(Tools::getValue('action')=='duplicateSlide' && $id_slide =Tools::getValue('id_slide'))
        {
            $this->duplicateSlide($id_slide);
        }
        if(Tools::getValue('action')=='duplicateSlideItem' && $id_item =Tools::getValue('id_item'))
        {
            $this->duplicateSlideItem($id_item);
        }
        if(Tools::isSubmit('delete_id_pg_group_slide') && $id_group=Tools::getValue('delete_id_pg_group_slide'))
        {
            $groupSlide  = new Pg_group_slide($id_group);
            $slides = $this->getSlides($id_group);
            if($slides)
            {
                foreach($slides as $slide)
                {
                    $pg_slide= new Pg_slide($slide['id_pg_slide']);
                    $slideItems = $this->getItemSlide($slide['id_pg_slide']);
                    foreach($slideItems as $slideItem)
                    {
                        $pg_slide_item = new Pg_slide_item($slideItem['id_pg_slide_item']);
                        $pg_slide_item->delete();
                    }
                    $pg_slide->delete();
                }
            }
            $groupSlide->delete();
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules',true).'&configure='.$this->name.'&conf=2');
        }
        if(Tools::getValue('action')=='deleteslide' && $id_slide= Tools::getValue('id_slide'))
        {
            $pg_slide= new Pg_slide($id_slide);
            $slideItems = $this->getItemSlide($id_slide);
            foreach($slideItems as $slideItem)
            {
                $pg_slide_item = new Pg_slide_item($slideItem['id_pg_slide_item']);
                $pg_slide_item->delete();
            }
            $pg_slide->delete();
            $slides = $this->getSlides();
            if($slides)
                $id_slide_first =$slides[0]['id_pg_slide'];
            else
                $id_slide_first=0;  
            die(Tools::jsonEncode(
                array(
                    'success' => $this->l('successfully deleted'),
                    'list_slide'=> $this->updateListSlide($id_slide_first),
                    'list_item'=> $this->getFormItemSlide($id_slide_first),
                )
            ));  
        }
        if(Tools::getValue('action')=='updateactiveslide' && $id_slide=(int)Tools::getValue('id_slide'))
        {
            $pg_slide = new Pg_slide($id_slide);
            $pg_slide->active =!(int)$pg_slide->active;
            $pg_slide->update();
            die(Tools::jsonEncode(
                array(
                    'success' => $this->l('successfully updated'),
                    'list_item'=> $this->getFormItemSlide($id_slide),
                )
            ));
        }
        if(Tools::getValue('action')=='deleteSlideItem'&& $id_item=Tools::getValue('id_item'))
        {
            $pg_slide_item = new Pg_slide_item($id_item);
            $id_slide = $pg_slide_item->id_pg_slide;
            $pg_slide_item->delete();
            die(Tools::jsonEncode(
                array(
                    'success' => $this->l('successfully deleted'),
                    'list_item'=> $this->getFormItemSlide($id_slide),
                )
            ));
        }
        if(Tools::isSubmit('saveSlide') || Tools::isSubmit('saveConfig') || Tools::isSubmit('saveItem') || Tools::isSubmit('submitSaveGroupSlide'))
        {
                    
            if($this->_postValidation())
            {
                $this->_postProcess();
            }
        }
        $this->context->controller->addJqueryUI('ui.sortable');
        $this->context->controller->addJqueryUI('ui.draggable');   
        $this->_html .= $this->displayAdminJs();
        $this->renderMain();
        return $this->_html;
    }
    public function _postValidation()
    {
        return true;
    }
    public function _postProcess()
    {
        if(Tools::isSubmit('saveSlide'))
        {
            $this->_postSlide();
        }
        if(Tools::isSubmit('saveConfig'))
        {
            $this->_postConfig();
        }
        if(Tools::isSubmit('saveItem'))
        {
            $this->_postItem();
        }
        if(Tools::isSubmit('submitSaveGroupSlide'))
        {
            $this->_postGroupSlide();
        }
    }
    public function _postGroupSlide()
    {
        if($id_pg_group_slide=(int)Tools::getValue('id_pg_group_slide'))
            $groupSlide= new Pg_group_slide($id_pg_group_slide);
        else
            $groupSlide= new Pg_group_slide();
        $languages= Language::getLanguages(false);
        foreach($languages as $language)
        {
            $groupSlide->title[$language['id_lang']]= Tools::getValue('title_'.$language['id_lang'],'title_'.Configuration::get('PS_LANG_DEFAULT'));
        }
        $groupSlide->position_hook = Tools::getValue('position_hook');
        $groupSlide->slide_layout =Tools::getValue('slide_layout');
        $groupSlide->background_color = Tools::getValue('background_color');
        $groupSlide->desktop_width = (float)Tools::getValue('desktop_width');
        $groupSlide->desktop_height = (float)Tools::getValue('desktop_height');
        $groupSlide->pause_time = (int)Tools::getValue('pause_time');
        $groupSlide->time_out = (int)Tools::getValue('time_out');
        $groupSlide->auto_play = (int)Tools::getValue('auto_play');
        $groupSlide->pase_when_hover = (int)Tools::getValue('pase_when_hover');
        $groupSlide->slide_lop = (int)Tools::getValue('slide_lop');
        $groupSlide->slide_nav = (int)Tools::getValue('slide_nav');
        $groupSlide->position_nav =Tools::getValue('position_nav');
        $groupSlide->slide_dots = (int)Tools::getValue('slide_dots');
        $groupSlide->position_dots =Tools::getValue('position_dots');
        $groupSlide->custom_class =Tools::getValue('custom_class');
        $groupSlide->active = (int)Tools::getValue('active');
        if(isset($_FILES['background_image'])&& $_FILES['background_image']['name'])
        {
            $background_image = $this->uploadFile('background_image','group');
            if($groupSlide->background_image)
            {
                $oldImage = dirname(__FILE__).'/views/img/group/'.$groupSlide->background_image;
                if(file_exists($oldImage))
                    @unlink($oldImage);
            }
            $groupSlide->background_image= $background_image;
        }
        if($id_pg_group_slide=(int)Tools::getValue('id_pg_group_slide'))
            $groupSlide->update();
        else
            $groupSlide->add();
    }
    public function _postItem()
    {
        if($id_item=Tools::getValue('id_item'))
            $pg_slide_item = new Pg_slide_item($id_item);
        else
        {
            $position_max = Db::getInstance()->getValue('SELECT MAX(position) FROM '._DB_PREFIX_.'pg_slide_item WHERE id_pg_slide='.(int)Tools::getValue('id_slide'));
            $pg_slide_item = new Pg_slide_item();
            $pg_slide_item->position = $position_max+1;
        }
        $pg_slide_item->type = Tools::getValue('type');
        $pg_slide_item->font_family = Tools::getValue('font_family');
        $pg_slide_item->font_size = Tools::getValue('font_size');
        $pg_slide_item->font_style = Tools::getValue('font_style');
        $pg_slide_item->border_radius = Tools::getValue('border_radius');
        $pg_slide_item->border_width_top = Tools::getValue('border_width_top');
        $pg_slide_item->border_width_left = Tools::getValue('border_width_left');
        $pg_slide_item->border_width_bottom = Tools::getValue('border_width_bottom');
        $pg_slide_item->border_width_right = Tools::getValue('border_width_right');
        $pg_slide_item->border_style = Tools::getValue('border_style');
        $pg_slide_item->border_color = Tools::getValue('border_color');
        $pg_slide_item->border_color_hover = Tools::getValue('border_color_hover');
        $pg_slide_item->background_color = Tools::getValue('it_background_color');
        $pg_slide_item->color = Tools::getValue('color');
        $pg_slide_item->hover_color = Tools::getValue('hover_color');
        $pg_slide_item->hover_background_color = Tools::getValue('hover_background_color');
        $pg_slide_item->padding_top = Tools::getValue('padding_top');
        $pg_slide_item->padding_right = Tools::getValue('padding_right');
        $pg_slide_item->padding_bottom = Tools::getValue('padding_bottom');
        $pg_slide_item->padding_left = Tools::getValue('padding_left');
        $pg_slide_item->font_weight = Tools::getValue('font_weight');
        $pg_slide_item->text_transform = Tools::getValue('text_transform');
        $pg_slide_item->text_decoration= Tools::getValue('text_decoration');
        $pg_slide_item->custom_class= Tools::getValue('custom_class');
        $pg_slide_item->position_top = Tools::getValue('position_top');
        $pg_slide_item->position_left = Tools::getValue('position_left');
        $pg_slide_item->effect_in = Tools::getValue('effect_in');
        $pg_slide_item->delay_in= Tools::getValue('delay_in');
        $pg_slide_item->time_effect_in= Tools::getValue('time_effect_in');
        $pg_slide_item->effect_out= Tools::getValue('effect_out');
        $pg_slide_item->delay_out = Tools::getValue('delay_out');
        $pg_slide_item->time_effect_out= Tools::getValue('time_effect_out');
        $pg_slide_item->id_pg_slide = Tools::getValue('id_slide');
        $pg_slide_item->width = Tools::getValue('width');
        $pg_slide_item->height = Tools::getValue('height');
        $pg_slide_item->active = Tools::getValue('active');
        if(isset($_FILES['image'])&& $_FILES['image']['name'])
        {
            $item_image= $this->uploadFile('image','item');
            if($pg_slide_item->image)
            {
                $oldImage = dirname(__FILE__).'/views/img/slide/'.$pg_slide_item->image;
                if(file_exists($oldImage))
                    @unlink($oldImage);
            }
            $pg_slide_item->image= $item_image;
        }
        $languages= Language::getLanguages(false);
        foreach($languages as $language)
        {
            $pg_slide_item->text_html[$language['id_lang']] = Tools::getValue('text_html_'.$language['id_lang']);
            $pg_slide_item->link[$language['id_lang']] = Tools::getValue('link_'.$language['id_lang']);
            $pg_slide_item->label_button[$language['id_lang']] = Tools::getValue('label_button_'.$language['id_lang']);
        }
        if(Tools::getValue('id_item'))
        {
            if($pg_slide_item->update())
            {
                die(Tools::jsonEncode(
                    array(
                        'success' => $this->l('successfully updated'),
                        'id_slide_item'=> $pg_slide_item->id,
                        'list_item'=> $this->getFormItemSlide($pg_slide_item->id_pg_slide),
                    )
                ));
            }
        }
        else
        {
            if($pg_slide_item->add())
            {
                die(Tools::jsonEncode(
                    array(
                        'success' => $this->l('successfully added'),
                        'id_slide_item'=> $pg_slide_item->id,
                        'list_item'=> $this->getFormItemSlide($pg_slide_item->id_pg_slide),
                    )
                ));
            }
        }
    }
    public function _postSlide()
    {
        if($id_slide= Tools::getValue('id_slide'))
            $slide= new Pg_slide($id_slide);
        else
        {
            $slide = new Pg_slide();
            $position_max = (int)Db::getInstance()->getValue('SELECT MAX(position) FROM '._DB_PREFIX_.'pg_slide WHERE id_pg_group_slide='.(int)Tools::getValue('id_pg_group_slide'));
            $slide->position = $position_max+1;
        }
        $slide->backgroud_color= Tools::getValue('backgroud_color');
        $slide->repeat_x = (int)Tools::getValue('repeat_x');
        $slide->repeat_y = (int)Tools::getValue('repeat_y');
        $slide->effect_in = Tools::getValue('effect_in');
        $slide->time_in =(int)Tools::getValue('time_in');
        $slide->time_out =(int)Tools::getValue('time_out');
        $slide->effect_out =Tools::getValue('effect_out');
        $slide->custom_class = Tools::getValue('custom_class');
        $slide->id_pg_group_slide=Tools::getValue('id_pg_group_slide');
        $slide->active = Tools::getValue('active');
        $languages = Language::getLanguages(false);
        if(isset($_FILES['backgroud_image'])&&$_FILES['backgroud_image']['name'])
        {
            $background_image = $this->uploadFile('backgroud_image','slide');
            if($slide->backgroud_image)
            {
                $oldImage = dirname(__FILE__).'/views/img/slide/'.$slide->backgroud_image;
                if(file_exists($oldImage))
                    @unlink($oldImage);
            }
            $slide->backgroud_image= $background_image;
        }
        foreach($languages as $language)
        {
            $slide->title[$language['id_lang']] = Tools::getValue('title_'.$language['id_lang']);
            $slide->link[$language['id_lang']] = Tools::getValue('link_'.$language['id_lang']);
        }
        if(Tools::getValue('id_slide'))
        {
            if($slide->update())
            {
                die(Tools::jsonEncode(
                    array(
                        'success' => $this->l('successfully updated'),
                        'id_slide'=> $slide->id,
                        'list_slide'=> $this->updateListSlide($slide->id),
                        'list_item'=> $this->getFormItemSlide($slide->id),
                    )
                ));
            }
        }
        else
        {
            if($slide->add())
            {
                die(Tools::jsonEncode(
                    array(
                        'success' => $this->l('successfully added'),
                        'id_slide'=> $slide->id,
                        'list_slide'=> $this->updateListSlide($slide->id),
                        'list_item'=> $this->getFormItemSlide($slide->id),
                    )
                ));
            }
        }
    }
    public function _postConfig()
    {
        $group_slide= new Pg_group_slide((int)Tools::getValue('id_pg_group_slide'));
        $group_slide->position_hook =Tools::getValue('position_hook');
        $group_slide ->slide_layout= Tools::getValue('slide_layout');
        $group_slide->desktop_width =(float)Tools::getValue('desktop_width');
        $group_slide->desktop_height =(float)Tools::getValue('desktop_height');
        $group_slide->notebook_size =(int)Tools::getValue('notebook_size');
        $group_slide->notebook_width =(float)Tools::getValue('notebook_width');
        $group_slide->notebook_height =(float)Tools::getValue('notebook_height');
        $group_slide->tablet_size =(int)Tools::getValue('tablet_size');
        $group_slide->tablet_width =(float)Tools::getValue('tablet_width');
        $group_slide->tablet_height =(float)Tools::getValue('tablet_height');
        $group_slide->mobile_size =(int)Tools::getValue('mobile_size');
        $group_slide->mobile_width =(float)Tools::getValue('mobile_width');
        $group_slide->mobile_height =(float)Tools::getValue('mobile_height');
        $group_slide->position_hook =Tools::getValue('position_hook');
        $group_slide->background_color =Tools::getValue('background_color');
        $group_slide->slide_width =(float)Tools::getValue('slide_width');
        $group_slide->slide_height =(float)Tools::getValue('slide_height');
        $group_slide->pause_time =(int)Tools::getValue('pause_time');
        $group_slide->auto_play =(int)Tools::getValue('auto_play');
        $group_slide->pase_when_hover =(int)Tools::getValue('pase_when_hover');
        $group_slide->slide_lop =(int)Tools::getValue('slide_lop');
        $group_slide->slide_nav =(int)Tools::getValue('slide_nav');
        $group_slide->position_nav =Tools::getValue('position_nav');
        $group_slide->slide_dots =(int)Tools::getValue('slide_dots');
        $group_slide->position_dots =Tools::getValue('position_dots');
        $group_slide->custom_class =Tools::getValue('custom_class');
        $group_slide->active =(int)Tools::getValue('active_group');
        if(isset($_FILES['background_image'])&&$_FILES['background_image']['name'])
        {
            $background_image = $this->uploadFile('background_image','group');
            if($group_slide->background_image)
            {
                $oldImage = dirname(__FILE__).'/views/img/group/'.$group_slide->background_image;
                if(file_exists($oldImage))
                    @unlink($oldImage);
            }
            $group_slide->background_image= $background_image;
        }
        $group_slide->update();
        die(
            Tools::jsonEncode(
                array(
                    'success' => $this->l('successfully updated'),
                    'id_slide'=> $slide->id,
                )
            )
        );
    }
    public function displayAdminJs()
    {
        $this->smarty->assign(array(
            'js_dir_path' => $this->_path.'views/js/',
        ));
        return $this->display(__FILE__,'admin-js.tpl');
    }
    public function renderMain()
    {
        if(Tools::isSubmit('viewGroup')&& $id_group= (int)Tools::getValue('id_pg_group_slide'))
        {
            $slides = $this->getSlides();
            $this->context->smarty->assign(
                array(
                    'form_config' => $this->renderFormConfig(),
                    'slides'=> $slides,
                    
                    'itemsSlide' => $slides? $this->getItemSlide($slides[0]['id_pg_slide']):array(),
                )
            );
            $this->_html .= $this->display(__FILE__,'main_slide.tpl');
        }
        else
        {
            if(Tools::isSubmit('addGroupSlide')||Tools::isSubmit('editGroup'))
            {
                $this->_html = $this->renderAddGroupForm();
            }
            else
            {
                $this->_html .= $this->renderListGroupSlider();
            }
        }
        
    }
    public function renderAddGroupForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Group slide information'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Group title'),
						'name' => 'title',
						'required' => true,
						'lang' => true,
					),
					array(
						'type' => 'select',
						'label' => $this->l('Position hook'),
						'name' => 'position_hook',
                        'desc' => $this->l('Put {hook h=\'pgCustomSlide\'} to file tpl'),
                        'options' => array(
                			'query' => array(
                                array(
                                    'id'=>'home',
                                    'name'=> $this->l('Home'),
                                ),
                                array(
                                    'id'=>'custom_hook',
                                    'name'=> $this->l('Custom hook'),
                                )
                            ),
                			'id' => 'id',
                			'name' => 'name'
                        )
					),
                    array(
                        'type'=>'select',
                        'label'=> $this->l('Slide layout'),
                        'name'=>'slide_layout',
                        'options' => array(
                            'query'=>array(
                                   array(
                                        'name'=>$this->l('Auto'),
                                        'id'=>'auto',
                                   ), 
                                   array(
                                        'name'=>$this->l('Full Width'),
                                        'id'=>'full_width',
                                   ),
                                   array(
                                        'name'=>$this->l('Full Screen'),
                                        'id'=>'full_screen',
                                   ),
                            ),
                            'id' => 'id',
                			'name' => 'name'
                        )
                    ),
                    array(
						'type' => 'color',
						'label' => $this->l('Background color'),
						'name' => 'background_color',
					),
                    array(
                        'type'=>'text',
                        'name'=>'desktop_width',
                        'label' => $this->l('Desktop width'),
                        'suffix' =>'px',
                    ),
                    array(
                        'type'=>'text',
                        'name'=>'desktop_height',
                        'label' => $this->l('Desktop height'),
                        'suffix' => 'px',
                    ),
                    array(
                        'type'=>'text',
                        'name'=>'pause_time',
                        'label'=> $this->l('Pause time'),
                        'suffix'=> 'millisecond',
                        'desc'=>$this->l('Time pauses between 2 slide (includes runtime items)')
                    ),
                    array(
                		'type' => 'switch',
                		'label' => $this->l('Auto play'),
                		'name' => 'auto_play',
                        'default' =>'1',
                		'values' => array(
                			array(
                				'id' => 'active_on',
                				'value' => 1,
                				'label' => $this->l('Enabled')
                			),
                			array(
                				'id' => 'active_off',
                				'value' => 0,
                				'label' => $this->l('Disabled')
                			)
                		),
                	),
                    array(
                		'type' => 'switch',
                		'label' => $this->l('Pase when hover'),
                		'name' => 'pase_when_hover',
                        'default' =>'1',
                		'values' => array(
                			array(
                				'id' => 'active_on',
                				'value' => 1,
                				'label' => $this->l('Enabled')
                			),
                			array(
                				'id' => 'active_off',
                				'value' => 0,
                				'label' => $this->l('Disabled')
                			)
                		),
                	),
                    array(
                		'type' => 'switch',
                		'label' => $this->l('Slide lop'),
                		'name' => 'slide_lop',
                        'default' =>'1',
                		'values' => array(
                			array(
                				'id' => 'active_on',
                				'value' => 1,
                				'label' => $this->l('Enabled')
                			),
                			array(
                				'id' => 'active_off',
                				'value' => 0,
                				'label' => $this->l('Disabled')
                			)
                		),
                	),
                    array(
                		'type' => 'switch',
                		'label' => $this->l('Slide nav'),
                		'name' => 'slide_nav',
                        'default' =>'1',
                		'values' => array(
                			array(
                				'id' => 'active_on',
                				'value' => 1,
                				'label' => $this->l('Enabled')
                			),
                			array(
                				'id' => 'active_off',
                				'value' => 0,
                				'label' => $this->l('Disabled')
                			)
                		),
                	),
                    array(
                        'type'=>'select',
                        'label'=> $this->l('Position nav'),
                        'name'=>'position_nav',
                        'options' => array(
                            'query'=>array(
                                    array(
                                        'name'=>$this->l('Default'),
                                        'id'=>'default'
                                    ),
                                    array(
                                        'name'=>$this->l('Bottom left'),
                                        'id'=>'botton_left'
                                    ),
                                    array(
                                        'name'=>$this->l('Bottom right'),
                                        'id'=>'bottom_right'
                                    ),
                                    array(
                                        'name'=>$this->l('Bottom center'),
                                        'id'=>'bottom_center'
                                    )
                            ),
                            'id' => 'id',
                			'name' => 'name'
                        )
                    ),
                    array(
                		'type' => 'switch',
                		'label' => $this->l('Slide dots'),
                		'name' => 'slide_dots',
                        'default' =>'1',
                		'values' => array(
                			array(
                				'id' => 'active_on',
                				'value' => 1,
                				'label' => $this->l('Enabled')
                			),
                			array(
                				'id' => 'active_off',
                				'value' => 0,
                				'label' => $this->l('Disabled')
                			)
                		),
                	),
                    array(
                		'type' => 'switch',
                		'label' => $this->l('Active'),
                		'name' => 'active',
                        'default' =>'1',
                		'values' => array(
                			array(
                				'id' => 'active_on',
                				'value' => 1,
                				'label' => $this->l('Enabled')
                			),
                			array(
                				'id' => 'active_off',
                				'value' => 0,
                				'label' => $this->l('Disabled')
                			)
                		),
                	),
                    array(
                        'type'=>'select',
                        'label'=> $this->l('Position dots'),
                        'name'=>'position_dots',
                        'options' => array(
                            'query'=>array(
                                array(
                                    'name'=>$this->l('Bottom left'),
                                    'id'=>'botton_left'
                                ),
                                array(
                                    'name'=>$this->l('Bottom right'),
                                    'id'=>'bottom_right'
                                ),
                                array(
                                    'name'=>$this->l('Bottom center'),
                                    'id'=>'bottom_center'
                                ),
                                array(
                                    'name'=>$this->l('Vertical Left middle'),
                                    'id'=>'vertical_left_middle'
                                ),
                                array(
                                    'name'=>$this->l('Vertical Left bottom'),
                                    'id'=>'vertical_left_bottom'
                                ),
                                array(
                                    'name'=>$this->l('Vertical Left top'),
                                    'id'=>'vertical_left_top'
                                ),
                                array(
                                    'name'=>$this->l('Vertical Right middle'),
                                    'id'=>'vertical_right_middle'
                                ),
                                array(
                                    'name'=>$this->l('Vertical Right bottom'),
                                    'id'=>'vertical_right_bottom'
                                ),
                                array(
                                    'name'=>$this->l('Vertical Right top'),
                                    'id'=>'vertical_right_top'
                                ),
                            ),
                            'id' => 'id',
                			'name' => 'name'
                        )
                    ),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
        if (Tools::isSubmit('id_pg_group_slide'))
		{
			$fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_pg_group_slide');
		}
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->module = $this;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitSaveGroupSlide';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->tpl_vars = array(
			'base_url' => $this->context->shop->getBaseURL(),
			'language' => array(
				'id_lang' => $language->id,
				'iso_code' => $language->iso_code
			),
			'fields_value' => $this->getAddGroupFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
			'image_baseurl' => $this->_path.'views/img/'
		);
		$helper->override_folder = '/';
        return $helper->generateForm(array($fields_form));	
	}
    public function getAddGroupFieldsValues()
    {
        $fields = array();
		if ($id_pg_group_slide=(int)Tools::getValue('id_pg_group_slide'))
		{
			$groupslide = new Pg_group_slide($id_pg_group_slide);
			$fields['id_pg_group_slide'] = $id_pg_group_slide;
		}
		else
			$groupslide = new Pg_group_slide();
		$languages = Language::getLanguages(false);
		foreach ($languages as $lang)
		{
			$fields['title'][$lang['id_lang']] = Tools::getValue('title_'.(int)$lang['id_lang'], $groupslide->title[$lang['id_lang']]);
		}
        $fields['position_hook'] = Tools::getValue('position_hook',$groupslide->position_hook);
        $fields['slide_layout'] =Tools::getValue('slide_layout',$groupslide->slide_layout);
        $fields['background_color'] = Tools::getValue('background_color',$groupslide->background_color);
        $fields['desktop_width'] = (float)Tools::getValue('desktop_width',$groupslide->desktop_width);
        $fields['desktop_height'] = (float)Tools::getValue('desktop_height',$groupslide->desktop_height);
        $fields['pause_time'] = (int)Tools::getValue('pause_time',$groupslide->pause_time);
        $fields['auto_play'] = (int)Tools::getValue('auto_play',$groupslide->auto_play);
        $fields['pase_when_hover'] = (int)Tools::getValue('pase_when_hover',$groupslide->pase_when_hover);
        $fields['slide_lop'] = (int)Tools::getValue('slide_lop',$groupslide->slide_lop);
        $fields['slide_nav'] = (int)Tools::getValue('slide_nav',$groupslide->slide_nav);
        $fields['position_nav'] = Tools::getValue('position_nav',$groupslide->position_nav);
        $fields['slide_dots'] = (int)Tools::getValue('slide_dots',$groupslide->slide_dots);
        $fields['position_dots'] = Tools::getValue('position_dots',$groupslide->position_dots);
        $fields['custom_class'] = Tools::getValue('custom_class',$groupslide->custom_class);
        $fields['active'] = (int)Tools::getValue('active',$groupslide->active);
		return $fields;
    }
    public function renderListGroupSlider()
    {
        $groupsliders = $this->getGroupSlides();
        $this->context->smarty->assign(
            array(
                'link'=>$this->context->link,
                'groupsliders' =>$groupsliders
            )
        );
        return $this->display(__FILE__,'listgroupsliders.tpl');
    }
    public function getGroupSlides()
	{
		$this->context = Context::getContext();
		$id_shop = $this->context->shop->id;
		$id_lang = $this->context->language->id;
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT gss.id_pg_group_slide,gsl.title,gs.position_hook FROM '._DB_PREFIX_.'pg_group_slide_shop gss
            LEFT JOIN '._DB_PREFIX_.'pg_group_slide gs ON (gss.id_pg_group_slide=gs.id_pg_group_slide)
            LEFT JOIN '._DB_PREFIX_.'pg_group_slide_lang gsl ON (gs.id_pg_group_slide=gsl.id_pg_group_slide)
            WHERE gss.id_shop ='.(int)$id_shop.' AND gsl.id_lang='.(int)$id_lang);
	}
    public function renderFormConfig()
    {
        $fields = array(
            array(
                'type'=>'select',
                'name' =>'position_hook',
                'label' => $this->l('Position hook'),
                'begin_tab'=>'general',
                'desc' => $this->l('Put {hook h=\'pgCustomSlide\'} to file tpl'),
                'options' =>array(
                    array(
                        'id'=>'home',
                        'name'=> $this->l('Home'),
                    ),
                    array(
                        'id'=>'custom_hook',
                        'name'=> $this->l('Custom hook'),
                    ),  
                )
            ),
            array(
                'type'=>'select',
                'label'=> $this->l('Slide layout'),
                'name'=>'slide_layout',
                'options'=>array(
                   array(
                        'name'=>$this->l('Auto'),
                        'id'=>'auto',
                   ), 
                   array(
                        'name'=>$this->l('Full Width'),
                        'id'=>'full_width',
                   ),
                   array(
                        'name'=>$this->l('Full Screen'),
                        'id'=>'full_screen',
                   ),
                )
            ),
            array(
                'type'=>'text',
                'name'=>'desktop_width',
                'label'=> $this->l('Desktop width'),
            ),
            array(
                'type'=>'text',
                'name'=>'desktop_height',
                'label'=> $this->l('Desktop height'),
            ),
            array(
                'type'=>'list_input',
                'name'=>'notebook_size',
                'label'=> $this->l('Notebook size'),
                'null_disable'=>true,
                'inputs' =>array(
                    array(
                        'name'=>'notebook_size',
                        'type'=>'switch',
                        'class'=>'pg_pd_ena col-sm-9',
                    ),
                    array(
                        'name'=>'notebook_width',
                        'label'=>$this->l('Width'),
                        'type'=>'text',
                        'class'=>'col-sm-5',
                    ),
                    array(
                        'name'=>'notebook_height',
                        'label'=>$this->l('Height'),
                        'type'=> 'text',
                        'class'=>'col-sm-5',
                    )
                )
            ),
            array(
                'type'=>'list_input',
                'name'=>'tablet_size',
                'label'=> $this->l('Tablet size'),
                'null_disable'=>true,
                'inputs' =>array(
                    array(
                        'name'=>'tablet_size',
                        'type'=>'switch',
                        'class'=>'pg_pd_ena col-sm-9',
                    ),
                    array(
                        'name'=>'tablet_width',
                        'label'=>$this->l('Width'),
                        'type'=>'text',
                        'class'=>'col-sm-5',
                    ),
                    array(
                        'name'=>'tablet_height',
                        'label'=>$this->l('Height'),
                        'type'=> 'text',
                        'class'=>'col-sm-5',
                    )
                )
            ),
            array(
                'type'=>'list_input',
                'name'=>'mobile_size',
                'label'=> $this->l('Mobile size'),
                'null_disable'=>true,
                'inputs' =>array(
                    array(
                        'name'=>'mobile_size',
                        'type'=>'switch',
                        'class'=>'pg_pd_ena col-sm-9',
                    ),
                    array(
                        'name'=>'mobile_width',
                        'label'=>$this->l('Width'),
                        'type'=>'text',
                        'class'=>'col-sm-5',
                    ),
                    array(
                        'name'=>'mobile_height',
                        'label'=>$this->l('Height'),
                        'type'=> 'text',
                        'class'=>'col-sm-5',
                    )
                )
            ),
            array(
                'type'=>'color',
                'name'=>'background_color',
                'label'=> $this->l('Backgroud color'),
            ),
            array(
                'type'=>'file',
                'name'=>'background_image',
                'label'=> $this->l('Background image'),
                'link_delete' => $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&delete_image_group=1&id_pg_group_slide='.(int)Tools::getValue('id_pg_group_slide'),
                'type_img' =>'group',
            ),
            array(
                'type'=>'switch',
                'name'=>'active_group',
                'label'=> $this->l('Active'),
                'end_tab'=>'general',
            ),
            array(
                'type'=>'text',
                'name'=>'pause_time',
                'label'=> $this->l('Pause time'),
                'suffix' => $this->l('millisecond'),
                'desc'=>$this->l('Time pauses between 2 slide (includes runtime items)'),
                'begin_tab'=>'animation'
            ),
            array(
                'type'=>'switch',
                'name'=>'auto_play',
                'label'=> $this->l('Auto play'),
            ),
            array(
                'type'=>'switch',
                'name'=>'pase_when_hover',
                'label' => $this->l('Pase when hover')
            ),
            array(
                'type'=>'switch',
                'name'=>'slide_lop',
                'label'=> $this->l('Slide lop')
            ),
            array(
                'type'=>'switch',
                'name'=>'slide_nav',
                'label' => $this->l('Show nav'),
            ),
            array(
                'type'=>'select',
                'name'=>'position_nav',
                'label' => $this->l('Position nav'),
                'options' =>array(
                    array(
                        'name'=>$this->l('Default'),
                        'id'=>'default'
                    ),
                    array(
                        'name'=>$this->l('Bottom left'),
                        'id'=>'botton_left'
                    ),
                    array(
                        'name'=>$this->l('Bottom right'),
                        'id'=>'bottom_right'
                    ),
                    array(
                        'name'=>$this->l('Bottom center'),
                        'id'=>'bottom_center'
                    ),
                )
            ),
            array(
                'type'=>'switch',
                'name'=>'slide_dots',
                'label'=> $this->l('Show dots'),
                
            ),
            array(
                'type'=>'select',
                'name'=>'position_dots',
                'label' => $this->l('Position dots'),
                'options' =>array(
                    array(
                        'name'=>$this->l('Bottom left'),
                        'id'=>'botton_left'
                    ),
                    array(
                        'name'=>$this->l('Bottom right'),
                        'id'=>'bottom_right'
                    ),
                    array(
                        'name'=>$this->l('Bottom center'),
                        'id'=>'bottom_center'
                    ),
                    array(
                        'name'=>$this->l('Vertical Left middle'),
                        'id'=>'vertical_left_middle'
                    ),
                    array(
                        'name'=>$this->l('Vertical Left bottom'),
                        'id'=>'vertical_left_bottom'
                    ),
                    array(
                        'name'=>$this->l('Vertical Left top'),
                        'id'=>'vertical_left_top'
                    ),
                    array(
                        'name'=>$this->l('Vertical Right middle'),
                        'id'=>'vertical_right_middle'
                    ),
                    array(
                        'name'=>$this->l('Vertical Right bottom'),
                        'id'=>'vertical_right_bottom'
                    ),
                    array(
                        'name'=>$this->l('Vertical Right top'),
                        'id'=>'vertical_right_top'
                    ),
                )
            ),
            array(
                'type'=>'text',
                'name'=>'custom_class',
                'label'=> $this->l('Custom class'),
                'end_tab'=>'animation'
            ),
            array(
                'type'=>'hidden',
                'name'=>'id_pg_group_slide',
                'value'=> (int)Tools::getValue('id_pg_group_slide')
            )
        );
        $this->context->smarty->assign(
            array(
                'fields' => $fields,
                'title_form' => $this->l('Group Setting'),
                'list_fields' => $this->getListFields($fields,$this->getConfigFieldsValues()),
                'languages'=> Language::getLanguages(false),
                'defaultFormLanguage'=>(int)Configuration::get('PS_LANG_DEFAULT'),
                'action'=>$this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name,
            )
        );
        return $this->display(__FILE__,'form_config.tpl');
    }
    public function getConfigFieldsValues()
    {
        $group_slide= new Pg_group_slide((int)Tools::getValue('id_pg_group_slide'));
        $fields= array();
        $fields['position_hook'] = Tools::getValue('position_hook',$group_slide->position_hook);
        $fields['slide_layout'] = Tools::getValue('slide_layout',$group_slide->slide_layout);
        $fields['desktop_width'] = (float)Tools::getValue('desktop_width',$group_slide->desktop_width);
        $fields['desktop_height'] = (float)Tools::getValue('desktop_height',$group_slide->desktop_height);
        $fields['notebook_width'] = (int)Tools::getValue('notebook_width',$group_slide->notebook_width);
        $fields['notebook_height'] = (float)Tools::getValue('notebook_height',$group_slide->notebook_height);
        $fields['tablet_size'] = (int)Tools::getValue('tablet_size',$group_slide->tablet_size);
        $fields['tablet_width'] = (float)Tools::getValue('tablet_width',$group_slide->tablet_width);
        $fields['tablet_height'] = (float)Tools::getValue('tablet_height',$group_slide->tablet_height);
        $fields['mobile_size'] = (int)Tools::getValue('mobile_size',$group_slide->mobile_size);
        $fields['mobile_width'] = (float)Tools::getValue('mobile_width',$group_slide->mobile_width);
        $fields['mobile_height'] = (float)Tools::getValue('mobile_height',$group_slide->mobile_height);
        $fields['background_color'] = Tools::getValue('background_color',$group_slide->background_color);
        $fields['pause_time'] = (int)Tools::getValue('pause_time',$group_slide->pause_time);
        $fields['auto_play'] = (int)Tools::getValue('auto_play',$group_slide->auto_play);
        $fields['pase_when_hover'] = (int)Tools::getValue('pase_when_hover',$group_slide->pase_when_hover);
        $fields['slide_lop'] = (int)Tools::getValue('slide_lop',$group_slide->slide_lop);
        $fields['slide_nav'] = (int)Tools::getValue('slide_nav',$group_slide->slide_nav);
        $fields['position_nav'] = Tools::getValue('position_nav',$group_slide->position_nav);
        $fields['slide_dots'] = (int)Tools::getValue('slide_dots',$group_slide->slide_dots);
        $fields['position_dots'] = Tools::getValue('position_dots',$group_slide->position_dots);
        $fields['custom_class'] = Tools::getValue('custom_class',$group_slide->custom_class);
        $fields['active_group'] = Tools::getValue('active_group',$group_slide->active);
        $fields['background_image'] = $group_slide->background_image;
        return $fields;
    }
    public function renderFormAddSlide()
    {
        $fields=array(
            array(
                'type'=>'text',
                'name'=>'title',
                'label'=>$this->l('Title'),
                'lang'=>true,
                'begin_tab' => 'general'
            ),
            array(
                'type'=>'text',
                'name'=>'link',
                'label'=>$this->l('Link'),
                'lang'=>true,
            ), 
            array(
                'type'=>'file',
                'name'=>'backgroud_image',
                'label'=>$this->l('Backgroud image'),
                'type_img' =>'slide',
            ),
            array(
                'type'=>'color',
                'name'=>'backgroud_color',
                'label'=>$this->l('Backgroud color'),
            ), 
            array(
                'type'=>'select',
                'name'=>'repeat',
                'label'=> $this->l('Repeat'),
                'options'=>array(
                    array(
                        'id'=> 'none',
                        'name'=>'none'
                    ),
                    array(
                        'id'=> 'repeat x',
                        'name'=>$this->l('Repeat x')
                    ),
                    array(
                        'id'=> 'repeat y',
                        'name'=>'repeat_y',
                    ),
                    
                )
            ),
            array(
                'type'=>'select',
                'name'=>'background_size',
                'label'=> $this->l('Background size'),
                'options'=>array(
                    array(
                        'name'=>$this->l('Cover'),
                        'id'=> 'cover'
                    ),
                    array(
                        'name'=>$this->l('X% , Y%'),
                        'id'=> 'X% , Y%'
                    ),
                    array(
                        'name'=>$this->l('Contain'),
                        'id'=> 'contain'
                    ), 
                    array(
                        'name'=>$this->l('Normal'),
                        'id'=>'normal',
                    ),   
                )
            ),
            array(
                'type'=>'select',
                'name'=>'background_position',
                'label'=> $this->l('Background position'),
                'options'=>array(
                    array(
                        'name'=>$this->l('Contain'),
                        'id'=>'contain',
                    ),
                    array(
                        'name'=>$this->l('Center center'),
                        'id'=>'center center',
                    ),array(
                        'name'=>$this->l('Center ceft'),
                        'id'=>'center ceft',
                    ),
                    array(
                        'name'=>$this->l('Center cight'),
                        'id'=>'center cight',
                    ),
                    array(
                        'name'=>$this->l('Top ceft'),
                        'id'=>'Top ceft',
                    ),
                    array(
                        'name'=>$this->l(' Top cight'),
                        'id'=>'top cight',
                    ),
                    array(
                        'name'=>$this->l('Top center'),
                        'id'=>'top center',
                    ),
                    array(
                        'name'=>$this->l('bottom ceft'),
                        'id'=>'bottom left',
                    ),
                    array(
                        'name'=>$this->l('Bottom cight'),
                        'id'=>'bottom right',
                    ),
                    array(
                        'name'=>$this->l('Bottom center'),
                        'id'=>'bottom center',
                    ),
                )
            ),
            array(
                'type'=>'switch',
                'label'=>$this->l('Active'),
                'name'=>'active',
                'end_tab'=>'general'
            ),
            array(
                'type'=>'text',
                'name'=>'time_in',
                'label'=>$this->l('Time in'),
                'suffix' => $this->l('millisecond'),
                'begin_tab'=>'animation'
            ), 
            array(
                'type'=>'select',
                'name'=>'effect_in',
                'label'=>'Effect in',
                'options' =>$this->animations,
            ),
            array(
                'type'=>'text',
                'name'=>'time_out',
                'label'=>$this->l('Time out'),
                'suffix' => $this->l('millisecond'),
            ), 
            array(
                'type'=>'select',
                'name'=>'effect_out',
                'label'=>'Effect out',
                'options' =>$this->effects,
            ),
            array(
                'name'=>'custom_class',
                'type'=>'text',
                'label'=> $this->l('Custom class'),
                'end_tab'=>'animation'
            ),
            array(
                'type'=>'hidden',
                'name'=>'id_pg_group_slide',
                'value'=> (int)Tools::getValue('id_pg_group_slide')
            )
            
        );
        if(Tools::getValue('id_slide'))
            $fields[]=array(
                'type'=>'hidden',
                'name'=>'id_slide',
                'value'=> (int)Tools::getValue('id_slide')
            );
        $this->context->smarty->assign(
            array(
                'form_title' => (int)Tools::getValue('id_slide') ? $this->l('Edit slide'): $this->l('Add slide'),
                'list_fields' => $this->getListFields($fields,$this->getSlideFieldsValues()),
                'js_dir_path' => $this->_path.'views/js/',
                'action'=>$this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name,
            )
        );
        return $this->display(__FILE__,'form_slide.tpl');
    }
    public function getSlideFieldsValues()
    {
        if(Tools::getValue('id_slide'))
            $slide = new Pg_slide(Tools::getValue('id_slide'));
        else
            $slide= new Pg_slide();
        $fields = array();
        $fields['active']=(int)Tools::getValue('active',$slide->active);
        $fields['backgroud_color']=Tools::getValue('backgroud_color',$slide->backgroud_color);
        $fields['repeat_x']=(int)Tools::getValue('repeat_x',$slide->repeat_x);
        $fields['backgroud_image']=Tools::getValue('backgroud_image',$slide->backgroud_image);
        $fields['repeat_y']=(int)Tools::getValue('repeat_y',$slide->repeat_y);
        $fields['effect_in']=Tools::getValue('effect_in',$slide->effect_in);
        $fields['time_in']=(int)Tools::getValue('time_in',$slide->time_in);
        $fields['time_out']=(int)Tools::getValue('time_out',$slide->time_out);
        $fields['effect_out']=Tools::getValue('effect_out',$slide->effect_out);
        $fields['custom_class']=Tools::getValue('custom_class',$slide->custom_class);
        $languages = Language::getLanguages(false);
        foreach($languages as $language)
        {
            $fields['link'][$language['id_lang']]= Tools::getValue('link_'.$language['id_lang'],$slide->link[$language['id_lang']]);
            $fields['title'][$language['id_lang']]=Tools::getValue('title_'.$language['id_lang'],$slide->title[$language['id_lang']]);
        }
        return $fields;
    }
    public function getListFields($fields,$fields_values)
    {
        $this->context->smarty->assign(
            array(
                'fields'=>$fields,
                'fields_values'=>$fields_values,
                'languages'=> Language::getLanguages(false),
                'defaultFormLanguage'=>(int)Configuration::get('PS_LANG_DEFAULT'),
            )
        );
        return $this->display(__FILE__,'list_fields.tpl');
    }
    public function getFormAjax($form)
    {
        switch ($form) {
            case 'slide':
                die(
                    Tools::jsonEncode(
                        array(
                            'form'=> $this->renderFormAddSlide(),
                        )
                    )
                );
            case 'slideitem':
                die(
                    Tools::jsonEncode(
                        array(
                            'form' => $this->getFormItemSlide(Tools::getValue('id_slide')),
                        )
                    )
                );
            case 'addtextitem':
                die(
                    Tools::jsonEncode(
                        array(
                            'form' => $this->getFormAddTextItem(Tools::getValue('id_slide')),
                        )
                    )
                );
            case 'addimgitem':
                die(
                    Tools::jsonEncode(
                        array(
                            'form' => $this->getFormAddImgItem(Tools::getValue('id_slide')),
                        )
                    )
                ); 
            case 'addbuttonitem':
                die(
                        Tools::jsonEncode(
                            array(
                                'form' => $this->getFormAddButtonItem(Tools::getValue('id_slide')),
                            )
                        )
                    );
            case 'getslideitem':
                die(
                    Tools::jsonEncode(
                        array(
                            'form' => $this->renderListSlideItem(Tools::getValue('id_slide')),
                        )
                    )
                );
        }
    }
    public function renderListSlideItem($id_slide)
    {
        $items = $this->getItemSlide($id_slide);
        $this->context->smarty->assign(
            array(
                'items'=> $items,
            )
        );
        return $this->display(__FILE__,'listitem.tpl');
    }
    public function getSlides($id_group=0,$active=false)
    {
        if(!$id_group)
            $id_group = (int)Tools::getValue('id_pg_group_slide');
        $sql ="SELECT * FROM "._DB_PREFIX_."pg_slide s
        LEFT JOIN "._DB_PREFIX_."pg_slide_lang sl ON (s.id_pg_slide = sl.id_pg_slide AND sl.id_lang='".(int)$this->context->language->id."')
        LEFT JOIN "._DB_PREFIX_."pg_slide_shop ss ON (s.id_pg_slide=ss.id_pg_slide)
        WHERE ".($active? ' s.active=1 AND':'')." s.id_pg_group_slide='".(int)$id_group."' AND ss.id_shop ='".(int)$this->context->shop->id."'
        ORDER BY s.position
        ";
        return Db::getInstance()->executeS($sql);
    }
    public function getItemSlide($id_slide,$type='',$active=false)
    {
        $sql ="SELECT * FROM "._DB_PREFIX_."pg_slide_item si
        LEFT JOIN "._DB_PREFIX_."pg_slide_item_lang sil ON (sil.id_pg_slide_item = si.id_pg_slide_item AND sil.id_lang= '".(int)$this->context->language->id."')
        WHERE ".($active?'si.active=1 AND':'')." si.id_pg_slide='".(int)$id_slide."'".($type? " AND si.type='".$type."'":"").' ORDER BY si.position';
        return Db::getInstance()->executeS($sql);
    }
    public function getFormItemSlide($id_slide)
    {
        $slide = new Pg_slide($id_slide);
        $itemsSlide = $this->getItemSlide($id_slide);
        $groupSlide= new Pg_group_slide($slide->id_pg_group_slide);
        $this->context->smarty->assign(
            array(
                'width_slide' =>$groupSlide->desktop_width,
                'height_slide' =>$groupSlide->desktop_height,
            )
        );
        $this->context->smarty->assign(
            array(
                'slide' => $slide,
                'itemsSlide' => $itemsSlide,
            )
        );
        return $this->display(__FILE__,'form_item_slide.tpl');
    }
    public function getFormAddTextItem($id_slide)
    {
        $fields=array(
            array(
                'type'=>'textarea',
                'name'=>'text_html',
                'label'=>$this->l('Text/html'),
                'lang'=>true,
                'begin_tab' => 'general'
            ),
            array(
                'type'=>'select',
                'name'=>'font_family',
                'label'=>$this->l('Font family'),
                'options' =>$this->googlefonts,
            ), 
            array(
                'type'=>'text',
                'name'=>'font_size',
                'label'=>$this->l('font size'),
                'suffix'=>'px'
            ),
            array(
                'type'=>'color',
                'name'=>'it_background_color',
                'label'=>$this->l('Background color'),
            ), 
            array(
                'type'=>'color',
                'name'=>'color',
                'label'=> $this->l('Color'),
            ),
            array(
                'type'=>'list_input',
                'name'=>'padding',
                'label'=> $this->l('Padding'),
                'inputs' =>array(
                    array(
                        'name'=>'padding_top',
                        'label'=>$this->l('Top(px)'),
                        'type'=>'text',
                        'class'=>'col-sm-3',
                    ),
                    array(
                        'name'=>'padding_right',
                        'label'=>$this->l('Right(px)'),
                        'type'=>'text',
                        'class'=>'col-sm-3',
                    ),
                    array(
                        'name'=>'padding_bottom',
                        'label'=>$this->l('Bottom(px)'),
                        'type'=> 'text',
                        'class'=>'col-sm-3',
                    ),
                    array(
                        'name'=>'padding_left',
                        'label'=>$this->l('Left(px)'),
                        'type'=>'text',
                        'class'=>'col-sm-3',
                    ),
                )
            ),
            array(
                'type'=>'radio',
                'name'=>'font_weight',
                'label'=>$this->l('Font Weight'),
                'options'=>array(
                    array(
                        'id'=>'normal',
                        'name'=>'Normal',
                    ),
                    array(
                        'id'=>'600',
                        'name'=>'600',
                    ),
                    array(
                        'id'=>'900',
                        'name'=>'900',
                    ),
                )
            ),
            array(
                'type'=>'radio',
                'name'=>'text_transform',
                'label'=>$this->l('Text Transform'),
                'options'=>array(
                    array(
                        'id'=>'none',
                        'name'=>'None',
                    ),
                    array(
                        'id'=>'uppercase',
                        'name'=>'Uppercase',
                    ),
                    array(
                        'id'=>'lowercase',
                        'name'=>'Lowercase',
                    ),
                )
            ),
            array(
                'type'=>'radio',
                'name'=>'text_decoration',
                'label'=>$this->l('Text Decoration'),
                'options'=>array(
                    array(
                        'id'=>'none',
                        'name'=>'None',
                    ),
                    array(
                        'id'=>'underline',
                        'name'=>'Underline',
                    ),
                    array(
                        'id'=>'line-through',
                        'name'=>'Line-through',
                    ),
                )
            ),
            array(
                'type'=>'radio',
                'name'=>'font_style',
                'label' =>'Font style',
                'options'=> array(
                    array(
                        'name'=>$this->l('Normal'),
                        'id'=>'normal'
                    ),
                    array(
                        'name'=>$this->l('Italic'),
                        'id'=>'italic'
                    ),
                )
            ),
            array(
                'type'=>'switch',
                'name'=>'active',
                'label'=> $this->l('Active'),
            ),
            array(
                'type'=>'text',
                'name'=>'custom_class',
                'label'=>$this->l('Custom class'),
                'end_tab'=>'general'
            ),
            array(
                'type'=>'text',
                'name'=>'position_top',
                'label'=> $this->l('Position top'),
                'begin_tab'=>'animation',
                'suffix'=>'px'
            ),
            array(
                'type'=>'text',
                'name'=>'position_left',
                'label'=> $this->l('Position left'),
                'suffix'=>'px'
            ),
            array(
                'type'=>'select',
                'name'=>'effect_in',
                'label'=> $this->l('Effect in'),
                'options'=>$this->animations
            ),
            array(
                'type'=>'text',
                'name'=>'delay_in',
                'label'=> $this->l('Delay in'),
                'suffix'=>'ms'
            ),
            array(
                'type'=>'text',
                'name'=>'time_effect_in',
                'label'=> $this->l('Time effect in'),
                'suffix'=>'ms'
            ),
            array(
                'type'=>'select',
                'name'=>'effect_out',
                'label'=> $this->l('Effect out'),
                'options'=>$this->effects,
            ),
            array(
                'type'=>'text',
                'name'=>'delay_out',
                'label'=> $this->l('Delay out'),
                'suffix'=>'ms'
            ),
            array(
                'type'=>'text',
                'name'=>'time_effect_out',
                'label'=> $this->l('Time effect out'),
                'suffix'=>'ms',
                'end_tab'=>'animation',
            ),
        );
        $fields[]=array(
                'type'=>'hidden',
                'name'=>'id_slide',
                'value'=> $id_slide,
        );
        if(Tools::getValue('id_item'))
        {
            $fields[]=array(
                'type'=>'hidden',
                'name'=>'id_item',
                'value'=> Tools::getValue('id_item'),
            );
        }
        else
        {
            $fields[]=array(
                'type'=>'hidden',
                'name'=>'id_item',
                'value'=> 0,
            );
        }    
        $fields[]=array(
            'type'=>'hidden',
            'name'=>'type',
            'value'=>'text',
        );
        
        $this->context->smarty->assign(
            array(
                'form_title' => Tools::getValue('id_item') ? $this->l('Edit text item'): $this->l('Add text item'),
                'list_fields' => $this->getListFields($fields,$this->getItemFieldsValues()),
                'js_dir_path' => $this->_path.'views/js/',
                'action'=>$this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name,
            )
        );
        return $this->display(__FILE__,'form_item.tpl');
    }
    public function getFormAddImgItem($id_slide)
    {
        $fields=array(
            array(
                'type'=>'file',
                'name'=>'image',
                'label'=>$this->l('Image'),
                'type_img'=>'item',
                'link_delete' => $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&delete_image_item=1&id_item='.(int)Tools::getValue('id_item'),
                'begin_tab' => 'general'
            ),
            
            array(
                'type'=>'text',
                'name'=>'width',
                'label'=>$this->l('Width image'),
            ), 
            array(
                'type'=>'text',
                'name'=>'height',
                'label'=>$this->l('Height image'),
            ),
            array(
                'type'=>'text',
                'name'=>'link',
                'label'=>$this->l('Link'),
                'lang'=>true,
                
            ),
            array(
                'type'=>'switch',
                'name'=>'active',
                'label' =>$this->l('Active'), 
            ),
            array(
                'type'=>'text',
                'name'=>'custom_class',
                'label'=>$this->l('Custom class'),
                'end_tab'=>'general'
            ),
            array(
                'type'=>'text',
                'name'=>'position_top',
                'label'=> $this->l('Position top'),
                'suffix'=>'px',
                'begin_tab'=>'animation',
                
            ),
            array(
                'type'=>'text',
                'name'=>'position_left',
                'suffix'=>'px',
                'label'=> $this->l('Position left')
            ),
            array(
                'type'=>'select',
                'name'=>'effect_in',
                'label'=> $this->l('Effect in'),
                'options'=>$this->animations,
            ),
            array(
                'type'=>'text',
                'name'=>'delay_in',
                'label'=> $this->l('Delay in'),
                'suffix'=>'ms'
            ),
            array(
                'type'=>'text',
                'name'=>'time_effect_in',
                'label'=> $this->l('Time effect in'),
                'suffix'=>'ms'
            ),
            array(
                'type'=>'select',
                'name'=>'effect_out',
                'label'=> $this->l('Effect out'),
                'options'=>$this->effects
            ),
            array(
                'type'=>'text',
                'name'=>'delay_out',
                'label'=> $this->l('Delay out'),
                'suffix'=>'ms'
            ),
            array(
                'type'=>'text',
                'name'=>'time_effect_out',
                'label'=> $this->l('Time effect out'),
                'end_tab'=>'animation',
                'suffix'=>'ms'
            ),
            
        );
        $fields[]=array(
                'type'=>'hidden',
                'name'=>'id_slide',
                'value'=> $id_slide,
        );
        if(Tools::getValue('id_item'))
        {
            $fields[]=array(
                'type'=>'hidden',
                'name'=>'id_item',
                'value'=> Tools::getValue('id_item'),
            );
        }
        else
        {
            $fields[]=array(
                'type'=>'hidden',
                'name'=>'id_item',
                'value'=> 0,
            );
        }
        $fields[]=array(
            'type'=>'hidden',
            'name'=>'type',
            'value'=>'image',
        );
        
        $this->context->smarty->assign(
            array(
                'form_title' => Tools::getValue('id_item') ? $this->l('Edit image item'): $this->l('Add image item'),
                'list_fields' => $this->getListFields($fields,$this->getItemFieldsValues()),
                'js_dir_path' => $this->_path.'views/js/',
                'action'=>$this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name,
            )
        );
        return $this->display(__FILE__,'form_item.tpl');
    }
    public function getFormAddButtonItem($id_slide)
    {
        $fields=array(
            array(
                'type'=>'text',
                'name'=>'label_button',
                'label'=>$this->l('Label button'),
                'lang'=>true,
                'begin_tab' => 'general'
            ),
            array(
                'type'=>'text',
                'name'=>'link',
                'label'=>$this->l('Link'),
                'lang'=>true,
            ),
            array(
                'type'=>'select',
                'name'=>'font_family',
                'label'=>$this->l('Font family'),
                'options' =>$this->googlefonts,
            ), 
            array(
                'type'=>'text',
                'name'=>'font_size',
                'label'=>$this->l('font size'),
                'suffix'=>'px'
            ),
            array(
                'type'=>'text',
                'name'=>'border_radius',
                'label'=> $this->l('Border radius'),
                'suffix'=>'px'
            ),
            array(
                'type'=>'list_input',
                'name'=>'border_width',
                'label'=> $this->l('Border width'),
                'inputs' =>array(
                    array(
                        'name'=>'border_width_top',
                        'label'=>$this->l('Top(px)'),
                        'type'=>'text',
                        'class'=>'col-sm-3',
                    ),
                    array(
                        'name'=>'border_width_right',
                        'label'=>$this->l('Right(px)'),
                        'type'=>'text',
                        'class'=>'col-sm-3',
                    ),
                    array(
                        'name'=>'border_width_bottom',
                        'label'=>$this->l('Bottom(px)'),
                        'type'=> 'text',
                        'class'=>'col-sm-3',
                    ),
                    array(
                        'name'=>'border_width_left',
                        'label'=>$this->l('Left(px)'),
                        'type'=>'text',
                        'class'=>'col-sm-3',
                    ),
                )
            ),
            array(
                'type'=>'select',
                'name'=>'border_style',
                'label'=>$this->l('Border Style'),
                'options'=>array(
                    array(
                        'name'=>'Solid',
                        'id'=>'solid',
                    ),
                    array(
                        'name'=>'Dashed',
                        'id'=>'dashed',
                    ),
                    array(
                        'name'=>'Dotted',
                        'id'=>'dotted',
                    ),
                )
            ),
            array(
                'type'=>'list_input',
                'name'=> 'border',
                'Label' =>$this->l('Border'),
                'inputs'=>array(
                    array(
                        'name'=> 'border_color',
                        'label'=>$this->l('Border color'),
                        'type'=>'color',
                        'class'=>'col-sm-6',
                    ),
                    array(
                        'name'=> 'border_color_hover',
                        'label'=>$this->l('Hover'),
                        'type'=>'color',
                        'class'=>'col-sm-6',
                    ),
                ),
            ),
            array(
                'type'=>'list_input',
                'name'=> 'color',
                'Label' =>$this->l('Color'),
                'inputs'=>array(
                    array(
                        'name'=> 'color',
                        'label'=>$this->l('Color'),
                        'type'=>'color',
                        'class'=>'col-sm-6',
                    ),
                    array(
                        'name'=> 'hover_color',
                        'label'=>$this->l('Hover'),
                        'type'=>'color',
                        'class'=>'col-sm-6',
                    ),
                ),
            ),
            array(
                'type'=>'list_input',
                'name'=> 'background',
                'Label' =>$this->l('Background'),
                'inputs'=>array(
                    array(
                        'name'=> 'it_background_color',
                        'label'=>$this->l('Background color'),
                        'type'=>'color',
                        'class'=>'col-sm-6',
                    ),
                    array(
                        'name'=> 'hover_background_color',
                        'label'=>$this->l('Hover'),
                        'type'=>'color',
                        'class'=>'col-sm-6',
                    ),
                ),
            ),
            array(
                'type'=>'list_input',
                'name'=>'padding',
                'label'=> $this->l('Padding'),
                'inputs' =>array(
                    array(
                        'name'=>'padding_top',
                        'label'=>$this->l('Top(px)'),
                        'type'=>'text',
                        'class'=>'col-sm-3',
                    ),
                    array(
                        'name'=>'padding_right',
                        'label'=>$this->l('Right(px)'),
                        'type'=>'text',
                        'class'=>'col-sm-3',
                    ),
                    array(
                        'name'=>'padding_bottom',
                        'label'=>$this->l('Bottom(px)'),
                        'type'=> 'text',
                        'class'=>'col-sm-3',
                    ),
                    array(
                        'name'=>'padding_left',
                        'label'=>$this->l('Left(px)'),
                        'type'=>'text',
                        'class'=>'col-sm-3',
                    ),
                )
            ),
            array(
                'type'=>'radio',
                'name'=>'font_weight',
                'label'=>$this->l('Font Weight'),
                'options'=>array(
                    array(
                        'id'=>'normal',
                        'name'=>'Normal',
                    ),
                    array(
                        'id'=>'600',
                        'name'=>'600',
                    ),
                    array(
                        'id'=>'900',
                        'name'=>'900',
                    ),
                )
            ),
            array(
                'type'=>'radio',
                'name'=>'text_transform',
                'label'=>$this->l('Text Transform'),
                'options'=>array(
                    array(
                        'id'=>'none',
                        'name'=>'None',
                    ),
                    array(
                        'id'=>'uppercase',
                        'name'=>'Uppercase',
                    ),
                    array(
                        'id'=>'lowercase',
                        'name'=>'Lowercase',
                    ),
                )
            ),
            array(
                'type'=>'radio',
                'name'=>'text_decoration',
                'label'=>$this->l('Text Decoration'),
                'options'=>array(
                    array(
                        'id'=>'none',
                        'name'=>'None',
                    ),
                    array(
                        'id'=>'underline',
                        'name'=>'Underline',
                    ),
                    array(
                        'id'=>'line-through',
                        'name'=>'Line-through',
                    ),
                )
            ),
            array(
                'type'=>'radio',
                'name'=>'font_style',
                'label' =>'Font style',
                'options'=> array(
                    array(
                        'name'=>$this->l('Normal'),
                        'id'=>'normal'
                    ),
                    array(
                        'name'=>$this->l('Italic'),
                        'id'=>'italic'
                    ),
                )
            ),
            array(
                'type'=>'switch',
                'name'=>'active',
                'label' =>$this->l('Active'), 
            ),
            array(
                'type'=>'text',
                'name'=>'custom_class',
                'label'=>$this->l('Custom class'),
                'end_tab'=>'general'
            ),
            array(
                'type'=>'text',
                'name'=>'position_top',
                'label'=> $this->l('Position top'),
                'begin_tab'=>'animation',
                'suffix'=>'px'
            ),
            array(
                'type'=>'text',
                'name'=>'position_left',
                'label'=> $this->l('Position left'),
                'suffix'=>'px'
            ),
            array(
                'type'=>'select',
                'name'=>'effect_in',
                'label'=> $this->l('Effect in'),
                'options'=>$this->animations,
            ),
            array(
                'type'=>'text',
                'name'=>'delay_in',
                'label'=> $this->l('Delay in'),
                'suffix'=>'ms'
            ),
            array(
                'type'=>'text',
                'name'=>'time_effect_in',
                'label'=> $this->l('Time effect in'),
                'suffix'=>'ms'
            ),
            array(
                'type'=>'select',
                'name'=>'effect_out',
                'label'=> $this->l('Effect out'),
                'options'=>$this->effects,
            ),
            array(
                'type'=>'text',
                'name'=>'delay_out',
                'label'=> $this->l('Delay out'),
                'suffix'=>'ms'
            ),
            array(
                'type'=>'text',
                'name'=>'time_effect_out',
                'label'=> $this->l('Time effect out'),
                'end_tab'=>'animation',
                'suffix'=>'ms'
            ),
        );
        $fields[]=array(
                'type'=>'hidden',
                'name'=>'id_slide',
                'value'=> $id_slide,
        );
        if(Tools::getValue('id_item'))
        {
            $fields[]=array(
                'type'=>'hidden',
                'name'=>'id_item',
                'value'=> Tools::getValue('id_item'),
            );
        }
        else
        {
            $fields[]=array(
                'type'=>'hidden',
                'name'=>'id_item',
                'value'=> 0,
            );
        }
        $fields[]=array(
            'type'=>'hidden',
            'name'=>'type',
            'value'=>'button',
        );
        
        $this->context->smarty->assign(
            array(
                'form_title' => Tools::getValue('id_item') ? $this->l('Edit button item'): $this->l('Add button item'),
                'list_fields' => $this->getListFields($fields,$this->getItemFieldsValues()),
                'js_dir_path' => $this->_path.'views/js/',
                'action'=>$this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name,
            )
        );
        return $this->display(__FILE__,'form_item.tpl');
    }
    public function getItemFieldsValues()
    {
        if($id_item= Tools::getValue('id_item'))
        {
            $pg_slide_item= new Pg_slide_item($id_item);
        }
        else
            $pg_slide_item= new Pg_slide_item();
        $fields = array();
        $fields['font_family']=Tools::getValue('font_family',$pg_slide_item->font_family);
        $fields['font_size']= Tools::getValue('font_size',$pg_slide_item->font_size);
        $fields['it_background_color'] = Tools::getValue('it_background_color',$pg_slide_item->background_color);
        $fields['font_style'] = Tools::getValue('font_style',$pg_slide_item->font_style);
        $fields['border_radius'] = Tools::getValue('border_radius',$pg_slide_item->border_radius);
        $fields['border_width_top'] = Tools::getValue('border_width_top',$pg_slide_item->border_width_top);
        $fields['border_width_left'] = Tools::getValue('border_width_left',$pg_slide_item->border_width_left);
        $fields['border_width_bottom'] = Tools::getValue('border_width_bottom',$pg_slide_item->border_width_bottom);
        $fields['border_width_right'] = Tools::getValue('border_width_right',$pg_slide_item->border_width_right);
        $fields['border_style'] = Tools::getValue('border_style',$pg_slide_item->border_style);
        $fields['border_color'] = Tools::getValue('border_color',$pg_slide_item->border_color);
        $fields['border_color_hover'] = Tools::getValue('border_color_hover',$pg_slide_item->border_color_hover);
        $fields['hover_color'] = Tools::getValue('color',$pg_slide_item->hover_color);
        $fields['hover_background_color'] = Tools::getValue('color',$pg_slide_item->hover_background_color);
        $fields['color'] = Tools::getValue('color',$pg_slide_item->color);
        $fields['padding_top'] = Tools::getValue('padding_top',$pg_slide_item->padding_top);
        $fields['padding_right'] = Tools::getValue('padding_right',$pg_slide_item->padding_right);
        $fields['padding_bottom'] = Tools::getValue('padding_bottom',$pg_slide_item->padding_bottom);
        $fields['padding_left'] = Tools::getValue('padding_left',$pg_slide_item->padding_left);
        $fields['font_weight'] = Tools::getValue('font_weight',$pg_slide_item->font_weight);
        $fields['text_transform'] = Tools::getValue('text_transform',$pg_slide_item->text_transform);
        $fields['text_decoration'] = Tools::getValue('text_decoration',$pg_slide_item->text_decoration);
        $fields['custom_class'] = Tools::getValue('custom_class',$pg_slide_item->custom_class);
        $fields['position_top'] = Tools::getValue('position_top',$pg_slide_item->position_top);
        $fields['position_left'] = Tools::getValue('position_left',$pg_slide_item->position_left);
        $fields['effect_in'] = Tools::getValue('effect_in',$pg_slide_item->effect_in);
        $fields['delay_in'] = Tools::getValue('delay_in',$pg_slide_item->delay_in);
        $fields['time_effect_in'] = Tools::getValue('time_effect_in',$pg_slide_item->time_effect_in);
        $fields['effect_out'] = Tools::getValue('effect_out',$pg_slide_item->effect_out);
        $fields['delay_out'] = Tools::getValue('delay_out',$pg_slide_item->delay_out);
        $fields['time_effect_out'] = Tools::getValue('time_effect_out',$pg_slide_item->time_effect_out);
        $fields['active'] = (int)Tools::getValue('active',$pg_slide_item->active);
        $fields['image'] = $pg_slide_item->image;
        $fields['width'] = Tools::getValue('width',$pg_slide_item->width);
        $fields['height'] = Tools::getValue('height',$pg_slide_item->height);
        $languages = Language::getLanguages(false);
        foreach($languages as $language)
        {
            $fields['text_html'][$language['id_lang']] = Tools::getValue('text_html_'.$language['id_lang'],$pg_slide_item->text_html[$language['id_lang']]);
            $fields['link'][$language['id_lang']] = Tools::getValue('link_'.$language['id_lang'],$pg_slide_item->link[$language['id_lang']]);
            $fields['label_button'][$language['id_lang']]=Tools::getValue('label_button_'.$language['id_lang'],$pg_slide_item->label_button[$language['id_lang']]);
        }
        return $fields;
        
    }
    public function uploadFile($key,$type_img)
    {
        //Upload file
        if(isset($_FILES[$key])&& isset($_FILES[$key]['tmp_name']) && isset($_FILES[$key]['name']) && $_FILES[$key]['name'])
        {
            $salt = sha1(microtime());
            $type = Tools::strtolower(Tools::substr(strrchr($_FILES[$key]['name'], '.'), 1));
            $imageName = $salt.'.'.$type;
            $fileName = dirname(__FILE__).'/views/img/'.$type_img.'/'.$imageName;
			$imagesize = @getimagesize($_FILES[$key]['tmp_name']);
            if ( isset($_FILES[$key]) &&
				!empty($_FILES[$key]['tmp_name']) &&
				!empty($imagesize) &&
				in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
			)
			{
				$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
				if ($error = ImageManager::validateUpload($_FILES[$key]))
				{
				   die(
                      Tools::jsonEncode(
                          array(
                                'error'=>true,
                                'errors' => $error,
                          )
                      )  
                    );
				}
				elseif (!$temp_name || !move_uploaded_file($_FILES[$key]['tmp_name'], $temp_name))
                {
                    die(
                          Tools::jsonEncode(
                              array(
                                    'error'=>true,
                                    'errors' => $this->l('Can not upload the file'),
                              )
                          )  
                    );
                }
				elseif (!ImageManager::resize($temp_name, $fileName, null, null, $type))
                {
                    die(
                          Tools::jsonEncode(
                              array(
                                    'error'=>true,
                                    'errors' => $this->displayError($this->l('An error occurred during the image upload process.')),
                              )
                          )  
                    );
                }
				if (isset($temp_name))
					@unlink($temp_name);
                return $imageName;
            }
            else
            {
                die(
                  Tools::jsonEncode(
                      array(
                            'error'=>true,
                            'errors' => $this->l('Sorry, only JPG, JPEG, PNG & GIF files are allowed.'),
                      )
                  )  
                );
            }
        }
    }
    public function duplicateSlide($id_slide){
        $slide = new Pg_slide($id_slide);
        $slide_new = new Pg_slide();
        $languages = Language::getLanguages(false);
        $slide_new->active = $slide->active; 
        $slide_new->backgroud_color = $slide->backgroud_color;
        $slide_new->repeat = $slide->repeat;
        $slide_new->background_size = $slide->background_size;
        $slide_new->background_position = $slide->background_position;
        $slide_new->effect_in = $slide->effect_in;
        $slide_new->effect_in = $slide->effect_in;
        $slide_new->time_in = $slide->time_in;
        $slide_new->time_out = $slide->time_out;
        $slide_new->custom_class = $slide->custom_class;
        $slide_new->id_pg_group_slide = $slide->id_pg_group_slide;
        $position_max = Db::getInstance()->getValue('SELECT MAX(position) FROM '._DB_PREFIX_.'pg_slide');
        $slide_new->position = $position_max+1;
        foreach($languages as $language)
        {
            $slide_new->title[$language['id_lang']] = $slide->title[$language['id_lang']];
            $slide_new->link[$language['id_lang']] = $slide->link[$language['id_lang']];
        }
        if($slide->backgroud_image)
        {
            $image= $slide->backgroud_image;
            $type = Tools::strtolower(Tools::substr(strrchr($slide->backgroud_image, '.'), 1));
            $image2 = sha1(microtime()).'.'.$type;
            copy(_PS_MODULE_DIR_.'pg_slider/views/img/slide/'.$image,_PS_MODULE_DIR_.'pg_slider/views/img/slide/'.$image2);
            $slide_new->backgroud_image=$image2;
        }
        $slide_new->add();
        $slide_items = Db::getInstance()->executeS('SELECT id_pg_slide_item FROM '._DB_PREFIX_.'pg_slide_item WHERE id_pg_slide='.(int)$id_slide);
        if($slide_items)
            foreach($slide_items as $slide_item)
            {
                $this->duplicateSlideItem($slide_item['id_pg_slide_item'],$slide_new->id);
            }
        
        die(Tools::jsonEncode(
            array(
                'success' => $this->l('successfully duplicate'),
                'id_slide'=> $slide_new->id,
                'list_slide'=> $this->updateListSlide($slide_new->id),
                'list_item'=> $this->getFormItemSlide($slide_new->id),
            )
        ));
    }
    public function duplicateSlideItem($id_item,$id_slide=0)
    {
        $pg_slide_item = new Pg_slide_item($id_item);
        $pg_slide_item_new = new Pg_slide_item();
        $languages = Language::getLanguages(false);
        if((int)$id_slide)
            $pg_slide_item_new->id_pg_slide = (int)$id_slide;
        else
            $pg_slide_item_new -> id_pg_slide= $pg_slide_item->id_pg_slide;
        $pg_slide_item_new->active = $pg_slide_item->active;
        $pg_slide_item_new->type = $pg_slide_item->type;
        $pg_slide_item_new->font_family = $pg_slide_item->font_family;
        $pg_slide_item_new->font_size = $pg_slide_item->font_size;
        $pg_slide_item_new->background_color = $pg_slide_item->background_color;
        $pg_slide_item_new->color = $pg_slide_item->color;
        $pg_slide_item_new->padding_top = $pg_slide_item->padding_top;
        $pg_slide_item_new->padding_right = $pg_slide_item->padding_right;
        $pg_slide_item_new->padding_bottom = $pg_slide_item->padding_bottom;
        $pg_slide_item_new->padding_left = $pg_slide_item->padding_left;
        $pg_slide_item_new->font_weight = $pg_slide_item->font_weight;
        $pg_slide_item_new->text_transform = $pg_slide_item->text_transform;
        $pg_slide_item_new->text_decoration = $pg_slide_item->text_decoration;
        $pg_slide_item_new->custom_class = $pg_slide_item->custom_class;
        $pg_slide_item_new->width = $pg_slide_item->width;
        $pg_slide_item_new->height = $pg_slide_item->height;
        $pg_slide_item_new->position_top = $pg_slide_item->position_top;
        $pg_slide_item_new->position_left = $pg_slide_item->position_left;
        $pg_slide_item_new->effect_in = $pg_slide_item->effect_in;
        $pg_slide_item_new->delay_in = $pg_slide_item->delay_in;
        $pg_slide_item_new->time_effect_in = $pg_slide_item->time_effect_in;
        $pg_slide_item_new->effect_out = $pg_slide_item->effect_out;
        $pg_slide_item_new->delay_out = $pg_slide_item->delay_out;
        $pg_slide_item_new->time_effect_out = $pg_slide_item->time_effect_out;
        foreach($languages as $language)
        {
            $pg_slide_item_new->link[$language['id_lang']] = $pg_slide_item->link[$language['id_lang']];
            $pg_slide_item_new->text_html[$language['id_lang']] = $pg_slide_item->text_html[$language['id_lang']];
            $pg_slide_item_new->label_button[$language['id_lang']] = $pg_slide_item->label_button[$language['id_lang']];
        }
        if($pg_slide_item->image)
        {
            $image= $pg_slide_item->image;
            $type = Tools::strtolower(Tools::substr(strrchr($pg_slide_item->image, '.'), 1));
            $image2 = sha1(microtime()).'.'.$type;
            copy(_PS_MODULE_DIR_.'pg_slider/views/img/item/'.$image,_PS_MODULE_DIR_.'pg_slider/views/img/item/'.$image2);
            $pg_slide_item_new->image=$image2;
        }
        $pg_slide_item_new->add();
        if(!$id_slide)
        {
            die(Tools::jsonEncode(
                array(
                    'success' => $this->l('successfully duplicate'),
                    'list_item'=> $this->getFormItemSlide($pg_slide_item_new -> id_pg_slide),
                )
            ));
        }
        return $pg_slide_item_new->id;
    }
    public function updateListSlide($id_slide=0)
    {
        $slides = $this->getSlides();
        if(!$id_slide && $slides)
            $id_slide = (int)$slides[0]['id_pg_slide'];
        $this->context->smarty->assign(
            array(
                'slides'=>$slides,
                'id_slide'=>$id_slide,
            )
        );
        return $this->display(__FILE__,'update_list_slide.tpl');
    }
    public function hookDisplayHeader($params)
	{
       $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/pg_slide.css','all');
       $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/animate.css','all');
       if(version_compare(_PS_VERSION_, '1.7', '>='))
            $this->context->controller->registerStylesheet('pg_slider', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', ['media' => 'all']);
       else
            $this->context->controller->addCSS('http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','all');
       $this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/pg_slide.js','all');
	}
    public function hookHome()
    {
        $sql ='SELECT gs.id_pg_group_slide as id_group,g.* FROM '._DB_PREFIX_.'pg_group_slide_shop gs
        LEFT JOIN '._DB_PREFIX_.'pg_group_slide g ON (g.id_pg_group_slide = gs.id_pg_group_slide)
        WHERE gs.id_shop ="'.(int)$this->context->shop->id.'" AND g.active=1 AND position_hook="home"';
        $groupslides = Db::getInstance()->executeS($sql);
        if($groupslides)
        {
            
            foreach($groupslides as &$groupslide)
            {
                $groupslide['slides'] = $this->getSlides($groupslide['id_group'],true);
                if($groupslide['slides'])
                {
                    foreach($groupslide['slides'] as &$slide)
                    {
                        $slide['items'] = $this->getItemSlide($slide['id_pg_slide'],'',true);
                    }
                }
            }
        }
        $this->context->smarty->assign(
            array(
                'groupslides' => $groupslides,
                'image_baseurl' => $this->_path.'/views/img/',
            )
        );
        return $this->display(__FILE__,'hookslide.tpl');
    }
    public function displaySlides($id_group_slide,$id_block)
    {
        $id_shop= $this->context->shop->id;
        $id_lang= $this->context->language->id;
        $slides = $this->getSlides($id_group_slide,true);
        $groupslide = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'pg_group_slide where id_pg_group_slide='.(int)$id_group_slide.' AND active=1');
        if($groupslide)
        {
            if($slides)
            {
                foreach($slides as &$slide)
                {
                    $slide['items'] = $this->getItemSlide($slide['id_pg_slide'],'',true);
                }
            }
            $this->context->smarty->assign(
                array(
                    'slides'=>$slides,
                    'groupslide'=>$groupslide,
                    'id_group_slide'=>$id_group_slide,
                    'image_baseurl' => $this->_path.'/views/img/',
                    'id_block' =>$id_block,
                )        
            );
            return $this->display(__FILE__, 'pg_slider.tpl');
        }
    }
    public function hookPGCustomSlide()
    {
      //custom_hook  
      $sql ='SELECT gs.id_pg_group_slide as id_group,g.* FROM '._DB_PREFIX_.'pg_group_slide_shop gs
      LEFT JOIN '._DB_PREFIX_.'pg_group_slide g ON (g.id_pg_group_slide = gs.id_pg_group_slide)
      WHERE gs.id_shop ="'.(int)$this->context->shop->id.'" AND g.active=1 AND position_hook="custom_hook"';
      $groupslides = Db::getInstance()->executeS($sql);
        if($groupslides)
        {
            
            foreach($groupslides as &$groupslide)
            {
                $groupslide['slides'] = $this->getSlides($groupslide['id_group'],true);
                if($groupslide['slides'])
                {
                    foreach($groupslide['slides'] as &$slide)
                    {
                        $slide['items'] = $this->getItemSlide($slide['id_pg_slide'],'',true);
                    }
                }
            }
        }
        $this->context->smarty->assign(
            array(
                'groupslides' => $groupslides,
                'image_baseurl' => $this->_path.'/views/img/',
            )
        );
        return $this->display(__FILE__,'hookslide.tpl');
    }
}