<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once('tea_lookbook.php');

$lookbook = new Tea_LookBook();
$lookbooks = array();
if(Tools::getValue('action')=='save_note')
{
    $id_lookbook=Tools::getValue('id_lookbook');
    $datas = Tools::jsonDecode(Tools::getValue('data'));
    if(count($datas))
    {
        Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."product_lookbook WHERE id_tea_lookbook=".(int)$id_lookbook);
        foreach($datas as $data)
        {
            $id_product= $data->id_product;
            $left=$data->left;
            $top = $data->top;
            Db::getInstance()->execute("INSERT INTO "._DB_PREFIX_."product_lookbook(id_product_lookbook,id_product,id_tea_lookbook,`left`,`top`) values('','".$id_product."','".$id_lookbook."','".$left."','".$top."')");
            
        }
        echo Tools::jsonEncode(
            array(
                'ok' =>true,
            )
        );
    }
    else
    {
        echo Tools::jsonEncode(
            array(
                'ok'=>false,
                
            )
        );
    }
        
}
if (!Tools::isSubmit('secure_key') || Tools::getValue('secure_key') != $lookbook->secure_key || !Tools::getValue('action'))
	die(1);

if (Tools::getValue('action') == 'updateLookBooksPosition' && Tools::getValue('lookbooks'))
{
	$lookbooks = Tools::getValue('lookbooks');

	foreach ($lookbooks as $position => $id_lookbook)
		$res = Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'tea_lookbook` SET `position` = '.(int)$position.'
			WHERE `id_tea_lookbook` = '.(int)$id_lookbook
		);
}