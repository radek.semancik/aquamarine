<?php
/*
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2015 PrestaShop SA
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class LookBook extends ObjectModel
{
    public $title;
    public $id_grouplook;
    public $image;
    public $width;
    public $height;
    public $position;
    public $custom_class;
    public $link;
    public $description;
    public $active;
    public $id_shop;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'tea_lookbook',
        'primary' => 'id_tea_lookbook',
        'multilang' => true,
        'fields' => array(
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'id_grouplook' => array('type' => self::TYPE_INT),
            'title' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
            'position' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            'image' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 255),
            'width' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'size' => 30, 'required' => true),
            'height' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'size' => 30, 'required' => true),
            'custom_class' => array('type' => self::TYPE_STRING),
            'link' => array('type' => self::TYPE_STRING, 'lang' => true),
            'description' => array('type' => self::TYPE_STRING, 'lang' => true),
        )
    );

    public function __construct($id_lookbook = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id_lookbook, $id_lang, $id_shop);
    }

    public function add($autodate = true, $null_values = false)
    {
        $context = Context::getContext();
        $id_shop = $context->shop->id;

        $res = parent::add($autodate, $null_values);
        $res &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'lookbook` (`id_shop`, `id_tea_lookbook`)
			VALUES('.(int) $id_shop.', '.(int) $this->id.')'
        );
        return $res;
    }

    public function delete()
    {
        $res = true;

        $images = $this->image;
        foreach ($images as $image) {
            if (preg_match('/sample/', $image) === 0)
                    if ($image && file_exists(dirname(__FILE__).'/images/'.$image)) $res &= @unlink(dirname(__FILE__).'/images/'.$image);
        }

        $res &= $this->reOrderPositions();

        $res &= Db::getInstance()->execute('
			DELETE FROM `'._DB_PREFIX_.'lookbook`
			WHERE `id_tea_lookbook` = '.(int) $this->id
        );

        $res &= parent::delete();
        return $res;
    }

    public function reOrderPositions()
    {
        $id_lookbook = $this->id;
        $context     = Context::getContext();
        $id_shop     = $context->shop->id;

        $max = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT MAX(tl.`position`) as position
			FROM `'._DB_PREFIX_.'tea_lookbook` tl, `'._DB_PREFIX_.'lookbook` ls
			WHERE tl.`id_tea_lookbook` = ls.`id_tea_lookbook` AND ls.`id_shop` = '.(int) $id_shop
        );

        if ((int) $max == (int) $id_lookbook) return true;

        $rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hss.`position` as position, hss.`id_tea_lookbook` as id_lookbook
			FROM `'._DB_PREFIX_.'tea_lookbook` hss
			LEFT JOIN `'._DB_PREFIX_.'lookbook` hs ON (hss.`id_tea_lookbook` = hs.`id_tea_lookbook`)
			WHERE hs.`id_shop` = '.(int) $id_shop.' AND hss.`position` > '.(int) $this->position
        );

        foreach ($rows as $row) {
            $current_slide = new LookBook($row['id_lookbook']);
            --$current_slide->position;
            $current_slide->update();
            unset($current_slide);
        }

        return true;
    }

    public static function getAssociatedIdsShop($id_lookbook)
    {
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_shop`
			FROM `'._DB_PREFIX_.'lookbook` hs
			WHERE hs.`id_tea_lookbook` = '.(int) $id_lookbook
        );

        if (!is_array($result)) return false;

        $return = array();

        foreach ($result as $id_shop)
            $return[] = (int) $id_shop['id_shop'];

        return $return;
    }
}