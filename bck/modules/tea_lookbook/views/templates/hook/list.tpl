{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($ajax) || !$ajax}
<div class="pg_lb_loading">
    <div class="pg_lb_loading_content">
    <div class="cssload-loader">
    	<div class="cssload-inner cssload-one"></div>
    	<div class="cssload-inner cssload-two"></div>
    	<div class="cssload-inner cssload-three"></div>
    </div>
    </div>
</div>
<div class="panel"><h3><i class="icon-list-ul"></i> {l s='Look book list' mod='tea_lookbook'}
	<span class="panel-heading-action">
		<a id="desc-product-new" class="list-toolbar-btn" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&layout={$grouplook->position}">
			<span title="{l s='back' mod='tea_lookbook'}" data-html="true">
				<i class="process-icon-back"></i>
			</span>
		</a>
	</span>
	</h3>
	<div id="lookbooksContent">
		<div id="lookbooks">
{/if}
			{foreach from=$lookbooks item=lookbook}
				<div id="lookbooks_{$lookbook.id_lookbook}" class="panel">
					<div class="row">
						<div class="col-lg-1">
							<span class="lb_move_icon"><i class="icon-arrows "></i></span>
						</div>
						<div class="col-md-2">
							<img src="{$image_baseurl}{$lookbook.image}" alt="{$lookbook.title}" class="img-thumbnail" />
						</div>
						<div class="col-md-9">
							<h4 class="pull-left">
								#{$lookbook.id_lookbook} - {$lookbook.title}
							</h4>
							<div class="btn-group-action pull-right">
                                <a class="btn {if $lookbook.active}btn-success{else}btn-danger{/if}" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&changeLookBookStatus&id_lookbook={$lookbook.id_lookbook}" title="{if $lookbook.active|intval}{l s='Enabled' mod='tea_lookbook'}{else}{l s='Disable' mod='tea_lookbook'}{/if}">
                                    <i class="{if $lookbook.active}icon-check{else}icon-remove{/if}"></i>{if $lookbook.active}{l s='Enabled' mod='tea_lookbook'}{else}{l s='Disabled' mod='tea_lookbook'}{/if}
							     </a> 
								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&add_note=1&id_lookbook={$lookbook.id_lookbook}">
									<i class="icon-plus"></i>
									{l s='Add note' mod='tea_lookbook'}
								</a>
								<span class="btn btn-default edit_lookbook"
									href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&id_lookbook={$lookbook.id_lookbook}">
									<i class="icon-edit"></i>
									{l s='Edit' mod='tea_lookbook'}
								</span>
								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&delete_id_lookbook={$lookbook.id_lookbook}">
									<i class="icon-trash"></i>
									{l s='Delete' mod='tea_lookbook'}
								</a>
							</div>
						</div>
					</div>
				</div>
			{/foreach}
            <span class="pg_lb_addnew addnew_lookbook" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&addlookbook=1&id_grouplook={$id_grouplook|intval}"><i class="fa fa-plus" aria-hidden="true"></i></span>
{if !isset($ajax) || !$ajax}
        </div>
	</div>
</div>
<div id="block-form">

</div>
{/if}
