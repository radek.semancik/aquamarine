{if !isset($ajax) || !$ajax}
<div class="pg_lb_loading">
    <div class="pg_lb_loading_content">
    <div class="cssload-loader">
    	<div class="cssload-inner cssload-one"></div>
    	<div class="cssload-inner cssload-two"></div>
    	<div class="cssload-inner cssload-three"></div>
    </div>
    </div>
</div>
<div class="container">
    <h2>{l s='Module Lookbook' mod='tea_lookbook'}</h2>
    <ul class="pg_lb_tab">
        <li><a class="header {if isset($position) && $position=='home_page'}active{/if}" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&layout=home_page">{l s='Home page' mod='tea_lookbook'}</a></li>
        <li><a class="option {if isset($position) && $position=='lookbook_page'}active{/if}" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&layout=lookbook_page">{l s='Lookbook Page' mod='tea_lookbook'}</a></li>
        <li><a class="option {if isset($position) && $position=='lookbook_product_page'}active{/if}" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&layout=lookbook_product_page">{l s='Lookbook Product Page' mod='tea_lookbook'}</a></li>
        <li><a class="option {if isset($position) && $position=='lookbook_category_page'}active{/if}" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&layout=lookbook_category_page">{l s='Lookbook Category Page' mod='tea_lookbook'}</a></li>
        <li><span class="option setting_banner" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&layout=config_look">{l s='Discription and banner' mod='tea_lookbook'}</span></li>
    </ul>
    {if isset($position) && $position=='lookbook_page'}
        <a class="link_lookbook_page" target="_blank" href="{$link->getModuleLink('tea_lookbook','default')}">{l s='View Page lookbook'}</a>
    {/if}
    <div id="list-grouplook" class="ui-sortable pg_lb_tab_content active">
{/if}
        {if isset($groupLooks) && $groupLooks}
            <div id="grouplook_list">
                {foreach from=$groupLooks item='groupLook'}
                    <div id="grouplook_{$groupLook.id_grouplook}" class="panel">
                        <div class="col-lg-1">
                			<span><i class="icon-arrows"></i></span>
                		</div>
                        <div class="col-md-11">
                			<h4 class="pull-left">#{$groupLook.id_grouplook} - {$groupLook.title}</h4>
                            {if $groupLook.id_product}
                                <div class="product-name"><a href="{$groupLook.link_product}" title="{$groupLook.name_product}" target="_blank">#{$groupLook.id_product}-{$groupLook.name_product}</a></div>
                                <a href="{$groupLook.link_product}" title="{$groupLook.name_product}" target="_blank"><img src="{$groupLook.url_image}" alt="{$groupLook.name_product}" class="image-product"/></a>
                            {/if}
                			<div class="pg_lb_group_button pull-right">
                				<span class="btn btn-default edit_group_look" data-toggle="modal" data-target="#pg_lb_modal_section_edit" title="{l s='Edit group' mod='tea_lookbook'}" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&editgrouplook=1&id_grouplook={$groupLook.id_grouplook}&layout={$position}">
                					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                				</span>
                				<a class="btn btn-default" title="{l s='View group' mod='tea_lookbook'}" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&viewgrouplook=1&id_grouplook={$groupLook.id_grouplook|intval}">
                					<i class="fa fa-eye" aria-hidden="true"></i>
                				</a>
                				<a class="btn btn-default" title="{l s='Delete' mod='tea_lookbook'}" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&deletegrouplook=1&id_grouplook={$groupLook.id_grouplook|intval}" >
                					<i class="fa fa-trash" aria-hidden="true"></i>
                				</a>
                			</div>
                		</div>
                    </div>
                  {/foreach}
            </div>
        {/if}
        <span class="pg_lb_addnew addnew_group" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&addgrouplook=1&layout={$position}"><i class="fa fa-plus" aria-hidden="true"></i></span>
{if !isset($ajax) || !$ajax}
    </div>   
</div>
<div id="block-form">

</div>
{/if}