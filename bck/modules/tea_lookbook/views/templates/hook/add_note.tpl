<style>
.image-note .draggable {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: medium none;
    width: 36px !important;
}
.ui-widget-content {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  color: #222222;
  border:none;
}
</style>
<script type="text/javascript">
var $image_baseurl='{$image_baseurl}';
</script>
    <div id="fieldset_0" class="panel">
        <div class="panel-heading">
            <i class="icon-cogs"></i>
            {l s='Add note' mod='tea_lookbook'}
            <span class="panel-heading-action">
        		<a id="desc-product-new" class="list-toolbar-btn" href="{$link->getAdminLink('AdminModules')}&configure=tea_lookbook&viewgrouplook=1&id_grouplook={$lookbook->id_grouplook|intval}">
        			<span title="{l s='Back' mod='tea_lookbook'}" data-html="true">
        				<i class="process-icon-back"></i>
        			</span>
        		</a>
        	</span>
            <span id="add_note" style="margin-right: 10px; margin-top: 10px;" data-lookbook="{$lookbook->id|intval}" class="btn btn-default pull-right"><i class="icon-plus"></i>{l s='ADD NOTE' mod='tea_lookbook'}</span>
        </div>
        <div class="form-wrapper">
            <div class="row_lookbook_addnote">
                <div class="lookbook_addnote_content">
                
            {if isset($lookbook)}
                <div class="row lookbook_addnoteform" style="background-size: 100%;background-image: url('{$image_baseurl}{$lookbook->image}'); width:{$lookbook->width}px; height:{$lookbook->height}px;" >
                    {if isset($lookbook_products) && $lookbook_products}
                        {foreach from=$lookbook_products key='key' item='lookbook_product'}
                            <div id="draggable-{$lookbook_product.id_product_lookbook|intval}" class="ui-widget-content draggable edit-note" data-product_lookbook="{$lookbook_product.id_product_lookbook|intval}" style="position: relative; left: {$lookbook_product.left}px; top: {$lookbook_product.top}px; bottom: auto;" data-left="{$lookbook_product.left|floatval}" data-top="{$lookbook_product.top|floatval}" data-id_product_lookbook="{$lookbook_product.id_product_lookbook|intval}" data-animation="{if $lookbook_product.efect_when_hover}{$lookbook_product.efect_when_hover}{else}fade{/if}">
                                <div class="note-type">
                                    {if $lookbook_product.type=="number" || $lookbook_product.type=="price"}
                                        {if $lookbook_product.type=="number"}
                                            <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="{if $lookbook_product.note_circle}note_circle{/if}">{if $lookbook_product.number}{$lookbook_product.number|intval}{else}{$lookbook_product.id_product_lookbook|intval}{/if}</span>
                                        {else}
                                            <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="{if $lookbook_product.note_circle}note_circle{/if}">{$lookbook_product.price}</span>
                                        {/if}
                                    {else}
                                       <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="normal {if $lookbook_product.note_circle}note_circle{/if}"></span>
                                    {/if}
                                </div>
                                {if $lookbook_product.content_type=='html'}
                                    <div class="note-menu pg_animation {if $lookbook_product.position_product}pos_{$lookbook_product.position_product}{else}pos_bottom{/if}">
                                        <div class="product-info" style="text-align: center;">
                                            <div class="html_content">
                                                {$lookbook_product.html_content}
                                            </div>
                                            <div class="product-action" style="text-align: center;">
                                                <a class="submitdelete deletion" href="javascript:void(0);" data-id-lookbook="{$lookbook_product.id_product_lookbook|intval}">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                {else}
                                    {if $lookbook_product.show_title}
                                        <div class="note-title pg_animation {if $lookbook_product.efect_when_hover}pg_{$lookbook_product.efect_when_hover}{else}pgfade{/if}">
                                            <div class="product-name">{$lookbook_product.name_product}</div>
                                        </div>
                                    {/if}
                                    <div class="note-menu pg_animation {if $lookbook_product.position_product}pos_{$lookbook_product.position_product}{else}pos_bottom{/if}">
                                        <div class="product-info" style="text-align: center;">
                                            {if $lookbook_product.show_img}
                                                <div class="product-image"><img {if !$lookbook_product.id_product}style="display:none;"{/if} src="{$lookbook_product.link_img}"/></div>
                                            {/if}
                                            {if $lookbook_product.show_name}
                                                <div class="product-name">{$lookbook_product.name_product}</div>
                                            {/if}
                                            {if $lookbook_product.show_price}
                                                <div class="product-price">{$lookbook_product.price}</div>
                                            {/if}
                                            {if $lookbook_product.show_description}
                                                <div class="product-description">{$lookbook_product.description_product|truncate:150:'...' nofilter}</div>
                                            {/if}
                                            <div class="product-action" style="text-align: center;">
                                                <a class="submitdelete deletion" href="javascript:void(0);" data-id-lookbook="{$lookbook_product.id_product_lookbook|intval}">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                {/if}
                            </div>
                            <script type="text/javascript">
                                    makeDrag({$lookbook_product.id_product_lookbook|intval});
                            </script>
                        {/foreach}
                    {/if}
                </div>
            {/if}
            </div>
            </div>
        </div>
    </div>
<div id="block-form-note">
    
</div>

