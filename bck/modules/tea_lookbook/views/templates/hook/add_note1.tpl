<div class="pg_lb_addnote_form">
<div class="pg_lb_addnote_form_header">
    <i class="icon-cogs"></i> Add note 
    <div class="pg_lb_addnote_form_listbutton">
        <a id="add_note" class="pg_lb_button">
            <i class="icon-plus"></i>ADD NOTE
        </a>
        <a id="back_to_list" class="pg_lb_button">
            <i class="icon-plus"></i>Back to list
        </a>
        <a id="add_note_save" class="pg_lb_button">
            <i class="icon-floppy-o"></i>SAVE
        </a>
    </div>
</div>
<div class="pg_lb_addnote_form_content">
    <div class="pg_lb_addnote_img" style="background-size: 100%;background-image: url('{$image_baseurl}{$lookbook->image}'); width:{$lookbook->width}px; height:{$lookbook->height}px;" >
        {foreach from=$lookbook_products key='key' item='lookbook_product'}
            <div id="draggable-{$key+1}" class="ui-widget-content draggable" style="position: relative; left: {$lookbook_product.left}px; top: {$lookbook_product.top}px; width: 40px; right: auto; height: 40px; bottom: auto;" data-left="{$lookbook_product.left}" data-top="{$lookbook_product.top}" data-id_product="{$lookbook_product.id_product}">
                <img class="spot" src="{$image_baseurl}hotspot-icon.png" width="40px" height="40px" />
                <div class="note-menu" style="display: none;">
                    <div class="product-info" style="text-align: center;">
                        <div class="product-image"><img src="{$lookbook_product.link_img}" /></div>
                        <div class="product-name">{$lookbook_product.name_product}</div>
                    </div>
                    <div class="find-product" style="margin: 0 auto;">
                        <form>
                            <input class="ac_input" type="text" placeholder="input product name" name="note-product" autocomplete="off" />
                        </form>
                    </div>
                    <div class="product-action" style="text-align: center;">
                        <a class="submitdelete deletion" href="javascript:void(0);">Remove</a>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                    makeDrag({$key+1});
                    SearchProduct($('#draggable-{$key+1}')); 
            </script>
        {/foreach}
    </div>
    <div class="pg_lb_addnote_config">
        <div class="pg_lb_addnote_config_header">
            <h3>setting</h3>
        </div>
        <form id="module_form" class="defaultForm form-horizontal" novalidate="" enctype="multipart/form-data" method="post" action="">
            <div class="pg_lb_addnote_config_form">
                <div class="pg_form_control">
                    <label>Type note</label>
                    <select>
                        <option>Normal</option>
                        <option>Number</option>
                        <option>Price</option>
                    </select>
                </div>
                <div class="pg_form_control">
                    <label>Product</label>
                    <input placeholder="search product" type="text" />
                </div>
                <div class="pg_form_control">
                    <label>Efect when hover</label>
                    <select>
                        <option>Fade</option>
                        <option>Fade in up</option>
                        <option>Fade in down</option>
                        <option>Left to Right</option>
                        <option>Right to Left</option>
                        <option>Zoom</option>
                    </select>
                </div>
                <div class="pg_form_control">
                    <label>Background</label>
                    <input type="color" />
                </div>
                <div class="pg_form_control">
                    <label>Color</label>
                    <input type="color" />
                </div>
                <div class="pg_form_control">
                    <label>Show img</label>
                    Yes/No
                </div>
                <div class="pg_form_control">
                    <label>Show name</label>
                    Yes/No
                </div>
                <div class="pg_form_control">
                    <label>Show Price</label>
                    Yes/No
                </div>
                <div class="pg_form_control">
                    <button class="remove_note">Delete</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>