{if $groupLooks}
    <div id="pg_lb_category_custom" class="pg_page_lookbook">

        {foreach from=$groupLooks item='groupLook'}
            {assign var='row_item' value=12/$groupLook.item_in_row}
            {assign var='row_item_mobile' value=12/$groupLook.item_in_row_mobile}
            {assign var='row_item_tablet' value=12/$groupLook.item_in_row_tablet}
            {assign var='row_masonry' value=12/$groupLook.masonry_column}
            {assign var='row_masonry_mobile' value=12/$groupLook.masonry_column_mobile}
            {assign var='row_masonry_tablet' value=12/$groupLook.masonry_column_tablet}
            {if $groupLook.lookbooks}
                {if isset($groupLook.type) && $groupLook.type == 'slide'}

                    {if isset($groupLook.full_width) && $groupLook.full_width == 1}
                        <div class="{$groupLook.animation} {if $groupLook.item_in_row == 1}pg_lookbook_item_loop_one {else}pg_lookbook_item_loop_multi {/if}{if isset($groupLook.custom_class) && $groupLook.custom_class} {$groupLook.custom_class}{/if}" style="{if isset($groupLook.margin) && $groupLook.margin}margin:{$groupLook.margin};{/if}{if isset($groupLook.padding) && $groupLook.padding}padding:{$groupLook.padding};{/if} {if isset($groupLook.background) && $groupLook.background}background-color:{$groupLook.background}{/if}">
                        {else}
                            <div class="container">
                                <div class="row">
                                    <div>
                                    {/if}
                                    <div class="{$groupLook.effect} grouplook_{$groupLook.type}" data-item-desktop="{$groupLook.item_in_row}" data-item-tablet="{$groupLook.item_in_row_tablet}" data-item-mobile="{$groupLook.item_in_row_mobile}" data-effect="{$groupLook.effect}" data-autoplay="{$groupLook.autoplay}" data-show-next-preview="{$groupLook.show_next_preview}" data-dots="{$groupLook.pavination}" data-loop="{$groupLook.infinite}">
                                        {foreach from=$groupLook.lookbooks item='lookbook'}
                                            <div class="row_lookbook grid-item col-ms-6 col-sm-6 col-xs-12 col-md-4 col-lg-{$row_item}">
                                                <div class="panel-lookbook" {if $groupLook.padding_item}style="padding:{$groupLook.padding_item}"{/if}>
                                                    <div class="panel-lookbook-wrap{if isset($lookbook.custom_class) && $lookbook.custom_class} {$lookbook.custom_class}{/if}" data-width="{$lookbook.width}" data-height="{$lookbook.height}">
                                                        {if isset($lookbook.link) && $lookbook.link}
                                                            <a href="{$lookbook.link}" class="pg_lookbook_item_link">
                                                            {/if}
                                                            <img src="{$image_baseurl}{$lookbook.image}" alt="" title="" />
                                                            {if isset($lookbook.link) && $lookbook.link}
                                                            </a>
                                                        {/if}
                                                        {if isset($lookbook.lookbook_products) && $lookbook.lookbook_products}
                                                            {foreach from=$lookbook.lookbook_products key='key' item='lookbook_product'}
                                                                <div class="draggable draggable_note" data-pos-left="{$lookbook_product.left}" data-pos-top="{$lookbook_product.top}" style="position: absolute; right: auto; bottom: auto;" data-animation="{if $lookbook_product.efect_when_hover}{$lookbook_product.efect_when_hover}{else}fade{/if}" >
                                                                    <div class="note-type">
                                                                        {if $lookbook_product.type=="number" || $lookbook_product.type=="price"}
                                                                            {if $lookbook_product.type=="number"}
                                                                                <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="{if $lookbook_product.note_circle}note_circle{/if}">{if $lookbook_product.number}{$lookbook_product.number|intval}{else}{$lookbook_product.id_product_lookbook|intval}{/if}<i class="pe-7s-close"></i></span>
                                                                                {else}
                                                                                <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="{if $lookbook_product.note_circle}note_circle{/if}">{$lookbook_product.price}<i class="pe-7s-close"></i></span>
                                                                                {/if}
                                                                            {else}
                                                                            <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="normal {if $lookbook_product.note_circle}note_circle{/if}">
                                                                                <i class="pe-7s-close"></i>
                                                                            </span>
                                                                        {/if}
                                                                    </div>
                                                                    {if $lookbook_product['content_type']=='html'}
                                                                        <div class="note-menu pg_animation {if $lookbook_product.position_product}pos_{$lookbook_product.position_product}{else}pos_bottom{/if}">
                                                                            <div class="product-info" style="text-align: center;">
                                                                                <span class="close_product_info">{l s='close' mod='tea_lookbook'}</span>
                                                                                <div class="html_content">{$lookbook_product.html_content nofilter}</div>    
                                                                            </div>
                                                                        </div>
                                                                    {else}
                                                                        {if $lookbook_product.show_title}
                                                                            <div class="note-title pg_animation {if $lookbook_product.efect_when_hover}pg_{$lookbook_product.efect_when_hover}{else}pg_fade{/if}">
                                                                                <div class="product-name">{$lookbook_product.name_product}</div>
                                                                            </div>
                                                                        {/if}
                                                                        <div class="note-menu pg_animation {if $lookbook_product.position_product}pos_{$lookbook_product.position_product}{else}pos_bottom{/if}">
                                                                            <div class="product-info" style="text-align: center;">
                                                                                <span class="close_product_info">{l s='close' mod='tea_lookbook'}</span>
                                                                                {if $lookbook_product.show_img}
                                                                                    <div class="product-image"><img {if !$lookbook_product.id_product}style="display:none;"{/if} src="{$lookbook_product.link_img}"/></div>
                                                                                    {/if}
                                                                                    {if $lookbook_product.show_name}
                                                                                    <div class="product-name">{$lookbook_product.name_product}</div>
                                                                                {/if}
                                                                                {if $lookbook_product.show_price}
                                                                                    <div class="product-price">{$lookbook_product.price}</div>
                                                                                {/if}
                                                                                {if $lookbook_product.show_description}
                                                                                    <div class="product-description">{$lookbook_product.description_product|truncate:150:'...' nofilter}</div>
                                                                                {/if}
                                                                                {if $lookbook_product.link_product}
                                                                                    <div class="product-fotter">
                                                                                        <a href="{$lookbook_product.link_product}">{l s='detail' mod='tea_lookbook'}</a>
                                                                                        <form action="{if isset($urls)}{$urls.pages.cart}{/if}" method="post">
                                                                                            <input type="hidden" name="token" value="{if isset($static_token)}{$static_token}{/if}" />
                                                                                            <input type="hidden" value="{$lookbook_product.id_product}" name="id_product" />
                                                                                            <input type="hidden" class="input-group form-control" name="qty" min="1" value="1"/>
                                                                                            <button data-button-action="add-to-cart" class="product-item-cart">{l s='Buy now' mod='tea_lookbook'}</button>
                                                                                        </form>
                                                                                    </div>
                                                                                {else}    
                                                                                    <div class="product-fotter category-footer">                                                                        
                                                                                        <a href="{$lookbook_product.link_category}{if $smarty.get.q}?q={$smarty.get.q}{/if}">{l s='detail' mod='tea_lookbook'}</a>
                                                                                    </div>    
                                                                                {/if}    


                                                                            </div>
                                                                        </div>
                                                                    {/if}
                                                                </div>
                                                            {/foreach}
                                                        {/if}
                                                    </div>
                                                    {if $lookbook.description}
                                                        <div class="lookbook-description">
                                                            {$lookbook.description}
                                                        </div>
                                                    {/if}
                                                </div>
                                            </div>
                                        {/foreach}
                                    </div>
                                    {if $groupLook.description}
                                        <div class="grouplook-desctiption {$groupLook.animation}">
                                            {$groupLook.description}
                                        </div>
                                    {/if}
                                    {if isset($groupLook.full_width) && $groupLook.full_width == 1}
                                    </div>
                                {else}
                                </div>
                            </div>
                        </div>
                    {/if}
                {else if isset($groupLook.type) && $groupLook.type == 'grid'}
                    {if isset($groupLook.full_width) && $groupLook.full_width == 1}
                        <div class="{$groupLook.animation} {if isset($groupLook.custom_class) && $groupLook.custom_class} {$groupLook.custom_class}{/if}" style="{if isset($groupLook.margin) && $groupLook.margin}margin:{$groupLook.margin};{/if}{if isset($groupLook.padding) && $groupLook.padding}padding:{$groupLook.padding};{/if}">
                        {else}
                            <div class="container">
                                <div class="row">
                                    <div class="{$groupLook.animation} {if isset($groupLook.custom_class) && $groupLook.custom_class} {$groupLook.custom_class}{/if}" style="{if isset($groupLook.margin) && $groupLook.margin}margin:{$groupLook.margin};{/if}{if isset($groupLook.padding) && $groupLook.padding}padding:{$groupLook.padding};{/if}">
                                    {/if}
                                    <div class="grouplook_{$groupLook.type}" data-item-desktop="{$groupLook.item_in_row}" data-item-tablet="{$groupLook.item_in_row_tablet}" data-item-mobile="{$groupLook.item_in_row_mobile}" data-effect="{$groupLook.animation}" data-autoplay="{$groupLook.autoplay}" data-show-next-preview="{$groupLook.show_next_preview}" >
                                        {foreach from=$groupLook.lookbooks item='lookbook'}
                                            <div class="row_lookbook grid-item-lookbook col-ms-6 col-sm-6 col-xs-12 col-md-4 col-lg-{$row_item}">
                                                <div class="panel-lookbook" {if $groupLook.padding_item}style="padding:{$groupLook.padding_item}"{/if}>
                                                    <div class="panel-lookbook-wrap" data-width="{$lookbook.width}" data-height="{$lookbook.height}">
                                                        <img src="{$image_baseurl}{$lookbook.image}" alt="" title="" />
                                                        {if isset($lookbook.lookbook_products) && $lookbook.lookbook_products}
                                                            {foreach from=$lookbook.lookbook_products key='key' item='lookbook_product'}
                                                                <div class="draggable draggable_note" data-pos-left="{$lookbook_product.left}" data-pos-top="{$lookbook_product.top}" style="position: absolute; right: auto; bottom: auto;" >
                                                                    <div class="note-type">
                                                                        {if $lookbook_product.type=="number" || $lookbook_product.type=="price"}
                                                                            {if $lookbook_product.type=="number"}
                                                                                <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="{if $lookbook_product.note_circle}note_circle{/if}">{if $lookbook_product.number}{$lookbook_product.number|intval}{else}{$lookbook_product.id_product_lookbook|intval}{/if}<i class="pe-7s-close"></i></span>
                                                                                {else}
                                                                                <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="{if $lookbook_product.note_circle}note_circle{/if}">{$lookbook_product.price}<i class="pe-7s-close"></i></span>
                                                                                {/if}
                                                                            {else}
                                                                            <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="normal {if $lookbook_product.note_circle}note_circle{/if}">
                                                                                <i class="pe-7s-close"></i>
                                                                            </span>
                                                                        {/if}
                                                                    </div>
                                                                    {if $lookbook_product['content_type']=='html'}
                                                                        <div class="note-menu pg_animation {if $lookbook_product.position_product}pos_{$lookbook_product.position_product}{else}pos_bottom{/if}">
                                                                            <div class="product-info" style="text-align: center;">
                                                                                <span class="close_product_info">{l s='close' mod='tea_lookbook'}</span>
                                                                                <div class="html_content">{$lookbook_product.html_content nofilter}</div>    
                                                                            </div>
                                                                        </div>
                                                                    {else}
                                                                        {if $lookbook_product.show_title}
                                                                            <div class="note-title pg_animation {if $lookbook_product.efect_when_hover}pg_{$lookbook_product.efect_when_hover}{else}pg_fade{/if}">
                                                                                <div class="product-name">{$lookbook_product.name_product}</div>
                                                                            </div>
                                                                        {/if}
                                                                        <div class="note-menu pg_animation {if $lookbook_product.position_product}pos_{$lookbook_product.position_product}{else}pos_bottom{/if}">
                                                                            <div class="product-info" style="text-align: center;">
                                                                                <span class="close_product_info">{l s='close' mod='tea_lookbook'}</span>
                                                                                {if $lookbook_product.show_img}
                                                                                    <div class="product-image"><img {if !$lookbook_product.id_product}style="display:none;"{/if} src="{$lookbook_product.link_img}"/></div>
                                                                                    {/if}
                                                                                    {if $lookbook_product.show_name}
                                                                                    <div class="product-name">{$lookbook_product.name_product}</div>
                                                                                {/if}
                                                                                {if $lookbook_product.show_price}
                                                                                    <div class="product-price">{$lookbook_product.price}</div>
                                                                                {/if}
                                                                                {if $lookbook_product.show_description}
                                                                                    <div class="product-description">{$lookbook_product.description_product|truncate:150:'...' nofilter}</div>
                                                                                {/if}
                                                                                {if $lookbook_product.link_product}
                                                                                    <div class="product-fotter">
                                                                                        <a href="{$lookbook_product.link_product}">{l s='detail' mod='tea_lookbook'}</a>
                                                                                        <form action="{if isset($urls)}{$urls.pages.cart}{/if}" method="post">
                                                                                            <input type="hidden" name="token" value="{if isset($static_token)}{$static_token}{/if}" />
                                                                                            <input type="hidden" value="{$lookbook_product.id_product}" name="id_product" />
                                                                                            <input type="hidden" class="input-group form-control" name="qty" min="1" value="1"/>
                                                                                            <button data-button-action="add-to-cart" class="product-item-cart">{l s='Buy now' mod='tea_lookbook'}</button>
                                                                                        </form>
                                                                                    </div>
                                                                                {else}    
                                                                                    <div class="product-fotter category-footer">                                                                        
                                                                                        <a href="{$lookbook_product.link_category}{if $smarty.get.q}?q={$smarty.get.q}{/if}">{l s='detail' mod='tea_lookbook'}</a>
                                                                                    </div>    
                                                                                {/if}  
                                                                            </div>
                                                                        </div>
                                                                    {/if}
                                                                </div>
                                                            {/foreach}
                                                        {/if}
                                                    </div>
                                                    {if $lookbook.description}
                                                        <div class="lookbook-description">
                                                            {$lookbook.description}
                                                        </div>
                                                    {/if}
                                                </div>
                                            </div>
                                        {/foreach}
                                    </div>
                                    {if $groupLook.description}
                                        <div class="grouplook-desctiption {$groupLook.animation}">
                                            {$groupLook.description}
                                        </div>
                                    {/if}
                                    {if isset($groupLook.full_width) && $groupLook.full_width == 1}
                                    </div>
                                {else}
                                </div>
                            </div>
                        </div>
                    {/if}
                {else}
                    {if isset($groupLook.full_width) && $groupLook.full_width == 1}
                        <div class="{$groupLook.animation} {if isset($groupLook.custom_class) && $groupLook.custom_class} {$groupLook.custom_class}{/if}" style="{if isset($groupLook.margin) && $groupLook.margin}margin:{$groupLook.margin};{/if}{if isset($groupLook.padding) && $groupLook.padding}padding:{$groupLook.padding};{/if}">
                        {else}
                            <div class="container">
                                <div class="row">
                                    <div class="{$groupLook.animation} {if isset($groupLook.custom_class) && $groupLook.custom_class} {$groupLook.custom_class}{/if}" style="{if isset($groupLook.margin) && $groupLook.margin}margin:{$groupLook.margin};{/if}{if isset($groupLook.padding) && $groupLook.padding}padding:{$groupLook.padding};{/if}">
                                    {/if}
                                    <div class="grouplook_{$groupLook.type}">
                                        <div class="grouplook_masonry_content">
                                            <div class="grid-sizer-lookbook {if $groupLook.type=='masonry'}col-xs-{$row_masonry_mobile} col-sm-{$row_masonry_tablet} col-lg-{$row_masonry}{/if}"></div>
                                            {foreach from=$groupLook.lookbooks item='lookbook'}
                                                <div class="row_lookbook grid-item-masonry grid-item-lookbook col-ms-6 col-sm-6 col-xs-12 col-md-4 {if $groupLook.type=='list'}col-lg-{$row_item}{/if}">
                                                    <div class="panel-lookbook" {if $groupLook.padding_item}style="padding:{$groupLook.padding_item}"{/if}>
                                                        <div class="panel-lookbook-wrap" data-width="{$lookbook.width}" data-height="{$lookbook.height}">
                                                            <img alt="" title="" src="{$image_baseurl}{$lookbook.image}" />
                                                            {if isset($lookbook.lookbook_products) && $lookbook.lookbook_products}
                                                                {foreach from=$lookbook.lookbook_products key='key' item='lookbook_product'}
                                                                    <div class="draggable draggable_note" data-pos-left="{$lookbook_product.left}" data-pos-top="{$lookbook_product.top}" style="position: absolute; right: auto; bottom: auto;" data-animation="{if $lookbook_product.efect_when_hover}{$lookbook_product.efect_when_hover}{else}fade{/if}" >
                                                                        <div class="note-type">
                                                                            {if $lookbook_product.type=="number" || $lookbook_product.type=="price"}
                                                                                {if $lookbook_product.type=="number"}
                                                                                    <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="{if $lookbook_product.note_circle}note_circle{/if}">
                                                                                {if $lookbook_product.number}{$lookbook_product.number|intval}{else}{$lookbook_product.id_product_lookbook|intval}{/if}
                                                                                <i class="pe-7s-close"></i>
                                                                            </span>
                                                                        {else}
                                                                            <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="{if $lookbook_product.note_circle}note_circle{/if}">{$lookbook_product.price}<i class="pe-7s-close"></i></span>
                                                                            {/if}
                                                                        {else}
                                                                        <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="normal {if $lookbook_product.note_circle}note_circle{/if}">
                                                                            <i class="pe-7s-close"></i>
                                                                        </span>
                                                                    {/if}
                                                                </div>
                                                                {if $lookbook_product['content_type']=='html'}
                                                                    <div class="note-menu pg_animation {if $lookbook_product.position_product}pos_{$lookbook_product.position_product}{else}pos_bottom{/if}">
                                                                        <div class="product-info" style="text-align: center;">
                                                                            <span class="close_product_info">{l s='close' mod='tea_lookbook'}</span>
                                                                            <div class="html_content">{$lookbook_product.html_content nofilter}</div>    
                                                                        </div>
                                                                    </div>
                                                                {else}
                                                                    {if $lookbook_product.show_title}
                                                                        <div class="note-title pg_animation {if $lookbook_product.efect_when_hover}pg_{$lookbook_product.efect_when_hover}{else}pg_fade{/if}">
                                                                            <div class="product-name">{$lookbook_product.name_product}</div>
                                                                        </div>
                                                                    {/if}
                                                                    <div class="note-menu pg_animation {if $lookbook_product.position_product}pos_{$lookbook_product.position_product}{else}pos_bottom{/if}">
                                                                        <div class="product-info" style="text-align: center;">
                                                                            <span class="close_product_info">{l s='close' mod='tea_lookbook'}</span>
                                                                            {if $lookbook_product.show_img}
                                                                                <div class="product-image"><img {if !$lookbook_product.id_product}style="display:none;"{/if} src="{$lookbook_product.link_img}"/></div>
                                                                                {/if}
                                                                                {if $lookbook_product.show_name}
                                                                                <div class="product-name">{$lookbook_product.name_product}</div>
                                                                            {/if}
                                                                            {if $lookbook_product.show_price}
                                                                                <div class="product-price">{$lookbook_product.price}</div>
                                                                            {/if}
                                                                            {if $lookbook_product.show_description}
                                                                                <div class="product-description">{$lookbook_product.description_product|truncate:150:'...' nofilter}</div>
                                                                            {/if}
                                                                            {if $lookbook_product.link_product}
                                                                                <div class="product-fotter">
                                                                                    <a href="{$lookbook_product.link_product}">{l s='detail' mod='tea_lookbook'}</a>
                                                                                    <form action="{if isset($urls)}{$urls.pages.cart}{/if}" method="post">
                                                                                        <input type="hidden" name="token" value="{if isset($static_token)}{$static_token}{/if}" />
                                                                                        <input type="hidden" value="{$lookbook_product.id_product}" name="id_product" />
                                                                                        <input type="hidden" class="input-group form-control" name="qty" min="1" value="1"/>
                                                                                        <button data-button-action="add-to-cart" class="product-item-cart">{l s='Buy now' mod='tea_lookbook'}</button>
                                                                                    </form>
                                                                                </div>
                                                                            {else}    
                                                                                <div class="product-fotter category-footer">                                                                        
                                                                                    <a href="{$lookbook_product.link_category}{if $smarty.get.q}?q={$smarty.get.q}{/if}">{l s='detail' mod='tea_lookbook'}</a>
                                                                                </div>    
                                                                            {/if}  
                                                                        </div>
                                                                    </div>
                                                                {/if}
                                                            </div>
                                                        {/foreach}
                                                    {/if}
                                                </div>
                                                {if $lookbook.description}
                                                    <div class="lookbook-description">
                                                        {$lookbook.description}
                                                    </div>
                                                {/if}
                                            </div>
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                            {if $groupLook.description}
                                <div class="grouplook-desctiption {$groupLook.animation}">
                                    {$groupLook.description}
                                </div>
                            {/if}
                            {if isset($groupLook.full_width) && $groupLook.full_width == 1}
                            </div>
                        {else}
                        </div>
                    </div>
                </div>
            {/if}
        {/if}
    {/if}

{/foreach}

</div>
{/if}