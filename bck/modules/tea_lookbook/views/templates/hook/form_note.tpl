<script src="{$module_url}/views/js/jquery.colorpicker.js"></script>
<form id="module_form" class="defaultForm form-horizontal" novalidate="" enctype="multipart/form-data" method="post" action="">
        <input type="hidden" name="id_product_lookbook" id="id_product_lookbook" value="{if isset($id_product_lookbook)}{$id_product_lookbook}{/if}" />
        <input type="hidden" name="SubmitNoteLookBook" value="1" />
        <div class="pg_lb_addnote_config">
            <div class="pg_lb_addnote_config_header">
                <h3>{l s='setting' mod='tea_lookbook'}</h3>
                <span class="pg_mini_setting"><i class="material-icons">open_with</i></span>
            </div>
            <div class="pg_lb_addnote_config_form">
                <div class="pg_form_control">
                    <label for="type_note">{l s='Type note' mod='tea_lookbook'}</label>
                    <select id="type_note" name="type_note">
                        <option value="normal" {if isset($type)&& $type=='normal'} selected="selected" {/if}>{l s='Normal' mod='tea_lookbook'}</option>
                        <option value="number" {if isset($type)&& $type=='number'} selected="selected" {/if}>{l s='Number' mod='tea_lookbook'}</option>
                        <option value="price" {if isset($type)&& $type=='price'} selected="selected"{/if}>{l s='Price' mod='tea_lookbook'}</option>
                    </select>
                </div>
                <div class="pg_form_control display_number">
                    <label for="number">{l s='Number' mod='tea_lookbook'}</label>
                    <input type="text" name="number" id="number" value="{if isset($number)&&$number}{$number}{else}{$id_product_lookbook}{/if}" />
                </div>
                <div class="pg_form_control" id="form-not-content-type">
                     <label for="content_type">{l s='Content type' mod='tea_lookbook'}</label>
                     <select name="content_type">
                            <option value="product" {if isset($content_type) && $content_type=='product'}selected="selected"{/if} >{l s='Product' mod='tea_lookbook'}</option>
                            <option value="category" {if isset($content_type) && $content_type=='category'}selected="selected"{/if} >{l s='Category' mod='tea_lookbook'}</option>
                            <option value="html" {if isset($content_type) && $content_type=='html'}selected="selected"{/if}>{l s='HTML' mod='tea_lookbook'}</option>
                     </select>
                </div>
                <div class="pg_form_control content_type html">
                    <label for="html_content">{l s='HTML content' mod='tea_lookbook'}</label>
                    <div class="form-group-language">
                        {foreach from=$languages item='language'}
                            {if $languages|count > 1}
                        		<div class="translatable-field row lang-{$language.id_lang}">
                        			<div class="col-lg-9">
                        	{/if}   
                            <textarea name="html_content_{$language.id_lang|intval}">{if isset($html_content[$language.id_lang])}{$html_content[$language.id_lang]|htmlentitiesUTF8}{/if}</textarea>
                            {if $languages|count > 1}
                    			</div>
                    			<div class="col-lg-2">
                    				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    					{$language.iso_code}
                    					<span class="caret"></span>
                    				</button>
                    				<ul class="dropdown-menu">
                    					{foreach from=$languages item=language}
                    					<li><a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a></li>
                    					{/foreach}
                    				</ul>
                    			</div>
                    		</div>
                    	{/if}
                        {/foreach}
                    </div>
                </div>
                <div class="pg_form_control content_type product" id="from-note-product">
                    <label for="product">{l s='Product' mod='tea_lookbook'}</label>
                    <input id="id_product" type="hidden" value="{if isset($id_product)}{$id_product}{/if}" name="id_product" />                    
                    <input name="product" class="note-product" type="text" placeholder="search product" autocomplete="off" />
                    <div class="product-search-info">
                        {if isset($link_img) && $link_img}
                            <span class="product-image">
                                <img src="{$link_img}"/>
                            </span>
                        {/if}
                        {if isset($name_product) && $name_product}
                            <span class="product-name">
                                {$name_product}
                            </span>
                        {/if}
                    </div>
                </div>
                    <div class="pg_form_control content_type category" id="from-note-product">
                    <label for="category">{l s='Category' mod='tea_lookbook'}</label>                    
                    <input id="id_category" type="text" value="{if isset($id_category)}{$id_category}{/if}" name="id_category" />
                                                                               
                    <div class="product-search-info">
                        {if isset($link_img) && $link_img}
                            <span class="product-image">
                                <img src="{$link_img}"/>
                            </span>
                        {/if}
                        {if isset($name_product) && $name_product}
                            <span class="product-name">
                                {$name_product}
                            </span>
                        {/if}
                    </div>
                </div>
                <div class="pg_form_control">
                    <label for="link">{l s='Link' mod='tea_lookbook'}</label>                    
                    <input id="link" type="text" value="{if isset($link)}{$link}{/if}" name="link" />
                </div>    
                <div class="pg_form_control">
                    <label for="efect_when_hover">{l s='Efect when hover' mod='tea_lookbook'}</label>
                    <select name="efect_when_hover" id="efect_when_hover">
                        <option value="fade" {if isset($efect_when_hover)&& $efect_when_hover=='fade'} selected="selected" {/if}>{l s='Fade' mod='tea_lookbook'}</option>
                        <option value="fadeinup" {if isset($efect_when_hover)&& $efect_when_hover=='fadeinup'} selected="selected" {/if}>{l s='Fade in up' mod='tea_lookbook'}</option>
                        <option value="fadeindown" {if isset($efect_when_hover)&& $efect_when_hover=='fadeindown'} selected="selected" {/if}>{l s='Fade in down' mod='tea_lookbook'}</option>
                        <option value="lefttoright" {if isset($efect_when_hover)&& $efect_when_hover=='lefttoright'} selected="selected" {/if}>{l s='Left to Right' mod='tea_lookbook'}</option>
                        <option value="righttoleft" {if isset($efect_when_hover)&& $efect_when_hover=='righttoleft'} selected="selected" {/if}>{l s='Right to Left' mod='tea_lookbook'}</option>
                        <option value="zoom" {if isset($efect_when_hover)&& $efect_when_hover=='zoom'} selected="selected" {/if}>{l s='Zoom' mod='tea_lookbook'}</option>
                    </select>
                </div>
                <div class="pg_form_control">
                    <label for="background">{l s='Background' mod='tea_lookbook'}</label>
                    <input data-hex="true" type="color" name="background" id="background" value="{if isset($background)}{$background}{/if}"/>
                </div>
                <div class="pg_form_control">
                    <label for="color">{l s='Color' mod='tea_lookbook'}</label>
                    <input data-hex="true" type="color" name="color" id="color" class="color mColorPickerInput" value="{if isset($color)}{$color}{/if}" />
                </div>
                <div class="pg_form_control">
                    <label for="width">{l s='Width' mod='tea_lookbook'}</label>
                    <input type="text" name="width" id="width" value="{if isset($width)&& $width!=0}{$width}{else}36{/if}" />
                    <span class="input-group-addon">px</span>
                </div>
                <div class="pg_form_control">
                    <label for="height">{l s='Height' mod='tea_lookbook'}</label>
                    <input type="text" name="height" id="height" value="{if isset($height) && $height!=0}{$height}{else}36{/if}" />
                    <span class="input-group-addon">px</span>
                </div>
                <div class="pg_form_control">
                    <label for="note_circle">{l s='Not a circle' mod='tea_lookbook'}</label>
                    <span class="switch prestashop-switch fixed-width-lg">
    					<input type="radio" name="note_circle"  id="note_circle_on" value="1" {if isset($note_circle) && $note_circle==1}checked="checked"{/if}/>
    					{strip}
    					<label for="note_circle_on">
    							{l s='Yes' mod='tea_lookbook'}
    					</label>
    					{/strip}
                        <input type="radio" name="note_circle"  id="note_circle_off" value="0" {if isset($note_circle) && $note_circle==0}checked="checked"{/if}/>
    					{strip}
    					<label for="note_circle_off">
    							{l s='No' mod='tea_lookbook'}
    					</label>
    					{/strip}
    					<a class="slide-button btn"></a>
    				</span>
                </div>
                <div class="pg_form_control content_type product">
                    <label>{l s='Show img' mod='tea_lookbook'}</label>
                    <span class="switch prestashop-switch fixed-width-lg">
    					<input type="radio" name="show_img"  id="show_img_on" value="1" {if isset($show_img) && $show_img==1}checked="checked"{/if}/>
    					{strip}
    					<label for="show_img_on">
    							{l s='Yes' mod='tea_lookbook'}
    					</label>
    					{/strip}
                        <input type="radio" name="show_img"  id="show_img_off" value="0" {if isset($show_img) && $show_img==0}checked="checked"{/if}/>
    					{strip}
    					<label for="show_img_off">
    							{l s='No' mod='tea_lookbook'}
    					</label>
    					{/strip}
    					<a class="slide-button btn"></a>
    				</span>
                </div>
                <div class="pg_form_control content_type product">
                    <label>{l s='Show name' mod='tea_lookbook'}</label>
                    <span class="switch prestashop-switch fixed-width-lg">
    					<input type="radio" name="show_name"  id="show_name_on" value="1" {if isset($show_name) && $show_name==1}checked="checked"{/if}/>
    					{strip}
    					<label for="show_name_on">
    							{l s='Yes' mod='tea_lookbook'}
    					</label>
    					{/strip}
                        <input type="radio" name="show_name"  id="show_name_off" value="0" {if isset($show_name) && $show_name==0}checked="checked"{/if}/>
    					{strip}
    					<label for="show_name_off">
    							{l s='No' mod='tea_lookbook'}
    					</label>
    					{/strip}
    					<a class="slide-button btn"></a>
    				</span>
                </div>
                <div class="pg_form_control content_type product">
                    <label>{l s='Show Price' mod='tea_lookbook'}</label>
                    <span class="switch prestashop-switch fixed-width-lg">
    					<input type="radio" name="show_price"  id="show_price_on" value="1" checked="checked" {if isset($show_price) && $show_price==1}checked="checked"{/if}/>
    					{strip}
    					<label for="show_price_on">
  							{l s='Yes' mod='tea_lookbook'}
    					</label>
    					{/strip}
                        <input type="radio" name="show_price"  id="show_price_off" value="0" {if isset($show_price) && $show_price==0}checked="checked"{/if}/>
    					{strip}
    					<label for="show_price_off">
  							{l s='No' mod='tea_lookbook'}
    					</label>
    					{/strip}
    					<a class="slide-button btn"></a>
    				</span>
                </div>
                <div class="pg_form_control content_type product">
                    <label>{l s='Show title' mod='tea_lookbook'}</label>
                    <span class="switch prestashop-switch fixed-width-lg">
    					<input type="radio" name="show_title"  id="show_title_on" value="1" checked="checked" {if isset($show_title) && $show_title==1}checked="checked"{/if}/>
    					{strip}
    					<label for="show_title_on">
  							{l s='Yes' mod='tea_lookbook'}
    					</label>
    					{/strip}
                        <input type="radio" name="show_title"  id="show_title_off" value="0" {if isset($show_title) && $show_title==0}checked="checked"{/if}/>
    					{strip}
    					<label for="show_title_off">
  							{l s='No' mod='tea_lookbook'}
    					</label>
    					{/strip}
    					<a class="slide-button btn"></a>
    				</span>
                </div>
                <div class="pg_form_control content_type product">
                    <label>{l s='Show Description'}</label>
                    <span class="switch prestashop-switch fixed-width-lg">
    					<input type="radio" name="show_description"  id="show_description_on" value="1" checked="checked" {if isset($show_description) && $show_description==1}checked="checked"{/if}/>
    					{strip}
    					<label for="show_description_on">
  							{l s='Yes' mod='tea_lookbook'}
    					</label>
    					{/strip}
                        <input type="radio" name="show_description"  id="show_description_off" value="0" {if isset($show_description) && $show_description==0}checked="checked"{/if}/>
    					{strip}
    					<label for="show_description_off">
  							{l s='No' mod='tea_lookbook'}
    					</label>
    					{/strip}
    					<a class="slide-button btn"></a>
    				</span>
                </div>
                <div class="pg_form_control">
                    <label for="position_product">{l s='Position content' mod='tea_lookbook'}</label>
                    <select name="position_product" id="position_product">
                        <option value="bottom" {if isset($position_product)&& $position_product=='bottom'} selected="selected" {/if}>{l s='Bottom' mod='tea_lookbook'}</option>
                        <option value="top" {if isset($position_product)&& $position_product=='top'} selected="selected" {/if}>{l s='Top' mod='tea_lookbook'}</option>
                        <option value="left" {if isset($position_product)&& $position_product=='left'} selected="selected" {/if}>{l s='Left' mod='tea_lookbook'}</option>
                        <option value="right" {if isset($position_product)&& $position_product=='right'} selected="selected" {/if}>{l s='Right' mod='tea_lookbook'}</option>
                    </select>
                </div>
                <div class="pg_form_control">
                    <button class="SubmitNoteLookBook" id="SubmitNoteLookBook" name="SubmitNoteLookBook">{l s='Save' mod='tea_lookbook'}</button>
                </div>
            </div>
        </div>
</form>
<script type="text/javascript">
SearchProduct($('#from-note-product'));
hideOtherLanguage({Configuration::get('PS_LANG_DEFAULT')});
displayFormNote();
</script>