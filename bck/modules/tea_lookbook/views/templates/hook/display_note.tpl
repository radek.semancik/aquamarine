{if $lookbook_product}
    <div class="note-type">
        {if $lookbook_product.type=="number" || $lookbook_product.type=="price"}
            {if $lookbook_product.type=="number"}
                <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="{if $lookbook_product.note_circle}note_circle{/if}">{if $lookbook_product.number}{$lookbook_product.number|intval}{else}{$lookbook_product.id_product_lookbook|intval}{/if}</span>
            {else}
                <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="{if $lookbook_product.note_circle}note_circle{/if}">{$lookbook_product.price}</span>
            {/if}
        {else}
           <span style="background:{$lookbook_product.background};color:{$lookbook_product.color};width:{if $lookbook_product.width!=0}{$lookbook_product.width}px;{else}36px;{/if}height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}line-height:{if $lookbook_product.height!=0}{$lookbook_product.height}px;{else}36px;{/if}" class="normal {if $lookbook_product.note_circle}note_circle{/if}"></span>
        {/if}
    </div>
    {if $lookbook_product.content_type=='html'}
        <div class="note-menu pg_animation {if $lookbook_product.position_product}pos_{$lookbook_product.position_product}{else}pos_bottom{/if}">
            <div class="product-info" style="text-align: center;">
                <div class="html_content">
                    {$lookbook_product.html_content}
                </div>
                <div class="product-action" style="text-align: center;">
                    <a class="submitdelete deletion" href="javascript:void(0);" data-id-lookbook="{$lookbook_product.id_product_lookbook|intval}">Remove</a>
                </div>
            </div>
        </div>
    {else}
        {if $lookbook_product.show_title}
            <div class="note-title pg_animation {if $lookbook_product.efect_when_hover}pg_{$lookbook_product.efect_when_hover}{else}pgfade{/if}">
                <div class="product-name">{$lookbook_product.name_product}</div>
            </div>
        {/if}
        <div class="note-menu pg_animation {if $lookbook_product.position_product}pos_{$lookbook_product.position_product}{else}pos_bottom{/if}">
            <div class="product-info" style="text-align: center;">
                {if $lookbook_product.show_img}
                    <div class="product-image"><img {if !$lookbook_product.id_product}style="display:none;"{/if} src="{$lookbook_product.link_img}"/></div>
                {/if}
                {if $lookbook_product.show_name}
                    <div class="product-name">{$lookbook_product.name_product}</div>
                {/if}
                {if $lookbook_product.show_price}
                    <div class="product-price">{$lookbook_product.price}</div>
                {/if}
                {if $lookbook_product.show_description}
                    <div class="product-description">{$lookbook_product.description_product|truncate:150:'...' nofilter}</div>
                {/if}
                <div class="product-action" style="text-align: center;">
                    <a class="submitdelete deletion" href="javascript:void(0);" data-id-lookbook="{$lookbook_product.id_product_lookbook|intval}">Remove</a>
                </div>
            </div>
        </div>
    {/if}
{/if}