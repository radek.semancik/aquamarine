$(document).ready(function(){
    ScaleSlider();
        InitLookbookSlider();
        InitMasonryLb();
        openLookbook();
    $(window).load(function(){
        ScaleSlider();
        //InitLookbookSlider();
        InitMasonryLb();
    });
    $(window).resize(function(){
        ScaleSlider();
        //InitLookbookSlider();
        InitMasonryLb();
        
    });
    $(document).on('click','.draggable_note .note-type,.draggable_note .product-image',function(e){
        var data_animation = $(this).closest('.draggable_note').attr('data-animation');
        if(!$(this).closest('.draggable_note').hasClass('active'))
        {
            $('.draggable_note').each(function(){
                if($(this).hasClass('active'))
                {
                    $(this).removeClass('active');
                    var animation= $(this).attr('data-animation');
                    $(this).find('.note-menu').removeClass('pg_'+animation); 
                }
            });
            $(this).closest('.draggable_note').addClass('active');
            $(this).closest('.draggable_note').find('.note-menu').addClass('pg_'+data_animation);
            changePositionNote();
        }
        else
        {
            $(this).closest('.draggable_note').removeClass('active');
            $(this).closest('.draggable_note').find('.note-menu').removeClass('pg_'+data_animation);
            $(this).closest('.draggable_note').find('.note-menu').css({'margin-left':'10px','margin-right':'10px'});
        }
    });
    $(document).on('click','.close_product_info',function(e){
        e.preventDefault();
        $(this).closest('.draggable_note').removeClass('active');
    });
});
function changePositionNote(){
        if ($(window).width() < 768){
            var posNoteLeft = $('.draggable_note.active .note-menu').offset().left;
            var widthNote = $('.draggable_note.active .note-menu').width();
            var posNoteLeftChange = posNoteLeft + widthNote;
            var changeNote = function(posNoteLeft,posNoteLeftChange){
                var scroll_note = $(window).scrollTop(); 
                if (posNoteLeft < 0) {
                    var magin_posNoteLeft = posNoteLeft*-1 + 10
                    $('.draggable_note.active .note-menu:not(.pos_left)').css('margin-left',magin_posNoteLeft+'px');
                    $('.draggable_note.active .note-menu.pos_left').css('margin-right','-'+magin_posNoteLeft+'px');
                } else {
                    if (posNoteLeftChange > $(window).width()){
                        var mRightNote = posNoteLeftChange - $(window).width() + 10;
                        $('.draggable_note.active .note-menu:not(.pos_left)').css('margin-left','-'+mRightNote+'px');
                        $('.draggable_note.active .note-menu.pos_left').css('margin-right',mRightNote+'px');
                    }
                    
                } 
            };
            changeNote(posNoteLeft,posNoteLeftChange);
        }
}
function InitMasonryLb(){
	$( '.grouplook_masonry_content' ).each( function() {
	   var el = $( '.grouplook_masonry_content' );
			//$( this ).imagesLoaded( function() {
                $(this).isotope( {
    				itemSelector: '.grid-item-masonry',
    				percentPosition: true,
                    layoutMode: 'masonry',
    				masonry: {
    					columnWidth: '.grid-sizer-lookbook'
    				}
    			} );
            //});
	});
}
function ScaleSlider(){
    $(".row_lookbook").each(function() {
        /*var ratio = $(this).width()/$(this).find('.panel-lookbook-wrap').width();
        var height = ratio*$(this).find('.panel-lookbook-wrap').height();*/
        var current_width = $(this).find('.panel-lookbook-wrap').attr('data-width');
        var current_height = $(this).find('.panel-lookbook-wrap').attr('data-height');
        
        $(this).find('.draggable_note').each(function() {
            var top_item = $(this).attr('data-pos-top');
            var top_item1 = (top_item/current_height)*100;
            var left_item = $(this).attr('data-pos-left');
            var left_item1 = (left_item/current_width)*100;
            
            $(this).css({"left":left_item1+"%","top":top_item1+"%"}).addClass('loaded');
        });
        
        
        /*if ( ratio <= 1 ){
            var buttonscale = 2 - ratio;
        } else {
            var buttonscale = ratio - 1;
        }
        if($(this).width() < $(this).find('.panel-lookbook-wrap').width())
            {            
                $(this).find('.panel-lookbook').addClass('loaded').css('height',height+'px');
                $(this).find('.panel-lookbook-wrap').css('transform', 'scale('+ratio+')');
                $(this).find('.draggable_note').css('transform', 'scale('+buttonscale+')');
            }
            else
            {
                $(this).find('.panel-lookbook-wrap').css('transform', '');
                $(this).find('.panel-lookbook').css('height','auto');
            }*/
    });
}
function BoolSlick(element){
    if ( element != 1){
            return false;
        } else {
            return  true;
        }
}
function InitLookbookSlider(){
    if ($(".pg_lookbook_item_loop_one .grouplook_slide").length > 0){
        $(".pg_lookbook_item_loop_one .grouplook_slide").each(function() {
            var _vertical = BoolSlick($(this).attr('data-vertical-slide')),
            _lk_rtl = BoolSlick($(this).attr('data-rtl')),
            _arrow = BoolSlick($(this).attr('data-show-next-preview')),
            _autoplay = BoolSlick($(this).attr('data-autoplay')),
            _loop = BoolSlick($(this).attr('data-loop')),
            _fade = BoolSlick($(this).attr('data-effect')),
            _desktop = $(this).attr('data-item-desktop'),
            _tablet = $(this).attr('data-item-tablet'),
            _dots = BoolSlick($(this).attr('data-dots')),
            _mobile = $(this).attr('data-item-mobile');
            $(this).slick({
        	  slidesToShow: 1,
        	  slidesToScroll: 1,
              autoplay: _autoplay,
        	  vertical: false,
              dots: _dots,
              infinite: _loop,
        	  arrows: _arrow,
              fade: true,
              adaptiveHeight: true,
              speed: 1000,
              autoplaySpeed: 5000,
              nextArrow: '<div class="next-slick-custom"><i class="pe-7s-angle-right"></i></div>',
              prevArrow: '<div class="prev-slick-custom"><i class="pe-7s-angle-left"></i></div>',
          	});
       	});
    }
    if ($(".pg_lookbook_item_loop_multi .grouplook_slide").length > 0){
        $(".pg_lookbook_item_loop_multi .grouplook_slide").each(function() {
            var _vertical = BoolSlick($(this).attr('data-vertical-slide')),
            _lk_rtl = BoolSlick($(this).attr('data-rtl')),
            _arrow = BoolSlick($(this).attr('data-show-next-preview')),
            _autoplay = BoolSlick($(this).attr('data-autoplay')),
            _loop = BoolSlick($(this).attr('data-loop')),
            _fade = BoolSlick($(this).attr('data-effect')),
            _desktop = $(this).attr('data-item-desktop'),
            _tablet = $(this).attr('data-item-tablet'),
            _dots = BoolSlick($(this).attr('data-dots')),
            _mobile = $(this).attr('data-item-mobile');

            $(this).slick({
        	  slidesToShow: parseInt(_desktop),
        	  slidesToScroll: 1,
              autoplay: _autoplay,
        	  vertical: false,
              dots: _dots,
              infinite: _loop,
        	  arrows: _arrow,
              fade: _fade,
              adaptiveHeight: true,
              rtl: _lk_rtl,
              speed: 1000,
              autoplaySpeed: 5000,
              nextArrow: '<div class="next-slick-custom"><i class="pe-7s-angle-right"></i></div>',
              prevArrow: '<div class="prev-slick-custom"><i class="pe-7s-angle-left"></i></div>',
              responsive:[
                        {
                            breakpoint: 992,
                            settings:{slidesToShow: parseInt(_desktop)}
                        },
                        {
                            breakpoint: 768,
                            settings:{slidesToShow: parseInt(_desktop)}
                        },
                        {
                            breakpoint: 480,
                            settings:{slidesToShow: parseInt(_desktop)}
                        }],
          	});
       	});
    }    
}

function openLookbook(){                        
   $( ".active-lookbook" ).hide();
   
   if($( "#pg_lb_category_custom" ).length > 0){
    $('.active-lookbook').show();
   }
   
   
   
   $( ".active-lookbook" ).click(function() {
    
    $( "#pg_lb_category_custom" ).toggle();
   });            
}