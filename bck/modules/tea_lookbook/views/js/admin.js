var draggable;
var ratio =1;
$(document).ready(function(){
    ScaleSlider();
    $(window).load(function(){
        ScaleSlider();
    });
    $(window).resize(function(){
        ScaleSlider();
    });
    $(document).on('click','#type_note',function(){
        if($('#type_note').val()=='number')
            $('.pg_form_control.display_number').show();
        else
            $('.pg_form_control.display_number').hide();
    });
    $(document).on('change','select[name="content_type"]',function(){
        displayFormNote();
    });
    $(document).on('click','.pg_mini_setting',function(){
        $(this).parent().next().toggleClass('mini_setting');
        $(this).parent().parent().toggleClass('mini_setting_form').parent().addClass('mini_setting_formwrap');
    });
    var $myProf = $("#grouplook_list");
	$myProf.sortable({
		opacity: 0.6,
		cursor: "move",
		update: function() {
			var order = $(this).sortable("serialize") + "&action=updateGroupLookOrdering";						
			$.post("", order);
		},
    	stop: function( event, ui ) {
  			$.growl.notice({ message: 'Update success'});
   		}
	});
	$myProf.hover(function() {
		$(this).css("cursor","move");
		},
		function() {
		$(this).css("cursor","auto");
	});
    var total_del=0;    
    $(document).on('click','.tea_close_popup',function(){
        $('#block-form').hide();
    });
    $(document).on('click', '#add_note_save', function(e){
        e.preventDefault();
        var data = new Array();
        jQuery('.draggable').each(function(){
            var tmp = new Object();
            tmp['id_product'] = jQuery(this).attr('data-id_product');
            tmp['top']  = jQuery(this).attr('data-top');
            tmp['left'] = jQuery(this).attr('data-left');
            if(parseInt(tmp['id_product']) > 0)
            {
                data.push(tmp);
            }else{
                jQuery('.note-menu').hide();
                jQuery( this ).draggable( "disable" );
                jQuery(this).remove();
            }
        });
        jQuery.ajax({
            url: "../modules/tea_lookbook/ajax_tea_lookbook.php",
            dataType: "json",
            type: 'POST',
            data: {
                data: JSON.stringify(data),
                action: 'save_note',
                id_lookbook: $('#id_lookbook').val()
            },
            success: function(data) {
                if(data.ok)
                    alert('Add product successful');
                else
                    alert('Have a error, please check again ');
            }
        });
    });
    $(document).on('click', '#add_note', function(e){
        e.preventDefault();
        $.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: '',
			async: true,
			cache: false,
			dataType : "json",
			data: 'ajax=1&action=addNoteLookBook&id_lookbook='+$(this).attr('data-lookbook'),
			success: function(jsonData)
			{
                var html = '<div id="draggable-'+jsonData.id_new+'" class="ui-widget-content draggable edit-note" data-product_lookbook="'+jsonData.id_new+'">';
                html += '<div class="note-type"><span style="width:36px;height:36px;" class="normal"></span></div>';
                html += '<div class="note-menu pg_animation pos_bottom">';
                html += '<div class="product-info" style="text-align: center;">';
                html += '<div class="product-image"><img src="" style="display:none;" /></div>';
                html += '<div class="product-name"></div>';
                html += '<div class="product-action"  style="text-align: center;"><a href="javascript:void(0);" class="submitdelete deletion" data-id-lookbook="'+jsonData.id_new+'">Remove</a></div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                $('.lookbook_addnoteform').append(html);
                $('.note-menu .product-name').text('');
                makeDrag(jsonData.id_new); 
			}
		});     
   });
   $(document).on('click','.edit-note',function(e){
        e.preventDefault();
        var data_animation = $(this).attr('data-animation');
        if(!$(this).hasClass('active'))
        {
            $('.edit-note').each(function(){
                if($(this).hasClass('active'))
                {
                    $(this).removeClass('active');
                    var animation= $(this).attr('data-animation');
                    $(this).find('.note-menu').removeClass('pg'+animation); 
                }
            });
            $(this).addClass('active');
            var id_product_lookbook=$(this).attr('data-product_lookbook');
            $(this).find('.note-menu').addClass('pg'+data_animation);
            $.ajax({
    			type: 'POST',
    			headers: { "cache-control": "no-cache" },
    			url: '',
    			async: true,
    			cache: false,
    			dataType : "json",
    			data: 'ajax=1&action=getFormNoteLookBook&id_product_lookbook='+id_product_lookbook,
    			success: function(jsonData)
    			{
                    $('#block-form-note').html(jsonData.form_note);
                    if($('#type_note').val()=='number')
                        $('.pg_form_control.display_number').show();
                    else
                        $('.pg_form_control.display_number').hide();
    			}
    		});
        }
        else
        {
            $('#block-form-note').html('');
            $(this).removeClass('active');
            $(this).find('.note-menu').removeClass('pg'+data_animation);
        }
   });
   $(document).on('click','.pg_lb_addnote_config_header h3',function(e){
        e.preventDefault();
        $('.edit-note').removeClass('active');
        $('#block-form-note').html('');
   });
   $(document).on('click','button[name="SubmitNoteLookBook"]',function(e){
        e.preventDefault();
        var id_product_lookbook = $('input[name="id_product_lookbook"]').val();
        var formData = new FormData($(this).parents('form').get(0));
        $.ajax({
            url: $(this).parents('form').eq(0).attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(json){
                if(json.error)
                {
                    $.growl.error({ message: json.errors });
                }                
                else
                {
                    $.growl.notice({ message: json.success });
                    $('#draggable-'+id_product_lookbook).html(json.product_lookbook);
                    $('#draggable-'+id_product_lookbook).attr('data-animation',$('#efect_when_hover').val());
                }
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });         
   });
   $('body').on('click','.spot',function(){
        var id = $(this).closest('.draggable').attr('id');
        var name = $(this).closest('.draggable').attr('data-product-name');
        var product_id = $(this).closest('.draggable').attr('data-product-id');
        var thumb = $(this).closest('.draggable').attr('data-product-thumb');
        if($(this).closest('.draggable').find('.note-menu').hasClass('show'))
        {
            $(this).closest('.draggable').find('.note-menu').hide();
            $('.note-menu').removeClass('show');
        }else{
            $('.note-menu').hide();
            $(this).closest('.draggable').find('.note-menu').show();
            $(this).closest('.draggable').find('.note-menu').addClass('show');
            $(this).closest('.draggable').find('.note-menu .product-name').text(name);
            if(thumb && thumb.length > 0)
            {
                $(this).closest('.draggable').find('.note-menu .product-image').html('<img src="'+thumb+'" width="100px">');
            }
        }
    });
    $('body').on('click','.product-action a',function(){
        var drag = $(this).closest('.draggable');
        var id_product_lookbook= $(this).attr('data-id-lookbook');
        $.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: window.location.url,
			async: true,
			cache: false,
			dataType : "json",
			data: 'ajax=1&action=deleteProductLookbook&id_product_lookbook='+id_product_lookbook,
			success: function(json)
			{
                if(json.error)
                {
                    $.growl.error({ message: json.errors });
                }                
                else
                {
                    drag.draggable( "disable" );
                    drag.remove();
                    total_del++;
                    $.growl.notice({ message: json.success });  
                }
			}
		});
        
    });
    $(document).on('click','.addnew_group,.edit_group_look,.addnew_lookbook,.edit_lookbook',function(e){
        e.preventDefault();
        $('.pg_lb_loading').addClass('show');
        $.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: $(this).attr('href'),
			async: true,
			cache: false,
			dataType : "json",
			data: 'ajax=1',
			success: function(jsonData)
			{
			     $('.pg_lb_loading').removeClass('show');
			     $('#block-form').show();
			     $('#block-form').html('<span class="tea_close_popup"></span>'+jsonData.block_form);
                 $('.panel-footer a.btn-default').removeAttr('onclick');
                 displayField();
                 $('input[name="product_name_lookbook"]').autocomplete('../modules/tea_lookbook/ajax_products_list.php',{
            		minChars: 1,
            		autoFill: true,
            		max:20,
            		matchContains: true,
            		mustMatch:true,
            		scroll:false,
            		cacheLength:0,
            		formatItem: function(item) {
            			return item[1]+' - '+item[0];
            		}
            	}).result(AddProductlookbook);
			}
		});
   });
   $(document).on('click','button[name="SubmitGroupLook"]',function(e){
        e.preventDefault();
        $('.pg_lb_loading').addClass('show');
        if($('.tea_lookbook_error').length)
            $('.tea_lookbook_error').remove();
        var formData = new FormData($(this).parents('form').get(0));
        $.ajax({
            url: $(this).parents('form').eq(0).attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(json){
                if(json.error)
                {
                    $('#module_form .form-wrapper').append('<div class="tea_lookbook_error">'+json.errors+'</div>');
                }                
                else
                {
                    $('#block-form').hide();
                    $('#list-grouplook').html(json.group_list)
                    $.growl.notice({ message: json.success });  
                }
                $('.pg_lb_loading').removeClass('show');
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });         
   });
   $(document).on('click','button[name="settinglookbook"]',function(e){
        e.preventDefault();
        tinyMCE.triggerSave();
        var formData = new FormData($(this).parents('form').get(0));
        $.ajax({
            url: $(this).parents('form').eq(0).attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(json){
                if(json.error)
                {
                    $.growl.error({ message: json.errors });
                }                
                else
                {
                    $('#block-form').hide();
                    $.growl.notice({ message: json.success });  
                }
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });
   });
   $(document).on('click','.setting_banner',function(e){
        e.preventDefault();
        $('.pg_lb_loading').addClass('show');
        $.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: $(this).attr('href'),
			async: true,
			cache: false,
			dataType : "json",
			data: 'ajax=1',
			success: function(jsonData)
			{
			     $('#block-form').show();
			     $('#block-form').html('<span class="tea_close_popup"></span>'+jsonData.block_form);
                 $('.panel-footer a.btn-default').removeAttr('onclick');
                 $('.pg_lb_loading').removeClass('show');
			}
		});
   });
   $(document).on('change','select#type',function(e){
        displayField();
   });
   $(document).on('click','button[name="submitlookbook"]',function(e){
        e.preventDefault();
        $('.pg_lb_loading').addClass('show');
        if($('.tea_lookbook_error').length)
            $('.tea_lookbook_error').remove();
        var formData = new FormData($(this).parents('form').get(0));
        $.ajax({
            url: $(this).parents('form').eq(0).attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(json){
                if(json.error)
                {
                    $('#module_form .form-wrapper').append('<div class="tea_lookbook_error">'+json.errors+'</div>');
                }                
                else
                {
                    $('#block-form').hide();
                    $('#lookbooks').html(json.lookbook_list)
                    $.growl.notice({ message: json.success });  
                }
                $('.pg_lb_loading').removeClass('show');
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });   
                $('.pg_lb_loading').removeClass('show');            
            }
        });         
   });
});
function SearchProduct(element)
{
    var input = element.find('.note-product');
    var ybc_featuredcat_ajax_url='../modules/tea_lookbook/ajax_products_list.php';
    $(input).autocomplete(ybc_featuredcat_ajax_url,{
		minChars: 1,
		autoFill: true,
		max:20,
		matchContains: true,
		mustMatch:true,
		scroll:false,
		cacheLength:0,
		formatItem: function(item) {
            draggable=element;
			return item[1]+' - '+item[0];
		}
	}).result(ybcAddAccessory);
}
var AddProductlookbook=function(event,data,formatted)
{
    if (data == null)
		return false;
	var productId = data[1];
	var productName = data[0];
    $('input[name="id_product"]').val(productId);
}
var ybcAddAccessory = function(event, data, formatted)
{
	if (data == null)
		return false;
	var productId = data[1];
	var productName = data[0];
    $('#id_product').val(productId);
    $('.product-search-info').html('<span class="product-image"><img src="'+data[2]+'"/></span><span class="product-name">'+productName+'</span>');
};
function ScaleSlider(){
    $(".row_lookbook_addnote").each(function() {
        ratio= $(this).width()/$(this).find('.lookbook_addnoteform').width();
        var height = ratio*$(this).find('.lookbook_addnoteform').height();
        if ( ratio <= 1 ){
            var buttonscale = 2 - ratio;
        } else {
            var buttonscale = ratio - 1;
        }
        if($(this).width() < $(this).find('.lookbook_addnoteform').width())
        {            
            $(this).find('.lookbook_addnote_content').css('height',height+'px');
            $(this).find('.lookbook_addnoteform').css('transform', 'scale('+ratio+')');
            $('.lookbook_addnoteform .draggable').css('transform', 'scale('+buttonscale+')');
        }
        else
        {
            $(this).find('.lookbook_addnoteform').css('transform', '');
            $(this).find('.lookbook_addnote_content').css('height','auto');
        }
    });
}
function makeDrag(id)
{
    var element = $('#draggable-'+id);
    var click = {
            x: 0,
            y: 0
        };
    element.draggable({
        cursor: "move",
        create: function( event, ui ) {
        },
        start: function( event, ui ) {
            click.x = event.clientX;
            click.y = event.clientY;
             ui.helper.css('right','auto');
            element.attr('data-left',ui.position.left).attr('data-top',ui.position.top);
        },
        stop: function( event, ui ) {
            $.ajax({
                url: '',
                data:'action=updatePositionNote&left='+ui.position.left+'&top='+ui.position.top+'&id_note='+id,
                type: 'post',
                dataType: 'json',
                success: function(json){
                    if(json.error)
                    {
                        $.growl.error({ message: json.errors });
                    }                
                    else
                    {
                        $.growl.notice({ message: json.success });  
                    }
                },
                error: function(xhr, status, error)
                {
                    var err = eval("(" + xhr.responseText + ")");     
                    $.growl.error({ message: err.Message });               
                }
            });
            element.attr('data-left',ui.position.left).attr('data-top',ui.position.top);
        },
        drag: function(event, ui) {
            var original = ui.originalPosition;
            ui.position = {
                left: (event.clientX - click.x + original.left) / ratio,
                top:  (event.clientY - click.y + original.top ) / ratio
            };   
        }
    });
}
function displayField(){
    var type =$('select#type').val();
    $('.form-group.custom').hide();
    $('.form-group.'+type).show();
    if($('select#position').val()!='lookbook_product_page'){
        $('.form-group.product_lookbook').hide();
        $('.form-group.id_product_lookbook').hide();    
    }    
    if($('select#position').val()!='lookbook_category_page'){
        $('.form-group.category_lookbook').hide();
        $('.form-group.id_category_lookbook').hide();
    }
           
}
function displayFormNote()
{
    $('.pg_form_control.content_type').hide();
    $('.pg_form_control.content_type.'+$('select[name="content_type"]').val()).show();
}