<?php
/*
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2015 PrestaShop SA
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

/**
 * @since   1.5.0
 */
if (!defined('_PS_VERSION_')) exit;

include_once(_PS_MODULE_DIR_.'tea_lookbook/LookBook.php');
include_once(_PS_MODULE_DIR_.'tea_lookbook/GroupLook.php');
include_once(_PS_MODULE_DIR_.'tea_lookbook/install/export.php');
include_once(_PS_MODULE_DIR_.'tea_lookbook/install/import.php');

class Tea_LookBook extends Module
{
    protected $_html         = '';
    protected $default_width = 779;
    protected $default_speed = 500;
    protected $default_pause = 3000;
    protected $default_loop  = 1;

    public function __construct()
    {
        $this->name          = 'tea_lookbook';
        $this->tab           = 'front_office_features';
        $this->version       = '10.1.0';
        $this->author        = 'prestagold.com';
        $this->need_instance = 0;
        $this->secure_key    = Tools::encrypt($this->name);
        $this->bootstrap     = true;
        parent::__construct();
        $this->displayName   = $this->l('Tea Lookbook');
        $this->description   = $this->l('Make lookbook page for your store.');
    }

    /**
     * @see Module::install()
     */
    public function install()
    {
        /* Adds Module */
        if (parent::install() &&
            $this->registerHook('displayHeader') && $this->registerHook('displayHome') &&
            $this->registerHook('actionShopDataDuplication') && $this->registerHook('displayBackOfficeHeader') && $this->registerHook('displayFooterProduct') && $this->registerHook('displayCustomProduct')
            && $this->registerHook('displayCustomCategory')
        ) {
            $res = $this->createTables();
            if ($res) {
                $import = new TeaImportLookbook();
                $import->importLookBooks();
            }
            $this->disableDevice(Context::DEVICE_MOBILE);
            $TEA_LOOKBOOK_DESCRIPTION = '<h1 class="page-title">Lookbook</h1><div id="page-caption">Fashion trends for this summer</div>';
            $image                    = '092ca32fae5b06b92ea183984159e1931f157ff5.jpg';

            $languages = Language::getLanguages(false);
            $valules   = array();
            foreach ($languages as $lang) {
                $valules[$lang['id_lang']] = $TEA_LOOKBOOK_DESCRIPTION;
            }
            Configuration::updateValue('TEA_LOOKBOOK_DESCRIPTION', $valules, true);
            Configuration::updateValue('TEA_LOOKBOOK_BANNER', $image, true);
            Configuration::updateValue('TEA_LOOKBOOK_LIBRARY_MASONRY', 1);
            Configuration::updateValue('TEA_LOOKBOOK_LIBRARY_SLICK_SLIDER', 1);
            Configuration::updateValue('TEA_LOOKBOOK_CSS3_EFFECT', 1);
            return (bool) $res;
        }

        return false;
    }

    /**
     * @see Module::uninstall()
     */
    public function uninstall()
    {
        /* Deletes Module */
        if (parent::uninstall()) {
            /* Deletes tables */
            $res = $this->deleteTables();

            return (bool) $res;
        }

        return false;
    }

    /**
     * Creates tables
     */
    protected function createTables()
    {
        $res = (bool) Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'lookbook` (
				`id_tea_lookbook` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_shop` int(10) unsigned NOT NULL,
				PRIMARY KEY (`id_tea_lookbook`, `id_shop`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');
        $res &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tea_lookbook` (
			  `id_tea_lookbook` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `id_grouplook` int(10) unsigned NOT NULL DEFAULT \'0\',
			  `position` int(10) unsigned NOT NULL DEFAULT \'0\',
			  `active` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
              `width` int(12) NOT NULL,
              `height` int(12) NOT NULL,
              `custom_class` varchar(222) NOT NULL,
			  PRIMARY KEY (`id_tea_lookbook`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');
        $res &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tea_lookbook_lang` (
			  `id_tea_lookbook` int(10) unsigned NOT NULL,
			  `id_lang` int(10) unsigned NOT NULL,
			  `title` varchar(255) NOT NULL,
			  `image` varchar(255) NOT NULL,
              `link` varchar(255) NOT NULL,
              `description` text NOT NULL,
			  PRIMARY KEY (`id_tea_lookbook`,`id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');
        $res &= Db::getInstance()->execute("CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."product_lookbook` (
          `id_product_lookbook` int(12) NOT NULL AUTO_INCREMENT,
          `content_type` VARCHAR(22),
          `id_product` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
          `link` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
          `id_tea_lookbook` int(12) NOT NULL,
          `left` varchar(12) NOT NULL,
          `top` varchar(12) NOT NULL,
          `type` varchar(22) NOT NULL,
          `efect_when_hover` varchar(22) NOT NULL,
          `background` varchar(22) NOT NULL,
          `color` varchar(22) NOT NULL,
          `note_circle` int(1) DEFAULT '0',
          `number` INT(11) ,
          `width` FLOAT(10,2),
          `height` FLOAT(10,2),
          `show_img` INT(1) DEFAULT '0',
          `show_name` INT (1) DEFAULT '0',
          `show_price` INT (1) DEFAULT '0',
          `position_product` varchar(22) NOT NULL,
          `show_description` INT(1) DEFAULT '0',
          `show_title` INT(1) DEFAULT '0',
          PRIMARY KEY (`id_product_lookbook`))");
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'product_lookbook_lang` (
            `id_product_lookbook` int(12) NOT NULL,
            `id_lang` INT(11) NOT NULL,
            `html_content` text,
            PRIMARY KEY (`id_product_lookbook`,`id_lang`))
        ');
        $res &= Db::getInstance()->execute("CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."tea_grouplook`(
        `id_grouplook` INT(11) NOT NULL AUTO_INCREMENT,
        `position` VARCHAR(222) NOT NULL , 
        `type` VARCHAR(22) NOT NULL , 
        `id_product` INT(11) NOT NULL,
        `id_category` INT(11) NOT NULL,
        `hook_product` VARCHAR(222) NOT NULL,
        `hook_category` VARCHAR(222) NOT NULL,
        `item_in_row` INT(2) NOT NULL ,
        `item_in_row_mobile` INT(2) NOT NULL ,
        `item_in_row_tablet` INT(2) NOT NULL ,
        `masonry_column` INT(2) NOT NULL ,
        `masonry_column_tablet` INT(2) NOT NULL ,  
        `masonry_column_mobile` INT(2) NOT NULL,
        `custom_class` VARCHAR(222) NOT NULL,
        `animation`  VARCHAR(222) NOT NULL ,
        `effect`  VARCHAR(222) NOT NULL ,
        `infinite` INT(2) NOT NULL ,
        `autoplay` INT(2) NOT NULL ,
        `show_next_preview` INT(2) NOT NULL ,
        `pavination` INT(2) NOT NULL,
        `order_by` INT(11) NOT NUll,
        `full_width` INT(2) NOT NULL, 
        `background` VARCHAR(222) NOT NULL , 
        `margin` VARCHAR(222) NOT NULL , 
        `padding` VARCHAR(222) NOT NULL , 
        `padding_item` VARCHAR(222) NOT NULL ,
        `active` INT(2) NOT NULL , 
        PRIMARY KEY (`id_grouplook`)) ENGINE = InnoDB;");
        $res &= Db::getInstance()->execute("CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."tea_grouplook_lang` (
        `id_grouplook` INT(11) NOT NULL , 
        `id_lang` INT(11) NOT NULL , 
        `title` VARCHAR(222) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL ,
        `description` text NOT NULL,
        PRIMARY KEY (`id_grouplook`, `id_lang`)) ENGINE = InnoDB;");
        $res &= Db::getInstance()->execute("CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."tea_grouplook_shop` (
        `id_grouplook` INT(11) NOT NULL , 
        `id_shop` INT(11) NOT NULL , 
        PRIMARY KEY (`id_grouplook`, `id_shop`)) ENGINE = InnoDB;");
        return $res;
    }

    /**
     * deletes tables
     */
    protected function deleteTables()
    {
        $lookbooks = $this->getLookBooks();
        foreach ($lookbooks as $lookbook) {
            $to_del = new LookBook($lookbook['id_lookbook']);
            $to_del->delete();
        }
        return Db::getInstance()->execute('
			DROP TABLE IF EXISTS `'._DB_PREFIX_.'lookbook`, `'._DB_PREFIX_.'tea_lookbook`, `'._DB_PREFIX_.'tea_lookbook_lang`,`'._DB_PREFIX_.'product_lookbook`,`'._DB_PREFIX_.'tea_grouplook`,`'._DB_PREFIX_.'tea_grouplook_lang`,`'._DB_PREFIX_.'tea_grouplook_shop`;
		');
    }

    public function getContent()
    {
        $this->context->smarty->assign(array(
            'link' => $this->context->link,
        ));
        if (Tools::getValue('export_data')) {
            $export = new TeaExportLookBook();
            $export->exportLookBooks();
        }
        if (Tools::getValue('action') == 'updateGroupLookOrdering') {
            if (Tools::getValue('grouplook')) {
                foreach (Tools::getValue('grouplook') as $key => $value) {
                    Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'tea_grouplook SET order_by="'.$key.'" where id_grouplook='.(int) $value);
                }
            }
            die(1);
        }
        if (Tools::getValue('action') == 'addNoteLookBook' && $id_lookbook = Tools::getValue('id_lookbook')) {
            Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'product_lookbook (`id_product_lookbook`,`id_tea_lookbook`,`type`,show_img,show_title,show_price,show_name,efect_when_hover) values("","'.(int) $id_lookbook.'","normal",1,0,1,1,"fadeinup")');
            die(Tools::jsonEncode(
                    array(
                        'id_new' => Db::getInstance()->Insert_ID(),
                    )
            ));
        }
        if (Tools::getValue('action') == 'getFormNoteLookBook' && $id_product_lookbook = (int) Tools::getValue('id_product_lookbook')) {
            die(Tools::jsonEncode(
                    array(
                        'form_note' => $this->renderFormNote(),
                    )
            ));
        }
        if (Tools::getValue('action') == 'updatePositionNote') {
            $left    = Tools::getValue('left');
            $top     = Tools::getValue('top');
            $id_note = Tools::getValue('id_note');
            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product_lookbook SET `left`="'.$left.'",`top`="'.$top.'" WHERE id_product_lookbook='.(int) $id_note);
            die(
                Tools::jsonEncode(array(
                    'error' => false,
                    'success' => $this->l('Update position successfully'),
                ))
            );
        }
        if (Tools::getValue('action') == 'deleteProductLookbook' && $id_note = (int) Tools::getValue('id_product_lookbook')) {
            Db::getInstance()->execute('DELETE FROM  '._DB_PREFIX_.'product_lookbook WHERE id_product_lookbook='.(int) $id_note);
            die(
                Tools::jsonEncode(array(
                    'error' => false,
                    'success' => $this->l('Delete successfully'),
                ))
            );
        }
        if (Tools::isSubmit('SubmitGroupLook') || Tools::isSubmit('submitlookbook') || Tools::isSubmit('delete_id_lookbook') || Tools::isSubmit('submitlookbooks')
            || Tools::isSubmit('settinglookbook') || Tools::isSubmit('SubmitNoteLookBook') || Tools::isSubmit('changeLookBookStatus') || Tools::getValue('deletegrouplook')) {
            if ($this->_postValidation()) {
                $this->_postProcess();
            }
        } elseif (Tools::isSubmit('add_note') && Tools::getValue('id_lookbook')) {
            if (Tools::getValue('id_lookbook')) {
                if (!Validate::isInt(Tools::getValue('id_lookbook')) && !$this->lookbookExists(Tools::getValue('id_lookbook'))) {
                    $error = $this->l('Invalid look book ID');
                    $this->context->smarty->assign('error', $error);
                } else {
                    $lookbook          = new LookBook((int) Tools::getValue('id_lookbook'), $this->context->language->id);
                    $lookbook_products = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."product_lookbook where id_tea_lookbook =".(int) Tools::getValue('id_lookbook').' order by id_product_lookbook');
                    if ($lookbook_products)
                            foreach ($lookbook_products as &$lookbook_product) {
                            if ($lookbook_product['content_type'] == 'html') {
                                $lookbook_product['html_content'] = Db::getInstance()->getValue('SELECT html_content FROM '._DB_PREFIX_.'product_lookbook_lang WHERE id_product_lookbook="'.(int) $lookbook_product['id_product_lookbook'].'" AND id_lang="'.(int) $this->context->language->id.'"');
                            } else {
                                $product                                 = new Product($lookbook_product['id_product'], true, $this->context->language->id);
                                $id_image                                = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int) $lookbook_product['id_product'].' AND cover=1');
                                $url_image                               = Context::getContext()->link->getImageLink($product->link_rewrite, $id_image,
                                    'home_default');
                                $lookbook_product['name_product']        = $product->name;
                                $lookbook_product['link_img']            = $url_image;
                                $price                                   = $product->getPrice(true, null);
                                $lookbook_product['price']               = Tools::displayPrice($price);
                                $lookbook_product['description_product'] = $product->description_short ? $product->description_short : $product->description;
                            }
                        }
                    $this->context->smarty->assign(
                        array(
                            'image_baseurl' => $this->_path.'views/img/images/',
                            'lookbook' => $lookbook,
                            'lookbook_products' => $lookbook_products
                    ));
                }
            }
            return $this->display(__FILE__, 'add_note.tpl');
        } elseif ((Tools::isSubmit('addlookbook') && Tools::getValue('id_grouplook') ) || (Tools::isSubmit('id_lookbook') && $this->lookbookExists((int) Tools::getValue('id_lookbook')))) {
            if (Tools::isSubmit('addlookbook')) $mode = 'add';
            else $mode = 'edit';
            if (Tools::isSubmit('ajax')) {
                die(Tools::jsonEncode(
                        array(
                            'errors' => false,
                            'block_form' => $this->renderAddForm(),
                        )
                ));
            }
            $this->_html .= $this->renderAddForm();
        } elseif (Tools::isSubmit('addgrouplook') || Tools::isSubmit('editgrouplook')) {
            if (Tools::isSubmit('ajax')) {
                die(Tools::jsonEncode(
                        array(
                            'errors' => false,
                            'block_form' => '<script src="'._PS_JS_DIR_.'tiny_mce/tiny_mce.js"></script><script src="'._PS_JS_DIR_.'admin/tinymce.inc.js"></script>'.'<script src="'.__PS_BASE_URI__.'modules/'.$this->name.'/views/js/jquery.colorpicker.js"></script>'.$this->renderFromGroup(),
                        )
                ));
            } else $this->_html .= $this->renderFromGroup();
        }
        elseif (Tools::isSubmit('viewgrouplook') && (int) Tools::getValue('id_grouplook')) {
            $this->_html .= $this->headerHTML();
            $this->_html .= $this->renderList((int) Tools::getValue('id_grouplook'));
        } elseif (Tools::getValue('layout') == 'config_look') {
            if (Tools::isSubmit('ajax')) {
                $this->context->controller->addJS(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');
                $this->context->controller->addJS(_PS_JS_DIR_.'admin/tinymce.inc.js');
                die(Tools::jsonEncode(
                        array(
                            'errors' => false,
                            'block_form' => '<script src="'._PS_JS_DIR_.'tiny_mce/tiny_mce.js"></script><script src="'._PS_JS_DIR_.'admin/tinymce.inc.js"></script>'.$this->renderForm(),
                        )
                ));
            }
            $this->_html .= $this->renderForm();
        } else $this->_html .= $this->renderMain();

        return $this->_html;
    }

    protected function _postValidation()
    {
        $errors          = array();
        $id_lang_default = (int) Configuration::get('PS_LANG_DEFAULT');
        if (Tools::isSubmit('submitlookbook')) {
            if (!Validate::isInt(Tools::getValue('active_lookbook')) || (Tools::getValue('active_lookbook') != 0 && Tools::getValue('active_lookbook') != 1))
                    $errors[] = $this->l('Invalid look book state.');

            if (!Validate::isInt(Tools::getValue('position')) || (Tools::getValue('position') < 0)) $errors[] = $this->l('Invalid look book position.');
            if (Tools::isSubmit('id_lookbook')) {
                if (!Validate::isInt(Tools::getValue('id_lookbook')) && !$this->lookbookExists(Tools::getValue('id_lookbook')))
                        $errors[] = $this->l('Invalid look book ID');
            }
            $languages = Language::getLanguages(false);
            foreach ($languages as $language) {
                if (Tools::strlen(Tools::getValue('title_'.$language['id_lang'])) > 255) $errors[] = $this->l('The title is too long.');
                if (Tools::getValue('image_'.$language['id_lang']) != null && !Validate::isFileName(Tools::getValue('image_'.$language['id_lang'])))
                        $errors[] = $this->l('Invalid filename.');
                if (Tools::getValue('image_old_'.$language['id_lang']) != null && !Validate::isFileName(Tools::getValue('image_old_'.$language['id_lang'])))
                        $errors[] = $this->l('Invalid filename.');
            }
            if (Tools::strlen(Tools::getValue('title_'.$id_lang_default)) == 0) $errors[] = $this->l('The title is not set.');
            if (!Tools::isSubmit('has_picture') && (!isset($_FILES['image_'.$id_lang_default]) || empty($_FILES['image_'.$id_lang_default]['tmp_name'])))
                    $errors[] = $this->l('The image is not set.');
            if (Tools::getValue('image_old_'.$id_lang_default) && !Validate::isFileName(Tools::getValue('image_old_'.$id_lang_default)))
                    $errors[] = $this->l('The image is not set.');
            if (count($errors)) {
                die(
                    Tools::jsonEncode(
                        array(
                            'error' => true,
                            'errors' => $this->displayError(implode('<br />', $errors)),
                        )
                    )
                );
            }
        } elseif (Tools::isSubmit('delete_id_lookbook') && (!Validate::isInt(Tools::getValue('delete_id_lookbook')) || !$this->lookbookExists((int) Tools::getValue('delete_id_lookbook'))))
                $errors[] = $this->l('Invalid lookbook ID');
        if (Tools::isSubmit('SubmitGroupLook')) {
            if (Tools::getValue('title_'.$id_lang_default) == '') $errors[] = $this->l('Title is requie');
            
            $valProduct = Tools::getValue('hook_product');
            if (Tools::getValue('position') == 'lookbook_product_page' && empty($valProduct) && !(int) Tools::getValue('hook_product')) $errors[] = $this->l('Product is requie');

            $valCategory = Tools::getValue('hook_category');
            if (Tools::getValue('position') == 'lookbook_category_page' && empty($valCategory) && !(int) Tools::getValue('hook_category')) $errors[] = $this->l('Hook Category is requie');
            if (count($errors)) {
                die(
                    Tools::jsonEncode(
                        array(
                            'error' => true,
                            'errors' => $this->displayError(implode('<br />', $errors)),
                        )
                    )
                );
            }
        }
        if (count($errors)) {
            $this->_html .= $this->displayError(implode('<br />', $errors));
            return false;
        }
        return true;
    }

    protected function _postProcess()
    {
        $errors = array();
        if (Tools::isSubmit('SubmitNoteLookBook')) {
            $id_product_lookbook = (int) Tools::getValue('id_product_lookbook');
            $sql                 = 'UPDATE '._DB_PREFIX_.'product_lookbook
                    SET `type` ="'.pSQL(Tools::getValue('type_note')).'",
                        `efect_when_hover` = "'.pSQL(Tools::getValue('efect_when_hover')).'",
                        `background` ="'.pSQL(Tools::getValue('background')).'",
                        `color` ="'.pSQL(Tools::getValue('color')).'",
                        `number` ="'.(int) Tools::getValue('number').'",
                        `note_circle`="'.(int) Tools::getValue('note_circle').'",
                        `show_img` ="'.(int) Tools::getValue('show_img').'",
                        `show_name` ="'.(int) Tools::getValue('show_name').'",
                        `id_product` ="'.(int) Tools::getValue('id_product').'",
                        `id_category` ="'.(int) Tools::getValue('id_category').'",
                        `link` ="'.Tools::getValue('link').'",
                        `show_price` ="'.(int) Tools::getValue('show_price').'",
                        `position_product` ="'.Tools::getValue('position_product').'",
                        `show_title` ="'.(int) Tools::getValue('show_title').'",
                        `width` ="'.(float) Tools::getValue('width').'",
                        `height` ="'.(float) Tools::getValue('height').'",
                        `content_type` ="'.pSQL(Tools::getValue('content_type')).'",
                        `show_description` = "'.(int) Tools::getValue('show_description').'"
                    WHERE id_product_lookbook="'.(int) $id_product_lookbook.'"
            ';
            $languages           = Language::getLanguages(false);
            if ($languages) {
                foreach ($languages as $language) {
                    if (!Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'product_lookbook_lang WHERE id_product_lookbook="'.(int) $id_product_lookbook.'" AND id_lang="'.(int) $language['id_lang'].'"')) {
                        Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'product_lookbook_lang (id_product_lookbook,id_lang,html_content) VALUES ("'.(int) $id_product_lookbook.'","'.(int) $language['id_lang'].'","'.pSQL(trim(Tools::getValue('html_content_'.$language['id_lang']))).'")');
                    } else {
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product_lookbook_lang SET html_content ="'.pSQL(trim(Tools::getValue('html_content_'.$language['id_lang']))).'" WHERE id_product_lookbook="'.(int) $id_product_lookbook.'" AND id_lang="'.(int) $language['id_lang'].'"');
                    }
                }
            }
            if (Db::getInstance()->execute($sql)) {
                die(
                    Tools::jsonEncode(array(
                        'error' => false,
                        'success' => $this->l('Update successfully'),
                        'product_lookbook' => $this->displayNote($id_product_lookbook),
                    ))
                );
            }
        }
        if (Tools::isSubmit('SubmitGroupLook')) {
            if (Tools::getValue('id_grouplook')) {
                $groupLook = new GroupLook(Tools::getValue('id_grouplook'));
            } else {
                $groupLook           = new GroupLook();
                $groupLook->order_by = $this->getMaxPositionGroupLook() + 1;
            }
            $languages = Language::getLanguages(false);
            if ($languages) {
                foreach ($languages as $language) {
                    $groupLook->title[$language['id_lang']]       = Tools::getValue('title_'.$language['id_lang']);
                    $groupLook->description[$language['id_lang']] = Tools::getValue('description_'.$language['id_lang']);
                }
            }
            $groupLook->active                = (int) Tools::getValue('active');
            $groupLook->position              = Tools::getValue('position');
            $groupLook->type                  = Tools::getValue('type');
            $groupLook->id_product            = (int) Tools::getValue('id_product');
            $groupLook->id_category           = (int) Tools::getValue('id_category');
            $groupLook->item_in_row           = (int) Tools::getValue('item_in_row');
            $groupLook->item_in_row_mobile    = (int) Tools::getValue('item_in_row_mobile');
            $groupLook->item_in_row_tablet    = (int) Tools::getValue('item_in_row_tablet');
            $groupLook->masonry_column        = (int) Tools::getValue('masonry_column');
            $groupLook->masonry_column_mobile = (int) Tools::getValue('masonry_column_mobile');
            $groupLook->masonry_column_tablet = (int) Tools::getValue('masonry_column_tablet');
            $groupLook->animation             = Tools::getValue('animation');
            $groupLook->effect                = Tools::getValue('effect');
            $groupLook->infinite              = Tools::getValue('infinite');
            $groupLook->autoplay              = Tools::getValue('autoplay');
            $groupLook->show_next_preview     = Tools::getValue('show_next_preview');
            $groupLook->pavination            = Tools::getValue('pavination');
            $groupLook->full_width            = Tools::getValue('full_width');
            $groupLook->background            = Tools::getValue('background');
            $groupLook->margin                = Tools::getValue('margin');
            $groupLook->padding               = Tools::getValue('padding');
            $groupLook->padding_item          = Tools::getValue('padding_item');
            $groupLook->custom_class          = Tools::getValue('custom_class');
            $groupLook->hook_product          = Tools::getValue('hook_product');
            $groupLook->hook_category         = Tools::getValue('hook_category');
            if (Tools::getValue('id_grouplook')) {
                if (!$groupLook->update()) {
                    if (Tools::getValue('ajax')) {
                        die(Tools::jsonEncode(
                                array(
                                    'error' => true,
                                    'errors' => $this->l('The group look could not be updated.'),
                                )
                        ));
                    } else $errors[] = $this->displayError($this->l('The group look could not be updated.'));
                }
                elseif (Tools::getValue('ajax')) {
                    die(
                        Tools::jsonEncode(array(
                            'error' => false,
                            'success' => $this->l('Update successfully'),
                            'group_list' => $this->renderMain(),
                        ))
                    );
                }
            } else {
                if (!$groupLook->add()) {
                    if (Tools::getValue('ajax')) {
                        die(Tools::jsonEncode(
                                array(
                                    'error' => true,
                                    'errors' => $this->l('The group look could not be added'),
                                )
                        ));
                    } else $errors[] = $this->displayError($this->l('The group look could not be added'));
                }
                elseif (Tools::getValue('ajax')) {
                    die(
                        Tools::jsonEncode(array(
                            'error' => false,
                            'success' => $this->l('add successfully'),
                            'group_list' => $this->renderMain(),
                        ))
                    );
                }
            }
        }
        if (Tools::isSubmit('submitlookbook')) {
            if (Tools::getValue('id_lookbook')) {
                $lookbook = new LookBook((int) Tools::getValue('id_lookbook'));
                if (!Validate::isLoadedObject($lookbook)) {
                    if (Tools::getValue('ajax')) {
                        die(Tools::jsonEncode(
                                array(
                                    'error' => true,
                                    'errors' => $this->l('Invalid lookbook ID'),
                                )
                        ));
                    } else $this->_html .= $this->displayError($this->l('Invalid lookbook ID'));
                    return false;
                }
            } else $lookbook               = new LookBook();
            $lookbook->position     = (int) Tools::getValue('position');
            $lookbook->active       = (int) Tools::getValue('active_slide');
            $lookbook->width        = (int) Tools::getValue('width');
            $lookbook->height       = (int) Tools::getValue('height');
            $lookbook->custom_class = Tools::getValue('custom_class');
            $lookbook->id_grouplook = (int) Tools::getValue('id_grouplook');
            $languages              = Language::getLanguages(false);
            foreach ($languages as $language) {
                $lookbook->title[$language['id_lang']]       = Tools::getValue('title_'.$language['id_lang']);
                $lookbook->link[$language['id_lang']]        = Tools::getValue('link_'.$language['id_lang']);
                $lookbook->description[$language['id_lang']] = Tools::getValue('description_'.$language['id_lang']);
                $type                                        = Tools::strtolower(Tools::substr(strrchr($_FILES['image_'.$language['id_lang']]['name'], '.'), 1));
                $imagesize                                   = @getimagesize($_FILES['image_'.$language['id_lang']]['tmp_name']);
                if (isset($_FILES['image_'.$language['id_lang']]) &&
                    isset($_FILES['image_'.$language['id_lang']]['tmp_name']) &&
                    !empty($_FILES['image_'.$language['id_lang']]['tmp_name']) &&
                    !empty($imagesize) &&
                    in_array(
                        Tools::strtolower(Tools::substr(strrchr($imagesize['mime'], '/'), 1)),
                        array(
                        'jpg',
                        'gif',
                        'jpeg',
                        'png'
                        )
                    ) &&
                    in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
                ) {
                    $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                    $salt      = sha1(microtime());
                    if ($error     = ImageManager::validateUpload($_FILES['image_'.$language['id_lang']])) $errors[]  = $error;
                    elseif (!$temp_name || !move_uploaded_file($_FILES['image_'.$language['id_lang']]['tmp_name'], $temp_name)) return false;
                    elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/views/img/images/'.$salt.'_'.$_FILES['image_'.$language['id_lang']]['name'],
                            null, null, $type)) {
                        $errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
                    }
                    if (isset($temp_name)) @unlink($temp_name);
                    $lookbook->image[$language['id_lang']] = $salt.'_'.$_FILES['image_'.$language['id_lang']]['name'];
                }
                elseif (Tools::getValue('image_old_'.$language['id_lang']) != '')
                        $lookbook->image[$language['id_lang']] = Tools::getValue('image_old_'.$language['id_lang']);
            }
            if (!$errors) {
                if (!Tools::getValue('id_lookbook')) {
                    if (!$lookbook->add()) {
                        if (Tools::getValue('ajax')) {
                            die(Tools::jsonEncode(
                                    array(
                                        'error' => true,
                                        'errors' => $this->l('The look book could not be added.'),
                                    )
                            ));
                        } else $errors[] = $this->displayError($this->l('The look book could not be added.'));
                    }
                    elseif (Tools::getValue('ajax')) {
                        die(
                            Tools::jsonEncode(array(
                                'error' => false,
                                'success' => $this->l('add successfully'),
                                'lookbook_list' => $this->renderList($lookbook->id_grouplook),
                            ))
                        );
                    }
                } elseif (!$lookbook->update()) {
                    if (Tools::getValue('ajax')) {
                        die(Tools::jsonEncode(
                                array(
                                    'error' => true,
                                    'errors' => $this->l('The look book could not be updated.'),
                                )
                        ));
                    } else $errors[] = $this->displayError($this->l('The look book could not be updated.'));
                }
                elseif (Tools::getValue('ajax')) {
                    die(
                        Tools::jsonEncode(array(
                            'error' => false,
                            'success' => $this->l('add successfully'),
                            'lookbook_list' => $this->renderList($lookbook->id_grouplook),
                        ))
                    );
                }
                $this->clearCache();
            }
        } elseif (Tools::isSubmit('changeLookBookStatus') && $id_lookbook = Tools::getValue('id_lookbook')) {
            $lookbook         = new LookBook((int) $id_lookbook);
            $id_grouplook     = $lookbook->id_grouplook;
            $lookbook->active = !(int) $lookbook->active;
            $lookbook->update();
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=5&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&viewgrouplook=1&id_grouplook='.(int) $id_grouplook);
        } elseif (Tools::isSubmit('delete_id_lookbook')) {
            $lookbook     = new LookBook((int) Tools::getValue('delete_id_lookbook'));
            $id_grouplook = $lookbook->id_grouplook;
            $res          = $lookbook->delete();
            $this->clearCache();
            if (!$res) $this->_html  .= $this->displayError('Could not delete.');
            else
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=1&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&viewgrouplook=1&id_grouplook='.(int) $id_grouplook);
        }
        elseif (Tools::isSubmit('deletegrouplook') && Tools::getValue('id_grouplook')) {
            $groupLook = new GroupLook((int) Tools::getValue('id_grouplook'));
            $lookbooks = $this->getLookBooks(false, Tools::getValue('id_grouplook'));
            if ($lookbooks)
                    foreach ($lookbooks as $lookbook) {
                    $class_look = new LookBook($lookbook['id_tea_lookbook']);
                    $class_look->delete();
                }
            $position    = $groupLook->position;
            $res         = $groupLook->delete();
            if (!$res) $this->_html .= $this->displayError('Could not delete.');
            else
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=1&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&layout='.(int) $position);
        }
        elseif (Tools::isSubmit('settinglookbook')) {
            Configuration::updateValue('TEA_LOOKBOOK_LIBRARY_MASONRY', (int) Tools::getValue('TEA_LOOKBOOK_LIBRARY_MASONRY'));
            Configuration::updateValue('TEA_LOOKBOOK_LIBRARY_SLICK_SLIDER', (int) Tools::getValue('TEA_LOOKBOOK_LIBRARY_SLICK_SLIDER'));
            Configuration::updateValue('TEA_LOOKBOOK_CSS3_EFFECT', (int) Tools::getValue('TEA_LOOKBOOK_CSS3_EFFECT'));
            if (isset($_FILES['TEA_LOOKBOOK_BANNER']['tmp_name']) && isset($_FILES['TEA_LOOKBOOK_BANNER']['name']) && $_FILES['TEA_LOOKBOOK_BANNER']['name']) {
                $salt      = sha1(microtime());
                $type      = Tools::strtolower(Tools::substr(strrchr($_FILES['TEA_LOOKBOOK_BANNER']['name'], '.'), 1));
                $imageName = $salt.'.'.$type;
                $fileName  = dirname(__FILE__).'/views/img/images/config/'.$imageName;
                if (file_exists($fileName)) {
                    $errors[] = $this->l(' already exists. Try to rename the file then reupload');
                } else {
                    $imagesize = @getimagesize($_FILES['TEA_LOOKBOOK_BANNER']['tmp_name']);

                    if (!$errors && isset($_FILES['TEA_LOOKBOOK_BANNER']) &&
                        !empty($_FILES['TEA_LOOKBOOK_BANNER']['tmp_name']) &&
                        !empty($imagesize) &&
                        in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
                    ) {
                        $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                        if ($error     = ImageManager::validateUpload($_FILES['TEA_LOOKBOOK_BANNER'])) $errors[]  = $error;
                        elseif (!$temp_name || !move_uploaded_file($_FILES['TEA_LOOKBOOK_BANNER']['tmp_name'], $temp_name))
                                $errors[]  = $this->l('Can not upload the file');
                        elseif (!ImageManager::resize($temp_name, $fileName, null, null, $type))
                                $errors[]  = $this->displayError($this->l('An error occurred during the image upload process.'));
                        if (isset($temp_name)) @unlink($temp_name);
                        if (!$errors) {
                            if (Configuration::get('TEA_LOOKBOOK_BANNER') != '') {
                                $oldImage = dirname(__FILE__).'/views/img/images/config/'.Configuration::get('TEA_LOOKBOOK_BANNER');
                                if (file_exists($oldImage)) @unlink($oldImage);
                            }
                            Configuration::updateValue('TEA_LOOKBOOK_BANNER', $imageName, true);
                        }
                    }
                }
            }
            if (!$errors) {
                $languages = Language::getLanguages(false);
                $valules   = array();
                foreach ($languages as $lang) {
                    $valules[$lang['id_lang']] = trim(Tools::getValue('TEA_LOOKBOOK_DESCRIPTION_'.$lang['id_lang']));
                }
                Configuration::updateValue('TEA_LOOKBOOK_DESCRIPTION', $valules, true);
            }
            if (Tools::isSubmit('ajax')) {
                die(
                    Tools::jsonEncode(array(
                        'error' => false,
                        'success' => $this->l('add successfully'),
                    ))
                );
            }
        }
        if (count($errors)) $this->_html .= $this->displayError(implode('<br />', $errors));
        elseif (Tools::isSubmit('submitlookbook') && Tools::getValue('id_lookbook'))
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
        elseif (Tools::isSubmit('submitlookbook'))
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=3&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
    }

    public function hookDisplayHeader($params)
    {
        $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/front.css', 'all');
        $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/style.css', 'all');
        $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/animate.css', 'all');
        $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/owl.carousel.css', 'all');
        $this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/imagesloaded.pkgd.min.js', 'all');
        if (Configuration::get('TEA_LOOKBOOK_LIBRARY_MASONRY'))
                $this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/isotope.pkgd.min.js', 'all');
        if (Configuration::get('TEA_LOOKBOOK_LIBRARY_SLICK_SLIDER')) {
            $this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/slick.js', 'all');
            $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/slick.css', 'all');
            $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/slick-theme.css', 'all');
        }
        if (Configuration::get('TEA_LOOKBOOK_CSS3_EFFECT')) {
            $this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/wow.min.js', 'all');
            $this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/wow-custom.js', 'all');
        }
        $this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/lookbook.js', 'all');
    }

    public function clearCache()
    {
        
    }

    public function hookActionShopDataDuplication($params)
    {
        Db::getInstance()->execute('
			INSERT IGNORE INTO '._DB_PREFIX_.'lookbook (id_tea_lookbook, id_shop)
			SELECT id_tea_lookbook, '.(int) $params['new_id_shop'].'
			FROM '._DB_PREFIX_.'lookbook
			WHERE id_shop = '.(int) $params['old_id_shop']
        );
    }

    public function headerHTML()
    {
        if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name) return;

        $this->context->controller->addJqueryUI('ui.sortable');
        $html = '<script type="text/javascript">
			$(function() {
				var $lookbooks = $("#lookbooks");
				$lookbooks.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize") + "&action=updateLookBooksPosition";
						$.post("'.$this->context->shop->physical_uri.$this->context->shop->virtual_uri.'modules/'.$this->name.'/ajax_'.$this->name.'.php?secure_key='.$this->secure_key.'", order);
						}
					});
				$lookbooks.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
			});
		</script>';

        return $html;
    }

    public function getNextPosition()
    {
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT MAX(hss.`position`) AS `next_position`
			FROM `'._DB_PREFIX_.'tea_lookbook` hss, `'._DB_PREFIX_.'lookbook` hs
			WHERE hss.`id_tea_lookbook` = hs.`id_tea_lookbook` AND hs.`id_shop` = '.(int) $this->context->shop->id
        );

        return ( ++$row['next_position']);
    }

    public function getLookBooks($active = null, $id_grouplook = 0)
    {
        $this->context = Context::getContext();
        $id_shop       = $this->context->shop->id;
        $id_lang       = $this->context->language->id;
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_tea_lookbook` as id_lookbook, hss.`position`, hss.`active`, hssl.`title`, hssl.`image`,hss.id_grouplook
			FROM '._DB_PREFIX_.'lookbook hs
			INNER JOIN '._DB_PREFIX_.'tea_lookbook hss ON (hs.id_tea_lookbook = hss.id_tea_lookbook)
			LEFT JOIN '._DB_PREFIX_.'tea_lookbook_lang hssl ON (hss.id_tea_lookbook = hssl.id_tea_lookbook)
			WHERE id_shop = '.(int) $id_shop.'
			AND hssl.id_lang = '.(int) $id_lang.
                ($active ? ' AND hss.`active` = 1' : ' ').($id_grouplook ? ' AND hss.id_grouplook='.(int) $id_grouplook : '' ).'
			ORDER BY hss.position'
        );
    }

    public function getAllImagesByLookBooksId($id_lookbook, $active = null, $id_shop = null)
    {
        $this->context = Context::getContext();
        $images        = array();

        if (!isset($id_shop)) $id_shop = $this->context->shop->id;

        $results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hssl.`image`, hssl.`id_lang`
			FROM '._DB_PREFIX_.'lookbook hs
			LEFT JOIN '._DB_PREFIX_.'tea_look_book hss ON (hs.id_tea_lookbook = hss.id_tea_lookbook)
			LEFT JOIN '._DB_PREFIX_.'tea_lookbook_lang hssl ON (hss.id_tea_lookbook = hssl.id_tea_lookbook)
			WHERE hs.`id_tea_lookbook` = '.(int) $id_lookbook.' AND hs.`id_shop` = '.(int) $id_shop.
            ($active ? ' AND hss.`active` = 1' : ' ')
        );

        foreach ($results as $result)
            $images[$result['id_lang']] = $result['image'];

        return $images;
    }

    public function displayStatus($id_lookbook)
    {
        $title = $this->l('Add note');
        $icon  = 'icon-plus';
        $class = '';
        $html  = '<a class="btn btn-default '.$class.'" href="'.AdminController::$currentIndex.
            '&configure='.$this->name.'
				&token='.Tools::getAdminTokenLite('AdminModules').'
				&add_note&id_lookbook='.(int) $id_lookbook.'" title="'.$title.'"><i class="'.$icon.'"></i> '.$title.'</a>';
        return $html;
    }

    public function lookbookExists($id_lookbook)
    {
        $req = 'SELECT hs.`id_tea_lookbook` as id_lookbook
				FROM `'._DB_PREFIX_.'lookbook` hs
				WHERE hs.`id_tea_lookbook` = '.(int) $id_lookbook;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);

        return ($row);
    }

    public function renderList($id_grouplook)
    {
        $lookbooks = $this->getLookBooks(false, $id_grouplook);
        foreach ($lookbooks as $key => $lookbook) {
            $lookbooks[$key]['status']    = $this->displayStatus($lookbook['id_lookbook']);
            $associated_shop_ids          = LookBook::getAssociatedIdsShop((int) $lookbook['id_lookbook']);
            if ($associated_shop_ids && count($associated_shop_ids) > 1) $lookbooks[$key]['is_shared'] = true;
            else $lookbooks[$key]['is_shared'] = false;
        }
        $this->context->smarty->assign(
            array(
                'link' => $this->context->link,
                'lookbooks' => $lookbooks,
                'ajax' => Tools::getValue('ajax'),
                'image_baseurl' => $this->_path.'/views/img/images/',
                'grouplook' => new GroupLook(Tools::getValue('id_grouplook')),
                'id_grouplook' => Tools::getValue('id_grouplook'),
            )
        );
        return $this->display(__FILE__, 'list.tpl');
    }

    public function renderAddForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Look book information'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'file_lang',
                        'label' => $this->l('Select a file'),
                        'name' => 'image',
                        'required' => true,
                        'lang' => true,
                        'desc' => sprintf($this->l('Maximum image size: %s.'), ini_get('upload_max_filesize'))
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Title'),
                        'name' => 'title',
                        'required' => true,
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Image width'),
                        'name' => 'width',
                        'required' => true,
                        'lang' => false,
                        'size' => 20
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Image height'),
                        'name' => 'height',
                        'required' => true,
                        'size' => 20
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Custom class'),
                        'name' => 'custom_class',
                        'size' => 20
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Link'),
                        'name' => 'link',
                        'size' => 20,
                        'lang' => True,
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Description'),
                        'name' => 'description',
                        'lang' => true,
                        'autoload_rte' => true,
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enabled'),
                        'name' => 'active_slide',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );
        if (Tools::isSubmit('id_lookbook') && $this->lookbookExists((int) Tools::getValue('id_lookbook'))) {
            $lookbook                       = new LookBook((int) Tools::getValue('id_lookbook'));
            $fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_lookbook');
            $fields_form['form']['images']  = $lookbook->image;
            $has_picture                    = true;
            foreach (Language::getLanguages(false) as $lang)
                if (!isset($lookbook->image[$lang['id_lang']])) $has_picture                    &= false;
            if ($has_picture) $fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'has_picture');
        }
        if (Tools::getValue('ajax')) $fields_form['form']['input'][]   = array('type' => 'hidden', 'name' => 'ajax');
        $fields_form['form']['input'][]   = array('type' => 'hidden', 'name' => 'id_grouplook');
        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();
        $helper->module                   = $this;
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'submitlookbook';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $language                         = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->tpl_vars                 = array(
            'base_url' => $this->context->shop->getBaseURL(),
            'language' => array(
                'id_lang' => $language->id,
                'iso_code' => $language->iso_code
            ),
            'fields_value' => $this->getAddFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path.'views/img/images/'
        );
        $helper->override_folder          = '/';
        $languages                        = Language::getLanguages(false);
        return $helper->generateForm(array($fields_form));
    }

    public function getAddFieldsValues()
    {
        $fields = array();
        if (Tools::isSubmit('id_lookbook') && $this->lookbookExists((int) Tools::getValue('id_lookbook'))) {
            $lookbook              = new LookBook((int) Tools::getValue('id_lookbook'));
            $fields['id_lookbook'] = (int) Tools::getValue('id_lookbook', $lookbook->id);
        } else $lookbook               = new LookBook();
        $fields['active_slide'] = Tools::getValue('active_slide', $lookbook->active);
        $fields['has_picture']  = true;
        $fields['id_grouplook'] = Tools::getValue('id_grouplook', $lookbook->id_grouplook);
        if (Tools::getValue('ajax')) $fields['ajax']         = 1;
        $languages              = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $fields['image'][$lang['id_lang']]       = Tools::getValue('image_'.(int) $lang['id_lang']);
            $fields['title'][$lang['id_lang']]       = Tools::getValue('title_'.(int) $lang['id_lang'], $lookbook->title[$lang['id_lang']]);
            $fields['link'][$lang['id_lang']]        = Tools::getValue('link_'.(int) $lang['id_lang'], $lookbook->link[$lang['id_lang']]);
            $fields['description'][$lang['id_lang']] = Tools::getValue('description_'.$lang['id_lang'], $lookbook->description[$lang['id_lang']]);
        }
        $fields['width']        = Tools::getValue('width', $lookbook->width);
        $fields['height']       = Tools::getValue('height', $lookbook->height);
        $fields['custom_class'] = Tools::getValue('custom_class', $lookbook->custom_class);
        return $fields;
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Description'),
                        'name' => 'TEA_LOOKBOOK_DESCRIPTION',
                        'lang' => true,
                        'autoload_rte' => true,
                        'suffix' => 'pixels'
                    ),
                    array(
                        'type' => 'file',
                        'label' => $this->l('Lookbook banner'),
                        'name' => 'TEA_LOOKBOOK_BANNER',
                        'thumb' => $this->_path.'views/img/images/config/'.Configuration::get('TEA_LOOKBOOK_BANNER'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Use library masonry'),
                        'name' => 'TEA_LOOKBOOK_LIBRARY_MASONRY',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Use library slick slider'),
                        'name' => 'TEA_LOOKBOOK_LIBRARY_SLICK_SLIDER',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Use CSS3 effect'),
                        'name' => 'TEA_LOOKBOOK_CSS3_EFFECT',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        if (Tools::getValue('ajax')) {
            $fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'ajax');
        }
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();

        $helper->identifier    = $this->identifier;
        $helper->submit_action = 'settinglookbook';
        $helper->currentIndex  = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token         = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars      = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path.'views/img/images/config'
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        $languages = Language::getLanguages(false);
        $fields    = array(
            'has_picture' => true,
        );
        if (Tools::isSubmit('settinglookbook')) {

            foreach ($languages as $l) {
                $fields['TEA_LOOKBOOK_DESCRIPTION'][$l['id_lang']] = Tools::getValue('TEA_LOOKBOOK_DESCRIPTION_'.$l['id_lang']);
            }
        } else {
            foreach ($languages as $l) {
                $fields['TEA_LOOKBOOK_DESCRIPTION'][$l['id_lang']] = Configuration::get('TEA_LOOKBOOK_DESCRIPTION', $l['id_lang']);
            }
        }
        $fields['TEA_LOOKBOOK_LIBRARY_MASONRY']      = (int) Tools::getValue('TEA_LOOKBOOK_LIBRARY_MASONRY',
                (int) Configuration::get('TEA_LOOKBOOK_LIBRARY_MASONRY'));
        $fields['TEA_LOOKBOOK_LIBRARY_SLICK_SLIDER'] = (int) Tools::getValue('TEA_LOOKBOOK_LIBRARY_SLICK_SLIDER',
                (int) Configuration::get('TEA_LOOKBOOK_LIBRARY_SLICK_SLIDER'));
        $fields['TEA_LOOKBOOK_CSS3_EFFECT']          = (int) Tools::getValue('TEA_LOOKBOOK_CSS3_EFFECT', (int) Configuration::get('TEA_LOOKBOOK_CSS3_EFFECT'));
        if (Tools::getValue('ajax')) $fields['ajax']                              = 1;
        return $fields;
    }

    public function hookDisplayBackOfficeHeader()
    {
        if (Tools::getValue('configure') == 'tea_lookbook') {
            $this->context->controller->addJquery();
            $this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/jquery-ui.js');
            $this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/admin.js');
            $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/jquery-ui.css', 'all');
            $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/font-awesome.min.css', 'all');
            $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/admin.css', 'all');
        }
    }

    public function renderFromGroup()
    {
        $position    = array(
            array(
                'id_option' => 'home_page',
                'name' => $this->l('Home page'),
            ),
            array(
                'id_option' => 'lookbook_page',
                'name' => $this->l('Lookbook page'),
            ),
            array(
                'id_option' => 'lookbook_product_page',
                'name' => $this->l('Lookbook Product Page'),
            ),
            array(
                'id_option' => 'lookbook_category_page',
                'name' => $this->l('Lookbook Category Page'),
            ),
        );
        $type        = array(
            array(
                'id_option' => 'list',
                'name' => $this->l('List'),
            ),
            array(
                'id_option' => 'slide',
                'name' => $this->l('Slide'),
            ),
            array(
                'id_option' => 'masonry',
                'name' => $this->l('Masonry'),
            )
        );
        $rows        = array(
            array(
                'id' => 1,
                'name' => '1',
            ),
            array(
                'id' => 2,
                'name' => '2',
            ),
            array(
                'id' => 3,
                'name' => '3',
            ),
            array(
                'id' => 4,
                'name' => '4',
            ),
            array(
                'id' => 6,
                'name' => '6',
            )
        );
        $animations  = array(
            array(
                'id' => 'fadeInUp',
                'name' => 'fadeInUp',
            ),
            array(
                'id' => 'fadeIn',
                'name' => 'fadeIn',
            ),
            array(
                'id' => 'fadeInDown',
                'name' => 'fadeInDown',
            ),
            array(
                'id' => 'fadeInLeft',
                'name' => 'fadeInLeft',
            ),
            array(
                'id' => 'fadeInRight',
                'name' => 'fadeInRight',
            ),
            array(
                'id' => 'zoomIn',
                'name' => 'zoomIn',
            ),
            array(
                'id' => 'slideInUp',
                'name' => 'slideInUp',
            ),
            array(
                'id' => 'slideInDown',
                'name' => 'slideInDown',
            ),
            array(
                'id' => 'slideInLeft',
                'name' => 'slideInLeft',
            ),
            array(
                'id' => 'slideInRight',
                'name' => 'slideInRight',
            ),
            array(
                'id' => 'flipInX',
                'name' => 'flipInX',
            ),
            array(
                'id' => 'flipInY',
                'name' => 'flipInY',
            ),
        );
        $effects     = array(
            array(
                'id' => 'fadeOutUp',
                'name' => 'fadeOutUp',
            ),
            array(
                'id' => 'fadeOut',
                'name' => 'fadeOut',
            ),
            array(
                'id' => 'fadeOutDown',
                'name' => 'fadeOutDown',
            ),
            array(
                'id' => 'fadeOutLeft',
                'name' => 'fadeOutLeft',
            ),
            array(
                'id' => 'fadeOutRight',
                'name' => 'fadeOutRight',
            ),
            array(
                'id' => 'zoomOut',
                'name' => 'zoomOut',
            ),
            array(
                'id' => 'slideOutUp',
                'name' => 'slideOutUp',
            ),
            array(
                'id' => 'slideOutDown',
                'name' => 'slideOutDown',
            ),
            array(
                'id' => 'slideOutLeft',
                'name' => 'slideOutLeft',
            ),
            array(
                'id' => 'slideOutRight',
                'name' => 'slideOutRight',
            ),
            array(
                'id' => 'flipOutX',
                'name' => 'flipOutX',
            ),
            array(
                'id' => 'flipOutY',
                'name' => 'flipOutY',
            ),
        );
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => Tools::isSubmit('id_grouplook') ? $this->l('Edit group') : $this->l('Add group'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Title'),
                        'name' => 'title',
                        'required' => true,
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Product'),
                        'name' => 'product_name_lookbook',
                        'form_group_class' => 'product_lookbook',
                        'required' => true,
                        'class' => 'col-lg-9',
                        'suffix' => '<i class="icon-search"></i>',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'id_product',
                        'label' => $this->l('Product id'),
                        'class' => 'col-lg-3',
                        'form_group_class' => 'id_product_lookbook',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'id_category',
                        'label' => $this->l('Category id'),
                        'class' => 'col-lg-3',
                        'form_group_class' => 'id_category_lookbook',
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Hook product'),
                        'name' => 'hook_product',
                        'form_group_class' => 'product_lookbook',
                        'options' => array(
                            'query' => array(
                                array(
                                    'name' => $this->l('Vyberte'),
                                    'id_option' => ''
                                ),
                                array(
                                    'name' => $this->l('Hook display footer product'),
                                    'id_option' => 'displayFooterProduct'
                                ),
                                array(
                                    'name' => $this->l('Hook custom product'),
                                    'id_option' => 'displayCustomProduct'
                                )
                            ),
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Hook category'),
                        'name' => 'hook_category',
                        'form_group_class' => 'category_lookbook',
                        'options' => array(
                            'query' => array(
                                array(
                                    'name' => $this->l('Vyberte'),
                                    'id_option' => ''
                                ),
                                array(
                                    'name' => $this->l('Hook display category'),
                                    'id_option' => 'displayCustomCategory'
                                )
                            ),
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Description'),
                        'name' => 'description',
                        'lang' => true,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Position'),
                        'name' => 'position',
                        'options' => array(
                            'query' => $position,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                        'form_group_class' => 'group_position'
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Type'),
                        'name' => 'type',
                        'options' => array(
                            'query' => $type,
                            'id' => 'id_option',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Item in rows desktop'),
                        'name' => 'item_in_row',
                        'options' => array(
                            'query' => $rows,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'form_group_class' => 'slide list custom'
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Item in rows mobile'),
                        'name' => 'item_in_row_mobile',
                        'options' => array(
                            'query' => $rows,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'form_group_class' => 'slide list custom'
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Item in rows tablet'),
                        'name' => 'item_in_row_tablet',
                        'options' => array(
                            'query' => $rows,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'form_group_class' => 'slide list custom'
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Masonry column in desktop'),
                        'name' => 'masonry_column',
                        'options' => array(
                            'query' => $rows,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'form_group_class' => 'masonry custom',
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Masonry column in mobile'),
                        'name' => 'masonry_column_mobile',
                        'options' => array(
                            'query' => $rows,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'form_group_class' => 'masonry custom',
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Masonry column in tablet'),
                        'name' => 'masonry_column_tablet',
                        'options' => array(
                            'query' => $rows,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'form_group_class' => 'masonry custom',
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Effect'),
                        'name' => 'effect',
                        'options' => array(
                            'query' => $effects,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'form_group_class' => 'slide custom',
                        'desc' => $this->l('this option work only with one item'),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Animation'),
                        'name' => 'animation',
                        'options' => array(
                            'query' => $animations,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Autoplay'),
                        'name' => 'autoplay',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                        'form_group_class' => 'slide custom',
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Loop'),
                        'name' => 'infinite',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                        'form_group_class' => 'slide custom'
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Show next/preview'),
                        'name' => 'show_next_preview',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                        'form_group_class' => 'slide custom'
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Pavination'),
                        'name' => 'pavination',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                        'form_group_class' => 'slide custom'
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Full width'),
                        'name' => 'full_width',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Custom class'),
                        'name' => 'custom_class'
                    ),
                    array(
                        'type' => 'color',
                        'label' => $this->l('Background'),
                        'name' => 'background'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Margin'),
                        'name' => 'margin'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Padding'),
                        'name' => 'padding',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Padding item'),
                        'name' => 'padding_item',
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enabled'),
                        'name' => 'active',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );
        if (Tools::isSubmit('id_grouplook')) {
            $fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_grouplook');
        }
        if (Tools::isSubmit('ajax')) $fields_form['form']['input'][]   = array('type' => 'hidden', 'name' => 'ajax');
        $fields_form['form']['input'][]   = array('type' => 'hidden', 'name' => 'layout');
        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'SubmitGroupLook';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars                 = array(
            'fields_value' => $this->getGroupFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path.'views/img/images/config'
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getGroupFieldsValues()
    {
        if (Tools::isSubmit('id_grouplook')) {
            $groupLook              = new GroupLook(Tools::getValue('id_grouplook'));
            $fields['id_grouplook'] = Tools::getValue('id_grouplook');
        } else $groupLook                       = new GroupLook();
        if (Tools::getValue('ajax')) $fields['ajax']                  = 1;
        $fields['active']                = Tools::getValue('active', $groupLook->active);
        $fields['position']              = Tools::getValue('layout', $groupLook->position);
        $fields['type']                  = Tools::getValue('type', $groupLook->type);
        $fields['item_in_row']           = Tools::getValue('item_in_row', $groupLook->item_in_row);
        $fields['item_in_row_mobile']    = Tools::getValue('item_in_row_mobile', $groupLook->item_in_row_mobile);
        $fields['item_in_row_tablet']    = Tools::getValue('item_in_row_tablet', $groupLook->item_in_row_tablet);
        $fields['masonry_column']        = Tools::getValue('masonry_column', $groupLook->masonry_column);
        $fields['masonry_column_mobile'] = Tools::getValue('masonry_column_mobile', $groupLook->masonry_column_mobile);
        $fields['masonry_column_tablet'] = Tools::getValue('masonry_column_tablet', $groupLook->masonry_column_tablet);
        $fields['custom_class']          = Tools::getValue('custom_class', $groupLook->custom_class);
        $fields['animation']             = Tools::getValue('animation', $groupLook->animation);
        $fields['effect']                = Tools::getValue('effect', $groupLook->effect);
        $fields['autoplay']              = Tools::getValue('autoplay', $groupLook->autoplay);
        $fields['infinite']              = Tools::getValue('infinite', $groupLook->infinite);
        $fields['show_next_preview']     = Tools::getValue('show_next_preview', $groupLook->show_next_preview);
        $fields['pavination']            = Tools::getValue('pavination', $groupLook->pavination);
        $fields['full_width']            = Tools::getValue('full_width', $groupLook->full_width);
        $fields['background']            = Tools::getValue('background', $groupLook->background);
        $fields['margin']                = Tools::getValue('margin', $groupLook->margin);
        $fields['padding']               = Tools::getValue('padding', $groupLook->padding);
        $fields['padding_item']          = Tools::getValue('padding_item', $groupLook->padding_item);
        $fields['layout']                = Tools::getValue('layout', $groupLook->position);
        $fields['id_product']            = Tools::getValue('id_product', $groupLook->id_product);
        $fields['id_category']           = Tools::getValue('id_category', $groupLook->id_category);
        $fields['hook_product']          = Tools::getValue('hook_product', $groupLook->hook_product);
        $fields['hook_category']         = Tools::getValue('hook_category', $groupLook->hook_category);
        if ($groupLook->id_product) {
            $product                         = new Product($groupLook->id_product, false, $this->context->language->id);
            $fields['product_name_lookbook'] = Tools::getValue('product_name_lookbook', $product->name);
        } else $fields['product_name_lookbook'] = Tools::getValue('product_name_lookbook');
        $languages                       = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $fields['title'][$lang['id_lang']]       = Tools::getValue('title_'.(int) $lang['id_lang'], $groupLook->title[$lang['id_lang']]);
            $fields['description'][$lang['id_lang']] = Tools::getValue('description_'.$lang['id_lang'], $groupLook->description[$lang['id_lang']]);
        }
        return $fields;
    }

    public function hookDisplayBackOfficeFooter()
    {
        //$html='<script type="text/javascript" src="'.(__PS_BASE_URI__).'modules/'.$this->name.'/views/js/jquery-ui.js"></script>';
        // $html .='<script type="text/javascript" src="'.(__PS_BASE_URI__).'modules/'.$this->name.'/views/js/admin.js"></script>';
        //return $html;
        //$this->context->controller->addJS((__PS_BASE_URI__).'modules/'.$this->name.'/views/js/admin.js');
    }

    public function renderMain()
    {
        if (Tools::getValue('layout')) $position   = Tools::getValue('layout');
        else $position   = 'home_page';
        $sql        = "SELECT * FROM "._DB_PREFIX_."tea_grouplook gl
        LEFT JOIN "._DB_PREFIX_."tea_grouplook_lang gll ON( gl.id_grouplook= gll.id_grouplook AND gll.id_lang='".(int) $this->context->language->id."')
        LEFT JOIN "._DB_PREFIX_."tea_grouplook_shop gls ON (gl.id_groupLook= gls.id_grouplook)
        WHERE gls.id_shop ='".(int) $this->context->shop->id."' AND gl.position='".$position."' ORDER BY gl.order_by";
        $groupLooks = Db::getInstance()->executeS($sql);
        if ($groupLooks) {
            foreach ($groupLooks as $key => &$groupLook) {
                if ($groupLook['id_product']) {
                    $product = new Product($groupLook['id_product'], false, $this->context->language->id);
                    if ($product->id) {
                        $groupLook['name_product'] = $product->name;
                        $id_image                  = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int) $product->id.' AND cover=1');
                        $url_image                 = $this->context->link->getImageLink($product->link_rewrite, $id_image, 'small_default');
                        $groupLook['url_image']    = $url_image;
                        $groupLook['link_product'] = $this->context->link->getProductLink($product->id);
                    } else unset($groupLooks[$key]);
                }
            }
        }
        $this->context->smarty->assign(
            array(
                'groupLooks' => $groupLooks,
                'position' => $position,
                'ajax' => Tools::getValue('ajax'),
            )
        );
        return $this->display(__FILE__, 'admin.tpl');
    }

    public function getMaxPositionGroupLook()
    {
        return Db::getInstance()->getValue('SELECT MAX(order_by) FROM '._DB_PREFIX_.'tea_grouplook');
    }

    public function hookDisplayHome()
    {
        $postion    = 'home_page';
        $groupLooks = $this->getLookbookPage($postion, Tools::getValue('id_product'));
        if ($groupLooks) {
            foreach ($groupLooks as $key => &$groupLook) {
                if ($groupLook['id_product']) {
                    $product = new Product($groupLook['id_product'], false, $this->context->language->id);
                    if ($product->id) {
                        $groupLook['name_product'] = $product->name;
                        $id_image                  = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int) $product->id.' AND cover=1');
                        $url_image                 = $this->context->link->getImageLink($product->link_rewrite, $id_image, 'small_default');
                        $groupLook['url_image']    = $url_image;
                        $groupLook['link_product'] = $this->context->link->getProductLink($product->id);
                    } else unset($groupLooks[$key]);
                }
            }
        }
        $this->context->smarty->assign(
            array(
                'image_baseurl' => _MODULE_DIR_.'tea_lookbook/views/img/images/',
                'groupLooks' => $groupLooks,
                'link' => $this->context->link,
                'tea_lookbook_description' => Configuration::get('TEA_LOOKBOOK_DESCRIPTION', $this->context->language->id),
                'thumb' => _MODULE_DIR_.'tea_lookbook/views/img/images/config/'.Configuration::get('TEA_LOOKBOOK_BANNER'),
        ));
        if (version_compare(_PS_VERSION_, '1.7', '>=')) return $this->display(__FILE__, 'lookbook.tpl');
        else return $this->display(__FILE__, 'lookbook16.tpl');
    }

    public function renderFormNote()
    {
        $this->context->smarty->assign($this->getNoteFieldsValue());
        $this->context->smarty->assign(
            array(
                'module_url' => $this->_path,
                'languages' => Language::getLanguages(false),
            )
        );
        return $this->display(__FILE__, 'form_note.tpl');
    }

    public function getNoteFieldsValue()
    {
        $id_product_lookbook = (int) Tools::getValue('id_product_lookbook');
        $note                = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'product_lookbook WHERE id_product_lookbook='.(int) $id_product_lookbook);
        if ($note) {
            $product = new Product($note['id_product'], true, $this->context->language->id);
            if (Validate::isLoadedObject($product)) {
                $id_image             = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int) $note['id_product'].' AND cover=1');
                $url_image            = Context::getContext()->link->getImageLink($product->link_rewrite, $id_image, 'home_default');
                $note['name_product'] = $product->name;
                $note['link_img']     = $url_image;
                $note['id_product']   = $product->id;
            }
        }
        $note_langs = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'product_lookbook_lang WHERE id_product_lookbook='.(int) $id_product_lookbook);
        if ($note_langs) {
            foreach ($note_langs as $note_lang) {
                $note['html_content'][$note_lang['id_lang']] = $note_lang['html_content'];
            }
        }
        return $note;
    }

    public function displayNote($id_product_lookbook)
    {
        $lookbook_product = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."product_lookbook WHERE id_product_lookbook=".(int) $id_product_lookbook);
        if ($lookbook_product) {
            if ($lookbook_product['content_type'] == 'html') {
                $lookbook_product['html_content'] = Db::getInstance()->getValue('SELECT html_content FROM '._DB_PREFIX_.'product_lookbook_lang WHERE id_lang="'.(int) $this->context->language->id.'" AND id_product_lookbook='.(int) $id_product_lookbook);
            } else {
                $product = new Product($lookbook_product['id_product'], true, $this->context->language->id);
                if (Validate::isLoadedObject($product)) {
                    $id_image                                = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int) $lookbook_product['id_product'].' AND cover=1');
                    $url_image                               = Context::getContext()->link->getImageLink($product->link_rewrite, $id_image, 'home_default');
                    $lookbook_product['name_product']        = $product->name;
                    $lookbook_product['link_img']            = $url_image;
                    $lookbook_product['id_product']          = $product->id;
                    $lookbook_product['description']         = $product->description;
                    $price                                   = $product->getPrice(true, null);
                    $lookbook_product['price']               = Tools::displayPrice($price);
                    $lookbook_product['description_product'] = $product->description_short ? $product->description_short : $product->description;
                }
            }
        }
        $this->context->smarty->assign(
            array(
                'lookbook_product' => $lookbook_product,
            )
        );
        return $this->display(__FILE__, 'display_note.tpl');
    }

    public function hookDisplayFooterProduct($params)
    {
        $postion    = 'lookbook_product_page';
        $groupLooks = $this->getLookbookPage($postion, Tools::getValue('id_product'), 'displayFooterProduct');
        if ($groupLooks) {
            foreach ($groupLooks as $key => &$groupLook) {
                if ($groupLook['id_product']) {
                    $product = new Product($groupLook['id_product'], false, $this->context->language->id);
                    if ($product->id) {
                        $groupLook['name_product'] = $product->name;
                        $id_image                  = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int) $product->id.' AND cover=1');
                        $url_image                 = $this->context->link->getImageLink($product->link_rewrite, $id_image, 'small_default');
                        $groupLook['url_image']    = $url_image;
                        $groupLook['link_product'] = $this->context->link->getProductLink($product->id);
                    } else unset($groupLooks[$key]);
                }
            }
        }
        $this->context->smarty->assign(
            array(
                'image_baseurl' => _MODULE_DIR_.'tea_lookbook/views/img/images/',
                'groupLooks' => $groupLooks,
                'link' => $this->context->link,
                'tea_lookbook_description' => Configuration::get('TEA_LOOKBOOK_DESCRIPTION', $this->context->language->id),
                'thumb' => _MODULE_DIR_.'tea_lookbook/views/img/images/config/'.Configuration::get('TEA_LOOKBOOK_BANNER'),
        ));
        if (version_compare(_PS_VERSION_, '1.7', '>=')) return $this->display(__FILE__, 'product_lookbook.tpl');
        else return $this->display(__FILE__, 'product_lookbook16.tpl');
    }

    public function getLookbookPage($position, $id_product = 0, $hook_product = '')
    {
        $groupLooks = Db::getInstance()->executeS('
            SELECT * FROM '._DB_PREFIX_.'tea_grouplook gl
            LEFT JOIN '._DB_PREFIX_.'tea_grouplook_lang gll ON (gl.id_grouplook=gll.id_grouplook AND gll.id_lang='.(int) $this->context->language->id.')
            LEFT JOIN '._DB_PREFIX_.'tea_grouplook_shop gls ON (gl.id_grouplook=gls.id_grouplook )
            WHERE gls.id_shop ='.(int) $this->context->shop->id.' AND gl.position="'.pSQL($position).'" '.($id_product ? ' AND id_product="'.(int) $id_product.'"'
                : '').($hook_product != '' ? ' AND gl.hook_product ="'.pSQL($hook_product).'"' : '').' AND gl.active=1 ORDER BY gl.order_by
        ');
        if ($groupLooks) {
            foreach ($groupLooks as &$groupLook) {
                $lookbooks = Db::getInstance()->executeS('
            		SELECT *
            		FROM '._DB_PREFIX_.'lookbook hs
            		INNER JOIN '._DB_PREFIX_.'tea_lookbook hss ON (hs.id_tea_lookbook = hss.id_tea_lookbook AND hss.id_grouplook='.(int) $groupLook['id_grouplook'].')
            		LEFT JOIN '._DB_PREFIX_.'tea_lookbook_lang hssl ON (hss.id_tea_lookbook = hssl.id_tea_lookbook)
            		WHERE id_shop = '.(int) $this->context->shop->id.'
            		AND hssl.id_lang = '.(int) $this->context->language->id.
                    ' AND hss.`active` = 1 ORDER BY hss.position'
                );
                if ($lookbooks)
                        foreach ($lookbooks as &$lookbook) {
                        $lookbook_products = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."product_lookbook where id_tea_lookbook =".(int) $lookbook['id_tea_lookbook'].' order by id_product_lookbook');
                        if ($lookbook_products)
                            foreach ($lookbook_products as $key => &$lookbook_product) {
                                if ($lookbook_product['content_type'] == 'html') {
                                    $lookbook_product['html_content'] = Db::getInstance()->getValue('SELECT html_content FROM '._DB_PREFIX_.'product_lookbook_lang WHERE id_product_lookbook="'.(int) $lookbook_product['id_product_lookbook'].'" AND id_lang="'.(int) Context::getContext()->language->id.'"');
                                }elseif($lookbook_product['content_type'] == 'category'){
                                   $category = new Category($lookbook_product['id_category'], true, $this->context->language->id);
                                    if (Validate::isLoadedObject($category) && $lookbook_product['id_category']) {
                                        $id_image                                = false;
                                        $url_image                               = false;
                                        $lookbook_product['name_product']        = $category->name;
                                        $lookbook_product['link_img']            = false;
                                        $lookbook_product['id_category']          = $category->id;
                                        $lookbook_product['price']               = false;
                                        $lookbook_product['description_product'] = false;
                                        $lookbook_product['link_category']        = $this->context->link->getCategoryLink($category);
                                    } else {
                                        unset($lookbook_products[$key]);
                                    }
                                }else {
                                    $product = new Product($lookbook_product['id_product'], true, $this->context->language->id);

                                    if($product){
                                         if (Validate::isLoadedObject($product) && $lookbook_product['id_product']) {
                                            $id_image                                = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int) $lookbook_product['id_product'].' AND cover=1');
                                            $url_image                               = Context::getContext()->link->getImageLink($product->link_rewrite, $id_image,
                                                'home_default');
                                            $lookbook_product['name_product']        = $product->name;
                                            $lookbook_product['link_img']            = $url_image;
                                            $lookbook_product['id_product']          = $product->id;
                                            $lookbook_product['price']               = Tools::displayPrice($product->getPrice());
                                            $lookbook_product['description_product'] = $product->description_short ? $product->description_short : $product->description;
                                            $lookbook_product['link_product']        = $this->context->link->getProductLink($product);
                                        } else {
                                            unset($lookbook_products[$key]);
                                        }                                    
                                    }

                                }
                            }
                        $lookbook['lookbook_products'] = $lookbook_products;
                    }
                $groupLook['lookbooks'] = $lookbooks;
            }
        }
        return $groupLooks;
    }

    public function getLookbookCategory($position, $id_category = 0, $hook_category = '')
    {
        $groupLooks = Db::getInstance()->executeS('
            SELECT * FROM '._DB_PREFIX_.'tea_grouplook gl
            LEFT JOIN '._DB_PREFIX_.'tea_grouplook_lang gll ON (gl.id_grouplook=gll.id_grouplook AND gll.id_lang='.(int) $this->context->language->id.')
            LEFT JOIN '._DB_PREFIX_.'tea_grouplook_shop gls ON (gl.id_grouplook=gls.id_grouplook )
            WHERE gls.id_shop ='.(int) $this->context->shop->id.' AND gl.position="'.pSQL($position).'" '.($id_category ? ' AND id_category="'.(int) $id_category.'"'
                : '').($hook_category != '' ? ' AND gl.hook_category ="'.pSQL($hook_category).'"' : '').' AND gl.active=1 ORDER BY gl.order_by
        ');

        if ($groupLooks) {
            foreach ($groupLooks as &$groupLook) {
                $lookbooks = Db::getInstance()->executeS('
            		SELECT *
            		FROM '._DB_PREFIX_.'lookbook hs
            		INNER JOIN '._DB_PREFIX_.'tea_lookbook hss ON (hs.id_tea_lookbook = hss.id_tea_lookbook AND hss.id_grouplook='.(int) $groupLook['id_grouplook'].')
            		LEFT JOIN '._DB_PREFIX_.'tea_lookbook_lang hssl ON (hss.id_tea_lookbook = hssl.id_tea_lookbook)
            		WHERE id_shop = '.(int) $this->context->shop->id.'
            		AND hssl.id_lang = '.(int) $this->context->language->id.
                    ' AND hss.`active` = 1 ORDER BY hss.position'
                );

                if ($lookbooks)
                        foreach ($lookbooks as &$lookbook) {
                        $lookbook_products = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."product_lookbook where id_tea_lookbook =".(int) $lookbook['id_tea_lookbook'].' order by id_product_lookbook');                        
                        if ($lookbook_products)
                                foreach ($lookbook_products as $key => &$lookbook_product) {
                                if ($lookbook_product['content_type'] == 'html') {
                                    $lookbook_product['html_content'] = Db::getInstance()->getValue('SELECT html_content FROM '._DB_PREFIX_.'product_lookbook_lang WHERE id_product_lookbook="'.(int) $lookbook_product['id_product_lookbook'].'" AND id_lang="'.(int) Context::getContext()->language->id.'"');
                                }elseif($lookbook_product['content_type'] == 'product'){
                                   $product = new Product($lookbook_product['id_product'], false, $this->context->language->id);
                                    if ($product->id) {
                                        $lookbook_product['name_product'] = $product->name;
                                        $id_image                  = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int) $product->id.' AND cover=1');
                                        $url_image                 = $this->context->link->getImageLink($product->link_rewrite, $id_image, 'small_default');
                                        $lookbook_product['url_image']    = $url_image;
                                        $lookbook_product['link_product'] = $this->context->link->getProductLink($product->id);
                                    } else unset($groupLooks[$key]); 
                                }
                                else {
                                    $category = new Category($lookbook_product['id_category'], true, $this->context->language->id);
                                    if (Validate::isLoadedObject($category) && $lookbook_product['id_category']) {
                                        $id_image                                = false;
                                        $url_image                               = false;
                                        $lookbook_product['name_product']        = $category->name;
                                        $lookbook_product['link_img']            = false;
                                        $lookbook_product['id_category']          = $category->id;
                                        $lookbook_product['price']               = false;
                                        $lookbook_product['description_product'] = false;
                                        $lookbook_product['link_category']        = $this->context->link->getCategoryLink($category);
                                    } else {
                                        unset($lookbook_products[$key]);
                                    }
                                }
                            }
                        $lookbook['lookbook_products'] = $lookbook_products;
                    }
                $groupLook['lookbooks'] = $lookbooks;
            }
        }
        return $groupLooks;
    }

    public function hookDisplayCustomProduct()
    {
        if ((int) Tools::getValue('id_product')) {
            $postion    = 'lookbook_product_page';
            $groupLooks = $this->getLookbookPage($postion, Tools::getValue('id_product'), 'displayCustomProduct');
            if ($groupLooks) {
                foreach ($groupLooks as $key => &$groupLook) {
                    if ($groupLook['id_product']) {
                        $product = new Product($groupLook['id_product'], false, $this->context->language->id);
                        if ($product->id) {
                            $groupLook['name_product'] = $product->name;
                            $id_image                  = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int) $product->id.' AND cover=1');
                            $url_image                 = $this->context->link->getImageLink($product->link_rewrite, $id_image, 'small_default');
                            $groupLook['url_image']    = $url_image;
                            $groupLook['link_product'] = $this->context->link->getProductLink($product->id);
                        } else unset($groupLooks[$key]);
                    }
                }
            }
            $this->context->smarty->assign(
                array(
                    'image_baseurl' => _MODULE_DIR_.'tea_lookbook/views/img/images/',
                    'groupLooks' => $groupLooks,
                    'link' => $this->context->link,
                    'tea_lookbook_description' => Configuration::get('TEA_LOOKBOOK_DESCRIPTION', $this->context->language->id),
                    'thumb' => _MODULE_DIR_.'tea_lookbook/views/img/images/config/'.Configuration::get('TEA_LOOKBOOK_BANNER'),
            ));
            if (version_compare(_PS_VERSION_, '1.7', '>=')) return $this->display(__FILE__, 'custom_product_lookbook.tpl');
            else return $this->display(__FILE__, 'custom_product_lookbook16.tpl');
        }
        return '';
    }

    public function hookDisplayCustomCategory()
    {        
        if ((int) Tools::getValue('id_category')) {
            $postion    = 'lookbook_category_page';
            $groupLooks = $this->getLookbookCategory($postion, Tools::getValue('id_category'), 'displayCustomCategory');
            if ($groupLooks) {
                foreach ($groupLooks as $key => &$groupLook) {
                    if ($groupLook['id_category']) {
                        $category = new Category($groupLook['id_category'], false, $this->context->language->id);
                        if ($category->id) {
                            $groupLook['name_category'] = $category->name;
                            $id_image                   = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_category=".(int) $category->id.' AND cover=1');
                            $url_image                  = $this->context->link->getImageLink($category->link_rewrite, $id_image, 'small_default');
                            $groupLook['url_image']     = $url_image;
                            $groupLook['link_category'] = $this->context->link->getCategoryLink($category->id);
                        } else unset($groupLooks[$key]);
                    }
                }
            }
            $this->context->smarty->assign(
                array(
                    'image_baseurl' => _MODULE_DIR_.'tea_lookbook/views/img/images/',
                    'groupLooks' => $groupLooks,
                    'link' => $this->context->link,
                    'tea_lookbook_description' => Configuration::get('TEA_LOOKBOOK_DESCRIPTION', $this->context->language->id),
                    'thumb' => _MODULE_DIR_.'tea_lookbook/views/img/images/config/'.Configuration::get('TEA_LOOKBOOK_BANNER'),
            ));
            if (version_compare(_PS_VERSION_, '1.7', '>=')) return $this->display(__FILE__, 'custom_category_lookbook.tpl');
        }
        return '';
    }
}