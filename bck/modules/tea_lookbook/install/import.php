<?php
include_once(_PS_MODULE_DIR_.'tea_lookbook/LookBook.php');
include_once(_PS_MODULE_DIR_.'tea_lookbook/GroupLook.php');
class TeaImportLookbook extends Module
{
	public	function __construct()
	{
		$this->name = 'tea_lookbook';
		parent::__construct();
	}
    public function importLookBooks()
    {
        $context = Context::getContext();
		$shop_id = $context->shop->id;
        $languages = Language::getLanguages(false);
        $filename="import.xml";
        if (file_exists(_PS_MODULE_DIR_.'tea_lookbook/xml/'.$filename))	
		{	
			$xml = simplexml_load_file(_PS_MODULE_DIR_.'tea_lookbook/xml/'.$filename);
            foreach($xml->grouplookbook as $groupLookbook)
            {
               $groupLook = new GroupLook();
               $groupLook->position=$groupLookbook['position'];
               $groupLook->type =$groupLookbook['type'];
               if($groupLookbook['id_product'])
               {
                    if(Db::getInstance()->executeS('SELECT id_product FROM '._DB_PREFIX_.'product where id_product='.(int)$groupLookbook['id_product']))
                        $groupLook->id_product =$groupLookbook['id_product'];
                    else
                        $groupLook->id_product = (int)Db::getInstance()->getValue('SELECT id_product FROM '._DB_PREFIX_.'product');
                    $groupLook->hook_product=$groupLookbook['hook_product'];
                    $groupLook->item_in_row =$groupLookbook['item_in_row'];
                    $groupLook->item_in_row_mobile =$groupLookbook['item_in_row_mobile'];
                    $groupLook->item_in_row_tablet =$groupLookbook['item_in_row_tablet'];
                    $groupLook->masonry_column =$groupLookbook['masonry_column'];
                    $groupLook->masonry_column_tablet =$groupLookbook['masonry_column_tablet'];
                    $groupLook->masonry_column_mobile=$groupLookbook['masonry_column_mobile'];
                    $groupLook->custom_class=$groupLookbook['custom_class'];
                    $groupLook->animation =$groupLookbook['animation'];
                    $groupLook->effect =$groupLookbook['effect'];
                    $groupLook->infinite =$groupLookbook['infinite'];
                    $groupLook->autoplay =$groupLookbook['autoplay'];
                    $groupLook->show_next_preview =$groupLookbook['show_next_preview'];
                    $groupLook->pavination =$groupLookbook['pavination'];
                    $groupLook->order_by =$groupLookbook['order_by'];
                    $groupLook->full_width =$groupLookbook['full_width'];
                    $groupLook->background = $groupLookbook['background'];
                    $groupLook->margin =$groupLookbook['margin'];
                    $groupLook->active =$groupLookbook['active'];
                    foreach ($languages as $language)
                    {
                        $groupLook->title[$language['id_lang']] = $groupLookbook['title'];
                        $groupLook->description[$language['id_lang']] = (string)$groupLookbook->description;
                    }
                    $groupLook->add();
                    if($groupLook->id)
                    {
                        $id_group = $groupLook->id;
                        if($groupLookbook->lookbook)
                        {
                            foreach($groupLookbook->lookbook as $lookbook)
                            {
                                $lookBook = new LookBook();
                                $lookBook->id_grouplook=$id_group;
                                $lookBook->position=$lookbook['position'];
                                $lookBook->active=$lookbook['active'];
                                $lookBook->width=$lookbook['width'];
                                $lookBook->height=$lookbook['height'];
                                $lookBook->custom_class=$lookbook['custom_class'];
                                foreach ($languages as $language)
                                {
                                    $lookBook->title[$language['id_lang']] = $lookbook['title'];
                                    $lookBook->image[$language['id_lang']] = $lookbook['image']; 
                                    $lookBook->link[$language['id_lang']] = $lookbook['link']; 
                                    $lookBook->description[$language['id_lang']] = (string)$lookbook->description; 
                                }
                                $lookBook->add();
                                if($lookBook->id)
                                {
                                    $id_lookbook= $lookBook->id;
                                    if($lookbook->product)
                                    {
                                        foreach($lookbook->product as $product)
                                        {
                                            $id_product=$product['id_product'];
                                            if(!Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'product WHERE id_product='.(int)$id_product.' AND active=1'))
                                                $id_product= (int)Db::getInstance()->getValue('SELECT id_product FROM '._DB_PREFIX_.'product');
                                            if($id_product)
                                            {
                                                Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'product_lookbook(`id_product_lookbook`,`id_product`,`id_tea_lookbook`,`left`,`top`,`type`,`efect_when_hover`,`background`,`color`,`note_circle`,`number`,`width`,`height`,`show_img`,`show_name`,`show_price`,`position_product`,`show_description`,`show_title`) value("","'.(int)$id_product.'","'.$id_lookbook.'","'.$product['left'].'","'.$product['top'].'","'.$product['type'].'","'.$product['efect_when_hover'].'","'.$product['background'].'","'.$product['color'].'","'.$product['note_circle'].'","'.$product['number'].'","'.$product['width'].'","'.$product['height'].'","'.$product['show_img'].'","'.$product['show_name'].'","'.$product['show_price'].'","'.$product['position_product'].'","'.$product['show_description'].'","'.$product['show_title'].'")');
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
               }
               
               
            }
            foreach($xml->lookbook as $lookbook)
            {
                $class_lookbook = new LookBook();
                $class_lookbook->position = (int)$lookbook['position'];
    			$class_lookbook->active = (int)$lookbook['active'];
                $class_lookbook->width =(int)$lookbook['width'];
                $class_lookbook->height=(int)$lookbook['height'];
    			$languages = Language::getLanguages(false);
    			foreach ($languages as $language)
    			{
                    $class_lookbook->title[$language['id_lang']] = $lookbook['title'];
                    $class_lookbook->image[$language['id_lang']] = $lookbook['image'];
                }
                $class_lookbook->add();
                if($lookbook->product)
                {
                    foreach($lookbook->product as $product)
                    {
                        
                    }       
                
                }
            }
        }
    }
}