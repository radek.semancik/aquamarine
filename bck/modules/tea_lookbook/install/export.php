<?php
include_once(_PS_MODULE_DIR_.'tea_lookbook/LookBook.php');
class TeaExportLookBook extends Module
{
	public	function __construct()
	{
		$this->name = 'tea_lookbook';
		parent::__construct();
	}
    public function getGroupLookBooks()
    {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'tea_grouplook tg,'._DB_PREFIX_.'tea_grouplook_lang tgl,'._DB_PREFIX_.'tea_grouplook_shop tgs WHERE tg.id_grouplook=tgl.id_grouplook AND tg.id_grouplook=tg.id_grouplook AND tgl.id_lang="'.(int)$this->context->language->id.'" AND tgs.id_shop='.(int)$this->context->shop->id;
        return Db::getInstance()->executeS($sql);
    }
    public function getLookBooks($id_group)
    {
        $id_shop=(int)$this->context->shop->id;
        $sql ='SELECT * FROM `'._DB_PREFIX_.'lookbook` lbs,'._DB_PREFIX_.'tea_lookbook lb,'._DB_PREFIX_.'tea_lookbook_lang lbl WHERE  lb.id_tea_lookbook = lbs.id_tea_lookbook and lb.id_tea_lookbook=lbl.id_tea_lookbook and lbs.id_shop='.(int)$id_shop.' and lbl.id_lang='.(int)$this->context->language->id.' AND lb.id_grouplook="'.(int)$id_group.'" ORDER BY lb.id_tea_lookbook ASC'; 
        return Db::getInstance()->executeS($sql);
    }
    public function getProducts($id_tea_lookbook)
    {
        $sql ='SELECT * FROM '._DB_PREFIX_.'product_lookbook where id_tea_lookbook='.(int)$id_tea_lookbook;
        return Db::getInstance()->executeS($sql);
    }
    public function exportLookBooks()
    {
        $groupLooks = $this->getGroupLookBooks();
        if($groupLooks)
        {
            header('Content-type: text/xml');
    		header('Content-Disposition: attachment; filename="import.xml"');	
    		$xml_output = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
            $xml_output .= '<entity_profile>'."\n";	
            foreach($groupLooks as $groupLook)
            {
                $xml_output .="<grouplookbook position ='".$groupLook['position']."' type ='".$groupLook['type']."' id_product ='".$groupLook['id_product']."' hook_product='".$groupLook['hook_product']."' item_in_row='".$groupLook['item_in_row']."' item_in_row_mobile ='".$groupLook['item_in_row_mobile']."' item_in_row_tablet='".$groupLook['item_in_row_tablet']."' masonry_column_mobile='".$groupLook['masonry_column_mobile']."' custom_class='".$groupLook['custom_class']."' animation='".$groupLook['animation']."' effect='".$groupLook['effect']."' infinite='".$groupLook['infinite']."' autoplay ='".$groupLook['autoplay']."' show_next_preview='".$groupLook['show_next_preview']."' pavination='".$groupLook['pavination']."' order_by='".$groupLook['order_by']."' full_width='".$groupLook['full_width']."' background='".$groupLook['background']."' margin='".$groupLook['margin']."' padding='".$groupLook['padding']."' active='".$groupLook['active']."' title='".$groupLook['title']."'>"."\n";
                $xml_output .="<description><![CDATA[".$groupLook['description']."]]></description>"."\n";
                $lookbooks = $this->getLookBooks($groupLook['id_grouplook']);
                if($lookbooks)
                {
                    foreach($lookbooks as $lookbook)
                    {
                        $xml_output .="<lookbook id_grouplook='".$lookbook['id_grouplook']."' position='".$lookbook['position']."' active='".$lookbook['active']."' width='".$lookbook['width']."' height='".$lookbook['height']."' custom_class='".$lookbook['custom_class']."' title='".$lookbook['title']."' image='".$lookbook['image']."' link='".$lookbook['link']."'>"."\n";
                        $xml_output .="<description><![CDATA[".$lookbook['description']."]]></description>"."\n";
                        $products= $this->getProducts($lookbook['id_tea_lookbook']);
                        if($products)
                        {
                            foreach($products as $product)
                            {
                                $xml_output .="<product id_product='".$product['id_product']."' id_tea_lookbook='".$product['id_tea_lookbook']."' left ='".$product['left']."' top='".$product['top']."' type='".$product['type']."' efect_when_hover='".$product['efect_when_hover']."' background='".$product['background']."' color='".$product['color']."' note_circle='".$product['note_circle']."' number ='".$product['number']."' width='".$product['width']."' height='".$product['height']."' show_img='".$product['show_img']."' show_price='".$product['show_price']."' position_product='".$product['position_product']."' show_description='".$product['show_description']."' show_title='".$product['show_title']."'>"."\n";
                                $xml_output .="</product>"."\n";
                            } 
                        }                        
                        $xml_output .="</lookbook>"."\n";
                    }
                }
                $xml_output .='</grouplookbook>'."\n";
            }
            $xml_output .='</entity_profile>'."\n";
            echo $xml_output; 
            exit; 
        } 
    }
}
?>