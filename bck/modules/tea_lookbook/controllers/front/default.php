<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
class Tea_LookBookDefaultModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
	}
	public function initContent()
	{
		parent::initContent();
        $groupLooks = Db::getInstance()->executeS('
            SELECT * FROM '._DB_PREFIX_.'tea_grouplook gl
            LEFT JOIN '._DB_PREFIX_.'tea_grouplook_lang gll ON (gl.id_grouplook=gll.id_grouplook AND gll.id_lang='.(int)$this->context->language->id.')
            LEFT JOIN '._DB_PREFIX_.'tea_grouplook_shop gls ON (gl.id_grouplook=gls.id_grouplook )
            WHERE gls.id_shop ='.(int)$this->context->shop->id.' AND gl.position="lookbook_page" AND gl.active=1 ORDER BY gl.order_by
        ');
        if($groupLooks)
        {
            foreach($groupLooks as &$groupLook)
            {
                $lookbooks = Db::getInstance()->executeS('
        			SELECT *
        			FROM '._DB_PREFIX_.'lookbook hs
        			INNER JOIN '._DB_PREFIX_.'tea_lookbook hss ON (hs.id_tea_lookbook = hss.id_tea_lookbook AND hss.id_grouplook='.(int)$groupLook['id_grouplook'].')
        			LEFT JOIN '._DB_PREFIX_.'tea_lookbook_lang hssl ON (hss.id_tea_lookbook = hssl.id_tea_lookbook)
        			WHERE id_shop = '.(int)$this->context->shop->id.'
        			AND hssl.id_lang = '.(int)$this->context->language->id.
        			' AND hss.`active` = 1 ORDER BY hss.position'
        		);
                if($lookbooks)
                    foreach($lookbooks as &$lookbook)
                    {
                        $lookbook_products = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."product_lookbook where id_tea_lookbook =".(int)$lookbook['id_tea_lookbook'].' order by id_product_lookbook');
                        
                        if($lookbook_products)
                            foreach($lookbook_products as $key=> &$lookbook_product)
                            {
                                if($lookbook_product['content_type']=='html')
                                {
                                    $lookbook_product['html_content'] = Db::getInstance()->getValue('SELECT html_content FROM '._DB_PREFIX_.'product_lookbook_lang WHERE id_product_lookbook="'.(int)$lookbook_product['id_product_lookbook'].'" AND id_lang="'.(int)Context::getContext()->language->id.'"');
                                }
                                else
                                {
                                    $product = new Product($lookbook_product['id_product'],true,$this->context->language->id);
                                    if(Validate::isLoadedObject($product) && $lookbook_product['id_product'])
                                    {
                                        $id_image = Db::getInstance()->getValue("SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product=".(int)$lookbook_product['id_product'].' AND cover=1');
                                        $url_image = Context::getContext()->link->getImageLink($product->link_rewrite,$id_image,'home_default');
                                        $lookbook_product['name_product'] = $product->name;
                                        $lookbook_product['link_img'] = $url_image;
                                        $lookbook_product['id_product'] = $product->id;
                                        $lookbook_product['price'] = Tools::displayPrice($product->getPrice());
                                        $lookbook_product['description_product'] = $product->description_short?$product->description_short:$product->description;
                                        $lookbook_product['link_product'] = $this->context->link->getProductLink($product);
                                    }
                                    else
                                        unset($lookbook_products[$key]);
                                }
                            }
                        $lookbook['lookbook_products'] = $lookbook_products;
                    }
                $groupLook['lookbooks'] =$lookbooks;
                }
          }
          $this->context->smarty->assign(
            array(
                'image_baseurl' => _MODULE_DIR_.'tea_lookbook/views/img/images/',
                'groupLooks'=>$groupLooks,
                'link' =>$this->context->link,
                'tea_lookbook_description'=>Configuration::get('TEA_LOOKBOOK_DESCRIPTION',$this->context->language->id),
                'thumb' => _MODULE_DIR_.'tea_lookbook/views/img/images/config/'.Configuration::get('TEA_LOOKBOOK_BANNER'),
          ));
        if(version_compare(_PS_VERSION_, '1.7', '>='))
            $this->setTemplate('module:tea_lookbook/views/templates/front/lookbook.tpl');
        else
            $this->setTemplate('lookbook16.tpl');
	}
    public function getBreadcrumbLinks()
    {
        if(version_compare(_PS_VERSION_, '1.7', '<'))
            return '';
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Lookbook', [], 'Breadcrumb'),
            'url' => $this->context->link->getModuleLink('tea_lookbook', 'default')
         ];
         return $breadcrumb;
     }
}