<?php
class GroupLook extends ObjectModel
{
	public $title;
	public $position;
	public $type;
    public $id_product;
    public $id_category;
	public $item_in_row;
    public $item_in_mobile;
    public $item_in_tablet;
    public $masonry_column;
    public $masonry_column_mobile;
    public $masonry_column_tablet;
    public $custom_class;
    public $animation;
    public $effect;
    public $autoplay;
    public $show_next_preview;
    public $pavination;
    public $infinite;
    public $full_width;
    public $background;
    public $margin;
    public $padding;
    public $padding_item;
    public $active;
    public $order_by;
    public $description;
    public $hook_product;
    public $hook_category;
    public $id_shop;
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'tea_grouplook',
		'primary' => 'id_grouplook',
		'multilang' => true,
		'fields' => array(
            'active' =>			array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'title' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
            'description' =>    array('type'=>self::TYPE_STRING,'lang'=>true),
            'position' =>		array('type' => self::TYPE_STRING, 'required' => true),
			'type' =>			array('type' => self::TYPE_STRING,'validate' => 'isCleanHtml', 'size' => 255),
            'id_product' =>     array('type'=>self::TYPE_INT),
            'id_category' =>     array('type'=>self::TYPE_INT),
            'hook_product' =>   array('type'=>self::TYPE_STRING),
            'hook_category' =>   array('type'=>self::TYPE_STRING),
            'item_in_row' =>    array('type'=>self::TYPE_INT,'validate' => 'isunsignedInt', 'size'=>30),
            'item_in_row_mobile' =>    array('type'=>self::TYPE_INT,'validate' => 'isunsignedInt', 'size'=>30),
            'item_in_row_tablet' =>    array('type'=>self::TYPE_INT,'validate' => 'isunsignedInt', 'size'=>30),
            'masonry_column' => array('type'=>self::TYPE_INT,'validate' => 'isunsignedInt', 'size'=>30),
            'masonry_column_mobile' => array('type'=>self::TYPE_INT,'validate' => 'isunsignedInt', 'size'=>30),
            'masonry_column_tablet' => array('type'=>self::TYPE_INT,'validate' => 'isunsignedInt', 'size'=>30),
            'custom_class' =>   array('type'=>self::TYPE_STRING),
            'animation' =>array('type'=>self::TYPE_STRING),
            'effect' =>array('type'=>self::TYPE_STRING),
            'autoplay' => array('type'=>self::TYPE_BOOL),
            'show_next_preview' =>array('type'=>self::TYPE_BOOL),
            'pavination' =>array('type'=>self::TYPE_BOOL),
            'full_width' =>array('type'=>self::TYPE_BOOL),
            'infinite' =>array('type'=>self::TYPE_BOOL),
            'order_by' =>array('type'=>self::TYPE_INT),
            'background' =>array('type'=>self::TYPE_STRING),
            'margin'=>array('type'=>self::TYPE_STRING),
            'padding'=>array('type'=>self::TYPE_STRING),
            'padding_item'=>array('type'=>self::TYPE_STRING),
		)
	);
	public	function __construct($id_lookbook = null, $id_lang = null,$id_shop = null)
	{
		parent::__construct($id_lookbook, $id_lang,$id_shop);
	}
	public function add($autodate = true, $null_values = false)
	{
		$context = Context::getContext();
		$id_shop = $context->shop->id;

		$res = parent::add($autodate, $null_values);
		$res &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'tea_grouplook_shop` (`id_shop`, `id_grouplook`)
			VALUES('.(int)$id_shop.', '.(int)$this->id.')'
		);
		return $res;
	}
}