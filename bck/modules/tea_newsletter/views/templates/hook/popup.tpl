{*

**

* TeaThemes (c) All rights reserved.

*

* DISCLAIMER

*

* Do not edit, modify or copy this file.

* If you wish to customize it, contact us at teathemes@gmail.com.

*

* @author    TeaThemes

* @copyright 2016-2017 TeaThemes

* @license   Do not edit, modify or copy this file

**

*}

<script type="text/javascript">

var time_delay_popup ={$TEA_NEWSLETTER_DELAY_POPUP};

</script>

<div id="newsletter-modal" class="tea-newsletter-popup{if $TEA_NEWSLETTER_MOBILE_HIDE} tea-mobile-hide{/if}">

    <div class="tea-div-l1">

        <div class="tea-div-l2">

            <div class="tea-div-l3 modal-dialog modal-newsletter">

                <div class="modal-content">

                    <div class="row">

                        <div class="col-sm-6 col-xs-12 right-sm">

                            <div class="newsletter-signup newsletter-column">

                                <div class="newsletter-heading">

                                  {$TEA_NEWSLETTER_TITLE}

                                  <button id="newsletter-close" class="newsletter-close tea-close button"><i class="fa fa-times"></i></button>

                            

                                </div>

                                <div class="newsletter-content">

                                    {$TEA_NEWSLETTER_DESCRIPTION nofilter}

                                    <form class="tea-form frm-email" action="{$TEA_NEWSLETTER_ACTION}" method="post">

                                        <div class="tea-loading-div">

                                            <img class="tea-loading" src="{$TEA_NEWSLETTER_LOADING_IMG}" alt="{l s='Loading...' mod='tea_newsletter'}" />

                                        </div>

                                        <div class="tea-input-row">

                                            <label for="tea-email-input">{l s='Email: ' mod='tea_newsletter'}</label>

                                            <input type="text" id="tea-email-input" value="" placeholder="{l s='Enter your email...' mod='tea_newsletter'}" />

                                            <input class="button" type="submit" name="ynpSubmit" id="tea-submit" value="{l s='Subcribe' mod='tea_newsletter'}" />

                                        </div>

                                        <div class="tea-alert">

                                            {l s='Your Information will never be shared with any third party.' mod='tea_newsletter'}

                                        </div>

                                        {if !$TEA_NEWSLETTER_AUTO_HIDE}

                                            <div class="tea-input-checkbox">

                                                <input type="checkbox" id="tea-input-dont-show" name="ynpcheckbox" />

                                                {l s='Do not show this again' mod='tea_newsletter'}

                                            </div>

                                        {/if}

                                    </form>

                                </div>

                            </div>

                        </div>

                        <div class="col-sm-6 col-xs-12">

                            <div class="newsletter-media newsletter-column" {if $TEA_NEWSLETTER_IMAGE}style="background-image: url('{$TEA_NEWSLETTER_IMAGE}');"{/if}>

                                <div class="newsletter-group">

                                    {$TEA_NEWSLETTER_DES_LEFT nofilter}

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                

            </div>

        </div>

    </div>

</div>