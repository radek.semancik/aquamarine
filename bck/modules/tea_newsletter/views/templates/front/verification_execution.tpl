{*
**
* TeaThemes (c) All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at teathemes@gmail.com.
*
* @author    TeaThemes
* @copyright 2016-2017 TeaThemes
* @license   Do not edit, modify or copy this file
**
*}
{extends file='page.tpl'}

{block name="page_content"}
    {$message}
{/block}
