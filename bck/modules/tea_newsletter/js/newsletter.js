var _0xaae8="";
/**
* TeaThemes (c) All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at teathemes@gmail.com.
*
* @author    TeaThemes
* @copyright 2016-2017 TeaThemes
* @license   Do not edit, modify or copy this file
*/
$(document).ready(function(){
    $(window).load(function(){
        if($('.tea-newsletter-popup').length > 0)
        {
            delayMillis=time_delay_popup*1000;
            setTimeout(function() {
                $('.tea-newsletter-popup').addClass('active');
                $('.modal-newsletter').addClass('fadeInDown');
            }, delayMillis);
        }
    });
    $('#tea-submit').click(function(){
        var npemail = $('#tea-email-input').val();
        var npaction = $('.tea-form').attr('action');
        $('.tea-alert').remove();
        $('.tea-loading-div').show();
        $.ajax({
            url : npaction,
            type : 'post',
            dataType : 'json',
            data : {
                npemail : npemail
            },
            success: function(json){
                $('.tea-loading-div').hide();
                if(json['success'])
                {
                    $('.tea-form').after('<div class="tea-alert alert alert-success">'+json['success']+'</div>');
                    $('.tea-form').hide();                    
                }
                else
                {
                    $('.tea-input-div').after('<div class="tea-alert alert alert-danger">'+json['error']+'</div>');                    
                }                
            },
            error: function(){
                $('.tea-loading-div').hide();
            }
        });
        return false;
    });
    $('.tea-close').click(function(){
        var npemail = $('#tea-email-input').val();
        var npaction = $('.tea-form').attr('action');
        if($('#tea-input-dont-show').is(':checked'))
        {
            $.ajax({
                url : npaction,
                type : 'post',                
                data : {
                    close : 'yes'
                }
            });
        }
        $('.tea-newsletter-popup').hide().removeClass('active');
    });
    $(document).mouseup(function (e)
    {
        var container = $('#newsletter-modal .modal-newsletter');
        if (!container.is(e.target)&& container.has(e.target).length === 0)
        {
            $('.tea-newsletter-popup').hide().removeClass('active');
        }
    });
    $(document).keyup(function(e) { 
        if(e.keyCode == 27) {
            $('.tea-newsletter-popup').hide().removeClass('active');
        }
    });
});