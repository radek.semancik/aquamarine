<?php
/**
* TeaThemes (c) All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at teathemes@gmail.com.
*
* @author    TeaThemes
* @copyright 2016-2017 TeaThemes
* @license   Do not edit, modify or copy this file
*/
if (!defined('_PS_VERSION_'))
	exit;
if(!class_exists('Tea_newsletter'))
    require_once(dirname(__FILE__).'/../../tea_newsletter.php');
class Tea_newsletterSubmitModuleFrontController extends ModuleFrontController
{
    const TEA_GUEST_NOT_REGISTERED = -1;
	const TEA_CUSTOMER_NOT_REGISTERED = 0;
	const TEA_GUEST_REGISTERED = 1;
	const TEA_CUSTOMER_REGISTERED = 2;
    private $_mailDir;
    private $_id_lang_email;
    public function init()
	{	            
        if(!Module::isInstalled('tea_newsletter'))
        {
            $module = new Tea_newsletter();
            $module->_teaInstallDb();
        }
		parent::init();
        if(Module::isInstalled('tea_newsletter') && Module::isEnabled('tea_newsletter'))
            $this->_mailDir = dirname(__FILE__).'/../../../blocknewsletter/mails/';
        else
            $this->_mailDir = dirname(__FILE__).'/../../mails/';
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $language = new Language((int)$this->context->language->id);
        if(is_dir($this->_mailDir.$language->iso_code))
            $this->_id_lang_email = (int)$this->context->language->id;
        else
            $this->_id_lang_email = $id_lang_default;            
	}
    private function teaJsonEncode($message, $type = 'error')
    {
        $json=array();
        if($type == 'error')
            $json['error'] = $message;
        else
            $json['success'] = $message;
        die(Tools::JsonEncode($json));
    }
    private function teaMarkSubcribed()
    {
        $this->context->cookie->teanewsletter = 'subcribed';
        $this->context->cookie->write;
    }
    public function initContent()
	{
        if(Tools::getValue('close'))
        {
            $this->teaMarkSubcribed();
            die('closed');
        }
        if($npemail = Tools::getValue('npemail'))
        {            
            //$json = array();
            if(!Validate::isEmail($npemail))
            {
                $this->teaJsonEncode($this->module->l('Email is invalid','submit'));             
            }
            //Subcription
            $teaRegister_status = $this->teaIsNewsletterRegistered($npemail);
    		if ($teaRegister_status > 0)
    			$this->teaJsonEncode($this->module->l('This email address is already registered.','submit'));
    		$email = pSQL($npemail);
    		if (!$this->teaIsRegistered($teaRegister_status))
    		{
    			if (Configuration::get('NW_VERIFICATION_EMAIL'))
    			{
    				// create an unactive entry in the newsletter database
    				if ($teaRegister_status == self::TEA_GUEST_NOT_REGISTERED)
    					$this->teaRegisterGuest($email, false);                    
    				if (!$token = $this->teaGetToken($email, $teaRegister_status))
    					$this->teaJsonEncode($this->module->l('An error occurred during the subscription process.','submit'));
                    $this->teaMarkSubcribed();
    				$this->teaSendVerificationEmail($email, $token);
    
    				$this->teaJsonEncode($this->module->l('A verification email has been sent. Please check your inbox.','submit'),'success');
    			}
    			else
    			{        			
    				if ($this->teaRegister($email, $teaRegister_status))
                    {
                        if ($code = Configuration::get('NW_VOUCHER_CODE'))
            					$this->sendVoucher($email, $code);    
        				if (Configuration::get('NW_CONFIRMATION_EMAIL'))
        					$this->teaSendConfirmationEmail($email);
                        $this->teaMarkSubcribed();
                        $this->teaJsonEncode($this->module->l('You have successfully subscribed to this newsletter.','submit'),'success');   
                    }    					
    				else
    					$this->teaJsonEncode($this->module->l('An error occurred during the subscription process.','submit'));   
    				
    			}
    		}
        }
        $this->teaJsonEncode($this->module->l('Please enter your email','submit'));        
    }   
    private function teaIsRegistered($teaRegister_status)
	{
		return in_array(
			$teaRegister_status,
			array(self::TEA_GUEST_REGISTERED, self::TEA_CUSTOMER_REGISTERED)
		);
	} 
    private function teaIsNewsletterRegistered($customer_email)
	{
		$sql = 'SELECT `email`
				FROM '._DB_PREFIX_.'newsletter
				WHERE `email` = \''.pSQL($customer_email).'\'
				AND id_shop = '.$this->context->shop->id;

		if (Db::getInstance()->getRow($sql))
			return self::TEA_GUEST_REGISTERED;

		$sql = 'SELECT `newsletter`
				FROM '._DB_PREFIX_.'customer
				WHERE `email` = \''.pSQL($customer_email).'\'
				AND id_shop = '.$this->context->shop->id;

		if (!$teaRegistered = Db::getInstance()->getRow($sql))
			return self::TEA_GUEST_NOT_REGISTERED;

		if ($teaRegistered['newsletter'] == '1')
			return self::TEA_CUSTOMER_REGISTERED;

		return self::TEA_CUSTOMER_NOT_REGISTERED;
	}
    private function teaRegisterGuest($email, $active = true)
	{
		$sql = 'INSERT INTO '._DB_PREFIX_.'newsletter (id_shop, id_shop_group, email, newsletter_date_add, ip_registration_newsletter, http_referer, active)
				VALUES
				('.$this->context->shop->id.',
				'.$this->context->shop->id_shop_group.',
				\''.pSQL($email).'\',
				NOW(),
				\''.pSQL(Tools::getRemoteAddr()).'\',
				(
					SELECT c.http_referer
					FROM '._DB_PREFIX_.'connections c
					WHERE c.id_guest = '.(int)$this->context->customer->id.'
					ORDER BY c.date_add DESC LIMIT 1
				),
				'.(int)$active.'
				)';

		return Db::getInstance()->execute($sql);
	}
    private function teaGetToken($email, $teaRegister_status)
	{
		if (in_array($teaRegister_status, array(self::TEA_GUEST_NOT_REGISTERED, self::TEA_GUEST_REGISTERED)))
		{
			$sql = 'SELECT MD5(CONCAT( `email` , `newsletter_date_add`, \''.pSQL(Configuration::get('NW_SALT')).'\')) as token
					FROM `'._DB_PREFIX_.'newsletter`
					WHERE `active` = 0
					AND `email` = \''.pSQL($email).'\'';
		}
		else if ($teaRegister_status == self::TEA_CUSTOMER_NOT_REGISTERED)
		{
			$sql = 'SELECT MD5(CONCAT( `email` , `date_add`, \''.pSQL(Configuration::get('NW_SALT')).'\' )) as token
					FROM `'._DB_PREFIX_.'customer`
					WHERE `newsletter` = 0
					AND `email` = \''.pSQL($email).'\'';
		}
		return Db::getInstance()->getValue($sql);
	}
    protected function teaRegister($email, $teaRegister_status)
	{
		if ($teaRegister_status == self::TEA_GUEST_NOT_REGISTERED)
			return $this->teaRegisterGuest($email);

		if ($teaRegister_status == self::TEA_CUSTOMER_NOT_REGISTERED)
			return $this->teaRegisterUser($email);

		return false;
	}
    public function teaRegisterUser($email)
	{
		$sql = 'UPDATE '._DB_PREFIX_.'customer
				SET `newsletter` = 1, newsletter_date_add = NOW(), `ip_registration_newsletter` = \''.pSQL(Tools::getRemoteAddr()).'\'
				WHERE `email` = \''.pSQL($email).'\'
				AND id_shop = '.$this->context->shop->id;

		return Db::getInstance()->execute($sql);
	}
    public function teaSendConfirmationEmail($email)
	{
		return Mail::Send($this->_id_lang_email, 'newsletter_conf', Mail::l('Newsletter confirmation', $this->context->language->id), array(), pSQL($email), null, null, null, null, null, $this->_mailDir, false, $this->context->shop->id);
	}
    public function sendVoucher($email, $code)
	{
		return Mail::Send($this->_id_lang_email, 'newsletter_voucher', Mail::l('Newsletter voucher', $this->context->language->id), array('{discount}' => $code), $email, null, null, null, null, null, $this->_mailDir, false, $this->context->shop->id);
	}
    public function teaSendVerificationEmail($email, $token)
	{
		$verif_url = 
        Module::isInstalled('blocknewsletter') && Module::isEnabled('blocknewsletter') ? 
            Context::getContext()->link->getModuleLink(
    			'blocknewsletter', 'verification', array(
    				'token' => $token,
    			)
    		)
        :
            Context::getContext()->link->getModuleLink(
    			'TEA_newsletter', 'verification', array(
    				'token' => $token,
    			)
    		);
		return Mail::Send($this->_id_lang_email, 'newsletter_verif', Mail::l('Email verification', $this->context->language->id), array('{verif_url}' => $verif_url), $email, null, null, null, null, null, $this->_mailDir, false, $this->context->shop->id);
	}
}