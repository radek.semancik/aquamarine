<?php
/**
* TeaThemes (c) All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at teathemes@gmail.com.
*
* @author    TeaThemes
* @copyright 2016-2017 TeaThemes
* @license   Do not edit, modify or copy this file
*/

class Tea_newsletterVerificationModuleFrontController extends ModuleFrontController
{
	private $_message = '';
    private $_mailDir;
    private $_id_lang_email;
	/**
	 * @see FrontController::postProcess()
	 */
	public function postProcess()
	{
	   if(Module::isInstalled('blocknewsletter') && Module::isEnabled('blocknewsletter'))
            $this->_mailDir = dirname(__FILE__).'/../../../blocknewsletter/mails/';
        else
            $this->_mailDir = dirname(__FILE__).'/../../mails/';
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $language = new Language((int)$this->context->language->id);
        if(is_dir($this->_mailDir.$language->iso_code))
            $this->_id_lang_email = (int)$this->context->language->id;
        else
            $this->_id_lang_email = $id_lang_default;  
		$this->_message = $this->teaConfirmEmail(Tools::getValue('token'));
	}
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{	    
		parent::initContent();        
		$this->context->smarty->assign(
            array(
                '_message' => $this->_message,
                'path' => '<span class="tea-testimonial-breadcrumb-span">'.$this->module->l('Verify subcription','verification').'</span>'        
            )
        );
        $this->setTemplate('module:tea_newsletter/views/templates/front/verification_execution.tpl');
	}
    public function teaConfirmEmail($token)
	{	    
		$activated = false;
		if ($email = $this->teaGetGuestEmailByToken($token))
			$activated = $this->teaActivateGuest($email);
		else if ($email = $this->teaGetUserEmailByToken($token))
			$activated = $this->teaRegisterUser($email);

		if (!$activated)
			return $this->module->l('This email is already teaRegistered and/or invalid.','verification');

		if ($discount = Configuration::get('NW_VOUCHER_CODE'))
			$this->teaSendVoucher($email, $discount);

		if (Configuration::get('NW_CONFIRMATION_EMAIL'))
			$this->teaSendConfirmationEmail($email);

		return $this->module->l('Thank you for subscribing to our newsletter.','verification');
	}
    private function teaGetGuestEmailByToken($token)
	{
		$sql = 'SELECT `email`
				FROM `'._DB_PREFIX_.'newsletter`
				WHERE MD5(CONCAT( `email` , `newsletter_date_add`, \''.pSQL(Configuration::get('NW_SALT')).'\')) = \''.pSQL($token).'\'
				AND `active` = 0';

		return Db::getInstance()->getValue($sql);
	}
    private function teaActivateGuest($email)
	{
		return Db::getInstance()->execute(
			'UPDATE `'._DB_PREFIX_.'newsletter`
						SET `active` = 1
						WHERE `email` = \''.pSQL($email).'\''
		);
	}
    private function teaGetUserEmailByToken($token)
	{
		$sql = 'SELECT `email`
				FROM `'._DB_PREFIX_.'customer`
				WHERE MD5(CONCAT( `email` , `date_add`, \''.pSQL(Configuration::get('NW_SALT')).'\')) = \''.pSQL($token).'\'
				AND `newsletter` = 0';

		return Db::getInstance()->getValue($sql);
	}
    public function teaRegisterUser($email)
	{
		$sql = 'UPDATE '._DB_PREFIX_.'customer
				SET `newsletter` = 1, newsletter_date_add = NOW(), `ip_registration_newsletter` = \''.pSQL(Tools::getRemoteAddr()).'\'
				WHERE `email` = \''.pSQL($email).'\'
				AND id_shop = '.$this->context->shop->id;

		return Db::getInstance()->execute($sql);
	}
    public function teaSendVoucher($email, $code)
	{
		return Mail::Send($this->_id_lang_email, 'newsletter_voucher', Mail::l('Newsletter voucher', $this->context->language->id), array('{discount}' => $code), $email, null, null, null, null, null, $this->_mailDir, false, $this->context->shop->id);
	}
    public function teaSendConfirmationEmail($email)
	{
		return Mail::Send($this->_id_lang_email, 'newsletter_conf', Mail::l('Newsletter confirmation', $this->context->language->id), array(), pSQL($email), null, null, null, null, null, $this->_mailDir, false, $this->context->shop->id);
	}
}
