<?php

/**

* TeaThemes (c) All rights reserved.

*

* DISCLAIMER

*

* Do not edit, modify or copy this file.

* If you wish to customize it, contact us at teathemes@gmail.com.

*

* @author    TeaThemes

* @copyright 2016-2017 TeaThemes

* @license   Do not edit, modify or copy this file

*/



if (!defined('_PS_VERSION_'))

	exit;

class Tea_newsletter extends Module

{

    private $_teaErrorMessage;

    public $_teaConfigs;

    public $__teaAdminUrl;



    public function __construct()

	{

		$this->name = 'tea_newsletter';

		$this->tab = 'front_office_features';

		$this->version = '1.0.0';

		$this->author = 'prestagold.com';

		$this->need_instance = 0;

		$this->secure_key = Tools::encrypt($this->name);

		$this->bootstrap = true;



		parent::__construct();



		$this->displayName = $this->l('Prestagold Newsletter Popup');

		$this->description = $this->l('Show newsletter popup on home Page');

		$this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => _PS_VERSION_);

        if(isset($this->context->controller->controller_type) && $this->context->controller->controller_type =='admin')

            $this->__teaAdminUrl = $this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;

        $this->_files = array(

			'name' => array('newsletter_conf', 'newsletter_voucher','newsletter_verif'),

			'ext' => array(

				0 => 'html',

				1 => 'txt'

			)

		);

        //Config fields

        $this->_teaConfigs = array(
            'TEA_NEWSLETTER_DISPLAY_POPUP' => array(
                'label' => $this->l('Show newsletter popup'),
                'type' => 'switch',
                'default' => 1
            ),
            'TEA_NEWSLETTER_DISPLAY_POPUP_ONLY_HOME_PAGE' => array(
                'label' => $this->l('Only show home page'),
                'type' => 'switch',
                'default' => 1
            ),
            'TEA_NEWSLETTER_DES_LEFT' => array(
                'label' => $this->l('Title on banner'),
                'type' => 'text',
                'default' => 'Sign Up Newsletter',
				'autoload_rte' => true,
                'lang' => true,
            ),
            'TEA_NEWSLETTER_TITLE' => array(
                'label' => $this->l('Popup title'),
                'type' => 'text',
                'lang' => true,
                'default' => $this->l('SIGN UP NEWSLETTER'),
                'required' => true,
            ),
            'TEA_NEWSLETTER_DESCRIPTION' => array(
                'label' => $this->l('Popup description'),
                'type' => 'textarea',
                'default' => 'Subscribe to our newsletters and don�t miss new arrivals, the latest fashion updates and our promotions.',
				'autoload_rte' => true,
                'lang' => true,
            ),
            'TEA_NEWSLETTER_DESEXTRA' => array(
                'label' => $this->l('Popup description extra'),
                'type' => 'textarea',
                'default' => 'Your Information will never be shared with any third party.',
				'autoload_rte' => true,
                'lang' => true,
            ),
            'TEA_NEWSLETTER_IMAGE' => array(
                'label' => $this->l('Popup background image'),
                'type' => 'file',
                'default' => 'newsletter.jpg'
            ),
            'TEA_NEWSLETTER_AUTO_HIDE' => array(
                'label' => $this->l('Auto hide popup after first time customer see it'),
                'type' => 'switch',
                'default' => 0
            ),
            'TEA_NEWSLETTER_MOBILE_HIDE' => array(
                'label' => $this->l('Hide popup on mobile devices'),
                'type' => 'switch',
                'default' => 0
            ),
            'TEA_NEWSLETTER_DELAY_POPUP' => array(
                'label' => $this->l('Time delay popup'),
                'type' => 'text',
                'default' =>'1',
            ),
        );
    }

    /**

	 * @see Module::install()

	 */

    public function install()

	{

	    

        return parent::install()

        && $this->registerHook('displayHeader')

        && $this->registerHook('displayFooter')

        && $this->_teaInstallDb();

    }

    /**

	 * @see Module::uninstall()

	 */

	public function uninstall()

	{

        return parent::uninstall() && $this->_teaUninstallDb();

    }

    public function _teaInstallTbls()

    {

        return Db::getInstance()->execute('

    		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'newsletter` (

    			`id` int(6) NOT NULL AUTO_INCREMENT,

    			`id_shop` INTEGER UNSIGNED NOT NULL DEFAULT \'1\',

    			`id_shop_group` INTEGER UNSIGNED NOT NULL DEFAULT \'1\',

    			`email` varchar(255) NOT NULL,

    			`newsletter_date_add` DATETIME NULL,

    			`ip_registration_newsletter` varchar(15) NOT NULL,

    			`http_referer` VARCHAR(255) NULL,

    			`active` TINYINT(1) NOT NULL DEFAULT \'0\',

    			PRIMARY KEY(`id`)

    		) ENGINE='._MYSQL_ENGINE_.' default CHARSET=utf8');

    }

    public function _teaInstallDb()

    {

        $languages = Language::getLanguages(false);

        if($this->_teaConfigs)

        {

            foreach($this->_teaConfigs as $key => $config)

            {

                if(isset($config['lang']) && $config['lang'])

                {

                    $values = array();

                    foreach($languages as $lang)

                    {

                        $values[$lang['id_lang']] = isset($config['default']) ? $config['default'] : '';

                    }

                    Configuration::updateValue($key, $values,true);

                }

                else

                    Configuration::updateValue($key, isset($config['default']) ? $config['default'] : '',true);

            }

        }

        if(file_exists(dirname(__FILE__).'/images/temp/newsletter.png'))

            @copy(dirname(__FILE__).'/images/temp/newsletter.png', dirname(__FILE__).'/images/config/newsletter.png');

        $this->_teaInstallTbls();

        return true;

    }



    private function _teaUninstallDb()

    {

        if($this->_teaConfigs)

        {

            foreach($this->_teaConfigs as $key => $config)

            {

                Configuration::deleteByName($key);

            }

        }

        $dirs = array('config');

        foreach($dirs as $dir)

        {

            $files = glob(dirname(__FILE__).'/images/'.$dir.'/*');

            foreach($files as $file){

              if(is_file($file))

                @unlink($file);

            }

        }

        return true;

    }

    public function getContent()

	{

	   $this->_html = '';

	   $this->_teaPostConfig();

       //Display errors if have

       if($this->_teaErrorMessage)

            $this->_html .= $this->_teaErrorMessage;

       //Render views

       $this->_html .= $this->teaRenderConfig();

       return $this->_html;

    }

    public function teaRenderConfig()

    {

        $_teaConfigs = $this->_teaConfigs;

        $fields_form = array(

			'form' => array(

				'legend' => array(

					'title' => $this->l('Newsletter popup settings'),

					'icon' => 'icon-AdminAdmin'

				),

				'input' => array(),

                'submit' => array(

					'title' => $this->l('Save'),

				)

            ),

		);

        if($_teaConfigs)

        {

            foreach($_teaConfigs as $key => $config)

            {

                $confFields = array(

                    'name' => $key,

                    'type' => $config['type'],

                    'label' => $config['label'],

                    'desc' => isset($config['desc']) ? $config['desc'] : false,

                    'required' => isset($config['required']) && $config['required'] ? true : false,

                    'options' => isset($config['options']) && $config['options'] ? $config['options'] : array(),

                    'autoload_rte' => isset($config['autoload_rte']) && $config['autoload_rte']?true :false,

                    'values' => array(

							array(

								'id' => 'active_on',

								'value' => 1,

								'label' => $this->l('Yes')

							),

							array(

								'id' => 'active_off',

								'value' => 0,

								'label' => $this->l('No')

							)

						),

                    'lang' => isset($config['lang']) ? $config['lang'] : false

                );

                if($config['type'] == 'file')

                {

                    if($imageName = Configuration::get($key))

                    {

                        $confFields['display_img'] = $this->_path.'images/config/'.$imageName;

                        if(!isset($config['required']) || (isset($config['required']) && !$config['required']))

                            $confFields['img_del_link'] = $this->__teaAdminUrl.'&delimage=yes&image='.$key;

                    }

                }

                $fields_form['form']['input'][] = $confFields;

            }

        }

        $helper = new HelperForm();

		$helper->show_toolbar = false;

		$helper->table = $this->table;

		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

		$helper->default_form_language = $lang->id;

		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

		$this->fields_form = array();

		$helper->module = $this;

		$helper->identifier = $this->identifier;

		$helper->submit_action = 'saveConfig';

		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&control=config';

		$helper->token = Tools::getAdminTokenLite('AdminModules');

		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        //$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');

        $fields = array();

        $languages = Language::getLanguages(false);

        $helper->override_folder = '/';

        if(Tools::isSubmit('saveConfig'))

        {

            if($_teaConfigs)

            {

                foreach($_teaConfigs as $key => $config)

                {

                    if(isset($config['lang']) && $config['lang'])

                        {

                            foreach($languages as $l)

                            {

                                $fields[$key][$l['id_lang']] = Tools::getValue($key.'_'.$l['id_lang'],isset($config['default']) ? $config['default'] : '');

                            }

                        }

                        else

                            $fields[$key] = Tools::getValue($key,isset($config['default']) ? $config['default'] : '');

                }

            }

        }

        else

        {

            if($_teaConfigs)

            {

                    foreach($_teaConfigs as $key => $config)

                    {

                        if(isset($config['lang']) && $config['lang'])

                        {

                            foreach($languages as $l)

                            {

                                $fields[$key][$l['id_lang']] = Configuration::get($key,$l['id_lang']);

                            }

                        }

                        else

                            $fields[$key] = Configuration::get($key);

                    }

            }

        }

        $helper->tpl_vars = array(

			'base_url' => $this->context->shop->getBaseURL(),

			'language' => array(

				'id_lang' => $language->id,

				'iso_code' => $language->iso_code

			),

			'fields_value' => $fields,

			'languages' => $this->context->controller->getLanguages(),

			'id_language' => $this->context->language->id,

            'export_link' => $this->__teaAdminUrl.'&exportNewsletter=yes'

        );

       return $helper->generateForm(array($fields_form));

     }

     private function _teaPostConfig()

     {

        $errors = array();

        $languages = Language::getLanguages(false);

        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');

        $_teaConfigs = $this->_teaConfigs;

        //Export to csv

        if(Tools::isSubmit('exportNewsletter'))

        {

            $emails = $this->teaGetBlockNewsletterSubscriber();

            header('Content-Type: text/csv; charset=utf-8');

            header('Content-Disposition: attachment; filename=mailing_list.csv');

            $out = fopen('php://output', 'w');

            fputcsv($out, array($this->l('Email')));

            if($emails)

            {

                  foreach($emails as $emai)

                    fputcsv($out, array((string)$emai['email']));

            }

			fclose($out);

            die;

        }

        //Delete image

        if(Tools::isSubmit('delimage'))

        {

            $image = Tools::getValue('image');

            if(isset($_teaConfigs[$image]) && !isset($_teaConfigs[$image]['required']) || (isset($_teaConfigs[$image]['required']) && !$_teaConfigs[$image]['required']))

            {

                $imageName = Configuration::get($image);

                $imagePath = dirname(__FILE__).'/images/config/'.$imageName;

                if($imageName && file_exists($imagePath))

                {

                    @unlink($imagePath);

                    Configuration::updateValue($image,'');

                }

                Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);

            }

            else

                $errors[] = $_teaConfigs[$image]['label'].$this->l(' is required');

        }

        if(Tools::isSubmit('saveConfig'))

        {

            if($_teaConfigs)

            {

                foreach($_teaConfigs as $key => $config)

                {

                    if(isset($config['lang']) && $config['lang'])

                    {

                        if(isset($config['required']) && $config['required'] && $config['type']!='switch' && trim(Tools::getValue($key.'_'.$id_lang_default) == ''))

                        {

                            $errors[] = $config['label'].' '.$this->l('is required');

                        }

                    }

                    else

                    {

                        if(isset($config['required']) && $config['required'] && isset($config['type']) && $config['type']=='file')

                        {

                            if(Configuration::get($key)=='' && !isset($_FILES[$key]['size']))

                                $errors[] = $config['label'].' '.$this->l('is required');

                            elseif(isset($_FILES[$key]['size']))

                            {

                                $fileSize = round((int)$_FILES[$key]['size'] / (1024 * 1024));

                    			if($fileSize > 100)

                                    $errors[] = $config['label'].$this->l(' can not be larger than 100Mb');

                            }

                        }

                        else

                        {

                            if(isset($config['required']) && $config['required'] && $config['type']!='switch' && trim(Tools::getValue($key) == ''))

                            {

                                $errors[] = $config['label'].' '.$this->l('is required');

                            }

                            elseif(!Validate::isCleanHtml(trim(Tools::getValue($key))))

                            {

                                $errors[] = $config['label'].' '.$this->l('is invalid');

                            }

                        }

                    }

                }

            }



            //Custom validation



            if(!$errors)

            {

                if($_teaConfigs)

                {

                    foreach($_teaConfigs as $key => $config)

                    {

                        if(isset($config['lang']) && $config['lang'])

                        {

                            $valules = array();

                            foreach($languages as $lang)

                            {

                                if($config['type']=='switch')

                                    $valules[$lang['id_lang']] = (int)trim(Tools::getValue($key.'_'.$lang['id_lang'])) ? 1 : 0;

                                else

                                    $valules[$lang['id_lang']] = trim(Tools::getValue($key.'_'.$lang['id_lang'])) ? trim(Tools::getValue($key.'_'.$lang['id_lang'])) : trim(Tools::getValue($key.'_'.$id_lang_default));

                            }

                            Configuration::updateValue($key,$valules,true);

                        }

                        else

                        {

                            if($config['type']=='switch')

                            {

                                Configuration::updateValue($key,(int)trim(Tools::getValue($key)) ? 1 : 0,true);

                            }

                            if($config['type']=='file')

                            {

                                //Upload file

                                if(isset($_FILES[$key]['tmp_name']) && isset($_FILES[$key]['name']) && $_FILES[$key]['name'])

                                {

                                    $salt = sha1(microtime());

                                    $type = Tools::strtolower(Tools::substr(strrchr($_FILES[$key]['name'], '.'), 1));

                                    $imageName = $salt.'.'.$type;

                                    $fileName = dirname(__FILE__).'/images/config/'.$imageName;

                                    if(file_exists($fileName))

                                    {

                                        $errors[] = $config['label'].$this->l(' already exists. Try to rename the file then reupload');

                                    }

                                    else

                                    {



                            			$imagesize = @getimagesize($_FILES[$key]['tmp_name']);



                                        if (!$errors && isset($_FILES[$key]) &&

                            				!empty($_FILES[$key]['tmp_name']) &&

                            				!empty($imagesize) &&

                            				in_array($type, array('jpg', 'gif', 'jpeg', 'png'))

                            			)

                            			{

                            				$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');

                            				if ($error = ImageManager::validateUpload($_FILES[$key]))

                            					$errors[] = $error;

                            				elseif (!$temp_name || !move_uploaded_file($_FILES[$key]['tmp_name'], $temp_name))

                            					$errors[] = $this->l('Can not upload the file');

                            				elseif (!ImageManager::resize($temp_name, $fileName, null, null, $type))

                            					$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));

                            				if (isset($temp_name))

                            					@unlink($temp_name);

                                            if(!$errors)

                                            {

                                                if(Configuration::get($key)!='')

                                                {

                                                    $oldImage = dirname(__FILE__).'/images/config/'.Configuration::get($key);

                                                    if(file_exists($oldImage))

                                                        @unlink($oldImage);

                                                }

                                                Configuration::updateValue($key, $imageName);

                                            }

                                        }

                                    }

                                }

                                //End upload file

                            }

                            else

                                Configuration::updateValue($key,trim(Tools::getValue($key)),true);

                        }

                    }

                }

            }

            if (count($errors))

            {

               $this->_teaErrorMessage = $this->displayError(implode('<br />', $errors));

            }

            else

               Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);

        }

     }

     public function hookDisplayFooter()

     {

          if(!Configuration::get('TEA_NEWSLETTER_DISPLAY_POPUP') || $this->context->cookie->teanewsletter)
            return;
          if(Configuration::get('TEA_NEWSLETTER_DISPLAY_POPUP_ONLY_HOME_PAGE')==1 && Tools::getValue('controller')!='index')
            return;
          if((int)Configuration::get('TEA_NEWSLETTER_AUTO_HIDE'))
            $this->context->cookie->teanewsletter = 'subcribed';
          $this->smarty->assign(array(
            'TEA_NEWSLETTER_TITLE' => Configuration::get('TEA_NEWSLETTER_TITLE', (int)$this->context->language->id),
            'TEA_NEWSLETTER_DESCRIPTION' => Configuration::get('TEA_NEWSLETTER_DESCRIPTION', (int)$this->context->language->id),
            'TEA_NEWSLETTER_DESEXTRA' => Configuration::get('TEA_NEWSLETTER_DESEXTRA', (int)$this->context->language->id),
            'TEA_NEWSLETTER_DES_LEFT' => Configuration::get('TEA_NEWSLETTER_DES_LEFT', (int)$this->context->language->id),
            'TEA_NEWSLETTER_IMAGE' => Configuration::get('TEA_NEWSLETTER_IMAGE') ? $this->_path.'images/config/'.Configuration::get('TEA_NEWSLETTER_IMAGE') : false,
            'TEA_NEWSLETTER_ACTION' => $this->context->link->getModuleLink('tea_newsletter', 'submit'),
            'TEA_NEWSLETTER_LOADING_IMG' => $this->_path.'images/icon/loading.gif',
            'TEA_NEWSLETTER_MOBILE_HIDE' => (int)Configuration::get('TEA_NEWSLETTER_MOBILE_HIDE') ? true : false,
            'TEA_NEWSLETTER_AUTO_HIDE' => (int)Configuration::get('TEA_NEWSLETTER_AUTO_HIDE') ? true : false,
            'TEA_NEWSLETTER_DELAY_POPUP' =>(float)Configuration::get('TEA_NEWSLETTER_DELAY_POPUP')?(float)Configuration::get('TEA_NEWSLETTER_DELAY_POPUP'):1,
          ));
          return $this->display(__FILE__, 'popup.tpl');
     }

     public function hookDisplayHeader()

     {

        if(!Configuration::get('TEA_NEWSLETTER_DISPLAY_POPUP') || $this->context->cookie->teanewsletter)

            return;

        $this->context->controller->addCSS($this->_path.'css/newsletter.css','all');

        $this->context->controller->addJS($this->_path.'js/newsletter.js');

     }

     public function teaGetBlockNewsletterSubscriber()

	 {

		$rq_sql = 'SELECT `id`, `email`, `newsletter_date_add`, `ip_registration_newsletter`

			FROM `'._DB_PREFIX_.'newsletter`

			WHERE `active` = 1';



		if (Context::getContext()->cookie->shopContext)

			$rq_sql .= ' AND `id_shop` = '.(int)Context::getContext()->shop->id;



		$rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($rq_sql);



		return $rq;

	 }

}

