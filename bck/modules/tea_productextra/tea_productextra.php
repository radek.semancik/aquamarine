<?php
if (!defined('_PS_VERSION_'))
	exit;
class Tea_productextra extends Module
{
	private $_hooks = array(
        'displayBackOfficeHeader',
        'header',
        'displayAdminProductsExtra',
        'actionProductUpdate',
        'displayProductExtra',
        'displayVideoTab',
        'productWishlist'
    );
    private $errorMessage;
    public $configs;
    public $baseAdminPath;
    private $_html;
    private $teamplates;
    protected $fields_config=array();
    public function __construct()
	{
	    $this->name = 'tea_productextra';
		$this->tab = 'front_office_features';
		$this->version = '1.0.1';
		$this->author = 'prestagold.com';
		$this->need_instance = 0;
	 	parent::__construct();
		$this->displayName = $this->l('Prestagold product extra');
		$this->description = $this->l('Prestagold product extra');
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall module?');
        //Config fields
        $this->bootstrap = true;
    }
    public function install()
	{
		if (!parent::install()||!$this->installDB() )
			return false;
		foreach ($this->_hooks as $hook) {
            if(!$this->registerHook($hook)) return false;
        }
		return true;
	}
    public function uninstall()
	{
		if (!parent::uninstall()|| !$this->uninstallDb()) return false;
		return true;
	}
    public function installDb()
	{
	   $res = Db::getInstance ()->Execute ('DROP TABLE IF EXISTS `'._DB_PREFIX_.'product_extra`')
			&& Db::getInstance ()->Execute ('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_extra` (
			`id_product_extra` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`id_product` varchar(20) NOT NULL, 
			`is_size_guide` tinyint(1) NOT NULL DEFAULT \'1\',
            `is_video` tinyint(1) NOT NULL,
            `is_external_affiliate_product` tinyint(1) NOT NULL,
            `link_external` VARCHAR(1000),
			PRIMARY KEY (`id_product_extra`)) ENGINE=InnoDB default CHARSET=utf8')&&Db::getInstance ()->Execute ('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_extra_lang` (
			`id_product_extra` int(10),
			`id_lang` int(22) NOT NULL, 
			`size_guide_content` text NOT NULL DEFAULT \'\' ,
            `video` text NOT  NULL,
            `title_external` VARCHAR(222),
			PRIMARY KEY (`id_product_extra`, `id_lang`)) ENGINE=InnoDB default CHARSET=utf8');
	   return true;
    }
    private function uninstallDb()
	{
	   return true;
	}
    public function getContent()
	{
	   if(Tools::isSubmit('settingsizeguide'))
       {
            $languages = Language::getLanguages(false);
            $valules=array();
            foreach($languages as $lang)
            {
                $valules[$lang['id_lang']] = trim(Tools::getValue('TEA_SIZE_GUIZECONTENT'.$lang['id_lang']));
            }
            Configuration::updateValue('TEA_SIZE_GUIZECONTENT',$valules,true);
       }
       $this->_html .= $this->renderForm();
       return $this->_html;
    }
    public function hookProductWishlist($params)
    {
        $id_product = $params['product']['id_product'];
        $product_extra = Db::getInstance()->getRow('SELECT size_guide_content,title_external,link_external,is_external_affiliate_product,is_size_guide FROM '._DB_PREFIX_.'product_extra p, '._DB_PREFIX_.'product_extra_lang pl where p.id_product_extra=pl.id_product_extra and p.id_product='.(int)$id_product.' and pl.id_lang='.(int)$this->context->language->id);
        if(!isset($product_extra['size_guide_content']) || !$product_extra['size_guide_content'])
            $product_extra['size_guide_content'] = Configuration::get('TEA_SIZE_GUIZECONTENT',$this->context->language->id);
        $this->context->smarty->assign('product_extra',$product_extra);
        return $this->display(__FILE__,'product_buttom.tpl');
    }
    public function hookDisplayAdminProductsExtra($params)
    {
        $languages = Language::getLanguages(false);
        $input_value=array(
            'size_guide_content'=>array(),
            'video'=>array(),
            'title_external' =>array(),
        );
        if($languages && $id_product_extra= (int)Db::getInstance()->getValue('SELECT id_product_extra FROM '._DB_PREFIX_.'product_extra WHERE id_product='.(int)$params['id_product']))
        {
            foreach($languages as $language)
            {
                $product_extra = Db::getInstance()->getRow('SELECT size_guide_content,video,title_external FROM '._DB_PREFIX_.'product_extra_lang where id_lang='.(int)$language['id_lang'].' AND id_product_extra='.(int)$id_product_extra);
                $input_value['size_guide_content'][$language['id_lang']]==$product_extra['size_guide_content'];
                $input_value['video'][$language['id_lang']]=$product_extra['video'];
                $input_value['title_external'][$language['id_lang']] = $product_extra['title_external'];
            }
        }    
        $this->context->smarty->assign(
            array(
                'id_product'=>$params['id_product'],
                'input_value' =>$input_value,
                'class' =>'autoload_rte',
                'is_size_guide' => (int)Db::getInstance()->getValue('SELECT is_size_guide FROM '._DB_PREFIX_.'product_extra where id_product='.(int)$params['id_product']),
                'is_video' => (int)Db::getInstance()->getValue('SELECT is_video FROM '._DB_PREFIX_.'product_extra where id_product='.(int)$params['id_product']),
                'is_external_affiliate_product' => (int)Db::getInstance()->getValue('SELECT is_external_affiliate_product FROM '._DB_PREFIX_.'product_extra where id_product='.(int)$params['id_product']),
                'link_external' => Db::getInstance()->getValue('SELECT link_external FROM '._DB_PREFIX_.'product_extra where id_product='.(int)$params['id_product']),
                'languages'=>Language::getLanguages(false),
                'module_extra_dir' => $this->context->link->getBaseLink().'modules/'.$this->name,
            )
        );
        return $this->display(__FILE__,'productTab.tpl');
    }
    public function hookActionProductUpdate($params)
    {
       $id_product=$params['id_product'];
       
       $display_sizeguide=(int)Tools::getValue('display_sizeguide');
       $display_video = (int)Tools::getValue('display_video');
       if($id_product_extra= (int)Db::getInstance()->getValue('SELECT id_product_extra FROM '._DB_PREFIX_.'product_extra WHERE id_product='.(int)$params['id_product']))
       {
            
            Db::getInstance()->execute('update '._DB_PREFIX_.'product_extra set `is_size_guide`="'.(int)$display_sizeguide.'",`is_video`="'.(int)$display_video.'",`is_external_affiliate_product`="'.(int)Tools::getValue('external_affiliate_product').'",link_external ="'.pSQL(Tools::getValue('link_external')).'" where id_product='.(int)$params['id_product']);
            
            $languages = Language::getLanguages(false);
            foreach($languages as $language){
                if(Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'product_extra_lang where id_product_extra='.(int)$id_product_extra.' AND id_lang='.(int)$language['id_lang']))
                {
                    Db::getInstance()->execute('update '._DB_PREFIX_.'product_extra_lang set title_external="'.pSQL(Tools::getValue('title_external_'.$language['id_lang'])).'", size_guide_content="'.addslashes(Tools::getValue('size_guide_content_'.$language['id_lang'])).'", video="'.addslashes(Tools::getValue('product_video_'.$language['id_lang'])).'" where id_product_extra='.(int)$id_product_extra.' AND id_lang='.(int)$language['id_lang']);
                }
                else
                {
                    Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'product_extra_lang (id_product_extra,id_lang,size_guide_content,video,link_external) values("'.$id_product_extra.'","'.$language['id_lang'].'","'.addslashes(Tools::getValue('size_guide_content_'.$language['id_lang'])).'","'.addslashes(Tools::getValue('product_video_'.$language['id_lang'])).'","'.pSQL(Tools::getValue('title_external_'.$language['id_lang'])).'")');
                }
            }
       }
       else
       {
            Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'product_extra (id_product,`is_size_guide`,`is_video`,`is_external_affiliate_product`,`link_external`) VALUES ("'.(int)Tools::getValue('id_product').'","'.(int)$display_sizeguide.'","'.(int)$display_video.'","'.(int)Tools::getValue('is_external_affiliate_product').'","'.pSQL(Tools::getValue('link_external')).'")');
            $id_product_extra=Db::getInstance()->Insert_ID();
            $languages = Language::getLanguages(false);
            foreach($languages as $language){
                if(Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'product_extra_lang where id_product_extra='.(int)$id_product_extra.' AND id_lang='.(int)$language['id_lang']))
                {
                    Db::getInstance()->execute('update '._DB_PREFIX_.'product_extra_lang SET title_external="'.pSQL(Tools::getValue('title_external_'.$language['id_lang'])).'", size_guide_content='.addslashes(Tools::getValue('size_guide_content_'.$language['id_lang'])).',`video`="'.addslashes(Tools::getValue('product_video_'.$language['id_lang'])).'" where id_product_extra='.(int)$id_product_extra.' AND id_lang='.(int)$language['id_lang']);
                }
                else
                {
                    Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'product_extra_lang (`id_product_extra`,`id_lang`,`size_guide_content`,`video`,`title_external`) values("'.$id_product_extra.'","'.$language['id_lang'].'","'.addslashes(Tools::getValue('size_guide_content_'.$language['id_lang'])).'","'.addslashes(Tools::getValue('product_video_'.$language['id_lang'])).'","'.pSQL('title_external_'.$language['id_lang']).'")');
                }
            }
       }
    }
    public function hookDisplayProductExtra($params)
    {
        $id_product = $params['id_product'];
        $product_extra = Db::getInstance()->getRow('SELECT size_guide_content,title_external,link_external,is_external_affiliate_product,is_size_guide FROM '._DB_PREFIX_.'product_extra p, '._DB_PREFIX_.'product_extra_lang pl where p.id_product_extra=pl.id_product_extra and p.id_product='.(int)$id_product.' and pl.id_lang='.(int)$this->context->language->id);
        if(!isset($product_extra['size_guide_content']) || !$product_extra['size_guide_content'])
            $product_extra['size_guide_content'] = Configuration::get('TEA_SIZE_GUIZECONTENT',$this->context->language->id);
        $this->context->smarty->assign('product_extra',$product_extra);
        return $this->display(__FILE__,'product_extra.tpl');
    } 
    public function hookDisplayVideoTab($params)
    {
        $id_product = $params['id_product'];
        if((int)Db::getInstance()->getValue('SELECT is_video FROM '._DB_PREFIX_.'product_extra WHERE id_product="'.(int)$id_product.'"'))
        {
            $key_video = Db::getInstance()->getValue('SELECT video  FROM '._DB_PREFIX_.'product_extra p, '._DB_PREFIX_.'product_extra_lang pl where p.id_product_extra=pl.id_product_extra and p.id_product='.(int)$id_product.' and pl.id_lang='.(int)$this->context->language->id);
            if(!$key_video)
                return '';
            else
            $this->context->smarty->assign('key_video',$key_video);
            return $this->display(__FILE__,'product_video.tpl');
        }
        return '';
    }
    public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'textarea',
						'label' => $this->l('Size guide content'),
						'name' => 'TEA_SIZE_GUIZECONTENT',
                        'lang' => true,
                        'autoload_rte' => true,
					)
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'settingsizeguide';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path.'views/img/images/config'
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
        $languages = Language::getLanguages(false);
        if(Tools::isSubmit('settingsizeguide'))
        {
            foreach($languages as $l)
            {
                $fields['TEA_SIZE_GUIZECONTENT'][$l['id_lang']] = Tools::getValue('TEA_SIZE_GUIZECONTENT'.$l['id_lang']);
            }
        }
        else
        {
            foreach($languages as $l)
            {
                $fields['TEA_SIZE_GUIZECONTENT'][$l['id_lang']] = Configuration::get('TEA_SIZE_GUIZECONTENT',$l['id_lang']);
            }
        }
        return $fields;
	}   
}