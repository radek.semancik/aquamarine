<div class="p-video pa" onclick="myFunction()">
	<span class="view_popup_video">
        <i class="pe-7s-play pr"></i>{l s='View Video' d='Shop.Theme'}
    </span>		
</div>
<div class="video_product_popup">
    <div class="video_product_popup_content">
        <div class="video_product_popup_table">
            <div class="video_product_popup_table_cell">
                <div class="close_popup" onclick="stopVideo()">{l s='Close' mod='tea_productextra'}</div>
                <div id="player"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"> 
    var key_video ='{$key_video}';
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  
  var player;
  function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
      height: '100%',
      width: '100%',
      playerVars: {
                autoplay: 0,
                loop: 1,
                controls: 0,
                showinfo: 0,
                autohide: 1,
                modestbranding: 1,
                playlist: key_video,
                vq: 'hd1080'},                   
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    });
  }

  
  function onPlayerReady(event) {
    event.target.playVideo();
    player.mute();
  }

  function onPlayerReady(event) {
	  var player = event.target;
	 
 }



function myFunction()
{
     player.playVideo();
     $('.video_product_popup').addClass('show');
}
  var done = false;
  function onPlayerStateChange(event) {
    
  }
  function stopVideo() {
    player.stopVideo();
    $('.video_product_popup').removeClass('show');
  }
//  function onclick()
//  {
//	 player.playVideo();
//  }
  {literal}
  $(document).ready(function(){
    $(document).mouseup(function (e)
    {
        var container_pre_message = $('.video_product_popup_content');
        if (!container_pre_message.is(e.target)&& container_pre_message.has(e.target).length === 0)
        {
           stopVideo();
        }
    });
    $(document).keypress(function(e) { 
        if(e.which == 0) {
          stopVideo();
        }
    });
  });
  {/literal}
 </script>