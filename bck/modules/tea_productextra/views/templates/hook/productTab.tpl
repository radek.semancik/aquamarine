<input  type="hidden" name="id_product_extra"/>
<div class="form-group">
    <label class="control-label col-lg-4">
        <span class="label-tooltip" title="" > {l s='Display size giude in product page' mod='tea_productsextra'} </span>
    </label>
    <div class="col-lg-8">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" {if $is_size_guide} checked="checked" {/if} value="1" id="display_sizeguide_on" name="display_sizeguide" />
			<label for="display_sizeguide_on">{l s='Yes' mod='tea_productsextra'}</label>
			<input type="radio" {if !$is_size_guide} checked="checked" {/if} value="0" id="display_sizeguide_off" name="display_sizeguide" />
			<label for="display_sizeguide_off">{l s='No' mod='tea_productsextra'}</label>
			<a class="slide-button btn"></a>
		</span>								
	</div>
</div>
<div class="form-group tab_size_giude">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" title="" > {l s='Size guide content' mod='tea_productsextra'} </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
                <div class="col-lg-9">
    			{foreach from=$languages item=language}
                	{if $languages|count > 1}
                		<div class="translatable-field row lang-{$language.id_lang}">
                			<div class="col-lg-9">
                	{/if}
                	{if isset($maxchar) && $maxchar}
        				<div class="input-group">
        					<span id="size_guide_content_{$language.id_lang}" class="input-group-addon">
      						<span class="text-count-down">{$maxchar|intval}</span>
        					</span>
                	{/if}
        			 <textarea id="size_guide_content_{$language.id_lang}" name="size_guide_content_{$language.id_lang}" class="{if isset($class)}{$class}{else}textarea-autosize{/if}"{if isset($maxlength) && $maxlength} maxlength="{$maxlength|intval}"{/if}{if isset($maxchar) && $maxchar} data-maxchar="{$maxchar|intval}"{/if}>{if isset($input_value['size_guide_content'][$language.id_lang])}{$input_value['size_guide_content'][$language.id_lang]|htmlentitiesUTF8}{/if}</textarea>
        			 <span class="counter" data-max="{if isset($max)}{$max|intval}{/if}{if isset($maxlength)}{$maxlength|intval}{/if}{if !isset($max) && !isset($maxlength)}none{/if}"></span>
                    {if isset($maxchar) && $maxchar}
                    </div>
                    {/if}
                	{if $languages|count > 1}
                			</div>
                			<div class="col-lg-2">
                				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                					{$language.iso_code}
                					<span class="caret"></span>
                				</button>
                				<ul class="dropdown-menu">
                					{foreach from=$languages item=language}
                					<li><a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a></li>
                					{/foreach}
                				</ul>
                			</div>
                		</div>
                	{/if}
                {/foreach}   
            </div>        
    		</span>								
    </div>
</div>
<div class="form-group">
    <label class="control-label col-lg-4">
        <span class="label-tooltip" title="" > {l s='Display video in product page' mod='tea_productsextra'} </span>
    </label>
    <div class="col-lg-8">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" {if $is_video} checked="checked" {/if} value="1" id="display_video_on" name="display_video" />
			<label for="display_video_on">{l s='Yes' mod='tea_productsextra'}</label>
			<input type="radio" {if !$is_video} checked="checked" {/if} value="0" id="display_video_off" name="display_video" />
			<label for="display_video_off">{l s='No' mod='tea_productsextra'}</label>
			<a class="slide-button btn"></a>
		</span>								
	</div>
</div>
<div class="form-group tab_video">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" title="" > {l s='Video ID' mod='tea_productsextra'} </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <div class="col-lg-9">
    			{foreach from=$languages item=language}
                	{if $languages|count > 1}
                		<div class="translatable-field row lang-{$language.id_lang}">
                			<div class="col-lg-9">
                	{/if}
                	{if isset($maxchar) && $maxchar}
        				<div class="input-group">
        					<span id="product_video_{$language.id_lang}" class="input-group-addon">
      						<span class="text-count-down">{$maxchar|intval}</span>
        					</span>
                	{/if}
                    <input type="text" id="product_video_{$language.id_lang}" name="product_video_{$language.id_lang}"  value="{if isset($input_value['video'][$language.id_lang])}{$input_value['video'][$language.id_lang]|htmlentitiesUTF8}{/if}" style="border: 1px solid rgb(204, 204, 204); width: 352px; height: 35px;"/>
        			 <span class="counter" data-max="{if isset($max)}{$max|intval}{/if}{if isset($maxlength)}{$maxlength|intval}{/if}{if !isset($max) && !isset($maxlength)}none{/if}"></span>
                    {if isset($maxchar) && $maxchar}
                    </div>
                    {/if}
                	{if $languages|count > 1}
                			</div>
                			<div class="col-lg-2">
                				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                					{$language.iso_code}
                					<span class="caret"></span>
                				</button>
                				<ul class="dropdown-menu">
                					{foreach from=$languages item=language}
                					<li><a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a></li>
                					{/foreach}
                				</ul>
                			</div>
                		</div>
                	{/if}
                {/foreach} 
             </div>          
  		</span>								
    </div>
</div>
<div class="form-group">
    <label class="control-label col-lg-4">
        <span class="label-tooltip" title="{l s='External/Affiliate product' mod='tea_productsextra'} " > {l s='External/Affiliate product' mod='tea_productsextra'} </span>
    </label>
    <div class="col-lg-8">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" {if $is_external_affiliate_product} checked="checked" {/if} value="1" id="external_affiliate_product_on" name="external_affiliate_product" />
			<label for="external_affiliate_product_on">{l s='Yes' mod='tea_productsextra'}</label>
			<input type="radio" {if !$is_external_affiliate_product} checked="checked" {/if} value="0" id="external_affiliate_product_off" name="external_affiliate_product" />
			<label for="external_affiliate_product_off">{l s='No' mod='tea_productsextra'}</label>
			<a class="slide-button btn"></a>
		</span>								
	</div>
</div>
<div class="form-group tab_external">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" title="{l s='Title external' mod='tea_productsextra'}" > {l s='Title external' mod='tea_productsextra'} </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <div class="col-lg-9">
    			{foreach from=$languages item=language}
                	{if $languages|count > 1}
                		<div class="translatable-field row lang-{$language.id_lang}">
                			<div class="col-lg-9">
                	{/if}
                	{if isset($maxchar) && $maxchar}
        				<div class="input-group">
        					<span id="title_external_{$language.id_lang}" class="input-group-addon">
      						<span class="text-count-down">{$maxchar|intval}</span>
        					</span>
                	{/if}
                    <input type="text" id="title_external_{$language.id_lang}" name="title_external_{$language.id_lang}"  value="{if isset($input_value['title_external'][$language.id_lang])}{$input_value['title_external'][$language.id_lang]|htmlentitiesUTF8}{/if}" style="border: 1px solid rgb(204, 204, 204); width: 352px; height: 35px;"/>
        			 <span class="counter" data-max="{if isset($max)}{$max|intval}{/if}{if isset($maxlength)}{$maxlength|intval}{/if}{if !isset($max) && !isset($maxlength)}none{/if}"></span>
                    {if isset($maxchar) && $maxchar}
                    </div>
                    {/if}
                	{if $languages|count > 1}
                			</div>
                			<div class="col-lg-2">
                				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                					{$language.iso_code}
                					<span class="caret"></span>
                				</button>
                				<ul class="dropdown-menu">
                					{foreach from=$languages item=language}
                					<li><a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a></li>
                					{/foreach}
                				</ul>
                			</div>
                		</div>
                	{/if}
                {/foreach} 
             </div>          
  		</span>								
    </div>
</div>
<div class="form-group tab_external">
    <label class="control-label col-lg-3" for="link_external">
        <span class="label-tooltip" title="{l s='External/Affiliate product link' mod='tea_productsextra'} " > {l s='External/Affiliate product link' mod='tea_productsextra'} </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <div class="col-lg-9">
                <input type="text" name="link_external" id="link_external" value="{$link_external}" style="border: 1px solid rgb(204, 204, 204); width: 352px; height: 35px;" />
            </div>
		</span>								
	</div>
</div>
<script type="text/javascript">
hideOtherLanguage({Configuration::get('PS_LANG_DEFAULT')});
$(document).ready(function(){
   if($('input[name="display_sizeguide"]:checked').val()==1)
   {
        $('.form-group.tab_size_giude').show();
   }
   else
   {
        $('.form-group.tab_size_giude').hide();
   }
   $(document).on('click','input[name="display_sizeguide"]',function(){
        if($('input[name="display_sizeguide"]:checked').val()==1)
        {
            $('.form-group.tab_size_giude').show();
        }
        else
        {
            $('.form-group.tab_size_giude').hide();
        }
   });
   if($('input[name="display_video"]:checked').val()==1)
   {
        $('.form-group.tab_video').show();
   }
   else
   {
        $('.form-group.tab_video').hide();
   }
   $(document).on('click','input[name="display_video"]',function(){
        if($('input[name="display_video"]:checked').val()==1)
        {
            $('.form-group.tab_video').show();
        }
        else
        {
            $('.form-group.tab_video').hide();
        }
   }); 
   if($('input[name="external_affiliate_product"]:checked').val()==1)
   {
        $('.form-group.tab_external').show();
   }
   else
   {
        $('.form-group.tab_external').hide();
   }
   $(document).on('click','input[name="external_affiliate_product"]',function(){
        if($('input[name="external_affiliate_product"]:checked').val()==1)
        {
            $('.form-group.tab_external').show();
        }
        else
        {
            $('.form-group.tab_external').hide();
        }
   });
});
</script>
<style>
#module_tea_productextra .form-group {
  float: left;
  width: 100%;
}
</style>