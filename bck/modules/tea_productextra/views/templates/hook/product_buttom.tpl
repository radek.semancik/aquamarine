{if isset($product_extra.is_external_affiliate_product)&& $product_extra.is_external_affiliate_product}
    <style>
    .product-add-to-cart .add-to-cart{
        display:none;
    }
    </style>
    <a target="_blank" class="btn btn-primary add-to-cart_extra" href="{$product_extra.link_external}">{$product_extra.title_external}</a>
{/if}