
{if isset($product_extra.is_size_guide) && $product_extra.is_size_guide && $product_extra.size_guide_content}
<div class="panel-product-line">
    <a class="sizeguide-product" data-toggle="modal" data-target="#sizeguide-modal">{l s='Size Guide ' d='Shop.Theme.Catalog'} </a>
</div>
<div id="sizeguide-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <button type="button" class="close close_size_guide" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                {$product_extra.size_guide_content nofilter}
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
{/if}