<div class="panel-box">
    <h2 class="panel-box-title">{l s='Site Search' mod='smartblog'}</h2>
    <div class="panel-box-search">
        <form class="std" method="post" action="{smartblog::GetSmartBlogLink('smartblog_search')}">
			<input type="hidden" value="0" name="smartblogaction" />
			<input type="text" class="form-control grey panel-box-control" value="" name="smartsearch" id="search_query" placeholder="{l s='Search post' mod='smartblog'}" />
            <button class="btn btn-default button button-small panel-box-find" value="OK" name="smartblogsubmit" type="submit">
                <span><i class="fa fa-search"></i></span>
            </button>
        </form>
    </div>
</div>
{if $posts_new}
<div class="panel-box">
    <h2 class="panel-box-title">{l s='Recent Post' mod='smartblog'}</h2>
    {foreach from=$posts_new item='post'}
        <article class="panel-box-article">
            <a class="panel-box-media" href="{$post.link}">
                <img width="70" height="70" alt="" src="{$modules_dir}/smartblog/images/{$post.post_img}-single-default.jpg" />
            </a>
            <h3 class="panel-box-name">
                <a href="{$post.link}">{$post.title} <span>{$post.date_added}</span></a>
            </h3>
        </article>
    {/foreach}
</div>
{/if}
{if $blog_categories}
    <div class="panel-box">
        <h2 class="panel-box-title">{l s='Blog Categories' mod='smartblog'}</h2>
        <nav class="panel-box-nav">
            <ul class="nav-sidebar">
                {foreach from=$blog_categories item='category'}
                    <li>
                        <a href="{$category.link}">{$category.meta_title}<span>({$category.count_post})</span></a>
                    </li>
                {/foreach}
            </ul>
        </nav>
    </div>
{/if}
{if $tags}
        <div class="panel-box">
            <h2 class="panel-box-title">Tag</h2>
            <nav class="panel-box-nav">
                <ul class="nav-tags">
                    {foreach from=$tags item='tag'}
                        <li>
                            <a href="{$tag.link}">{$tag.name}</a>
                        </li>
                    {/foreach}
                </ul>
            </nav>
        </div>
{/if}