{extends file='page.tpl'}
{block name='breadcrumb'}
    <section data-depth="{$breadcrumb.count}" class="breadcrumb panel-title">
        <div class="container">
            <h2 class="panel-title-heading">{l s='Search Blog by' mod='smartblog'} {$keyword}</h2>
            <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="nav-breadcrumbs">
            {foreach from=$breadcrumb.links item=path name=breadcrumb}
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="{$path.url}">
                  <span itemprop="name">{$path.title}</span>
                </a>
                <meta itemprop="position" content="{$smarty.foreach.breadcrumb.iteration}" />
              </li>
            {/foreach}
            </ul>
        </div>
    </section>
{/block}
{block name="page_content"}
<div class="row">
    {if $smartshowcolumn==1}
        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
            {hook h='displayColumLeftBlog'}
        </div>
    {/if}
    <div id="content-wrapper" class="{if $smartshowcolumn==1}left-column{/if}{if $smartshowcolumn==2}right-column{/if} col-xs-12 {if $smartshowcolumn}col-sm-8 col-md-9{/if}">
        {if $title_category != ''}
            <span class="navigation-pipe">{if isset($navigationPipe)}{$navigationPipe}{/if}</span>{$title_category}
        {/if}
        {if $postcategory == ''}
            {include file="module:smartblog/views/templates/front/search-not-found.tpl" postcategory=$postcategory}
        {else}
            <div id="smartblogcat" class="block">
                {foreach from=$postcategory item=post}
                    {if isset($option_type_blog) && $option_type_blog == 'list_miniture'}
                        {include file="module:smartblog/views/templates/front/category_loop.tpl"}
                    {elseif isset($option_type_blog) && $option_type_blog == 'grid'}
                        {include file="module:smartblog/views/templates/front/category_loop_grid.tpl"}
                    {else}
                        {include file="module:smartblog/views/templates/front/category_loop_list.tpl"}
                    {/if}
                {/foreach}
            </div>
        {/if}
     </div>
     {if $smartshowcolumn==2}
        <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
            {hook h='displayColumRightBlog'}
        </div>
    {/if}
</div>
 {if isset($smartcustomcss)}
    <style>
        {$smartcustomcss}
    </style>
{/if}

{/block}