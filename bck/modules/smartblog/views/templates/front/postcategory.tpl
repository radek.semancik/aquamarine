{extends file='page.tpl'}
{block name='breadcrumb'}
    <section data-depth="{$breadcrumb.count}" class="breadcrumb panel-title">
        <div class="container">
            <h2 class="panel-title-heading">{if $id_category}{$title_category}{else}{l s='All Blog News' mod='smartblog'}{/if}</h2>
          <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="nav-breadcrumbs">
            {foreach from=$breadcrumb.links item=path name=breadcrumb}
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="{$path.url}">
                  <span itemprop="name">{$path.title}</span>
                </a>
                <meta itemprop="position" content="{$smarty.foreach.breadcrumb.iteration}" />
              </li>
            {/foreach}
          </ul>
        </div>
    </section>
{/block}
{block name="page_content"}
    {if $smartshowcolumn==1}
        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
            {hook h='displayColumLeftBlog'}
        </div>
    {/if}
    <div id="content-wrapper" class="{if $smartshowcolumn==1}left-column{/if}{if $smartshowcolumn==2}right-column{/if} col-xs-12 {if $smartshowcolumn}col-sm-8 col-md-9{/if}">
        {if $postcategory == ''}
            {if $title_category != ''}
                 <p class="error">{l s='No Post in Category' mod='smartblog'}</p>
            {else}
                 <p class="error">{l s='No Post in Blog' mod='smartblog'}</p>
            {/if}
        {else}
        <div id="smartblogcat" class="block sss">
            {foreach from=$postcategory item=post}
                {if isset($option_type_blog) && $option_type_blog == 'list_miniture'}
                    {include file="module:smartblog/views/templates/front/category_loop.tpl"}
                {elseif isset($option_type_blog) && $option_type_blog == 'grid'}
                    {include file="module:smartblog/views/templates/front/category_loop_grid.tpl"}
                    
                {else}
                    {include file="module:smartblog/views/templates/front/category_loop_list.tpl"}
                {/if}
            {/foreach}
        </div>
        {if !empty($pagenums)}
            <div class="row">
                <div class="post-page col-md-12">
                            <ul class="pagination panel-paging">
                                {for $k=0 to $pagenums}
                                    {if $title_category != ''}
                                        {assign var="options" value=null}
                                        {$options.page = $k+1}
                                        {$options.id_category = $id_category}
                                        {$options.slug = $cat_link_rewrite}
                                    {else}
                                        {assign var="options" value=null}
                                        {$options.page = $k+1}
                                    {/if}
                                    {if ($k+1) == $c}
                                        <li class="active"><a class="page-active">{$k+1}</a></li>
                                    {else}
                                        {if $title_category != ''}
                                            <li><a class="page-link" href="{smartblog::GetSmartBlogLink('smartblog_category_pagination',$options)}">{$k+1}</a></li>
                                        {else}
                                            <li><a class="page-link" href="{smartblog::GetSmartBlogLink('smartblog_list_pagination',$options)}">{$k+1}</a></li>
                                        {/if}
                                    {/if}
                               {/for}
                            </ul>
                        {*
            			<div class="col-md-6">
                            <div class="results">{l s="Showing" mod="smartblog"} {if $limit_start!=0}{$limit_start}{else}1{/if} {l s="to" mod="smartlatestnews"} {if $limit_start+$limit >= $total}{$total}{else}{$limit_start+$limit}{/if} {l s="of" mod="smartblog"} {$total} ({$c} {l s="Pages" mod="smartblog"})</div>
                        </div>*}
              </div>
          </div> 
      {/if}
     {/if}
 </div>
     {if $smartshowcolumn==2}
        <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
            {hook h='displayColumRightBlog'}
        </div>
    {/if}
{if isset($smartcustomcss)}
    <style>
        {$smartcustomcss}
    </style>
{/if}
{/block}