{extends file='page.tpl'}

{block name='breadcrumb'}

    <section data-depth="{$breadcrumb.count}" class="breadcrumb hidden-sm-down panel-title">

        <div class="container">

            <h2 class="panel-title-heading">{$post['meta_title']}</h2>

          <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="nav-breadcrumbs">

            {foreach from=$breadcrumb.links item=path name=breadcrumb}

              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">

                <a itemprop="item" href="{$path.url}">

                  <span itemprop="name">{$path.title}</span>

                </a>

                <meta itemprop="position" content="{$smarty.foreach.breadcrumb.iteration}" />

              </li>

            {/foreach}

          </ul>

        </div>

    </section>

{/block}

{block name="page_content"}

    {if $smartshowcolumn==1}

        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">

            {hook h='displayColumLeftBlog'}

        </div>

    {/if}

    <div id="content-wrapper" class="{if $smartshowcolumn==1}left-column{/if}{if $smartshowcolumn==2}right-column{/if} col-xs-12 {if $smartshowcolumn}col-sm-8 col-md-9{/if}">

    <div id="content" class="block">

       <div itemtype="#" itemscope="" id="sdsblogArticle" class="blog-post">

          

          <div itemprop="articleBody">

                <div id="lipsum" class="articleContent">

                        {assign var="activeimgincat" value='0'}

                        {$activeimgincat = $smartshownoimg} 

                        {if ($post_img != "no" && $activeimgincat == 0) || $activeimgincat == 1}

                            <a id="post_images" href="{$modules_dir}/smartblog/images/{$post_img}-single-default.jpg"><img src="{$modules_dir}/smartblog/images/{$post_img}-single-default.jpg" alt="{$meta_title}"></a>

                        {/if}

                 </div>

                <h1 class="panel-blog-title">

           			{$meta_title}

           		</h1> 

                <div class="panel-blog-group">

                    <ul class="panel-blog-info">

                        {assign var="catOptions" value=null}

                        {$catOptions.id_category = $id_category}

                        {$catOptions.slug = $cat_link_rewrite}

                        <li>

                            {l s='Posted by ' mod='smartblog'} {if $smartshowauthor ==1}&nbsp;<i class="icon icon-user"></i>

                           <span itemprop="author">{if $smartshowauthorstyle != 0}{$firstname} {$lastname}{else}{$lastname} {$firstname}{/if}

                           </span>

                           {/if}

                        </li>

                        <li>

                            <a href="{smartblog::GetSmartBlogLink('smartblog_category',$catOptions)}">

                                <i class="fa fa-file-text-o"></i>{$title_category}</a>

                        </li>

                        <li>

                            <i class="fa fa-clock-o"></i>

                           <span itemprop="dateCreated">{$created|date_format}</span>

                        </li>

                    </ul>

                    <ul class="panel-blog-info">

                        <li>

                               <i class="fa fa-comment-o"></i>

                               {if $countcomment != ''}

                               {$countcomment}{else}

                               {l s='0' mod='smartblog'}{/if}

                               {l s=' Comments' mod='smartblog'}

                        </li>

                    </ul>

                  </div>

                <div class="sdsarticle-des">

                   {$content nofilter}
                </div>
                {if $tags != ''}

                    <div class="panel-tags-blog">

                        <span class="panel-tags-label">{l s='Tags:' mod='smartblog'}</span>

                        {foreach from=$tags item=tag}

                            {assign var="options" value=null}

                            {$options.tag = $tag.name}

                            <a title="tag" href="{smartblog::GetSmartBlogLink('smartblog_tag',$options)}">{$tag.name}</a>

                        {/foreach}

                        

                    </div>

               {/if}

          </div>

          <div class="clearfix"></div>

          <div class="sdsarticleBottom sang">

            {$HOOK_SMART_BLOG_POST_FOOTER nofilter}

          </div>

       </div>

        {if $countcomment != ''}

            <div id="articleComments" class="panel-review panel-review-blog">

                <div class="panel-review-heading">

                        <h3 class="panel-review-title">{if $countcomment != ''}{$countcomment}{else}{l s='0' mod='smartblog'}{/if}{l s=' Reviews for this item' mod='smartblog'}<span></span></h3>

                </div>

                <div id="comments">      

                    <ul class="commentList">

                          {$i=1}

                        {foreach from=$comments item=comment}

                               {include file="module:smartblog/views/templates/front/comment_loop.tpl" childcommnets=$comment}

                        {/foreach}

                    </ul>

                </div>

            </div>

        {/if}

    </div>

    {if Configuration::get('smartenablecomment') == 1}

    {if $comment_status == 1}

    <div class="smartblogcomments panel-review-form" id="respond">

        

        <h3 class="panel-review-title" id="reply-title">{l s="Leave a comment"  mod="smartblog"} <small style="float:right;">

                <a style="display: none;" href="/wp/sellya/sellya/this-is-a-post-with-preview-image/#respond" 

                   id="cancel-comment-reply-link" rel="nofollow">{l s="Cancel Reply"  mod="smartblog"}</a>

            </small>

        </h3>

  		<div id="commentInput">

            <form action="" method="post" id="commentform">

                <div class="panel-review-group">

                    <label class="panel-review-label" for="your_comment">

                        {l s="Your comment"  mod="smartblog"}

                    </label>

                    <div class="panel-review-control">

                        <textarea tabindex="4" class="inputContent form-control grey" rows="8" cols="50" name="comment"></textarea>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-8 col-sm-12 col-xs-12">

                        <div class="row">

                            <div class="col-sm-6 col-xs-12">

                                <div class="panel-review-group">

                                    <label class="panel-review-label" for="your_name">{l s="Name"  mod="smartblog"}</label>

                                    <div class="panel-review-control">

                                        <input type="text" tabindex="1" class="inputName form-control grey" value="" name="name" />

                                    </div>

                                </div>

                            </div>

                            <div class="col-sm-6 col-xs-12">

                                <div class="panel-review-group">

                                    <label class="panel-review-label" for="your_name">{l s="E-mail"  mod="smartblog"}</label>

                                    <div class="panel-review-control">

                                        <input type="text" tabindex="2" class="inputMail form-control grey" value="" name="mail" />

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-4 col-sm-12 col-xs-12">

                        <div class="panel-review-group">

                            <label class="panel-review-label" for="your_website">{l s="Website"  mod="smartblog"}</label>

                            <div class="panel-review-control">

                                <input type="text" tabindex="3" value="" name="website" class="form-control grey" />

                            </div>

                        </div>

                    </div>

                    {if Configuration::get('smartcaptchaoption') == '1'}

                        <div class="col-sm-12 col-xs-12">

                            <div class="panel-review-group">

                                <label class="panel-review-label" for="your_website">{l s="Type Code" mod="smartblog"}</label>

                                <div class="panel-review-control">

                                    <img src="{$modules_dir}smartblog/classes/CaptchaSecurityImages.php?width=100&height=40&characters=5">

    		                        <input type="text" tabindex="" value="" name="smartblogcaptcha" class="smartblogcaptcha form-control grey">

                                </div>

                            </div>

                        </div>

                	{/if}

                </div>

                <input type='hidden' name='comment_post_ID' value='1478' id='comment_post_ID' />

                <input type='hidden' name='id_post' value='{$id_post}' id='id_post' />

                <input type='hidden' name='comment_parent' id='comment_parent' value='0' />

                <div class="submit panel-review-group">

                    <input type="submit" name="addComment" id="submitComment" class="panel-review-submit bbutton btn btn-default button-medium" value="Submit" />

        		</div>

            </form>

            

        </div>

  		</div>

    {/if}

{/if}

</div>

 {if $smartshowcolumn==2}

    <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">

        {hook h='displayColumRightBlog'}

    </div>

{/if}

{if isset($smartcustomcss)}

    <style>

        {$smartcustomcss}

    </style>

{/if}

<script type="text/javascript">

var modules_dir='{$modules_dir}';

</script>

{/block}





