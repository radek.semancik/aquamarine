{if $comment.id_smart_blog_comment != ''}
<div class="commentList panel-review-item panel-table">
    <div id="comment-{$comment.id_smart_blog_comment}">
        <div class="panel-row">
            <div class="panel-cell">
                <img class="avatar panel-review-avatar" alt="Avatar" src="{$modules_dir}/smartblog/images/avatar/avatar-author-default.jpg">
            </div>
            <div class="panel-cell">
                <h5 class="panel-review-header">
                    <span class="panel-review-author">{$childcommnets.name}</span>
                    <span class="panel-review-space">-</span>
                    <span class="panel-review-time">{$childcommnets.created|date_format}</span>
                </h5>
                <p class="panel-review-content">{$childcommnets.content}</p>
                {if Configuration::get('smartenablecomment') == 1}
                    {if $comment_status == 1}
                        <div class="reply panel-review-action">
                            <a onclick="return addComment.moveForm('comment-{$comment.id_smart_blog_comment}', '{$comment.id_smart_blog_comment}', 'respond', '{$smarty.get.id_post}')"  class="comment-reply-link">
                                <i class="fa fa-comment-o"></i>
                            </a>
                        </div>
                    {/if}
                {/if}
                {if isset($childcommnets.child_comments)}
                    <div class="sub_comment_reply">
                        {foreach from=$childcommnets.child_comments item=comment}  
                            {if isset($childcommnets.child_comments)}
                                {include file="module:smartblog/views/templates/front/comment_loop.tpl" childcommnets=$comment}
                                {$i=$i+1}
                            {/if}
                        {/foreach}
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>
{/if}
                                        
                                        