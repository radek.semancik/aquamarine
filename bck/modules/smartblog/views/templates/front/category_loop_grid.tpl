<div itemtype="#" itemscope="" class="sdsarticleCat col-md-4 col-sm-6 col-ms-6 col-xs-12">
    <div id="smartblogpost-{$post.id_post}" class="panel-news-item">
         <a itemprop="url" title="{$post.meta_title}" class="panel-news-media">
                    {assign var="activeimgincat" value='0'}
                    {$activeimgincat = $smartshownoimg} 
                    {if ($post.post_img != "no" && $activeimgincat == 0) || $activeimgincat == 1}
              <img itemprop="image" alt="{$post.meta_title}" src="{$modules_dir}/smartblog/images/{$post.post_img}-single-default.jpg" class="imageFeatured">
                    {/if}
          </a>
          
          {assign var='date_format' value='F j, Y'}
          
          
            {assign var="options" value=null}
                {$options.id_post = $post.id_post} 
                {$options.slug = $post.link_rewrite}
                
                {assign var="options" value=null}
                {$options.id_post = $post.id_post}
                {$options.slug = $post.link_rewrite}
                {assign var="catlink" value=null}
                {$catlink.id_category = $post.id_category}
                {$catlink.slug = $post.cat_link_rewrite}

                 
                 <a href="{smartblog::GetSmartBlogLink('smartblog_category',$catlink)}">
                 <span class="panel-news-category">
                 {if $title_category != ''}{$title_category}{else}{$post.cat_name}{/if}
                 </span>
                 </a>
                 
            
            <h3 class='panel-news-title'>
                <a class="panel-news-link" title="{$post.meta_title}" href='{smartblog::GetSmartBlogLink('smartblog_post',$options)}'>{$post.meta_title}
                </a>
                </h3>
            
            <div itemprop="description" class="panel-news-summary">
               {$post.short_description}
          </div>
            <ul class="panel-info-post">
                <li>
                    {date($date_format,strtotime($post.created))|escape:'html':'UTF-8'}
                </li>
                <li>
                    <span class="comment"> &nbsp;<i class="fa fa-comment-o"></i>&nbsp; 
                     <a title="{$post.totalcomment} Comments" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}#articleComments">
                     {$post.totalcomment}</a></span>
                 </li>
                 {if $smartshowviewed ==1}
                    <li>
                        <i class="fa fa-eye"></i>{$post.viewed}
                    </li>
                 {/if}
            </ul>
   </div>
</div>