{assign var='date_format' value='F j, Y'}
{assign var="options" value=null}
{$options.id_post = $post.id_post} 
{$options.slug = $post.link_rewrite}
<div itemtype="#" itemscope="" class="sdsarticleCat panel-blog-list">
    <div id="smartblogpost-{$post.id_post}">
    <div class="col-md-5 col-sm-12">
         <a href='{smartblog::GetSmartBlogLink('smartblog_post',$options)}' itemprop="url" title="{$post.meta_title}" class="imageFeaturedLink zoom-img">
                    {assign var="activeimgincat" value='0'}
                    {$activeimgincat = $smartshownoimg} 
                    {if ($post.post_img != "no" && $activeimgincat == 0) || $activeimgincat == 1}
                    <img itemprop="image" alt="{$post.meta_title}" src="{$modules_dir}/smartblog/images/{$post.post_img}-single-default.jpg" class="imageFeatured" />
                    {/if}
          </a>
    </div>
    <div class="col-md-7 col-sm-12">
                            <h3 class='panel-blog-title'>
                                <a title="{$post.meta_title}" href='{smartblog::GetSmartBlogLink('smartblog_post',$options)}'>{$post.meta_title}</a></h3>
             {assign var="options" value=null}
                        {$options.id_post = $post.id_post}
                        {$options.slug = $post.link_rewrite}
               {assign var="catlink" value=null}
                            {$catlink.id_category = $post.id_category}
                            {$catlink.slug = $post.cat_link_rewrite}
         <ul class="panel-blog-info">
             {if $smartshowauthor ==1}
                <li>
                    <span itemprop="author">
                    <i class="fa fa-user"></i>&nbsp; 
                     {if $smartshowauthorstyle != 0}{$post.firstname} {$post.lastname}
                     {else}{$post.lastname} {$post.firstname}{/if}
                    </span>
                </li>
             {/if} 
             
             <li>
                <i class="fa fa-tags"></i>&nbsp; 
                 <span itemprop="articleSection">
                 <a href="{smartblog::GetSmartBlogLink('smartblog_category',$catlink)}">
                 {if $title_category != ''}{$title_category}{else}{$post.cat_name}{/if}</a>
                 </span>
             </li>
             <li>
                {date($date_format,strtotime($post.created))|escape:'html':'UTF-8'}
             </li>
             
             
             
         </ul>
    
        <div itemprop="description" class="panel-blog-summary">
           {$post.short_description}
      </div>
        <ul class="panel-blog-info">
            <li>
                <span class="comment"> &nbsp;<i class="fa fa-comment-o"></i>&nbsp; 
                 <a title="{$post.totalcomment} Comments" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}#articleComments">
                 {$post.totalcomment} {l s=' Comments' mod='smartblog'}</a></span>
             </li>
             {if $smartshowviewed ==1}
                <li>
                    <i class="fa fa-eye"></i>
                    {l s=' views' mod='smartblog'} ({$post.viewed})
                </li>
             {/if}
        
        </ul>
          
    </div>
              {*
        <div class="sdsreadMore">
                  {assign var="options" value=null}
                        {$options.id_post = $post.id_post}  
                        {$options.slug = $post.link_rewrite}  
                         <span class="more"><a title="{$post.meta_title}" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}" class="r_more">{l s='Read more' mod='smartblog'} </a></span>
        </div>*}
   </div>
</div>