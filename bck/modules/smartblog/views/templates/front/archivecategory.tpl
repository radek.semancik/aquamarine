{capture name=path}<a href="{smartblog::GetSmartBlogLink('smartblog')}">{l s='All Blog News' mod='smartblog'}</a>
{extends file='page.tpl'}
{block name='breadcrumb'}
    <section data-depth="{$breadcrumb.count}" class="breadcrumb panel-title">
        <div class="container">
            <h2 class="panel-title-heading">{$title_category}</h2>
            <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="nav-breadcrumbs">
            {foreach from=$breadcrumb.links item=path name=breadcrumb}
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="{$path.url}">
                  <span itemprop="name">{$path.title}</span>
                </a>
                <meta itemprop="position" content="{$smarty.foreach.breadcrumb.iteration}" />
              </li>
            {/foreach}
            </ul>
        </div>
    </section>
{/block}
{block name="page_content"}
     {if $title_category != ''}
    <span class="navigation-pipe">{$navigationPipe}</span>{$title_category}{/if}{/capture}
    {if $postcategory == ''}
             <p class="error">{l s='No Post in Archive' mod='smartblog'}</p>
    {else}   
    <div id="smartblogcat" class="block sang">
        {foreach from=$postcategory item=post}
            {if isset($option_type_blog) && $option_type_blog == 'list_miniture'}
                {include file="module:smartblog/views/templates/front/category_loop.tpl"}
            {elseif isset($option_type_blog) && $option_type_blog == 'grid'}
                {include file="module:smartblog/views/templates/front/category_loop_grid.tpl"}
                
            {else}
                {include file="module:smartblog/views/templates/front/category_loop_list.tpl"}
            {/if}
        {/foreach}
    </div>
 {/if}
 {if isset($smartcustomcss)}
    <style>
        {$smartcustomcss}
    </style>
{/if}
{/block}
