<?php
class AdminImageTypeController extends ModuleAdminController {

    public function __construct() {
        $this->table = 'smart_blog_imagetype';
        $this->className = 'BlogImageType';
        $this->module = 'smartblog';
        $this->lang = false;
        parent::__construct();
        $this->context = Context::getContext();
        $this->bootstrap = true;
        $this->fields_list = array(
            'id_smart_blog_imagetype' => array(
                    'title' => $this->trans('Id',array(),'Modules.smartblog'),
                    'width' => 100,
                    'type' => 'text',
            ),
            'type_name' => array(
                    'title' => $this->trans('Type Name',array(),'Modules.smartblog'),
                    'width' => 350,
                    'type' => 'text',
            ),
            'width' => array(
                    'title' =>$this->trans('Width',array(),'Modules.smartblog'),
                    'width' => 60,
                    'type' => 'text',
            ),
            'height' => array(
                    'title' => $this->trans('Height',array(),'Modules.smartblog'),
                    'width' => 60,
                    'type' => 'text',
            ),
            'type' => array(
                    'title' => $this->trans('Type',array(),'Modules.smartblog'),
                    'width' => 220,
                    'type' => 'text',
            ),
            'active' => array(
                'title' =>$this->trans('Status',array(),'Modules.smartblog'),
                'width' => 60,
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false
            )
        );
        
    }

    public function renderForm()
            {
        $this->fields_form = array(
          'legend' => array(
          'title' => $this->trans('Blog Category',array(),'Modules.smartblog'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->trans('Image Type Name',array(),'Modules.smartblog'),
                    'name' => 'type_name',
                    'size' => 60,
                    'required' => true,
                    'desc' => $this->trans('Image Type Name',array(),'Modules.smartblog'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Width',array(),'Modules.smartblog'),
                    'name' => 'width',
                    'size' => 15,
                    'required' => true,
                    'desc' => $this->trans('Image height in px',array(),'Modules.smartblog'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Height',array(),'Modules.smartblog'),
                    'name' => 'height',
                    'size' => 15,
                    'required' => true,
                    'desc' => $this->trans('Image height in px',array(),'Modules.smartblog'),
                ),
                array(
                'type' => 'select',
                'label' => $this->trans('Type',array(),'Modules.smartblog'),
                'name' => 'type',
                'required' => true,
                'options' => array(
                    'query' => array(
                                array(
                                'id_option' => 'post',
                                'name' => 'Post'
                                ),
                                array(
                                'id_option' => 'Category',
                                'name' => 'category'
                                ),
                                array(
                                'id_option' => 'Author',
                                'name' => 'author'
                                )
                            ),
                    'id' => 'id_option',
                    'name' => 'name'
                    )
                ),
                array(
                    'type' => 'radio',
                    'label' => $this->trans('Status',array(),'Admin.Global'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active',
                            'value' => 1,
                            'label' => $this->trans('Enabled',array(),'Admin.Global'),
                        ),
                        array(
                            'id' => 'active',
                            'value' => 0,
                            'label' => $this->trans('Disabled',array(),'Admin.Global'),
                        )
                    )
                 )
            ),
            'submit' => array(
                'title' => $this->trans('Save',array(),'Admin.Global'),
                'class' => 'button'
            )
        );

        if (!($BlogImageType = $this->loadObject(true)))
            return;

        $this->fields_form['submit'] = array(
            'title' => $this->trans('Save',array(),'Admin.Global'),
            'class' => 'button'
        );
        return parent::renderForm();
    }
    public function renderList() {
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        return parent::renderList();;
    }
    public function initToolbar() {
        parent::initToolbar();
    }
}
