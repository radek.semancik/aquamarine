{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 
{block name='product_miniature_item'}

  <article class="product-miniature js-product-miniature" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      {block name='product_thumbnail'}
        <div class="product_special_img">
            <a href="{$product.url|escape:'html':'UTF-8'}" class="thumbnail product-thumbnail">
                <img src ="{$product.cover.bySize.home_default.url|escape:'html':'UTF-8'}" alt = "{$product.cover.legend|escape:'html':'UTF-8'}" data-full-size-image-url = "{$product.cover.large.url|escape:'html':'UTF-8'}" />
            </a>
            {if $product.has_discount}
                  <span class="discount-percentage">{if $product.discount_type=='amount'}-{$product.discount_amount|escape:'html':'UTF-8'}{else}{$product.discount_percentage|escape:'html':'UTF-8'}{/if}</span>
            {/if}
        </div>
      {/block}
      <div class="product-infomation-content">
      <div class="product-description">
            {block name='product_name'}
              <h3 class="h3 product-title" itemprop="name">
                  <a href="{$product.url|escape:'html':'UTF-8'}">
                    {$product.name|truncate:30:'...'|escape:'html':'UTF-8'} 
                  </a>
              </h3>
            {/block}
            {block name='product_price_and_shipping'}
                {if $product.show_price}
                    <div class="product-price-and-shipping">
                        {if $product.has_discount}
                            {hook h='displayProductPriceBlock' product=$product type="old_price"}
                            <span class="sr-only">{l s='Regular price' mod='ets_hotdeals'}</span>
                            <span class="regular-price">{$product.regular_price|escape:'html':'UTF-8'}</span>
                        {/if}
                        {hook h='displayProductPriceBlock' product=$product type="before_price"}
                        <span class="sr-only">{l s='Price' mod='ets_hotdeals'}</span>
                        <span itemprop="price" class="price">{$product.price|escape:'html':'UTF-8'}</span>
                        {hook h='displayProductPriceBlock' product=$product type='unit_price'}
                        {hook h='displayProductPriceBlock' product=$product type='weight'}
                    </div>
                {/if}
            {/block}
        {block name='product_reviews'}
            {hook h='displayProductListReviews' product=$product}
        {/block}
        {block name='product_flags'}
            <ul class="product-flags">
                {foreach from=$product.flags item=flag}
                    <li class="product-flag {$flag.type|escape:'html':'UTF-8'}">{$flag.label|escape:'html':'UTF-8'}</li>
                {/foreach}
            </ul>
        {/block} 
        {if isset($product.description_short) && $product.description_short !=''}
            <div class="short_description">{$product.description_short|truncate:90:'...' nofilter}</div>
        {else}
            <div class="short_description">{$product.description|truncate:90:'...' nofilter}</div>
        {/if}
      </div>
      <div class="highlighted-informations{if !$product.main_variants} no-variants{/if} hidden-sm-down">
            {block name='quick_view'}
            <a class="quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> {l s='Quick view' mod='ets_hotdeals'}
            </a>
            {/block}
            {block name='product_variants'}
            {if $product.main_variants}
              {include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
            {/if}
            {/block}
      </div>
        </div>
    </div>
  </article>
{/block}
