{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{extends file='page.tpl'}
{block name='breadcrumb'}
    <section data-depth="{$breadcrumb.count}" class="breadcrumb hidden-sm-down panel-title">
        <div class="container">
            <h2 class="panel-title-heading">{l s='Tag product' mod='pg_tagproduct'}</h2>
          <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="nav-breadcrumbs">
            {foreach from=$breadcrumb.links item=path name=breadcrumb}
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="{$path.url}">
                  <span itemprop="name">{$path.title}</span>
                </a>
                <meta itemprop="position" content="{$smarty.foreach.breadcrumb.iteration}" />
              </li>
            {/foreach}
          </ul>
        </div>
    </section>
{/block}
{block name="page_content"}
    <div id="tag_product" class="">
        <h3 class="h1 products-section-title text-uppercase">
            {l s='Products' mod='pg_tagproduct'}
        </h3>
        <div class="tag-product row">
            {if $products_list}
                <div class="list-product">
                    {foreach from=$products_list item=product name=products}
                    	{include file="catalog/_partials/miniatures/product.tpl" product=$product}
                    {/foreach}
                </div>
            {/if}
        </div>
    </div>
{/block}