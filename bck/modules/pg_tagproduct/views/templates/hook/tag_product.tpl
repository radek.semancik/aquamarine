{if $tags}
    <div class="block">
        <h4 class="widget-title">{l s='Product tags' mod='pg_tagproduct'}</h4>
        <div class="content_block block_content">
            <div class="product_tag">
                {foreach from=$tags item='tag'}
                    <a href="{$tag.link}" class="tag" title="{$tag.name}">{$tag.name}</a>     
                {/foreach}               
            </div>
        </div>
    </div>
{/if}  