<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @version  Release: $Revision$
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;
class Pg_tagproduct extends Module
{
    private $_html = '';
    public $is17 = false;
    public function __construct()
	{
        $this->name = 'pg_tagproduct';
		$this->tab = 'front_office_features';
		$this->version = '1.0.2';
		$this->author = 'prestagold.com';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		$this->bootstrap = true;
        if(version_compare(_PS_VERSION_, '1.7', '>='))
            $this->is17 = true; 
		parent::__construct();
		$this->displayName = $this->l('Tag product');
		$this->description = $this->l('Display tag product module');
		$this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => _PS_VERSION_);
    }
    public function install()
	{
	    return parent::install() && $this->registerHook('displayHeader')&& $this->registerHook('displayLeftColumn')&&$this->registerHook('displayRightColumn');
    }    
    /**
	 * @see Module::uninstall()
	 */
	public function uninstall()
	{
        return parent::uninstall();
    }
    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'views/css/pg_tagproduct.css');
    }
    public function hookDisplayLeftColumn()
    {
        $sql ='SELECT t.id_tag, t.name FROM '._DB_PREFIX_.'tag t,'._DB_PREFIX_.'product_tag pt,'._DB_PREFIX_.'product p
            WHERE p.id_product=pt.id_product AND t.id_tag= pt.id_tag AND p.active=1 AND t.id_lang='.(int)$this->context->language->id.' GROUP BY t.id_tag';
        $tags= Db::getInstance()->executeS($sql);
        if($tags)
        {
            foreach($tags as &$tag)
            {
                $tag['link'] = $this->context->link->getModuleLink($this->name,'product',array('id_tag'=>$tag['id_tag']));
            }
        }
        $this->context->smarty->assign(
            array(
                'tags'=> $tags,
            )
        );
        return $this->display(__FILE__,'tag_product.tpl');
    }
    public function hookDisplayRightColumn()
    {
        $sql ='SELECT t.id_tag, t.name FROM '._DB_PREFIX_.'tag t,'._DB_PREFIX_.'product_tag pt,'._DB_PREFIX_.'product p
            WHERE p.id_product=pt.id_product AND t.id_tag= pt.id_tag AND p.active=1 AND t.id_lang='.(int)$this->context->language->id.' GROUP BY t.id_tag';
        $tags= Db::getInstance()->executeS($sql);
        if($tags)
        {
            foreach($tags as &$tag)
            {
                $tag['link'] = $this->context->link->getModuleLink($this->name,'product',array('id_tag'=>$tag['id_tag']));
            }
        }
        $this->context->smarty->assign(
            array(
                'tags'=> $tags,
            )
        );
        return $this->display(__FILE__,'tag_product.tpl');
    }
    public function getLangLinkFriendly($id_lang = null, Context $context = null, $id_shop = null)
	{
		if (!$context)
			$context = Context::getContext();

		if ((!$this->allow && in_array($id_shop, array($context->shop->id,  null))) || !Language::isMultiLanguageActivated($id_shop) || !(int)Configuration::get('PS_REWRITING_SETTINGS', null, null, $id_shop))
			return '';

		if (!$id_lang)
			$id_lang = $context->language->id;

		return Language::getIsoById($id_lang).'/';
	}
	
	public function getBaseLinkFriendly($id_shop = null, $ssl = null)
	{
		static $force_ssl = null;
		
		if ($ssl === null)
		{
			if ($force_ssl === null)
				$force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
			$ssl = $force_ssl;
		}

		if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') && $id_shop !== null)
			$shop = new Shop($id_shop);
		else
			$shop = Context::getContext()->shop;

		$base = (($ssl && $this->ssl_enable) ? 'https://'.$shop->domain_ssl : 'http://'.$shop->domain);

		return $base.$shop->getBaseURI();
	}
    
}