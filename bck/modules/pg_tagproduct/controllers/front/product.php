<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  @version  Release: $Revision$
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;
class Pg_tagproductProductModuleFrontController extends ModuleFrontController
{
    public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
	}
    public function initContent()
	{
        parent::initContent();
        $id_lang= $this->context->language->id;
        $id_tag= (int)Tools::getValue('id_tag');
        $productIds = array();
        $products= Db::getInstance()->executeS('SELECT p.id_product FROM '._DB_PREFIX_.'product p, '._DB_PREFIX_.'product_tag pt WHERE p.id_product = pt.id_product AND pt.id_tag="'.(int)$id_tag.'" AND p.active=1 AND pt.id_lang='.(int)$this->context->language->id);
        if($products)
        {
            if($products)
            foreach($products as $product)
                $productIds[]= $product['id_product'];
            $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
            	pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`name`,
                image_shop.`id_image` id_image, il.`legend`, m.`name` manufacturer_name,
            	DATEDIFF(
            		p.`date_add`,
            		DATE_SUB(
            			"'.date('Y-m-d').' 00:00:00",
            			INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
            		)
            	) > 0 new'.(Combination::isFeatureActive() ? ', product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, IFNULL(product_attribute_shop.`id_product_attribute`,0) id_product_attribute2' : '').'
            	FROM '._DB_PREFIX_.'product p
            	'.Shop::addSqlAssociation('product', 'p').'
            	INNER JOIN `'._DB_PREFIX_.'product_lang` pl ON (
            		p.`id_product` = pl.`id_product`
            		AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
            	)
            	'.(Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute_shop` product_attribute_shop
            	ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop='.(int)$this->context->shop->id.')':'').'
            	'.Product::sqlStock('p', 0).'
            	LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
            	LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
            		ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$this->context->shop->id.')
            	LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
            	WHERE p.`id_product` IN ('.implode(',',$productIds).')
            	GROUP BY product_shop.id_product';
            $result = Db::getInstance()->executeS($sql, true, false);
            $products_list = Product::getProductsProperties((int)$id_lang, $result);
            $assembler = new ProductAssembler($this->context);
            $presenterFactory = new ProductPresenterFactory($this->context);
            $presentationSettings = $presenterFactory->getPresentationSettings();
            $presenter = new PrestaShop\PrestaShop\Core\Product\ProductListingPresenter(
                new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever(
                    $this->context->link
                ),
                $this->context->link,
                new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter(),
                new PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever(),
                $this->context->getTranslator()
            );
    
            $products_for_template = array();
            
            foreach ($products_list as $rawProduct) {
                $products_for_template[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct($rawProduct),
                    $this->context->language
                );
            }
        }
        else
            $products_for_template=array();
        $this->context->smarty->assign(
            array(
                 'products_list'=>$products_for_template,
            )
        );
        $this->setTemplate('module:pg_tagproduct/views/templates/front/list_product.tpl');
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
     
        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Tag product', [], 'Breadcrumb'),
            'url' => $this->context->link->getModuleLink('pg_tagproduct', 'product',array('id_tag'=>Tools::getValue('id_tag')))
         ];
         return $breadcrumb;
    }
}