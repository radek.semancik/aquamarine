{if $categories}
    <div id="pg_footer_categories" class="wrapper links">
        <h3 class="h3 title-footer-block hidden-xs-down">{l s='Categories' d='Shop.Theme'}</h3>
        <div class="title clearfix hidden-sm-up" data-target="#footer_categories" data-toggle="collapse">
            <span class="h3 widget-title">{l s='Categories' d='Shop.Theme'}</span>
            <span class="pull-xs-right">
              <span class="navbar-toggler collapse-icons">
                <i class="material-icons add">expand_more</i>
                <i class="material-icons remove">expand_less</i>
              </span>
            </span>
        </div>
        <ul id="footer_categories" class="collapse">
            {foreach from=$categories item='category'}
                <li><a href="{$link->getCategoryLink($category.id_category)}">{$category.name}</a></li>
            {/foreach}
        </ul>
    </div>
{/if} 