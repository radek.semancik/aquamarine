<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @version  Release: $Revision$
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;
class Pg_footercategories extends Module
{
    private $_html = '';
    public $is17 = false;
    public function __construct()
	{
        $this->name = 'pg_footercategories';
		$this->tab = 'front_office_features';
		$this->version = '1.0.2';
		$this->author = 'prestagold.com';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		$this->bootstrap = true;
        if(version_compare(_PS_VERSION_, '1.7', '>='))
            $this->is17 = true; 
        $this->module_key = 'da314fdf1af6d043f9b2f15dce2bef1e';
		parent::__construct();
		$this->displayName = $this->l('Footer categories');
		$this->description = $this->l('Display categories in footer');
		$this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => _PS_VERSION_);
    }
    public function install()
	{
	    return parent::install()&& $this->registerHook('displayFooter');
    }    
    /**
	 * @see Module::uninstall()
	 */
	public function uninstall()
	{
        return parent::uninstall();
    }
    public function getContent()
    {
        if (Tools::isSubmit('pg_btnSubmit')) {
            Configuration::updateValue('PG_CATEGORY_DISPLAY_FOOTER',implode(',',Tools::getValue('PG_CATEGORY_DISPLAY_FOOTER')));
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=5&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
        }
        $this->_html .= $this->renderForm();

        return $this->_html;
    }
    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Categories'),
                    'icon' => 'icon-envelope'
                ),
                'input' => array(
                    array(
                        'type' => 'categories',
                        'label' => $this->l('Categories'),
                        'name' => 'PG_CATEGORY_DISPLAY_FOOTER',
                        'tree'  => array(
                            'id'                  => 'categories-tree',
                            'selected_categories' => explode(',',Configuration::get('PG_CATEGORY_DISPLAY_FOOTER')),
                            'disabled_categories' => null,
                            'use_checkbox' =>true,
                            'use_radio'=>false,
                            'root_category'       => $this->context->shop->getCategory()
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'pg_btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='
            .$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => array(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }
    public function hookDisplayFooter()
    {
        if(!Configuration::get('PG_CATEGORY_DISPLAY_FOOTER')|| !$this->active)
            return '';
        $categories = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'category c,'._DB_PREFIX_.'category_lang cl WHERE c.id_category=cl.id_category and cl.id_lang='.(int)$this->context->language->id.' AND c.active=1 AND c.id_category IN ('.Configuration::get('PG_CATEGORY_DISPLAY_FOOTER').')');
        $this->context->smarty->assign(
            array(
                'categories'=>$categories,
                'link'=> $this->context->link,
            )
        );
        return $this->display(__FILE__,'footercategories.tpl');
    }
}