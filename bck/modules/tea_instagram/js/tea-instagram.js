var _0xaae8="";
;(function ( $, window, undefined ) {
    var pluginName = 'instastream',
        document = window.document,
        defaults = {
            instaUser: '1011689',
            instaResults: 3,
            instaMenu: 'yes'
        };
    ;
    var $proistclimit=$('#proistclimit').val();
    var $instaUrl;
    var $slideStatus =0;
    // Constructor
    function Plugin( element, options ) {
        this.element = element;

        this.options = $.extend( {}, defaults, options) ;
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }
    // Date converter
    String.prototype.timeconverter=function(){
    var a = new Date(this*1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var time = date+' '+month+' '+year ;
    return time;
    };
// Stream function
$.fn.createStream = function(slide, target){
      var j = slide;
     $(target).addClass('slider-wrapper').append("<div class='loading'></div>");
     $('div').remove('.slider-content');
      $('div').remove('.slider-menu');
     // stream constructor
$.ajax({
type: "GET",
dataType: "jsonp",
headers: { "cache-control": "no-cache" },
async: true,
cache: false,
url: $instaUrl,
success: function(data) {   
    if ($instaMenu == 'yes'){
        $(target).append("<div class='slider-menu'><a href='#' class='prev'><i class='icon-prev'></i></a><a href='#' class='next'><i class='icon-next'></i></a></div>");
    }
    $(target).append("<div id='sbi_images' class='sbi_images panel-instagram-list'></div>");
    var $proistclimit=$('#proistclimit').val();
    for (var i = 0; i < $proistclimit; i++) {
        if (j<20){
                if(typeof(data.data[j])=='undefined' || data.data[j].caption == null)
                {
                    var myCaption = '';
                }
                else
                {
                    var myCaption = data.data[j].caption.text;
                }
                if (typeof(data.data[j])=='undefined'||data.data[j].comments.count < 2)
                {
                    var commentLabel = 'commentaire';
                } else {
                    var commentLabel = 'commentaires'
                }
                if (typeof(data.data[j])=='undefined' ||data.data[j].likes.count < 2)
                {
                    var likeLabel = 'like'
                } 
                else {
                    var likeLabel = 'likes'
                }
                var html ="<div id='slider-item"+i+"' class='sbi_item sbi_type_image'>";
                html +="<div class='sbi_photo_wrap'>";
                html +="<a style='background-size:cover; background-image: url("+ (typeof(data.data[j])!='undefined'?data.data[j].images.standard_resolution.url:'')  +"); ' class='sbi_photo sbi_imgLiquid_bgSize sbi_imgLiquid_ready' href='" +(typeof(data.data[j])!='undefined'? data.data[j].link:'') + "'>";
                html +="<img style='display: none;' src='" +(typeof(data.data[j])!='undefined'? data.data[j].images.standard_resolution.url:'') + "' alt='" + myCaption + "'>";
                html +="</a>";
                html +="<div class='info pa tc flex ts__03 center-xs middle-xs'>";
                html +="<span class='pr cw mgr10'>"
                html +='<i class="fa fa-heart-o mr__5"></i>';
                html += (typeof(data.data[j])!='undefined'?data.data[j].comments.count:'0')
                html +='</span>';
                html +='<span class="pr cw">';
                html +='<i class="fa fa-comments-o mr__5"></i>';
                html += (typeof(data.data[j])!='undefined'?data.data[j].likes.count:'0')
                html +='</span>';
                html +='</div>';
                html +="</div></div>";
                $('.sbi_images').append(html);
                j++;
                $slideStatus = j;
        }
    };
    //prevLoad
    $('.prev').on('click',function(e){
    e.preventDefault();
    var nextSlide = $slideStatus - ($proistclimit * 2);
    $().createStream(nextSlide,target);
    });
    
    //nextLoad
    $('.next').on('click',function(e){
    e.preventDefault();
    var nextSlide = $slideStatus;
    $().createStream(nextSlide,target);
    });
}
}).done(function() {
$('.slider-item').hide();
$('.frame').find('span.frame-more').hide();
$('.frame').find('span.frame-title').hide();
$('.frame').hover(function(){
//$(this).find('span.blocWhite').hide().stop().fadeTo(200,1);
$(this).find('span.frame-more').show().animate({'top': -5},{queue:false,duration:200});
$(this).find('span.frame-title').show().animate({'bottom': 0},{queue:false,duration:200});
}, function(){
//$(this).find('span.blocWhite').stop().fadeOut();
$(this).find('span.frame-more').show().animate({'top': -38},{queue:false,duration:200});
$(this).find('span.frame-title').show().animate({'bottom': -60},{queue:false,duration:200});
});
var beginStatus = $slideStatus - $proistclimit;
if ($instaMenu == 'yes'){
if (beginStatus == 0){
$('.prev').hide();
} else {
$('.prev').show();
}
if ($slideStatus > 19){
$('.next').hide();
} else {
$('.next').show();
}
}
// stream appearance
$('div').remove('.loading');
for (var l = 0; l < $proistclimit; l++) {
k = l +1;
$('#slider-item'+ l).delay(200*k).fadeIn(800);
}
});

    }

    Plugin.prototype.init = function () {

     // Initial variables
     $slideStatus =0;
     //$proistclimit =this.options.instaResults;
     $instaMenu = this.options.instaMenu;
     $instaUrl = 'https://api.instagram.com/v1/users/' + this.options.instaUser + '/media/recent/?access_token=' + this.options.instaToken;

var $myContainer = this.element;

      $().createStream($slideStatus,$myContainer);

    };

    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
            }
        });
    };
}(jQuery, window));
$(document).ready(function ($) {
      
	$("#sb_instagram").instastream({
		instaToken: $('#proistctoken').val(),
		instaUser: $('#proistcid').val(),
		instaResults: $('#proistcid').val(),
		instaMenu: 'no',
	});	
    $("#sb_instagram_left").instastream({
		instaToken: $('#proistctoken').val(),
		instaUser: $('#proistcid').val(),
		instaResults: $('#proistcid').val(),
		instaMenu: 'no',
	});		
});