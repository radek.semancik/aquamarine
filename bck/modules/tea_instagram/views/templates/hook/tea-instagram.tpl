<div class="panel-instagram pt__85">
    <input type="hidden" id="proistclimit" value="{$proistclimit}" />
    <input type="hidden" id="proistcid" value="{$proistcid}" />
    <input type="hidden" id="proistctoken" value="{$proistctoken}" />
    <h3 class="section-title">{l s='@ FOLLOW US ON INSTAGRAM' mod='tea_instagram'}</h3>
    <div class="instagram_content">
        <div id="sb_instagram" class="sbi sbi_fixed_height sbi_col_6 image-instagram" ></div>
    </div>
</div>