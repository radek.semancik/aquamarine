<?php
if (!defined('_PS_VERSION_'))
	exit;
class Tea_Themeconfig extends Module
{
	private $_hooks = array(
        'displayBackOfficeHeader',
        'header',
        );
    private $errorMessage;
    public $configs;
    public $baseAdminPath;
    private $_html;
    private $teamplates;
    public $modeDeveloper = false;
    protected $fields_config=array();
    public function __construct()
	{
	    $this->name = 'tea_themeconfig';
		$this->tab = 'front_office_features';
		$this->version = '1.0.1';
		$this->author = 'prestagold.com';
		$this->need_instance = 0;
	 	parent::__construct();
		$this->displayName = $this->l('Tea theme config');
		$this->description = $this->l('Theme config');
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall module?');
        //Config fields
        $this->bootstrap = true;
    }

    public function install()
	{
		if (!parent::install()||!$this->installDB() )
			return false;
		foreach ($this->_hooks as $hook) {
            if(!$this->registerHook($hook)) return false;
        }
		return true;
	}

    public function uninstall()
	{

		if (!parent::uninstall()|| !$this->uninstallDb()) return false;
		return true;
	}
    public function installDb()
	{
	   $this->createFields();
       $languages = Language::getLanguages(false);
       foreach ($this->fields_config as $f)
       {
            foreach ($f['form']['input'] as $input)
            {
                if (isset($input['lang'])&& $input['lang']) {
                    if(isset($input['default']))
                        Configuration::updateValue(trim($input['name']), $input['default'], true);

                }
                else
                {
                    if(isset($input['default']))
                        Configuration::updateValue(trim($input['name']), $input['default'], true );

                }
            }
        }
	   return true;
    }
    private function uninstallDb()
	{
	   $this->createFields();
       $languages = Language::getLanguages(false);
       foreach ($this->fields_config as $f)
       {
            foreach ($f['form']['input'] as $input)
            {
                Configuration::deleteByName(trim($input['name']));
            }
        }
	   return true;
	}
    public function getContent()
	{
	    if(Tools::isSubmit('delete_tea_background_breadcrum') && Tools::getValue('delete_tea_background_breadcrum'))
        {
           unlink($this->_path.'/images/config/'.Configuration::get('tea_background_breadcrum'));
           Configuration::updateValue('dtea_background_breadcrum','');
           Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name);
        }   
        if (Tools::isSubmit('submitModule') || Tools::isSubmit('submitModule')) {
            $this->savefields();
            $this->_html .= '<div class="bootstrap"><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">×</button>'.$this->l('Successful update.').'</div></div>';
        }
        $this->_html .= '<h2>' . $this->displayName . '</h2>';
        return $this->_html . $this->renderForm();
    }
    public function renderForm()
    {
        $this->createFields();
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path.'images/config/'
        );
        return $helper->generateForm($this->fields_config);
    }
    public function getConfigFieldsValues() {
        $languages = Language::getLanguages(false);
        $fields_values = array();

        foreach ($this->fields_config as $f) {
            foreach ($f['form']['input'] as $input) {
                if (isset($input['lang'])) {
                    foreach ($languages as $lang) {
                        $v = Tools::getValue('title', Configuration::get($input['name'], $lang['id_lang'],null,$this->context->shop->id));
                        $fields_values[$input['name']][$lang['id_lang']] = $v;
                    }
                } else {
                    $v = Tools::getValue($input['name'], Configuration::get($input['name'],null,null,$this->context->shop->id));
                    $fields_values[$input['name']] = $v ;
                }
            }
        }

        return $fields_values;
    }
    public function savefields()
    {
        $this->createFields();
        $errors=array();
        $languages = Language::getLanguages(false);
        foreach ($this->fields_config as $f)
        {
            foreach ($f['form']['input'] as $input)
            {
                if (isset($input['lang'])&& $input['lang']) {
                    $data = array();
                    foreach ($languages as $lang) {
                        $v = Tools::getValue(trim($input['name']) . '_' . $lang['id_lang']);
                        $data[$lang['id_lang']] = $v ? $v : '';
                    }
                    Configuration::updateValue(trim($input['name']), $data, true);

                }
                else
                {
                    if($input['type']=='file')
                    {
                        //Upload file
                        $key=trim($input['name']);
                        if(isset($_FILES[$key]['tmp_name']) && isset($_FILES[$key]['name']) && $_FILES[$key]['name'])
                        {
                            $salt = sha1(microtime());
                            $type = Tools::strtolower(Tools::substr(strrchr($_FILES[$key]['name'], '.'), 1));
                            $imageName = $salt.'.'.$type;
                            $fileName = dirname(__FILE__).'/images/config/'.$imageName;
                            if(file_exists($fileName))
                            {
                                $errors[] = $input['label'].$this->l(' already exists. Try to rename the file then reupload');
                            }
                            else
                            {

                    			$imagesize = @getimagesize($_FILES[$key]['tmp_name']);

                                if (!$errors && isset($_FILES[$key]) &&
                    				!empty($_FILES[$key]['tmp_name']) &&
                    				!empty($imagesize) &&
                    				in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
                    			)
                    			{
                    				$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                    				if ($error = ImageManager::validateUpload($_FILES[$key]))
                    					$errors[] = $error;
                    				elseif (!$temp_name || !move_uploaded_file($_FILES[$key]['tmp_name'], $temp_name))
                    					$errors[] = $this->l('Can not upload the file');
                    				elseif (!ImageManager::resize($temp_name, $fileName, null, null, $type))
                    					$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
                    				if (isset($temp_name))
                    					@unlink($temp_name);
                                    if(!$errors)
                                    {
                                        if(Configuration::get($key)!='')
                                        {
                                            $oldImage = dirname(__FILE__).'/images/config/'.Configuration::get($key);
                                            if(file_exists($oldImage))
                                                @unlink($oldImage);
                                        }
                                        Configuration::updateValue($key, $imageName,true);
                                    }
                                }
                            }
                        }
                        //End upload file
                    }
                    else
                    {
                        $v = addslashes(Tools::getValue($input['name'], Configuration::get($input['name'])));
                        $dataSave = $v;
                        Configuration::updateValue(trim($input['name']), $dataSave, true );
                    }
                }
            }
        }

    }
    public function createFields()
    {
		//General field
        $googlefonts = Tools::jsonDecode(Tools::file_get_contents(dirname(__FILE__).'/data/google-fonts.json'),true);
		$general_fields = array(
            array(
					'type'=>'text',
					'label'=> $this->l('Logo max height'),
					'name' => 'TEA_LOGO_HEIGHT',
					'default' => "150",
                    'suffix' => 'px',
			),
            array(
					'type'=>'text',
					'label'=> $this->l('Logo max width'),
					'name' => 'TEA_LOGO_WIDTH',
					'default' => "200",
                    'suffix' => 'px',
			),
            array(
        		'type'=>'file',
        		'label'=> $this->l('Background breadcrumb'),
        		'name' => 'tea_background_breadcrum',
        		'default' =>'',
                'image' => Configuration::get('tea_background_breadcrum') ? '<img src="'.$this->_path.'/images/config/'.Configuration::get('tea_background_breadcrum').'" alt="'.$this->l('Background breadcrum').'" title="'.$this->l('Background breadcrum').'" />':null,
                'delete_url' =>defined('PS_ADMIN_DIR')? 'index.php?controller=AdminModules&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&delete_tea_background_breadcrum=1&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)$this->context->employee->id):'',
            ),
            array(
					'type'=>'color',
					'label'=> $this->l('Primary color'),
					'name' => 'TEA_SKIN_COLOR',
					'default' => "#56cfe1",
			),
			array(
					'type'=>'color',
					'label'=> $this->l('Title color'),
					'name' => 'TEA_TITLE_COLOR',
					'default' => "#222222",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('Body color'),
					'name' => 'TEA_MAIN_COLOR',
					'default' => "#878787",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('Button color'),
					'name' => 'TEA_BUTTON_COLOR',
					'default' => "#222222",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('Button background'),
					'name' => 'TEA_BUTTON_BACKGROUND',
					'default' => "#ffffff",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('Button color hover'),
					'name' => 'TEA_BUTTON_COLOR_HOVER',
					'default' => "#ffffff",

			),
            array(
					'type'=>'color',
					'label'=> $this->l('Button hover background '),
					'name' => 'TEA_BUTTON_BACKGROUND_HOVER',
					'default' => "#222222",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('Button second color'),
					'name' => 'TEA_BUTTON_COLOR_SECOND',
					'default' => "#ffffff",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('Button second background'),
					'name' => 'TEA_BUTTON_BACKGROUND_SECOND',
					'default' => "#222222",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('Button second color hover'),
					'name' => 'TEA_BUTTON_COLOR_SECOND_HOVER',
					'default' => "#ffffff",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('Button second hover background'),
					'name' => 'TEA_BUTTON_BACKGROUND_SECOND_HOVER',
					'default' => "#56cfe1",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('Price color'),
					'name' => 'TEA_PRICE_COLOR',
					'default' => "#ec0101",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('New label background'),
					'name' => 'TEA_NEW_BG',
					'default' => "#01bad4",
			),
            array(
					'type'=>'color',
					'label'=> $this->l('Sale label background'),
					'name' => 'TEA_SALE_BG',
					'default' => "#ffa800",
			),
            array(
    				'type' => 'select',
    				'label' => $this->l('Fonts title'),
    				'name' => 'TEA_FONTS_TITLE',
                    'default' =>'Poppins',
    				'options' => array(
  						'query' => $googlefonts,
    					'id' => 'id_option',
    					'name' => 'name'
    				 )
			),
            array(
    				'type' => 'select',
    				'label' => $this->l('Fonts main'),
    				'name' => 'TEA_FONTS_MAIN',
                    'default' =>'Poppins',
    				'options' => array(
  						'query' => $googlefonts,
    					'id' => 'id_option',
    					'name' => 'name'
    				 )
			),
            array(
				'type' => 'switch',
				'label' => $this->l('Header sticky'),
				'name' => 'pg_header_sticky',
                'default' =>'1',
				'values' => array(
					array(
						'id' => 'active_on',
						'value' => 1,
						'label' => $this->l('Yes')
					),
					array(
						'id' => 'active_off',
						'value' => 0,
						'label' => $this->l('No')
					)
				),
			),
            array(
				'type' => 'switch',
				'label' => $this->l('Page loading'),
				'name' => 'pg_please_waiting',
                'default' =>'1',
				'values' => array(
					array(
						'id' => 'active_on',
						'value' => 1,
						'label' => $this->l('Yes')
					),
					array(
						'id' => 'active_off',
						'value' => 0,
						'label' => $this->l('No')
					)
				),
			),
            array(
				'type' => 'switch',
				'label' => $this->l('"Page loading" only show home page'),
				'name' => 'pg_pleasewait_onlyshowhome',
                'default' =>'1',
				'values' => array(
					array(
						'id' => 'active_on',
						'value' => 1,
						'label' => $this->l('Yes')
					),
					array(
						'id' => 'active_off',
						'value' => 0,
						'label' => $this->l('No')
					)
				),
			),
            array(
    			'type'=>'textarea',
    			'label' => $this->l('"Page loading" text'),
    			'name' => 'pg_text_page_loading',
                'lang' => true,
    			'cols' => 60,
    			'rows' => 5,
                'default' =>'<h2>CLAUE Theme Prestashop</h2>',
        	),
		);
        $product_custom=array(
            /*Product Page option*/
                array(
    					'type' => 'select',
    					'label' => $this->l('Choose product layout'),
    					'name' => 'product_layout',
                        'default' =>'p_standar',
    					'options' => array(
    						'query' => $options = array(
    							array(
    								'id_option' => 'p_standar',
    								'name' => 'Standard layout'
    							),
                                array(
    								'id_option' => 'p_gallery_thumb',
    								'name' => 'product gallery thumbnail'
    							),
                                array(
    								'id_option' => 'p_sticky_info',       
    								'name' => 'Product sticky info'
    							),
    							
                                array(
    								'id_option' => 'p_images_swatch',
    								'name' => 'images syncing vertical'
    							),
                                array(
    								'id_option' => 'p_img_swatch_hoz',       
    								'name' => 'images syncing hoz'
    							),
    							array(
    								'id_option' => 'p_thumb_right',
    								'name' => 'thumbnail on right'
    							),
                                array(
    								'id_option' => 'p_thumb_bottom',
    								'name' => 'thumbnail on bottom'
    							),
                                array(
    								'id_option' => 'p_thumb_outside',       
    								'name' => 'thumbnail outside'
    							),
                                array(
    								'id_option' => 'p_sticky2',       
    								'name' => 'Product sticky info 2'
    							),
    						),
    						'id' => 'id_option',
    						'name' => 'name'
    				 )
    			),
                array(
    				'type' => 'switch',
    				'label' => $this->l('Enable Zoom when hover image'),
    				'name' => 'product_zoom_image',
                    'default' =>'1',
    				'values' => array(
    					array(
    						'id' => 'active_on',
    						'value' => 1,
    						'label' => $this->l('Yes')
    					),
    					array(
    						'id' => 'active_off',
    						'value' => 0,
    						'label' => $this->l('No')
    					)
    				),
    			),
                array(
    				'type' => 'switch',
    				'label' => $this->l('Page size Full'),
    				'name' => 'product_full_size',
                    'default' =>'0',
    				'values' => array(
    					array(
    						'id' => 'active_on',
    						'value' => 1,
    						'label' => $this->l('Yes')
    					),
    					array(
    						'id' => 'active_off',
    						'value' => 0,
    						'label' => $this->l('No')
    					)
    				),
    			),
                array(
    				'type' => 'switch',
    				'label' => $this->l('Disable/ Enable Reference label'),
    				'name' => 'show_reference_product',
                    'default' =>'0',
    				'values' => array(
    					array(
    						'id' => 'active_on',
    						'value' => 1,
    						'label' => $this->l('Yes')
    					),
    					array(
    						'id' => 'active_off',
    						'value' => 0,
    						'label' => $this->l('No')
    					)
    				),
    			),
                array(
    				'type' => 'switch',
    				'label' => $this->l('Disable/ Enable Discount price'),
    				'name' => 'show_discount_price',
                    'default' =>'0',
    				'values' => array(
    					array(
    						'id' => 'active_on',
    						'value' => 1,
    						'label' => $this->l('Yes')
    					),
    					array(
    						'id' => 'active_off',
    						'value' => 0,
    						'label' => $this->l('No')
    					)
    				),
    			),
                array(
    				'type' => 'switch',
    				'label' => $this->l('Disable/ Enable category parent'),
    				'name' => 'show_cat_parent',
                    'default' =>'1',
    				'values' => array(
    					array(
    						'id' => 'active_on',
    						'value' => 1,
    						'label' => $this->l('Yes')
    					),
    					array(
    						'id' => 'active_off',
    						'value' => 0,
    						'label' => $this->l('No')
    					)
    				),
    			),
                array(
    				'type' => 'switch',
    				'label' => $this->l('Disable/ Enable Tag product'),
    				'name' => 'show_tag_product',
                    'default' =>'1',
    				'values' => array(
    					array(
    						'id' => 'active_on',
    						'value' => 1,
    						'label' => $this->l('Yes')
    					),
    					array(
    						'id' => 'active_off',
    						'value' => 0,
    						'label' => $this->l('No')
    					)
    				),
    			),
                array(
    				'type' => 'switch',
    				'label' => $this->l('Circle button Attribute'),
    				'name' => 'attribute_square',
                    'default' =>'0',
    				'values' => array(
    					array(
    						'id' => 'active_on',
    						'value' => 1,
    						'label' => $this->l('Yes')
    					),
    					array(
    						'id' => 'active_off',
    						'value' => 0,
    						'label' => $this->l('No')
    					)
    				),
    			),                
                array(
    				'type' => 'select',
    				'label' => $this->l('BlockCart after addcart'),
    				'name' => 'order_mini_cart',
                    'default' =>'minicart',
					'options' => array(
						'query' => $options = array(
							array(
								'id_option' => 'minicart',
								'name' => 'Minicart'
							),
						),
						'id' => 'id_option',
						'name' => 'name'
				    ),
    			),                
            /**/
        );
        $category_custom = array(
            array(
					'type' => 'select',
					'label' => $this->l('Category page layout'),
					'name' => 'category_page_layout',
                    'default' =>'cat_standa',
					'options' => array(
						'query' => $options = array(
							array(
								'id_option' => 'cat_standa',       
								'name' => 'Standard layout'
							),
							array(
								'id_option' => 'cat_metro',
								'name' => 'Metro layout'
							),
                            array(
								'id_option' => 'cat_masonry',
								'name' => 'Masonry layout'
							),
						),
						'id' => 'id_option',
						'name' => 'name'
				 )
			),
            array(
					'type' => 'select',
					'label' => $this->l('Number product in line on category page'),
					'name' => 'number_product_in_line_on_category_page',
                    'default' =>'4',
					'options' => array(
						'query' => $options = array(
							array(
								'id_option' => '2',       
								'name' => '2'
							),
							array(
								'id_option' => '3',
								'name' => '3'
							),
                            array(
								'id_option' => '4',
								'name' => '4'
							),
                            array(
								'id_option' => '6',
								'name' => '6'
							)
						),
						'id' => 'id_option',
						'name' => 'name'
				 )
			),
            array(
    				'type' => 'select',
    				'label' => $this->l('Page size Full'),
    				'name' => 'category_full_size',
                    'default' =>'category_full_width',
    				'options' => array(
						'query' => $options = array(
							array(
								'id_option' => 'category_full_width',       
								'name' => 'Full width'
							),
							array(
								'id_option' => 'category_left',
								'name' => 'Sidebar Left'
							),
                            array(
								'id_option' => 'category_right',
								'name' => 'Sidebar Right'
							),
                            array(
								'id_option' => 'category_left_right',
								'name' => 'Sidebar Left and Right'
							)
						),
                        
						'id' => 'id_option',
						'name' => 'name',
                    )
    			),
            array(
				'type' => 'switch',
				'label' => $this->l('Enable Quick View'),
				'name' => 'enb_quickview_onlist',
                'default' =>'1',
				'values' => array(
					array(
						'id' => 'active_on',
						'value' => 1,
						'label' => $this->l('Yes')
					),
					array(
						'id' => 'active_off',
						'value' => 0,
						'label' => $this->l('No')
					)
				),
			),
            array(
				'type' => 'switch',
				'label' => $this->l('Enable sub categories'),
				'name' => 'enb_subcategories',
                'default' =>'1',
				'values' => array(
					array(
						'id' => 'active_on',
						'value' => 1,
						'label' => $this->l('Yes')
					),
					array(
						'id' => 'active_off',
						'value' => 0,
						'label' => $this->l('No')
					)
				),
			),
            array(
				'type' => 'switch',
				'label' => $this->l('Load more listing product'),
				'name' => 'load_more_listing_product',
                'default' =>'1',
				'values' => array(
					array(
						'id' => 'active_on',
						'value' => 1,
						'label' => $this->l('Yes')
					),
					array(
						'id' => 'active_off',
						'value' => 0,
						'label' => $this->l('No')
					)
				),
			),
        );
        $contact_custom_tab = array(
            array(
    			'type'=>'textarea',
    			'label' => $this->l('Text Information'),
    			'name' => 'TEA_CT_INFOR',
    			'cols' => 100,
    			'rows' => 5,
                'default' =>'We love to hear from you on our customer service, merchandise, website or any topics you want to share with us. Your comments and suggestions will be appreciated. Please complete the form below.',
        	),
            array(
    			'type'=>'text',
    			'label' => $this->l('Address'),
    			'name' => 'TEA_CT_ADDRESS',
                'default' =>'184 Main Rd E, St Albans Victoria 3021, Australia',
        	),
            array(
    			'type'=>'text',
    			'label' => $this->l('Phone Number label'),
    			'name' => 'TEA_CT_PHONENUMBER',
                'default' =>'00123-456-789 / 00987-654-321',
        	),
            array(
    			'type'=>'text',
    			'label' => $this->l('Phone Number Call when click'),
    			'name' => 'TEA_CT_PHONENUMBER_CALL',
                'default' =>'00123-456-789',
        	),
            array(
    			'type'=>'text',
    			'label' => $this->l('Open Time'),
    			'name' => 'TEA_CT_OPENTIME',
                'default' =>'Everyday 9:00-17:00',
        	)
        );
        $css_js_custom=array(
        	array(
    			'type'=>'textarea',
    			'label' => $this->l('CSS Code'),
    			'name' => 'TEA_CSS_CUSTOM',
    			'cols' => 100,
    			'rows' => 10,
                'default' =>'',
        	)
        );
        $this->fields_config[0]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => '<span class="label label-success">' . $this->l('General') . '</span>',
                'icon' => 'icon-cogs'
            ),
            'input' => $general_fields,
            'submit' => array('title' => $this->l('Save'), 'class' => 'button btn btn-danger')
        );
        $this->fields_config[1]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => '<span class="label label-success">' . $this->l('Option to product page') . '</span>',
                'icon' => 'icon-cogs'
            ),
            'input' => $product_custom,
            'submit' => array('title' => $this->l('Save'), 'class' => 'button btn btn-danger')
        );
        $this->fields_config[2]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => '<span class="label label-success">' . $this->l('Option to category page') . '</span>',
                'icon' => 'icon-cogs'
            ),
            'input' => $category_custom,
            'submit' => array('title' => $this->l('Save'), 'class' => 'button btn btn-danger'),
        );
        $this->fields_config[3]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => '<span class="label label-success">' . $this->l('Contact page') . '</span>',
                'icon' => 'icon-cogs'
            ),
            'input' => $contact_custom_tab,
            'submit' => array('title' => $this->l('Save'), 'class' => 'button btn btn-danger'),
        );
        $this->fields_config[4]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => '<span class="label label-success">' . $this->l('Field for developer') . '</span>',
                'icon' => 'icon-cogs'
            ),
            'input' => $css_js_custom,
            'submit' => array('title' => $this->l('Save'), 'class' => 'button btn btn-danger'),
            'desc' => $this->l('Add your custom css code'),
        );
    }
    public function hookHeader() {
        $css=array(
            Configuration::get('TEA_LOGO_HEIGHT'),
            Configuration::get('TEA_LOGO_WIDTH'),
            Configuration::get('TEA_SKIN_COLOR'),
            Configuration::get('TEA_TITLE_COLOR'),
            Configuration::get('TEA_MAIN_COLOR'),
            Configuration::get('TEA_BUTTON_COLOR'),
            Configuration::get('TEA_BUTTON_BACKGROUND'),
            Configuration::get('TEA_BUTTON_COLOR_HOVER'),
            Configuration::get('TEA_BUTTON_BACKGROUND_HOVER'),
            Configuration::get('TEA_BUTTON_COLOR_SECOND'),
            Configuration::get('TEA_BUTTON_BACKGROUND_SECOND'),
            Configuration::get('TEA_BUTTON_COLOR_SECOND_HOVER'),
            Configuration::get('TEA_BUTTON_BACKGROUND_SECOND_HOVER'),
            Configuration::get('TEA_PRICE_COLOR'),
            Configuration::get('TEA_NEW_BG'),
            Configuration::get('TEA_SALE_BG'),
        );
        $css[] = Configuration::get('TEA_FONTS_TITLE');
        $css[] = Configuration::get('TEA_FONTS_MAIN');
        $this->context->controller->addCSS('https://fonts.googleapis.com/css?family='.urlencode(Configuration::get('TEA_FONTS_TITLE'))); 
        $globalsCSS = @file_exists(dirname(__FILE__).'/views/css/global.css') && @is_readable(dirname(__FILE__).'/views/css/global.css') ? Tools::file_get_contents(dirname(__FILE__).'/views/css/global.css') : '';        
        $customCSS = trim(Configuration::get('TEA_CSS_CUSTOM'));
        $css_config =  ($globalsCSS || $customCSS) ? str_replace(array('logo_width','logo_height','skin_color','title_color','main_color','button_color','button_background_color','button_hover_color','button_background_hover','button_second_color','button_second_background_color','button_second_hover_color','button_background_second_hover','price_color','new_label_bg','sale_label_bg','fonts_title','fonts_main'),$css,$globalsCSS."\n".$customCSS) : '';
        $this->context->smarty->assign(
            array(
               'img_lang_dir'=>_THEME_LANG_DIR_ ,
               'css_config'=>$css_config,
               'urls' => $this->getTemplateVarUrls(),
               'show_category_banner' =>Configuration::get('show_category_banner'),
               'show_discount_price' =>Configuration::get('show_discount_price'),
               'show_tag_product' =>Configuration::get('show_tag_product'),
               'product_layout' =>Configuration::get('product_layout'),
               'order_mini_cart' =>Configuration::get('order_mini_cart'),
               'attribute_square' =>Configuration::get('attribute_square'),
               'product_zoom_image' =>Configuration::get('product_zoom_image'),
               'product_full_size' =>Configuration::get('product_full_size'),
               'category_full_size' =>Configuration::get('category_full_size'),
               'category_page_layout' =>Configuration::get('category_page_layout'),
               'enb_quickview_onlist' =>Configuration::get('enb_quickview_onlist'),
               'enb_subcategories' =>Configuration::get('enb_subcategories'),
               'show_cat_parent' =>Configuration::get('show_cat_parent'),
               'TEA_CT_INFOR' =>Configuration::get('TEA_CT_INFOR'),
               'TEA_CT_ADDRESS' =>Configuration::get('TEA_CT_ADDRESS'),
               'modeDeveloper' => $this->modeDeveloper, 
               'TEA_CT_PHONENUMBER' =>Configuration::get('TEA_CT_PHONENUMBER'),
               'TEA_CT_PHONENUMBER_CALL' =>Configuration::get('TEA_CT_PHONENUMBER_CALL'),
               'TEA_CT_OPENTIME' =>Configuration::get('TEA_CT_OPENTIME'), 
               'pg_please_waiting' =>Configuration::get('pg_please_waiting'),
               'pg_pleasewait_onlyshowhome' =>Configuration::get('pg_pleasewait_onlyshowhome'),
               'pg_text_page_loading' =>Configuration::get('pg_text_page_loading',$this->context->language->id),
               'show_reference_product' =>Configuration::get('show_reference_product'),
               'pg_header_sticky' => Configuration::get('pg_header_sticky'),
               'pg_background_breadcrum' => $this->_path.'/images/config/'.Configuration::get('tea_background_breadcrum'),
               'number_product_in_line_on_category_page' =>Configuration::get('number_product_in_line_on_category_page'),
            )
        );
        return $this->display(__FILE__,'header.tpl');
    }
    public function getTemplateVarUrls()
    {
        $http = Tools::getCurrentUrlProtocolPrefix();
        $base_url = $this->context->shop->getBaseURL(true, true);

        $urls = array(
            'base_url' => $base_url,
            'current_url' => $this->context->shop->getBaseURL(true, false).$_SERVER['REQUEST_URI'],
            'shop_domain_url' => $this->context->shop->getBaseURL(true, false),
        );

        $assign_array = array(
            'img_ps_url' => _PS_IMG_,
            'img_cat_url' => _THEME_CAT_DIR_,
            'img_lang_url' => _THEME_LANG_DIR_,
            'img_prod_url' => _THEME_PROD_DIR_,
            'img_manu_url' => _THEME_MANU_DIR_,
            'img_sup_url' => _THEME_SUP_DIR_,
            'img_ship_url' => _THEME_SHIP_DIR_,
            'img_store_url' => _THEME_STORE_DIR_,
            'img_col_url' => _THEME_COL_DIR_,
            'img_url' => _THEME_IMG_DIR_,
            'css_url' => _THEME_CSS_DIR_,
            'js_url' => _THEME_JS_DIR_,
            'pic_url' => _THEME_PROD_PIC_DIR_,
        );

        foreach ($assign_array as $assign_key => $assign_value) {
            if (substr($assign_value, 0, 1) == '/' || $this->ssl) {
                $urls[$assign_key] = $http.Tools::getMediaServer($assign_value).$assign_value;
            } else {
                $urls[$assign_key] = $assign_value;
            }
        }

        $pages = array();
        $p = array(
            'address', 'addresses', 'authentication', 'cart', 'category', 'cms', 'contact',
            'discount', 'guest-tracking', 'history', 'identity', 'index', 'my-account',
            'order-confirmation', 'order-detail', 'order-follow', 'order', 'order-return',
            'order-slip', 'pagenotfound', 'password', 'pdf-invoice', 'pdf-order-return', 'pdf-order-slip',
            'prices-drop', 'product', 'search', 'sitemap', 'stores', 'supplier',
        );
        foreach ($p as $page_name) {
            $index = str_replace('-', '_', $page_name);
            $pages[$index] = $this->context->link->getPageLink($page_name, true);
        }
        $pages['register'] = $this->context->link->getPageLink('authentication', true, null, array('create_account' => '1'));
        $pages['order_login'] = $this->context->link->getPageLink('order', true, null, array('login' => '1'));
        $urls['pages'] = $pages;

        $urls['theme_assets'] = __PS_BASE_URI__.'themes/'.$this->context->shop->theme->getName().'/assets/';

        $urls['actions'] = array(
            'logout' => $this->context->link->getPageLink('index', true, null, 'mylogout'),
        );

        return $urls;
    }
    public function hookDisplayBackOfficeHeader()
    {
        if(Tools::getValue('configure')=='tea_themeconfig')
        {
            $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/admin.css','all');
        }
    }
}