<?php
/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
*/

class TeaRow extends ObjectModel
{	
	public $id_prof;
	public $title;	
	public $class;
	public $fullwidth;
	public $active;
	public $ordering;		
    public $padding;
    public $margin;
	public static $definition = array(
		'table' => 'teaadv_rows',
		'primary' => 'id_row',		
		'fields' => array(						
			'id_prof'		=>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
			'title'			=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
			'class'			=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => false, 'size' => 200),
			'fullwidth'		=>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			'active'		=>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			'ordering'		=>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'padding'       => array('type'=>   self::TYPE_STRING,'validate' => 'isCleanHtml'),
            'margin'       => array('type'=>   self::TYPE_STRING,'validate' => 'isCleanHtml'),
		)
	);
	public	function __construct($id_row = null, $id_lang = null)
	{
		parent::__construct($id_row, $id_lang);
	}
	public function add($autodate = true, $null_values = false)
	{
		$res = true;
		$res &= parent::add($autodate, $null_values);		
		return $res;
	}
	public function delete()
	{
		$res = true;
		$res &= parent::delete();
		return $res;
	}
}