<form novalidate="" enctype="multipart/form-data" method="post" action="" class="defaultForm form-horizontal" id="module_form">
	<div id="fieldset_0" class="panel">
        <div class="panel-heading">
            <i class="icon-envelope"></i>{l s='Install demo' mod='ybc_themelayout'} 
        </div>
        <div class="form-wrapper">
            <div class="form-group">
                <label class="control-label col-lg-3 required"> {l s='Select layout' mod='ybc_themelayout'} </label>
                <div class="col-lg-4">
                    <select name="theme_layout_demo" id="theme_layout_demo">
                        <option value="">{l s='--Select layout--' mod='ybc_themelayout'}</option>
                        <option value="hand made">{l s='hand made' mod='ybc_themelayout'}</option>
                        <option value="home fashion 1">{l s='Home fashion 1' mod='ybc_themelayout'}</option>
                        <option value="home fashion 2">{l s='home fashion 2' mod='ybc_themelayout'}</option>
                        <option value="layout_3">{l s='layout 3' mod='ybc_themelayout'}</option>
                        <option value="Layout 4 - oganic store">{l s='Layout 4 - oganic store' mod='ybc_themelayout'}</option>
                        <option value="layout 5- funiture">{l s='layout 5- funiture' mod='ybc_themelayout'}</option>
                        <option value="layout 6- vertical slider">{l s='layout 6- vertical slider' mod='ybc_themelayout'}</option>
                    </select>
                    
                </div>
            </div>
        </div>
        <div class="panel-footer">
        <button id="module_form_submit_btn" class="btn btn-default pull-right" type="submit" value="1" name="btnSubmitInstallDemo">
            <i class="icon-plus-sign-alt"></i>
            {l s='Install' mod='ybc_themelayout'}
        </button>
        </div>
    </div>		
</form>