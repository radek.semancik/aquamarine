{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !$ajax}
<div class="panel">
	<div class="profile-title">
		<i class="icon-list-ul"></i> {l s='Row list' mod='tea_themelayout'} - {$name_prof|escape:'html':'UTF-8'}
		<span class="pull-right">
			<a class="save_pos" title="Save Layout Grid">
				<i class="icon-save"></i>
			</a>
			<a id="desc-product-new" class="add-new-row list-toolbar-btn" href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&id_prof={$id_prof|escape:'html':'UTF-8'}&addRow=1" title="Add Row">			
				<i class="icon-plus"></i>
			</a>
		</span>			
		<span class="btn-group-action pull-right devices-layout">
			<div class="dropdown btn-group">
				<a class="dropdown-toggle  btn btn-default" id="dLabel" role="button" data-toggle="dropdown" data-target="#">										
					Device width <b class="caret"></b>
				</a>
				<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
					<li><a class="btn btn-success switch-lg" data-device="lg"><i class="icon-desktop"></i>Desktop</a></li>
					<li><a class="btn btn-default switch-md" data-device="md"><i class="icon-desktop"></i>Table Medium</a></li>
					<li><a class="btn btn-default switch-sm" data-device="sm"><i class="icon-tablet"></i>Tablet small</a></li>
					<li><a class="btn btn-default switch-xs" data-device="xs"><i class="icon-mobile"></i>Mobile</a></li>
				</ul>
			</div>
		</span>
	</div>
	<div id="rowlist" class="col-lg">
{/if}
	<div class="rowlist">
		{foreach from=$rows key=i item=row}
				<div id="row_{$row.id_row|escape:'html':'UTF-8'}" class="adv-row container">
					<div class="row-title">
						<div class="col-lg-1">
							<span><i class="icon-arrows"></i></span>
						</div>						
						<div class="col-md-11">
							<div class="pull-left">{l s='Row' mod='tea_themelayout'} : {$row.title|escape:'html':'UTF-8'}</div>							
							<span class="btn-group-action pull-right">
                                <div class="list-action">
    								<ul>
                                        <li><a class="btn {if $row.active}btn-success{else}btn-danger{/if}" href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&changeRowStatus&id_prof={$id_prof|escape:'html':'UTF-8'}&id_row={$row.id_row|escape:'html':'UTF-8'}" title="{if $row.active}Enabled{else}Disabled{/if}"><i class="{if $row.active}icon-check{else}icon-remove{/if}"></i></a>	</li>
    									<li>
    										<span class="btn btn-default add_position" data-prof="{$id_prof|escape:'html':'UTF-8'}" data-row="{$row.id_row|escape:'html':'UTF-8'}" href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&id_prof={$id_prof|escape:'html':'UTF-8'}&id_row={$row.id_row|escape:'html':'UTF-8'}&addPosition=1" title="{l s='Add Position' mod='tea_themelayout'}"><i class="icon-plus"></i></span>
    									</li>	
    									<li>
    										<span class="btn btn-default edit-row" title="{l s='Edit row' mod='tea_themelayout'}" href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&id_prof={$id_prof|escape:'html':'UTF-8'}&id_row={$row.id_row|escape:'html':'UTF-8'}"><i class="icon-edit"></i></span>
    									</li>
    									<li>
    										<a class="btn btn-default"	href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&id_prof={$id_prof|escape:'html':'UTF-8'}&delete_id_row={$row.id_row|escape:'html':'UTF-8'}" onclick="return confirm('Are you sure you want to delete this row?');" title="{l s='Delete row' mod='tea_themelayout'}"><i class="icon-trash"></i></a>
    									</li>	
    								</ul>
    							</div>
                            </span>
						</div>
					</div>
					<div class="row-positions row">								
						{foreach from=$row.positions key=j item=position}
						<div class="position col-lg-{$position.col_lg|escape:'html':'UTF-8'} col-md-{$position.col_md|escape:'html':'UTF-8'} col-sm-{$position.col_sm|escape:'html':'UTF-8'} col-xs-{$position.col_xs|escape:'html':'UTF-8'}" id="position_{$position.id_position|escape:'html':'UTF-8'}" data-id-pos="{$position.id_position|escape:'html':'UTF-8'}" data-col-lg="{$position.col_lg|escape:'html':'UTF-8'}" data-col-md="{$position.col_md|escape:'html':'UTF-8'}" data-col-sm="{$position.col_sm|escape:'html':'UTF-8'}" data-col-xs="{$position.col_xs|escape:'html':'UTF-8'}"><div class="position-inner">
							<div class="position-title">	
								#{$position.id_position|escape:'html':'UTF-8'} - {$position.title|escape:'html':'UTF-8'}
								<span class="btn-group-action pull-right">
									<div class="list-action btn-group">
										<ul>
                                            <li><a class="btn {if $position.active}btn-success{else}btn-danger{/if}" href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&changePositionStatus&id_prof={$id_prof|escape:'html':'UTF-8'}&id_row={$position.id_row|escape:'html':'UTF-8'}&id_position={$position.id_position|escape:'html':'UTF-8'}" title="{if $position.active}Enabled{else}Disabled{/if}"><i class="{if $position.active}icon-check{else}icon-remove{/if}"></i></a></li>																						
											<li>
												<span class="btn btn-default add_block" title="{l s='Add Block' mod='tea_themelayout'}" href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&id_prof={$id_prof|escape:'html':'UTF-8'}&id_position={$position.id_position|escape:'html':'UTF-8'}&addBlock=1"><i class="icon-plus"></i></span>
											</li>
											<li>
												<span class="btn btn-default edit_position" title="{l s='Edit position' mod='tea_themelayout'}" data-prof="{$id_prof|escape:'html':'UTF-8'}" data-row="{$row.id_row|escape:'html':'UTF-8'}" href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&id_prof={$id_prof|escape:'html':'UTF-8'}&edit_position&id_row={$row.id_row|escape:'html':'UTF-8'}&id_position={$position.id_position|escape:'html':'UTF-8'}">
													<i class="icon-edit"></i>
												</span>
											</li>
											<li>
												<a class="btn btn-default" title="{l s='Delete position' mod='tea_themelayout'}"	href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&id_prof={$id_prof|escape:'html':'UTF-8'}&delete_id_position={$position.id_position|escape:'html':'UTF-8'}" onclick="return confirm('Are you sure you want to delete this position?');">
													<i class="icon-trash"></i>
												</a>
											</li>
										</ul>
									</div>
								</span>
							</div>
							<div class="pos-blocks">								
								{foreach from=$position.blocks item=block}
								<div class="block" id="block_{$block.id_block|escape:'html':'UTF-8'}">
									<div class="block-inner">										
										{$block.title|escape:'html':'UTF-8'}{if $block.block_type == 'custom_html'} (Html){/if}
										<span class="btn-group-action pull-right">
											<div class="list-action btn-group">
    											<ul>
                                                    <li><a class="btn {if $block.active}btn-success{else}btn-danger{/if}" href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&changeBlockStatus&id_prof={$id_prof|escape:'html':'UTF-8'}&id_block={$block.id_block|escape:'html':'UTF-8'}" title="{if $block.active}Enabled{else}Disabled{/if}"><i class="{if $block.active}icon-check{else}icon-remove{/if}"></i> </a></li>	
    												<li>
    													<span class="btn btn-default edit_block" title="{l s='Edit block' mod='tea_themelayout'}" href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&edit_block&id_prof={$id_prof|escape:'html':'UTF-8'}&id_position={$position.id_position|escape:'html':'UTF-8'}&id_block={$block.id_block|escape:'html':'UTF-8'}"><i class="icon-edit"></i></span>
    												</li>
    												<li>
    													<a class="btn btn-default" title="{l s='Delete block' mod='tea_themelayout'}"	href="{$adminlink|escape:'html':'UTF-8'}&configure=tea_themelayout&id_prof={$id_prof|escape:'html':'UTF-8'}&delete_id_block={$block.id_block|escape:'html':'UTF-8'}" onclick="return confirm('Are you sure you want to delete this block?');"><i class="icon-trash"></i></a>
    												</li>
    											</ul>
											</div>		
										</span>
									</div>
								</div>
								{/foreach}
							</div>
						</div></div>
						{/foreach}						
					</div>
				</div>
			{/foreach}		
	</div>
{if !$ajax}
    </div>
</div>
<input id="current_url" type="hidden" name="current_url" value="{$current_url|escape:'html':'UTF-8'}" />
<div id="block-form" style="">
</div>
<script type="text/javascript">
var edit_text ='{l s='Edit' mod='tea_megamenu'}';
var add_item_text ='{l s='Add item' mod='tea_megamenu'}';
var delete_text ='{l s='Delete' mod='tea_megamenu'}';
var delete_question ='{l s='Are you sure delete item?' mod='tea_megamenu'}';
var enabled_text ='{l s='Enabled' mod='tea_megamenu'}';
var disable_text ='{l s='Disable' mod='tea_megamenu'}';
var successful_deletion ='{l s='Successful deletion' mod='tea_megamenu'}';
var successful_creation  ='{l s='Successful creation' mod='tea_megamenu'}';
var successful_update  = '{l s='Successful update' mod='tea_megamenu'}'; 
$(document).on('click', '.tea_close_popup', function(){
        $('#block-form').hide();
});
$(document).on('click','button[name="submitCancelAddForm"],button[name="submitCancelBlock"],button[name="submitCancelPosition"]',function(e){
    e.preventDefault();
    $('#block-form').hide();
});
$(document).on('click','.add-new-row,.edit-row',function(e){
    e.preventDefault();
    $.ajax({
		type: 'POST',
		headers: { "cache-control": "no-cache" },
		url: $(this).attr('href'),
		async: true,
		cache: false,
		dataType : "json",
		data: 'ajax=1',
		success: function(jsonData)
		{
		     $('#block-form').show();
		     $('#block-form').html('<span class="tea_close_popup"></span>'+jsonData.block_form);
             $('.panel-footer a.btn-default').removeAttr('onclick');
		}
	});
});
$(document).on('click','button[name="submitRow"]',function(e){
    e.preventDefault();
    var formData = new FormData($(this).parents('form').get(0));
    $.ajax({
        url: $(this).parents('form').eq(0).attr('action'),
        data: formData,
        type: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(json){
            if(json.error)
            {
                $.growl.error({ message: json.errors }); 
            }                
            else
            {
                $('#block-form').hide();
                $.growl.notice({ message: successful_creation });
                $('#rowlist').html(json.list_prof_block);   
            }
        },
        error: function(xhr, status, error)
        {
            var err = eval("(" + xhr.responseText + ")");     
            $.growl.error({ message: err.Message });               
        }
    });         
});
$(document).on('click','.add_position,.edit_position',function(e){
    e.preventDefault();
    var id_row= $(this).attr('data-row');
    $.ajax({
		type: 'POST',
		headers: { "cache-control": "no-cache" },
		url: $(this).attr('href'),
		async: true,
		cache: false,
		dataType : "json",
		data: 'ajax=1',
		success: function(jsonData)
		{
		     $('#block-form').show();
		     $('#block-form').html('<span class="tea_close_popup"></span>'+jsonData.block_form);
             $('.panel-footer a.btn-default').removeAttr('onclick');
		}
	});
});
$(document).on('click','button[name="submitPosition"]',function(e){
    e.preventDefault();
    var formData = new FormData($(this).parents('form').get(0));
    $.ajax({
        url: $(this).parents('form').eq(0).attr('action'),
        data: formData,
        type: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(json){
            if(json.error)
            {
                $.growl.error({ message: json.errors }); 
            }                
            else
            {
                $('#block-form').hide();
                $.growl.notice({ message: successful_creation });
                $('#rowlist').html(json.list_prof_block);   
            }
        },
        error: function(xhr, status, error)
        {
            var err = eval("(" + xhr.responseText + ")");     
            $.growl.error({ message: err.Message });               
        }
    });         
});
$(document).on('click','.add_block,.edit_block',function(e){
    e.preventDefault();
    var id_row= $(this).attr('data-row');
    $.ajax({
		type: 'POST',
		headers: { "cache-control": "no-cache" },
		url: $(this).attr('href'),
		async: true,
		cache: false,
		dataType : "json",
		data: 'ajax=1',
		success: function(jsonData)
		{
		     $('#block-form').show();
		     $('#block-form').html('<span class="tea_close_popup"></span>'+jsonData.block_form);
             $('.panel-footer a.btn-default').removeAttr('onclick');
             _initload();
		}
	});
});
$(document).on('click','button[name="submitBlock"]',function(e){
    e.preventDefault();
    tinyMCE.triggerSave();
    var formData = new FormData($(this).parents('form').get(0));
    $.ajax({
        url: $(this).parents('form').eq(0).attr('action'),
        data: formData,
        type: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(json){
            if(json.error)
            {
                $.growl.error({ message: json.errors }); 
            }                
            else
            {
                $('#block-form').hide();
                $.growl.notice({ message: successful_creation });
                $('#rowlist').html(json.list_prof_block);   
            }
        },
        error: function(xhr, status, error)
        {
            var err = eval("(" + xhr.responseText + ")");     
            $.growl.error({ message: err.Message });               
        }
    });         
});
</script>
{/if}