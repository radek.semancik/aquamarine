{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !$ajax}
<div id="rows" class="content_builder_tab header active">
{/if}
		<div id="homepage_list" class="homepage">
			{foreach from=$homepages key=i item=homepage}
				<div id="homepage_{$homepage.id_homepage|escape:'html':'UTF-8'}" class="panel">
					<div class="row">
						<div class="col-lg-1">
							<span><i class="icon-arrows "></i></span>
						</div>
						<div class="col-md-11">
							<h4 class="pull-left">#{$homepage.id_homepage|escape:'html':'UTF-8'} - {$homepage.title|escape:'html':'UTF-8'}</h4>
							<div class="btn-group-action pull-right">
                                <a class="btn btn-default{if $TEAADV_HOMEPAGE==$homepage.id_homepage} active{/if}" href="{$link->getAdminLink('AdminTea_themelayoutHomepage')|escape:'html':'UTF-8'}&configure=tea_themelayout&active_id_homepage={$homepage.id_homepage|escape:'html':'UTF-8'}">
                                    {if $TEAADV_HOMEPAGE==$homepage.id_homepage}
                                        <i class="icon-star" style="font-size: 11px;"></i>
                                    {else}
                                        <i class="icon-star-o" style="font-size: 11px;"></i>
                                    {/if}
                                </a>
                                <a class="btn btn-default" target="_blank" 
									href="{$link->getBaseLink()|escape:'html':'UTF-8'}{$iso_code}/?getdemo&homepage_id={$homepage.id_homepage|escape:'html':'UTF-8'}">
									<i class="material-icons" style="font-size: 11px;">remove_red_eye</i>
									{l s='Preview' mod='tea_themelayout'}
								</a>							
								<a class="btn btn-default" style="display:none;"
									href="{$link->getAdminLink('AdminTea_themelayoutHomepage')|escape:'html':'UTF-8'}&configure=tea_themelayout&export_id_homepage={$homepage.id_homepage|escape:'html':'UTF-8'}">
									<i class="icon-download"></i>
									{l s='Export To XML' mod='tea_themelayout'}
								</a>	
								<span class="btn btn-default edit_homepage" href="{$link->getAdminLink('AdminTea_themelayoutHomepage')|escape:'html':'UTF-8'}&configure=tea_themelayout&edit_id_homepage={$homepage.id_homepage|escape:'html':'UTF-8'}&id_homepage={$homepage.id_homepage|escape:'html':'UTF-8'}">
									<i class="icon-edit"></i>
									{l s='Edit' mod='tea_themelayout'}
								</span>
								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminTea_themelayoutHomepage')|escape:'html':'UTF-8'}&configure=tea_themelayout&delete_id_homepage={$homepage.id_homepage|escape:'html':'UTF-8'}" onclick="return confirm('Are you sure you want to delete this item?');">
		          						<i class="icon-trash"></i>
									{l s='Delete' mod='tea_themelayout'}
								</a>							
							</div>
						</div>
					</div>
				</div>
			{/foreach}
		</div>
        <a id="add_new_homepage" class="addnew addnew_header" href="{$link->getAdminLink('AdminTea_themelayoutHomepage')|escape:'html':'UTF-8'}&configure=tea_themelayout&addHome=1">
		       <i class="icon icon-plus"></i>
		</a>
{if !$ajax}
</div>
<div id="block-form" style="">
</div>
<script type="text/javascript">
var edit_text ='{l s='Edit' mod='tea_megamenu'}';
var add_item_text ='{l s='Add item' mod='tea_megamenu'}';
var delete_text ='{l s='Delete' mod='tea_megamenu'}';
var delete_question ='{l s='Are you sure delete item?' mod='tea_megamenu'}';
var enabled_text ='{l s='Enabled' mod='tea_megamenu'}';
var disable_text ='{l s='Disable' mod='tea_megamenu'}';
var successful_deletion ='{l s='Successful deletion' mod='tea_megamenu'}';
var successful_creation  ='{l s='Successful creation' mod='tea_megamenu'}';
var successful_update  = '{l s='Successful update' mod='tea_megamenu'}'; 
$(document).ready(function(){
    $(document).on('click', '.tea_close_popup', function(){
        $('#block-form').hide();
    });
    $(document).on('click','.panel-footer a.btn-default',function(e){
        e.preventDefault();
        $('#block-form').hide();
    });
    $(document).on('click','#add_new_homepage,.edit_homepage',function(e){
        e.preventDefault();
        $.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: $(this).attr('href'),
			async: true,
			cache: false,
			dataType : "json",
			data: 'ajax=1',
			success: function(jsonData)
			{
			     $('#block-form').show();
			     $('#block-form').html('<span class="tea_close_popup"></span>'+jsonData.block_form);
                 $('.panel-footer a.btn-default').removeAttr('onclick');
			}
		});
   });
   $(document).on('click','button[name="submitHome"]',function(e){
        e.preventDefault();
        var formData = new FormData($(this).parents('form').get(0));
        $.ajax({
            url: $(this).parents('form').eq(0).attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(json){
                if(json.error)
                {
                    $.growl.error({ message: json.errors }); 
                }                
                else
                {
                    $('#block-form').hide();
                    $.growl.notice({ message: successful_creation });
                    $('#rows').html(json.list_home_block);   
                }
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });         
    });
   $(document).on('click','button[name="submitProf"]',function(e){
        e.preventDefault();
        var formData = new FormData($(this).parents('form').get(0));
        $.ajax({
            url: $(this).parents('form').eq(0).attr('action'),
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(json){
                if(json.error)
                {
                    $.growl.error({ message: json.errors }); 
                }                
                else
                {
                    $('#block-form').hide();
                    $.growl.notice({ message: successful_creation });
                    $('#rows').html(json.list_home_block);   
                }
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");     
                $.growl.error({ message: err.Message });               
            }
        });         
    });
});
</script>
{/if}