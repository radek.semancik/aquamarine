/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
**/

(function($) {	
    $.TeaAjaxFunc = function() {
        this.requestData = 'teaajax=1';
    };
    $.TeaAjaxFunc.prototype = {
        processAjax: function() {			
            var myElement = this;      
			myElement.getProductIDs();
			if(myElement.requestData != "teaajax=1"){								
            $.ajax({
                type: 'POST',
                headers: {"cache-control": "no-cache"},
                url: baseDir + 'modules/tea_themelayout/initajax.php' + '?rand=' + new Date().getTime(),
                async: true,
                cache: false,
                dataType: "json",
                data: myElement.requestData,
                success: function(jsonData) {					
                    if (jsonData) {						
						
                        if (jsonData.img2arr) {
                            var listProductImg = new Array();
                            for (i = 0; i < jsonData.img2arr.length; i++) {
                                listProductImg[jsonData.img2arr[i].id] = jsonData.img2arr[i].content;
                            }
                            $(".image-rollover").each(function() {
                                if (listProductImg[$(this).data("id-product")])									
									$( '<img class="img-responsive product-img2" title="" alt="" src="' + listProductImg[$(this).data("id-product")] + '"/>' ).insertAfter($(this).children('.product-img1'));									
                            });
                        }
                    }
                },
                error: function() {
                }
            });
            }
        },
		getProductIDs: function() {            
            var productimgs = "";
            $(".image-rollover").each(function() {
                if (!productimgs)
                    productimgs += $(this).data("id-product");
                else
                    productimgs += "," + $(this).data("id-product");
            });
            if (productimgs) {
                this.requestData += '&productids=' + productimgs;
            }						
            return false;
        }
    };
}(jQuery));

