/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
**/
function _initload() {
	var  block_type = $('#block_type');
	var  html_content = $('.html_content');
	var  assign_module = $('.module');
	var  linklist = $('#linklist');
	if(block_type.val()=='link') {
		linklist.show();
		html_content.hide();
		assign_module.hide();
        $('.parallax').hide();
        $('.list_product').hide();
        $('.widget_block').hide();
        $('.group_slider').hide();
	} else if(block_type.val()=='custom_html') {
		linklist.hide();
        $('.parallax').hide();
        $('.widget_block').hide();
        $('.list_product').hide();
		html_content.show();
		assign_module.hide();
        $('.group_slider').hide();
	}else if(block_type.val()=='parallax') {
		linklist.hide();
		html_content.hide();
        $('.widget_block').hide();
        $('.list_product').hide();
        $('.parallax').show();
		assign_module.hide();
        $('.group_slider').hide();
	}
    else if(block_type.val()=='widget_block') {
		linklist.hide();
		html_content.hide();
        $('.parallax').hide();
		assign_module.hide();
        $('.list_product').hide();
        $('.widget_block').show();
        $('.group_slider').hide();
	}
    else if(block_type.val()=='list_product')
    {
   	    linklist.hide();
		html_content.hide();
        $('.widget_block').hide();
        $('.parallax').hide();
        assign_module.hide();
        $('.list_product').show();
        $('.group_slider').hide();
		if($('#type_product').val()=='category')
        {
            $('.list_product').show();
            $('.list_id_product').hide();
        }
        else if($('#type_product').val()=='addproduct')
        {
            $('.list_product').show();
            $('.id_category').hide();
        }
        else{
            $('.list_id_product').hide();
            $('.id_category').hide();
        }
    }
    else if(block_type.val()=='slider')
    {
        linklist.hide();
		html_content.hide();
        $('.widget_block').hide();
        $('.parallax').hide();
        $('.list_product').hide();
		assign_module.hide();
        $('.group_slider').show();
    }
    else if(block_type.val()=='module'){
		linklist.hide();
		html_content.hide();
        $('.widget_block').hide();
        $('.parallax').hide();
        $('.list_product').hide();
		assign_module.show();
        $('.group_slider').hide();
	}
    else {
		linklist.hide();
		html_content.hide();
        $('.widget_block').hide();
        $('.parallax').hide();
        $('.list_product').hide();
		assign_module.hide();
        $('.group_slider').hide();
	}
}
function _loadhook()
{
   var module_name = $('#module_name').val();
   var id_block =$('#id_block').val();
    $.ajax({
        type: 'POST',
        headers: { "cache-control": "no-cache" },
        url: '../modules/tea_themelayout/ajax.php',
        async: true,
        cache: false,
        dataType : "html",
        data: 'module_name='+module_name+'&id_block='+id_block,
        success: function(jsonData)
        {
            $('#hook_name').html(jsonData);
        }
    });
}
$(document).ready(function() {
	_initload();
    //_loadhook();
    $(document).on('change','#block_type',function(e){
        _initload();
    });
    $(document).on('change','#type_product',function(e){
       if($('#type_product').val()=='category')
        {
           $('.list_product').show();
            $('.list_id_product').hide();
        }
        else if($('#type_product').val()=='addproduct')
        {
            $('.list_product').show();

            $('.id_category').hide();
        }
        else{
            $('.list_id_product').hide();

            $('.id_category').hide();
        } 
    });
});