<?php
/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
*/

if (!defined('_PS_VERSION_'))
	exit;
include_once(_PS_MODULE_DIR_.'tea_themelayout/class/TeaExecute.php');
include_once(_PS_MODULE_DIR_.'tea_themelayout/class/TeaImportExport.php');
include_once(_PS_MODULE_DIR_.'tea_themelayout/controllers/admin/AdminTea_themelayoutBase.php');
class AdminTea_themelayoutHomepageController extends AdminTea_themelayoutBaseController {
	public function __construct()
	{
		$this->name = 'tea_themelayout';
		$this->tab = 'front_office_features';
		$this->bootstrap = true;
		$this->lang = true;
		$this->context = Context::getContext();
		$this->secure_key = Tools::encrypt($this->name);
		parent::__construct();
        if(Tools::isSubmit('ajax'))
            $this->renderList();
	}
	public function renderList()
	{
		$tea_exc = new TeaExecute();
		$this->_html = $this->headerHTML();
		/* Validate & process */
        if(Tools::isSubmit('active_id_homepage')&& $id_homepage =Tools::getValue('active_id_homepage'))
            Configuration::updateValue('TEAADV_HOMEPAGE', $id_homepage);
		if (Tools::isSubmit('submitCancelAddForm'))
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminTea_themelayoutHomepage', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_homepage='.Tools::getValue('id_homepage'));
		elseif (Tools::isSubmit('submitConf'))
		{
			Configuration::updateValue('TEAADV_HOMEPAGE', Tools::getValue('TEAADV_HOMEPAGE'));
            $this->_html .= $this->displayHeaderlist(); 			
			$this->_html .= $this->renderListHomes();
		}
		elseif (Tools::isSubmit('addHome') || (Tools::isSubmit('edit_id_homepage') && $tea_exc->homeExists((int)Tools::getValue('id_homepage'))))
		{
			$this->_html .= $this->renderNavigation();
            if(Tools::isSubmit('ajax'))
                die(Tools::jsonEncode( array(
                    'block_form'=>$this->renderAddHome(),
                )));
            $this->_html .= $this->renderAddHome();
		}
		elseif (Tools::isSubmit('submitHome') || Tools::isSubmit('submitAddconfiguration') || Tools::isSubmit('delete_id_homepage') || (Tools::isSubmit('export_id_homepage') && $tea_exc->homeExists((int)Tools::getValue('export_id_homepage'))))
		{
			if ($this->_postValidation())
			{
				$this->_postProcess();		
                $this->_html .= $this->displayHeaderlist(); 		
				$this->_html .= $this->renderListHome();
			}
			else
			{
				$this->_html .= $this->renderNavigation();
				$this->_html .= $this->renderAddHome();
			}
		}			
		else
		{			
			$view = Tools::getValue('view', 'categories');
			if ($view == 'config')
				$this->_html .= $this->renderConfig();
			else
            {
                $this->_html .= $this->displayHeaderlist(); 
                $this->_html .= $this->renderListHome();
            }	
		}
		return $this->_html;
	}
	private function _postValidation()
	{
		$errors = array();
		$tea_exc = new TeaExecute();
		/*Validation for configuration */	
		if (Tools::isSubmit('submitHome')|| Tools::isSubmit('submitAddconfiguration'))
		{
			if (Tools::strlen(Tools::getValue('title')) > 255)
            {
                if(Tools::getValue('ajax'))
                    die( Tools::jsonEncode(
                          array(
                            'error'=>true,
                            'errors'=>$this->l('The title is too long.'),
                          )  
                    ));
                $errors[] = $this->l('The title is too long.');
            }
			if (Tools::strlen(Tools::getValue('title')) == 0)
            {
                if(Tools::getValue('ajax'))
                    die( Tools::jsonEncode(
                         array(
                            'error'=>true,
                            'errors'=>$this->l('The title is not set.'),
                          )  
                    ));
                $errors[] = $this->l('The title is not set.');
            }
		}				
		elseif (Tools::isSubmit('changeHomePageStatus')) 
		{
			if (!Validate::isInt(Tools::getValue('id_homepage')))

				$errors[] = $this->l('Invalid Home Page');
		}		
		elseif (Tools::isSubmit('delete_id_homepage'))
			if	((!Validate::isInt(Tools::getValue('delete_id_homepage')) || !$tea_exc->homeExists((int)Tools::getValue('delete_id_homepage'))))
				$errors[] = $this->l('Invalid id_homepage');		
		elseif (Tools::isSubmit('export_id_homepage'))
			if	((!Validate::isInt(Tools::getValue('export_id_homepage')) || !$tea_exc->homeExists((int)Tools::getValue('export_id_homepage'))))
				$errors[] = $this->l('Invalid id_homepage');		
		/* Display errors if needed */
		if (count($errors))
		{
			$this->_html .= Tools::displayError(implode('<br />', $errors));
			return false;
		}
		/* Returns if validation is ok */
		return true;
	}
	private function _postProcess()
	{
		$tea_exc = new TeaExecute();
		$errors = array();
		if (Tools::isSubmit('submitHome')|| Tools::isSubmit('submitAddconfiguration'))
		{
        	if (Tools::getValue('id_homepage'))
				$id_homepage = Tools::getValue('id_homepage');
			else
				$id_homepage = null;
			$res = $tea_exc->addUpdateHome($id_homepage, Tools::getValue('title'), Tools::getValue('id_header'), Tools::getValue('id_homebody'), Tools::getValue('id_footer'), Tools::getValue('css_file'), Tools::getValue('font_url'),Tools::getValue('custom_class'));
			if ($res)
				$errors[] = $res;
            else
            {
                if(Tools::getValue('ajax'))
                    die( Tools::jsonEncode(
                          array(
                            'error'=>false,
                            'list_home_block'=>$this->renderListHome(true),
                          )  
                    ));           
            }
		}
		elseif (Tools::isSubmit('delete_id_homepage'))
		{
			$res = $tea_exc->deleteHome((int)Tools::getValue('delete_id_homepage'));
			if (!$res)
				$this->_html .= Tools::displayError('Could not delete');
			else
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminTea_themelayoutHomepage', true).'&conf=1&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
		}
		elseif (Tools::isSubmit('export_id_homepage'))
		{			
			$tea_importexport = new TeaImportExport();
			$res = $tea_importexport->exportHomepage(Tools::getValue('export_id_homepage'));			
		}				
		if (count($errors))
			$this->_html .= Tools::displayError(implode('<br />', $errors));
		elseif (Tools::isSubmit('submitHome') && Tools::getValue('id_homepage'))
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminTea_themelayoutHomepage', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
		elseif (Tools::isSubmit('delete_id_homepage') && Tools::getValue('delete_id_homepage'))
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminTea_themelayoutHomepage', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
		elseif (Tools::isSubmit('changeHomePageStatus') && Tools::getValue('id_homepage'))
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminTea_themelayoutHomepage', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);		
	}
	public function renderConfig()
	{
		$tea_exc = new TeaExecute();
		$homepages = $tea_exc->getHomes();
		$this->fields_form = array(
				'legend' => array(
					'title' => $this->l('Config'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'select',
						'label' => $this->l('Home Page Default '),
						'name' => 'TEAADV_HOMEPAGE',
						'desc' => $this->l('Select HomePage'),
						'options' => array('query' => $homepages,'id' => 'id_homepage','name' => 'title'
						)
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
					'name' => 'submitConf'
				)
		);
		$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_homepage');
		$this->fields_value = $this->getConfFieldsValues();
		return adminController::renderForm();
	}
	public function getConfFieldsValues()
	{
		return array(
			'TEAADV_HOMEPAGE' => Tools::getValue('TEAADV_HOMEPAGE', Configuration::get('TEAADV_HOMEPAGE'))
		);
	}
	public function renderPathway()
	{
		$tpl = $this->createTemplate('path.tpl');
		$tpl->assign(
			array(
				'view' => Tools::getValue('view'),
				'link' => $this->context->link
			)
		);
		return $tpl->fetch();
	}
	public function renderListHome($ajax=false)
	{
		$this->context->controller->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/admin_style.css', 'all');
		$this->context->controller->addJqueryUI('ui.draggable');
		$tea_exc = new TeaExecute();
		$homepages = $tea_exc->getHomes();
        $this->override_folder = 'tea_themelayout_base/';
		$tpl = $this->createTemplate('listHome.tpl');
		$tpl->assign( array(
			'link' => $this->context->link,
			'homepages' => $homepages,
            'TEAADV_HOMEPAGE' => Configuration::get('TEAADV_HOMEPAGE'),
            'iso_code'=>$this->context->language->iso_code,
            'ajax'=>$ajax,
		));
		return $tpl->fetch();
	}
	public function renderNavigation()
	{
		$html = '<div class="navigation">';
		$html .= '<a class="btn btn-default" href="'.AdminController::$currentIndex.
			'&configure='.$this->name.'
				&token='.Tools::getAdminTokenLite('AdminTea_themelayoutHomepage').'" title="Back to Dashboard"><i class="icon-home"></i>Back to Dashboard</a>';
		$html .= '</div>';
		return $html;
	}
	public function renderAddHome()
	{
		$this->context->controller->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/admin_style.css', 'all');
		$this->context->controller->addJqueryUI('ui.draggable');
		$tea_exc = new TeaExecute();
		$headers = $tea_exc->getProf('header');		
		$homebodys = $tea_exc->getProf('homebody');
		$footers = $tea_exc->getProf('footer');
		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Homepage Informations'),
				'icon' => 'icon-cogs'
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Title'),
					'name' => 'title',
					'class' => 'fixed-width-xl',
				),
				array(
					'type' => 'select',
					'label' => $this->l('Select Header'),
					'name' => 'id_header',
					'desc' => $this->l('Select Header for homepage.'),
					'options' => array('query' => $headers,'id' => 'id_prof','name' => 'title')
				),
				array(
					'type' => 'select',
					'label' => $this->l('Select Body'),
					'name' => 'id_homebody',
					'desc' => $this->l('Select Body for homepage.'),
					'options' => array('query' => $homebodys,'id' => 'id_prof','name' => 'title')
				),
				array(
					'type' => 'select',
					'label' => $this->l('Select Footer'),
					'name' => 'id_footer',
					'desc' => $this->l('Select Footer for homepage.'),
					'options' => array('query' => $footers,'id' => 'id_prof','name' => 'title')
				),
                array(
					'type' => 'text',
					'label' => $this->l('Custom class'),
					'name' => 'custom_class',
					'class' => 'fixed-width-xl',
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'name' => 'submitHome'
			)
		);
		if (Tools::isSubmit('edit_id_homepage'))
			$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_homepage');
        $this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'ajax');
		$this->fields_value = $this->getHomeFieldsValues();
        $this->fields_value['ajax']=1;
        $this->show_form_cancel_button = true;
		return parent::renderForm();
	}
	public function getHomeFieldsValues()
	{
		$tea_exc = new TeaExecute();
		return $tea_exc->getHomeFieldsValues((int)Tools::getValue('edit_id_homepage'));
	}
	public function headerHTML()
	{
		if (Tools::getValue('controller') != 'AdminTea_themelayoutHomepage' && Tools::getValue('configure') != $this->name)
			return;
		$this->context->controller->addJqueryUI('ui.resizable');		
		$this->context->controller->addJqueryUI('ui.sortable');		
		/* Style & js for fieldset 'rows configuration' */
		$html = '<script type="text/javascript">
			$(function() {
				var $myhomepages = $(".homepage");
				$myhomepages.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {						
						var order = $(this).sortable("serialize") + "&action=updateHomesOrdering";												
						$.post("'.$this->context->shop->physical_uri.$this->context->shop->virtual_uri.'modules/'.$this->name.'/ajax_'.$this->name.'.php?secure_key='.$this->secure_key.'", order);
					},
					stop: function( event, ui ) {
						showSuccessMessage("Saved!");
					}	
				});
				$myhomepages.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});					
			});
		</script>';
		return $html;
	}
}