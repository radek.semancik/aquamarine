<?php
/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
*/

if (!defined('_PS_VERSION_'))
	exit;
include_once(_PS_MODULE_DIR_.'tea_themelayout/controllers/admin/AdminTea_themelayoutBase.php');
class AdminTea_themelayoutFooterController extends AdminTea_themelayoutBaseController {
	public function __construct()
	{
		$this->name = 'tea_themelayout';
		$this->tab = 'front_office_features';
		$this->bootstrap = true;
		$this->lang = true;
		$this->context = Context::getContext();
		$this->secure_key = Tools::encrypt($this->name);
		parent::__construct();
	}
}