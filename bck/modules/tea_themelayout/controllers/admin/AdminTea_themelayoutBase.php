<?php
/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
*/
if (!defined('_PS_VERSION_'))
	exit;
include_once(_PS_MODULE_DIR_.'tea_themelayout/class/TeaExecute.php');
class AdminTea_themelayoutBaseController extends ModuleAdminControllerCore {
	public function __construct()
	{	
        $this->name = 'tea_themelayout';
		$this->tab = 'front_office_features';
		$this->bootstrap = true;
		$this->lang = true;
		$this->context = Context::getContext();
		$this->secure_key = Tools::encrypt($this->name);		
		$_controller = Tools::getValue('controller');
		$this->classname = $_controller;
		$this->profiletype = strtolower(substr($_controller, 20, strlen($_controller)));
		parent::__construct();
        if(Tools::isSubmit('ajax'))
            $this->renderList();
	}
	public function renderList()
	{
		$tea_exc = new TeaExecute();
        if(!Tools::isSubmit('ajax'))
            $this->_html = $this->headerHTML();
		/* Validate & process */
		if (Tools::isSubmit('submitCancelAddForm') || Tools::isSubmit('submitCancelBlock') || Tools::isSubmit('submitCancelPosition') || Tools::isSubmit('submitLinkCancel'))
		{
          Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		}			
		elseif (Tools::isSubmit('addProf') || (Tools::isSubmit('edit_id_prof') && $tea_exc->profileExists((int)Tools::getValue('edit_id_prof'))))
		{
            if(Tools::isSubmit('ajax'))
                die(Tools::jsonEncode( array(
                    'block_form'=>$this->renderAddProf(),
                )));
			$this->_html .= $this->renderAddProf();
		}
		elseif (Tools::isSubmit('submitprof') || Tools::isSubmit('delete_id_prof'))
		{
			if ($this->_postValidation())
			{
				$this->_postProcess();	
                $this->_html .= $this->displayHeaderlist();	
                 if(Tools::getValue('ajax'))
                 {
                    die( Tools::jsonEncode(
                      array(
                        'error'=>false,
                        'list_prof_block'=>$this->renderListProf(true),
                      )  
                    )); 
                 }   		
				$this->_html .= $this->renderListProf();
			}
			else
			{
				$this->_html .= $this->renderAddProf();
			}
		}
        elseif (Tools::isSubmit('submitRow') || Tools::isSubmit('submitrow') || Tools::isSubmit('delete_id_row') || Tools::isSubmit('changeRowStatus') || Tools::isSubmit('submitPosition')||Tools::isSubmit('submitposition') || Tools::isSubmit('delete_id_position') || Tools::isSubmit('changePositionStatus') || Tools::isSubmit('submitBlock')||Tools::isSubmit('submitblock') || Tools::isSubmit('delete_id_block') || Tools::isSubmit('changeBlockStatus') || Tools::isSubmit('changeLinkStatus') || Tools::isSubmit('submitLink') || Tools::isSubmit('delete_id_link'))
		{
			if ($this->_postValidation())
			{
				$this->_postProcess();
                if(Tools::getValue('ajax'))
                 {
                    die( Tools::jsonEncode(
                      array(
                        'error'=>false,
                        'list_prof_block'=>$this->renderListRows(true),
                      )  
                    )); 
                 }
				$this->_html .= $this->renderListRows();
			}
			else 
			{
                if(Tools::isSubmit('submitBlock'))
                {
                    $this->_html .= $this->renderBlockAddForm();
                }
                elseif(Tools::isSubmit('submitPosition'))
                {
                    $this->_html .= $this->renderAddPosition();
                }
			}	
		}
		elseif (Tools::isSubmit('rowlist'))
		{		
            $this->_html .= $this->displayHeaderlist();
			$this->_html .= $this->renderListRows();
		}
		
		elseif (Tools::isSubmit('addPosition') || (Tools::isSubmit('edit_position') && $tea_exc->positionExists((int)Tools::getValue('id_position'))))
		{
            if(Tools::isSubmit('ajax'))
                die(Tools::jsonEncode( array(
                    'block_form'=>$this->renderAddPosition(),
            )));
			$this->_html .= $this->renderAddPosition();
			$this->_html .= $this->renderBlockList((int)Tools::getValue('id_row'), Tools::getValue('id_prof'));
		}
		elseif (Tools::isSubmit('addRow') || (Tools::isSubmit('id_row') && $tea_exc->rowExists((int)Tools::getValue('id_row')))) 
		{
            if(Tools::isSubmit('ajax'))
                die(Tools::jsonEncode( array(
                    'block_form'=>$this->renderRowForm(),
                )));
			$this->_html .= $this->renderRowForm();
		}
		elseif (Tools::isSubmit('addBlock') || (Tools::isSubmit('edit_block') && $tea_exc->blockExists((int)Tools::getValue('id_block')))) 
		{
            if(Tools::isSubmit('ajax'))
                die(Tools::jsonEncode( array(
                    'block_form'=>$this->renderBlockAddForm(),
            )));
			$this->_html .= $this->renderBlockAddForm();			
		}		
		else
        {
            $this->_html .= $this->displayHeaderlist(); 
            $this->_html .= $this->renderListProf();
        }		
		return $this->_html;
	}
	private function _postValidation()
	{
	   $errors = array();
	   $tea_exc = new TeaExecute();
		/* Validation for configuration */
		if (Tools::isSubmit('changeRowStatus'))
		{
			if (!Validate::isInt(Tools::getValue('id_row')))
				$errors[] = $this->l('Invalid row');
		}
		elseif (Tools::isSubmit('submitprof'))
		{
            if (Tools::strlen(Tools::getValue('title')) > 255)
            {
                if(Tools::isSubmit('ajax'))
                {
                    die( Tools::jsonEncode(
                      array(
                        'error'=>true,
                        'errors'=>$this->l('The title is too long.'),
                      )  
                    ));
                }
                else
                   $errors[]=$this->l('The title is too long.'); 
            }
            if (Tools::strlen(Tools::getValue('title')) == 0)
            {
                if(Tools::isSubmit('ajax'))
                {
                    die( Tools::jsonEncode(
                      array(
                        'error'=>true,
                        'errors'=>$this->l('The title is not set.'),
                      )  
                    ));
                }
                else
                    $errors[]=$this->l('The title is not set.');
            }
		}
		elseif (Tools::isSubmit('submitPosition')||Tools::isSubmit('submitposition'))
		{
			if (Tools::strlen(Tools::getValue('title')) > 255)
            {
                if(Tools::isSubmit('ajax'))
                {
                    die( Tools::jsonEncode(
                      array(
                        'error'=>true,
                        'errors'=>$this->l('The title is too long.'),
                      )  
                    ));
                }
                else
                    $errors[] = $this->l('The title is too long.');
            }
				
			if (Tools::strlen(Tools::getValue('title')) == 0)
            {
                if(Tools::isSubmit('ajax'))
                {
                    die( Tools::jsonEncode(
                      array(
                        'error'=>true,
                        'errors'=>$this->l('The title is not set.'),
                      )  
                    ));
                }
                else
                    $errors[] = $this->l('The title is not set.');
            }
				
		}
		/* Validation for Point */
		elseif (Tools::isSubmit('submitRow') || Tools::isSubmit('submitrow'))
		{
			/* If edit : checks id_row */
			if (Tools::isSubmit('id_row'))
			{
				if (!Validate::isInt(Tools::getValue('id_row')) && !$tea_exc->rowExists(Tools::getValue('id_row')))
                {
                    if(Tools::isSubmit('ajax'))
                    {
                        die( Tools::jsonEncode(
                          array(
                            'error'=>true,
                            'errors'=>$this->l('Invalid id_row.'),
                          )  
                        ));
                    }
                    else	
                        $errors[] = $this->l('Invalid id_row');
                }
			}
			if (Tools::strlen(Tools::getValue('title')) > 255)
            {
                if(Tools::isSubmit('ajax'))
                    {
                        die( Tools::jsonEncode(
                          array(
                            'error'=>true,
                            'errors'=>$this->l('The title is too long.'),
                          )  
                        ));
                    }
                    else	
                        $errors[] = $this->l('The title is too long.');
            }
			if (Tools::strlen(Tools::getValue('title')) == 0)
            {
                if(Tools::isSubmit('ajax'))
                    {
                        die( Tools::jsonEncode(
                          array(
                            'error'=>true,
                            'errors'=>$this->l('The title is not set.'),
                          )  
                        ));
                    }
                    else	
                        $errors[] = $this->l('The title is not set.');
            }
		}
		elseif (Tools::isSubmit('changeBlockStatus')) 
		{
			if (!Validate::isInt(Tools::getValue('id_block')))
				$errors[] = $this->l('Invalid block');
		}
		elseif (Tools::isSubmit('submitBlock')||Tools::isSubmit('submitblock')) 
		{
			/* If edit : checks id_row */
			if (Tools::isSubmit('id_block'))
			{
				if (!Validate::isInt(Tools::getValue('id_block')) && !$tea_exc->blockExists(Tools::getValue('id_block')))
				{
				    if(Tools::isSubmit('ajax'))
                    {
                        die( Tools::jsonEncode(
                          array(
                            'error'=>true,
                            'errors'=>$this->l('Invalid id_block'),
                          )  
                        ));
                    }
                    else	
                       $errors[] = $this->l('Invalid id_block');
				}
                	
			}
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
			{
				if (Tools::strlen(Tools::getValue('title_'.$language['id_lang'])) > 255)
                {
                    if(Tools::isSubmit('ajax'))
                    {
                        die( Tools::jsonEncode(
                          array(
                            'error'=>true,
                            'errors'=>$this->l('The title is too long.'),
                          )  
                        ));
                    }
                    else	
                       $errors[] = $this->l('The title is too long.');
                }
									
			}
			$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
			if (Tools::strlen(Tools::getValue('title_'.$id_lang_default)) == 0)
            {
                if(Tools::isSubmit('ajax'))
                {
                    die(Tools::jsonEncode(
                      array(
                        'error'=>true,
                        'errors'=>$this->l('The title is not set.'),
                      )  
                    ));
                }
                else	
                   $errors[] = $this->l('The title is not set.');
            }		
		}		
		elseif (Tools::isSubmit('changeLinkStatus')) 
		{
			if (!Validate::isInt(Tools::getValue('id_link')))
			 $errors[] = $this->l('Invalid link');
		}		
		elseif (Tools::isSubmit('submitLink')) 
		{
			/* If edit : checks id_row */
			if (Tools::isSubmit('id_link'))
			{					
				if (!Validate::isInt(Tools::getValue('id_link')) && !$tea_exc->linkExists(Tools::getValue('id_link')))
					$errors[] = $this->l('Invalid id_link');
			}
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
			{
				if (Tools::strlen(Tools::getValue('title_'.$language['id_lang'])) > 255)
					$errors[] = $this->l('The title is too long.');				
			}
			$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
			if (Tools::strlen(Tools::getValue('title_'.$id_lang_default)) == 0)
				$errors[] = $this->l('The title is not set.');								
		} /* Validation for deletion */
		elseif (Tools::isSubmit('delete_id_row'))
		{
			if	((!Validate::isInt(Tools::getValue('delete_id_row')) || !$tea_exc->rowExists((int)Tools::getValue('delete_id_row'))))
				$errors[] = $this->l('Invalid id_row');
		}
		elseif (Tools::isSubmit('delete_id_position'))
		{
		  if	(!Validate::isInt(Tools::getValue('delete_id_position')))

				$errors[] = $this->l('Invalid id position');

		}

		elseif (Tools::isSubmit('delete_id_block') && (!Validate::isInt(Tools::getValue('delete_id_block')) || !$tea_exc->blockExists((int)Tools::getValue('delete_id_block'))))

			$errors[] = $this->l('Invalid id_block');

		elseif (Tools::isSubmit('delete_id_link') && (!Validate::isInt(Tools::getValue('delete_id_link')) || !$tea_exc->linkExists((int)Tools::getValue('delete_id_link'))))

			$errors[] = $this->l('Invalid id_link');
		/* Display errors if needed */
		if (count($errors))
		{
			$this->_html .= Tools::displayError(implode('<br />', $errors));
			return false;
		}
		/* Returns if validation is ok */
		return true;
	}
	private function _postProcess()
	{
		$tea_exc = new TeaExecute();
		$errors = array();
		if (Tools::isSubmit('submitprof'))
		{
		  if (Tools::getValue('id_prof'))
				$id_prof = Tools::getValue('id_prof');
			else
				$id_prof = null;
			$res = $tea_exc->addUpdateProf($id_prof, Tools::getValue('title'), Tools::getValue('class_suffix'), $this->profiletype);
			if ($res)
				$errors[] = $res;
		}
		elseif (Tools::isSubmit('delete_id_prof'))
		{
			$res = $tea_exc->deleteProfile((int)Tools::getValue('delete_id_prof'));
			if (!$res)
				$this->_html .= Tools::displayError('Could not delete');
			else
				Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=1&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
		}
		elseif (Tools::isSubmit('changeRowStatus') && Tools::isSubmit('id_row'))
		{
			$res = $tea_exc->changeRowStatus(Tools::getValue('id_row'));
			if (!$res)
				$this->_html .= Tools::displayError('The status could not be updated.');
			else
				Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=5&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		}
		elseif (Tools::isSubmit('submitRow') || Tools::isSubmit('submitrow'))
		{
			/* Sets ID if needed */
			if (Tools::getValue('id_row'))
				$id_row = Tools::getValue('id_row');
			else
				$id_row = null;
			$res = $tea_exc->addUpdateRows($id_row, Tools::getValue('id_prof'), Tools::getValue('title'), Tools::getValue('class'), Tools::getValue('fullwidth'), Tools::getValue('active'),Tools::getValue('padding'),Tools::getValue('margin'));
			if ($res)
				$errors[] = $res;
		}
		elseif (Tools::isSubmit('delete_id_row'))
		{
			$res = $tea_exc->deleteRow((int)Tools::getValue('delete_id_row'));
			if (!$res)
				$this->_html .= Tools::displayError('Could not delete');
			else
				Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=1&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		}
		elseif (Tools::isSubmit('submitPosition') || Tools::isSubmit('submitposition'))
		{
			if (Tools::getValue('id_position'))
				$id_position = Tools::getValue('id_position');
			else
				$id_position = null;
			$res = $tea_exc->addUpdatePosition($id_position, Tools::getValue('id_row'), Tools::getValue('title'), Tools::getValue('class_suffix'), Tools::getValue('col_lg'), Tools::getValue('col_md'), Tools::getValue('col_sm'), Tools::getValue('col_xs'), Tools::getValue('active'));
			
            if ($res)
				$errors[] = $res;
		}
		elseif (Tools::isSubmit('delete_id_position'))
		{
			$res = $tea_exc->deletePosition(Tools::getValue('delete_id_position'));
			if (!$res)
				$this->_html .= Tools::displayError('Could not delete');
			else
				Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=1&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		}
		elseif (Tools::isSubmit('changePositionStatus') && Tools::isSubmit('id_position'))
		{
			$res = $tea_exc->changePositionStatus(Tools::getValue('id_position'));
			if (!$res)
				$this->_html .= Tools::displayError('The status could not be updated.');
			else
				Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=5&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		}
		elseif (Tools::isSubmit('submitBlock')||Tools::isSubmit('submitblock'))
		{
			/* Sets ID if needed */
			if (Tools::getValue('id_block'))
			{
				$block = new TeaBlock((int)Tools::getValue('id_block'));
				if (!Validate::isLoadedObject($block))
				{
                    if(Tools::isSubmit('ajax'))
                    {
                        die(Tools::jsonEncode(
                          array(
                            'error'=>true,
                            'errors'=>$this->l('Invalid id_block'),
                          )  
                        ));
                    }
                    else
                        $this->_html .= Tools::displayError($this->l('Invalid id_block'));
					return;
				}
			}
			else
				$block = new TeaBlock();
			/* Sets values */
            if(isset($_FILES['backgroud']['tmp_name']) && isset($_FILES['backgroud']['name']) && $_FILES['backgroud']['name'])
            {
                $salt = sha1(microtime());
                $type = Tools::strtolower(Tools::substr(strrchr($_FILES['backgroud']['name'], '.'), 1));
                $imageName = $salt.'.'.$type;
                $fileName = _PS_MODULE_DIR_.'tea_themelayout/images/'.$imageName;
    			$imagesize = @getimagesize($_FILES['backgroud']['tmp_name']);
                if (isset($_FILES['backgroud']) &&
    				!empty($_FILES['backgroud']['tmp_name']) &&
    				!empty($imagesize) &&
    				in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
    			)
    			{
                    $errors=array();
    				$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
    				if ($error = ImageManager::validateUpload($_FILES['backgroud']))
    					$errors[] = $error;
    				elseif (!$temp_name || !move_uploaded_file($_FILES['backgroud']['tmp_name'], $temp_name))
    					$errors[] = $this->l('Can not upload the file');
    				elseif (!ImageManager::resize($temp_name, $fileName, null, null, $type))
    					$errors[] = 'An error occurred during the image upload process.';
    				if (isset($temp_name))
    					@unlink($temp_name);
                    if(!$errors)
                    {
                        if($block->backgroud!='')
                        {
                            $oldImage = _PS_MODULE_DIR_.'tea_themelayout/images/'.$block->backgroud;
                            if(file_exists($oldImage))
                               @unlink($oldImage);
                        }
                        $block->backgroud=$imageName;
                    }
                }
            }
			$block->id_position = (int)Tools::getValue('id_position');
			$block->active = (int)Tools::getValue('active');
			$block->block_type = Tools::getValue('block_type');
            $block->type_parallax = Tools::getValue('type_parallax');
            $block->link_video = Tools::getValue('link_video');
            $block->type_product = Tools::getValue('type_product');
            $block->number_product = (int)Tools::getValue('number_product');	
            $block->list_id_product = Tools::getValue('list_id_product');	
            $block->id_category = (int)Tools::getValue('id_category');				
			$block->module_name = Tools::getValue('module_name');
			$block->hook_name = Tools::getValue('hook_name');
			$block->show_title = Tools::getValue('show_title');
            $block->class_custom = Tools::getValue('class_custom');
            $block->position_content = Tools::getValue('position_content');
            $block->height = Tools::getValue('height');
            $block->group_slider = Tools::getValue('group_slider');
            $block->backgroud_widget = Tools::getValue('backgroud_widget');
            $block->link_extra = Tools::getValue('link_extra');
            $block->animation = Tools::getValue('animation');
            $block->animation_type = Tools::getValue('animation_type');
            $block->padding = Tools::getValue('padding');
            $block->margin =Tools::getValue('margin');
			$id_lang = $this->context->language->id;
			/* Sets each langue fields */
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
			{
				$block->title[$language['id_lang']] = Tools::getValue('title_'.$language['id_lang']);
				if (Tools::getValue('id_block'))
					$block->html_content[$language['id_lang']] = Tools::getValue('html_content_'.$language['id_lang']);
				else
				{
					$html_content = Tools::getValue('html_content_'.$id_lang);
					$block->html_content[$language['id_lang']] = $html_content;
				}
			}
			/* Processes if no errors  */
			if (!$errors)
			{
				/* Adds */
				if (!Tools::getValue('id_block'))
				{
					if (!$block->add())
						$errors[] = Tools::displayError($this->l('The block could not be added.'));
				}
				/* Update */
				elseif (!$block->update())
					$errors[] = Tools::displayError($this->l('The block could not be updated.'));
			}
		}
		elseif (Tools::isSubmit('delete_id_block'))
		{
			$res = $tea_exc->deleteBlock(Tools::getValue('delete_id_block'));
			if (!$res)
				$this->_html .= Tools::displayError('Could not delete');
			else
				Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=1&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		}
		elseif (Tools::isSubmit('changeBlockStatus') && Tools::isSubmit('id_block'))
		{
			$res = $tea_exc->changeBlockStatus(Tools::getValue('id_block'));
			if (!$res)
				$this->_html .= Tools::displayError('The status could not be updated.');
			else
				Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=5&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		}		
		if (count($errors))
			$this->_html .= Tools::displayError(implode('<br />', $errors));
		elseif (Tools::isSubmit('submitRow') && Tools::getValue('id_row'))
			Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		elseif (Tools::isSubmit('submitRow'))
			Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=3&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		elseif (Tools::isSubmit('submitLink') && Tools::getValue('id_link'))
			Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&edit_block&id_prof='.Tools::getValue('id_prof').'&id_block='.Tools::getValue('id_block').'&id_position='.Tools::getValue('id_position'));
		elseif (Tools::isSubmit('submitLink'))
			Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=3&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&edit_block&id_prof='.Tools::getValue('id_prof').'&id_block='.Tools::getValue('id_block').'&id_position='.Tools::getValue('id_position'));
		elseif (Tools::isSubmit('submitBlock') && Tools::getValue('id_block'))
			Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		elseif (Tools::isSubmit('submitBlock') || Tools::isSubmit('submitPosition') || Tools::isSubmit('submitRow'))
			Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=3&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&rowlist&id_prof='.Tools::getValue('id_prof'));
		elseif (Tools::isSubmit('changeLinkStatus'))	
			Tools::redirectAdmin($this->context->link->getAdminLink($this->classname, true).'&conf=5&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&edit_block&id_prof='.Tools::getValue('id_prof').'&id_block='.Tools::getValue('id_block').'&id_position='.Tools::getValue('id_position'));	
	}
	public function renderListProf($ajax=false)
	{
		$this->context->controller->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/admin_style.css', 'all');
		$this->context->controller->addJqueryUI('ui.draggable');
		$tea_exc = new TeaExecute();
		$get_prof = $tea_exc->getProf($this->profiletype);
		$this->override_folder = 'tea_themelayout_base/';
		$tpl = $this->createTemplate('listProf.tpl');
		$tpl->assign( array(
			'link' => $this->context->link,
			'getProf' => $get_prof,
            'ajax'=>$ajax,
			'adminlink' => $this->context->link->getAdminLink($this->classname),
		));
		return $tpl->fetch();
	}
	public function renderAddProf()
	{
		//$this->context->controller->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/admin_style.css', 'all');
		$this->context->controller->addJqueryUI('ui.draggable');
		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Profile Informations'),
				'icon' => 'icon-cogs'
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Title'),
					'name' => 'title',
                    'required'=>true,
				),
				array(
					'type' => 'text',
					'label' => $this->l('class_suffix'),
					'name' => 'class_suffix'
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'name' => 'submitProf'
			)
		);
		if (Tools::isSubmit('edit_id_prof'))
			$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_prof');
        $this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'submitprof');
        $this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'ajax');
		$this->fields_value = $this->getProfFieldsValues();
		return adminController::renderForm();
	}
	public function getProfFieldsValues()
	{
		$tea_exc = new TeaExecute();
		return $tea_exc->getProfFieldsValues((int)Tools::getValue('edit_id_prof'));
	}
	public function renderListRows($ajax=false)
	{
        $tea_exc = new TeaExecute();
		$this->context->controller->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/admin_style.css', 'all');
		$this->context->controller->addJqueryUI('ui.draggable');
		if (Tools::isSubmit('rowlist'))
		{
			$rows = $tea_exc->getRows(null, Tools::getValue('id_prof'));
			$id_prof = Tools::getValue('id_prof');
			$name_prof = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT title
				FROM '._DB_PREFIX_.'teaadv_prof'.($id_prof ? ' WHERE id_prof = '.$id_prof : '')
			);			
		}
		foreach ($rows as $key => $row) 
		{
			$rows[$key]['status'] = $tea_exc->displayRowStatus($row['id_row'], $row['active'], Tools::getValue('id_prof'), $this->classname);

			$rows[$key]['positions'] = $tea_exc->getPositions($row['id_row'], null, Tools::getValue('id_prof'), $this->classname);

			foreach ($rows[$key]['positions'] as $number => $position)

				$rows[$key]['positions'][$number]['blocks'] = $tea_exc->getBlocks($position['id_position'], null, $id_prof, $this->classname);

		}	
		$this->override_folder = 'tea_themelayout_base/';
		$tpl = $this->createTemplate('rowslist.tpl');
		$tpl->assign(
			array(
				'link' => $this->context->link,
				'adminlink' => $this->context->link->getAdminLink($this->classname),
				'rows' => $rows,
                'ajax'=>$ajax,
				'id_prof' => $id_prof,
				'name_prof' => $name_prof[0]['title'],
				'image_baseurl' => _MODULE_DIR_.$this->module->name.'/views/img/',
				'current_url' => Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/ajax_'.$this->name.'.php'
				)
		);
		return $tpl->fetch();
	}
	public function headerHTML()
	{
	   if (Tools::getValue('controller') != $this->classname && Tools::getValue('configure') != $this->name)
		return;
		$this->context->controller->addJqueryUI('ui.resizable');		
		$this->context->controller->addJqueryUI('ui.sortable');
		$this->context->controller->addJS(_MODULE_DIR_.$this->module->name.'/views/js/resize_script.js', 'all');
        $this->context->controller->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/admin_style.css', 'all');
		$this->context->controller->addJS(_MODULE_DIR_.$this->module->name.'/views/js/admin_script.js', 'all');
        $this->context->controller->addJS(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');
        $this->context->controller->addJS(_PS_JS_DIR_.'admin/tinymce.inc.js');
		$this->context->controller->addJqueryUI('ui.draggable');
		/* Style & js for fieldset 'rows configuration' */
		$html = '<script type="text/javascript">
			$(function() {
				var $myProf = $("#prof_list");
				$myProf.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize") + "&action=updateProfOrdering";						
						$.post("'.$this->context->shop->physical_uri.$this->context->shop->virtual_uri.'modules/'.$this->name.'/ajax_'.$this->name.'.php?secure_key='.$this->secure_key.'", order);
					},
				stop: function( event, ui ) {
						showSuccessMessage("Saved!");
					}
				});
				$myProf.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
				var $myRows = $(".rowlist");
				$myRows.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize") + "&action=updateRowsOrdering";						
						$.post("'.$this->context->shop->physical_uri.$this->context->shop->virtual_uri.'modules/'.$this->name.'/ajax_'.$this->name.'.php?secure_key='.$this->secure_key.'", order);
					},
					stop: function( event, ui ) {
						showSuccessMessage("Saved!");
					}	
				});
				$myRows.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					   $(this).css("cursor","auto");
				});
				var $myposition = $(".row-positions");
				$myposition.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize") + "&action=updatePositionsOrdering";
						$.post("'.$this->context->shop->physical_uri.$this->context->shop->virtual_uri.'modules/'.$this->name.'/ajax_'.$this->name.'.php?secure_key='.$this->secure_key.'", order);
					},
					stop: function( event, ui ) {
						showSuccessMessage("Saved!");
					}	
				});	
				$myposition.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
				var $myblocks = $(".pos-blocks");
				$myblocks.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize") + "&action=updateBlocksOrdering";
						$.post("'.$this->context->shop->physical_uri.$this->context->shop->virtual_uri.'modules/'.$this->name.'/ajax_'.$this->name.'.php?secure_key='.$this->secure_key.'", order);
					},
					stop: function( event, ui ) {
					   showSuccessMessage("Saved!");
					}					
				});
				$myblocks.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
				var $myLinks = $("#links");
				$myLinks.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize") + "&action=updateLinksOrdering";
						$.post("'.$this->context->shop->physical_uri.$this->context->shop->virtual_uri.'modules/'.$this->name.'/ajax_'.$this->name.'.php?secure_key='.$this->secure_key.'", order);
					},
					stop: function( event, ui ) {
						showSuccessMessage("Saved!");
					}
				});
				$myLinks.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
			});
		</script>';
		return $html;
	}
	public function renderAddPosition()
	{
		$columns = array();				
		$columns[] = array('value' => 12);		
		$columns[] = array('value' => 11);
		$columns[] = array('value' => 10);
		$columns[] = array('value' => 9);
		$columns[] = array('value' => 8);
		$columns[] = array('value' => 7);
		$columns[] = array('value' => 6);
		$columns[] = array('value' => 5);
		$columns[] = array('value' => 4);
		$columns[] = array('value' => 3);
		$columns[] = array('value' => 2);
		$columns[] = array('value' => 1);
        $columns[] = array('value' => 0);
		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Position informations'),
				'icon' => 'icon-cogs'
			),
			'input' => array(
				array(
					'type' => 'text',
				    'label' => $this->l('title'),
					'name' => 'title',
					'class' => ' fixed-width-xl',
                    'required'=>true,
					),
				array(
					'type' => 'text',
					'label' => $this->l('Class Suffix'),
					'name' => 'class_suffix',
					'class' => ' fixed-width-xl'
					),
				array(
					'type' => 'select',
					'label' => $this->l('Desktops Width( ≥1200px )'),
					'name' => 'col_lg',
					'desc' => $this->l('Bootstrap Grid has 12 columns, You select number of column.'),
					'options' => array('query' => $columns,'id' => 'value','name' => 'value')
				),
				array(
					'type' => 'select',
					'label' => $this->l('Medium devices Width( ≥992px )'),
					'name' => 'col_md',
					'desc' => $this->l('Bootstrap Grid has 12 columns, You select number of column.'),
					'options' => array('query' => $columns,'id' => 'value','name' => 'value')
				),
				array(
					'type' => 'select',
					'label' => $this->l('Tablets Width( ≥768px )'),
					'name' => 'col_sm',
					'desc' => $this->l('Bootstrap Grid has 12 columns, You select number of column.'),
					'options' => array('query' => $columns,'id' => 'value','name' => 'value')
				),
				array(
					'type' => 'select',
					'label' => $this->l('Phone Width( <768px )'),
					'name' => 'col_xs',
					'desc' => $this->l('Bootstrap Grid has 12 columns, You select number of column.'),
					'options' => array('query' => $columns,'id' => 'value','name' => 'value')
				),
				array(
					'type' => 'switch',
					'label' => $this->l('Active'),
					'name' => 'active',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'name' => 'submitPosition'
			)
		);
		if (Tools::isSubmit('edit_position'))
			$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_position');
		$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_prof');
		$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_row');
        if(Tools::isSubmit('ajax'))
        {
            $this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'submitposition');
            $this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'ajax');
        }
		$this->fields_form['buttons'][] = array( 'type' => 'submit', 'name' => 'submitCancelPosition','class' => 'pull-left', 'title' => 'Cancel','icon' => 'process-icon-cancel');
		$this->fields_value = $this->getFieldsValuesOfPosition();
		return adminController::renderForm();
	}
	public function getFieldsValuesOfPosition()
	{
		$tea_exc = new TeaExecute();
		return $tea_exc->getFieldsValuesOfPosition((int)Tools::getValue('id_position'));
	}
	public function renderRowForm()
	{
		$tea_exc = new TeaExecute();
		$this->context->controller->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/admin_style.css', 'all');
		$this->context->controller->addJqueryUI('ui.draggable');
		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Row informations'),
				'icon' => 'icon-cogs'
			),
			'input' => array(
				array(
					'type' => 'text',
				    'label' => $this->l('Title'),
					'name' => 'title'
				),
				array(
					'type' => 'text',
					'label' => $this->l('Class'),
					'name' => 'class',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Padding'),
					'name' => 'padding',
                    'desc' => $this->l('Eg: 1px 1px 1px 1px'),
				),
                array(
					'type' => 'text',
					'label' => $this->l('Margin'),
					'name' => 'margin',
                    'desc' => $this->l('Eg: 1px 1px 1px 1px'),
				),
				array(
					'type' => 'switch',
					'label' => $this->l('Full Width'),
					'name' => 'fullwidth',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
				array(
					'type' => 'switch',
					'label' => $this->l('Active'),
					'name' => 'active',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'name' => 'submitRow'
			)
		);
		if (Tools::isSubmit('id_row') && $tea_exc->rowExists((int)Tools::getValue('id_row')))
			$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_row');
		$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_prof');
        $this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'submitrow');
        $this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'ajax');
		$this->fields_form['buttons'][] = array( 'type' => 'submit', 'name' => 'submitCancelAddForm','class' => 'pull-left', 'title' => 'Cancel','icon' => 'process-icon-cancel');
		$this->fields_value = $this->getFieldsValuesOfRows();
		return adminController::renderForm();
	}
	public function getFieldsValuesOfRows()
	{
		$tea_exc = new TeaExecute();
		return $tea_exc->getFieldsValuesOfRows((int)Tools::getValue('id_row'));
	}
	public function renderBlockAddForm()
	{
		
        if(Tools::isSubmit('id_block')&& Tools::getValue('id_block'))
        {
            $block = new TeaBlock(Tools::getValue('id_block'));
            $image= $block->backgroud;
        }
		$tea_exc = new TeaExecute();
		$btypes = array();
        $btypes[] = array('value'=>'','title'=>'--Select type--');
		$btypes[] = array('value' => 'custom_html','title' => 'Custom Html');
		$btypes[] = array('value' => 'module','title' => 'Assign Module');		
		$btypes[] = array('value' => 'logo','title' => 'Theme logo');
        $btypes[] = array('value' => 'parallax','title' => 'Custom Parallax');
        $btypes[] = array('value' => 'widget_block','title' => 'Widget Block');	
        $btypes[] = array('value' => 'list_product','title' => 'List product');
        $btypes[] = array('value' => 'slider','title' => 'Slider');		
		$modules = array();			  
		$modules = $tea_exc->getModules();	
		$hookAssign = array();
        $hookAssign[]=  array('name' => '--Select hook--');
		$hookAssign[] = array('name' => 'rightcolumn');
		$hookAssign[] = array('name' => 'leftcolumn');
		$hookAssign[] = array('name' => 'home');
		$hookAssign[] = array('name' => 'top');
        $hookAssign[] = array('name' => 'displayNav');
		$hookAssign[] = array('name' => 'footer');	
        $hookAssign[] = array('name' => 'displayMenu');
        $hookAssign[] = array('name' => 'displayMenu2');
        $hookAssign[] = array('name' => 'displayMenu3');
        $position_content=array();
        $position_content[] = array('value'=>'','title'=>'--Select position--');
        $position_content[] = array('value'=>'inner_top','title'=>'Inner top');
        $position_content[] = array('value'=>'inner_bottom','title'=>'Inner bottom');
        $position_content[] = array('value'=>'inner_center','title'=>'Inner center');
        $position_content[] = array('value'=>'top','title'=>'Top');		
        $position_content[] = array('value'=>'bottom','title'=>'Bottom');	
        $type_products=array();
        $type_products[]= array('value'=>'','title'=>'--Select type product--');
        $type_products[] = array('value'=>'bestseller','title'=>'Bestseller');
        $type_products[] = array('value'=>'category','title'=>'Category');
        $type_products[] = array('value'=>'addproduct','title'=>'Addproduct');
        $type_products[] = array('value'=>'special_product','title'=>'Special product');
        $type_products[] = array('value'=>'featured_product','title'=>'Featured product');
        $animations =array();
        $animations[]= array('value'=>'bounce','title'=>'Bounce');
        $animations[]= array('value'=>'flash','title'=>'Flash');
        $animations[]= array('value'=>'pulse','title'=>'Pulse');
        $animations[]= array('value'=>'rubberBand','title'=>'RubberBand');
        $animations[]= array('value'=>'shake','title'=>'Shake');
        $animations[]= array('value'=>'swing','title'=>'Swing');
        $animations[]= array('value'=>'tada','title'=>'Tada');
        $animations[]= array('value'=>'wobble','title'=>'Wobble');
        $animations[]= array('value'=>'jello','title'=>'Jello');
        $animations[]= array('value'=>'bounceIn','title'=>'BounceIn');
        $animations[]= array('value'=>'bounceInDown','title'=>'BounceInDown');
        $animations[]= array('value'=>'bounceInLeft','title'=>'DounceInLeft');
        $animations[]= array('value'=>'bounceInRight','title'=>'BounceInRight');
        $animations[]= array('value'=>'bounceInUp','title'=>'BounceInUp');
        $animations[]= array('value'=>'bounceOut','title'=>'BounceOut');
        $animations[]= array('value'=>'bounceOutDown','title'=>'BounceOutDown');
        $animations[]= array('value'=>'bounceOutLeft','title'=>'BounceOutLeft');
        $animations[]= array('value'=>'bounceOutRight','title'=>'BounceOutRight');
        $animations[]= array('value'=>'bounceOutUp','title'=>'BounceOutUp');
        $animations[]= array('value'=>'fadeIn','title'=>'FadeIn');
        $animations[]= array('value'=>'fadeInDown','title'=>'FadeInDown');
        $animations[]= array('value'=>'fadeInDownBig','title'=>'FadeInDownBig');
        $animations[]= array('value'=>'fadeInRight','title'=>'FadeInRight');
        $animations[]= array('value'=>'fadeInUp','title'=>'FadeInUp');
        $animations[]= array('value'=>'fadeInUpBig','title'=>'FadeInUpBig');
        $animations[]= array('value'=>'flip','title'=>'Flip');
        $animations[]= array('value'=>'flipInX','title'=>'FlipInX');
        $animations[]= array('value'=>'flipInY','title'=>'FlipInY');
        $animations[]= array('value'=>'lightSpeedIn','title'=>'LightSpeedIn');
        $animations[]= array('value'=>'rotateIn','title'=>'RotateIn');
        $animations[]= array('value'=>'rotateInDownLeft','title'=>'RotateInDownLeft');
        $animations[]= array('value'=>'rotateInDownRight','title'=>'RotateInDownRight');
        $animations[]= array('value'=>'rotateInUpLeft','title'=>'RotateInUpLeft');
        $animations[]= array('value'=>'rotateInUpRight','title'=>'RotateInUpRight');
        $animations[]= array('value'=>'slideInUp','title'=>'SlideInUp');
        $animations[]= array('value'=>'slideInDown','title'=>'SlideInDown');
        $animations[]= array('value'=>'slideInLeft','title'=>'SlideInLeft');
        $animations[]= array('value'=>'slideInRight','title'=>'SlideInRight');
        $animations[]= array('value'=>'zoomIn','title'=>'ZoomIn');
        $animations[]= array('value'=>'zoomInDown','title'=>'ZoomInDown');
        $animations[]= array('value'=>'zoomInLeft','title'=>'ZoomInLeft');
        $animations[]= array('value'=>'zoomInRight','title'=>'ZoomInRight');
        $animations[]= array('value'=>'zoomInUp','title'=>'ZoomInUp');
        $animations[]= array('value'=>'hinge','title'=>'Hinge');
        $animations[]= array('value'=>'rollIn','title'=>'RollIn');
        $GroupSlides=array();
        $GroupSlides[] = array(
            'id_pg_group_slide' =>0,
            'title' => '--Select group slider--',
            'action' =>'',
        );
        if (Module::isInstalled('pg_slider') && Module::isEnabled('pg_slider'))
        {
            $pg_slider= Module::getInstanceByName('pg_slider');
            $slideGroups =$pg_slider->getGroupSlides();
            if($slideGroups)
                foreach($slideGroups as $group)
                    $GroupSlides[]=$group;    
        } 
		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Block informations'),
				'icon' => 'icon-cogs'
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Title'),
					'name' => 'title',
					'lang' => true,
                    'required'=>true,
				),
                array(
					'type' => 'text',
					'label' => $this->l('Class custom'),
					'name' => 'class_custom',
				),
				array(
					'type' => 'switch',
					'label' => $this->l('Show Title'),
					'name' => 'show_title',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'show_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id' => 'show_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
				array(
					'type' => 'select',
					'lang' => true,
					'label' => $this->l('Block Type'),
					'name' => 'block_type',
					'desc' => $this->l('Select Block Type'),
					'options' => array('query' => $btypes,'id' => 'value','name' => 'title'
					)
				),
                array(
                    'type' =>'select',
                    'label' => $this->l('Group slider'),
                    'name' =>'group_slider',
                    'options' =>array('query' => $GroupSlides,'id'=>'id_pg_group_slide','name'=>'title'),
                    'form_group_class' =>'group_slider',
                    'desc' =>'<a target="_blank" href="index.php?controller=AdminModules&configure=pg_slider&tab_module=front_office_features&module_name=pg_slider&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)$this->context->employee->id).'"> View and add group slider</a>',
                ),	
                array(
                    'type'=> 'select',
                    'label' =>'Choose type',
                    'name'=>'type_product',
                    'options'=>array('query'=>$type_products,'id'=>'value','name'=>'title'),
                    'form_group_class' =>'list_product',
                ),
                array(
                    'type'=>'text',
                    'label'=> 'Number product',
                    'name'=>'number_product',
                    'form_group_class' =>'list_product',
                ),
                array(
                    'type' => 'text',
                    'label'=>'List id product',
                    'name' =>'list_id_product',
                    'form_group_class' =>'list_product list_id_product',
                ),
                array(
                    'type'=> 'text',
                    'label'=> 'Id category',
                    'name'=> 'id_category',
                    'form_group_class' =>'list_product id_category',
                ),
                array(
					'type' => 'select',
					'label' => $this->l('Position content'),
					'name' => 'position_content', 
                    'width' =>50,
                    'options' => array('query' => $position_content,'id' => 'value','name' => 'title'),
                    'form_group_class' =>'widget_block',
				),
                array(
					'type' => 'select',
					'label' => $this->l('Type parallax'),
					'name' => 'type_parallax', 
                    'width' =>50,
                    'options' => array('query' => array(array('value'=>'image','title'=>'Image'),array('value'=>'video','title'=>'Video')),'id' => 'value','name' => 'title'),
                    'form_group_class' =>'parallax',
				),
                array(
					'type' => 'textarea',
					'label' => $this->l('Video iframe'),
					'name' => 'link_video',
                    'autoload_rte' => true,
					'form_group_class' => 'parallax',
				),
                array(
					'type' => 'file',
					'label' => $this->l('Image'),
					'name' => 'backgroud',
                    'image' => isset($image) ? '<img src="'._MODULE_DIR_.$this->module->name.'/images/'.$image.'" alt="'.$this->l('Image').'" title="'.$this->l('Image').'" />':null,
					'form_group_class' => 'parallax widget_block',
				),
                array(
					'type' => 'text',
					'label' => $this->l('Link'),
					'name' => 'link_extra',
					'form_group_class' => 'widget_block',
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Display image background'),
					'name' => 'backgroud_widget',
					'is_bool' => true,
                    'form_group_class' => 'widget_block',
					'values' => array(
						array(
							'id' => 'show_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id' => 'show_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
                array
                (
					'type' => 'text',
					'label' => $this->l('Height'),
					'name' => 'height',
					'form_group_class' => 'parallax',
                    'width' =>50,
				),
				array(
					'type' => 'textarea',
					'label' => $this->l('Html Content'),
					'name' => 'html_content',
					'autoload_rte' => true,
					'lang' => true,
					'form_group_class' => 'html_content parallax widget_block',
				),
				array(
					'type' => 'select',
					'lang' => true,
					'label' => $this->l('Module Assign'),
					'name' => 'module_name',
					'desc' => $this->l('Select a Module Name'),
					'form_group_class' => 'module',
					'options' => array('query' => $modules,'id' => 'name','name' => 'name')
				),
				array(
					'type' => 'select',
					'lang' => true,
					'label' => $this->l('Hook'),
					'name' => 'hook_name',
					'desc' => $this->l('Select a Hook'),
					'form_group_class' => 'module',
					'options' => array('query' => $hookAssign,'id' => 'name','name' => 'name')
				),
				array(
					'type' => 'switch',
					'label' => $this->l('Active'),
					'name' => 'active',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
	                       'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
                array(
					'type' => 'switch',
					'label' => $this->l('Animation'),
					'name' => 'animation',
                    'form_group_class' => 'html_content parallax widget_block list_product custom_html',
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
					       	'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
                array(
					'type' => 'select',
					'label' => $this->l('Animations type'),
					'name' => 'animation_type',
					'desc' => $this->l('Select a animations'),
					'form_group_class' => 'html_content parallax widget_block list_product custom_html',
					'options' => array('query' => $animations,'id' => 'value','name' => 'title')
				),
                array(
					'type' => 'text',
					'label' => $this->l('Padding'),
					'name' => 'padding',
                    'desc' => $this->l('Eg: 1px 1px 1px 1px'),
				),
                array(
					'type' => 'text',
					'label' => $this->l('Margin'),
					'name' => 'margin',
                    'desc' => $this->l('Eg: 1px 1px 1px 1px'),
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'name' => 'submitBlock'
			)
		);
		if (Tools::isSubmit('id_block') && $tea_exc->blockExists((int)Tools::getValue('id_block')))
    		$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_block');
		$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_prof');
		$this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'id_position');
        if(Tools::getValue('ajax'))
        {
            $this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'ajax');
            $this->fields_form['input'][] = array('type' => 'hidden', 'name' => 'submitblock');
        }
		$this->fields_form['buttons'][] = array( 'type' => 'submit', 'name' => 'submitCancelBlock','class' => 'pull-left', 'title' => 'Cancel','icon' => 'process-icon-cancel');
		$this->fields_value = $this->getFieldsValuesOfBlock();
		return adminController::renderForm();
	}
	public function getFieldsValuesOfBlock()
	{
		$tea_exc = new TeaExecute();
		$fields = array();
		if (Tools::isSubmit('id_block') && $tea_exc->blockExists((int)Tools::getValue('id_block')))
		{
			$block = new TeaBlock((int)Tools::getValue('id_block'));
			$fields['id_block'] = (int)Tools::getValue('id_block', $block->id);
		}
		else
			$block = new TeaBlock();
		$fields['id_prof']		= (int)Tools::getValue('id_prof');
		$fields['id_position']	= (int)Tools::getValue('id_position');
		$fields['active'] 		= Tools::getValue('active', $block->active);
        $fields['backgroud_widget'] = Tools::getValue('backgroud_widget', $block->backgroud_widget);		
		$fields['block_type'] 	= Tools::getValue('block_type', $block->block_type);
        $fields['type_product'] 	= Tools::getValue('type_product', $block->type_product);
        $fields['number_product'] 	= Tools::getValue('number_product', $block->number_product);
        $fields['list_id_product'] 	= Tools::getValue('list_id_product', $block->list_id_product);
        $fields['id_category'] 	= Tools::getValue('id_category', $block->id_category);						
		$fields['module_name'] 	= Tools::getValue('module_name', $block->module_name);
		$fields['hook_name'] 	= Tools::getValue('hook_name', $block->hook_name);
		$fields['show_title'] 	= Tools::getValue('show_title', $block->show_title);
		$fields['position_content'] = Tools::getValue('position_content', $block->position_content);
        $fields['class_custom'] 	= Tools::getValue('class_custom', $block->class_custom);
        $fields['height'] 	= Tools::getValue('height', $block->height);
        $fields['group_slider'] = Tools::getValue('group_slider',$block->group_slider);
        $fields['animation'] = Tools::getValue('animation',$block->animation);
        $fields['link_extra'] = Tools::getValue('link_extra',$block->link_extra);
        $fields['animation_type'] = Tools::getValue('animation_type',$block->animation_type);
        $fields['padding'] = Tools::getValue('padding',$block->padding);
        $fields['margin'] = Tools::getValue('margin',$block->margin);
        $fields['type_parallax'] = Tools::getValue('type_parallax',$block->type_parallax);
        $fields['link_video'] = Tools::getValue('link_video',$block->link_video);
        if(Tools::getValue('ajax'))
        {
            $fields['ajax'] = 1;
            $fields['submitblock'] = 1;
        }
		$languages = Language::getLanguages(false);
		foreach ($languages as $lang)
		{	
			$fields['title'][$lang['id_lang']] = Tools::getValue('title_'.(int)$lang['id_lang'], isset($block->title[$lang['id_lang']])?$block->title[$lang['id_lang']]:'');			
			$fields['html_content'][$lang['id_lang']] = Tools::getValue('html_content_'.(int)$lang['id_lang'], isset($block->html_content[$lang['id_lang']])?$block->html_content[$lang['id_lang']]:'');
		}
		return $fields;
	}
	public function renderBlockList($id_row, $id_prof)
	{
		$tea_exc = new TeaExecute();
		$this->context->controller->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/admin_style.css', 'all');
		$positions = $tea_exc->getIdPositions($id_row);
		//get id
		foreach ($positions as $key => $position)
			$positions[$key]['status'] = $tea_exc->displayPositionStatus($position['id_position'], $position['id_row'], $position['active'], $id_prof, $this->classname);
		$this->override_folder = 'tea_themelayout_base/';	
		$tpl = $this->createTemplate('positionlist.tpl');
		$tpl->assign(
			array(
				'link' => $this->context->link,
				'adminlink' => $this->context->link->getAdminLink($this->classname),
				'positions' => $positions,
				'id_row' => $id_row,
				'id_prof' => $id_prof
			)
		);
		return $tpl->fetch();
	}
    public function displayHeaderlist()
    {
        $this->override_folder = 'tea_themelayout_base/';
        $tpl = $this->createTemplate('HeaderAdmin.tpl');
		$tpl->assign(
			array(
				'link' => $this->context->link,
                'page_name' => str_replace('AdminTea_themelayout','',Tools::getValue('controller')),
			)
		);
		return $tpl->fetch();
    }	
}