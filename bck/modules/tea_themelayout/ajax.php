<?php 
include_once('../../config/config.inc.php');
include_once('../../init.php');
$id_block = Tools::getValue('id_block');
$module_name=Tools::getValue('module_name');
$block = Db::getInstance()->getRow('SELECT module_name,hook_name FROM '._DB_PREFIX_.'teaadv_blocks where id_block='.(int)$id_block);
$hooks= Db::getInstance()->executeS('SELECT h.name FROM '._DB_PREFIX_.'hook h,'._DB_PREFIX_.'module m,'._DB_PREFIX_.'hook_module hm WHERE  m.name="'.$module_name.'" AND h.id_hook=hm.id_hook AND m.id_module=hm.id_module AND h.name not like "action%"');
$options='';
if($hooks)
    foreach($hooks as $hook)
    {
        $options .='<option value="'.$hook['name'].'" '.($hook['name']==$block['hook_name']?' selected="selected"':'').' >'.$hook['name'].'</option>';
    }
die($options);
