<?php
/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
*/

class TeaBlock extends ObjectModel
{	
	public $id_position;
	public $title;
	public $show_title;
	public $html_content;
	public $block_type;
    public $type_parallax;
    public $link_video;
    public $type_product;
    public $number_product;
    public $list_id_product;
    public $id_category;
	public $hook_name;	
	public $module_name;
    public $backgroud;
    public $position_content;
    public $class_custom;
    public $group_slider;
    public $height;
	public $active;
    public $backgroud_widget;
	public $ordering;
    public $link_extra;
    public $animation;
    public $animation_type;
    public $padding;
    public $margin;
	public static $definition = array(
		'table' => 'teaadv_blocks',
		'primary' => 'id_block',
		'multilang' => true,
		'fields' => array(
			'id_position'	=>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
			'title'			=>	array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
			'show_title'	=>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
			'html_content'	=>	array('type' => self::TYPE_HTML, 'lang' => true, 'size' => 4000),
			'block_type'	=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => false, 'size' => 30),
            'type_parallax'	=>	array('type' => self::TYPE_STRING, 'required' => false, 'size' => 30), 
            'link_video'	=>	array('type' => self::TYPE_HTML, 'required' => false, 'size' => 3000),
            'type_product'	=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => false, 'size' => 30),
            'number_product'	=>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => false),
            'list_id_product'	=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => false, 'size' => 300),
            'id_category'	=>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => false),
            'group_slider'	=>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => false),
			'hook_name'		=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => false, 'size' => 30),			
			'module_name'	=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => false, 'size' => 100),
            'backgroud'	=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => false),
            'class_custom'	=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => false),
            'position_content'	=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => false),
            'height'	=>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => false),
			'active'		=>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'backgroud_widget'	=>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => false),
			'ordering'		=>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'link_extra'		=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
            'animation'		=>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'animation_type'		=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
            'padding'		=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
            'margin'		=>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
		)
	);
	public	function __construct($id_block = null, $id_lang = null, $id_shop = null)
	{
		parent::__construct($id_block, $id_lang, $id_shop);
	}
	public function add($autodate = true, $null_values = false)
	{
		$res = true;
		$res = parent::add($autodate, $null_values);
		return $res;
	}
	public function delete()
	{
		$res = true;		
		$res &= parent::delete();
		return $res;
	}
}