<?php
/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
*/

if (!defined('_PS_VERSION_'))
	exit;
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
include_once(_PS_MODULE_DIR_.'tea_themelayout/class/TeaExecute.php');
include_once(_PS_MODULE_DIR_.'tea_themelayout/params.php');
class Tea_ThemeLayout extends Module
{
	private $hookspos = array();
	private $hookalias = array();
	private $teaHooks = array();
	private $_themeskins = array();
	private $_producthovers = array();
	private $_productboxs = array();
	private $fields_options = array();	
	public function __construct()
	{
		$this->name = 'tea_themelayout';
		$this->tab = 'front_office_features';
		$this->version = '2.5.5';
		$this->author = 'prestagold.com';
		$this->need_instance = 0;
		$this->bootstrap = true;
		$this->id_header = 0;
		$this->id_homebody = 0;
		$this->id_footer = 0;
		parent::__construct();
		$this->displayName = $this->l('Prestagold Page Builder module');
		$this->description = $this->l('Home Page Builder For Prestashop Theme.');
		if(_TEA_THEME_SKINS_)
			$this->_themeskins = explode(",", _TEA_THEME_SKINS_);
		if(_TEA_PRODUCT_HOVERS_) {
			$hover_strs = explode(",", _TEA_PRODUCT_HOVERS_);
			foreach($hover_strs as $hover_str ) {
				$_fields = explode(":", $hover_str);
				$this->_producthovers[$_fields[0]] = $_fields[1];
			}	
		}
		if(_TEA_PRODUCT_BOXS_)		
			$this->_productboxs = explode(",", _TEA_PRODUCT_BOXS_);
	}
	public function install()
	{		
		if (parent::install() && $this->registerHook('header') && $this->registerHook('displayTop') && $this->registerHook('displayHome') && $this->registerHook('displayFooter') && $this->registerHook('displayBackOfficeHeader')) 
		{			
			$res = Configuration::updateValue('TEASETTING_SKIN', '');						
			$res &= Configuration::updateValue('TEASETTING_PRODUCTHOVER', '');	
			$res &= Configuration::updateValue('TEASETTING_PRODUCTBOX', '');	
			$res &= Configuration::updateValue('TEASETTING_RTL', '0');				
			$res &= Configuration::updateValue('TEASETTING_TOOLS', '1');	
			Configuration::updateValue('PS_ALLOW_HTML_IFRAME', 1);
			include(dirname(__FILE__).'/install/install.php');
			$install_demo = new TeaLayoutInstall();
			$install_demo->createTable();	
            $install_demo->InstallDemo();	
            $id_parent = Db::getInstance()->getValue('SELECT id_tab FROM '._DB_PREFIX_.'tab WHERE class_name="IMPROVE"');
			$id_tab1 = $this->addTab('Page Builder', 'dashboard',$id_parent);
			$this->addTab('Header', 'header', $id_tab1);
			$this->addTab('Body', 'homebody', $id_tab1);
			$this->addTab('Footer', 'footer', $id_tab1);
			$this->addTab('Home Pages', 'homepage', $id_tab1);			
			return true;
		}
		return false;
	}
	public function getCurrentHomePage()
	{
		if ($this->context->cookie->teahomepage != '')
			$homepage_id = $this->context->cookie->teahomepage;
		else
			$homepage_id = Configuration::get('TEAADV_HOMEPAGE');				

		$homepage = $this->getHomePage($homepage_id);
		return $homepage;		
	}
	public function uninstall()
	{
		/* Deletes Module */
		if (parent::uninstall())
		{
			$res = Configuration::deleteByName('TEASETTING_SKIN');					
			$res &= Configuration::deleteByName('TEASETTING_PRODUCTHOVER');				
			$res &= Configuration::deleteByName('TEASETTING_PRODUCTBOX');
			$res &= Configuration::deleteByName('TEASETTING_RTL');			
			$res &= Configuration::deleteByName('TEASETTING_TOOLS');						
			$sql = array();
			include(dirname(__FILE__).'/install/uninstall.php');
			foreach ($sql as $s)
				Db::getInstance()->execute($s);						
			Configuration::deleteByName('TEAADV_HOMEPAGE');
			$this->removeTab('header');
			$this->removeTab('homebody');
			$this->removeTab('footer');
			$this->removeTab('homepage');
			$this->removeTab('themesetting');			
			$this->removeTab('dashboard');	
			return $res;
		}
		return false;
	}
    public function getContent(){
        return '';
    }
	private function addTab($title, $class_sfx = '', $parent_id = 0)
	{
		$class = 'Admin'.Tools::ucfirst($this->name).Tools::ucfirst($class_sfx);
		@Tools::copy(_PS_MODULE_DIR_.$this->name.'/logo.gif', _PS_IMG_DIR_.'t/'.$class.'.gif');
		$_tab = new Tab();
		$_tab->class_name = $class;
		$_tab->module = $this->name;
		$_tab->id_parent = $parent_id;
		$langs = Language::getLanguages(false);
		foreach ($langs as $l)
			$_tab->name[$l['id_lang']] = $title;
		if ($parent_id == -1)
		{
			$_tab->id_parent = -1;
			$_tab->add();
		}
		else
			$_tab->add(true, false);
		return $_tab->id;
	}
	private function removeTab($class_sfx = '')
	{
		$tabClass = 'Admin'.Tools::ucfirst($this->name).Tools::ucfirst($class_sfx);
		$idTab = Tab::getIdFromClassName($tabClass);
		if ($idTab != 0)
		{
			$tab = new Tab($idTab);
			$tab->delete();
			return true;
		}
		return false;
	}
	public function getHomePage($id_homepage) 
	{	
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT *
			FROM '._DB_PREFIX_.'teaadv_homepages
			WHERE id_homepage = '.(int)$id_homepage
		);
	}
	public function getHomePages() 
	{		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT *
			FROM '._DB_PREFIX_.'teaadv_homepages
			ORDER BY ordering');	
	}
	private function setting_reset() 
	{		
		$this->context->cookie->__unset('teaskin');		
		$this->context->cookie->__unset('teahomepage');		
		$this->context->cookie->__unset('teaphover');		
		$this->context->cookie->__unset('teapbox');		
		$this->context->cookie->__unset('teartl');
	}
	public function hookDisplayHeader()	
	{	
        if(Tools::isSubmit('getdemo')&& (int)Tools::getValue('homepage_id')&& $this->getHomePage((int)Tools::getValue('homepage_id')))
        {
            $this->context->cookie->teahomepage= (int)Tools::getValue('homepage_id');
            $this->context->cookie->write();
        }
        /*if(Tools::getValue('controller')=='index')
        {*/
            $this->context->controller->addJS(($this->_path).'views/js/wow.min.js');

            $this->context->controller->addJS(($this->_path).'views/js/skrollr.min.js');

            $this->context->controller->addJS(($this->_path).'views/js/jquery.multiscroll.js');

            $this->context->controller->addJS(($this->_path).'views/js/masonry.js');

            $this->context->controller->addJS(($this->_path).'views/js/tea_themelayout_frontend.js');

            $this->context->controller->addCSS(($this->_path).'views/css/jquery.multiscroll.css', 'all');
        /*}*/
        $this->context->controller->addCSS(($this->_path).'views/css/flaticon.css', 'all');
        $this->context->controller->addCSS(($this->_path).'views/css/font-awesome.min.css', 'all');
        $this->context->controller->addCSS(($this->_path).'views/css/lightgallery.min.css', 'all');
        $this->context->controller->addCSS(($this->_path).'views/css/owl.carousel.min.css', 'all');
        $this->context->controller->addCSS(($this->_path).'views/css/preview.css', 'all');
        $this->context->controller->addCSS(($this->_path).'views/css/style.css', 'all');
        $this->context->controller->addJS(($this->_path).'views/js/imageloaded.js');
        $this->context->controller->addJS(($this->_path).'views/js/jquery.elevatezoom.js');  
        $this->context->controller->addJS(($this->_path).'views/js/sticky-kit.min.js');      
        $this->context->controller->addJS(($this->_path).'views/js/jquery.zoom.js');  
        $this->context->controller->addJS(($this->_path).'views/js/slick.js');  
        $this->context->controller->addJS(($this->_path).'views/js/magnific-popup.js');
        $this->context->controller->addJS(($this->_path).'views/js/owl.carousel.min.js');
        $this->context->controller->addJS(($this->_path).'views/js/isotope.pkgd.min.js','all');
		if (Tools::isSubmit('settingdemo') && (int)(Tools::getValue('settingdemo')) == 1) 
		{
			if (Tools::isSubmit('teaskin')) $this->context->cookie->teaskin = Tools::getValue('teaskin');									
			if (Tools::isSubmit('teahomepage')) $this->context->cookie->teahomepage = Tools::getValue('teahomepage');			
			if (Tools::isSubmit('teaphover')) $this->context->cookie->teaphover = Tools::getValue('teaphover');	
			if (Tools::isSubmit('teapbox')) $this->context->cookie->teapbox = Tools::getValue('teapbox');
			if (Tools::isSubmit('teartl')) $this->context->cookie->teartl = Tools::getValue('teartl');
			Tools::redirect($this->context->shop->getBaseURL());
		} 
		elseif (Tools::isSubmit('settingreset')) 
		{
			$this->setting_reset();
			Tools::redirect($this->context->shop->getBaseURL());
		}
		if ($this->context->cookie->teaskin != '') 
			$skin = $this->context->cookie->teaskin;
		else 		
			$skin = Configuration::get('TEASETTING_SKIN');					
		if ($skin == 'default' || $skin == 'preset1') 
			$skin = '';
		if ($this->context->cookie->teahomepage != '') 
			$homepage = $this->context->cookie->teahomepage;
		else 		
			$homepage = Configuration::get('TEAADV_HOMEPAGE');	
        $custom_class=  Db::getInstance()->getValue('SELECT custom_class FROM '._DB_PREFIX_.'teaadv_homepages where id_homepage='.(int)$homepage);			
		if ($this->context->cookie->teaphover != '') 		
			$phover = $this->context->cookie->teaphover;			
		else
			$phover = Configuration::get('TEASETTING_PRODUCTHOVER');
		if ($phover == 'image_swap') 
		{	
			$this->context->controller->addJS(($this->_path).'views/js/runajax.js');					
		}
		if ($this->context->cookie->jmspbox != '') 		
			$pbox = $this->context->cookie->teapbox;			
		else
			$pbox = Configuration::get('TEASETTING_PRODUCTBOX');
		if ($this->context->cookie->teartl != '') 		
			$rtl = $this->context->cookie->teartl;			
		else
			$rtl = Configuration::get('TEASETTING_RTL');		
		$homepages = $this->getHomePages();		
		$this->context->smarty->assign('phover', $phover);
		$this->context->smarty->assign('homepage', $homepage);
		$this->context->smarty->assign('pbox', $pbox);		
		$this->context->smarty->assign('rtl', $rtl);
        $this->context->smarty->assign('custom_class',$custom_class);
		$this->context->smarty->assign('themename', _THEME_NAME_);			
		$this->context->smarty->assign('themeskins', $this->_themeskins);				
		$this->context->smarty->assign('skin', $skin);				
		if (count($homepages)) 
			$this->context->smarty->assign('homepages', $homepages);
		if (count($this->_producthovers)) 
			$this->context->smarty->assign('producthovers', $this->_producthovers);		
		if (count($this->_productboxs)) 
			$this->context->smarty->assign('productboxs', $this->_productboxs);	
		$this->context->smarty->assign('tools', Configuration::get('TEASETTING_TOOLS'));				
	}
	public function hookTop()
	{
		$tea_exc = new TeaExecute();
		$this->context->controller->addCSS(($this->_path).'views/css/style.css', 'all');
		$homepage = $this->getCurrentHomePage();
		if ($homepage['id_header'])
			$rows = $tea_exc->getRows(1, $homepage['id_header']);
        if(isset($rows) && $rows)
		foreach ($rows as $key => $row)
		{
			$rows[$key]['positions'] = $tea_exc->getIdPositions($row['id_row'], 1);
			foreach ($rows[$key]['positions'] as $number => $position)
			{
				$values = $tea_exc->getActBlocks($position['id_position']);
				$rows[$key]['positions'][$number]['blocks'] = $values;
				foreach ($values as $count => $value)
					$rows[$key]['positions'][$number]['blocks'][$count]['block_type'] = $value['block_type'];
			}
		}
		$profile_class = $tea_exc->getProfClass($homepage['id_header']);
		$this->smarty->assign(array(
			'rows_header' => isset($rows)?$rows : array(),
			'class_header' => $profile_class,
		));
		return $this->display(__FILE__, 'teaadvheader.tpl');
	}
	public function hookdisplayHome()
	{
		$tea_exc = new TeaExecute();
		$homepage = $this->getCurrentHomePage();
		if ($homepage['id_homebody'])
			$rows = $tea_exc->getRows(1, $homepage['id_homebody']);
		if(isset($rows) && $rows)
		foreach ($rows as $key => $row)
		{
			$rows[$key]['positions'] = $tea_exc->getIdPositions($row['id_row'], 1);
			foreach ($rows[$key]['positions'] as $number => $position)
				$rows[$key]['positions'][$number]['blocks'] = $tea_exc->getActBlocks($position['id_position']);
		}
		$profile_class = $tea_exc->getProfClass($homepage['id_homebody']);
		$font_url = $tea_exc->getFontUrl($homepage['id_homepage']);
		$css_file = $tea_exc->getHomeCssFile($homepage['id_homepage']);
		if ($font_url)
			$this->context->controller->addCSS($font_url, 'text/css');	
		if ($css_file)
			$this->context->controller->addCSS($this->context->shop->getBaseURL().'themes/'._THEME_NAME_.'/css/'.$css_file, 'all');	
		$this->smarty->assign(array(
			'rows' => isset($rows)?$rows:array(),
			'class_home' => $profile_class
		));
		return $this->display(__FILE__, 'teaadvhomebody.tpl');
	}
	public function hookFooter()
	{
		$tea_exc = new TeaExecute();
		$homepage = $this->getCurrentHomePage();
		if ($homepage['id_footer'])		
			$rows = $tea_exc->getRows(1, $homepage['id_footer']);
        if(isset($rows)&&$rows)
		foreach ($rows as $key => $row)
		{
			$rows[$key]['positions'] = $tea_exc->getIdPositions($row['id_row'], 1);
			foreach ($rows[$key]['positions'] as $number => $position)
				$rows[$key]['positions'][$number]['blocks'] = $tea_exc->getActBlocks($position['id_position']);
		}
		$profile_class = $tea_exc->getProfClass($homepage['id_footer']);
		$this->context->smarty->assign(array(
			'rows_footer' => isset($rows)?$rows:array(),
			'class_footer' => $profile_class
		));
		return $this->display(__FILE__, 'teaadvfooter.tpl');
	}
	public function getAllImages($id_lang, $where, $order)
	{
		$sql = 'SELECT i.`id_product`, image_shop.`cover`, i.`id_image`, il.`legend`, i.`position`,pl.`link_rewrite`
				FROM `'._DB_PREFIX_.'image` i
				'.Shop::addSqlAssociation('image', 'i').'
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (i.`id_product` = pl.`id_product`) 
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')'.$where.' '.$order;
		return Db::getInstance()->executeS($sql);
	}
	public function getSecondImgs($productids)
	{
		$link = $this->context->link;
		$id_lang = Context::getContext()->language->id;
		$where  = ' WHERE i.`id_product` IN ('.$productids.') AND (i.`cover` IS NULL OR i.`cover` = 0)';
		$order  = ' ORDER BY i.`id_product`,`position`';
		$limit  = ' LIMIT 0,1';
		//get product info\
		$listImg = $this->getAllImages($id_lang, $where, $order, $limit);
		$savedImg = array();
		$obj = array();
		$this->smarty->assign(array('homeSize' => Image::getSize(ImageType::getFormatedName('home')),'mediumSize' => Image::getSize(ImageType::getFormatedName('medium')),'smallSize' => Image::getSize(ImageType::getFormatedName('small'))));
		foreach ($listImg as $product)
		{
			if (!in_array($product['id_product'], $savedImg))
				$obj[] = array('id'=>$product['id_product'],'content'=>($link->getImageLink($product['link_rewrite'], $product['id_image'], 'home_default')));
			$savedImg[] = $product['id_product'];
		}
		return $obj;
	}
    public function getProductsByIdCategory($id_category,$count_product)
    {
        $category = new Category($id_category,$this->context->language->id);
        $products = $category->getProducts($this->context->language->id, 1, $count_product);
        $assembler = new ProductAssembler($this->context);
        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );
        $products_for_template = [];
        if($products)
            foreach ($products as $rawProduct) {
                $products_for_template[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct($rawProduct),
                    $this->context->language
                );
            }
        $this->context->smarty->assign('products',$products_for_template);
        return $this->display(__FILE__, 'list_products.tpl');
    }
    public function getProductsBestSales($count_product)
    {
        $products = ProductSale::getBestSales($this->context->language->id,0 ,$count_product,null, null);
        $assembler = new ProductAssembler($this->context);
        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );
        $products_for_template = [];
        if($products)
            foreach ($products as $rawProduct) {
                $products_for_template[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct($rawProduct),
                    $this->context->language
                );
            }
        $this->context->smarty->assign('products',$products_for_template);
        return $this->display(__FILE__, 'list_products.tpl');
    }
    public function getProductsSpecial($count_product)
    {
        $products = Product::getPricesDrop($this->context->language->id,0, (int)$count_product, false, null, null);
        $assembler = new ProductAssembler($this->context);
        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );
        $products_for_template = [];
        if($products)
            foreach ($products as $rawProduct) {
                $products_for_template[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct($rawProduct),
                    $this->context->language
                );
            }
        $this->context->smarty->assign('products',$products_for_template);
        return $this->display(__FILE__, 'list_products.tpl');
    }
    public function getNewProducts($count_product)
    {
        $products = Product::getNewProducts($this->context->language->id, 0, (int)$count_product, false, null, null);
        $assembler = new ProductAssembler($this->context);
        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );
        $products_for_template = [];
        if($products)
            foreach ($products as $rawProduct) {
                $products_for_template[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct($rawProduct),
                    $this->context->language
                );
            }
        $this->context->smarty->assign('products',$products_for_template);
        return $this->display(__FILE__, 'list_products.tpl');
    }
    public function getListProductsById($list_id_product,$count_product)
    {
        $id_lang =$this->context->language->id;
        $sql = 'SELECT p.*, product_shop.*, pl.* , m.`name` AS manufacturer_name, s.`name` AS supplier_name
				FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
				LEFT JOIN `'._DB_PREFIX_.'supplier` s ON (s.`id_supplier` = p.`id_supplier`)
				WHERE pl.`id_lang` = '.(int)$id_lang.' AND product_shop.`visibility` IN ("both", "catalog") AND product_shop.`active` = 1 AND p.id_product IN ('.$list_id_product.') LIMIT 0,'.(int)$count_product;
        $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if($products)
        foreach($products as &$product)
        {
            $class_product = new Product($product['id_product']);
            $product['id_image']=Db::getInstance()->getValue('select id_image FROM '._DB_PREFIX_.'image WHERE id_product='.$product['id_product'].' AND cover=1');
            if(!$product['id_image'])
                $product['id_image']=Db::getInstance()->getValue('select id_image FROM '._DB_PREFIX_.'image WHERE id_product='.$product['id_product']);
            $product['link']=$this->context->link->getProductLink($product['id_product']);
            $product['price']=$class_product->getPrice(true);
            $product['price_tax_exc']=$class_product->getPrice(false);
        }
        $assembler = new ProductAssembler($this->context);
        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );
        $products_for_template = [];
        if($products)
            foreach ($products as $rawProduct) {
                $products_for_template[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct($rawProduct),
                    $this->context->language
                );
            }
        $this->context->smarty->assign('products',$products_for_template);
        return $this->display(__FILE__, 'list_products.tpl');
    }
    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addCSS((__PS_BASE_URI__).'modules/'.$this->name.'/views/css/admin.css','all');
    }
}