<?php
/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
*/

include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once('tea_themelayout.php');
$context = Context::getContext();
$rows = array();
if (Tools::getValue('action') == 'updateHomesOrdering' && Tools::getValue('homepage'))
{
	$homepage = Tools::getValue('homepage');
	foreach ($homepage as $position => $id_homepage)
	{
		$res = Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'teaadv_homepages` SET `ordering` = '.(int)$position.'
			WHERE `id_homepage` = '.(int)$id_homepage
		);
	}
	$tea_homepage = new TeaHomepage();
	$tea_homepage->clearCache();
}
if (Tools::getValue('action') == 'updateProfOrdering' && Tools::getValue('prof'))
{
	$prof = Tools::getValue('prof');
	foreach ($prof as $position => $id_prof)
	{
		$res = Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'teaadv_prof` SET `ordering` = '.(int)$position.'
			WHERE `id_prof` = '.(int)$id_prof
		);
	}
	$tea_prof = new TeaProf();
	$tea_prof->clearCache();
}
if (Tools::getValue('action') == 'updateRowsOrdering' && Tools::getValue('row'))
{
	$rows = Tools::getValue('row');
	foreach ($rows as $position => $id_row)
	{
		$res = Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'teaadv_rows` SET `ordering` = '.(int)$position.'
			WHERE `id_row` = '.(int)$id_row
		);
	}
	$tea_row = new TeaRow();
	$tea_row->clearCache();
}
if (Tools::getValue('action') == 'updatePositionsOrdering' && Tools::getValue('position'))
{
	$positions = Tools::getValue('position');	
	foreach ($positions as $position => $id_pos)
	{
		$res = Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'teaadv_position` SET `ordering` = '.(int)$position.'
			WHERE `id_position` = '.(int)$id_pos
		);
	}
	$tea_position = new TeaPosition();
	$tea_position->clearCache();
}
if (Tools::getValue('action') == 'updateBlocksOrdering' && Tools::getValue('block'))
{
	$blocks = Tools::getValue('block');	
	foreach ($blocks as $position => $id_block)
	{
		$res = Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'teaadv_blocks` SET `ordering` = '.(int)$position.'
			WHERE `id_block` = '.(int)$id_block
		);
	}
	$tea_block = new TeaBlock();
	$tea_block->clearCache();
}
if (Tools::getValue('action') == 'savepos' && Tools::getValue('pos'))
{
	$pos = Tools::getValue('pos');			
	$pos_arr = explode('|', $pos);	
	foreach ($pos_arr as $position)
	{
		$pos_obj = explode('-', $position);
		$query = 'UPDATE `'._DB_PREFIX_.'teaadv_position` SET `col_lg` = '.$pos_obj[1].', `col_md` = '.$pos_obj[2].', `col_sm` = '.$pos_obj[3].', `col_xs` = '.$pos_obj[4].
		'	WHERE `id_position` = '.$pos_obj[0];
		$res = Db::getInstance()->execute($query);
	}	
}