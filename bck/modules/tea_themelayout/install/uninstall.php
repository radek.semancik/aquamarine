<?php
/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
*/

	$sql = array();
	$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'teaadv_homepages`';
	$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'teaadv_prof`';
	$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'teaadv_rows`';
	$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'teaadv_position`';
	$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'teaadv_blocks`';
	$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'teaadv_blocks_lang`';	