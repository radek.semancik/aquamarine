<?php
/**
* 2016 PrestaShop
*
* Tea Theme Layout
*
*  @author    teathemes.net <teathems.net@gmail.com>
*  @copyright 2016 Teathemes.net
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.teathemes.net
*/

include_once(_PS_MODULE_DIR_.'tea_themelayout/TeaHomepage.php');
include_once(_PS_MODULE_DIR_.'tea_themelayout/TeaProf.php');
include_once(_PS_MODULE_DIR_.'tea_themelayout/TeaRow.php');
include_once(_PS_MODULE_DIR_.'tea_themelayout/TeaPosition.php');
include_once(_PS_MODULE_DIR_.'tea_themelayout/TeaBlock.php');
class TeaLayoutInstall
{
	public function createTable()
	{
		$sql = array();
		$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'teaadv_homepages` (
					`id_homepage` int(11) NOT NULL AUTO_INCREMENT,
					`title` varchar(100) NOT NULL,
					`id_header` int(11) NOT NULL,
					`id_homebody` int(11) NOT NULL,
					`id_footer` int(11) NOT NULL,
					`css_file` varchar(200) NOT NULL,
					`font_url` varchar(255) NOT NULL,
                    `custom_class` varchar(222) NOT NULL,
					`ordering` int(11) NOT NULL,
					PRIMARY KEY (`id_homepage`)
				 ) ENGINE=InnoDB  DEFAULT CHARSET=UTF8';
		$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'teaadv_prof` (
					`id_prof` int(10) unsigned NOT NULL AUTO_INCREMENT,
					`title` varchar(255) NOT NULL,
					`profile_type` varchar(255) NOT NULL,
					`class_suffix` varchar(200),					
					`ordering` int(10) unsigned NOT NULL,
					PRIMARY KEY (`id_prof`)
				) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8';
		$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'teaadv_rows` (
					`id_row` int(10) unsigned NOT NULL AUTO_INCREMENT,					
					`id_prof` int(10) unsigned NOT NULL,
					`title` varchar(255) NOT NULL,
					`class` varchar(200) NOT NULL,
					`fullwidth` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
					`active` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
					`ordering` int(10) unsigned NOT NULL,
                    `padding` text NOT NULL,
                    `margin` text NOT NULL, 
					PRIMARY KEY (`id_row`)
				) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8';
		$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'teaadv_position` (
					`id_position` int(10) unsigned NOT NULL AUTO_INCREMENT,
					`id_row` int(10) unsigned NOT NULL,
					`title` varchar(255) NOT NULL,
					`class_suffix` varchar(200) NOT NULL,
					`col_lg` int(11) NOT NULL,
					`col_sm` int(11) NOT NULL,
					`col_md` int(11) NOT NULL,
					`col_xs` int(11) NOT NULL,
					`active` tinyint(1) unsigned NOT NULL DEFAULT 0,
					`ordering` int(10) unsigned NOT NULL,
				  PRIMARY KEY (`id_position`)
				) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8';
		$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'teaadv_blocks` (
				  `id_block` int(10) unsigned NOT NULL AUTO_INCREMENT,
				  `id_position` int(10) unsigned NOT NULL,
				  `show_title` tinyint(1) unsigned NOT NULL,
				  `block_type` varchar(25) NOT NULL,
                  `type_parallax` varchar(12) NOT NULL,                  
                  `link_video` text NOT NULL,
                  `type_product` varchar(222) NOT NUll,
                  `number_product` INT (10),
                  `list_id_product` VARCHAR(225) NOT NULL,
                  `id_category` INT (10) NOT NULL,
                  `group_slider` INT(10) NOT NULL,
				  `hook_name` varchar(30) NOT NULL,
				  `module_name` varchar(100) NOT NULL,
                  `backgroud` text NOT NULL,
                  `height` int(11) NOT NULL,
                  `class_custom` varchar(222) NOT NULL,
                  `position_content` varchar(100) NOT NULL,
                  `backgroud_widget` int(2),
				  `active` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
				  `ordering` int(10) unsigned NOT NULL,
                  `link_extra` text NOT NULL,
                  `animation` int(2) NOT NULL,
                  `animation_type` text NOT NULL,
                  `padding` text NOT NULL,
                  `margin` text NOT NULL,  
				  PRIMARY KEY (`id_block`)
				) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8';
		$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'teaadv_blocks_lang` (
					`id_block` int(10) unsigned NOT NULL AUTO_INCREMENT,
					`id_lang` int(10) unsigned NOT NULL,
    				`title` varchar(255) NOT NULL,
					`html_content` text NOT NULL,
					PRIMARY KEY (`id_block`, `id_lang`)
				) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8';
		foreach ($sql as $s)
			if (!Db::getInstance()->execute($s))
				return false;
	}
	public function _addHomePage($title, $header_id, $body_id, $footer_id, $ordering, $css_file = '', $font_url = '',$custom_class='')
	{
		$homepage = new TeaHomepage();
		$homepage->title = $title;
		$homepage->id_header = $header_id;
		$homepage->id_homebody = $body_id;
		$homepage->id_footer = $footer_id;
		$homepage->css_file = $css_file;
		$homepage->font_url = $font_url;
		$homepage->ordering = $ordering;
        $homepage->custom_class =$custom_class;
		$homepage->add();
		return $homepage->id;
	}
	public function _addProfile($title, $profile_type, $class_suffix, $ordering)
	{
		$prof = new TeaProf();
		$prof->title = $title;
		$prof->profile_type = $profile_type;
		$prof->class_suffix = $class_suffix;
		$prof->ordering = $ordering;
		$prof->add();
		return $prof->id;
	}
	public function _addRow($prof_id, $title, $class_suffix, $fullwidth, $ordering = 0, $active = 1,$padding='',$margin='')
	{
		$row = new TeaRow();		
		$row->id_prof = $prof_id;
		$row->title = $title;
		$row->class = $class_suffix;
		$row->fullwidth = $fullwidth;
		$row->active = $active;
		$row->ordering = $ordering;
        $row->padding=$padding;
        $row->margin=$margin;
		$row->add();
		return $row->id;
	}
	public function _addPos($row_id, $title, $class_suffix, $ordering = 0, $col_lg = 3, $col_md = 3, $col_sm = 6, $col_xs = 12, $active = 1)
	{
		$position = new TeaPosition();
		$position->id_row = $row_id;
		$position->title = $title;
		$position->class_suffix = $class_suffix;
		$position->col_lg = $col_lg;
		$position->col_md = $col_md;
		$position->col_sm = $col_sm;
		$position->col_xs = $col_xs;
		$position->active = $active;
		$position->ordering = $ordering;
		$position->add();
		return $position->id;
	}
	public function _addModule($pos_id, $blockname, $title, $hookname, $ordering = 0, $showtitle = 0, $active = 1,$class_custom='',$padding='',$margin='')
	{
		$languages = Language::getLanguages(false);
		$block = new TeaBlock();
		$block->id_position = $pos_id;
		$block->show_title = $showtitle;
		$block->block_type = 'module';
		$block->hook_name = $hookname;		
		$block->module_name = $blockname;
		$block->active = $active;
		$block->ordering = $ordering;
        $block->padding=$padding;
        $block->margin=$margin;
        $block->class_custom=$class_custom;
		foreach ($languages as $language)
			$block->title[$language['id_lang']] = $title;
		$block->add();
	}
	public function _addHtml($pos_id, $title, $html, $ordering = 0, $showtitle = 1, $active = 1,$class_custom='',$padding='',$margin='',$animation=0,$animation_type='')
	{
		$languages = Language::getLanguages(false);
		$block = new TeaBlock();
		$block->id_position = $pos_id;
		$block->show_title = $showtitle;
		$block->block_type = 'custom_html';
		$block->hook_name = 'footer';
		$block->module_name = '';
		$block->active = $active;
		$block->ordering = $ordering;
        $block->class_custom=$class_custom;
        $block->padding=$padding;
        $block->margin=$margin;
        $block->animation=(int)$animation;
        $block->animation_type=$animation_type;
		foreach ($languages as $language)
		{
			$block->title[$language['id_lang']] = $title;
			$block->html_content[$language['id_lang']] = $html;
		}
		$block->add();
	}
	public function _addLogoTheme($pos_id, $title, $ordering = 0, $active = 1)
	{
		$languages = Language::getLanguages(false);
		$block = new TeaBlock();
		$block->id_position = $pos_id;
		$block->show_title = 0;
		$block->block_type = 'logo';
		$block->hook_name = 'footer';
		$block->module_id = 0;
		$block->active = $active;
		$block->ordering = $ordering;
		foreach ($languages as $language)
		{
			$block->title[$language['id_lang']] = $title;			
		}
		$block->add();
	}
    public function _addParallax($pos_id, $title, $html, $ordering = 0, $showtitle = 1, $active = 1,$class_custom='',$padding='',$margin='',$animation=0,$animation_type='',$backgroud='',$height='')
    {
        $languages = Language::getLanguages(false);
        $block= new TeaBlock();
        $block->id_position=$pos_id;
        $block->ordering=0;
        $block->show_title=$showtitle;
        $block->active=$active;
        $block->block_type = 'parallax';
        $block->class_custom=$class_custom;
        $block->padding=$padding;
        $block->margin=$margin;
        $block->animation=(int)$animation;
        $block->animation_type=$animation_type;
        $block->backgroud=$backgroud;
        $block->height=$height;
        foreach ($languages as $language)
		{
			$block->title[$language['id_lang']] = $title;
			$block->html_content[$language['id_lang']] = $html;
		}
        $block->add();
    }
    public function _addWidgetBlock($pos_id, $title, $html, $ordering = 0, $showtitle = 1, $active = 1,$class_custom='',$padding='',$margin='',$animation=0,$animation_type='',$backgroud='',$height='',$position_content='',$link_extra='',$backgroud_widget=0)
    {
        $languages = Language::getLanguages(false);
        $block= new TeaBlock();
        $block->id_position=$pos_id;
        $block->ordering=0;
        $block->show_title=$showtitle;
        $block->active=$active;
        $block->class_custom=$class_custom;
        $block->padding=$padding;
        $block->margin=$margin;
        $block->block_type = 'widget_block';
        $block->animation=(int)$animation;
        $block->animation_type=$animation_type;
        $block->backgroud=$backgroud;
        $block->height=$height;
        $block->position_content=$position_content;
        $block->link_extra=$link_extra;
        $block->backgroud_widget=(int)$backgroud_widget;
        foreach ($languages as $language)
		{
			$block->title[$language['id_lang']] = $title;
			$block->html_content[$language['id_lang']] = $html;
		}
        $block->add();
    }
    public function _addListProduct($pos_id, $title, $ordering = 0, $showtitle = 1, $active = 1,$class_custom='',$padding='',$margin='',$animation=0,$animation_type='',$type_product='',$number_product=0)
    {
        $languages = Language::getLanguages(false);
        $block= new TeaBlock();
        $block->id_position=$pos_id;
        $block->ordering=0;
        $block->show_title=$showtitle;
        $block->active=$active;
        $block->block_type = 'list_product';
        $block->type_product=$type_product;
        $block->number_product=(int)$number_product;
        $block->class_custom=$class_custom;
        $clock->padding=$padding;
        $block->margin=$margin;
        $block->animation=(int)$animation;
        $block->animation_type=$animation_type;
        foreach ($languages as $language)
		{
			$block->title[$language['id_lang']] = $title;
		}
        $block->add();
    }
    public function _addSlider($pos_id, $title, $ordering = 0, $showtitle = 1, $active = 1,$class_custom='',$padding='',$margin='',$group_slider='')
    {
        $languages = Language::getLanguages(false);
        $block= new TeaBlock();
        $block->id_position=$pos_id;
        $block->ordering=0;
        $block->show_title=$showtitle;
        $block->active=$active;
        $block->block_type = 'slider'; 
        $block->class_custom=$class_custom;
        $block->padding=$padding;
        $block->margin=$margin;
        $block->group_slider=$group_slider;
        foreach ($languages as $language)
		{
			$block->title[$language['id_lang']] = $title;
		}
        $block->add();
    }
	public function InstallXML($filename) 
	{			
		$profile_ids = array();
		$context = Context::getContext();
		$shop_id = $context->shop->id;
		if (file_exists(_PS_MODULE_DIR_.'tea_themelayout/xml/'.$filename))	
		{	
			$xml = simplexml_load_file(_PS_MODULE_DIR_.'tea_themelayout/xml/'.$filename);
			foreach ($xml->profile as $profile)
			{				
			     $profile_id = $this->_addProfile((string)$profile['title'], (string)$profile['profile_type'], (string)$profile['class_suffix'],0);
				if(!isset($profile_ids['custom_class']))
                    $profile_ids['custom_class'] = $profile['class_home_page'];
                $profile_ids[(string)$profile['profile_type']][] = $profile_id;
				$row_ordering = 0;
				foreach ($profile->row as $row)
				{
					$row_id = $this->_addRow($profile_id, (string)$row['title'], (string)$row['class'], (int)$row['fullwidth'], $row_ordering,1,$row['padding'],$row['margin']);			
					$pos_ordering = 0;
					foreach ($row->position as $position)
					{
						$pos_id = $this->_addPos($row_id, (string)$position['title'], (string)$position['class_suffix'], $pos_ordering, (int)$position['col_lg'], (int)$position['col_sm'], (int)$position['col_md'], (int)$position['col_xs']);
						$block_ordering = 0;
						foreach ($position->block as $block)
						{
							if ((string)$block['block_type'] == 'module')
								$this->_addModule($pos_id, (string)$block['module_name'], (string)$block['title'], (string)$block['hook_name'], $block_ordering,$block['show_title'],1,$block['class_custom'],$block['padding'],$block['margin']);
							elseif ((string)$block['block_type'] == 'custom_html') 
							{
								$html = (string)$block->htmlData[0];
								$this->_addHtml($pos_id, (string)$block['title'], $html, $block_ordering, (int)$block['show_title'],1,$block['class_custom'],$block['padding'],$block['margin'],$block['animation'],$block['animation_type']);
							}
                            elseif((string)$block['block_type']=='parallax')
                            {
                                $html = (string)$block->htmlData[0];
                                $this->_addParallax($pos_id,$block['title'],$html,$block_ordering,$block['show_title'],1,$block['class_custom'],$block['padding'],$block['margin'],$block['animation'],$block['animation_type'],$block['backgroud'],$block['height']);
                            }
                            elseif((string)$block['block_type']=='widget_block')
                            {
                                $html = (string)$block->htmlData[0];
                                $this->_addWidgetBlock($pos_id,$block['title'],$html,$block_ordering,$block['show_title'],1,$block['class_custom'],$block['padding'],$block['margin'],$block['animation'],$block['animation_type'],$block['backgroud'],$block['height'],$block['position_content'],$block['link_extra'],$block['backgroud_widget']);
                            }
                            elseif((string)$block['block_type']=='list_product')
                            {
                               $this->_addListProduct($pos_id,$block['title'],$block_ordering,$block['show_title'],1,$block['class_custom'],$block['padding'],$block['margin'],$block['animation'],$block['animation_type'],$block['type_product'],$block['number_product']);
                            }
                            elseif((string)$block['block_type']=='slider')
                            {
                                $this->_addSlider($pos_id,$block['title'],$block_ordering,$block['show_title'],1,$block['class_custom'],$block['padding'],$block['margin'],$block['group_slider']);
                            }
							elseif ((string)$block['block_type'] == 'logo') 
							{
								$this->_addLogoTheme($pos_id, 'Logo', 0);
							}
							$block_ordering++;
						}		
						$pos_ordering++;
					}
					$row_ordering++;
				}
			}	
		}
		return $profile_ids;	
	}
	public function InstallDemo() 
	{
        $profile1_ids = $this->InstallXML('home1.xml');
		$home1_id = $this->_addHomePage('Home 1 (default)', $profile1_ids['header'][0], $profile1_ids['homebody'][0], $profile1_ids['footer'][0], 0,'','',$profile1_ids['custom_class']);
		Configuration::updateValue('TEAADV_HOMEPAGE', $home1_id);
        $profile1_ids = $this->InstallXML('home2.xml');
		$home1_id = $this->_addHomePage('Home 2 (classic)', $profile1_ids['header'][0], $profile1_ids['homebody'][0], $profile1_ids['footer'][0], 0,'','',$profile1_ids['custom_class']);
        $profile1_ids = $this->InstallXML('home3.xml');
		$home1_id = $this->_addHomePage('home 3 (product list)', $profile1_ids['header'][0], $profile1_ids['homebody'][0], $profile1_ids['footer'][0], 0,'','',$profile1_ids['custom_class']);
        $profile1_ids = $this->InstallXML('home4.xml');
		$home1_id = $this->_addHomePage('Home 4 (static image)', $profile1_ids['header'][0], $profile1_ids['homebody'][0], $profile1_ids['footer'][0], 0,'','',$profile1_ids['custom_class']);
        $profile1_ids = $this->InstallXML('home5.xml');
		$home1_id = $this->_addHomePage('Home 5 (product masonry)', $profile1_ids['header'][0], $profile1_ids['homebody'][0], $profile1_ids['footer'][0], 0,'','',$profile1_ids['custom_class']);
        $profile1_ids = $this->InstallXML('home6.xml');
		$home1_id = $this->_addHomePage('home 6 (parallax)', $profile1_ids['header'][0], $profile1_ids['homebody'][0], $profile1_ids['footer'][0], 0,'','',$profile1_ids['custom_class']);
        $profile1_ids = $this->InstallXML('home7.xml');
		$home1_id = $this->_addHomePage('home 7 (lookbook)', $profile1_ids['header'][0], $profile1_ids['homebody'][0], $profile1_ids['footer'][0], 0,'','',$profile1_ids['custom_class']);
        $profile1_ids = $this->InstallXML('home8.xml');
		$home1_id = $this->_addHomePage('home 8 (home instagram)', $profile1_ids['header'][0], $profile1_ids['homebody'][0], $profile1_ids['footer'][0], 0,'','',$profile1_ids['custom_class']);
        $profile1_ids = $this->InstallXML('home9.xml');
		$home1_id = $this->_addHomePage('Home page 9', $profile1_ids['header'][0], $profile1_ids['homebody'][0], $profile1_ids['footer'][0], 0,'','',$profile1_ids['custom_class']);
	}
}