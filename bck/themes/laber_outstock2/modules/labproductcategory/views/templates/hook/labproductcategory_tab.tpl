
{foreach from=$group_cat_result item=group_cat name=group_cat_result}

	{assign var='id' value=$group_cat.id_labgroupcategory}
	{assign var='show_sub' value=$group_cat.show_sub}
	{assign var='use_slider' value=$group_cat.use_slider}
	{assign var='type_display' value=$group_cat.type_display}

{/foreach}
{$number_line = 1}
{$id_lang = Context::getContext()->language->id}
{foreach from=$group_cat_result item=group_cat name=group_cat_result}
{if $group_cat.show_sub}
	{assign var='nbItemsPerLine' value=4}
	{assign var='nbItemsPerLineTablet' value=3}
	{assign var='nbItemsPerLineMobile' value=2}
{/if}
{if $group_cat.id_labgroupcategory == $id }
<div class="block-content labProductList type-tab clearfix">
	<div class="lab_tab">
		<ul id="LabProductCategoryTabs" class="nav nav-tabs clearfix">
			{foreach from=$group_cat_info item=cat_info name=g_cat_info}
				<li class="nav-item">
					<a data-toggle="tab" href="#labProdCat-{$id}{$smarty.foreach.g_cat_info.index|intval}_tab" class="{if $smarty.foreach.g_cat_info.first} active{/if} nav-link">
						{if $cat_info.cat_icon !=''}
						<span class="icon_cat">
							<img src="{$icon_path nofilter}{$cat_info.cat_icon nofilter}" alt=""/>
						</span>
						{/if}
						 <span>{$cat_info.cat_name  nofilter} </span>
					</a>
				</li>
			{/foreach}
		</ul>
	</div>
	<div class="tab-content clearfix ">
		{foreach from=$group_cat_info item=cat_info name=g_cat_info}
		<div id="labProdCat-{$id}{$smarty.foreach.g_cat_info.index|intval}_tab" class="tab-pane {if $smarty.foreach.g_cat_info.first} active{/if}">
			{if $cat_info.show_img == 1 && isset($cat_info.id_image) && $cat_info.id_image > 0}
			<div class="cat-img">
				<a href="{$link->getCategoryLink($cat_info.id_cat, $cat_info.link_rewrite)  nofilter}" title="{$cat_info.cat_name  nofilter}">
					<!-- <img src="{$link->getCatImageLink($cat_info.link_rewrite, $cat_info.id_image, 'medium_default')  nofilter}"/> -->
					<img src="../img/c/{$cat_info.id_image}.jpg"/>
				</a>
			</div>
			{/if}
			{if $cat_info.cat_desc}
				<div class="cat_desc">
					<p>{$cat_info.cat_desc nofilter}</p>
				</div>
			{/if}
			{if $show_sub}
			<div class="sub-cat">
				<ul class="sub-cat-ul">
					{foreach from = $cat_info.sub_cat item=sub_cat name=sub_cat_info}
						<li><a href="{$link->getCategoryLink($sub_cat.id_category, $sub_cat.link_rewrite)  nofilter}" title="{$sub_cat.name  nofilter}">{$sub_cat.name  nofilter}</a></li>
					{/foreach}
				</ul>
			</div>
			{/if}
			{if $cat_info.cat_banner !=''}
			<div class="cat-banner">
				{if $cat_info.cat_banner!='' }
					<a href="{$link->getCategoryLink($cat_info.id_cat, $cat_info.link_rewrite)  nofilter}" title="{$cat_info.cat_name  nofilter}">
					<img src="{$banner_path  nofilter}{$cat_info.cat_banner  nofilter}" alt=""/>
					</a>
				{/if}
			</div>
			{/if}
			<div class="labContent">
				<div class="product_list row">
					<div class="owlProductCate-{$id}{$cat_info.id_cat|intval} {if $use_slider != 1}row{/if}">
						{if isset($cat_info.product_list) && count($cat_info.product_list) > 0}
						{foreach from=$cat_info.product_list item=product name=product_list}
							{if $use_slider != 1}

								<div class="item-inner col-lg-3 col-md-3 col-sm-4 col-xs-12
								{if $smarty.foreach.product_list.iteration%$nbItemsPerLine == 0} last-in-line
								{elseif $smarty.foreach.product_list.iteration%$nbItemsPerLine == 1} first-in-line{/if}
								{if $smarty.foreach.product_list.iteration%$nbItemsPerLineTablet == 0} last-item-of-tablet-line
								{elseif $smarty.foreach.product_list.iteration%$nbItemsPerLineTablet == 1} first-item-of-tablet-line{/if}
								{if $smarty.foreach.product_list.iteration%$nbItemsPerLineMobile == 0} last-item-of-mobile-line
								{elseif $smarty.foreach.product_list.iteration%$nbItemsPerLineMobile == 1} first-item-of-mobile-line{/if}
								">
							{else}
								{if $smarty.foreach.product_list.iteration % $number_line == 1 || $number_line == 1}
									<div class="item-inner ajax_block_product">
								{/if}
							{/if}

							{block name='product'}
							   {include file="catalog/_partials/miniatures/product.tpl"  product = $product}
							{/block}

								{*<div class="product-container">
									<div class="product-container-img">
										<a class="product_img_link" href="{$product.link  nofilter}" title="{$product.legend  nofilter}">
											<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')  nofilter}" alt="{$product.legend|escape:html:'UTF-8'}" />
										</a>
										<div class="lab-label">
										{if isset($product.new) && $product.new == 1}
											<a class="new-box" href="{$product.link  nofilter}">
												<span class="new-label">{l s='New' mod='labproductcategory'}</span>
											</a>
										{/if}
										{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
											<a class="sale-box" href="{$product.link  nofilter}">
												<span class="sale-label">{l s='Sale!' mod='labproductcategory'}</span>
											</a>
										{/if}
										</div>
										{if isset($quick_view) && $quick_view}
										<a class="quick-view" href="{$product.link  nofilter}" rel="{$product.link  nofilter}" title="{l s='Quick view' mod='labproductcategory'}">
											<span>{l s='Quick view' mod='labproductcategory'}</span>
										</a>
										{/if}
									</div>
									<div class="g-prod-content">
										<h5 class="product-name">
										<a class="" href="{$product.link  nofilter}" title="{$product.legend  nofilter}">		{$product.name  nofilter}
										</a>
										</h5>
										{hook h='displayProductListReviews' product=$product}
										{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
										<div class="content_price">
											{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
												{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
													<span class="price product-price special-price">
													{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
													</span>
													{hook h="displayProductPriceBlock" product=$product type="old_price"}
													<span class="old-price product-price">
														{displayWtPrice p=$product.price_without_reduction}
													</span>
													{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
												{else}
													<span class="price product-price">
													{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
													</span>
												{/if}
												{hook h="displayProductPriceBlock" product=$product type="price"}
												{hook h="displayProductPriceBlock" product=$product type="unit_price"}
											{/if}
										</div>
										{/if}
									</div>
									{$imageAll=Image::getImages(Context::getContext()->language->id,$product.id_product)}
									{if count($imageAll)>1}
										<ul class="img-thumbnail">
										{$i=0}
										{foreach from=$imageAll item=imaget name=images}
											{if $imaget.cover!=1}
												{$i=$i+1}
												{if $i<=3}
												{assign  var=img_second_id value ="`$product.id_product`-`$imaget.id_image`"}
												<li class="item-thumbnail">
												<img class="img_hover" src="{$link->getImageLink($product.link_rewrite,$img_second_id, 'small_default')|escape:'htmlall':'UTF-8'}" alt="{$product.legend|escape:'htmlall':'UTF-8'}" />
												</li>
												{/if}
											{/if}
										{/foreach}
										</ul>
									{/if}
								</div>*}
							{if $use_slider != 1}
								</div>
							{else}
								{if $smarty.foreach.product_list.iteration % $number_line == 0 ||$smarty.foreach.product_list.last || $number_line == 1}
								</div>
								{/if}
							{/if}
						{/foreach}
						{else}
							<div class="item product-box ajax_block_product">
								<p class="alert alert-warning">{l s='No product at this time' d='Modules.labproductcategory.Admin'}</p>
							</div>
						{/if}
					</div>
					{if count($cat_info.manufacture)>0}
					<div class="manu-list">
						<ul>
							{foreach from=$cat_info.manufacture item=manu_item name=manufacture}
								<li><a href="#">{$manu_item->name  nofilter}</a></li>
							{/foreach}
						</ul>
					</div>
					{/if}

				</div>
			</div>
			{if $use_slider == 1}
				<script type="text/javascript">
					$(document).ready(function() {
						var owl = $(".owlProductCate-{$id}{$cat_info.id_cat|intval}");
						owl.owlCarousel({
							items : 4,
							itemsDesktop : [1199,3],
							itemsDesktopSmall : [991,2],
							itemsTablet: [767,2],
							itemsMobile : [480,1],
							autoHeight : true,
							navigation : true,
							navigationText : ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
							rewindNav : false,
							autoPlay :  false,
							stopOnHover: false,
							pagination : false,
						});
					});
				</script>
			{/if}
		</div>
	{/foreach}
	</div>
</div>
{/if}
{/foreach}
