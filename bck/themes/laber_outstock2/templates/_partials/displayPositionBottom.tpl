{if Hook::exec('displayPosition4')}
<div class="displayPosition displayPosition4">
	<div class="container">
		<div class="row">
			{hook h="displayPosition4"}
		</div>
	</div>
</div>
{/if}
{if Hook::exec('displayPosition5')}
<div class="displayPosition displayPosition5">
	<div class="container">
		<div class="row">
			{hook h="displayPosition5"}
		</div>
	</div>
</div>
{/if}

{if $page.page_name == 'index'}
	{if Hook::exec('displayBlog')}
	<div class="laberBlog">
		<div class="container">
			{hook h="displayBlog"}
		</div>
	</div>
	{/if}
{/if}
{if $page.page_name == 'index'}
	{if Hook::exec('displayManufacture')}
	<div class="laberManufacture">
		<div class="container">
			{hook h="displayManufacture"}
		</div>
	</div>
	{/if}
{/if}