<div id="_desktop_user_info">
  <div class="user-info laber-user-info">
	
    {if $logged}
      <a class="logout"
        href="{$logout_url}"
        rel="nofollow">
       
        {l s='Sign out' d='Shop.Theme.Laberthemes'}
      </a>
      <a class="account"
        href="{$my_account_url}"
        title="{l s='View my customer account' d='Shop.Theme.Laberthemes'}"
        rel="nofollow">
        {$customerName}
      </a>
    {else}

		<a class="Signin" href="{$my_account_url}"
		title="{l s='Log in to your customer account' d='Shop.Theme.Laberthemes'}"
		rel="nofollow">
			{l s='Sign in' d='Shop.Theme.Laberthemes'}
		</a>
		<a class="register" href="{$urls.pages.register}">
			</span> {l s='Register' d='Shop.Theme.Laberthemes'}
		</a>
    {/if}
  </div>
</div>