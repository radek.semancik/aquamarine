<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:35
  from 'C:\Work\pixape\pragaglobal\modules\tea_productcomparison\views\templates\hook\comparison_button.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91f0d7905_92161707',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bc82a94303b9f70003c0707a74987abd733d2a57' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\modules\\tea_productcomparison\\views\\templates\\hook\\comparison_button.tpl',
      1 => 1559309866,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f91f0d7905_92161707 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['comparator_max_item']->value) && $_smarty_tpl->tpl_vars['comparator_max_item']->value) {?>
	<div class="compare">
		<a class="add_to_compare <?php if ($_smarty_tpl->tpl_vars['checked']->value) {?>checked<?php }?>" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['link'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" data-id-product="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8');?>
">
            <i class="show_not_check pe-7s-graph3"></i>
            <i class="show_when_check pe-7s-close"></i>
        </a>
	</div>
<?php }
}
}
