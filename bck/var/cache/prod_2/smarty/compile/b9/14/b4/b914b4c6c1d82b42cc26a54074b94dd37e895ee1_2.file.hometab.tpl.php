<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:36
  from 'C:\Work\pixape\pragaglobal\themes\claue\modules\tea_producttabhome\views\templates\hook\hometab.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f92065b505_57833367',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b914b4c6c1d82b42cc26a54074b94dd37e895ee1' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\modules\\tea_producttabhome\\views\\templates\\hook\\hometab.tpl',
      1 => 1559309873,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f92065b505_57833367 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['featuredproducts']->value) || isset($_smarty_tpl->tpl_vars['newproducts']->value) || isset($_smarty_tpl->tpl_vars['bestsellerproducts']->value) || isset($_smarty_tpl->tpl_vars['specialproducts']->value)) {?>
    <?php $_smarty_tpl->_assignInScope('active', true);?>
    <ul id="product-home-page-tabs" class="clearfix panel-filters">
        <?php if (isset($_smarty_tpl->tpl_vars['bestsellerproducts']->value)) {?>
            <li class="blockbestsellers <?php if ($_smarty_tpl->tpl_vars['active']->value) {?>active<?php }?>">
                <a class="blockbestsellers active" href="#tabblockbestsellers" data-toggle="tab"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Best Sellers','mod'=>'tea_producttabhome'),$_smarty_tpl ) );?>
</a>
            </li>
            <?php $_smarty_tpl->_assignInScope('active', false);?>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['featuredproducts']->value)) {?>
            <li class="homefeatured <?php if ($_smarty_tpl->tpl_vars['active']->value) {?>active<?php }?>">
                <a class="homefeatured active" href="#tabhomefeatured" data-toggle="tab"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Featured','mod'=>'tea_producttabhome'),$_smarty_tpl ) );?>
</a>
            </li>
            <?php $_smarty_tpl->_assignInScope('active', false);?>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['specialproducts']->value)) {?>
            <li class="blockspecials <?php if ($_smarty_tpl->tpl_vars['active']->value) {?>active<?php }?>">
                <a class="blockspecials active" href="#tabblockspecials" data-toggle="tab"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sale','mod'=>'tea_producttabhome'),$_smarty_tpl ) );?>
</a>
            </li>
            <?php $_smarty_tpl->_assignInScope('active', false);?>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['newproducts']->value)) {?>
            <li class="blocknewproduct <?php if ($_smarty_tpl->tpl_vars['active']->value) {?>active<?php }?>">
                <a class="blocknewproduct active" href="#tabblocknewproduct" data-toggle="tab"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'New product','mod'=>'tea_producttabhome'),$_smarty_tpl ) );?>
</a>
            </li>
            <?php $_smarty_tpl->_assignInScope('active', false);?>
        <?php }?>
    </ul>
<?php }?>
<div id="tab-content-products" class="tab-content">
    <?php $_smarty_tpl->_assignInScope('active', true);?>
    <?php if (isset($_smarty_tpl->tpl_vars['bestsellerproducts']->value)) {?>
        <div id="tabblockbestsellers" class="tab-pane <?php if ($_smarty_tpl->tpl_vars['active']->value) {?>active<?php }?>"><?php echo $_smarty_tpl->tpl_vars['bestsellerproducts']->value;?>
</div>
        <?php $_smarty_tpl->_assignInScope('active', false);?>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['featuredproducts']->value)) {?>
        <div id="tabhomefeatured" class="tab-pane <?php if ($_smarty_tpl->tpl_vars['active']->value) {?>active<?php }?>"><?php echo $_smarty_tpl->tpl_vars['featuredproducts']->value;?>
</div>
        <?php $_smarty_tpl->_assignInScope('active', false);?>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['newproducts']->value)) {?>
        <div id="tabblocknewproduct" class="tab-pane <?php if ($_smarty_tpl->tpl_vars['active']->value) {?>active<?php }?>"><?php echo $_smarty_tpl->tpl_vars['newproducts']->value;?>
</div>
        <?php $_smarty_tpl->_assignInScope('active', false);?>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['specialproducts']->value)) {?>
        <div id="tabblockspecials" class="tab-pane <?php if ($_smarty_tpl->tpl_vars['active']->value) {?>active<?php }?>"><?php echo $_smarty_tpl->tpl_vars['specialproducts']->value;?>
</div>
        <?php $_smarty_tpl->_assignInScope('active', false);?> 
    <?php }?>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
$(document).on('click','#product-home-page-tabs li a',function(e){
   $('#product-home-page-tabs li').removeClass('active');
   $(this).parent().addClass('active'); 
   $('#tab-content-products.tab-content .tab-pane').removeClass('active');
   $('#tab-content-products.tab-content '+$(this).attr('href')).addClass('active');
   return false;
});
<?php echo '</script'; ?>
><?php }
}
