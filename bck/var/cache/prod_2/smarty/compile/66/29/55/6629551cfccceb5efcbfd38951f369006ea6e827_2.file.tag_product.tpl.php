<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:31
  from 'C:\Work\pixape\pragaglobal\modules\pg_tagproduct\views\templates\hook\tag_product.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91bc11c59_28416231',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6629551cfccceb5efcbfd38951f369006ea6e827' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\modules\\pg_tagproduct\\views\\templates\\hook\\tag_product.tpl',
      1 => 1559309862,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f91bc11c59_28416231 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['tags']->value) {?>
    <div class="block">
        <h4 class="widget-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product tags','mod'=>'pg_tagproduct'),$_smarty_tpl ) );?>
</h4>
        <div class="content_block block_content">
            <div class="product_tag">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tags']->value, 'tag');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['tag']->value) {
?>
                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tag']->value['link'], ENT_QUOTES, 'UTF-8');?>
" class="tag" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tag']->value['name'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tag']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a>     
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>               
            </div>
        </div>
    </div>
<?php }?>  <?php }
}
