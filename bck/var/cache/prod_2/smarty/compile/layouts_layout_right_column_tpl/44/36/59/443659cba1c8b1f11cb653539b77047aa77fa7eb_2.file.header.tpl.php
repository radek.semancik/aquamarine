<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:31
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\_partials\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91b4d1c32_65639473',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '443659cba1c8b1f11cb653539b77047aa77fa7eb' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\_partials\\header.tpl',
      1 => 1559309874,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f91b4d1c32_65639473 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6154911155d70f91b4cdd11_87036356', 'header_top');
?>

<?php }
/* {block 'header_top'} */
class Block_6154911155d70f91b4cdd11_87036356 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header_top' => 
  array (
    0 => 'Block_6154911155d70f91b4cdd11_87036356',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayTop'),$_smarty_tpl ) );?>


  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNavFullWidth'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'header_top'} */
}
