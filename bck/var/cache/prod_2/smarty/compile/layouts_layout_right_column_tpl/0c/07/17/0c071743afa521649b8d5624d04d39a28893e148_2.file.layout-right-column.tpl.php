<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:31
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\layouts\layout-right-column.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91b2c7e57_44165592',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0c071743afa521649b8d5624d04d39a28893e148' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\layouts\\layout-right-column.tpl',
      1 => 1559309874,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f91b2c7e57_44165592 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12024682545d70f91b2afc02_27383028', 'left_column');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11942151625d70f91b2b0f06_01833196', 'content_wrapper');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'layouts/layout-both-columns.tpl');
}
/* {block 'left_column'} */
class Block_12024682545d70f91b2afc02_27383028 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_12024682545d70f91b2afc02_27383028',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'left_column'} */
/* {block 'content'} */
class Block_287943725d70f91b2b46a9_38566208 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                  <p>Hello world! This is HTML5 Boilerplate.</p>
                <?php
}
}
/* {/block 'content'} */
/* {block 'content'} */
class Block_8936714825d70f91b2bcc01_96493468 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                          <p>Hello world! This is HTML5 Boilerplate.</p>
                        <?php
}
}
/* {/block 'content'} */
/* {block 'content'} */
class Block_13177290475d70f91b2bf8a8_08214951 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                          <p>Hello world! This is HTML5 Boilerplate.</p>
                        <?php
}
}
/* {/block 'content'} */
/* {block 'content'} */
class Block_6339691335d70f91b2c3e68_51201506 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

              <p>Hello world! This is HTML5 Boilerplate.</p>
            <?php
}
}
/* {/block 'content'} */
/* {block 'content_wrapper'} */
class Block_11942151625d70f91b2b0f06_01833196 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_11942151625d70f91b2b0f06_01833196',
  ),
  'content' => 
  array (
    0 => 'Block_287943725d70f91b2b46a9_38566208',
    1 => 'Block_8936714825d70f91b2bcc01_96493468',
    2 => 'Block_13177290475d70f91b2bf8a8_08214951',
    3 => 'Block_6339691335d70f91b2c3e68_51201506',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
          <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_right') {?>
              <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_287943725d70f91b2b46a9_38566208', 'content', $this->tplIndex);
?>

              </div>
          <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?>
                    <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8936714825d70f91b2bcc01_96493468', 'content', $this->tplIndex);
?>

                    </div>
                <?php } else { ?>
                    <div id="content-wrapper" class="full-width sang col-xs-12 col-sm-12 col-md-12">
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13177290475d70f91b2bf8a8_08214951', 'content', $this->tplIndex);
?>

                      </div>
                <?php }?>
                
          <?php }?>
    <?php } else { ?>
        <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6339691335d70f91b2c3e68_51201506', 'content', $this->tplIndex);
?>

          </div>
    <?php }
}
}
/* {/block 'content_wrapper'} */
}
