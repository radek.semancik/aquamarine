<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:31
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\catalog\listing\product-list.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91b27dd62_18729645',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1b58e0c8cd7522974a2602234e93d75a05f07133' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\catalog\\listing\\product-list.tpl',
      1 => 1563180660,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/products-top.tpl' => 5,
    'file:catalog/_partials/products.tpl' => 5,
    'file:catalog/_partials/products-bottom.tpl' => 5,
    'file:errors/not-found.tpl' => 5,
  ),
),false)) {
function content_5d70f91b27dd62_18729645 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13748067305d70f91b1ec563_80687851', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'product_list_top'} */
class Block_19340329425d70f91b1f5091_76401514 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, false);
?>
                      <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_10382013945d70f91b216d93_01764865 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                      <div class="hidden-sm-down">
                        <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

                      </div>
                    <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_12302554455d70f91b21bb79_23221301 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, false);
?>
                      <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_74269045d70f91b2202a7_26767300 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, false);
?>
                      <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'product_list_top'} */
class Block_13036189525d70f91b22e140_54914239 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_20576403425d70f91b2304d3_69244131 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                      <div class="hidden-sm-down">
                        <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

                      </div>
                    <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_12045398615d70f91b232778_88228591 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_1852377435d70f91b2346f4_82516287 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'product_list_top'} */
class Block_13891644075d70f91b239446_48221435 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_13154099755d70f91b23b401_31179152 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                      <div class="hidden-sm-down">
                        <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

                      </div>
                    <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_18437779885d70f91b23d534_96752224 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_9767108035d70f91b23f438_39397024 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'product_list_top'} */
class Block_526157035d70f91b257a30_25551005 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_20859846995d70f91b259c71_23793542 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                  <div class="hidden-sm-down">
                    <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

                  </div>
                <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_4408677935d70f91b25c187_27588793 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_15041811155d70f91b25f0e4_81016618 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'product_list_top'} */
class Block_2977398435d70f91b2760b1_67510278 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_8004070345d70f91b2779e1_60540172 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                  <div class="hidden-sm-down">
                    <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

                  </div>
                <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_1100717835d70f91b2795d8_03283288 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_8603345055d70f91b27abc8_34656332 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'content'} */
class Block_13748067305d70f91b1ec563_80687851 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_13748067305d70f91b1ec563_80687851',
  ),
  'product_list_top' => 
  array (
    0 => 'Block_19340329425d70f91b1f5091_76401514',
    1 => 'Block_13036189525d70f91b22e140_54914239',
    2 => 'Block_13891644075d70f91b239446_48221435',
    3 => 'Block_526157035d70f91b257a30_25551005',
    4 => 'Block_2977398435d70f91b2760b1_67510278',
  ),
  'product_list_active_filters' => 
  array (
    0 => 'Block_10382013945d70f91b216d93_01764865',
    1 => 'Block_20576403425d70f91b2304d3_69244131',
    2 => 'Block_13154099755d70f91b23b401_31179152',
    3 => 'Block_20859846995d70f91b259c71_23793542',
    4 => 'Block_8004070345d70f91b2779e1_60540172',
  ),
  'product_list' => 
  array (
    0 => 'Block_12302554455d70f91b21bb79_23221301',
    1 => 'Block_12045398615d70f91b232778_88228591',
    2 => 'Block_18437779885d70f91b23d534_96752224',
    3 => 'Block_4408677935d70f91b25c187_27588793',
    4 => 'Block_1100717835d70f91b2795d8_03283288',
  ),
  'product_list_bottom' => 
  array (
    0 => 'Block_74269045d70f91b2202a7_26767300',
    1 => 'Block_1852377435d70f91b2346f4_82516287',
    2 => 'Block_9767108035d70f91b23f438_39397024',
    3 => 'Block_15041811155d70f91b25f0e4_81016618',
    4 => 'Block_8603345055d70f91b27abc8_34656332',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div id="main">
       
    <?php if ((isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value)) {?>         
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
            <?php if (isset($_smarty_tpl->tpl_vars['category']->value['id']) && $_smarty_tpl->tpl_vars['category']->value['id'] == 3) {?>
                <div id="products" class="product_list_metro pg_list_masonry">
                    <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>
                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19340329425d70f91b1f5091_76401514', 'product_list_top', $this->tplIndex);
?>

                    </div>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10382013945d70f91b216d93_01764865', 'product_list_active_filters', $this->tplIndex);
?>

                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12302554455d70f91b21bb79_23221301', 'product_list', $this->tplIndex);
?>

                    </div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_74269045d70f91b2202a7_26767300', 'product_list_bottom', $this->tplIndex);
?>

                  <?php } else { ?>
                    <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                  <?php }?>
                </div>
            <?php } elseif (isset($_smarty_tpl->tpl_vars['category']->value['id']) && $_smarty_tpl->tpl_vars['category']->value['id'] == 4) {?>
                <div id="products" class="pg_list_masonry product_list_masonry">
                <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>
                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13036189525d70f91b22e140_54914239', 'product_list_top', $this->tplIndex);
?>

                    </div>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20576403425d70f91b2304d3_69244131', 'product_list_active_filters', $this->tplIndex);
?>

                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12045398615d70f91b232778_88228591', 'product_list', $this->tplIndex);
?>

                    </div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1852377435d70f91b2346f4_82516287', 'product_list_bottom', $this->tplIndex);
?>

                  <?php } else { ?>
                    <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
                  <?php }?>
                </div>
            <?php } else { ?>
                <div id="products" class="">
                <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>
                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13891644075d70f91b239446_48221435', 'product_list_top', $this->tplIndex);
?>

                    </div>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13154099755d70f91b23b401_31179152', 'product_list_active_filters', $this->tplIndex);
?>

                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18437779885d70f91b23d534_96752224', 'product_list', $this->tplIndex);
?>

                    </div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9767108035d70f91b23f438_39397024', 'product_list_bottom', $this->tplIndex);
?>

                  <?php } else { ?>
                    <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
                  <?php }?>
                </div>
            <?php }?>
        <?php } else { ?>             
            <div id="products" class="<?php if (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_metro') {?>product_list_metro pg_list_masonry<?php } elseif (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_masonry') {?> pg_list_masonry product_list_masonry<?php } else { ?> product_list_default<?php }?>">
            <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>
                <div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_526157035d70f91b257a30_25551005', 'product_list_top', $this->tplIndex);
?>

                </div>               
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20859846995d70f91b259c71_23793542', 'product_list_active_filters', $this->tplIndex);
?>

                <div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4408677935d70f91b25c187_27588793', 'product_list', $this->tplIndex);
?>

                </div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15041811155d70f91b25f0e4_81016618', 'product_list_bottom', $this->tplIndex);
?>

              <?php } else { ?>
                <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
              <?php }?>
            </div>
        <?php }?>
    <?php } else { ?>       
        <div id="products" class="<?php if (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_metro') {?>product_list_metro pg_list_masonry<?php } elseif (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_masonry') {?> pg_list_masonry product_list_masonry<?php } else { ?> product_list_default<?php }?>">
            <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>
                <div>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayCustomCategory'),$_smarty_tpl ) );?>

                    <div class="clear"></div>
                </div>
                <div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2977398435d70f91b2760b1_67510278', 'product_list_top', $this->tplIndex);
?>

                </div>                
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8004070345d70f91b2779e1_60540172', 'product_list_active_filters', $this->tplIndex);
?>

                <div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1100717835d70f91b2795d8_03283288', 'product_list', $this->tplIndex);
?>

                </div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8603345055d70f91b27abc8_34656332', 'product_list_bottom', $this->tplIndex);
?>

              <?php } else { ?>
                <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
              <?php }?>
            </div>
        <?php }?>
  </div>
<?php
}
}
/* {/block 'content'} */
}
