<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:31
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\layouts\layout-both-columns.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91b37cc54_70394936',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c8be8840b0ff827daedfb9c55f72c056e8c1128e' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\layouts\\layout-both-columns.tpl',
      1 => 1566916292,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/head.tpl' => 1,
    'file:_partials/javascript.tpl' => 1,
    'file:catalog/_partials/product-activation.tpl' => 1,
    'file:_partials/header.tpl' => 1,
    'file:_partials/notifications.tpl' => 1,
    'file:_partials/breadcrumb.tpl' => 1,
    'file:_partials/footer.tpl' => 1,
  ),
),false)) {
function content_5d70f91b37cc54_70394936 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

  <head>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_217287275d70f91b2ec725_56560868', 'head');
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9535127965d70f91b2ee6e7_29070461', 'javascript_bottom');
?>

  </head>

  <body id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
" class="sang <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'classnames' ][ 0 ], array( $_smarty_tpl->tpl_vars['page']->value['body_classes'] )), ENT_QUOTES, 'UTF-8');
if (isset($_smarty_tpl->tpl_vars['order_mini_cart']->value) && $_smarty_tpl->tpl_vars['order_mini_cart']->value == 'minicart') {?> pg_minicart<?php }
if ((isset($_smarty_tpl->tpl_vars['product_zoom_image']->value) && $_smarty_tpl->tpl_vars['product_zoom_image']->value)) {?> img_zoom_whenhover<?php }?>">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21394287355d70f91b2f6ef6_35258913', 'hook_after_body_opening_tag');
?>


    <main>
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8680361335d70f91b2f8c21_30019212', 'product_activation');
?>



        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11048471005d70f91b2fa8c3_25525399', 'header');
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15741943955d70f91b2fc438_06386871', 'notifications');
?>


      <div id="wrapper">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19796320165d70f91b2fdfc5_63768068', 'breadcrumb');
?>

        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'index') {?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product')) {?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['product']->value['id'] == '3') {?>
                    <div class="page_full_size col-xs-12">
                        <div class="row">
                <?php } else { ?>
            
                    <?php if (isset($_smarty_tpl->tpl_vars['product_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value == 1) {?>
                        <div class="page_full_size col-xs-12">
                            <div class="row">
                    <?php } else { ?>
                        <div class="container">
                        <div class="row">
                    <?php }?>
                <?php }?>
            <?php }?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category')) {?>                
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '6') {?>                   
                    <div class="page_full_size col-xs-12">
                        <div class="row">
                <?php } else { ?>
                    <?php if (isset($_smarty_tpl->tpl_vars['category_full_size']->value) && $_smarty_tpl->tpl_vars['category_full_size']->value != 1) {?>
                        <div class="container">
                        <div class="row">
                    <?php } else { ?>
                        <div class="page_full_size col-xs-12">
                            <div class="row">
                    <?php }?>
                <?php }?>
            <?php }?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'product' && $_smarty_tpl->tpl_vars['page']->value['page_name'] != 'category')) {?>
                <div class="container">
                    <div class="row">
            <?php }?>
        <?php }?>
        
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>            
            <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_left') {?>
                  <div id="left-column" class="col-xs-12 col-sm-4 col-md-3" <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?> style="display: none!important"<?php }?>>                     
                      <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                              
                           <?php }?>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                        </div>
              <?php } else { ?>
                    <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '8') {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13200334615d70f91b322107_35924639', "left_column");
?>

                    <?php }?>
              <?php }?>
        <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9200563805d70f91b328dc2_25508825', "left_column");
?>

        <?php }?>
        
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
          <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_left') {?>
               <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4494845905d70f91b33b674_86421739', "content");
?>
                   
                </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_right') {?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15559174035d70f91b340721_76606494', "content_wrapper");
?>

          <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11998200045d70f91b352b41_42095571', "content_wrapper");
?>

                <?php } elseif (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '8') {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4839530225d70f91b359418_19795956', "content_wrapper");
?>

                <?php } else { ?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15675736325d70f91b35c593_76306722', "content_wrapper");
?>

                <?php }?>
          <?php }?>
        <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10444186735d70f91b35fca6_95410418', "content_wrapper");
?>

        <?php }?>
          
          <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
            <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_right') {?>
              <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2754940715d70f91b364b87_54870547', "right_column");
?>

            <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?>
                    <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                    </div>
                <?php }?>
            <?php }?>
          <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1056335475d70f91b36b775_72241389', "right_column");
?>

          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'index') {?>
            
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product' && isset($_smarty_tpl->tpl_vars['product_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value != 1) && ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category' && isset($_smarty_tpl->tpl_vars['category_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value != 1)) {?>
                </div>
                </div>
            <?php } else { ?>
                </div>
                </div>
            <?php }?>
            
            
          <?php }?>
      </div>

      <footer id="footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1971223035d70f91b378178_68962651', "footer");
?>

      </footer>

    </main>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2020313865d70f91b37a2a0_14042498', 'hook_before_body_closing_tag');
?>

  </body>

</html>
<?php }
/* {block 'head'} */
class Block_217287275d70f91b2ec725_56560868 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_217287275d70f91b2ec725_56560868',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender('file:_partials/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php
}
}
/* {/block 'head'} */
/* {block 'javascript_bottom'} */
class Block_9535127965d70f91b2ee6e7_29070461 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'javascript_bottom' => 
  array (
    0 => 'Block_9535127965d70f91b2ee6e7_29070461',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender("file:_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['bottom']), 0, false);
?>
    <?php
}
}
/* {/block 'javascript_bottom'} */
/* {block 'hook_after_body_opening_tag'} */
class Block_21394287355d70f91b2f6ef6_35258913 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_after_body_opening_tag' => 
  array (
    0 => 'Block_21394287355d70f91b2f6ef6_35258913',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterBodyOpeningTag'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_after_body_opening_tag'} */
/* {block 'product_activation'} */
class Block_8680361335d70f91b2f8c21_30019212 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_activation' => 
  array (
    0 => 'Block_8680361335d70f91b2f8c21_30019212',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-activation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'product_activation'} */
/* {block 'header'} */
class Block_11048471005d70f91b2fa8c3_25525399 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_11048471005d70f91b2fa8c3_25525399',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender('file:_partials/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'header'} */
/* {block 'notifications'} */
class Block_15741943955d70f91b2fc438_06386871 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'notifications' => 
  array (
    0 => 'Block_15741943955d70f91b2fc438_06386871',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'notifications'} */
/* {block 'breadcrumb'} */
class Block_19796320165d70f91b2fdfc5_63768068 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'breadcrumb' => 
  array (
    0 => 'Block_19796320165d70f91b2fdfc5_63768068',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:_partials/breadcrumb.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
          <?php
}
}
/* {/block 'breadcrumb'} */
/* {block "left_column"} */
class Block_13200334615d70f91b322107_35924639 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_13200334615d70f91b322107_35924639',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                              <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                                  <div class="left_column_filter filter-trigger">
                            		<i class="fa fa-sliders p_r active"></i>
                            	  </div>
                               <?php }?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                            </div>
                          <?php
}
}
/* {/block "left_column"} */
/* {block "left_column"} */
class Block_9200563805d70f91b328dc2_25508825 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_9200563805d70f91b328dc2_25508825',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                      <div class="left_column_filter filter-trigger">
                		<i class="fa fa-sliders p_r active"></i>
                	  </div>
                   <?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayLeftColumnProduct'),$_smarty_tpl ) );?>

                  <?php } else { ?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                  <?php }?>
                </div>
              <?php
}
}
/* {/block "left_column"} */
/* {block "content"} */
class Block_4494845905d70f91b33b674_86421739 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_4494845905d70f91b33b674_86421739',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <p>Hello world! This is HTML5 Boilerplate.</p>
                  <?php
}
}
/* {/block "content"} */
/* {block "content"} */
class Block_10794487965d70f91b3428c5_19080362 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <p>Hello world! This is HTML5 Boilerplate.</p>
                      <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_15559174035d70f91b340721_76606494 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_15559174035d70f91b340721_76606494',
  ),
  'content' => 
  array (
    0 => 'Block_10794487965d70f91b3428c5_19080362',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10794487965d70f91b3428c5_19080362', "content", $this->tplIndex);
?>

                    </div>
                  <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_10797916065d70f91b3547e5_16525957 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_11998200045d70f91b352b41_42095571 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_11998200045d70f91b352b41_42095571',
  ),
  'content' => 
  array (
    0 => 'Block_10797916065d70f91b3547e5_16525957',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10797916065d70f91b3547e5_16525957', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_15526078325d70f91b35a3b9_41782553 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_4839530225d70f91b359418_19795956 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_4839530225d70f91b359418_19795956',
  ),
  'content' => 
  array (
    0 => 'Block_15526078325d70f91b35a3b9_41782553',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15526078325d70f91b35a3b9_41782553', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_2485099175d70f91b35d455_63912905 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_15675736325d70f91b35c593_76306722 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_15675736325d70f91b35c593_76306722',
  ),
  'content' => 
  array (
    0 => 'Block_2485099175d70f91b35d455_63912905',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="full-width col-xs-12 col-sm-12 col-md-12">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2485099175d70f91b35d455_63912905', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_12154710435d70f91b360b19_48856588 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <p>Hello world! This is HTML5 Boilerplate.</p>
                  <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_10444186735d70f91b35fca6_95410418 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_10444186735d70f91b35fca6_95410418',
  ),
  'content' => 
  array (
    0 => 'Block_12154710435d70f91b360b19_48856588',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="content-wrapper" class="left-column right-column col-sm-4 col-md-6">
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12154710435d70f91b360b19_48856588', "content", $this->tplIndex);
?>

                </div>
              <?php
}
}
/* {/block "content_wrapper"} */
/* {block "right_column"} */
class Block_2754940715d70f91b364b87_54870547 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'right_column' => 
  array (
    0 => 'Block_2754940715d70f91b364b87_54870547',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                </div>
              <?php
}
}
/* {/block "right_column"} */
/* {block "right_column"} */
class Block_1056335475d70f91b36b775_72241389 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'right_column' => 
  array (
    0 => 'Block_1056335475d70f91b36b775_72241389',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayRightColumnProduct'),$_smarty_tpl ) );?>

                  <?php } else { ?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayColumRightBlog'),$_smarty_tpl ) );?>

                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                  <?php }?>
                </div>
              <?php
}
}
/* {/block "right_column"} */
/* {block "footer"} */
class Block_1971223035d70f91b378178_68962651 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_1971223035d70f91b378178_68962651',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender("file:_partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block "footer"} */
/* {block 'hook_before_body_closing_tag'} */
class Block_2020313865d70f91b37a2a0_14042498 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_before_body_closing_tag' => 
  array (
    0 => 'Block_2020313865d70f91b37a2a0_14042498',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayBeforeBodyClosingTag'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_before_body_closing_tag'} */
}
