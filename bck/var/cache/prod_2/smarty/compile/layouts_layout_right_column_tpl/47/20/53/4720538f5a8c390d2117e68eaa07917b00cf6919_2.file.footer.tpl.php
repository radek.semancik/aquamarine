<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:31
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\_partials\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91bcd2bb0_06547023',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4720538f5a8c390d2117e68eaa07917b00cf6919' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\_partials\\footer.tpl',
      1 => 1559309874,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f91bcd2bb0_06547023 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4791232065d70f91bca7ce5_86909652', 'hook_footer_before');
?>

<div class="footer-container">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8767075305d70f91bcaa431_34981192', 'hook_footer');
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18642776945d70f91bcac8b9_35537786', 'hook_footer_after');
?>

    <a id="jas-backtop" class="pf br__50" style="display: block;">
        <span class="tc bgp br__50 db cw">
        <i class="pr pe-7s-angle-up"></i>
        </span>
    </a>
</div>
<!-- html at footer.tpl -->
<div class="form_alert_login">
    <div class="form_alert_login_content">
        <span class="close_form_alert"></span>
        <h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You must be logged in to manage your wishlist','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</h4>
        <div class="wishlist_action">
            <a class="continue_shop" href="#">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Continue Shopping','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

            </a>
            <a class="login_shop" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Login','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

            </a>
        </div>
    </div>
</div>
<div class="form_alert_compare">
    <div class="form_alert_compare_content">
        <span class="close_form_compare"></span>
        <h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add compare successful','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</h4>
        <div class="wishlist_action">
            <a class="continue_shop" href="#">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Continue Shopping','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

            </a>
            <a class="login_shop" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tea_productcomparison'),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Compare','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

            </a>
        </div>
    </div>
</div>
<div class="form_alert_compare_del">
    <div class="form_alert_compare_content">
        <span class="close_form_compare"></span>
        <h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Delete compare successful','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</h4>
            </div>
</div>
<div class="form_alert_wl_success">
    <div class="form_alert_login_content">
        <span class="close_form_alert"></span>
        <h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product added','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</h4>
    </div>
</div>
<?php if ((isset($_smarty_tpl->tpl_vars['pg_please_waiting']->value) && $_smarty_tpl->tpl_vars['pg_please_waiting']->value)) {?>
    <?php if ((isset($_smarty_tpl->tpl_vars['pg_pleasewait_onlyshowhome']->value) && $_smarty_tpl->tpl_vars['pg_pleasewait_onlyshowhome']->value == 1)) {?>
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'index') {?>
            <div class="pg_loading_page active">
                <div class="pg_loadding_page_content">
                    <div class="cssload-container">
                    	<div class="loader"></div>
                        <?php if (isset($_smarty_tpl->tpl_vars['pg_text_page_loading']->value) && $_smarty_tpl->tpl_vars['pg_text_page_loading']->value) {?>
                            <div class="pg_please_wait_des">
                                <?php echo $_smarty_tpl->tpl_vars['pg_text_page_loading']->value;?>

                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        <?php }?>
    <?php } else { ?>
        <div class="pg_loading_page active">
                <div class="pg_loadding_page_content">
                    <div class="cssload-container">
                    	<div class="cssload-progress cssload-float cssload-shadow">
                    		<div class="cssload-progress-item"></div>
                    	</div>
                        <?php if (isset($_smarty_tpl->tpl_vars['pg_text_page_loading']->value) && $_smarty_tpl->tpl_vars['pg_text_page_loading']->value) {?>
                            <div class="pg_please_wait_des">
                                <?php echo $_smarty_tpl->tpl_vars['pg_text_page_loading']->value;?>

                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
    <?php }
}
}
/* {block 'hook_footer_before'} */
class Block_4791232065d70f91bca7ce5_86909652 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_before' => 
  array (
    0 => 'Block_4791232065d70f91bca7ce5_86909652',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterBefore'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_footer_before'} */
/* {block 'hook_footer'} */
class Block_8767075305d70f91bcaa431_34981192 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer' => 
  array (
    0 => 'Block_8767075305d70f91bcaa431_34981192',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooter'),$_smarty_tpl ) );?>

      <?php
}
}
/* {/block 'hook_footer'} */
/* {block 'hook_footer_after'} */
class Block_18642776945d70f91bcac8b9_35537786 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_after' => 
  array (
    0 => 'Block_18642776945d70f91bcac8b9_35537786',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterAfter'),$_smarty_tpl ) );?>

      <?php
}
}
/* {/block 'hook_footer_after'} */
}
