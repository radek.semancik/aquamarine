<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:37
  from 'C:\Work\pixape\pragaglobal\themes\claue\modules\tea_newsletter\views\templates\hook\popup.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f92126bb33_30571793',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '783e747e427354bc1130c8db8e639828c5f3bfba' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\modules\\tea_newsletter\\views\\templates\\hook\\popup.tpl',
      1 => 1559309873,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f92126bb33_30571793 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">

var time_delay_popup =<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['TEA_NEWSLETTER_DELAY_POPUP']->value, ENT_QUOTES, 'UTF-8');?>
;

<?php echo '</script'; ?>
>

<div id="newsletter-modal" class="tea-newsletter-popup<?php if ($_smarty_tpl->tpl_vars['TEA_NEWSLETTER_MOBILE_HIDE']->value) {?> tea-mobile-hide<?php }?>">
    <div class="tea-div-l1">
        <div class="tea-div-l2">
            <div class="tea-div-l3 modal-dialog modal-newsletter">
                <div class="modal-content">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="newsletter-media newsletter-column" <?php if ($_smarty_tpl->tpl_vars['TEA_NEWSLETTER_IMAGE']->value) {?>style="background-image: url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['TEA_NEWSLETTER_IMAGE']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
                                <div class="newsletter-group">
                                    <?php echo $_smarty_tpl->tpl_vars['TEA_NEWSLETTER_DES_LEFT']->value;?>

                                </div>
                                <button id="newsletter-close" class="newsletter-close tea-close button"><i class="pe-7s-close"></i></button>
                            </div>
                            <div class="newsletter-signup newsletter-column">
                                <div class="newsletter-heading">
                                  <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['TEA_NEWSLETTER_TITLE']->value, ENT_QUOTES, 'UTF-8');?>

                                </div>
                                <div class="newsletter-content">
                                    <?php echo $_smarty_tpl->tpl_vars['TEA_NEWSLETTER_DESCRIPTION']->value;?>

                                    <form class="tea-form frm-email" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['TEA_NEWSLETTER_ACTION']->value, ENT_QUOTES, 'UTF-8');?>
" method="post">
                                        <div class="tea-loading-div">
                                            <img class="tea-loading" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['TEA_NEWSLETTER_LOADING_IMG']->value, ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Loading...','mod'=>'tea_newsletter'),$_smarty_tpl ) );?>
" />
                                        </div>
                                        <div class="tea-input-row">
                                            <label for="tea-email-input"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email: ','mod'=>'tea_newsletter'),$_smarty_tpl ) );?>
</label>
                                            <input type="text" id="tea-email-input" value="" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enter your email...','mod'=>'tea_newsletter'),$_smarty_tpl ) );?>
" />
                                            <input class="button" type="submit" name="ynpSubmit" id="tea-submit" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Subcribe','mod'=>'tea_newsletter'),$_smarty_tpl ) );?>
" />
                                        </div>
                                        <div class="tea-alert">
                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your Information will never be shared with any third party.','mod'=>'tea_newsletter'),$_smarty_tpl ) );?>

                                        </div>
                                        <?php if (!$_smarty_tpl->tpl_vars['TEA_NEWSLETTER_AUTO_HIDE']->value) {?>
                                            <div class="tea-input-checkbox">
                                                <input type="checkbox" id="tea-input-dont-show" name="ynpcheckbox" />
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Do not show this again','mod'=>'tea_newsletter'),$_smarty_tpl ) );?>

                                            </div>
                                        <?php }?>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
