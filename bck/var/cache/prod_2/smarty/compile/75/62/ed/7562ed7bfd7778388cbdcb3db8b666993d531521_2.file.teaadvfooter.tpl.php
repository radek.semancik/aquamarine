<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:32
  from 'C:\Work\pixape\pragaglobal\themes\claue\modules\tea_themelayout\views\templates\hook\teaadvfooter.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91c0f1c87_42813452',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7562ed7bfd7778388cbdcb3db8b666993d531521' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\modules\\tea_themelayout\\views\\templates\\hook\\teaadvfooter.tpl',
      1 => 1559309873,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f91c0f1c87_42813452 (Smarty_Internal_Template $_smarty_tpl) {
?><div <?php if ($_smarty_tpl->tpl_vars['class_footer']->value) {?>class="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['class_footer']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows_footer']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
    	<div <?php if ($_smarty_tpl->tpl_vars['row']->value['class']) {?>class="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['row']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"<?php }?> style="<?php if ($_smarty_tpl->tpl_vars['row']->value['padding']) {?>padding:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['padding'], ENT_QUOTES, 'UTF-8');?>
;<?php }?> <?php if ($_smarty_tpl->tpl_vars['row']->value['margin']) {?> margin:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['margin'], ENT_QUOTES, 'UTF-8');?>
;<?php }?>">
    		<div <?php if ($_smarty_tpl->tpl_vars['row']->value['fullwidth'] == 0) {?>class="container"<?php }?>>
    			<div class="footer-row row">
    			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['positions'], 'position');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['position']->value) {
?>
    				<div class="footer-position col-lg-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['position']->value['col_lg'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 col-sm-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['position']->value['col_sm'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 col-md-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['position']->value['col_md'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 col-xs-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['position']->value['col_xs'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['position']->value['class_suffix'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
    					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['position']->value['blocks'], 'block');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['block']->value) {
?>
    						<?php if ($_smarty_tpl->tpl_vars['block']->value['show_title']) {?><h4 class="title_block"><span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['block']->value['title'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span></h4><?php }?>
							<div class="block-content">
							     <?php if (isset($_smarty_tpl->tpl_vars['block']->value['return_value'])) {
echo $_smarty_tpl->tpl_vars['block']->value['return_value'];
}?>
							</div>
    					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    				</div>
    			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    			</div>
    		</div>
    	</div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>
<?php if ($_smarty_tpl->tpl_vars['phover']->value == 'image_swap') {
echo '<script'; ?>
 type="text/javascript">	
    $(document).ready(function(){	
		var TeaAjax = new $.TeaAjaxFunc();
        TeaAjax.processAjax();		
    });
<?php echo '</script'; ?>
>
<?php }
}
}
