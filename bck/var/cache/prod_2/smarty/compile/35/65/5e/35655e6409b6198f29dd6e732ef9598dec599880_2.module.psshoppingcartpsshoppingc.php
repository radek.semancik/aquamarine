<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:31
  from 'module:psshoppingcartpsshoppingc' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91b8f3117_54099495',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '35655e6409b6198f29dd6e732ef9598dec599880' => 
    array (
      0 => 'module:psshoppingcartpsshoppingc',
      1 => 1559309872,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:ps_shoppingcart/ps_shoppingcart-product-line.tpl' => 1,
  ),
),false)) {
function content_5d70f91b8f3117_54099495 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="desktop_cart" class="claue-action">
    <?php if (isset($_smarty_tpl->tpl_vars['order_mini_cart']->value) && $_smarty_tpl->tpl_vars['order_mini_cart']->value == 'minicart') {?><div class="background-overlay"></div><?php }?>
  <div class="blockcart cart-preview <?php if ($_smarty_tpl->tpl_vars['cart']->value['products_count'] > 0) {?>active<?php } else { ?>inactive<?php }?>" data-refresh-url="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['refresh_url']->value, ENT_QUOTES, 'UTF-8');?>
">
    <div class="header">
        <a class="cart-contents" rel="nofollow" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart_url']->value, ENT_QUOTES, 'UTF-8');?>
">
            <i class="pe-7s-shopbag"></i>
            <span class="cart-products-count pa count"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['products_count'], ENT_QUOTES, 'UTF-8');?>
</span>
        </a>
        <div class="body cart-wishlist-content">
              <?php if (isset($_smarty_tpl->tpl_vars['order_mini_cart']->value) && $_smarty_tpl->tpl_vars['order_mini_cart']->value == 'minicart') {?>
                    <h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Mini Cart','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</h3>
                    <a id="close-cart" href="javascript:void(0);"></a>
              <?php }?>
              <ul>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cart']->value['products'], 'product');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>
                  <li class="cart-wishlist-item"><?php $_smarty_tpl->_subTemplateRender('module:ps_shoppingcart/ps_shoppingcart-product-line.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?></li>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              </ul>
              <div class="cart-subtotals">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cart']->value['subtotals'], 'subtotal');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subtotal']->value) {
?>
                  <div class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['type'], ENT_QUOTES, 'UTF-8');?>
">
                    <span class="label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['label'], ENT_QUOTES, 'UTF-8');?>
</span>
                    <span class="value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['value'], ENT_QUOTES, 'UTF-8');?>
</span>
                  </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              </div>
              <div class="cart-total">
                <span class="label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['totals']['total']['label'], ENT_QUOTES, 'UTF-8');?>
</span>
                <span class="value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['totals']['total']['value'], ENT_QUOTES, 'UTF-8');?>
</span>
              </div>
              <div class="cart-wishlist-action">
                  <a class="cart-wishlist-viewcart" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart_url']->value, ENT_QUOTES, 'UTF-8');?>
">view cart</a>
                  <a class="cart-wishlist-checkout" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['order'], ENT_QUOTES, 'UTF-8');?>
">check out</a>
                </div>
        </div>
    </div>
  </div>
</div>
<?php if (isset($_smarty_tpl->tpl_vars['order_mini_cart']->value) && $_smarty_tpl->tpl_vars['order_mini_cart']->value == 'minicart') {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        var order_mini_cart = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_mini_cart']->value, ENT_QUOTES, 'UTF-8');?>
';
    <?php echo '</script'; ?>
>
<?php }
}
}
