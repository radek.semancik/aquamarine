<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:31
  from 'C:\Work\pixape\pragaglobal\themes\claue\modules\tea_megamenu\views\templates\hook\teamegamenu-top.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91b6d5aa5_93449151',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '88b17087d9a1b950611c9f666054757b78cb0765' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\modules\\tea_megamenu\\views\\templates\\hook\\teamegamenu-top.tpl',
      1 => 1559309873,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f91b6d5aa5_93449151 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="tea_megamenu_top" class="teamegamenu">
	<nav class="navbar-default">
        <button class="navbar-brand">
		</button>
        <div class="overlay_menu"></div>
		<div id="tea-megamenu-top" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['teamegamenu_style']->value, ENT_QUOTES, 'UTF-8');?>
 tea-megamenu-top tea-megamenu clearfix">
            <h3 class="title_menu"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Menu','mod'=>'tea_megamenu'),$_smarty_tpl ) );?>
<span class="remove-megamenu"><i class="close-menu pe-7s-close"></i></span></h3>
			<div id="content_nav_mobile" class="">
                <div class="currency_language flex-center">
                    <div id="_tablet_currency_selector" class="language"></div>
                    <div id="_tablet_language_selector" class="language"></div>
                </div>
                <div class="header_search flex-center">
                    <div id="_mobile_searchbar" class="claue-action"></div>
                    <div id="_mobile_contact_link"></div>
                    <div id="_mobile_user_info"></div>
                    <div id="_mobile_wishlisttop" class="claue-action"></div>
                </div>
            </div>
            <?php if ($_smarty_tpl->tpl_vars['teamegamenu']->value != '') {?>
                <?php echo $_smarty_tpl->tpl_vars['teamegamenu']->value;?>

            <?php }?>
            <div id="_tablet_social_selector">
            </div>
		</div>
	</nav>
</div>


<?php }
}
