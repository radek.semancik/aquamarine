<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:32
  from 'module:psemailsubscriptionviewst' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91c043d87_27484750',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '307dc6bd4724d29d1572cc301dd7148e962604ef' => 
    array (
      0 => 'module:psemailsubscriptionviewst',
      1 => 1559309871,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f91c043d87_27484750 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="block_newsletter widget-text widget_nav_menu pd_l pd_r">
    <h3 class="widget-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Newsletter Sign Up ','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</h3>
    <div class="text-widget">
        <?php if ($_smarty_tpl->tpl_vars['conditions']->value) {?>
            <p class="fs__14"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['conditions']->value, ENT_QUOTES, 'UTF-8');?>
</p>
        <?php }?>
      <form id="mc4wp-form-1" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
#footer" method="post">
          <div class="signup-newsletter-form dib mb__15">
            <input class="input-text" name="email" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your email address','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
">
            <input class="btn btn-primary pull-xs-right submit-btn" name="submitNewsletter" type="submit" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Subscribe','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
" >
            <input class="btn btn-primary pull-xs-right hidden-xs-down hidden-sm-up submit-btn" name="submitNewsletter" type="submit" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'OK','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
" >
            
            <input type="hidden" name="action" value="0">
            <div class="clearfix"></div>
          </div>
          <?php if ($_smarty_tpl->tpl_vars['msg']->value) {?>
            <p class="alert <?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>alert-danger<?php } else { ?>alert-success<?php }?>">
              <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['msg']->value, ENT_QUOTES, 'UTF-8');?>

            </p>
          <?php }?>
      </form>
    </div>
</div>
<?php }
}
