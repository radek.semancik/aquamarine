<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:36
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\layouts\layout-both-columns.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f9209bafb5_23276033',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c8be8840b0ff827daedfb9c55f72c056e8c1128e' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\layouts\\layout-both-columns.tpl',
      1 => 1566916292,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/head.tpl' => 1,
    'file:_partials/javascript.tpl' => 1,
    'file:catalog/_partials/product-activation.tpl' => 1,
    'file:_partials/header.tpl' => 1,
    'file:_partials/notifications.tpl' => 1,
    'file:_partials/breadcrumb.tpl' => 1,
    'file:_partials/footer.tpl' => 1,
  ),
),false)) {
function content_5d70f9209bafb5_23276033 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

  <head>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4000615865d70f9208d52b3_56560911', 'head');
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11519458635d70f9208d94a0_21025921', 'javascript_bottom');
?>

  </head>

  <body id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
" class="sang <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'classnames' ][ 0 ], array( $_smarty_tpl->tpl_vars['page']->value['body_classes'] )), ENT_QUOTES, 'UTF-8');
if (isset($_smarty_tpl->tpl_vars['order_mini_cart']->value) && $_smarty_tpl->tpl_vars['order_mini_cart']->value == 'minicart') {?> pg_minicart<?php }
if ((isset($_smarty_tpl->tpl_vars['product_zoom_image']->value) && $_smarty_tpl->tpl_vars['product_zoom_image']->value)) {?> img_zoom_whenhover<?php }?>">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9204174905d70f9208ec9b6_16239547', 'hook_after_body_opening_tag');
?>


    <main>
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10305400765d70f9208f0b00_13464433', 'product_activation');
?>



        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19513943685d70f9208f4ac2_38072190', 'header');
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11107207275d70f920902584_61700142', 'notifications');
?>


      <div id="wrapper">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19407160415d70f920904f29_95352978', 'breadcrumb');
?>

        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'index') {?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product')) {?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['product']->value['id'] == '3') {?>
                    <div class="page_full_size col-xs-12">
                        <div class="row">
                <?php } else { ?>
            
                    <?php if (isset($_smarty_tpl->tpl_vars['product_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value == 1) {?>
                        <div class="page_full_size col-xs-12">
                            <div class="row">
                    <?php } else { ?>
                        <div class="container">
                        <div class="row">
                    <?php }?>
                <?php }?>
            <?php }?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category')) {?>                
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '6') {?>                   
                    <div class="page_full_size col-xs-12">
                        <div class="row">
                <?php } else { ?>
                    <?php if (isset($_smarty_tpl->tpl_vars['category_full_size']->value) && $_smarty_tpl->tpl_vars['category_full_size']->value != 1) {?>
                        <div class="container">
                        <div class="row">
                    <?php } else { ?>
                        <div class="page_full_size col-xs-12">
                            <div class="row">
                    <?php }?>
                <?php }?>
            <?php }?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'product' && $_smarty_tpl->tpl_vars['page']->value['page_name'] != 'category')) {?>
                <div class="container">
                    <div class="row">
            <?php }?>
        <?php }?>
        
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>            
            <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_left') {?>
                  <div id="left-column" class="col-xs-12 col-sm-4 col-md-3" <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?> style="display: none!important"<?php }?>>                     
                      <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                              
                           <?php }?>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                        </div>
              <?php } else { ?>
                    <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '8') {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18232467665d70f92092f598_80986612', "left_column");
?>

                    <?php }?>
              <?php }?>
        <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6646472075d70f9209344f7_93726233', "left_column");
?>

        <?php }?>
        
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
          <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_left') {?>
               <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12563716175d70f9209494c6_93704297', "content");
?>
                   
                </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_right') {?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11808466595d70f92094e747_65983343', "content_wrapper");
?>

          <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14937203545d70f92095ae73_70284935', "content_wrapper");
?>

                <?php } elseif (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '8') {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16919758305d70f920966e39_07026438', "content_wrapper");
?>

                <?php } else { ?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16389853615d70f92096dd23_79926523', "content_wrapper");
?>

                <?php }?>
          <?php }?>
        <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2375599375d70f9209755b0_65937919', "content_wrapper");
?>

        <?php }?>
          
          <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
            <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_right') {?>
              <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_714460515d70f920982b79_49990576', "right_column");
?>

            <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?>
                    <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                    </div>
                <?php }?>
            <?php }?>
          <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10708226285d70f920993e38_72302792', "right_column");
?>

          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'index') {?>
            
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product' && isset($_smarty_tpl->tpl_vars['product_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value != 1) && ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category' && isset($_smarty_tpl->tpl_vars['category_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value != 1)) {?>
                </div>
                </div>
            <?php } else { ?>
                </div>
                </div>
            <?php }?>
            
            
          <?php }?>
      </div>

      <footer id="footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3164465675d70f9209b1b63_39564847', "footer");
?>

      </footer>

    </main>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_64229955d70f9209b6459_51198478', 'hook_before_body_closing_tag');
?>

  </body>

</html>
<?php }
/* {block 'head'} */
class Block_4000615865d70f9208d52b3_56560911 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_4000615865d70f9208d52b3_56560911',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender('file:_partials/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php
}
}
/* {/block 'head'} */
/* {block 'javascript_bottom'} */
class Block_11519458635d70f9208d94a0_21025921 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'javascript_bottom' => 
  array (
    0 => 'Block_11519458635d70f9208d94a0_21025921',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender("file:_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['bottom']), 0, false);
?>
    <?php
}
}
/* {/block 'javascript_bottom'} */
/* {block 'hook_after_body_opening_tag'} */
class Block_9204174905d70f9208ec9b6_16239547 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_after_body_opening_tag' => 
  array (
    0 => 'Block_9204174905d70f9208ec9b6_16239547',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterBodyOpeningTag'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_after_body_opening_tag'} */
/* {block 'product_activation'} */
class Block_10305400765d70f9208f0b00_13464433 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_activation' => 
  array (
    0 => 'Block_10305400765d70f9208f0b00_13464433',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-activation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'product_activation'} */
/* {block 'header'} */
class Block_19513943685d70f9208f4ac2_38072190 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_19513943685d70f9208f4ac2_38072190',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender('file:_partials/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'header'} */
/* {block 'notifications'} */
class Block_11107207275d70f920902584_61700142 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'notifications' => 
  array (
    0 => 'Block_11107207275d70f920902584_61700142',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'notifications'} */
/* {block 'breadcrumb'} */
class Block_19407160415d70f920904f29_95352978 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'breadcrumb' => 
  array (
    0 => 'Block_19407160415d70f920904f29_95352978',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:_partials/breadcrumb.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
          <?php
}
}
/* {/block 'breadcrumb'} */
/* {block "left_column"} */
class Block_18232467665d70f92092f598_80986612 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_18232467665d70f92092f598_80986612',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                              <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                                  <div class="left_column_filter filter-trigger">
                            		<i class="fa fa-sliders p_r active"></i>
                            	  </div>
                               <?php }?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                            </div>
                          <?php
}
}
/* {/block "left_column"} */
/* {block "left_column"} */
class Block_6646472075d70f9209344f7_93726233 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_6646472075d70f9209344f7_93726233',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                      <div class="left_column_filter filter-trigger">
                		<i class="fa fa-sliders p_r active"></i>
                	  </div>
                   <?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayLeftColumnProduct'),$_smarty_tpl ) );?>

                  <?php } else { ?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                  <?php }?>
                </div>
              <?php
}
}
/* {/block "left_column"} */
/* {block "content"} */
class Block_12563716175d70f9209494c6_93704297 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_12563716175d70f9209494c6_93704297',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <p>Hello world! This is HTML5 Boilerplate.</p>
                  <?php
}
}
/* {/block "content"} */
/* {block "content"} */
class Block_20421928015d70f920950b56_04713719 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <p>Hello world! This is HTML5 Boilerplate.</p>
                      <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_11808466595d70f92094e747_65983343 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_11808466595d70f92094e747_65983343',
  ),
  'content' => 
  array (
    0 => 'Block_20421928015d70f920950b56_04713719',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20421928015d70f920950b56_04713719', "content", $this->tplIndex);
?>

                    </div>
                  <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_15161955125d70f92095cfd3_36630067 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_14937203545d70f92095ae73_70284935 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_14937203545d70f92095ae73_70284935',
  ),
  'content' => 
  array (
    0 => 'Block_15161955125d70f92095cfd3_36630067',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15161955125d70f92095cfd3_36630067', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_7539032285d70f920969074_49949749 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_16919758305d70f920966e39_07026438 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_16919758305d70f920966e39_07026438',
  ),
  'content' => 
  array (
    0 => 'Block_7539032285d70f920969074_49949749',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7539032285d70f920969074_49949749', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_20327342845d70f92096fe05_18120657 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_16389853615d70f92096dd23_79926523 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_16389853615d70f92096dd23_79926523',
  ),
  'content' => 
  array (
    0 => 'Block_20327342845d70f92096fe05_18120657',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="full-width col-xs-12 col-sm-12 col-md-12">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20327342845d70f92096fe05_18120657', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_20799060375d70f920977664_39429212 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <p>Hello world! This is HTML5 Boilerplate.</p>
                  <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_2375599375d70f9209755b0_65937919 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_2375599375d70f9209755b0_65937919',
  ),
  'content' => 
  array (
    0 => 'Block_20799060375d70f920977664_39429212',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="content-wrapper" class="left-column right-column col-sm-4 col-md-6">
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20799060375d70f920977664_39429212', "content", $this->tplIndex);
?>

                </div>
              <?php
}
}
/* {/block "content_wrapper"} */
/* {block "right_column"} */
class Block_714460515d70f920982b79_49990576 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'right_column' => 
  array (
    0 => 'Block_714460515d70f920982b79_49990576',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                </div>
              <?php
}
}
/* {/block "right_column"} */
/* {block "right_column"} */
class Block_10708226285d70f920993e38_72302792 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'right_column' => 
  array (
    0 => 'Block_10708226285d70f920993e38_72302792',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayRightColumnProduct'),$_smarty_tpl ) );?>

                  <?php } else { ?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayColumRightBlog'),$_smarty_tpl ) );?>

                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                  <?php }?>
                </div>
              <?php
}
}
/* {/block "right_column"} */
/* {block "footer"} */
class Block_3164465675d70f9209b1b63_39564847 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_3164465675d70f9209b1b63_39564847',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender("file:_partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block "footer"} */
/* {block 'hook_before_body_closing_tag'} */
class Block_64229955d70f9209b6459_51198478 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_before_body_closing_tag' => 
  array (
    0 => 'Block_64229955d70f9209b6459_51198478',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayBeforeBodyClosingTag'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_before_body_closing_tag'} */
}
