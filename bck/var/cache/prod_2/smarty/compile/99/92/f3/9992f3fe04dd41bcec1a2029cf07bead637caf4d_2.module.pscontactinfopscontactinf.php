<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:31
  from 'module:pscontactinfopscontactinf' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91bda5b68_73271743',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9992f3fe04dd41bcec1a2029cf07bead637caf4d' => 
    array (
      0 => 'module:pscontactinfopscontactinf',
      1 => 1559309871,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f91bda5b68_73271743 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="block-contact links wrapper widget-text pd_l pd_r">
  <div class="text-widget">
      
      <?php if (isset($_smarty_tpl->tpl_vars['TEA_CT_ADDRESS']->value) && $_smarty_tpl->tpl_vars['TEA_CT_ADDRESS']->value) {?>
        <div class="">
            <i class="pe-7s-map-marker icon-Pe"></i> <span><?php echo $_smarty_tpl->tpl_vars['TEA_CT_ADDRESS']->value;?>
</span>
        </div>
      <?php } else { ?>
          <?php if (isset($_smarty_tpl->tpl_vars['contact_infos']->value['address']['address1']) && $_smarty_tpl->tpl_vars['contact_infos']->value['address']['address1']) {?>  
            <p class="">
                <i class="pe-7s-map-marker icon-Pe"></i>
                <span><?php echo $_smarty_tpl->tpl_vars['contact_infos']->value['address']['address1'];?>
</span>
            </p>
          <?php }?>
          <?php if (isset($_smarty_tpl->tpl_vars['contact_infos']->value['address']['address2']) && $_smarty_tpl->tpl_vars['contact_infos']->value['address']['address2']) {?>  
            <p class="">
                <i class="pe-7s-map-marker icon-Pe"></i>
                <span><?php echo $_smarty_tpl->tpl_vars['contact_infos']->value['address']['address2'];?>
</span>
            </p>
          <?php }?>
      <?php }?>
  
      <?php if ($_smarty_tpl->tpl_vars['contact_infos']->value['email']) {?>
        <p class="middle-xs">
            <i class="pe-7s-mail icon-Pe"></i>
            <a class="contact_email" href="mailto:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_infos']->value['email'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_infos']->value['email'], ENT_QUOTES, 'UTF-8');?>
</a>
        </p>
      <?php }?>
      
      <?php if (isset($_smarty_tpl->tpl_vars['TEA_CT_PHONENUMBER']->value) && $_smarty_tpl->tpl_vars['TEA_CT_PHONENUMBER']->value) {?>
        <p class="">
          <i class="pe-7s-call icon-Pe"></i>
          <?php if (isset($_smarty_tpl->tpl_vars['TEA_CT_PHONENUMBER_CALL']->value) && $_smarty_tpl->tpl_vars['TEA_CT_PHONENUMBER_CALL']->value) {?><a href="tel:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['TEA_CT_PHONENUMBER_CALL']->value, ENT_QUOTES, 'UTF-8');?>
"><?php }?>
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['TEA_CT_PHONENUMBER']->value, ENT_QUOTES, 'UTF-8');?>

          <?php if (isset($_smarty_tpl->tpl_vars['TEA_CT_PHONENUMBER_CALL']->value) && $_smarty_tpl->tpl_vars['TEA_CT_PHONENUMBER_CALL']->value) {?></a><?php }?>
        </p>
      <?php } else { ?>
        <?php if ($_smarty_tpl->tpl_vars['contact_infos']->value['phone']) {?>
            <p class="">
                <i class="pe-7s-call icon-Pe"></i>
                <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_infos']->value['phone'], ENT_QUOTES, 'UTF-8');?>
</span>
            </p>
            
          <?php }?>
      <?php }?>
      
      <?php if ($_smarty_tpl->tpl_vars['contact_infos']->value['fax']) {?>
        <p class="middle-xs">
            <i class="pe-7s-print icon-Pe"></i>
            <span> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_infos']->value['fax'], ENT_QUOTES, 'UTF-8');?>
</span>
        </p>
      <?php }?>
      
      <?php if (isset($_smarty_tpl->tpl_vars['TEA_CT_OPENTIME']->value) && $_smarty_tpl->tpl_vars['TEA_CT_OPENTIME']->value) {?>
        <p class="">
          <i class="fa fa-clock-o"></i>
          <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['TEA_CT_OPENTIME']->value, ENT_QUOTES, 'UTF-8');?>

        </p>
      <?php }?>
      
  </div>
</div>
<?php }
}
