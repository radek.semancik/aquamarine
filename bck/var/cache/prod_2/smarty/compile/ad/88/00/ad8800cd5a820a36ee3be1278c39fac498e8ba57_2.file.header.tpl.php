<?php
/* Smarty version 3.1.33, created on 2019-09-05 14:01:30
  from 'C:\Work\pixape\pragaglobal\modules\tea_productcomparison\views\templates\hook\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d70f91ae4d827_00762000',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ad8800cd5a820a36ee3be1278c39fac498e8ba57' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\modules\\tea_productcomparison\\views\\templates\\hook\\header.tpl',
      1 => 1559309866,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d70f91ae4d827_00762000 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
    var min_item ='<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please select at least one product','mod'=>'tea_productcomparison'),$_smarty_tpl ) );?>
';
    var text_add_compare ='<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add compare successful','mod'=>'tea_productcomparison'),$_smarty_tpl ) );?>
';
    var text_delete_compare ='<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Delete compare successful','mod'=>'tea_productcomparison'),$_smarty_tpl ) );?>
';
    var max_item= '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You cannot add more than','mod'=>'tea_productcomparison'),$_smarty_tpl ) );?>
 '+<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['comparator_max_item']->value), ENT_QUOTES, 'UTF-8');?>
+' <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'product(s) to the product comparison','mod'=>'tea_productcomparison'),$_smarty_tpl ) );?>
';
    var comparator_max_item =<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['comparator_max_item']->value), ENT_QUOTES, 'UTF-8');?>
;
    var comparedProductsIds =[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['comparedProductsIds']->value, ENT_QUOTES, 'UTF-8');?>
];
    var ulr_compare = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tea_productcomparison'),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';  
<?php echo '</script'; ?>
><?php }
}
