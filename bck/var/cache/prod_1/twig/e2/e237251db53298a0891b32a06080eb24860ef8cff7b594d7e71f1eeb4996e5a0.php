<?php

/* __string_template__7d2f3e866e017f03d7c897a1fcab71dcdc5f84fe9e5feb188c0e6e10c55678ee */
class __TwigTemplate_3083af42969514655d941e38e2bdc343a73553c75d69f8776ff498763205b03d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"cs\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>x • PragaGroup</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminProducts';
    var iso_user = 'cs';
    var lang_is_rtl = '0';
    var full_language_code = 'cs-CZ';
    var full_cldr_language_code = 'cs-CZ';
    var country_iso_code = 'CZ';
    var _PS_VERSION_ = '1.7.5.2';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Ve vašem obchodě je zaevidovaná nová objednávka.';
    var order_number_msg = 'Číslo objednávky ';
    var total_msg = 'Celkem: ';
    var from_msg = 'Od: ';
    var see_order_msg = 'Zobrazit tuto objednávku';
    var new_customer_msg = 'Ve vašem obchodě je zaregistrován nový zákazník.';
    var customer_name_msg = 'Jméno zákazníka: ';
    var new_msg = 'Nová zpráva byla přidána na váš obchod.';
    var see_msg = 'Číst tuto zprávu';
    var token = '88adc6afcf497bcf06704e66fd391187';
    var token_admin_orders = '5c556600f17857de3cc0cbb4a24b75bc';
    var token_admin_customers = '61ff7d046cb22aa63b1b2476b65a875a';
    var token_admin_customer_threads = 'b07326f7ee549e4b4dc152944845d89b';
    var currentIndex = 'index.php?controller=AdminProducts';
    var employee_token = '701f4714da48124030267662092e3bcd';
    var choose_language_translate = 'Vyberte jazyk';
    var default_language = '1';
    var admin_modules_link = '/Backoffice/index.php/improve/modules/catalog/recommended?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0';
    var tab_modules_list = 'prestagiftvouchers,dmuassocprodcat,etranslation,apiway,prestashoptoquickbooks';
    var update_success_msg = 'Aktualizace byla úspěšná';
    var errorLogin = 'PrestaShop vás nemohl přihlásit do Addons, zkontrolujte vaše přihlašovací údaje pro připojení k internetu.';
    var search_product_msg = 'Vyhledat produkt';
  </script>

      <link href=\"/modules/pg_productbundles/views/css/admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/Backoffice/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/Backoffice/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/Backoffice\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"CZK\",\"sign\":\"K\\u010d\",\"name\":\"\\u010desk\\u00e1 koruna\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/js/jquery/jquery-1.11.0.min.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/jquery-migrate-1.2.1.min.js\"></script>
<script type=\"text/javascript\" src=\"/modules/pg_productbundles/views/js/admin.js\"></script>
<script type=\"text/javascript\" src=\"/Backoffice/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.5.2\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.5.2\"></script>
<script type=\"text/javascript\" src=\"/Backoffice/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/Backoffice/themes/default/js/vendor/nv.d3.min.js\"></script>

  <style>

.icon-AdminSmartBlog:before{

  content: \"\\f14b\";

   }

 

</style>

";
        // line 86
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"lang-cs adminproducts\">


<header id=\"header\">
  <nav id=\"header_infos\" class=\"main-header\">

    <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

        
        <i class=\"material-icons js-mobile-menu\">menu</i>
    <a id=\"header_logo\" class=\"logo float-left\" href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminDashboard&amp;token=d1d9d1ba0652d886ca3e7d8c7cfd50dc\"></a>
    <span id=\"shop_version\">1.7.5.2</span>

    <div class=\"component\" id=\"quick-access-container\">
      <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Rychlý přístup
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=bea8eafbc0df8136c199927401a8a448\"
                 data-item=\"Katalogové soupisky\"
      >Katalogové soupisky</a>
          <a class=\"dropdown-item\"
         href=\"http://pragaglobal.loc/Backoffice/index.php/improve/modules/manage?token=df6a11eae9936b7d6764fec18e046c18\"
                 data-item=\"Nainstalované moduly\"
      >Nainstalované moduly</a>
          <a class=\"dropdown-item\"
         href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCategories&amp;addcategory&amp;token=59f3fb11c45b83535c85eb34fe6398f7\"
                 data-item=\"Novou kategorii\"
      >Novou kategorii</a>
          <a class=\"dropdown-item\"
         href=\"http://pragaglobal.loc/Backoffice/index.php/sell/catalog/products/new?token=df6a11eae9936b7d6764fec18e046c18\"
                 data-item=\"Nový produkt\"
      >Nový produkt</a>
          <a class=\"dropdown-item\"
         href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=6e5f3c4880042884d90f9e4b50ee6544\"
                 data-item=\"Nový slevový kupón\"
      >Nový slevový kupón</a>
          <a class=\"dropdown-item\"
         href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminOrders&amp;token=5c556600f17857de3cc0cbb4a24b75bc\"
                 data-item=\"Objednávky\"
      >Objednávky</a>
          <a class=\"dropdown-item\"
         href=\"http://pragaglobal.loc/Backoffice/index.php?admin8253dbzee/?controller=AdminModules&amp;&amp;configure=smartblog&amp;token=2e23266c226117ff240269f9bde50ecd\"
                 data-item=\"Smart Blog Setting\"
      >Smart Blog Setting</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"193\"
        data-icon=\"icon-AdminCatalog\"
        data-method=\"add\"
        data-url=\"index.php/sell/catalog/products/26\"
        data-post-link=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminQuickAccesses&token=08c0f6a80cf26228db8ab7e557eea8d2\"
        data-prompt-text=\"Pojmenujte tuto zkratku:\"
        data-link=\"Produkty - Seznam\"
      >
        <i class=\"material-icons\">add_circle</i>
        Přidat aktuální stránku do menu rychlého přístupu
      </a>
        <a class=\"dropdown-item\" href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminQuickAccesses&token=08c0f6a80cf26228db8ab7e557eea8d2\">
      <i class=\"material-icons\">settings</i>
      Správa rychlých přístupů
    </a>
  </div>
</div>
    </div>
    <div class=\"component\" id=\"header-search-container\">
      <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/Backoffice/index.php?controller=AdminSearch&amp;token=cd8dba153b57ea3d885647b7e39873be\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Vyhledat (např. kód produktu, jméno zákazníka...)\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        Všude
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"Všude\" href=\"#\" data-value=\"0\" data-placeholder=\"Co hledáte?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> Všude</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Katalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Název produktu, kód...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Katalog</a>
        <a class=\"dropdown-item\" data-item=\"Zákazníci podle jména\" href=\"#\" data-value=\"2\" data-placeholder=\"E-mail, jméno...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Zákazníci podle jména</a>
        <a class=\"dropdown-item\" data-item=\"Zákazníci podle IP adresy\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Zákazníci podle IP adresy</a>
        <a class=\"dropdown-item\" data-item=\"Objednávky\" href=\"#\" data-value=\"3\" data-placeholder=\"ID objednávky\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Objednávky</a>
        <a class=\"dropdown-item\" data-item=\"Faktury\" href=\"#\" data-value=\"4\" data-placeholder=\"Číslo faktury\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i> Faktury</a>
        <a class=\"dropdown-item\" data-item=\"Košíky\" href=\"#\" data-value=\"5\" data-placeholder=\"ID košíku\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Košíky</a>
        <a class=\"dropdown-item\" data-item=\"Moduly\" href=\"#\" data-value=\"7\" data-placeholder=\"Název modulu\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Moduly</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">HLEDAT</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
    </div>

            <div class=\"component\" id=\"header-shop-list-container\">
        <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://pragaglobal.loc/\" target= \"_blank\">
      <i class=\"material-icons\">visibility</i>
      Zobrazit můj obchod
    </a>
  </div>
    </div>
          <div class=\"component header-right-component\" id=\"header-notifications-container\">
        <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Objednávky<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Zákazníc<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Zprávy<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Zatím žádá nová objednávka :(<br>
              Co takhle nějaké sezónní slevy?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Zatím žádný nový zákazník :(<br>
              Uvažovali jste o prodeji na tržištích?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Zatím žádná nová zpráva.<br>
              Žádné zprávy - dobré zprávy, ne?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      z <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - zaregistrováno <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
      </div>
        <div class=\"component\" id=\"header-employee-container\">
      <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"text-center employee_avatar\">
      <img class=\"avatar rounded-circle\" src=\"http://profile.prestashop.com/radeksemi%40gmail.com.jpg\" />
      <span>Radek Semančík</span>
    </div>
    <a class=\"dropdown-item employee-link profile-link\" href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminEmployees&amp;id_employee=1&amp;updateemployee=1&amp;token=701f4714da48124030267662092e3bcd\">
      <i class=\"material-icons\">settings_applications</i>
      Váš profil
    </a>
    <a class=\"dropdown-item employee-link\" id=\"header_logout\" href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminLogin&amp;logout=1&amp;token=c9ff4d98dd63df5025ff8592470b43f6\">
      <i class=\"material-icons\">power_settings_new</i>
      <span>Odhlásit</span>
    </a>
  </div>
</div>
    </div>

      </nav>
  </header>

<nav class=\"nav-bar d-none d-md-block\">
  <span class=\"menu-collapse\">
    <i class=\"material-icons\">chevron_left</i>
    <i class=\"material-icons\">chevron_left</i>
  </span>

  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminDashboard&amp;token=d1d9d1ba0652d886ca3e7d8c7cfd50dc\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Nástěnka</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Obchod</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminOrders&amp;token=5c556600f17857de3cc0cbb4a24b75bc\" class=\"link\">
                    <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                    <span>
                    Objednávky
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminOrders&amp;token=5c556600f17857de3cc0cbb4a24b75bc\" class=\"link\"> Objednávky
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"/Backoffice/index.php/sell/orders/invoices/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Faktury
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminSlip&amp;token=e2305ed685c9943d62a62ecc177599ba\" class=\"link\"> Dobropisy
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"/Backoffice/index.php/sell/orders/delivery-slips/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Dodací listy
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCarts&amp;token=130b67cf753a311758d84278fb1107e4\" class=\"link\"> Nákupní košíky
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                                                    
                <li class=\"link-levelone has_submenu -active open ul-open\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/Backoffice/index.php/sell/catalog/products?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\">
                    <i class=\"material-icons mi-store\">store</i>
                    <span>
                    Katalog
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_up
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/Backoffice/index.php/sell/catalog/products?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Produkty
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCategories&amp;token=59f3fb11c45b83535c85eb34fe6398f7\" class=\"link\"> Kategorie
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminTracking&amp;token=5f8adbf9181a69e01ad9d1be858a9e8a\" class=\"link\"> Monitorování
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminAttributesGroups&amp;token=5bc21ced1313dd8aedd80caeb2298281\" class=\"link\"> Atributy a Vlastnosti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminManufacturers&amp;token=55a118db301933a90707a54cb2b2903a\" class=\"link\"> Značky a Dodavatelé
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminAttachments&amp;token=1038b764c580d0864bcd72f80a37ced1\" class=\"link\"> Soubory
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCartRules&amp;token=6e5f3c4880042884d90f9e4b50ee6544\" class=\"link\"> Sleva
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/Backoffice/index.php/sell/stocks/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCustomers&amp;token=61ff7d046cb22aa63b1b2476b65a875a\" class=\"link\">
                    <i class=\"material-icons mi-account_circle\">account_circle</i>
                    <span>
                    Zákazníci
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCustomers&amp;token=61ff7d046cb22aa63b1b2476b65a875a\" class=\"link\"> Zákazníci
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminAddresses&amp;token=ee9c6dc857d187d9cd92b983d6211754\" class=\"link\"> Adresy
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCustomerThreads&amp;token=b07326f7ee549e4b4dc152944845d89b\" class=\"link\">
                    <i class=\"material-icons mi-chat\">chat</i>
                    <span>
                    Zákaznický servis
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCustomerThreads&amp;token=b07326f7ee549e4b4dc152944845d89b\" class=\"link\"> Zákaznický servis
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminOrderMessage&amp;token=2c05b88deec5e751ec61ee58412fc985\" class=\"link\"> Zprávy k objednávkám
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminReturn&amp;token=38ae5db59f53d7c539138d5b44aba386\" class=\"link\"> Vrácení zboží
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminStats&amp;token=bea8eafbc0df8136c199927401a8a448\" class=\"link\">
                    <i class=\"material-icons mi-assessment\">assessment</i>
                    <span>
                    Statistiky
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Rozšíření</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminPsMboModule&amp;token=a2f5fd6eee4681faec463127c102121b\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Moduly
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"44\" id=\"subtab-AdminParentModulesCatalog\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminPsMboModule&amp;token=a2f5fd6eee4681faec463127c102121b\" class=\"link\"> Katalog modulů
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"47\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/Backoffice/index.php/improve/modules/manage?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Module Manager
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"52\" id=\"subtab-AdminParentThemes\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminThemes&amp;token=50681fac9aff7f8f69aa43a5f33f99a7\" class=\"link\">
                    <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                    <span>
                    Vzhled
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-52\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"123\" id=\"subtab-AdminThemesParent\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminThemes&amp;token=50681fac9aff7f8f69aa43a5f33f99a7\" class=\"link\"> Šablona a logo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"130\" id=\"subtab-AdminPsMboTheme\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminPsMboTheme&amp;token=37de32fc3f369ec7cfeec232f8b5b7f2\" class=\"link\"> Katalog šablon
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminCmsContent\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCmsContent&amp;token=bedf58d208857848cc9584b0e523d085\" class=\"link\"> Stránky
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"56\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"/Backoffice/index.php/improve/design/modules/positions/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Pozice modulů
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminImages\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminImages&amp;token=cb83a722bf215ea804a0bd529190963d\" class=\"link\"> Nastavení obrázků
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"122\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminLinkWidget&amp;token=1687c200d0d7e1c5e580ba10e175891d\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"58\" id=\"subtab-AdminParentShipping\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCarriers&amp;token=d55f1014255da5dbfaa55ce5e4c3e6fc\" class=\"link\">
                    <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                    <span>
                    Doručení
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-58\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"59\" id=\"subtab-AdminCarriers\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminCarriers&amp;token=d55f1014255da5dbfaa55ce5e4c3e6fc\" class=\"link\"> Dopravci
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"60\" id=\"subtab-AdminShipping\">
                              <a href=\"/Backoffice/index.php/improve/shipping/preferences?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Konfigurace
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"61\" id=\"subtab-AdminParentPayment\">
                  <a href=\"/Backoffice/index.php/improve/payment/payment_methods?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\">
                    <i class=\"material-icons mi-payment\">payment</i>
                    <span>
                    Platba
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-61\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"62\" id=\"subtab-AdminPayment\">
                              <a href=\"/Backoffice/index.php/improve/payment/payment_methods?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Platební metody
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"63\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"/Backoffice/index.php/improve/payment/preferences?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Konfigurace
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"64\" id=\"subtab-AdminInternational\">
                  <a href=\"/Backoffice/index.php/improve/international/localization/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\">
                    <i class=\"material-icons mi-language\">language</i>
                    <span>
                    Mezinárodní
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-64\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"65\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"/Backoffice/index.php/improve/international/localization/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Lokalizace
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"70\" id=\"subtab-AdminParentCountries\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminZones&amp;token=8066f3ab38faf0e6338f913c67fe61d4\" class=\"link\"> Lokace
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"74\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminTaxes&amp;token=6424f1149049b40c4eceeffa1b247059\" class=\"link\"> DPH
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"77\" id=\"subtab-AdminTranslations\">
                              <a href=\"/Backoffice/index.php/improve/international/translations/settings?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Překlady
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"132\" id=\"subtab-AdminTea_themelayoutDashboard\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminTea_themelayoutHeader&amp;token=5c57bb3d4ef0932da4d55e024a25fa6c\" class=\"link\">
                    <i class=\"material-icons mi-\"></i>
                    <span>
                    Page Builder
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-132\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"133\" id=\"subtab-AdminTea_themelayoutHeader\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminTea_themelayoutHeader&amp;token=5c57bb3d4ef0932da4d55e024a25fa6c\" class=\"link\"> Header
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"134\" id=\"subtab-AdminTea_themelayoutHomebody\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminTea_themelayoutHomebody&amp;token=780e1fab0a386d00d6a238b3a9c1f223\" class=\"link\"> Body
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"135\" id=\"subtab-AdminTea_themelayoutFooter\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminTea_themelayoutFooter&amp;token=8630cd2b2201b8b96c7bb5188eea4236\" class=\"link\"> Footer
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"136\" id=\"subtab-AdminTea_themelayoutHomepage\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminTea_themelayoutHomepage&amp;token=f9a7eef04f6d71817684f31077e91b9d\" class=\"link\"> Home Pages
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"78\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Konfigurace</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"79\" id=\"subtab-ShopParameters\">
                  <a href=\"/Backoffice/index.php/configure/shop/preferences/preferences?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\">
                    <i class=\"material-icons mi-settings\">settings</i>
                    <span>
                    Nastavení eshopu
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-79\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"80\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"/Backoffice/index.php/configure/shop/preferences/preferences?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Hlavní
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"83\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"/Backoffice/index.php/configure/shop/order-preferences/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Nastavení objednávek
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"86\" id=\"subtab-AdminPPreferences\">
                              <a href=\"/Backoffice/index.php/configure/shop/product-preferences/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Produkty
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"87\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"/Backoffice/index.php/configure/shop/customer-preferences/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Nastavení zákazníků
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"91\" id=\"subtab-AdminParentStores\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminContacts&amp;token=c1abf028587b267f8bb5383ab9c6f2a5\" class=\"link\"> Napište nám
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"94\" id=\"subtab-AdminParentMeta\">
                              <a href=\"/Backoffice/index.php/configure/shop/seo-urls/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Webový provoz a SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"98\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminSearchConf&amp;token=816610a1ff1e3b92cdbc6873ce9b731a\" class=\"link\"> Vyhledávání
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"127\" id=\"subtab-AdminGamification\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminGamification&amp;token=785e31f3133439ec9948d52fcf4a5144\" class=\"link\"> Merchant Expertise
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"101\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/Backoffice/index.php/configure/advanced/system-information/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\">
                    <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                    <span>
                    Nástroje
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-101\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"102\" id=\"subtab-AdminInformation\">
                              <a href=\"/Backoffice/index.php/configure/advanced/system-information/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Informace
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"103\" id=\"subtab-AdminPerformance\">
                              <a href=\"/Backoffice/index.php/configure/advanced/performance/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Výkon
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"104\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"/Backoffice/index.php/configure/advanced/administration/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Administrace
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\" id=\"subtab-AdminEmails\">
                              <a href=\"/Backoffice/index.php/configure/advanced/emails/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> E-maily
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"106\" id=\"subtab-AdminImport\">
                              <a href=\"/Backoffice/index.php/configure/advanced/import/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Importovat
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"107\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminEmployees&amp;token=701f4714da48124030267662092e3bcd\" class=\"link\"> Zaměstnanci
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"111\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminRequestSql&amp;token=5ebd5a46f4848296abccef74d7a3283f\" class=\"link\"> Databáze
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"114\" id=\"subtab-AdminLogs\">
                              <a href=\"/Backoffice/index.php/configure/advanced/logs/?_token=Q2kBHTEJSa03ryWrQbpOn8mMbHe2vrsoJgb1GzDn1e0\" class=\"link\"> Logy
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"115\" id=\"subtab-AdminWebservice\">
                              <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminWebservice&amp;token=010d60345270326d1f6c6e3291ff0fe3\" class=\"link\"> Webové služby
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"137\" id=\"tab-AdminSmartBlog\">
              <span class=\"title\">Blog</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"138\" id=\"subtab-AdminBlogCategory\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminBlogCategory&amp;token=2117f1e4eacbe57a9c342bdcfbdeb810\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Blog Category
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"139\" id=\"subtab-AdminBlogcomment\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminBlogcomment&amp;token=3b2b3467214025e988b579c662eced66\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Blog Comments
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"140\" id=\"subtab-AdminBlogPost\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminBlogPost&amp;token=b49966a854d653bbceabb35d754dd036\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Blog Post
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"141\" id=\"subtab-AdminImageType\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminImageType&amp;token=140cc76b2fd5d03c774bc652393f3cce\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Image Type
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"142\" id=\"subtab-AdminAboutUs\">
                  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminAboutUs&amp;token=d84729383d6c01dab8d34328a032f6cb\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    AboutUs
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
            </ul>
  
</nav>

<div id=\"main-div\">

  
        
    <div class=\"content-div -notoolbar \">

      

      
                        
      <div class=\"row \">
        <div class=\"col-sm-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  ";
        // line 1137
        $this->displayBlock('content_header', $context, $blocks);
        // line 1138
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1139
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1140
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1141
        echo "
           
<div class=\"modal fade\" id=\"modules_list_container\">
\t<div class=\"modal-dialog\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
\t\t\t\t<h3 class=\"modal-title\">Doporučené moduly a služby</h3>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div id=\"modules_list_container_tab_modal\" style=\"display:none;\"></div>
\t\t\t\t<div id=\"modules_list_loader\"><i class=\"icon-refresh icon-spin\"></i></div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
        </div>
      </div>

    </div>

  
</div>

<div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>Ale ne!</h1>
  <p class=\"mt-3\">
    Mobilní verze této stránky není zatím k dispozici.
  </p>
  <p class=\"mt-2\">
    Dokud nebude tato stránka uzpůsobena pro mobilní zařízení, otevírejte ji ze stolního počítače.
  </p>
  <p class=\"mt-2\">
    Děkujeme Vám.
  </p>
  <a href=\"http://pragaglobal.loc/Backoffice/index.php?controller=AdminDashboard&amp;token=d1d9d1ba0652d886ca3e7d8c7cfd50dc\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Zpět
  </a>
</div>
<div class=\"mobile-layer\"></div>

  <div id=\"footer\" class=\"bootstrap\">
    
</div>


  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-CS&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/cs/login?email=radeksemi%40gmail.com&amp;firstname=Radek&amp;lastname=Seman%C4%8D%C3%ADk&amp;website=http%3A%2F%2Fpragaglobal.loc%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-CS&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/Backoffice/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Propojte svůj obchod s PrestaShop Marketplace v zájmu automatického importování všech zakoupených doplňků.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Nemáte účet?</h4>
\t\t\t\t\t\t<p class='text-justify'>Objevte sílu PrestaShop Addons! Prozkoumejte oficiální obchod s více než 3500 inovativními moduly a šablonami vzhledu, které optimalizují míru konverze, zvyšují provoz, budují loajalitu zákazníků a zvyšují Vaší produktivitu</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Připojit k PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/cs/forgot-your-password\">Zapoměl jsem moje heslo</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/cs/login?email=radeksemi%40gmail.com&amp;firstname=Radek&amp;lastname=Seman%C4%8D%C3%ADk&amp;website=http%3A%2F%2Fpragaglobal.loc%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-CS&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tRegistrovat
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Přihlásit se
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

  </div>

";
        // line 1264
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 86
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
    }

    // line 1137
    public function block_content_header($context, array $blocks = array())
    {
    }

    // line 1138
    public function block_content($context, array $blocks = array())
    {
    }

    // line 1139
    public function block_content_footer($context, array $blocks = array())
    {
    }

    // line 1140
    public function block_sidebar_right($context, array $blocks = array())
    {
    }

    // line 1264
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "__string_template__7d2f3e866e017f03d7c897a1fcab71dcdc5f84fe9e5feb188c0e6e10c55678ee";
    }

    public function getDebugInfo()
    {
        return array (  1343 => 1264,  1338 => 1140,  1333 => 1139,  1328 => 1138,  1323 => 1137,  1314 => 86,  1306 => 1264,  1181 => 1141,  1178 => 1140,  1175 => 1139,  1172 => 1138,  1170 => 1137,  115 => 86,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "__string_template__7d2f3e866e017f03d7c897a1fcab71dcdc5f84fe9e5feb188c0e6e10c55678ee", "");
    }
}
