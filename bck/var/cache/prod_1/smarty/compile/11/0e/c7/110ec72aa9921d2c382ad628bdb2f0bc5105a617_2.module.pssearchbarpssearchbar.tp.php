<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:06
  from 'module:pssearchbarpssearchbar.tp' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b1611ac34_36455650',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '110ec72aa9921d2c382ad628bdb2f0bc5105a617' => 
    array (
      0 => 'module:pssearchbarpssearchbar.tp',
      1 => 1559309872,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b1611ac34_36455650 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- Block search module TOP -->
<div id="_desktop_searchbar" class="claue-action">
    <a class="sf-open" href="javascript:void(0)">
		<i class="pe-7s-search "></i>
	</a>
    <div id="search_widget" class="form-search search-widget header__search" data-search-controller-url="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_controller_url']->value, ENT_QUOTES, 'UTF-8');?>
">
    	<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_controller_url']->value, ENT_QUOTES, 'UTF-8');?>
">
    		<input type="hidden" name="controller" value="search">
    		<input type="text" name="s" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_string']->value, ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search for...','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
">
    		<button type="submit">
    			<i class="pe-7s-search search"></i>
                <span id="circularG">
                	<span id="circularG_1" class="circularG"></span>
                	<span id="circularG_2" class="circularG"></span>
                	<span id="circularG_3" class="circularG"></span>
                	<span id="circularG_4" class="circularG"></span>
                	<span id="circularG_5" class="circularG"></span>
                	<span id="circularG_6" class="circularG"></span>
                	<span id="circularG_7" class="circularG"></span>
                	<span id="circularG_8" class="circularG"></span>
                </span>
    		</button>
    	</form>
        <a id="sf-close" class="pa" href="javascript:void(0)"><i class="pe-7s-close"></i></a>
    </div>
</div>
<!-- /Block search module TOP -->
<?php }
}
