<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:06
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\catalog\_partials\miniatures\product.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b168a4592_81612479',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5e8b2bc2f0a13b447d128d05018dce03ba2ba826' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\catalog\\_partials\\miniatures\\product.tpl',
      1 => 1559309874,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/variant-links.tpl' => 1,
  ),
),false)) {
function content_5cfa6b168a4592_81612479 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4318639905cfa6b16837161_43857847', 'product_miniature_item');
?>

<?php }
/* {block 'product_thumbnail'} */
class Block_2941332635cfa6b1683ab32_75709699 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['url'], ENT_QUOTES, 'UTF-8');?>
" class="thumbnail product-thumbnail">
                  <img
                    src = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['bySize']['home_default']['url'], ENT_QUOTES, 'UTF-8');?>
"
                    alt = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['legend'], ENT_QUOTES, 'UTF-8');?>
"
                    data-full-size-image-url = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['url'], ENT_QUOTES, 'UTF-8');?>
"
                  >
                </a>
              <?php
}
}
/* {/block 'product_thumbnail'} */
/* {block 'product_flags'} */
class Block_2191713265cfa6b16848e33_45092694 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                  <ul class="product-flags">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['product']->value['flags'], 'flag');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['flag']->value) {
?>
                      <?php if ($_smarty_tpl->tpl_vars['flag']->value['type'] == 'on-sale') {?>
                        <li class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['type'], ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sale!','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</li>
                      <?php } else { ?>
                        <li class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['type'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['label'], ENT_QUOTES, 'UTF-8');?>
</li>
                      <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                  </ul>
              <?php
}
}
/* {/block 'product_flags'} */
/* {block 'quick_view'} */
class Block_20511216515cfa6b1686cb39_13960846 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <a class="quick-view btn-quickview" href="#" data-link-action="quickview">
                                      <i class="pe-7s-search"></i>
                                    </a>
                                <?php
}
}
/* {/block 'quick_view'} */
/* {block 'product_name'} */
class Block_15348101525cfa6b16873266_96352620 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <h3 class="h3 product-title" itemprop="name"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['name'],30,'...' )), ENT_QUOTES, 'UTF-8');?>
</a></h3>
        <?php
}
}
/* {/block 'product_name'} */
/* {block 'product_price_and_shipping'} */
class Block_19972999475cfa6b16879b75_74676606 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php if ($_smarty_tpl->tpl_vars['product']->value['show_price']) {?>
            <div class="product-price-and-shipping product-price">
              <?php if ($_smarty_tpl->tpl_vars['product']->value['has_discount']) {?>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductPriceBlock','product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"old_price"),$_smarty_tpl ) );?>


                <span class="regular-price"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['regular_price'], ENT_QUOTES, 'UTF-8');?>
</span>
                              <?php }?>

              <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductPriceBlock','product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"before_price"),$_smarty_tpl ) );?>


              <span itemprop="price" class="price <?php if ($_smarty_tpl->tpl_vars['product']->value['has_discount']) {?>price-sale<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price'], ENT_QUOTES, 'UTF-8');?>
</span>

              <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductPriceBlock','product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>'unit_price'),$_smarty_tpl ) );?>


            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductPriceBlock','product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>'weight'),$_smarty_tpl ) );?>

          </div>
        <?php }?>
      <?php
}
}
/* {/block 'product_price_and_shipping'} */
/* {block 'product_reviews'} */
class Block_18576852615cfa6b16895f59_35756900 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductListReviews','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>

      <?php
}
}
/* {/block 'product_reviews'} */
/* {block 'product_variants'} */
class Block_3103641025cfa6b1689a9b3_26705963 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php if ($_smarty_tpl->tpl_vars['product']->value['main_variants']) {?>
          <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/variant-links.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('variants'=>$_smarty_tpl->tpl_vars['product']->value['main_variants']), 0, false);
?>
        <?php }?>
      <?php
}
}
/* {/block 'product_variants'} */
/* {block 'product_miniature_item'} */
class Block_4318639905cfa6b16837161_43857847 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_miniature_item' => 
  array (
    0 => 'Block_4318639905cfa6b16837161_43857847',
  ),
  'product_thumbnail' => 
  array (
    0 => 'Block_2941332635cfa6b1683ab32_75709699',
  ),
  'product_flags' => 
  array (
    0 => 'Block_2191713265cfa6b16848e33_45092694',
  ),
  'quick_view' => 
  array (
    0 => 'Block_20511216515cfa6b1686cb39_13960846',
  ),
  'product_name' => 
  array (
    0 => 'Block_15348101525cfa6b16873266_96352620',
  ),
  'product_price_and_shipping' => 
  array (
    0 => 'Block_19972999475cfa6b16879b75_74676606',
  ),
  'product_reviews' => 
  array (
    0 => 'Block_18576852615cfa6b16895f59_35756900',
  ),
  'product_variants' => 
  array (
    0 => 'Block_3103641025cfa6b1689a9b3_26705963',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <article class="product-miniature js-product-miniature col-md-3 col-sm-4 col-xs-12 mt__30" data-id-product="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8');?>
" data-id-product-attribute="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product_attribute'], ENT_QUOTES, 'UTF-8');?>
" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container product-inner">
        <div class="product-image">
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2941332635cfa6b1683ab32_75709699', 'product_thumbnail', $this->tplIndex);
?>

              <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2191713265cfa6b16848e33_45092694', 'product_flags', $this->tplIndex);
?>

              
              <div class="product-item-control">
                  <div class="product-item-action<?php if (isset($_smarty_tpl->tpl_vars['enb_quickview_onlist']->value) && $_smarty_tpl->tpl_vars['enb_quickview_onlist']->value == 0) {?> no_quickview_product<?php }?>">
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductListFunctionalButtons','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>

                        <form action="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['cart'], ENT_QUOTES, 'UTF-8');
}?>" method="post">
                            <div class="pg_attr_custom"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAttributeList','id_product'=>$_smarty_tpl->tpl_vars['product']->value['id_product']),$_smarty_tpl ) );?>
</div>
                            <div class="group_addcart_quickshop">
                                <?php if ($_smarty_tpl->tpl_vars['product']->value['attributes']) {?>
                                    <span class="pg_button_quickshop product-item-cart"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Quick Shop','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</span>
                                <?php }?>
                                <input type="hidden" name="token" value="<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['static_token']->value, ENT_QUOTES, 'UTF-8');
}?>" />
                                <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8');?>
" name="id_product" />
                                <input type="hidden" class="input-group form-control" name="qty" value="1">
                                <button data-button-action="add-to-cart" class="product-item-cart"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</button>
                                
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['enb_quickview_onlist']->value) && $_smarty_tpl->tpl_vars['enb_quickview_onlist']->value == 1) {?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20511216515cfa6b1686cb39_13960846', 'quick_view', $this->tplIndex);
?>

                            <?php }?>
                        </form>
                                  
                  </div>
              </div>
              <?php if ($_smarty_tpl->tpl_vars['product']->value['attributes']) {?>
                  <div class="pg_attr_quickshop">
                    <span class="close_quickshop"><i class="pe-7s-close"></i></span>
                    <form action="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['cart'], ENT_QUOTES, 'UTF-8');
}?>" method="post">
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAttributeList','id_product'=>$_smarty_tpl->tpl_vars['product']->value['id_product']),$_smarty_tpl ) );?>

                        <input type="hidden" name="token" value="<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['static_token']->value, ENT_QUOTES, 'UTF-8');
}?>" />
                        <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8');?>
" name="id_product" />
                        <input type="hidden" class="input-group form-control" name="qty" value="1">
                        <button data-button-action="add-to-cart" class="product-item-cart"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</button>
                    </form>
                  </div>
              <?php }?>
        </div>
      

      <div class="product-description product-infor">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15348101525cfa6b16873266_96352620', 'product_name', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19972999475cfa6b16879b75_74676606', 'product_price_and_shipping', $this->tplIndex);
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18576852615cfa6b16895f59_35756900', 'product_reviews', $this->tplIndex);
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3103641025cfa6b1689a9b3_26705963', 'product_variants', $this->tplIndex);
?>

    </div>

    
    </div>
  </article>
<?php
}
}
/* {/block 'product_miniature_item'} */
}
