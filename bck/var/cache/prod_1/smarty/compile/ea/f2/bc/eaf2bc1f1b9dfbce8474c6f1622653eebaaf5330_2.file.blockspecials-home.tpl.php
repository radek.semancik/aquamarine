<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:10
  from 'C:\Work\pixape\pragaglobal\themes\claue\modules\tea_blockspecials\views\templates\hook\blockspecials-home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b1aa6e9c4_64909459',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eaf2bc1f1b9dfbce8474c6f1622653eebaaf5330' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\modules\\tea_blockspecials\\views\\templates\\hook\\blockspecials-home.tpl',
      1 => 1559309872,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b1aa6e9c4_64909459 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
if ($_smarty_tpl->tpl_vars['products']->value) {?>
    <div class="flex">
        <div class="panel-discount owl-carousel" data-mh="special" data-carousel='{"items": 1, "loop": true, "center": false, "margin": 0, "autoWidth": false, "rtl": false, "autoHeight": false, "autoplay": true, "autoplayTimeout": 5000, "nav": true, "dots": false}'>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products2']->value, 'product');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>
              <article class="panel-discount-item product-miniature js-product-miniature">
                <div class="thumbnail-container product-inner">
                    <div class="panel-discount-thumbnail">
                      <span class="sale-off"><?php if ($_smarty_tpl->tpl_vars['product']->value['discount_type'] != 'amount') {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['discount_percentage'], ENT_QUOTES, 'UTF-8');
} else { ?>-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['discount_amount'], ENT_QUOTES, 'UTF-8');
}?></span>
                      <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8');?>
">   
                        <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'home_default'),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" width="370" height="466" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8');?>
"/>
                      </a>
                    </div>
                    <div class="panel-discount-content">
                                            <?php if ($_smarty_tpl->tpl_vars['product']->value['specific_prices']['to'] != '0000-00-00 00:00:00') {?>
                        <div class="panel-discount-countdown" data-countdown="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['specific_prices']['to'], ENT_QUOTES, 'UTF-8');?>
"></div>
                      <?php }?>
                      <h3 class="panel-discount-name">
                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a>
                      </h3>
                      <div class="panel-discount-review" data-rating='{"score" : 1.2, "readOnly": true}'></div>
                                            <p class="panel-discount-price">
                          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"old_price"),$_smarty_tpl ) );?>

                          <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['regular_price'], ENT_QUOTES, 'UTF-8');?>
</span> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price'], ENT_QUOTES, 'UTF-8');?>

                      </p>
                    </div>
                    <div class="product-item-control">
                      <div class="product-item-action">
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductListFunctionalButtons','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>

                            <form action="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['cart'], ENT_QUOTES, 'UTF-8');
}?>" method="post">
                                <div class="group_addcart_quickshop">
                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['main_variants']) {?>
                                        <span class="pg_button_quickshop product-item-cart"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Quick Shop','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</span>
                                    <?php }?>
                                    <input type="hidden" name="token" value="<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['static_token']->value, ENT_QUOTES, 'UTF-8');
}?>" />
                                    <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8');?>
" name="id_product" />
                                    <input type="hidden" class="input-group form-control" name="qty" value="1">
                                    <button data-button-action="add-to-cart" class="product-item-cart"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</button>
                                </div>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9396936195cfa6b1aa67349_88438418', 'quick_view');
?>

                            </form>
                                      
                      </div>
                  </div>
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['main_variants']) {?>
                      <div class="pg_attr_quickshop">
                        <span class="close_quickshop"><i class="pe-7s-close"></i></span>
                        <form action="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['cart'], ENT_QUOTES, 'UTF-8');
}?>" method="post">
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAttributeList','id_product'=>$_smarty_tpl->tpl_vars['product']->value['id_product']),$_smarty_tpl ) );?>

                            <input type="hidden" name="token" value="<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['static_token']->value, ENT_QUOTES, 'UTF-8');
}?>" />
                            <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8');?>
" name="id_product" />
                            <input type="hidden" class="input-group form-control" name="qty" value="1">
                            <button data-button-action="add-to-cart" class="product-item-cart"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</button>
                        </form>
                      </div>
                  <?php }?>
                </div>
              </article>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      </div>
    </div>
<?php }
}
/* {block 'quick_view'} */
class Block_9396936195cfa6b1aa67349_88438418 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'quick_view' => 
  array (
    0 => 'Block_9396936195cfa6b1aa67349_88438418',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <a class="quick-view btn-quickview" href="#" data-link-action="quickview">
                                      <i class="pe-7s-search"></i>
                                    </a>
                                  <?php
}
}
/* {/block 'quick_view'} */
}
