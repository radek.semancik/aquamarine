<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:30
  from 'C:\Work\pixape\pragaglobal\modules\tea_productextra\views\templates\hook\productTab.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b2e2c1f43_46456289',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b2a18f2deff1f9e479ac721b4886e0059e1ae51a' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\modules\\tea_productextra\\views\\templates\\hook\\productTab.tpl',
      1 => 1559309867,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b2e2c1f43_46456289 (Smarty_Internal_Template $_smarty_tpl) {
?><input  type="hidden" name="id_product_extra"/>
<div class="form-group">
    <label class="control-label col-lg-4">
        <span class="label-tooltip" title="" > <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Display size giude in product page','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
 </span>
    </label>
    <div class="col-lg-8">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" <?php if ($_smarty_tpl->tpl_vars['is_size_guide']->value) {?> checked="checked" <?php }?> value="1" id="display_sizeguide_on" name="display_sizeguide" />
			<label for="display_sizeguide_on"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Yes','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
</label>
			<input type="radio" <?php if (!$_smarty_tpl->tpl_vars['is_size_guide']->value) {?> checked="checked" <?php }?> value="0" id="display_sizeguide_off" name="display_sizeguide" />
			<label for="display_sizeguide_off"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
</label>
			<a class="slide-button btn"></a>
		</span>								
	</div>
</div>
<div class="form-group tab_size_giude">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" title="" > <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Size guide content','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
 </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
                <div class="col-lg-9">
    			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
?>
                	<?php if (count($_smarty_tpl->tpl_vars['languages']->value) > 1) {?>
                		<div class="translatable-field row lang-<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
">
                			<div class="col-lg-9">
                	<?php }?>
                	<?php if (isset($_smarty_tpl->tpl_vars['maxchar']->value) && $_smarty_tpl->tpl_vars['maxchar']->value) {?>
        				<div class="input-group">
        					<span id="size_guide_content_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" class="input-group-addon">
      						<span class="text-count-down"><?php echo intval($_smarty_tpl->tpl_vars['maxchar']->value);?>
</span>
        					</span>
                	<?php }?>
        			 <textarea id="size_guide_content_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" name="size_guide_content_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" class="<?php if (isset($_smarty_tpl->tpl_vars['class']->value)) {
echo $_smarty_tpl->tpl_vars['class']->value;
} else { ?>textarea-autosize<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['maxlength']->value) && $_smarty_tpl->tpl_vars['maxlength']->value) {?> maxlength="<?php echo intval($_smarty_tpl->tpl_vars['maxlength']->value);?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['maxchar']->value) && $_smarty_tpl->tpl_vars['maxchar']->value) {?> data-maxchar="<?php echo intval($_smarty_tpl->tpl_vars['maxchar']->value);?>
"<?php }?>><?php if (isset($_smarty_tpl->tpl_vars['input_value']->value['size_guide_content'][$_smarty_tpl->tpl_vars['language']->value['id_lang']])) {
echo smarty_modifier_htmlentitiesUTF8($_smarty_tpl->tpl_vars['input_value']->value['size_guide_content'][$_smarty_tpl->tpl_vars['language']->value['id_lang']]);
}?></textarea>
        			 <span class="counter" data-max="<?php if (isset($_smarty_tpl->tpl_vars['max']->value)) {
echo intval($_smarty_tpl->tpl_vars['max']->value);
}
if (isset($_smarty_tpl->tpl_vars['maxlength']->value)) {
echo intval($_smarty_tpl->tpl_vars['maxlength']->value);
}
if (!isset($_smarty_tpl->tpl_vars['max']->value) && !isset($_smarty_tpl->tpl_vars['maxlength']->value)) {?>none<?php }?>"></span>
                    <?php if (isset($_smarty_tpl->tpl_vars['maxchar']->value) && $_smarty_tpl->tpl_vars['maxchar']->value) {?>
                    </div>
                    <?php }?>
                	<?php if (count($_smarty_tpl->tpl_vars['languages']->value) > 1) {?>
                			</div>
                			<div class="col-lg-2">
                				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                					<?php echo $_smarty_tpl->tpl_vars['language']->value['iso_code'];?>

                					<span class="caret"></span>
                				</button>
                				<ul class="dropdown-menu">
                					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
?>
                					<li><a href="javascript:hideOtherLanguage(<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
);"><?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
</a></li>
                					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                				</ul>
                			</div>
                		</div>
                	<?php }?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>   
            </div>        
    		</span>								
    </div>
</div>
<div class="form-group">
    <label class="control-label col-lg-4">
        <span class="label-tooltip" title="" > <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Display video in product page','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
 </span>
    </label>
    <div class="col-lg-8">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" <?php if ($_smarty_tpl->tpl_vars['is_video']->value) {?> checked="checked" <?php }?> value="1" id="display_video_on" name="display_video" />
			<label for="display_video_on"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Yes','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
</label>
			<input type="radio" <?php if (!$_smarty_tpl->tpl_vars['is_video']->value) {?> checked="checked" <?php }?> value="0" id="display_video_off" name="display_video" />
			<label for="display_video_off"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
</label>
			<a class="slide-button btn"></a>
		</span>								
	</div>
</div>
<div class="form-group tab_video">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" title="" > <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Video ID','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
 </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <div class="col-lg-9">
    			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
?>
                	<?php if (count($_smarty_tpl->tpl_vars['languages']->value) > 1) {?>
                		<div class="translatable-field row lang-<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
">
                			<div class="col-lg-9">
                	<?php }?>
                	<?php if (isset($_smarty_tpl->tpl_vars['maxchar']->value) && $_smarty_tpl->tpl_vars['maxchar']->value) {?>
        				<div class="input-group">
        					<span id="product_video_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" class="input-group-addon">
      						<span class="text-count-down"><?php echo intval($_smarty_tpl->tpl_vars['maxchar']->value);?>
</span>
        					</span>
                	<?php }?>
                    <input type="text" id="product_video_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" name="product_video_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
"  value="<?php if (isset($_smarty_tpl->tpl_vars['input_value']->value['video'][$_smarty_tpl->tpl_vars['language']->value['id_lang']])) {
echo smarty_modifier_htmlentitiesUTF8($_smarty_tpl->tpl_vars['input_value']->value['video'][$_smarty_tpl->tpl_vars['language']->value['id_lang']]);
}?>" style="border: 1px solid rgb(204, 204, 204); width: 352px; height: 35px;"/>
        			 <span class="counter" data-max="<?php if (isset($_smarty_tpl->tpl_vars['max']->value)) {
echo intval($_smarty_tpl->tpl_vars['max']->value);
}
if (isset($_smarty_tpl->tpl_vars['maxlength']->value)) {
echo intval($_smarty_tpl->tpl_vars['maxlength']->value);
}
if (!isset($_smarty_tpl->tpl_vars['max']->value) && !isset($_smarty_tpl->tpl_vars['maxlength']->value)) {?>none<?php }?>"></span>
                    <?php if (isset($_smarty_tpl->tpl_vars['maxchar']->value) && $_smarty_tpl->tpl_vars['maxchar']->value) {?>
                    </div>
                    <?php }?>
                	<?php if (count($_smarty_tpl->tpl_vars['languages']->value) > 1) {?>
                			</div>
                			<div class="col-lg-2">
                				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                					<?php echo $_smarty_tpl->tpl_vars['language']->value['iso_code'];?>

                					<span class="caret"></span>
                				</button>
                				<ul class="dropdown-menu">
                					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
?>
                					<li><a href="javascript:hideOtherLanguage(<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
);"><?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
</a></li>
                					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                				</ul>
                			</div>
                		</div>
                	<?php }?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> 
             </div>          
  		</span>								
    </div>
</div>
<div class="form-group">
    <label class="control-label col-lg-4">
        <span class="label-tooltip" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'External/Affiliate product','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
 " > <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'External/Affiliate product','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
 </span>
    </label>
    <div class="col-lg-8">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" <?php if ($_smarty_tpl->tpl_vars['is_external_affiliate_product']->value) {?> checked="checked" <?php }?> value="1" id="external_affiliate_product_on" name="external_affiliate_product" />
			<label for="external_affiliate_product_on"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Yes','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
</label>
			<input type="radio" <?php if (!$_smarty_tpl->tpl_vars['is_external_affiliate_product']->value) {?> checked="checked" <?php }?> value="0" id="external_affiliate_product_off" name="external_affiliate_product" />
			<label for="external_affiliate_product_off"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
</label>
			<a class="slide-button btn"></a>
		</span>								
	</div>
</div>
<div class="form-group tab_external">
    <label class="control-label col-lg-3">
        <span class="label-tooltip" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Title external','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
" > <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Title external','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
 </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <div class="col-lg-9">
    			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
?>
                	<?php if (count($_smarty_tpl->tpl_vars['languages']->value) > 1) {?>
                		<div class="translatable-field row lang-<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
">
                			<div class="col-lg-9">
                	<?php }?>
                	<?php if (isset($_smarty_tpl->tpl_vars['maxchar']->value) && $_smarty_tpl->tpl_vars['maxchar']->value) {?>
        				<div class="input-group">
        					<span id="title_external_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" class="input-group-addon">
      						<span class="text-count-down"><?php echo intval($_smarty_tpl->tpl_vars['maxchar']->value);?>
</span>
        					</span>
                	<?php }?>
                    <input type="text" id="title_external_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" name="title_external_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
"  value="<?php if (isset($_smarty_tpl->tpl_vars['input_value']->value['title_external'][$_smarty_tpl->tpl_vars['language']->value['id_lang']])) {
echo smarty_modifier_htmlentitiesUTF8($_smarty_tpl->tpl_vars['input_value']->value['title_external'][$_smarty_tpl->tpl_vars['language']->value['id_lang']]);
}?>" style="border: 1px solid rgb(204, 204, 204); width: 352px; height: 35px;"/>
        			 <span class="counter" data-max="<?php if (isset($_smarty_tpl->tpl_vars['max']->value)) {
echo intval($_smarty_tpl->tpl_vars['max']->value);
}
if (isset($_smarty_tpl->tpl_vars['maxlength']->value)) {
echo intval($_smarty_tpl->tpl_vars['maxlength']->value);
}
if (!isset($_smarty_tpl->tpl_vars['max']->value) && !isset($_smarty_tpl->tpl_vars['maxlength']->value)) {?>none<?php }?>"></span>
                    <?php if (isset($_smarty_tpl->tpl_vars['maxchar']->value) && $_smarty_tpl->tpl_vars['maxchar']->value) {?>
                    </div>
                    <?php }?>
                	<?php if (count($_smarty_tpl->tpl_vars['languages']->value) > 1) {?>
                			</div>
                			<div class="col-lg-2">
                				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                					<?php echo $_smarty_tpl->tpl_vars['language']->value['iso_code'];?>

                					<span class="caret"></span>
                				</button>
                				<ul class="dropdown-menu">
                					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
?>
                					<li><a href="javascript:hideOtherLanguage(<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
);"><?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
</a></li>
                					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                				</ul>
                			</div>
                		</div>
                	<?php }?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> 
             </div>          
  		</span>								
    </div>
</div>
<div class="form-group tab_external">
    <label class="control-label col-lg-3" for="link_external">
        <span class="label-tooltip" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'External/Affiliate product link','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
 " > <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'External/Affiliate product link','mod'=>'tea_productsextra'),$_smarty_tpl ) );?>
 </span>
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <div class="col-lg-9">
                <input type="text" name="link_external" id="link_external" value="<?php echo $_smarty_tpl->tpl_vars['link_external']->value;?>
" style="border: 1px solid rgb(204, 204, 204); width: 352px; height: 35px;" />
            </div>
		</span>								
	</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
hideOtherLanguage(<?php echo Configuration::get('PS_LANG_DEFAULT');?>
);
$(document).ready(function(){
   if($('input[name="display_sizeguide"]:checked').val()==1)
   {
        $('.form-group.tab_size_giude').show();
   }
   else
   {
        $('.form-group.tab_size_giude').hide();
   }
   $(document).on('click','input[name="display_sizeguide"]',function(){
        if($('input[name="display_sizeguide"]:checked').val()==1)
        {
            $('.form-group.tab_size_giude').show();
        }
        else
        {
            $('.form-group.tab_size_giude').hide();
        }
   });
   if($('input[name="display_video"]:checked').val()==1)
   {
        $('.form-group.tab_video').show();
   }
   else
   {
        $('.form-group.tab_video').hide();
   }
   $(document).on('click','input[name="display_video"]',function(){
        if($('input[name="display_video"]:checked').val()==1)
        {
            $('.form-group.tab_video').show();
        }
        else
        {
            $('.form-group.tab_video').hide();
        }
   }); 
   if($('input[name="external_affiliate_product"]:checked').val()==1)
   {
        $('.form-group.tab_external').show();
   }
   else
   {
        $('.form-group.tab_external').hide();
   }
   $(document).on('click','input[name="external_affiliate_product"]',function(){
        if($('input[name="external_affiliate_product"]:checked').val()==1)
        {
            $('.form-group.tab_external').show();
        }
        else
        {
            $('.form-group.tab_external').hide();
        }
   });
});
<?php echo '</script'; ?>
>
<style>
#module_tea_productextra .form-group {
  float: left;
  width: 100%;
}
</style><?php }
}
