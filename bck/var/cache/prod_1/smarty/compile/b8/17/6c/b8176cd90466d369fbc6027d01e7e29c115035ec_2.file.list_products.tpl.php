<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:10
  from 'C:\Work\pixape\pragaglobal\themes\claue\modules\tea_producttabhome\views\templates\hook\list_products.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b1aeffbb0_39038290',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b8176cd90466d369fbc6027d01e7e29c115035ec' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\modules\\tea_producttabhome\\views\\templates\\hook\\list_products.tpl',
      1 => 1559309873,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/miniatures/product.tpl' => 1,
  ),
),false)) {
function content_5cfa6b1aeffbb0_39038290 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['products']->value) {?>

  <div class="product_carousel owl-carousel">

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products']->value, 'product');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>

      <?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>

    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

  </div>

<?php } else { ?>

<p>

    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No product','mod'=>'tea_producttabhome'),$_smarty_tpl ) );?>


</p>

<?php }
}
}
