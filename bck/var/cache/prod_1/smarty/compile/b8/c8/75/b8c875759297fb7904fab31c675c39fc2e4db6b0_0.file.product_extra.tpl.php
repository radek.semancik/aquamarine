<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:30
  from 'C:\Work\pixape\pragaglobal\modules\pg_productbundles\views\templates\hook\product_extra.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b2e1fe712_41679097',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b8c875759297fb7904fab31c675c39fc2e4db6b0' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\modules\\pg_productbundles\\views\\templates\\hook\\product_extra.tpl',
      1 => 1559309861,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b2e1fe712_41679097 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
var url_search_product_ajax ='<?php echo $_smarty_tpl->tpl_vars['url_search_product_ajax']->value;?>
';
var PG_PB_MODULE_URL_AJAX= '<?php echo $_smarty_tpl->tpl_vars['PG_PB_MODULE_URL_AJAX']->value;?>
';
<?php echo '</script'; ?>
>
<div class="bundles-list">
    <div class="proudct_bundles">
        <ul class="products">
             <?php if ($_smarty_tpl->tpl_vars['bundles_products']->value) {?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['bundles_products']->value, 'product');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>
                    <li data-id-productbundles="<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_pg_productbundles']);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['product']->value['url_image'];?>
" /> <?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
-<?php echo $_smarty_tpl->tpl_vars['product']->value['attributes'];?>
 (<?php echo $_smarty_tpl->tpl_vars['product']->value['proudct_bundles_discount'];?>
%) <span class="delete"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Delete','mod'=>'pg_productbundles'),$_smarty_tpl ) );?>
</span></li>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             <?php }?>
        </ul>
    </div>
    <div class="form-group">
        <input type ="hidden" name="id_product_bundles" id="id_product_bundles" value="0"/>
        <input type ="hidden" name="id_product_attribute_bundles" id="id_product_attribute_bundles" value="0"/>
        <input type="text" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search product','mod'=>'pg_productbundles'),$_smarty_tpl ) );?>
" class="search_product_bundles"/>
        <input type="text" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'discount (%)','mod'=>'pg_productbundles'),$_smarty_tpl ) );?>
" name="proudct_bundles_discount"/>
        <button type="button" class="add_proudct_bundles"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add','mod'=>'pg_productbundles'),$_smarty_tpl ) );?>
</button>
    </div>
    <div class="product-resuts">
        <ul class="products"> 
        </ul>
    </div>
</div>
<?php }
}
