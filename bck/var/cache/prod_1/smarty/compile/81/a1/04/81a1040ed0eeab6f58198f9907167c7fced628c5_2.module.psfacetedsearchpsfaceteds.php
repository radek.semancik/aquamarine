<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:06
  from 'module:psfacetedsearchpsfaceteds' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b16d4da56_58013748',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '81a1040ed0eeab6f58198f9907167c7fced628c5' => 
    array (
      0 => 'module:psfacetedsearchpsfaceteds',
      1 => 1559320145,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b16d4da56_58013748 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['listing']->value['rendered_facets'])) {?>

    <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
        <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_facets'];?>

    <?php } else { ?>

        <div id="search_filters_wrapper" class="">
            <div id="search_filter_controls" class="hidden-md-up search_filter_controls_clear_all">
                <span id="_mobile_search_filters_clear_all"></span>
            </div>
            <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_facets'];?>

        </div>

    <?php }?>  
<?php }?>




<?php }
}
