<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:12
  from 'C:\Work\pixape\pragaglobal\themes\claue\modules\tea_instagram\views\templates\hook\tea-instagram.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b1c5e6452_04676334',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '692865fde3c5300377643c7228ef07e34b0ecde0' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\modules\\tea_instagram\\views\\templates\\hook\\tea-instagram.tpl',
      1 => 1559309873,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b1c5e6452_04676334 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="panel-instagram pt__85">
    <input type="hidden" id="proistclimit" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['proistclimit']->value, ENT_QUOTES, 'UTF-8');?>
" />
    <input type="hidden" id="proistcid" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['proistcid']->value, ENT_QUOTES, 'UTF-8');?>
" />
    <input type="hidden" id="proistctoken" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['proistctoken']->value, ENT_QUOTES, 'UTF-8');?>
" />
    <h3 class="section-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'@ FOLLOW US ON INSTAGRAM','mod'=>'tea_instagram'),$_smarty_tpl ) );?>
</h3>
    <div class="instagram_content">
        <div id="sb_instagram" class="sbi sbi_fixed_height sbi_col_6 image-instagram" ></div>
    </div>
</div><?php }
}
