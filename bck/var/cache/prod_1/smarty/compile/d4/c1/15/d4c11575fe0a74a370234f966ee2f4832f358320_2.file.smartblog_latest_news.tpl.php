<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:12
  from 'C:\Work\pixape\pragaglobal\themes\claue\modules\smartbloghomelatestnews\views\templates\front\smartblog_latest_news.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b1c58a484_79427225',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd4c11575fe0a74a370234f966ee2f4832f358320' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\modules\\smartbloghomelatestnews\\views\\templates\\front\\smartblog_latest_news.tpl',
      1 => 1559309872,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b1c58a484_79427225 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="row">
    <div class="sdsblog-box-content">
        <?php if (isset($_smarty_tpl->tpl_vars['view_data']->value) && !empty($_smarty_tpl->tpl_vars['view_data']->value)) {?>
            <?php $_smarty_tpl->_assignInScope('i', 1);?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['view_data']->value, 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
                    <?php $_smarty_tpl->_assignInScope('options', null);?>
                    <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['id_post'] = $_smarty_tpl->tpl_vars['post']->value['id'];
$_smarty_tpl->_assignInScope('options', $_tmp_array);?>
                    <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['slug'] = $_smarty_tpl->tpl_vars['post']->value['link_rewrite'];
$_smarty_tpl->_assignInScope('options', $_tmp_array);?>
                    <div class="sds_blog_post col-md-4 col-sm-4 col-xs-12">
                        <div class="panel-news-item">
                            <div class="blog-image">
                                <div class="blog-image-flip">
                                    <a class="panel-news-media" href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_post',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
">
                                    <img alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['title'], ENT_QUOTES, 'UTF-8');?>
" class="feat_img_small" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8');?>
smartblog/images/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['post_img'], ENT_QUOTES, 'UTF-8');?>
-home-default.jpg" /></a>
                                </div>
                            </div> 
                            <?php $_smarty_tpl->_assignInScope('date_format', 'F j, Y');?>
                            <div class="blog-info">
                                <h4 class="sds_post_title panel-news-title">
                                    <a class="panel-news-link" href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_post',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
">
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['title'], ENT_QUOTES, 'UTF-8');?>

                                    </a>
                                </h4>
                                <span class="post-author">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'By','mod'=>'smartbloghomelatestnews'),$_smarty_tpl ) );?>
 coder <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>' on ','mod'=>'smartbloghomelatestnews'),$_smarty_tpl ) );?>
 <span class="post-time"> <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( date($_smarty_tpl->tpl_vars['date_format']->value,strtotime($_smarty_tpl->tpl_vars['post']->value['date_added'])),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 </span>
                                </span>
                                <p class="panel-news-summary">
                                    <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['short_description'],150,"...",true )),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                                </p>
                            </div>
                        </div>
                    </div>
                <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <?php }?>
     </div>
</div><?php }
}
