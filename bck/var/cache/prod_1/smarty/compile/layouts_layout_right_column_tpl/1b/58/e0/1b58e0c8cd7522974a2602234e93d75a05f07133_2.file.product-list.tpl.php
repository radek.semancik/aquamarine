<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:05
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\catalog\listing\product-list.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b15ad4ca4_19829448',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1b58e0c8cd7522974a2602234e93d75a05f07133' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\catalog\\listing\\product-list.tpl',
      1 => 1559309873,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/products-top.tpl' => 5,
    'file:catalog/_partials/products.tpl' => 5,
    'file:catalog/_partials/products-bottom.tpl' => 5,
    'file:errors/not-found.tpl' => 5,
  ),
),false)) {
function content_5cfa6b15ad4ca4_19829448 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16051497605cfa6b15a45977_10822310', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'product_list_top'} */
class Block_18378723525cfa6b15a570a9_35293082 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, false);
?>
                      <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_8745725855cfa6b15a682c6_14267929 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                      <div class="hidden-sm-down">
                        <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

                      </div>
                    <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_1583874915cfa6b15a6d376_70496339 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, false);
?>
                      <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_1402784965cfa6b15a71c60_93496760 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, false);
?>
                      <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'product_list_top'} */
class Block_1465268195cfa6b15a831c9_55820982 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_21232469405cfa6b15a889b4_95051007 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                      <div class="hidden-sm-down">
                        <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

                      </div>
                    <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_9239073105cfa6b15a8d166_81915270 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_3575922155cfa6b15a91c04_12428950 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'product_list_top'} */
class Block_2855546285cfa6b15a9ba82_95506635 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_9318838085cfa6b15aa0736_30209152 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                      <div class="hidden-sm-down">
                        <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

                      </div>
                    <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_6573392625cfa6b15aa5541_01502561 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_19566411095cfa6b15aa9e94_20752870 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                      <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'product_list_top'} */
class Block_17393850455cfa6b15abedd7_25389003 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_9711112935cfa6b15ac1bc8_04608520 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                  <div class="hidden-sm-down">
                    <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

                  </div>
                <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_18546725245cfa6b15ac6583_47837905 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_6661041955cfa6b15ac7e43_30571667 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'product_list_top'} */
class Block_4730563605cfa6b15acdb26_11116464 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_5197311765cfa6b15acf122_72866392 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                  <div class="hidden-sm-down">
                    <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

                  </div>
                <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_2398255515cfa6b15ad07c1_30539341 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_9115488445cfa6b15ad1d18_73740834 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'content'} */
class Block_16051497605cfa6b15a45977_10822310 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_16051497605cfa6b15a45977_10822310',
  ),
  'product_list_top' => 
  array (
    0 => 'Block_18378723525cfa6b15a570a9_35293082',
    1 => 'Block_1465268195cfa6b15a831c9_55820982',
    2 => 'Block_2855546285cfa6b15a9ba82_95506635',
    3 => 'Block_17393850455cfa6b15abedd7_25389003',
    4 => 'Block_4730563605cfa6b15acdb26_11116464',
  ),
  'product_list_active_filters' => 
  array (
    0 => 'Block_8745725855cfa6b15a682c6_14267929',
    1 => 'Block_21232469405cfa6b15a889b4_95051007',
    2 => 'Block_9318838085cfa6b15aa0736_30209152',
    3 => 'Block_9711112935cfa6b15ac1bc8_04608520',
    4 => 'Block_5197311765cfa6b15acf122_72866392',
  ),
  'product_list' => 
  array (
    0 => 'Block_1583874915cfa6b15a6d376_70496339',
    1 => 'Block_9239073105cfa6b15a8d166_81915270',
    2 => 'Block_6573392625cfa6b15aa5541_01502561',
    3 => 'Block_18546725245cfa6b15ac6583_47837905',
    4 => 'Block_2398255515cfa6b15ad07c1_30539341',
  ),
  'product_list_bottom' => 
  array (
    0 => 'Block_1402784965cfa6b15a71c60_93496760',
    1 => 'Block_3575922155cfa6b15a91c04_12428950',
    2 => 'Block_19566411095cfa6b15aa9e94_20752870',
    3 => 'Block_6661041955cfa6b15ac7e43_30571667',
    4 => 'Block_9115488445cfa6b15ad1d18_73740834',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div id="main">

        <?php if ((isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value)) {?> 
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
            <?php if (isset($_smarty_tpl->tpl_vars['category']->value['id']) && $_smarty_tpl->tpl_vars['category']->value['id'] == 3) {?>
                <div id="products" class="product_list_metro pg_list_masonry">
                    <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>
                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18378723525cfa6b15a570a9_35293082', 'product_list_top', $this->tplIndex);
?>

                    </div>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8745725855cfa6b15a682c6_14267929', 'product_list_active_filters', $this->tplIndex);
?>

                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1583874915cfa6b15a6d376_70496339', 'product_list', $this->tplIndex);
?>

                    </div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1402784965cfa6b15a71c60_93496760', 'product_list_bottom', $this->tplIndex);
?>

                  <?php } else { ?>
                    <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                  <?php }?>
                </div>
            <?php } elseif (isset($_smarty_tpl->tpl_vars['category']->value['id']) && $_smarty_tpl->tpl_vars['category']->value['id'] == 4) {?>
                <div id="products" class="pg_list_masonry product_list_masonry">
                <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>
                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1465268195cfa6b15a831c9_55820982', 'product_list_top', $this->tplIndex);
?>

                    </div>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21232469405cfa6b15a889b4_95051007', 'product_list_active_filters', $this->tplIndex);
?>

                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9239073105cfa6b15a8d166_81915270', 'product_list', $this->tplIndex);
?>

                    </div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3575922155cfa6b15a91c04_12428950', 'product_list_bottom', $this->tplIndex);
?>

                  <?php } else { ?>
                    <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
                  <?php }?>
                </div>
            <?php } else { ?>
                <div id="products" class="">
                <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>
                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2855546285cfa6b15a9ba82_95506635', 'product_list_top', $this->tplIndex);
?>

                    </div>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9318838085cfa6b15aa0736_30209152', 'product_list_active_filters', $this->tplIndex);
?>

                    <div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6573392625cfa6b15aa5541_01502561', 'product_list', $this->tplIndex);
?>

                    </div>
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19566411095cfa6b15aa9e94_20752870', 'product_list_bottom', $this->tplIndex);
?>

                  <?php } else { ?>
                    <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
                  <?php }?>
                </div>
            <?php }?>
        <?php } else { ?>             <div id="products" class="<?php if (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_metro') {?>product_list_metro pg_list_masonry<?php } elseif (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_masonry') {?> pg_list_masonry product_list_masonry<?php } else { ?> product_list_default<?php }?>">
            <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>
                <div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17393850455cfa6b15abedd7_25389003', 'product_list_top', $this->tplIndex);
?>

                </div>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9711112935cfa6b15ac1bc8_04608520', 'product_list_active_filters', $this->tplIndex);
?>

                <div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18546725245cfa6b15ac6583_47837905', 'product_list', $this->tplIndex);
?>

                </div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6661041955cfa6b15ac7e43_30571667', 'product_list_bottom', $this->tplIndex);
?>

              <?php } else { ?>
                <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
              <?php }?>
            </div>
        <?php }?>
    <?php } else { ?>
        <div id="products" class="<?php if (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_metro') {?>product_list_metro pg_list_masonry<?php } elseif (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_masonry') {?> pg_list_masonry product_list_masonry<?php } else { ?> product_list_default<?php }?>">
            <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>
                <div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4730563605cfa6b15acdb26_11116464', 'product_list_top', $this->tplIndex);
?>

                </div>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5197311765cfa6b15acf122_72866392', 'product_list_active_filters', $this->tplIndex);
?>

                <div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2398255515cfa6b15ad07c1_30539341', 'product_list', $this->tplIndex);
?>

                </div>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9115488445cfa6b15ad1d18_73740834', 'product_list_bottom', $this->tplIndex);
?>

              <?php } else { ?>
                <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
              <?php }?>
            </div>
        <?php }?>
  </div>
<?php
}
}
/* {/block 'content'} */
}
