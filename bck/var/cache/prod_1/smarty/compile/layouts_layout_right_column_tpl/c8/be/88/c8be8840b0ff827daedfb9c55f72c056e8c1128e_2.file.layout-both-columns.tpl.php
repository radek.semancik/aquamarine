<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:05
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\layouts\layout-both-columns.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b15beb433_11665377',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c8be8840b0ff827daedfb9c55f72c056e8c1128e' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\layouts\\layout-both-columns.tpl',
      1 => 1559828411,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/head.tpl' => 1,
    'file:_partials/javascript.tpl' => 1,
    'file:catalog/_partials/product-activation.tpl' => 1,
    'file:_partials/header.tpl' => 1,
    'file:_partials/notifications.tpl' => 1,
    'file:_partials/breadcrumb.tpl' => 1,
    'file:_partials/footer.tpl' => 1,
  ),
),false)) {
function content_5cfa6b15beb433_11665377 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

  <head>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_219985015cfa6b15b50a92_07459749', 'head');
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9579618915cfa6b15b54607_29768854', 'javascript_bottom');
?>

  </head>

  <body id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
" class="sang <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'classnames' ][ 0 ], array( $_smarty_tpl->tpl_vars['page']->value['body_classes'] )), ENT_QUOTES, 'UTF-8');
if (isset($_smarty_tpl->tpl_vars['order_mini_cart']->value) && $_smarty_tpl->tpl_vars['order_mini_cart']->value == 'minicart') {?> pg_minicart<?php }
if ((isset($_smarty_tpl->tpl_vars['product_zoom_image']->value) && $_smarty_tpl->tpl_vars['product_zoom_image']->value)) {?> img_zoom_whenhover<?php }?>">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8312748585cfa6b15b67059_41084380', 'hook_after_body_opening_tag');
?>


    <main>
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5700885015cfa6b15b6c3f4_51430123', 'product_activation');
?>



        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6611319855cfa6b15b6fde8_69587439', 'header');
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16877972985cfa6b15b74131_61321713', 'notifications');
?>


      <div id="wrapper">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9577558725cfa6b15b78435_26867183', 'breadcrumb');
?>

        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'index') {?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product')) {?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['product']->value['id'] == '3') {?>
                    <div class="page_full_size col-xs-12">
                        <div class="row">
                <?php } else { ?>
            
                    <?php if (isset($_smarty_tpl->tpl_vars['product_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value == 1) {?>
                        <div class="page_full_size col-xs-12">
                            <div class="row">
                    <?php } else { ?>
                        <div class="container">
                        <div class="row">
                    <?php }?>
                <?php }?>
            <?php }?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category')) {?>                
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '6') {?>                   
                    <div class="page_full_size col-xs-12">
                        <div class="row">
                <?php } else { ?>
                    <?php if (isset($_smarty_tpl->tpl_vars['category_full_size']->value) && $_smarty_tpl->tpl_vars['category_full_size']->value != 1) {?>
                        <div class="container">
                        <div class="row">
                    <?php } else { ?>
                        <div class="page_full_size col-xs-12">
                            <div class="row">
                    <?php }?>
                <?php }?>
            <?php }?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'product' && $_smarty_tpl->tpl_vars['page']->value['page_name'] != 'category')) {?>
                <div class="container">
                    <div class="row">
            <?php }?>
        <?php }?>
        
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>            
            <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_left') {?>
                  <div id="left-column" class="col-xs-12 col-sm-4 col-md-3" <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?> style="display: none!important"<?php }?>>                     
                      <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                              
                           <?php }?>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                        </div>
              <?php } else { ?>
                    <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '8') {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17631145745cfa6b15ba5535_21858420', "left_column");
?>

                    <?php }?>
              <?php }?>
        <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3794261975cfa6b15bb0234_30011232', "left_column");
?>

        <?php }?>
        
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
          <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_left') {?>
               <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8765417205cfa6b15bb8f24_37884715', "content");
?>
                   
                </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_right') {?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12807605285cfa6b15bbceb8_55116563', "content_wrapper");
?>

          <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20293254635cfa6b15bc3a97_06637766', "content_wrapper");
?>

                <?php } elseif (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '8') {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18567734625cfa6b15bcb667_93823640', "content_wrapper");
?>

                <?php } else { ?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4579847315cfa6b15bcd8e5_32912569', "content_wrapper");
?>

                <?php }?>
          <?php }?>
        <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18095771115cfa6b15bcfdd8_03343745', "content_wrapper");
?>

        <?php }?>
          
          <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
            <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_right') {?>
              <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18680586835cfa6b15bd32f2_30291245', "right_column");
?>

            <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?>
                    <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                    </div>
                <?php }?>
            <?php }?>
          <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_415768255cfa6b15bd7847_35014953', "right_column");
?>

          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'index') {?>
            
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product' && isset($_smarty_tpl->tpl_vars['product_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value != 1) && ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category' && isset($_smarty_tpl->tpl_vars['category_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value != 1)) {?>
                </div>
                </div>
            <?php } else { ?>
                </div>
                </div>
            <?php }?>
            
            
          <?php }?>
      </div>

      <footer id="footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13068090105cfa6b15be0f87_41460899', "footer");
?>

      </footer>

    </main>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15331848845cfa6b15be5977_58132619', 'hook_before_body_closing_tag');
?>

  </body>

</html>
<?php }
/* {block 'head'} */
class Block_219985015cfa6b15b50a92_07459749 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_219985015cfa6b15b50a92_07459749',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender('file:_partials/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php
}
}
/* {/block 'head'} */
/* {block 'javascript_bottom'} */
class Block_9579618915cfa6b15b54607_29768854 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'javascript_bottom' => 
  array (
    0 => 'Block_9579618915cfa6b15b54607_29768854',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender("file:_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['bottom']), 0, false);
?>
    <?php
}
}
/* {/block 'javascript_bottom'} */
/* {block 'hook_after_body_opening_tag'} */
class Block_8312748585cfa6b15b67059_41084380 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_after_body_opening_tag' => 
  array (
    0 => 'Block_8312748585cfa6b15b67059_41084380',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterBodyOpeningTag'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_after_body_opening_tag'} */
/* {block 'product_activation'} */
class Block_5700885015cfa6b15b6c3f4_51430123 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_activation' => 
  array (
    0 => 'Block_5700885015cfa6b15b6c3f4_51430123',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-activation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'product_activation'} */
/* {block 'header'} */
class Block_6611319855cfa6b15b6fde8_69587439 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_6611319855cfa6b15b6fde8_69587439',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender('file:_partials/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'header'} */
/* {block 'notifications'} */
class Block_16877972985cfa6b15b74131_61321713 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'notifications' => 
  array (
    0 => 'Block_16877972985cfa6b15b74131_61321713',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'notifications'} */
/* {block 'breadcrumb'} */
class Block_9577558725cfa6b15b78435_26867183 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'breadcrumb' => 
  array (
    0 => 'Block_9577558725cfa6b15b78435_26867183',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:_partials/breadcrumb.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
          <?php
}
}
/* {/block 'breadcrumb'} */
/* {block "left_column"} */
class Block_17631145745cfa6b15ba5535_21858420 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_17631145745cfa6b15ba5535_21858420',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                              <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                                  <div class="left_column_filter filter-trigger">
                            		<i class="fa fa-sliders p_r active"></i>
                            	  </div>
                               <?php }?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                            </div>
                          <?php
}
}
/* {/block "left_column"} */
/* {block "left_column"} */
class Block_3794261975cfa6b15bb0234_30011232 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_3794261975cfa6b15bb0234_30011232',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                      <div class="left_column_filter filter-trigger">
                		<i class="fa fa-sliders p_r active"></i>
                	  </div>
                   <?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayLeftColumnProduct'),$_smarty_tpl ) );?>

                  <?php } else { ?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                  <?php }?>
                </div>
              <?php
}
}
/* {/block "left_column"} */
/* {block "content"} */
class Block_8765417205cfa6b15bb8f24_37884715 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_8765417205cfa6b15bb8f24_37884715',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <p>Hello world! This is HTML5 Boilerplate.</p>
                  <?php
}
}
/* {/block "content"} */
/* {block "content"} */
class Block_3831704385cfa6b15bbf004_84833772 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <p>Hello world! This is HTML5 Boilerplate.</p>
                      <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_12807605285cfa6b15bbceb8_55116563 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_12807605285cfa6b15bbceb8_55116563',
  ),
  'content' => 
  array (
    0 => 'Block_3831704385cfa6b15bbf004_84833772',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3831704385cfa6b15bbf004_84833772', "content", $this->tplIndex);
?>

                    </div>
                  <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_4884591385cfa6b15bc4e91_34516276 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_20293254635cfa6b15bc3a97_06637766 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_20293254635cfa6b15bc3a97_06637766',
  ),
  'content' => 
  array (
    0 => 'Block_4884591385cfa6b15bc4e91_34516276',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4884591385cfa6b15bc4e91_34516276', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_1502257755cfa6b15bcc185_95168128 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_18567734625cfa6b15bcb667_93823640 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_18567734625cfa6b15bcb667_93823640',
  ),
  'content' => 
  array (
    0 => 'Block_1502257755cfa6b15bcc185_95168128',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1502257755cfa6b15bcc185_95168128', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_3979087185cfa6b15bce2d7_06210108 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_4579847315cfa6b15bcd8e5_32912569 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_4579847315cfa6b15bcd8e5_32912569',
  ),
  'content' => 
  array (
    0 => 'Block_3979087185cfa6b15bce2d7_06210108',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="full-width col-xs-12 col-sm-12 col-md-12">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3979087185cfa6b15bce2d7_06210108', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_17531686555cfa6b15bd07a6_97684760 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <p>Hello world! This is HTML5 Boilerplate.</p>
                  <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_18095771115cfa6b15bcfdd8_03343745 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_18095771115cfa6b15bcfdd8_03343745',
  ),
  'content' => 
  array (
    0 => 'Block_17531686555cfa6b15bd07a6_97684760',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="content-wrapper" class="left-column right-column col-sm-4 col-md-6">
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17531686555cfa6b15bd07a6_97684760', "content", $this->tplIndex);
?>

                </div>
              <?php
}
}
/* {/block "content_wrapper"} */
/* {block "right_column"} */
class Block_18680586835cfa6b15bd32f2_30291245 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'right_column' => 
  array (
    0 => 'Block_18680586835cfa6b15bd32f2_30291245',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                </div>
              <?php
}
}
/* {/block "right_column"} */
/* {block "right_column"} */
class Block_415768255cfa6b15bd7847_35014953 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'right_column' => 
  array (
    0 => 'Block_415768255cfa6b15bd7847_35014953',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayRightColumnProduct'),$_smarty_tpl ) );?>

                  <?php } else { ?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayColumRightBlog'),$_smarty_tpl ) );?>

                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                  <?php }?>
                </div>
              <?php
}
}
/* {/block "right_column"} */
/* {block "footer"} */
class Block_13068090105cfa6b15be0f87_41460899 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_13068090105cfa6b15be0f87_41460899',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender("file:_partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block "footer"} */
/* {block 'hook_before_body_closing_tag'} */
class Block_15331848845cfa6b15be5977_58132619 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_before_body_closing_tag' => 
  array (
    0 => 'Block_15331848845cfa6b15be5977_58132619',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayBeforeBodyClosingTag'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_before_body_closing_tag'} */
}
