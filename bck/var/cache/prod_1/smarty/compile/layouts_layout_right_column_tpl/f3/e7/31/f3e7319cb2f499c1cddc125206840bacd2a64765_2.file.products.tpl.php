<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:06
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\catalog\_partials\products.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b16e79471_42701772',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3e7319cb2f499c1cddc125206840bacd2a64765' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\catalog\\_partials\\products.tpl',
      1 => 1559309874,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/miniatures/sub_listing/metro_products.tpl' => 2,
    'file:catalog/_partials/miniatures/sub_listing/masonry_product.tpl' => 2,
    'file:catalog/_partials/miniatures/product.tpl' => 2,
    'file:_partials/pagination.tpl' => 1,
  ),
),false)) {
function content_5cfa6b16e79471_42701772 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
if (!isset($_smarty_tpl->tpl_vars['load_more_link']->value) || !$_smarty_tpl->tpl_vars['load_more_link']->value) {?>
<div id="js-product-list">
<?php }?>
    <?php if ((isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value)) {?> 
        <?php if (isset($_smarty_tpl->tpl_vars['category']->value['id']) && $_smarty_tpl->tpl_vars['category']->value['id'] == 3) {?>
            <div class="products row product-masonry-list" data-masonry-metro='{"selector":".product-miniature","layoutMode":"masonry","columnWidth":".product-miniature"}'>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listing']->value['products'], 'product', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1314969895cfa6b16e2b830_19152741', 'product_miniature');
?>

                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                          </div>
        <?php } elseif (isset($_smarty_tpl->tpl_vars['category']->value['id']) && $_smarty_tpl->tpl_vars['category']->value['id'] == 4) {?>
            <div class="products row product-masonry-list" data-masonry-metro='{"selector":".product-miniature","layoutMode":"masonry","columnWidth":".product-miniature"}'>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listing']->value['products'], 'product', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7847458015cfa6b16e3b9d1_74170090', 'product_miniature');
?>

                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                          </div>
        <?php } else { ?>
            <div class="products row list_default" data-masonry-metro='{"selector":".product-miniature","layoutMode":"masonry","columnWidth":".product-miniature"}'>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listing']->value['products'], 'product', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18450623185cfa6b16e46882_36107823', 'product_miniature');
?>

                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </div>
        <?php }?>
    <?php } else { ?>
      <div class="products row<?php if (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && ($_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_metro' || $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_masonry')) {?> product-masonry-list<?php } else { ?> list_default<?php }?>" data-masonry-metro='{"selector":".product-miniature","layoutMode":"masonry","columnWidth":".product-miniature"}'>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listing']->value['products'], 'product', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['product']->value) {
?>
              <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19741538945cfa6b16e5beb9_86337566', 'product_miniature');
?>

            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                  </div>
    <?php }?>
  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18446442315cfa6b16e6ff99_19672459', 'pagination');
?>


<?php echo '<script'; ?>
 type="text/javascript">
initMasonryList();
<?php echo '</script'; ?>
>
<?php if (!isset($_smarty_tpl->tpl_vars['load_more_link']->value) || !$_smarty_tpl->tpl_vars['load_more_link']->value) {?>
</div>
<?php }
}
/* {block 'product_miniature'} */
class Block_1314969895cfa6b16e2b830_19152741 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_miniature' => 
  array (
    0 => 'Block_1314969895cfa6b16e2b830_19152741',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/sub_listing/metro_products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_miniature'} */
class Block_7847458015cfa6b16e3b9d1_74170090 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_miniature' => 
  array (
    0 => 'Block_7847458015cfa6b16e3b9d1_74170090',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/sub_listing/masonry_product.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_miniature'} */
class Block_18450623185cfa6b16e46882_36107823 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_miniature' => 
  array (
    0 => 'Block_18450623185cfa6b16e46882_36107823',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/product.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
                  <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_miniature'} */
class Block_19741538945cfa6b16e5beb9_86337566 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_miniature' => 
  array (
    0 => 'Block_19741538945cfa6b16e5beb9_86337566',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php if (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_metro') {?>
                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/sub_listing/metro_products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
                    <?php } elseif (isset($_smarty_tpl->tpl_vars['category_page_layout']->value) && $_smarty_tpl->tpl_vars['category_page_layout']->value == 'cat_masonry') {?>
                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/sub_listing/masonry_product.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
                    <?php } else { ?>
                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/product.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
                    <?php }?>
              <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'pagination'} */
class Block_18446442315cfa6b16e6ff99_19672459 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'pagination' => 
  array (
    0 => 'Block_18446442315cfa6b16e6ff99_19672459',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_subTemplateRender('file:_partials/pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pagination'=>$_smarty_tpl->tpl_vars['listing']->value['pagination']), 0, false);
?>
  <?php
}
}
/* {/block 'pagination'} */
}
