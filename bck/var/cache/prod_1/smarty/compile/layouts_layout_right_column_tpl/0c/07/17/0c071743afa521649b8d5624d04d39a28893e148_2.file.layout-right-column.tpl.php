<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:05
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\layouts\layout-right-column.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b15b1fed4_46626988',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0c071743afa521649b8d5624d04d39a28893e148' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\layouts\\layout-right-column.tpl',
      1 => 1559309874,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b15b1fed4_46626988 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9396431655cfa6b15b03fa2_64318586', 'left_column');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14181281505cfa6b15b06695_71688471', 'content_wrapper');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'layouts/layout-both-columns.tpl');
}
/* {block 'left_column'} */
class Block_9396431655cfa6b15b03fa2_64318586 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_9396431655cfa6b15b03fa2_64318586',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'left_column'} */
/* {block 'content'} */
class Block_16076132425cfa6b15b0cc11_73107380 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                  <p>Hello world! This is HTML5 Boilerplate.</p>
                <?php
}
}
/* {/block 'content'} */
/* {block 'content'} */
class Block_17286462985cfa6b15b14f99_22309404 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                          <p>Hello world! This is HTML5 Boilerplate.</p>
                        <?php
}
}
/* {/block 'content'} */
/* {block 'content'} */
class Block_18656722945cfa6b15b181e6_12775958 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                          <p>Hello world! This is HTML5 Boilerplate.</p>
                        <?php
}
}
/* {/block 'content'} */
/* {block 'content'} */
class Block_10946068295cfa6b15b1c586_89556041 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

              <p>Hello world! This is HTML5 Boilerplate.</p>
            <?php
}
}
/* {/block 'content'} */
/* {block 'content_wrapper'} */
class Block_14181281505cfa6b15b06695_71688471 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_14181281505cfa6b15b06695_71688471',
  ),
  'content' => 
  array (
    0 => 'Block_16076132425cfa6b15b0cc11_73107380',
    1 => 'Block_17286462985cfa6b15b14f99_22309404',
    2 => 'Block_18656722945cfa6b15b181e6_12775958',
    3 => 'Block_10946068295cfa6b15b1c586_89556041',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
          <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_right') {?>
              <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16076132425cfa6b15b0cc11_73107380', 'content', $this->tplIndex);
?>

              </div>
          <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?>
                    <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17286462985cfa6b15b14f99_22309404', 'content', $this->tplIndex);
?>

                    </div>
                <?php } else { ?>
                    <div id="content-wrapper" class="full-width sang col-xs-12 col-sm-12 col-md-12">
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18656722945cfa6b15b181e6_12775958', 'content', $this->tplIndex);
?>

                      </div>
                <?php }?>
                
          <?php }?>
    <?php } else { ?>
        <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10946068295cfa6b15b1c586_89556041', 'content', $this->tplIndex);
?>

          </div>
    <?php }
}
}
/* {/block 'content_wrapper'} */
}
