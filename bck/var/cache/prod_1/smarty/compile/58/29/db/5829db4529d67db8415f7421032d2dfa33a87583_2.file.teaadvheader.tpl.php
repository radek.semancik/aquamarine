<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:06
  from 'C:\Work\pixape\pragaglobal\themes\claue\modules\tea_themelayout\views\templates\hook\teaadvheader.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b16254135_99949405',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5829db4529d67db8415f7421032d2dfa33a87583' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\modules\\tea_themelayout\\views\\templates\\hook\\teaadvheader.tpl',
      1 => 1559832207,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b16254135_99949405 (Smarty_Internal_Template $_smarty_tpl) {
?>
<header id="header" <?php if ($_smarty_tpl->tpl_vars['class_header']->value) {?>class="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['class_header']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows_header']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
	<div class="section_header<?php if ($_smarty_tpl->tpl_vars['row']->value['class']) {?> <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['row']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>" style="<?php if ($_smarty_tpl->tpl_vars['row']->value['padding']) {?>padding:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['padding'], ENT_QUOTES, 'UTF-8');?>
;<?php }?> <?php if ($_smarty_tpl->tpl_vars['row']->value['margin']) {?> margin:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['margin'], ENT_QUOTES, 'UTF-8');?>
;<?php }?>">
		<?php if ($_smarty_tpl->tpl_vars['row']->value['fullwidth'] == 0) {?><div class="container">
			<div class="home-row row"> <?php }?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['positions'], 'position');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['position']->value) {
?>
				<div class="header-position <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['position']->value['class_suffix'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 col-lg-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['position']->value['col_lg'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 col-sm-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['position']->value['col_sm'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 col-md-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['position']->value['col_md'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 col-xs-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['position']->value['col_xs'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['position']->value['blocks'], 'block');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['block']->value) {
?>
						<div class="header-block <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['class_custom'], ENT_QUOTES, 'UTF-8');?>
">
							<?php if ($_smarty_tpl->tpl_vars['block']->value['show_title']) {?><h4 class="title_block"><span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['block']->value['title'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span></h4><?php }?>
							<div class="block-content">
								<?php if (isset($_smarty_tpl->tpl_vars['block']->value['return_value'])) {
echo $_smarty_tpl->tpl_vars['block']->value['return_value'];
}?>
							</div>
						</div>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</div>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			<?php if ($_smarty_tpl->tpl_vars['row']->value['fullwidth'] == 0) {?>	</div>
		</div><?php }?>
	</div>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</header><?php }
}
