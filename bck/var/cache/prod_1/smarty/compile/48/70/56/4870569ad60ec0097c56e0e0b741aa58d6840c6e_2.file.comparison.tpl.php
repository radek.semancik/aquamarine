<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:06
  from 'C:\Work\pixape\pragaglobal\modules\tea_productcomparison\views\templates\hook\comparison.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b16db1830_05897337',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4870569ad60ec0097c56e0e0b741aa58d6840c6e' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\modules\\tea_productcomparison\\views\\templates\\hook\\comparison.tpl',
      1 => 1559309866,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b16db1830_05897337 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['comparator_max_item']->value) {?>
	<form method="post" action="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tea_productcomparison'),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="compare-form">
		<button type="submit" class="btn btn-default button button-medium bt_compare bt_compare<?php if (isset($_smarty_tpl->tpl_vars['paginationId']->value)) {?>_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['paginationId']->value, ENT_QUOTES, 'UTF-8');
}?>" disabled="disabled">
			<i class="pe-7s-graph3"></i><span class="total-compare-val"><?php echo htmlspecialchars(count($_smarty_tpl->tpl_vars['compared_products']->value), ENT_QUOTES, 'UTF-8');?>
</span>
		</button>
		<input type="hidden" name="compare_product_count" class="compare_product_count" value="<?php echo htmlspecialchars(count($_smarty_tpl->tpl_vars['compared_products']->value), ENT_QUOTES, 'UTF-8');?>
" />
		<input type="hidden" name="compare_product_list" class="compare_product_list" value="" />
	</form>
<?php }
}
}
