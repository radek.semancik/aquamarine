<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:12
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\layouts\layout-both-columns.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b1c7cc4c3_20662658',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c8be8840b0ff827daedfb9c55f72c056e8c1128e' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\layouts\\layout-both-columns.tpl',
      1 => 1559828411,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/head.tpl' => 1,
    'file:_partials/javascript.tpl' => 1,
    'file:catalog/_partials/product-activation.tpl' => 1,
    'file:_partials/header.tpl' => 1,
    'file:_partials/notifications.tpl' => 1,
    'file:_partials/breadcrumb.tpl' => 1,
    'file:_partials/footer.tpl' => 1,
  ),
),false)) {
function content_5cfa6b1c7cc4c3_20662658 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

  <head>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10053887315cfa6b1c72a167_08689376', 'head');
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2147386915cfa6b1c72b742_35543988', 'javascript_bottom');
?>

  </head>

  <body id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
" class="sang <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'classnames' ][ 0 ], array( $_smarty_tpl->tpl_vars['page']->value['body_classes'] )), ENT_QUOTES, 'UTF-8');
if (isset($_smarty_tpl->tpl_vars['order_mini_cart']->value) && $_smarty_tpl->tpl_vars['order_mini_cart']->value == 'minicart') {?> pg_minicart<?php }
if ((isset($_smarty_tpl->tpl_vars['product_zoom_image']->value) && $_smarty_tpl->tpl_vars['product_zoom_image']->value)) {?> img_zoom_whenhover<?php }?>">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15594978505cfa6b1c736976_08472631', 'hook_after_body_opening_tag');
?>


    <main>
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17985200645cfa6b1c737e78_11407093', 'product_activation');
?>



        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14747014045cfa6b1c739336_58597700', 'header');
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11018676145cfa6b1c73a6a7_27163177', 'notifications');
?>


      <div id="wrapper">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11528047305cfa6b1c73b998_41336613', 'breadcrumb');
?>

        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'index') {?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product')) {?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['product']->value['id'] == '3') {?>
                    <div class="page_full_size col-xs-12">
                        <div class="row">
                <?php } else { ?>
            
                    <?php if (isset($_smarty_tpl->tpl_vars['product_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value == 1) {?>
                        <div class="page_full_size col-xs-12">
                            <div class="row">
                    <?php } else { ?>
                        <div class="container">
                        <div class="row">
                    <?php }?>
                <?php }?>
            <?php }?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category')) {?>                
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '6') {?>                   
                    <div class="page_full_size col-xs-12">
                        <div class="row">
                <?php } else { ?>
                    <?php if (isset($_smarty_tpl->tpl_vars['category_full_size']->value) && $_smarty_tpl->tpl_vars['category_full_size']->value != 1) {?>
                        <div class="container">
                        <div class="row">
                    <?php } else { ?>
                        <div class="page_full_size col-xs-12">
                            <div class="row">
                    <?php }?>
                <?php }?>
            <?php }?>
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'product' && $_smarty_tpl->tpl_vars['page']->value['page_name'] != 'category')) {?>
                <div class="container">
                    <div class="row">
            <?php }?>
        <?php }?>
        
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>            
            <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_left') {?>
                  <div id="left-column" class="col-xs-12 col-sm-4 col-md-3" <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?> style="display: none!important"<?php }?>>                     
                      <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                              
                           <?php }?>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                        </div>
              <?php } else { ?>
                    <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '8') {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9535844925cfa6b1c76f833_47895896', "left_column");
?>

                    <?php }?>
              <?php }?>
        <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13949190235cfa6b1c779df7_49426388', "left_column");
?>

        <?php }?>
        
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
          <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_left') {?>
               <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10544783325cfa6b1c78d591_77437290', "content");
?>
                   
                </div>
          <?php } elseif ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_right') {?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12757565285cfa6b1c794702_41979472', "content_wrapper");
?>

          <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9987329685cfa6b1c79e994_36937203', "content_wrapper");
?>

                <?php } elseif (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '8') {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11415106125cfa6b1c7a35a5_77150209', "content_wrapper");
?>

                <?php } else { ?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12004636435cfa6b1c7a6d24_22480803', "content_wrapper");
?>

                <?php }?>
          <?php }?>
        <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10985142935cfa6b1c7a9388_01548647', "content_wrapper");
?>

        <?php }?>
          
          <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
            <?php if ($_smarty_tpl->tpl_vars['category_full_size']->value == 'category_right') {?>
              <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_917012035cfa6b1c7affc4_65069066', "right_column");
?>

            <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['modeDeveloper']->value) && $_smarty_tpl->tpl_vars['modeDeveloper']->value && $_smarty_tpl->tpl_vars['category']->value['id'] == '7') {?>
                    <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                    </div>
                <?php }?>
            <?php }?>
          <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_759010015cfa6b1c7b4664_83693211', "right_column");
?>

          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'index') {?>
            
            <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product' && isset($_smarty_tpl->tpl_vars['product_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value != 1) && ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category' && isset($_smarty_tpl->tpl_vars['category_full_size']->value) && $_smarty_tpl->tpl_vars['product_full_size']->value != 1)) {?>
                </div>
                </div>
            <?php } else { ?>
                </div>
                </div>
            <?php }?>
            
            
          <?php }?>
      </div>

      <footer id="footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15076451335cfa6b1c7c2914_20644025', "footer");
?>

      </footer>

    </main>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10234023795cfa6b1c7c7107_88456519', 'hook_before_body_closing_tag');
?>

  </body>

</html>
<?php }
/* {block 'head'} */
class Block_10053887315cfa6b1c72a167_08689376 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_10053887315cfa6b1c72a167_08689376',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender('file:_partials/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php
}
}
/* {/block 'head'} */
/* {block 'javascript_bottom'} */
class Block_2147386915cfa6b1c72b742_35543988 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'javascript_bottom' => 
  array (
    0 => 'Block_2147386915cfa6b1c72b742_35543988',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender("file:_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['bottom']), 0, false);
?>
    <?php
}
}
/* {/block 'javascript_bottom'} */
/* {block 'hook_after_body_opening_tag'} */
class Block_15594978505cfa6b1c736976_08472631 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_after_body_opening_tag' => 
  array (
    0 => 'Block_15594978505cfa6b1c736976_08472631',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterBodyOpeningTag'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_after_body_opening_tag'} */
/* {block 'product_activation'} */
class Block_17985200645cfa6b1c737e78_11407093 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_activation' => 
  array (
    0 => 'Block_17985200645cfa6b1c737e78_11407093',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-activation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'product_activation'} */
/* {block 'header'} */
class Block_14747014045cfa6b1c739336_58597700 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_14747014045cfa6b1c739336_58597700',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender('file:_partials/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'header'} */
/* {block 'notifications'} */
class Block_11018676145cfa6b1c73a6a7_27163177 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'notifications' => 
  array (
    0 => 'Block_11018676145cfa6b1c73a6a7_27163177',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'notifications'} */
/* {block 'breadcrumb'} */
class Block_11528047305cfa6b1c73b998_41336613 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'breadcrumb' => 
  array (
    0 => 'Block_11528047305cfa6b1c73b998_41336613',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:_partials/breadcrumb.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
          <?php
}
}
/* {/block 'breadcrumb'} */
/* {block "left_column"} */
class Block_9535844925cfa6b1c76f833_47895896 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_9535844925cfa6b1c76f833_47895896',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                              <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                                  <div class="left_column_filter filter-trigger">
                            		<i class="fa fa-sliders p_r active"></i>
                            	  </div>
                               <?php }?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                            </div>
                          <?php
}
}
/* {/block "left_column"} */
/* {block "left_column"} */
class Block_13949190235cfa6b1c779df7_49426388 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_13949190235cfa6b1c779df7_49426388',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
                      <div class="left_column_filter filter-trigger">
                		<i class="fa fa-sliders p_r active"></i>
                	  </div>
                   <?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayLeftColumnProduct'),$_smarty_tpl ) );?>

                  <?php } else { ?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

                  <?php }?>
                </div>
              <?php
}
}
/* {/block "left_column"} */
/* {block "content"} */
class Block_10544783325cfa6b1c78d591_77437290 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_10544783325cfa6b1c78d591_77437290',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <p>Hello world! This is HTML5 Boilerplate.</p>
                  <?php
}
}
/* {/block "content"} */
/* {block "content"} */
class Block_3568990835cfa6b1c796941_93853298 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <p>Hello world! This is HTML5 Boilerplate.</p>
                      <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_12757565285cfa6b1c794702_41979472 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_12757565285cfa6b1c794702_41979472',
  ),
  'content' => 
  array (
    0 => 'Block_3568990835cfa6b1c796941_93853298',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3568990835cfa6b1c796941_93853298', "content", $this->tplIndex);
?>

                    </div>
                  <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_6203074585cfa6b1c79f443_51003203 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_9987329685cfa6b1c79e994_36937203 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_9987329685cfa6b1c79e994_36937203',
  ),
  'content' => 
  array (
    0 => 'Block_6203074585cfa6b1c79f443_51003203',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="right-column col-xs-12 col-sm-8 col-md-9">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6203074585cfa6b1c79f443_51003203', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_9650942765cfa6b1c7a5485_33075708 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_11415106125cfa6b1c7a35a5_77150209 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_11415106125cfa6b1c7a35a5_77150209',
  ),
  'content' => 
  array (
    0 => 'Block_9650942765cfa6b1c7a5485_33075708',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9650942765cfa6b1c7a5485_33075708', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_8559701275cfa6b1c7a76e7_14400480 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <p>Hello world! This is HTML5 Boilerplate.</p>
                          <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_12004636435cfa6b1c7a6d24_22480803 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_12004636435cfa6b1c7a6d24_22480803',
  ),
  'content' => 
  array (
    0 => 'Block_8559701275cfa6b1c7a76e7_14400480',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div id="content-wrapper" class="full-width col-xs-12 col-sm-12 col-md-12">
                          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8559701275cfa6b1c7a76e7_14400480', "content", $this->tplIndex);
?>

                        </div>
                      <?php
}
}
/* {/block "content_wrapper"} */
/* {block "content"} */
class Block_6952348305cfa6b1c7a9e56_50402791 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <p>Hello world! This is HTML5 Boilerplate.</p>
                  <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_10985142935cfa6b1c7a9388_01548647 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_10985142935cfa6b1c7a9388_01548647',
  ),
  'content' => 
  array (
    0 => 'Block_6952348305cfa6b1c7a9e56_50402791',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="content-wrapper" class="left-column right-column col-sm-4 col-md-6">
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6952348305cfa6b1c7a9e56_50402791', "content", $this->tplIndex);
?>

                </div>
              <?php
}
}
/* {/block "content_wrapper"} */
/* {block "right_column"} */
class Block_917012035cfa6b1c7affc4_65069066 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'right_column' => 
  array (
    0 => 'Block_917012035cfa6b1c7affc4_65069066',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                </div>
              <?php
}
}
/* {/block "right_column"} */
/* {block "right_column"} */
class Block_759010015cfa6b1c7b4664_83693211 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'right_column' => 
  array (
    0 => 'Block_759010015cfa6b1c7b4664_83693211',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayRightColumnProduct'),$_smarty_tpl ) );?>

                  <?php } else { ?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayColumRightBlog'),$_smarty_tpl ) );?>

                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

                  <?php }?>
                </div>
              <?php
}
}
/* {/block "right_column"} */
/* {block "footer"} */
class Block_15076451335cfa6b1c7c2914_20644025 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_15076451335cfa6b1c7c2914_20644025',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender("file:_partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block "footer"} */
/* {block 'hook_before_body_closing_tag'} */
class Block_10234023795cfa6b1c7c7107_88456519 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_before_body_closing_tag' => 
  array (
    0 => 'Block_10234023795cfa6b1c7c7107_88456519',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayBeforeBodyClosingTag'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_before_body_closing_tag'} */
}
