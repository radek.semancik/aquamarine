<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:12
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b1c6cfc12_85925150',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '96fff6dd383fe7feeba637f9aad5ae8240e17d53' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\page.tpl',
      1 => 1559309874,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b1c6cfc12_85925150 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16326915725cfa6b1c6b8676_51892761', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_18529686545cfa6b1c6b9883_75894789 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_20539650445cfa6b1c6b8ec0_92382091 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18529686545cfa6b1c6b9883_75894789', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_19830511525cfa6b1c6c4066_88020649 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_20177484765cfa6b1c6c6378_84215770 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_17812874265cfa6b1c6c2955_95112493 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <div id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19830511525cfa6b1c6c4066_88020649', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20177484765cfa6b1c6c6378_84215770', 'page_content', $this->tplIndex);
?>

      </div>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_3937277355cfa6b1c6cb850_36813058 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_15390298285cfa6b1c6c9612_48388609 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3937277355cfa6b1c6cb850_36813058', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_16326915725cfa6b1c6b8676_51892761 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_16326915725cfa6b1c6b8676_51892761',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_20539650445cfa6b1c6b8ec0_92382091',
  ),
  'page_title' => 
  array (
    0 => 'Block_18529686545cfa6b1c6b9883_75894789',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_17812874265cfa6b1c6c2955_95112493',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_19830511525cfa6b1c6c4066_88020649',
  ),
  'page_content' => 
  array (
    0 => 'Block_20177484765cfa6b1c6c6378_84215770',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_15390298285cfa6b1c6c9612_48388609',
  ),
  'page_footer' => 
  array (
    0 => 'Block_3937277355cfa6b1c6cb850_36813058',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <div id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20539650445cfa6b1c6b8ec0_92382091', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17812874265cfa6b1c6c2955_95112493', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15390298285cfa6b1c6c9612_48388609', 'page_footer_container', $this->tplIndex);
?>


  </div>

<?php
}
}
/* {/block 'content'} */
}
