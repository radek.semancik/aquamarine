<?php
/* Smarty version 3.1.33, created on 2019-06-07 15:48:12
  from 'C:\Work\pixape\pragaglobal\themes\claue\templates\_partials\breadcrumb.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cfa6b1cc402f8_60972864',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ce2bfcc211aa5917c9b7dc85cc02ef537e746dc9' => 
    array (
      0 => 'C:\\Work\\pixape\\pragaglobal\\themes\\claue\\templates\\_partials\\breadcrumb.tpl',
      1 => 1559309874,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cfa6b1cc402f8_60972864 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<?php if (isset($_smarty_tpl->tpl_vars['page']->value['page_name']) && $_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category') {?>
    <?php if (isset($_smarty_tpl->tpl_vars['enb_subcategories']->value) && $_smarty_tpl->tpl_vars['enb_subcategories']->value) {?>
        <?php if (isset($_smarty_tpl->tpl_vars['subcategories']->value)) {?>
            <!-- Subcategories -->
            <div id="subcategories">
                <div class="container">
                    <div class="row">
                        <ul class="subcategories_list">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subcategories']->value, 'subcategory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subcategory']->value) {
?>
                                <li>
                                    <a class="subcategory-name" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['subcategory']->value['id_category'],$_smarty_tpl->tpl_vars['subcategory']->value['link_rewrite']),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['subcategory']->value['name'],25,'...' )),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                                    </a>
                                </li>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php }?>
    <?php }?>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9482693995cfa6b1cbb1b22_53923184', 'product_list_header');
?>

<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'contact') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Contact','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'order-confirmation') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Order Confirmation','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'module-blockwishlist-mywishlist') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My Wishlist','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'stores') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Stores','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'prices-drop') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sales','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'my-account') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My Account','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'manufacturers') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Brands','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'manufacturer') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer']->value['name'], ENT_QUOTES, 'UTF-8');?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'suppliers') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Brands','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'supplier') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['supplier']->value['name'], ENT_QUOTES, 'UTF-8');?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'history') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Order History','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'cart') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Shopping Cart','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'search') {?>
        <div class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search','d'=>'Shop.Theme.Action'),$_smarty_tpl ) );?>
</h3>
                </div>
            </div>
        </div>
<?php } else { ?>
    <nav data-depth="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['breadcrumb']->value['count'], ENT_QUOTES, 'UTF-8');?>
" class="breadcrumb" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
      <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'authentication') {?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['breadcrumb']->value['links'], 'path', false, NULL, 'breadcrumb', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['path']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_breadcrumb']->value['iteration']++;
?>
              <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16763448395cfa6b1cc35497_92129577', 'breadcrumb_item');
?>

            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <?php } else { ?>
            <li>
              <span class="breadcrumb_title" itemprop="name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['title'], ENT_QUOTES, 'UTF-8');?>
</span>
            </li>
        <?php }?>
      </ol>
    </nav>
<?php }
}
/* {block 'product_list_header'} */
class Block_9482693995cfa6b1cbb1b22_53923184 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_header' => 
  array (
    0 => 'Block_9482693995cfa6b1cbb1b22_53923184',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <div class="breadcrumb block-category card card-block" <?php if (isset($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) && $_smarty_tpl->tpl_vars['pg_background_breadcrum']->value) {?>style="background-image:url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pg_background_breadcrum']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?>>
            <div class="<?php if (isset($_smarty_tpl->tpl_vars['category']->value['image']) && $_smarty_tpl->tpl_vars['category']->value['image'] != '') {?>block-category-parallax<?php }?>" <?php if (isset($_smarty_tpl->tpl_vars['category']->value['image']) && $_smarty_tpl->tpl_vars['category']->value['image'] != '') {?> data-bottom-top="top: -70%;" data-top-bottom="top: 0%;" style="background-image:url(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['image']['large']['url'], ENT_QUOTES, 'UTF-8');?>
);"<?php }?>></div>
            <div class="container">
                <div class="block-category-top">
                  <h3 class="h1 cat_title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8');?>
</h3>
                  <?php if ($_smarty_tpl->tpl_vars['category']->value['description']) {?>
                    <div id="category-description" class="text-muted"><?php echo $_smarty_tpl->tpl_vars['category']->value['description'];?>
</div>
                  <?php }?>
                </div>
            </div>
        </div>
    <?php
}
}
/* {/block 'product_list_header'} */
/* {block 'breadcrumb_item'} */
class Block_16763448395cfa6b1cc35497_92129577 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'breadcrumb_item' => 
  array (
    0 => 'Block_16763448395cfa6b1cc35497_92129577',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                  <a itemprop="item" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['url'], ENT_QUOTES, 'UTF-8');?>
">
                    <span class="breadcrumb_title" itemprop="name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['title'], ENT_QUOTES, 'UTF-8');?>
</span>
                  </a>
                  <meta itemprop="position" content="<?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_foreach_breadcrumb']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_breadcrumb']->value['iteration'] : null), ENT_QUOTES, 'UTF-8');?>
">
                </li>
              <?php
}
}
/* {/block 'breadcrumb_item'} */
}
