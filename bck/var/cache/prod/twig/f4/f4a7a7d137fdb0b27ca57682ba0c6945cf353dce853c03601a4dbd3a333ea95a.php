<?php

/* __string_template__d5a695ffc5c5e6ef4237695a9212b9e5f033e3d6114395356fd88323a813e357 */
class __TwigTemplate_da865c316296a228bc5662ff88ba9868670704d387b0cc2be8f76f885c913be0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Products • Outstock Responsive Prestashop v1.7 Theme</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminProducts';
    var iso_user = 'en';
    var lang_is_rtl = '0';
    var full_language_code = 'en-us';
    var full_cldr_language_code = 'en-US';
    var country_iso_code = 'GB';
    var _PS_VERSION_ = '1.7.5.2';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'A new order has been placed on your shop.';
    var order_number_msg = 'Order number: ';
    var total_msg = 'Total: ';
    var from_msg = 'From: ';
    var see_order_msg = 'View this order';
    var new_customer_msg = 'A new customer registered on your shop.';
    var customer_name_msg = 'Customer name: ';
    var new_msg = 'A new message was posted on your shop.';
    var see_msg = 'Read this message';
    var token = '88adc6afcf497bcf06704e66fd391187';
    var token_admin_orders = '5c556600f17857de3cc0cbb4a24b75bc';
    var token_admin_customers = '61ff7d046cb22aa63b1b2476b65a875a';
    var token_admin_customer_threads = 'b07326f7ee549e4b4dc152944845d89b';
    var currentIndex = 'index.php?controller=AdminProducts';
    var employee_token = '29b403844ae8c844a240f455e6e96476';
    var choose_language_translate = 'Choose language';
    var default_language = '1';
    var admin_modules_link = '/backoffice/index.php/improve/modules/catalog/recommended?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg';
    var tab_modules_list = 'prestagiftvouchers,dmuassocprodcat,etranslation,apiway,prestashoptoquickbooks';
    var update_success_msg = 'Update successful';
    var errorLogin = 'PrestaShop was unable to log in to Addons. Please check your credentials and your Internet connection.';
    var search_product_msg = 'Search for a product';
  </script>

      <link href=\"/backoffice/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/backoffice/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/backoffice\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"GBP\",\"sign\":\"\\u00a3\",\"name\":\"British Pound Sterling\",\"format\":\"\\u00a4#,##0.00\"};
var host_mode = false;
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/backoffice/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.5.2\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.5.2\"></script>
<script type=\"text/javascript\" src=\"/backoffice/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/backoffice/themes/default/js/vendor/nv.d3.min.js\"></script>

  

";
        // line 72
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"lang-en adminproducts\">


<header id=\"header\">
  <nav id=\"header_infos\" class=\"main-header\">

    <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

        
        <i class=\"material-icons js-mobile-menu\">menu</i>
    <a id=\"header_logo\" class=\"logo float-left\" href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminDashboard&amp;token=d1d9d1ba0652d886ca3e7d8c7cfd50dc\"></a>
    <span id=\"shop_version\">1.7.5.2</span>

    <div class=\"component\" id=\"quick-access-container\">
      <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Quick Access
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"http://backoffice/index.php?controller=AdminModules&amp;configure=labproductcategory&amp;token=766b5044650b61e0ec60365d4bfe77be\"
                 data-item=\"Config Laber Product Category\"
      >Config Laber Product Category</a>
          <a class=\"dropdown-item\"
         href=\"http://backoffice/index.php?controller=AdminModules&amp;configure=labproductfilter&amp;token=766b5044650b61e0ec60365d4bfe77be\"
                 data-item=\"Config Laber Product Filter\"
      >Config Laber Product Filter</a>
          <a class=\"dropdown-item\"
         href=\"http://backoffice/index.php/module/manage?token=df6a11eae9936b7d6764fec18e046c18\"
                 data-item=\"Installed modules\"
      >Installed modules</a>
          <a class=\"dropdown-item\"
         href=\"http://backoffice/index.php?controller=AdminCategories&amp;addcategory&amp;token=59f3fb11c45b83535c85eb34fe6398f7\"
                 data-item=\"New category\"
      >New category</a>
          <a class=\"dropdown-item\"
         href=\"http://backoffice/index.php/product/new?token=df6a11eae9936b7d6764fec18e046c18\"
                 data-item=\"New product\"
      >New product</a>
          <a class=\"dropdown-item\"
         href=\"http://backoffice/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=6e5f3c4880042884d90f9e4b50ee6544\"
                 data-item=\"New voucher\"
      >New voucher</a>
          <a class=\"dropdown-item\"
         href=\"http://backoffice/index.php?controller=AdminOrders&amp;token=5c556600f17857de3cc0cbb4a24b75bc\"
                 data-item=\"Orders\"
      >Orders</a>
          <a class=\"dropdown-item\"
         href=\"http://backoffice/index.php?localhost/laber_outstock_v17/adminps/?controller=AdminModules&amp;&amp;configure=xipblog&amp;token=766b5044650b61e0ec60365d4bfe77be\"
                 data-item=\"XipBlog Settings\"
      >XipBlog Settings</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"85\"
        data-icon=\"icon-AdminCatalog\"
        data-method=\"add\"
        data-url=\"index.php/sell/catalog/products\"
        data-post-link=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminQuickAccesses&token=0b43ce8b555fee8737b06f58ae9b9380\"
        data-prompt-text=\"Please name this shortcut:\"
        data-link=\"Products - List\"
      >
        <i class=\"material-icons\">add_circle</i>
        Add current page to QuickAccess
      </a>
        <a class=\"dropdown-item\" href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminQuickAccesses&token=0b43ce8b555fee8737b06f58ae9b9380\">
      <i class=\"material-icons\">settings</i>
      Manage quick accesses
    </a>
  </div>
</div>
    </div>
    <div class=\"component\" id=\"header-search-container\">
      <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/backoffice/index.php?controller=AdminSearch&amp;token=cd8dba153b57ea3d885647b7e39873be\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Search (e.g.: product reference, customer name…)\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        Everywhere
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"Everywhere\" href=\"#\" data-value=\"0\" data-placeholder=\"What are you looking for?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> Everywhere</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Product name, SKU, reference...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catalog</a>
        <a class=\"dropdown-item\" data-item=\"Customers by name\" href=\"#\" data-value=\"2\" data-placeholder=\"Email, name...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Customers by name</a>
        <a class=\"dropdown-item\" data-item=\"Customers by ip address\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Customers by IP address</a>
        <a class=\"dropdown-item\" data-item=\"Orders\" href=\"#\" data-value=\"3\" data-placeholder=\"Order ID\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Orders</a>
        <a class=\"dropdown-item\" data-item=\"Invoices\" href=\"#\" data-value=\"4\" data-placeholder=\"Invoice Number\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i> Invoices</a>
        <a class=\"dropdown-item\" data-item=\"Carts\" href=\"#\" data-value=\"5\" data-placeholder=\"Cart ID\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Carts</a>
        <a class=\"dropdown-item\" data-item=\"Modules\" href=\"#\" data-value=\"7\" data-placeholder=\"Module name\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Modules</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">SEARCH</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
    </div>

            <div class=\"component\" id=\"header-shop-list-container\">
        <div id=\"shop-list\" class=\"shop-list dropdown ps-dropdown stores\">
    <button class=\"btn btn-link\" type=\"button\" data-toggle=\"dropdown\">
      <span class=\"selected-item\">
        <i class=\"material-icons visibility\">visibility</i>
                  All shops
                <i class=\"material-icons arrow-down\">arrow_drop_down</i>
      </span>
    </button>
    <div class=\"dropdown-menu dropdown-menu-right ps-dropdown-menu\">
      <ul class=\"items-list\"><li class=\"active\"><a class=\"dropdown-item\" href=\"/backoffice/index.php/sell/catalog/products?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg&amp;setShopContext=\">All shops</a></li><li class=\"group\"><a class=\"dropdown-item\" href=\"/backoffice/index.php/sell/catalog/products?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg&amp;setShopContext=g-1\">Default group</a></li><li class=\"shop\"><a class=\"dropdown-item  disabled\" href=\"#\">Outstock 1</a><a class=\"link-shop\" href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminShop&amp;token=b1cdc3e2fda940209f83652c051adcc5\" target=\"_blank\"><i class=\"material-icons\">&#xE869;</i></a></li><li class=\"shop\"><a class=\"dropdown-item  disabled\" href=\"#\">Outstock 2</a><a class=\"link-shop\" href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminShop&amp;token=b1cdc3e2fda940209f83652c051adcc5\" target=\"_blank\"><i class=\"material-icons\">&#xE869;</i></a></li><li class=\"shop\"><a class=\"dropdown-item  disabled\" href=\"#\">Outstock 3</a><a class=\"link-shop\" href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminShop&amp;token=b1cdc3e2fda940209f83652c051adcc5\" target=\"_blank\"><i class=\"material-icons\">&#xE869;</i></a></li><li class=\"shop\"><a class=\"dropdown-item \" href=\"/backoffice/index.php/sell/catalog/products?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg&amp;setShopContext=s-4\">Outstock 4</a><a class=\"link-shop\" href=\"/\" target=\"_blank\"><i class=\"material-icons\">&#xE8F4;</i></a></li></ul>

    </div>
  </div>
    </div>
          <div class=\"component header-right-component\" id=\"header-notifications-container\">
        <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Orders<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Customers<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Messages<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No new order for now :(<br>
              Did you check your conversion rate lately?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No new customer for now :(<br>
              Have you considered selling on marketplaces?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No new message for now.<br>
              Seems like all your customers are happy :)
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      from <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registered <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
      </div>
        <div class=\"component\" id=\"header-employee-container\">
      <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"text-center employee_avatar\">
      <img class=\"avatar rounded-circle\" src=\"http://profile.prestashop.com/demo%40demo.com.jpg\" />
      <span>Demo Demo</span>
    </div>
    <a class=\"dropdown-item employee-link profile-link\" href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminEmployees&amp;id_employee=1&amp;updateemployee=1&amp;token=29b403844ae8c844a240f455e6e96476\">
      <i class=\"material-icons\">settings_applications</i>
      Your profile
    </a>
    <a class=\"dropdown-item employee-link\" id=\"header_logout\" href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminLogin&amp;logout=1&amp;token=c9ff4d98dd63df5025ff8592470b43f6\">
      <i class=\"material-icons\">power_settings_new</i>
      <span>Sign out</span>
    </a>
  </div>
</div>
    </div>

      </nav>
  </header>

<nav class=\"nav-bar d-none d-md-block\">
  <span class=\"menu-collapse\">
    <i class=\"material-icons\">chevron_left</i>
    <i class=\"material-icons\">chevron_left</i>
  </span>

  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminDashboard&amp;token=d1d9d1ba0652d886ca3e7d8c7cfd50dc\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Dashboard</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Sell</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminOrders&amp;token=5c556600f17857de3cc0cbb4a24b75bc\" class=\"link\">
                    <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                    <span>
                    Orders
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminOrders&amp;token=5c556600f17857de3cc0cbb4a24b75bc\" class=\"link\"> Orders
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"/backoffice/index.php/sell/orders/invoices/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Invoices
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminSlip&amp;token=e2305ed685c9943d62a62ecc177599ba\" class=\"link\"> Credit Slips
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"/backoffice/index.php/sell/orders/delivery-slips/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Delivery Slips
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCarts&amp;token=130b67cf753a311758d84278fb1107e4\" class=\"link\"> Shopping Carts
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                                                    
                <li class=\"link-levelone has_submenu -active open ul-open\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/backoffice/index.php/sell/catalog/products?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\">
                    <i class=\"material-icons mi-store\">store</i>
                    <span>
                    Catalog
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_up
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/backoffice/index.php/sell/catalog/products?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Products
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCategories&amp;token=59f3fb11c45b83535c85eb34fe6398f7\" class=\"link\"> Categories
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminTracking&amp;token=5f8adbf9181a69e01ad9d1be858a9e8a\" class=\"link\"> Monitoring
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminAttributesGroups&amp;token=5bc21ced1313dd8aedd80caeb2298281\" class=\"link\"> Attributes &amp; Features
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminManufacturers&amp;token=55a118db301933a90707a54cb2b2903a\" class=\"link\"> Brands &amp; Suppliers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminAttachments&amp;token=1038b764c580d0864bcd72f80a37ced1\" class=\"link\"> Files
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCartRules&amp;token=6e5f3c4880042884d90f9e4b50ee6544\" class=\"link\"> Discounts
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/backoffice/index.php/sell/stocks/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCustomers&amp;token=61ff7d046cb22aa63b1b2476b65a875a\" class=\"link\">
                    <i class=\"material-icons mi-account_circle\">account_circle</i>
                    <span>
                    Customers
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCustomers&amp;token=61ff7d046cb22aa63b1b2476b65a875a\" class=\"link\"> Customers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminAddresses&amp;token=ee9c6dc857d187d9cd92b983d6211754\" class=\"link\"> Addresses
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCustomerThreads&amp;token=b07326f7ee549e4b4dc152944845d89b\" class=\"link\">
                    <i class=\"material-icons mi-chat\">chat</i>
                    <span>
                    Customer Service
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCustomerThreads&amp;token=b07326f7ee549e4b4dc152944845d89b\" class=\"link\"> Customer Service
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminOrderMessage&amp;token=2c05b88deec5e751ec61ee58412fc985\" class=\"link\"> Order Messages
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminReturn&amp;token=38ae5db59f53d7c539138d5b44aba386\" class=\"link\"> Merchandise Returns
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminStats&amp;token=bea8eafbc0df8136c199927401a8a448\" class=\"link\">
                    <i class=\"material-icons mi-assessment\">assessment</i>
                    <span>
                    Stats
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Improve</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/backoffice/index.php/improve/modules/manage?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Modules
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/backoffice/index.php/improve/modules/manage?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Modules &amp; Services
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"46\" id=\"subtab-AdminAddonsCatalog\">
                              <a href=\"/backoffice/index.php/improve/modules/addons-store?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Modules Catalog
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"47\" id=\"subtab-AdminParentThemes\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminThemes&amp;token=4e9fc1c6e7c472f6f0c1c69fc94e2d67\" class=\"link\">
                    <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                    <span>
                    Design
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-47\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\" id=\"subtab-AdminThemes\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminThemes&amp;token=4e9fc1c6e7c472f6f0c1c69fc94e2d67\" class=\"link\"> Theme &amp; Logo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\" id=\"subtab-AdminThemesCatalog\">
                              <a href=\"/backoffice/index.php/improve/design/themes-catalog/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Theme Catalog
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"50\" id=\"subtab-AdminCmsContent\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCmsContent&amp;token=b4bf57ea279edb701317708402b3f715\" class=\"link\"> Pages
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"51\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"/backoffice/index.php/improve/design/modules/positions/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Positions
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"52\" id=\"subtab-AdminImages\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminImages&amp;token=477425b8cc5f6b67e7c97d418bc3bc39\" class=\"link\"> Image Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"117\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminLinkWidget&amp;token=0407b3e5e0ba0dcf5112cceb2bb0404e\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"53\" id=\"subtab-AdminParentShipping\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCarriers&amp;token=a850055b01e0885a7a2397299b7b772d\" class=\"link\">
                    <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                    <span>
                    Shipping
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-53\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\" id=\"subtab-AdminCarriers\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCarriers&amp;token=a850055b01e0885a7a2397299b7b772d\" class=\"link\"> Carriers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminShipping\">
                              <a href=\"/backoffice/index.php/improve/shipping/preferences?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Preferences
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"56\" id=\"subtab-AdminParentPayment\">
                  <a href=\"/backoffice/index.php/improve/payment/payment_methods?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\">
                    <i class=\"material-icons mi-payment\">payment</i>
                    <span>
                    Payment
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-56\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminPayment\">
                              <a href=\"/backoffice/index.php/improve/payment/payment_methods?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Payment Methods
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"/backoffice/index.php/improve/payment/preferences?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Preferences
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"59\" id=\"subtab-AdminInternational\">
                  <a href=\"/backoffice/index.php/improve/international/localization/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\">
                    <i class=\"material-icons mi-language\">language</i>
                    <span>
                    International
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-59\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"60\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"/backoffice/index.php/improve/international/localization/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Localization
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"65\" id=\"subtab-AdminParentCountries\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminCountries&amp;token=eefcfd35d8392503405943fe4ea72a93\" class=\"link\"> Locations
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"69\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminTaxes&amp;token=5db34d88dc46147b54f66db8506d3634\" class=\"link\"> Taxes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminTranslations\">
                              <a href=\"/backoffice/index.php/improve/international/translations/settings?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Translations
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"123\" id=\"subtab-Adminxprtblogdashboard\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=Adminxippost&amp;token=34556598b5615b89ec485ada6104ab1f\" class=\"link\">
                    <i class=\"material-icons mi-\"></i>
                    <span>
                    Xpert Blog
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-123\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"124\" id=\"subtab-Adminxippost\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=Adminxippost&amp;token=34556598b5615b89ec485ada6104ab1f\" class=\"link\"> Blog Posts
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"125\" id=\"subtab-Adminxipcategory\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=Adminxipcategory&amp;token=c0c2505b44e57e1e4020b6309ab708a6\" class=\"link\"> Blog Categories
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"126\" id=\"subtab-Adminxipcomment\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=Adminxipcomment&amp;token=779add016b346d03171d91d0d7d6aa2a\" class=\"link\"> Blog Comments
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"127\" id=\"subtab-Adminxipimagetype\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=Adminxipimagetype&amp;token=bc7d62a9bfd05c850ce4180bc4e8c6ac\" class=\"link\"> Blog Image Type
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"73\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Configure</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"74\" id=\"subtab-ShopParameters\">
                  <a href=\"/backoffice/index.php/configure/shop/preferences/preferences?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\">
                    <i class=\"material-icons mi-settings\">settings</i>
                    <span>
                    Shop Parameters
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-74\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"75\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"/backoffice/index.php/configure/shop/preferences/preferences?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> General
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"78\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"/backoffice/index.php/configure/shop/order-preferences/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Order Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\" id=\"subtab-AdminPPreferences\">
                              <a href=\"/backoffice/index.php/configure/shop/product-preferences/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Product Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"82\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"/backoffice/index.php/configure/shop/customer-preferences/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Customer Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"86\" id=\"subtab-AdminParentStores\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminContacts&amp;token=7cb5548570dc0f953c801cd73ba1439c\" class=\"link\"> Contact
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentMeta\">
                              <a href=\"/backoffice/index.php/configure/shop/seo-urls/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Traffic &amp; SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"93\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminSearchConf&amp;token=ee4b44ce93cb4f9724ff55aaa8eea026\" class=\"link\"> Search
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"119\" id=\"subtab-AdminGamification\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminGamification&amp;token=832887389bea77c4b94911e18eb2a2b3\" class=\"link\"> Merchant Expertise
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"96\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/backoffice/index.php/configure/advanced/system-information/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\">
                    <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                    <span>
                    Advanced Parameters
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-96\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"97\" id=\"subtab-AdminInformation\">
                              <a href=\"/backoffice/index.php/configure/advanced/system-information/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Information
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"98\" id=\"subtab-AdminPerformance\">
                              <a href=\"/backoffice/index.php/configure/advanced/performance/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Performance
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"99\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"/backoffice/index.php/configure/advanced/administration/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Administration
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminEmails\">
                              <a href=\"/backoffice/index.php/configure/advanced/emails/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> E-mail
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\" id=\"subtab-AdminImport\">
                              <a href=\"/backoffice/index.php/configure/advanced/import/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Import
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"102\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminEmployees&amp;token=29b403844ae8c844a240f455e6e96476\" class=\"link\"> Team
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"106\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminRequestSql&amp;token=b4c220d0014fbedaecfe0b26b23e12e6\" class=\"link\"> Database
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminLogs\">
                              <a href=\"/backoffice/index.php/configure/advanced/logs/?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" class=\"link\"> Logs
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"110\" id=\"subtab-AdminWebservice\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminWebservice&amp;token=16beff71e0a0d04803391a1658ba5db8\" class=\"link\"> Webservice
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"111\" id=\"subtab-AdminShopGroup\">
                              <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminShopGroup&amp;token=f8fba7a76273cd851a3657c2e7893317\" class=\"link\"> Multistore
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"130\" id=\"tab-AdminLaberMenu2\">
              <span class=\"title\">LaberThemes Configure</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"131\" id=\"subtab-Adminlabthemeoptions\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=Adminlabthemeoptions&amp;token=18aea8cf2c8473fb2417d685ae85bd15\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Configure Themeoptions
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"133\" id=\"subtab-Adminlabslideshow\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=Adminlabslideshow&amp;token=bb666260456a9819c15151fdc253c6f2\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Configure Slider show
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"135\" id=\"subtab-Adminlabcustomhtml\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=Adminlabcustomhtml&amp;token=4ef39b05af89c33d7a458e74b5c36e57\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Configure Custom HTML
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"137\" id=\"subtab-Adminlabmegamenu\">
                  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=Adminlabmegamenu&amp;token=1ed76dad1be993b2a845ce925fdc1017\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Configure Megamenu
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
            </ul>
  
</nav>

<div id=\"main-div\">

  
    
<div class=\"header-toolbar\">
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Catalog</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/backoffice/index.php/sell/catalog/products?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\" aria-current=\"page\">Products</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Products          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                                                                                    <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-add\"
                  href=\"/backoffice/index.php/sell/catalog/products/new?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\"                  title=\"Create a new product: CTRL+P\"                  data-toggle=\"pstooltip\"
                  data-placement=\"bottom\"                >
                  <i class=\"material-icons\">add_circle_outline</i>
                  New product
                </a>
                                                                  <a
                class=\"btn btn-outline-secondary \"
                id=\"page-header-desc-configuration-modules-list\"
                href=\"/backoffice/index.php/improve/modules/catalog?_token=3wNynodNP_Uh_kVpRpmFKSeb7qI94nHlyNgdvuGkyTg\"                title=\"Recommended Modules\"
                              >
                Recommended Modules
              </a>
            
            
                              <a class=\"btn btn-outline-secondary\" href=\"https://help.prestashop.com/en/doc/AdminProducts?version=1.7.5.2&amp;country=en\" title=\"Help\">
                  Help
                </a>
                                    </div>
        </div>
      
    </div>
  </div>

  
    
</div>
    
    <div class=\"content-div  \">

      

      
                        
      <div class=\"row \">
        <div class=\"col-sm-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  ";
        // line 1181
        $this->displayBlock('content_header', $context, $blocks);
        // line 1182
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1183
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1184
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1185
        echo "
          
        </div>
      </div>

    </div>

  
</div>

<div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>Oh no!</h1>
  <p class=\"mt-3\">
    The mobile version of this page is not available yet.
  </p>
  <p class=\"mt-2\">
    Please use a desktop computer to access this page, until is adapted to mobile.
  </p>
  <p class=\"mt-2\">
    Thank you.
  </p>
  <a href=\"http://shop.aquamarinespa.loc/backoffice/index.php?controller=AdminDashboard&amp;token=d1d9d1ba0652d886ca3e7d8c7cfd50dc\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Back
  </a>
</div>
<div class=\"mobile-layer\"></div>

  <div id=\"footer\" class=\"bootstrap\">
    
</div>


  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-EN&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/en/login?email=demo%40demo.com&amp;firstname=Demo&amp;lastname=Demo&amp;website=&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-EN&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/backoffice/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Connect your shop to PrestaShop's marketplace in order to automatically import all your Addons purchases.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Don't have an account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Discover the Power of PrestaShop Addons! Explore the PrestaShop Official Marketplace and find over 3 500 innovative modules and themes that optimize conversion rates, increase traffic, build customer loyalty and maximize your productivity</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Connect to PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/en/forgot-your-password\">I forgot my password</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/en/login?email=demo%40demo.com&amp;firstname=Demo&amp;lastname=Demo&amp;website=&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-EN&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tCreate an Account
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Sign in
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

  </div>

";
        // line 1294
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 72
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
    }

    // line 1181
    public function block_content_header($context, array $blocks = array())
    {
    }

    // line 1182
    public function block_content($context, array $blocks = array())
    {
    }

    // line 1183
    public function block_content_footer($context, array $blocks = array())
    {
    }

    // line 1184
    public function block_sidebar_right($context, array $blocks = array())
    {
    }

    // line 1294
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "__string_template__d5a695ffc5c5e6ef4237695a9212b9e5f033e3d6114395356fd88323a813e357";
    }

    public function getDebugInfo()
    {
        return array (  1373 => 1294,  1368 => 1184,  1363 => 1183,  1358 => 1182,  1353 => 1181,  1344 => 72,  1336 => 1294,  1225 => 1185,  1222 => 1184,  1219 => 1183,  1216 => 1182,  1214 => 1181,  101 => 72,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "__string_template__d5a695ffc5c5e6ef4237695a9212b9e5f033e3d6114395356fd88323a813e357", "");
    }
}
