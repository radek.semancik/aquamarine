<?php
/* Smarty version 3.1.33, created on 2019-11-07 17:06:01
  from 'C:\Work\pixape\aquamarinespa\themes\laber_outstock4\modules\labproductfilter\views\templates\hook\labproductfilter_accordion.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dc44ef9aa7006_03276756',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '192c25281337c234dd888ada87261b6a9ba02d03' => 
    array (
      0 => 'C:\\Work\\pixape\\aquamarinespa\\themes\\laber_outstock4\\modules\\labproductfilter\\views\\templates\\hook\\labproductfilter_accordion.tpl',
      1 => 1519810836,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/miniatures/product.tpl' => 1,
  ),
),false)) {
function content_5dc44ef9aa7006_03276756 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<?php $_smarty_tpl->_assignInScope('id_lang', Context::getContext()->language->id);
if ($_smarty_tpl->tpl_vars['use_slider']->value != 1) {?>
	<?php $_smarty_tpl->_assignInScope('nbItemsPerLine', 4);?>
	<?php $_smarty_tpl->_assignInScope('nbItemsPerLineTablet', 3);?>
	<?php $_smarty_tpl->_assignInScope('nbItemsPerLineMobile', 2);
}
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['product_groups']->value, 'product_group', false, NULL, 'product_group', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_group']->value) {
?>
	<div class="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 laberProductFilter laberProductGrid clearfix">
		<div class="title_block">
			<h3><span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['title'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span></h3>
		</div>
		<div class="prod-filter labContent">
			
					<?php if (isset($_smarty_tpl->tpl_vars['product_group']->value['product_list']) && count($_smarty_tpl->tpl_vars['product_group']->value['product_list']) > 0) {?>
					<div class="labProductFilter row">
						<div class="owlProductFilter-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['use_slider']->value != 1) {?>no-Slider row<?php }?>">
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['product_group']->value['product_list'], 'product', false, NULL, 'product_list', array (
  'iteration' => true,
  'last' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['total'];
?>
								<?php if (isset($_smarty_tpl->tpl_vars['product']->value['id_product'])) {?>
									<?php if ($_smarty_tpl->tpl_vars['use_slider']->value != 1) {?>
										<div class="item-inner col-lg-3 col-md-3 col-sm-4 col-xs-12  
										<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null)%$_smarty_tpl->tpl_vars['nbItemsPerLine']->value == 0) {?> last-in-line
										<?php } elseif ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null)%$_smarty_tpl->tpl_vars['nbItemsPerLine']->value == 1) {?> first-in-line<?php }?>
										<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null)%$_smarty_tpl->tpl_vars['nbItemsPerLineTablet']->value == 0) {?> last-item-of-tablet-line
										<?php } elseif ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null)%$_smarty_tpl->tpl_vars['nbItemsPerLineTablet']->value == 1) {?> first-item-of-tablet-line<?php }?>
										<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null)%$_smarty_tpl->tpl_vars['nbItemsPerLineMobile']->value == 0) {?> last-item-of-mobile-line
										<?php } elseif ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null)%$_smarty_tpl->tpl_vars['nbItemsPerLineMobile']->value == 1) {?> first-item-of-mobile-line<?php }?>
										wow fadeIn " data-wow-delay="<?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null), ENT_QUOTES, 'UTF-8');?>
00ms">
										
									<?php } else { ?>
										<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null)%$_smarty_tpl->tpl_vars['num_row']->value == 1 || $_smarty_tpl->tpl_vars['num_row']->value == 1) {?>
										<div class="item-inner  ajax_block_product wow fadeIn " data-wow-delay="<?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null), ENT_QUOTES, 'UTF-8');?>
00ms">
										<?php }?>
									<?php }?>
										<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7047041085dc44ef9a98db2_61570531', 'product');
?>

									<?php if ($_smarty_tpl->tpl_vars['use_slider']->value != 1) {?>
										</div>
									<?php } else { ?>
										<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null)%$_smarty_tpl->tpl_vars['num_row']->value == 0 || (isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['last'] : null) || $_smarty_tpl->tpl_vars['num_row']->value == 1) {?>
											</div>
										<?php }?>
									<?php }?>
								<?php }?>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
						</div>
					</div>
					<?php } else { ?>
						<div class="item product-box ajax_block_product">
							<p class="alert alert-warning"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No product at this time','d'=>'Modules.LABProductfilter'),$_smarty_tpl ) );?>
</p>
						</div>	
					<?php }?>
			  
			
		</div>
		<?php if ($_smarty_tpl->tpl_vars['use_slider']->value == 1) {?>
			<?php echo '<script'; ?>
 type="text/javascript">
			$(document).ready(function() {
				var owl = $(".owlProductFilter-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
");
				owl.owlCarousel({
					items : <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['num_column']->value, ENT_QUOTES, 'UTF-8');?>
,
					itemsDesktop : [1199,4],
					itemsDesktopSmall : [991,3],
					itemsTablet: [767,2],
					itemsMobile : [480,1],
					navigation : true,
					navigationText : ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
					rewindNav : false,
					autoPlay :  false,
					stopOnHover: false,
					pagination : false,
				});
			});
			<?php echo '</script'; ?>
>
		<?php }?>
	</div>
	
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

	<?php }
/* {block 'product'} */
class Block_7047041085dc44ef9a98db2_61570531 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product' => 
  array (
    0 => 'Block_7047041085dc44ef9a98db2_61570531',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

											<?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
										<?php
}
}
/* {/block 'product'} */
}
