<?php
/* Smarty version 3.1.33, created on 2019-11-07 17:06:01
  from 'module:labthemeoptionsviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dc44ef90e8c95_41006669',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0d2ccbdd283803665a2b0e52e129eaefc367a85' => 
    array (
      0 => 'module:labthemeoptionsviewstempl',
      1 => 1520394361,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dc44ef90e8c95_41006669 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['lab_show_color']->value != 0) {?>
<style type="text/css">
<?php if ($_smarty_tpl->tpl_vars['lab_cbgcolor']->value != '') {?>
.tabs .nav-tabs .nav-link.active, .tabs .nav-tabs .nav-link:hover,
.comments_note div.star_on, #productCommentsBlock div.star_on,
.block_newsletter form input.btn:hover,
.block-categories .category-sub-menu li[data-depth="0"] > a:hover,
.copyright a,.laberFooter-center ul li a:hover,.laberFooter-bottom .payment a:hover,
.blog_post_content .post_meta .meta_author span,.blog_post_content .read_more:hover,
.comments_note div.star_on, #productCommentsBlock div.star_on,
#new_comment_form div.star_hover:before, #new_comment_form div.star_on:before,
.laberProductGrid .laberCart .laberBottom:hover,
.subpage .laberProductGrid .productName:hover a, .labcolumn .productName:hover a, .laberProductGrid .productName:hover a,
.laberProductGrid .laberItem .laberwishlist a:hover, .laberProductGrid .laberActions a:hover,
#header .laber-cart .cart-prices .amount, #header .laber-cart .cart_block .products .cart-info .laberPrice .price,
#header .dropdown-menu li a.dropdown-item:hover, #header .dropdown-menu li.current a.dropdown-item,
.dropdown.right-nav .expand-more:hover,
.search-widget .dropdown:hover .expand-more,
a:hover, #header a:hover
{
	color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lab_cbgcolor']->value, ENT_QUOTES, 'UTF-8');?>
 ;
}

/* background-color:#6fc138 */
.btn-primary.focus, .btn-primary:focus, .btn-primary:hover, .btn-secondary.focus, .btn-secondary:focus, .btn-secondary:hover, .btn-tertiary:focus, .btn-tertiary:hover, .focus.btn-tertiary,
.scroll-box-arrows i:hover,
.laberSocial-sharing li a:hover,
.product-actions .add-to-cart:hover,
.pagination .page-list li a:hover, .pagination .page-list li a.disabled,
.btn-primary.focus, .btn-primary:focus, .btn-primary:hover, .btn-secondary.focus, .btn-secondary:focus, .btn-secondary:hover, .btn-tertiary:focus, .btn-tertiary:hover, .focus.btn-tertiary,
.laberGridList li a:hover, .active_list .laberGridList li#list a, .active_grid .laberGridList li#grid a,
.laberBlog .slick-arrow:hover,
.laberProductGrid .owl-next:hover, .laberProductGrid .owl-prev:hover,
.laber-product-flags li.laber-new,
.mypresta_scrollup:hover span,.mypresta_scrollup span,
#newsletter_block_popup .block_content .send-reqest:hover,
.laberPopupnewsletter-i .close,
#header .laber-cart .cart_block .cart-buttons a:hover,
#header .laber-user-info a.account, #header .laber-user-info a.register
{
	background-color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lab_cbgcolor']->value, ENT_QUOTES, 'UTF-8');?>
;
}
/* border-color:#6fc138 */
.btn-primary.focus, .btn-primary:focus, .btn-primary:hover, .btn-secondary.focus, .btn-secondary:focus, .btn-secondary:hover, .btn-tertiary:focus, .btn-tertiary:hover, .focus.btn-tertiary,
.scroll-box-arrows i:hover,
.product-images > li.thumb-container > .thumb.selected, .product-images > li.thumb-container > .thumb:hover,
.product-actions .add-to-cart:hover,
.block_newsletter form input[type="text"]:focus,
.block_newsletter form input.btn:hover,
.pagination .page-list li a:hover, .pagination .page-list li a.disabled,
.btn-primary.focus, .btn-primary:focus, .btn-primary:hover, .btn-secondary.focus, .btn-secondary:focus, .btn-secondary:hover, .btn-tertiary:focus, .btn-tertiary:hover, .focus.btn-tertiary,
.blog_post_content .read_more:hover,
.nivo-controlNav a:hover, .nivo-controlNav a.active
{
	border-color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lab_cbgcolor']->value, ENT_QUOTES, 'UTF-8');?>
;
}

.form-control:focus, .input-group.focus,
.search-widget .laber-search form input[type="text"]:focus{
	outline-color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lab_cbgcolor']->value, ENT_QUOTES, 'UTF-8');?>
;
}
<?php }?>
</style>
<?php }
if ($_smarty_tpl->tpl_vars['labshowthemecolor']->value == 1) {?>
			<?php if ($_smarty_tpl->tpl_vars['labthemecolor']->value && $_smarty_tpl->tpl_vars['labthemecolor']->value != 'default') {?>
				<link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['PS_BASE_URL']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['PS_BASE_URI']->value, ENT_QUOTES, 'UTF-8');?>
themes/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['LAB_THEMENAME']->value, ENT_QUOTES, 'UTF-8');?>
/assets/css/color/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['labthemecolor']->value, ENT_QUOTES, 'UTF-8');?>
.css" />
			<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['labskin']->value) {?>
			<style type="text/css">
				body{
					background-image: url("<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['PS_BASE_URL']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['PS_BASE_URI']->value, ENT_QUOTES, 'UTF-8');?>
modules/labthemeoptions/views/templates/front/colortool/images/pattern/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['labskin']->value, ENT_QUOTES, 'UTF-8');?>
.png") ;
				}
			</style>
        <?php }
}
}
}
