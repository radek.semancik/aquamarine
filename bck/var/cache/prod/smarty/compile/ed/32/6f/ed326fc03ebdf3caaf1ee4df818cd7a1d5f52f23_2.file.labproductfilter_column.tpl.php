<?php
/* Smarty version 3.1.33, created on 2019-11-07 17:06:01
  from 'C:\Work\pixape\aquamarinespa\themes\laber_outstock4\modules\labproductfilter\views\templates\hook\labproductfilter_column.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dc44ef9c144b9_15233117',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ed326fc03ebdf3caaf1ee4df818cd7a1d5f52f23' => 
    array (
      0 => 'C:\\Work\\pixape\\aquamarinespa\\themes\\laber_outstock4\\modules\\labproductfilter\\views\\templates\\hook\\labproductfilter_column.tpl',
      1 => 1519810385,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./product-column.tpl' => 1,
  ),
),false)) {
function content_5dc44ef9c144b9_15233117 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<div class="row">
<?php $_smarty_tpl->_assignInScope('count', 12/count($_smarty_tpl->tpl_vars['product_groups']->value));?>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['product_groups']->value, 'product_group', false, NULL, 'product_group', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_group']->value) {
?>
			<div class="col-md-4">
			<div class="labProductList labcolumn clearfix">
			<div class="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 column">
				<div class="title_block">
					<h3>
						<span>
							<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['title'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

						</span>
					</h3>
				</div>
				<?php if (isset($_smarty_tpl->tpl_vars['product_group']->value['product_list']) && count($_smarty_tpl->tpl_vars['product_group']->value['product_list']) > 0) {?>
				<div class="labProductFilter row">
					<div class="owlProductFilter-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
-column laberColumn">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['product_group']->value['product_list'], 'product', false, NULL, 'product_list', array (
  'iteration' => true,
  'last' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['total'];
?>
							<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null)%$_smarty_tpl->tpl_vars['num_row']->value == 1 || $_smarty_tpl->tpl_vars['num_row']->value == 1) {?>
								<div class="item-inner ajax_block_product ">
							<?php }?>
								<div class="item wow fadeIn " data-wow-delay="<?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null), ENT_QUOTES, 'UTF-8');?>
00ms">
									<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9948345125dc44ef9c089f0_76544552', 'product');
?>

								</div>
							<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['iteration'] : null)%$_smarty_tpl->tpl_vars['num_row']->value == 0 || (isset($_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_product_list']->value['last'] : null) || $_smarty_tpl->tpl_vars['num_row']->value == 1) {?>
								</div>
							<?php }?>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</div>
				</div>
				<?php } else { ?>
					<div class="item product-box ajax_block_product">
						<p class="alert alert-warning"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No product at this time','d'=>'Modules.LABProductfilter'),$_smarty_tpl ) );?>
</p>
					</div>	
				<?php }?>
				<div class="owl-buttons">
					<p class="owl-prev prev-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
-column"><i class="fa fa-angle-left"></i></p>
					<p class="owl-next next-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
-column"><i class="fa fa-angle-right"></i></p>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['use_slider']->value == 1) {?>
					<?php echo '<script'; ?>
 type="text/javascript">
					$(document).ready(function() {
						var owl = $(".owlProductFilter-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
-column");
						owl.owlCarousel({
							items : <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['num_column']->value, ENT_QUOTES, 'UTF-8');?>
,
							itemsDesktop : [1199,1],
							itemsDesktopSmall : [991,1],
							itemsTablet: [767,2],
							itemsMobile : [480,1],
							navigation : false,
							rewindNav : false,
							autoPlay :  false,
							stopOnHover: false,
							autoHeight : true,
							pagination : false,
						});
						$(".next-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
-column").click(function(){
							owl.trigger('owl.next');
						})
						$(".prev-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_group']->value['class'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
-column").click(function(){
							owl.trigger('owl.prev');
						})
					});
					<?php echo '</script'; ?>
>
				<?php }?>
			</div>
			</div>
			</div>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>		
</div><?php }
/* {block 'product'} */
class Block_9948345125dc44ef9c089f0_76544552 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product' => 
  array (
    0 => 'Block_9948345125dc44ef9c089f0_76544552',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

										<?php $_smarty_tpl->_subTemplateRender('file:./product-column.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
									<?php
}
}
/* {/block 'product'} */
}
