<?php
/* Smarty version 3.1.33, created on 2019-11-07 17:06:01
  from 'C:\Work\pixape\aquamarinespa\themes\laber_outstock4\templates\_partials\displayPositionTop.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dc44ef99b6fb2_94521170',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '948c063c5eb0379f4c9d92a7fb72f95ededd4cc4' => 
    array (
      0 => 'C:\\Work\\pixape\\aquamarinespa\\themes\\laber_outstock4\\templates\\_partials\\displayPositionTop.tpl',
      1 => 1519810877,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dc44ef99b6fb2_94521170 (Smarty_Internal_Template $_smarty_tpl) {
if (Hook::exec('displayPosition1')) {?>
<div class="displayPosition displayPosition1">
	<div class="container">
		<div class="row">
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayPosition1"),$_smarty_tpl ) );?>

		</div>
	</div>
</div>
<?php }
if (Hook::exec('displayPosition2')) {?>
<div class="displayPosition displayPosition2">
	<div class="container">
		<div class="row">
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayPosition2"),$_smarty_tpl ) );?>

		</div>
	</div>
</div>
<?php }
if (Hook::exec('displayPosition3')) {?>
<div class="displayPosition displayPosition3">
	
		<div class="container">
			<div class="row">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayPosition3"),$_smarty_tpl ) );?>

			</div>
		</div>
</div>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'index') {?>
	<?php if (Hook::exec('displayManufacture')) {?>
	<div class="laberManufacture">
		<div class="container">
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayManufacture"),$_smarty_tpl ) );?>

		</div>
	</div>
	<?php }
}
if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'index') {?>
	<?php if (Hook::exec('displayBlog')) {?>
	<div class="laberBlog">
		<div class="container">
			<div class="row">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayBlog"),$_smarty_tpl ) );?>

			</div>
		</div>
	</div>
	<?php }
}?>

<?php }
}
