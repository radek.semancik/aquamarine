<?php
/* Smarty version 3.1.33, created on 2019-11-07 17:06:00
  from 'C:\Work\pixape\aquamarinespa\themes\laber_outstock4\templates\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dc44ef8edbf46_73716750',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '312eed5ca43808fe38078c0af6cbedefbe22f2f2' => 
    array (
      0 => 'C:\\Work\\pixape\\aquamarinespa\\themes\\laber_outstock4\\templates\\index.tpl',
      1 => 1517450352,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dc44ef8edbf46_73716750 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20346737675dc44ef8ed83d9_65036196', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_10916771785dc44ef8ed8ef2_36296168 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_6460885885dc44ef8eda3e8_01341842 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_4937753455dc44ef8ed9c53_25350203 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6460885885dc44ef8eda3e8_01341842', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_20346737675dc44ef8ed83d9_65036196 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_20346737675dc44ef8ed83d9_65036196',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_10916771785dc44ef8ed8ef2_36296168',
  ),
  'page_content' => 
  array (
    0 => 'Block_4937753455dc44ef8ed9c53_25350203',
  ),
  'hook_home' => 
  array (
    0 => 'Block_6460885885dc44ef8eda3e8_01341842',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <div id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10916771785dc44ef8ed8ef2_36296168', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4937753455dc44ef8ed9c53_25350203', 'page_content', $this->tplIndex);
?>

      </div>
    <?php
}
}
/* {/block 'page_content_container'} */
}
