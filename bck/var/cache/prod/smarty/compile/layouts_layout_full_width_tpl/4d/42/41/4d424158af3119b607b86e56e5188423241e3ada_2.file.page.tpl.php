<?php
/* Smarty version 3.1.33, created on 2019-11-07 17:06:00
  from 'C:\Work\pixape\aquamarinespa\themes\laber_outstock4\templates\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dc44ef8f08ce6_73651702',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4d424158af3119b607b86e56e5188423241e3ada' => 
    array (
      0 => 'C:\\Work\\pixape\\aquamarinespa\\themes\\laber_outstock4\\templates\\page.tpl',
      1 => 1517450352,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dc44ef8f08ce6_73651702 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16885749045dc44ef8efe850_56384393', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_17159943465dc44ef8effc98_92459676 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_8874116375dc44ef8eff196_22913328 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17159943465dc44ef8effc98_92459676', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_6133251585dc44ef8f05cc1_26519166 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_8108768345dc44ef8f066f4_02552660 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_3776016435dc44ef8f05478_89555739 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6133251585dc44ef8f05cc1_26519166', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8108768345dc44ef8f066f4_02552660', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_19627557235dc44ef8f07c48_66997598 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_8903226835dc44ef8f07595_22278927 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19627557235dc44ef8f07c48_66997598', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_16885749045dc44ef8efe850_56384393 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_16885749045dc44ef8efe850_56384393',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_8874116375dc44ef8eff196_22913328',
  ),
  'page_title' => 
  array (
    0 => 'Block_17159943465dc44ef8effc98_92459676',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_3776016435dc44ef8f05478_89555739',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_6133251585dc44ef8f05cc1_26519166',
  ),
  'page_content' => 
  array (
    0 => 'Block_8108768345dc44ef8f066f4_02552660',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_8903226835dc44ef8f07595_22278927',
  ),
  'page_footer' => 
  array (
    0 => 'Block_19627557235dc44ef8f07c48_66997598',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <div id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8874116375dc44ef8eff196_22913328', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3776016435dc44ef8f05478_89555739', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8903226835dc44ef8f07595_22278927', 'page_footer_container', $this->tplIndex);
?>


  </div>

<?php
}
}
/* {/block 'content'} */
}
