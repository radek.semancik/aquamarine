<?php
/* Smarty version 3.1.33, created on 2019-11-07 17:06:02
  from 'C:\Work\pixape\aquamarinespa\themes\laber_outstock4\templates\_partials\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dc44efa5c9ce9_24386151',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e4861474791d2c8189c27b7913073f90cd33f849' => 
    array (
      0 => 'C:\\Work\\pixape\\aquamarinespa\\themes\\laber_outstock4\\templates\\_partials\\footer.tpl',
      1 => 1517450352,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dc44efa5c9ce9_24386151 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="laberFooter-top">
	<div class="container">
	  <div class="row">
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterBefore'),$_smarty_tpl ) );?>

	  </div>
	</div>
</div>

<div class="laberFooter-center">
  <div class="container">
    <div class="row">
      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooter'),$_smarty_tpl ) );?>

    </div>
  </div>
</div>
<div class="laberFooter-bottom">
	<div class="container">
		<div class="row">
		  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterAfter'),$_smarty_tpl ) );?>

		</div>
	</div>
</div>
<?php }
}
