<?php
/* Smarty version 3.1.33, created on 2019-11-07 17:06:01
  from 'module:pscontactinfonav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dc44ef924db88_97058268',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0eb2119957cbc13b240126b3ccd8fac8f109f1e2' => 
    array (
      0 => 'module:pscontactinfonav.tpl',
      1 => 1517541837,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dc44ef924db88_97058268 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="_desktop_contact_link">
	<div class="language-selector-wrapper">
		<span class="title-language"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Contact us','d'=>'Shop.Theme.Laberthemes'),$_smarty_tpl ) );?>
</span>
	  <div id="contact-link">
		<ul class="laber-contact-link media-body">
			<?php if ($_smarty_tpl->tpl_vars['contact_infos']->value['email']) {?>
			<li class="item email">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email:','d'=>'Shop.Theme.Laberthemes'),$_smarty_tpl ) );?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_infos']->value['email'], ENT_QUOTES, 'UTF-8');?>

			</li>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['contact_infos']->value['phone']) {?>
			<li class="item phone">
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Phone:','d'=>'Shop.Theme.Laberthemes'),$_smarty_tpl ) );?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_infos']->value['phone'], ENT_QUOTES, 'UTF-8');?>

			</li>
			<?php }?>
			<!-- <li class=" pull-left item contact">
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['contact'], ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Contact us','d'=>'Shop.Theme'),$_smarty_tpl ) );?>
</a>
			</li> -->
		</ul>
	  </div>
	</div>
</div>
<?php }
}
