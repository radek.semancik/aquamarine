<?php
/* Smarty version 3.1.33, created on 2019-11-07 17:06:02
  from 'C:\Work\pixape\aquamarinespa\modules\labpopupnewsletter\labpopupnewsletter.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dc44efa8419b5_03525209',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '17bd6ecc3ebec9fefcadd2143e53a3a8f5f8df0d' => 
    array (
      0 => 'C:\\Work\\pixape\\aquamarinespa\\modules\\labpopupnewsletter\\labpopupnewsletter.tpl',
      1 => 1519318291,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dc44efa8419b5_03525209 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="laberPopupnewsletter" class="modal fade" tabindex="-1" role="dialog">

  <div class="laberPopupnewsletter-i" role="document" style="<?php if ($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_WIDTH']) {?>max-width:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_WIDTH'], ENT_QUOTES, 'UTF-8');?>
px;<?php }?>">

    <button type="button" class="close" data-dismiss="modal" aria-label="Close">close</button>

<div class="labpopupnewsletter" style="<?php if ($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_HEIGHT']) {?>height:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_HEIGHT'], ENT_QUOTES, 'UTF-8');?>
px;<?php }
if ($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_BG'] == 1 && !empty($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_BG_IMAGE'])) {?>background-image: url(<?php echo htmlspecialchars(($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_BG_IMAGE']), ENT_QUOTES, 'UTF-8');?>
);<?php }?>">

	<?php if ($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_NEWSLETTER'] == 1) {?>

	<div id="newsletter_block_popup" class="block">

		<div class="block_content">

		<?php if (isset($_smarty_tpl->tpl_vars['msg']->value) && $_smarty_tpl->tpl_vars['msg']->value) {?>

			<p class="<?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>warning_inline<?php } else { ?>success_inline<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['msg']->value, ENT_QUOTES, 'UTF-8');?>
</p>

		<?php }?>

		<form method="post">

				<?php if ($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LABER_TITLE_POPUP']) {?>
					<div class="newsletter_title">
						<h3 class="h3"><?php echo stripslashes($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LABER_TITLE_POPUP']);?>
</h3>
					</div>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_TEXT']) {?>
					<div class="laberContent">
						<?php echo stripslashes($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_TEXT']);?>

					</div>
				<?php }?>
				<div class="labAlert"></div>

				<input class="inputNew" id="labnewsletter-input" type="text" name="email" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enter your mail...','mod'=>'labpopupnewsletter'),$_smarty_tpl ) );?>
"/>

				<div class="send-reqest button_unique"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Subscribe!','mod'=>'labpopupnewsletter'),$_smarty_tpl ) );?>
</div>
		</form>

		</div>

			<div class="newsletter_block_popup-bottom">

				<div class="subscribe-bottom">	

					<input id="laber_newsletter_dont_show_again" type="checkbox">

				</div>

				<label class="laber_newsletter_dont_show_again" for="laber_newsletter_dont_show_again"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Do not show this popup again','mod'=>'labpopupnewsletter'),$_smarty_tpl ) );?>
</label>

			</div>

		</div>

	<?php }?>

</div>

  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<?php if ($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_NEWSLETTER'] == 1) {?>

<?php echo '<script'; ?>
 type="text/javascript">

    var placeholder2 = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enter your mail...','mod'=>'labpopupnewsletter','js'=>1),$_smarty_tpl ) );?>
";

    

        $(document).ready(function() {

            $('#labnewsletter-input').on({

                focus: function() {

                    if ($(this).val() == placeholder2) {

                        $(this).val('');

                    }

                },

                blur: function() {

                    if ($(this).val() == '') {

                        $(this).val(placeholder2);

                    }

                }

            });

        });

    

<?php echo '</script'; ?>
>

<?php }?>

<?php echo '<script'; ?>
 type="text/javascript">

var field_width=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_WIDTH'], ENT_QUOTES, 'UTF-8');?>
;

var field_height=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_HEIGHT'], ENT_QUOTES, 'UTF-8');?>
;

var field_newsletter=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_NEWSLETTER'], ENT_QUOTES, 'UTF-8');?>
;

var field_path='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['laber_popupnewlestter']->value['LAB_PATH'], ENT_QUOTES, 'UTF-8');?>
';

<?php echo '</script'; ?>
>

<?php }
}
