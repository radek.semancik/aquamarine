<div class="laberFooter-top">
	<div class="container">
	  <div class="row">
		{hook h='displayFooterBefore'}
	  </div>
	</div>
</div>

<div class="laberFooter-center">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <h3>Váš účet</h3>
                <ul>
                    <li><a href="/muj-ucet">Osobní informace</a></li>
                    <li><a href="/adresy">Adresa</a></li>
                    <li><a href="/historie-objednavek">Historie objednávek</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <h3>Informace</h3>
                <ul>
                    <li><a href="/napiste-nam">Kontakty</a></li>
                    <li><a href="/content/1-doprava">Doprava</a></li>
                    <li><a href="/content/3-obchodni-podminky">Obchodní podmínky</a></li>
                    <li><a href="/content/4-prodejna">O nás</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <h3>Rychlý kontakt</h3>
                <ul>
                    <li>Telefon: <span class="phone">+420 272 654 944</span></li>
                    <li>E-mail: <a href="mailto:info@aquamarinespa.cz"><span class="email">info@aquamarinespa.cz</span></a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 no-padding">
                <h3>Adresa</h3>
                Aquamarine Spa s.r.o<br>
                Vrchotovy Janovice 266<br>
                257 53 Vrchotovy Janovice






            </div>
        </div>
    </div>
</div>

<div class="laberFooter-bottom">
	<div class="container">
		<div class="row">
		  {hook h='displayFooterAfter'}
		</div>
	</div>
</div>



