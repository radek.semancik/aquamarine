{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='head_seo' prepend}
  <link rel="canonical" href="{$product.canonical_url}">
{/block}

{block name='head' append}
  <meta property="og:type" content="product">
  <meta property="og:url" content="{$urls.current_url}">
  <meta property="og:title" content="{$page.meta.title}">
  <meta property="og:site_name" content="{$shop.name}">
  <meta property="og:description" content="{$page.meta.description}">
  <meta property="og:image" content="{$product.cover.large.url}">
  <meta property="product:pretax_price:amount" content="{$product.price_tax_exc}">
  <meta property="product:pretax_price:currency" content="{$currency.iso_code}">
  <meta property="product:price:amount" content="{$product.price_amount}">
  <meta property="product:price:currency" content="{$currency.iso_code}">
  {if isset($product.weight) && ($product.weight != 0)}
  <meta property="product:weight:value" content="{$product.weight}">
  <meta property="product:weight:units" content="{$product.weight_unit}">
  {/if}
{/block}

{block name='content'}

  <section id="main" itemscope itemtype="https://schema.org/Product">
    <meta itemprop="url" content="{$product.url}">

    <div class="laberProduct">
    <div class="row no-margin">
      <div class="col-md-6">
        {block name='page_content_container'}
          <section class="page-content" id="content">
            {block name='page_content'}
              <!-- {block name='product_flags'}
                <ul class="product-flags">
                  {foreach from=$product.flags item=flag}
                    <li class="product-flag {$flag.type}">{$flag.label}</li>
                  {/foreach}
                </ul>
              {/block} -->

              {block name='product_cover_tumbnails'}
                {include file='catalog/_partials/product-cover-thumbnails.tpl'}
              {/block}
              <div class="scroll-box-arrows">
				<i class="left" aria-hidden="true"></i>
				<i class="right" aria-hidden="true"></i>
              </div>

            {/block}
          </section>
        {/block}
        </div>
        <div class="col-md-6">
			{hook h='displayProductNextPrev'}
          {block name='page_header_container'}
            {block name='page_header'}
              <h1 class="h1" itemprop="name">{block name='page_title'}{$product.name}{/block}</h1>
            {/block}
          {/block}
		   {*hook h='displayProductListReviews' product=$product*}
		  {block name='product_description_short'}
              <div class="product-description-short" id="product-description-short-{$product.id}" itemprop="description">{$product.description_short nofilter}</div>
            {/block}
		 
          {block name='product_prices'}
            {include file='catalog/_partials/product-prices.tpl'}
          {/block}

          <div class="product-information">
            
            {if $product.is_customizable && count($product.customizations.fields)}
              {block name='product_customization'}
                {include file="catalog/_partials/product-customization.tpl" customizations=$product.customizations}
              {/block}
            {/if}

            <div class="product-actions">
              {block name='product_buy'}
                <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                  <input type="hidden" name="token" value="{$static_token}">
                  <input type="hidden" name="id_product" value="{$product.id}" id="product_page_product_id">
                  <input type="hidden" name="id_customization" value="{$product.id_customization}" id="product_customization_id">

                  {block name='product_variants'}
                    {include file='catalog/_partials/product-variants.tpl'}
                  {/block}

                  {block name='product_pack'}
                    {if $packItems}
                      <section class="product-pack">
                        <h3 class="h4">{l s='This pack contains' d='Shop.Theme.Catalog'}</h3>
                        {foreach from=$packItems item="product_pack"}
                          {block name='product_miniature'}
                            {include file='catalog/_partials/miniatures/pack-product.tpl' product=$product_pack}
                          {/block}
                        {/foreach}
                    </section>
                    {/if}
                  {/block}

                  {block name='product_discounts'}
                    {include file='catalog/_partials/product-discounts.tpl'}
                  {/block}

                  {block name='product_add_to_cart'}
                    {include file='catalog/_partials/product-add-to-cart.tpl'}
                  {/block}

                  {hook h='displayProductButtons' product=$product}

                  {block name='product_refresh'}
                    <input class="product-refresh ps-hidden-by-js" name="refresh" type="submit" value="{l s='Refresh' d='Shop.Theme.Actions'}">
                  {/block}
                </form>
              {/block}

            </div>
              
             {if $product.images|@count > 1}
                <div class="js-qv-mask mask">
                 <ul class="product-images js-qv-product-images">
                   {foreach from=$product.images item=image}
                     <li class="thumb-container">
                       <img
                         class="thumb js-thumb {if $image.id_image == $product.cover.id_image} selected {/if}"
                         data-image-medium-src="{$image.bySize.medium_default.url}"
                         data-image-large-src="{$image.bySize.large_default.url}"
                         src="{$image.bySize.small_default.url}"
                         alt="{$image.legend}"
                         title="{$image.legend}"
                         itemprop="image"
                       >
                     </li>
                   {/foreach}
                 </ul>
               </div> 
             {/if} 
            

            {hook h='displayReassurance'}

            
        </div>
      </div>
    </div>
    </div>
	<div class="col-md-12 col-xs-12">
	<div class="tabs laberTabs">
          {if !empty($product.description)}<h2>{l s='Description' d='Shop.Theme.Catalog'}</h2>{/if}
	  <div class="tab-content" id="tab-content">
	   <div class="tab-pane fade in{if $product.description} active{/if}" id="description">
		 {block name='product_description'}
		   <div class="product-description">{$product.description nofilter}</div>
		 {/block}
	   </div>

	   {block name='product_details'}
		 {include file='catalog/_partials/product-details.tpl'}
	   {/block}
	   {block name='product_attachments'}
		 {if $product.attachments}
		  <div class="tab-pane fade in" id="attachments">
			 <section class="product-attachments">
			   <h3 class="h5 text-uppercase">{l s='Download' d='Shop.Theme.Actions'}</h3>
			   {foreach from=$product.attachments item=attachment}
				 <div class="attachment">
				   <h4><a href="{url entity='attachment' params=['id_attachment' => $attachment.id_attachment]}">{$attachment.name}</a></h4>
				   <p>{$attachment.description}</p
				   <a href="{url entity='attachment' params=['id_attachment' => $attachment.id_attachment]}">
					 {l s='Download' d='Shop.Theme.Actions'} ({$attachment.file_size_formatted})
				   </a>
				 </div>
			   {/foreach}
			 </section>
		   </div>
		 {/if}
	   {/block}
	   {foreach from=$product.extraContent item=extra key=extraKey}
	   <div class="tab-pane fade in {$extra.attr.class}" id="extra-{$extraKey}" {foreach $extra.attr as $key => $val} {$key}="{$val}"{/foreach}>
		   {$extra.content nofilter}
	   </div>
	   {/foreach}
	</div>
  </div>
  </div>
	{*hook h='productFooterReviews'*}
    {block name='product_accessories'}
      {if $accessories}
        <section class="col-md-12 col-xs-12">
        <div class="product-accessories clearfix laberProductGrid">
			<div class="title_block">
				<h3><span>{l s='You might also like' d='Shop.Theme.Catalog'}</span></h3>
			</div>
          <div class="laberAcce row">
			  <div class="laberAccessories">
				{foreach from=$accessories item="product_accessory" name=product_list}
				  {block name='product_miniature'}
					<div class="item-inner  ajax_block_product wow fadeIn " data-wow-delay="{$smarty.foreach.product_list.iteration}00ms">
					{include file='catalog/_partials/miniatures/product.tpl' product=$product_accessory}
					</div>	
				  {/block}
				{/foreach}
			  </div>
          </div>
		  <div class="owl-buttons">
			<p class="owl-prev prevAccessories"><i class="fa fa-angle-left"></i></p>
			<p class="owl-next nextAccessories"><i class="fa fa-angle-right"></i></p>
		</div>
		</div>
        </section>
		<script type="text/javascript">
			$(document).ready(function() {
				var owl = $(".laberAccessories");
				owl.owlCarousel({
					items : 4,
					itemsDesktop : [1199,4],
					itemsDesktopSmall : [991,3],
					itemsTablet: [767,2],
					itemsMobile : [480,1],
					rewindNav : false,
					autoPlay :  false,
					stopOnHover: false,
					pagination : false,
				});
				$(".nextAccessories").click(function(){
				owl.trigger('owl.next');
				})
				$(".prevAccessories").click(function(){
				owl.trigger('owl.prev');
				})
			});
		</script>
      {/if}
    {/block}

    {block name='product_footer'}
      {hook h='displayFooterProduct' product=$product category=$category}
    {/block}

    {block name='product_images_modal'}
      {include file='catalog/_partials/product-images-modal.tpl'}
    {/block}

    {block name='page_footer_container'}
      <footer class="page-footer">
        {block name='page_footer'}
          <!-- Footer content -->
        {/block}
      </footer>
    {/block}
  </section>

{/block}
